({

    /** getDatosInventarioDistribuidor */
    getDatosInventarioDistribuidor : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosInventarioDistribuidor..");
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistCons");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objClienteResult = objResponse.getReturnValue();
            	Component.set("v.Cliente", objClienteResult);
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosInventarioDistribuidor objClienteResult: ", objClienteResult);
            	this.getDatosDistribInvent(Component, Event, objClienteResult);
            	this.getReglasColaboraInvent(Component, Event, objClienteResult);
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
	}, 

    /** getDatosDistribInvent */
    getDatosDistribInvent : function(Component, Event, objClienteResult){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosDistribInvent objClienteResult: ", objClienteResult);
    	
    	//Unas variables para las funciones y los tipos de inventarios
        let opTFuncDistPrinc = [];
        let optTipInvPrinc = [];
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getObjDatosDistribInv");
        objAction.setParams({
        	"recordId" : Component.get("v.strIdRegistro"), 
        	"objClientePrm" : JSON.stringify(objClienteResult)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	//Toma el resultado del llamado de la función	
            	let objWrpMapDatosDistResult = objResponse.getReturnValue();

   				//Una variable para la lista de setFuncDistPrinc
   				let lstFunctionsDistPric = objWrpMapDatosDistResult.setFuncDistPrinc; 
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let functName in lstFunctionsDistPric){
   					opTFuncDistPrinc.push({ value: lstFunctionsDistPric[functName], label: lstFunctionsDistPric[functName]});
   				}//Fin si
   				//Inicializa la lista de listopTFuncDistPrinc
   				objWrpMapDatosDistResult.setFuncDistPrinc = opTFuncDistPrinc;
   				console.log('EN TAM_AdminVisibInventarioToyota setFuncDistPrinc: ', objWrpMapDatosDistResult.setFuncDistPrinc);   

   				//Una variable para la lista de lstTiposInvenDistPrinc
   				let lstTiposInvenDistPrinc = objWrpMapDatosDistResult.setTiposInvenDistPrinc; 
   				//Recorre la lista de lstTiposInvenDistPrinc del dist 
   				for (let tipInvName in lstTiposInvenDistPrinc){
   					optTipInvPrinc.push({ value: lstTiposInvenDistPrinc[tipInvName], label: lstTiposInvenDistPrinc[tipInvName]});
   				}//Fin si
   				//Inicializa la lista de listOptions
   				objWrpMapDatosDistResult.setTiposInvenDistPrinc = optTipInvPrinc;
   				console.log('EN TAM_AdminVisibInventarioToyota setTiposInvenDistPrinc: ', objWrpMapDatosDistResult.setTiposInvenDistPrinc);
            	
            	Component.set("v.objWrpMapDatosDist", objWrpMapDatosDistResult);
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosDistribInvent objWrpMapDatosDist: ", Component.get("v.objWrpMapDatosDist"));
            	
            	//Ve si tiene una colaboración por default y no puede modificarla
            	if (objWrpMapDatosDistResult.blnDefaultColaboraGrupo){
            		let strMsgAdvert = 'En este momento tu inventario esta gestionado por el Admin del sistema.\n';
            		strMsgAdvert += 'Si requieres gestinar tu inventario ponte en contacto con el Admin del sistema.';
            		this.showToastWarning(Component, Event, strMsgAdvert);            	
            	}//Fin si objWrpMapDatosDistResult.blnDefaultColaboraGrupo
      
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
	}, 

    /** getReglasColaboraInvent */
    getReglasColaboraInvent : function(Component, Event, objClienteResult){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosDistribInvent getReglasColaboraInvent: ", objClienteResult);

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getReglasColabora");
        objAction.setParams({
        	"recordId" : Component.get("v.strIdRegistro"),         
        	"objClientePrm" : JSON.stringify(objClienteResult)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lWrpReglaColaboraResult = objResponse.getReturnValue();
            	Component.set("v.lWrpReglaColabora", lWrpReglaColaboraResult); 
            	Component.set("v.blnHayReglas", true); 
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDatosDistribInvent lWrpReglaColaboraResult: ", lWrpReglaColaboraResult);
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    actFuncPrinc: function(objComponent, objEvent) {
    	console.log('EN actFuncPrinc...');
    	
    	let optFuncSel = [];
    	let strCodDistSelPaso = objComponent.get("v.strCodDistSel");
		let funSelecList = objEvent.getParam("value");
    	console.log('EN actFuncPrinc strCodDistSelPaso: ', strCodDistSelPaso);
    	console.log('EN actFuncPrinc funSelecList: ', funSelecList);
		
        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let objWrpMapDatosDistPaso = objComponent.get("v.objWrpMapDatosDist");
   				
		//Inicializa la lista de listopTFuncDistPrinc
		objWrpMapDatosDistPaso.setSelectFuncDistPrinc = funSelecList;
		console.log('EN actFuncPrinc setSelectFuncDistPrinc: ', objWrpMapDatosDistPaso.setSelectFuncDistPrinc);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    actTipoInvPrinc: function(objComponent, objEvent) {
    	console.log('EN actTipoInvPrinc...');
    	
    	let optInvtSel = [];
    	let strCodDistSelPaso = objComponent.get("v.strCodDistSel");
		let inventSelecList = objEvent.getParam("value");
    	console.log('EN actTipoInvPrinc strCodDistSelPaso: ', strCodDistSelPaso);
    	console.log('EN actTipoInvPrinc inventSelecList: ', inventSelecList);

        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let objWrpMapDatosDistPaso = objComponent.get("v.objWrpMapDatosDist");
		//Inicializa la lista de setSelectTiposInvenDistPrinc
		objWrpMapDatosDistPaso.setSelectTiposInvenDistPrinc = inventSelecList;
		console.log('EN actTipoInvPrinc setSelectTiposInvenDistPrinc: ', objWrpMapDatosDistPaso.setSelectTiposInvenDistPrinc);
    },

    /** hpCreaReglaColaboraClick */
    hpCreaReglaColaboraClick : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick..");

    	let lWrpReglaColaboraPaso = Component.get("v.lWrpReglaColabora");	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.guardaReglaColabora");
        objAction.setParams({
        	"lWrpReglaColaboraPrm" : lWrpReglaColaboraPaso,
        	"objWrpMapDatosDistPrm" : JSON.stringify(Component.get("v.objWrpMapDatosDist"))
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lWrpReglaColaboraResult = objResponse.getReturnValue();
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick lWrpReglaColaboraResult: ", lWrpReglaColaboraResult);
            	
            	let blnErrorRegla = false;
            	let strMsgReglaExiste = '';
            	
            	//Recorre la lista de reglas de colaboración y ve si ya existe la ultima que se envio
   				for (let regla in lWrpReglaColaboraResult){
   					if (lWrpReglaColaboraResult[regla].blnExisteRegla){
   						strMsgReglaExiste += 'La regla con la Función(es): \n' + lWrpReglaColaboraResult[regla].lstSelectFuncDistPrinc + '\n';
   						strMsgReglaExiste += 'Y el Inventario(s): \n' + lWrpReglaColaboraResult[regla].ltsSelectTiposInvenDistPrinc + '\n';
   						strMsgReglaExiste += 'Ya existen dentro de las reglas definidas.';
   						blnErrorRegla = true;
   						break;	
   					}//Fin si lWrpReglaColaboraResult[regla].blnErrorRegla
   					if (lWrpReglaColaboraResult[regla].blnNoExisteFunOInvent){
   						strMsgReglaExiste += 'Debes seleccionar una Funcion y un Inventario para crear la regla.';
   						blnErrorRegla = true;
   						break;	
   					}//Fin si lWrpReglaColaboraResult[regla].blnErrorRegla
   				}//Fin si
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick strMsgReglaExiste: " + strMsgReglaExiste + ' blnErrorRegla: ' + blnErrorRegla);
            	
            	//Ya existe la regla
            	if (blnErrorRegla){
					this.showToastWarning(Component, Event, strMsgReglaExiste);
            		Component.set("v.lWrpReglaColabora", lWrpReglaColaboraPaso);
            	}//Fin si blnErrorRegla
            	
            	//No existe la regla	
            	if (!blnErrorRegla){
					strMsgReglaExiste += 'La nueva regla se agrego a la lista.';
            		this.showToastSuccess(Component, Event, strMsgReglaExiste);            	
            		Component.set("v.lWrpReglaColabora", lWrpReglaColaboraResult);
            		Component.set("v.blnHayReglas", true);
            	}//Fin si blnErrorRegla
            	          		            	
            	//Unas variables para las funciones y los tipos de inventarios
            	let opTFuncDistPrincLimpia = [];
            	let optTipInvPrincLimpia = [];
            	//Toma el resultado del llamado de la función	
            	let objWrpMapDatosDistResult = Component.get("v.objWrpMapDatosDist");
   				//Limpia la lista de setFuncDistPrinc
   				objWrpMapDatosDistResult.setSelectFuncDistPrinc = opTFuncDistPrincLimpia;				
   				//Limpia la lista de setTiposInvenDistPrinc
   				objWrpMapDatosDistResult.setSelectTiposInvenDistPrinc = optTipInvPrincLimpia;   				
            	Component.set("v.objWrpMapDatosDist", objWrpMapDatosDistResult);
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick lWrpReglaColabora: ", Component.get("v.lWrpReglaColabora"));
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick objWrpMapDatosDist: ", Component.get("v.objWrpMapDatosDist"));

 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
	}, 

    /** hpEliminaRegla */
    hpEliminaRegla : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpEliminaRegla..");
    	let strLabel = Event.getSource().get("v.label");
    	let strNoRegla = Event.getSource().get("v.name");
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpEliminaRegla strLabel: " + strLabel + ' strNoRegla: ' + strNoRegla);
    	
    	let lWrpReglaColaboraPaso = Component.get("v.lWrpReglaColabora");
       	let lWrpReglaColaboraFinal = [];

    	//Dale la vuelta a la lista de las reglas y quita la que no se quiere
   		for (let regla in lWrpReglaColaboraPaso){
   			if (lWrpReglaColaboraPaso[regla].intNoRegla !== strNoRegla){
   				lWrpReglaColaboraFinal.push(lWrpReglaColaboraPaso[regla]);
   			}//Fin si lWrpReglaColaboraResult[regla].blnErrorRegla
   		}//Fin del for para lWrpReglaColaboraPaso
   		//Ya tienes la lista final de reglas actualiza la lista original
   		Component.set("v.lWrpReglaColabora", lWrpReglaColaboraFinal);
       	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaReglaColaboraClick lWrpReglaColabora: ", Component.get("v.lWrpReglaColabora"));
    		
    },

    /** hpRecalculaColaboracion */
    hpRecalculaColaboracion : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpRecalculaColaboracion..");
    	
    	let lWrpReglaColaboraPaso = Component.get("v.lWrpReglaColabora");	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.guardaRecalculaReglasColabora");
        objAction.setParams({
        	"lWrpReglaColaboraPrm" : lWrpReglaColaboraPaso,
        	"objWrpMapDatosDistPrm" : JSON.stringify(Component.get("v.objWrpMapDatosDist"))
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResActResult = objResponse.getReturnValue();
            	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpRecalculaColaboracion objWrpResActResult: ", objWrpResActResult);
            	if (objWrpResActResult.blnEstatus == false)
            		this.showToastSuccess(Component, Event, objWrpResActResult.strDetalle);            	
            	if (objWrpResActResult.blnEstatus == true)
            		this.showToastError(Component, Event, objWrpResActResult.strDetalle);
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);    		
    },

    cancelar : function(Component, Event) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },
    
    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2500,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Toast Error */
   showToastWarning: function(Component, Event, strMensaje) {
    let toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Advertencia",
        "message": strMensaje, 
        "duration": 7000,
        "type": "warning"
    });
    toastEvent.fire();
   },

})