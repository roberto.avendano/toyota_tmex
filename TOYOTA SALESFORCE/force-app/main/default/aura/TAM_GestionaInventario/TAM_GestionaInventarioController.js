({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.doInit..");
		Component.set('v.recordId', Component.get("v.recordId"));
    	//Busca la lusra de clientes
    	Helper.getDatosInventarioDistribuidor(Component, Event);
    	Component.set("v.blnShowDist", true);
    },

   /** Funcion ocFuncPrinc */
	ocFuncPrinc : function(Component, Event, Helper) {
		console.log("EN ocFuncPrinc...");
    	Helper.actFuncPrinc(Component, Event);
    },

   /** Funcion ocTipoInvPrinc */
	ocTipoInvPrinc : function(Component, Event, Helper) {
		console.log("EN ocTipoInvPrinc...");
    	Helper.actTipoInvPrinc(Component, Event);
    },

   /** Funcion jsCreaReglaColaboraClick */
	jsCreaReglaColaboraClick : function(Component, Event, Helper) {
		console.log("EN jsCreaReglaColaboraClick...");
    	Helper.hpCreaReglaColaboraClick(Component, Event);
    },

   /** Funcion jsEliminaRegla */
	jsEliminaRegla : function(Component, Event, Helper) {
		console.log("EN jsEliminaRegla...");
    	Helper.hpEliminaRegla(Component, Event);
    },

   /** Funcion btnRecalculaColaboracion */
	btnRecalculaColaboracion : function(Component, Event, Helper) {
		console.log("EN btnRecalculaColaboracion...");
    	Helper.hpRecalculaColaboracion(Component, Event);
    },

   /** Funcion cancelar */
	cancelar : function(Component, Event, Helper) {
     	Helper.cancelar(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})