({
	/** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
	},

    /* Buscador de VIN*/
    SearchVIN : function(objComponent, objEvent, objHelper){
        var searchField = objComponent.find('searchField');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            objHelper.SearchHelper(objComponent, objEvent);
        }
    },

    /* Buscador de Dealer*/
    SearchDealer : function(objComponent, objEvent, objHelper){
        var searchField = objComponent.find('searchField_Dealer');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            objHelper.SearchHelper_Dealer(objComponent, objEvent);
        }
    },
    
    /*Guardado de Provisión*/
    Save : function(objComponent, objEvent, objHelper){
        objHelper.SaveList(objComponent, objEvent);
    },

    /*Cerrar Provisión */
    cerrar : function(objComponent, objEvent, objHelper){
        objHelper.cerrarProvision(objComponent, objEvent);
    }

    
})