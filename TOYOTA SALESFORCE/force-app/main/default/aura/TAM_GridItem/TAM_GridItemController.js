({
	/** Añadir fila en la tabla */
    addRow : function (objComponent, objEvent, objHelper) {
        objHelper.addRow(objComponent, objEvent);
    },

    /** Remover fila de la tabla */
    deleteRow : function (objComponent, objEvent, objHelper) {
        objHelper.deleteRow(objComponent, objEvent);
    },

    /** Guardar */
    saveSeccion : function(objComponent, objEvent, objHelper){
        objHelper.saveSeccion(objComponent, objEvent);
    },
    
    
})