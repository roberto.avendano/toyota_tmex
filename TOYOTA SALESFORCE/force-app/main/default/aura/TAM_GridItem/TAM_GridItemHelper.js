({
	/** Añadir fila en tabla */
    addRow : function(objComponent, objEvent){
        var lstWrapper = objComponent.get("v.lstDetalle");
        var intConsecutivo = objComponent.get("v.numConsecutivo");
        var intConsecutivoFinal = intConsecutivo + 1;
        var buttonId = objEvent.target.id;
        var objWrapperAux = lstWrapper[buttonId];
        var objWrapperClone = Object.assign({}, objWrapperAux);
        var indexSiguiente = lstWrapper.indexOf(objWrapperAux) + 1;
        
        objWrapperClone.TAM_Duplicar__c = true;
        objWrapperClone.TAM_Consecutivo__c = intConsecutivoFinal;
        objWrapperClone.TAM_IdExterno__c = null;
        
        objComponent.set("v.numConsecutivo", intConsecutivoFinal);
        lstWrapper.splice(indexSiguiente, 0, objWrapperClone);
        objComponent.set("v.lstDetalle", lstWrapper);
    },
    
    /** Remover fila en tabla */
    deleteRow : function(objComponent, objEvent){
        var buttonId = objEvent.target.id;
        var lstWrapper = objComponent.get("v.lstDetalle");
        var objWrapperAux = lstWrapper[buttonId];
        var index = lstWrapper.indexOf(objWrapperAux);
        var objDetalle = lstWrapper[index];
        if (index > -1) {
            lstWrapper.splice(index, 1);
        }
        //Borrar
        if(objDetalle.TAM_IdExterno__c != null){
            var objAction = objComponent.get("c.borrarRegistro");
            objAction.setParams({objRegistroBorrar : objDetalle});
            objAction.setCallback(this, function(objResponse){
                var objState = objResponse.getState();
                if (objState === "SUCCESS") {
                    var booleanResult = objResponse.getReturnValue();
                    if(booleanResult){
                        objComponent.set("v.lstDetalle", lstWrapper);
                    } else {
                        this.showToastError(objComponent, objEvent); 
                    }  
                } 
            });
            $A.enqueueAction(objAction);
        } else {
            objComponent.set("v.lstDetalle", lstWrapper);
        }
    },

    /** Guardado de sección */
    saveSeccion : function(objComponent, objEvent){
        var lstWrapper = objComponent.get("v.lstDetalle");

        //validar información
        lstWrapper.forEach(function(objWrapper, index){
            
            //Pesos
            if(objWrapper.TAM_IncPropuesto_Cash__c != null){
                var incentivoPesos =  objWrapper.TAM_IncentivoTMEX_Cash__c + objWrapper.TAM_IncDealer_Cash__c;
                if(objWrapper.TAM_IncPropuesto_Cash__c != incentivoPesos){
                    alert('Favor de verificar la distribución de incentivo para el vehículo:\n' + objWrapper.TAM_Serie__c + ' ' + objWrapper.TAM_Clave__c + ' ' + objWrapper.TAM_Version__c + ' ' + objWrapper.TAM_AnioModelo__c);
                }
            }

            //Porcentaje
            if(objWrapper.TAM_IncPropuesto_Porcentaje__c != null){
                var incentivoPorcentaje =  objWrapper.TAM_IncentivoTMEX_Porcentaje__c + objWrapper.TAM_IncDealer_Porcentaje__c;
                if(objWrapper.TAM_IncPropuesto_Porcentaje__c != incentivoPorcentaje){
                    
                    alert('Favor de verificar la distribución de incentivo para el vehículo:\n' + objWrapper.TAM_Serie__c + ' ' + objWrapper.TAM_Clave__c + ' ' + objWrapper.TAM_Version__c + ' ' + objWrapper.TAM_AnioModelo__c);
                    
                }
            }

        });


        var objAction = objComponent.get("c.guardarPolitica");
        objAction.setParams(
        		{   			
                    lstDetallePoliticaAUX : lstWrapper,
                    recordId : objComponent.get("v.recordId")
        		}
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var booleanResult = objResponse.getReturnValue();
                if(booleanResult){
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);


    },

    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
    
})