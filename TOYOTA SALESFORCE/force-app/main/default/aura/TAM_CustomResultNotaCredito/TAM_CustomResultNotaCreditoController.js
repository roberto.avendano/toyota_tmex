({
    selectAccount : function(component, event, helper){      
        var getSelectAccount = component.get("v.objNotaCredito");  
        var compEvent = component.getEvent("oSelectedAccountEvent");
        compEvent.setParams({"accountByEvent" : getSelectAccount });  
        compEvent.fire();
    },
})