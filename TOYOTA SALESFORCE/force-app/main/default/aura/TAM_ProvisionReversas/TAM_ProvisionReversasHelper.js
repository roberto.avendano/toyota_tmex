({
	/** Función Inicial */
    initializeComponent : function(objComponent, objEvent){
        objComponent.set('v.loaded', true);
        var objAction = objComponent.get("c.getReversa");
        objAction.setParams({ recordId : objComponent.get("v.recordId") });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0){
                    lstResult.sort();
                    objComponent.set('v.lstReversa', lstResult);
                    objComponent.set('v.lstReversa_AUX', lstResult);
                    
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', true);
                } else {
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', false);
                } 
            }
        });
        $A.enqueueAction(objAction);
    },

    /* Buscador de VIN */
    SearchHelper: function(objComponent, objEvent) {
        var SearchKey = objComponent.get("v.SearchKey");
        var lstRetailConReversa = objComponent.get("v.lstReversa");
        var PagList = [];
        lstRetailConReversa.forEach(function(objConReversa) {
            var strVINRecord = objConReversa.TAM_VIN__r.Name;
            if(strVINRecord.includes(SearchKey)){
                PagList.push(objConReversa);
            }
        });
        objComponent.set('v.lstReversa_AUX', PagList);
    },
})