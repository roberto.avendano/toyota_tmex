({
    sectionActive: function(objComponent, objEvent, objHelper) {
        var sectionActive = objComponent.get("v.activeSection");
        
        if(sectionActive.includes('Analisis')){
            objComponent.set("v.boolFiltrado_Analisis",true);
        }
        
        if(sectionActive.includes('Pendientes')){
        	objComponent.set("v.boolFiltrado_Pendientes",true);
        }
        
        if(sectionActive.includes('Variacion')){
        	objComponent.set("v.boolFiltrado_Variacion",true);
        }
       
    }
})