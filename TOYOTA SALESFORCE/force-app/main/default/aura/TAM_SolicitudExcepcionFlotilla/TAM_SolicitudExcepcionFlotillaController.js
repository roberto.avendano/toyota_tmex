({
    doInit : function(component, event, helper) {
        
        helper.getMotivoSolicitud(component, event, helper);
        helper.getMesesPoliticas(component, event, helper);
        
        let infoVIN = component.get("v.objModeloSelExcepCte.lModelosSeleccionados[0].strNombre");
        let fechaVenta =  component.get("v.objModeloSelExcepCte.strFechaCierre");
        let rangoPrograma = component.get("v.objModeloSelExcepCte.strRangoPrograma");
        let array;
        let serie ;
        let	codigoModelo;
        let anioModelo ;
        
        if(infoVIN != null){
            array = infoVIN.split("-");
            serie = array[0];
            codigoModelo = array[1];
            anioModelo =  array[2];
            
        }
        
        let tipoVenta;
        if(component.get("v.strTipoVenta") == 'Financiamiento TFS'){
            tipoVenta = 'Con TFS';
            
        }else{
            tipoVenta = 'Sin TFS';
            
        }
        
        
        let listaVINES = component.get("v.objModeloSelExcepCte.lVinesSeleccionados");
        let numeroVINES = listaVINES.length;
        let vinesSanitizados = new Array();
        
        for (var i = 0; i < numeroVINES; i++) {
            //var beforePlus = listaVINES[i].split(':').shift();
            var vin = listaVINES[i].strVin.split(':').shift();
            vinesSanitizados.push(vin);
            
            
        }
        
        let codigoDealer = listaVINES[0].strVin.split(':').pop();
        
        component.set("v.serieIn",serie);
        component.set("v.modeloIn",codigoModelo);
        component.set("v.anioModeloIn",anioModelo);
        component.set("v.tipoVentaIn",tipoVenta);
        component.set("v.listaVINES",vinesSanitizados);
        component.set("v.numeroVINES",numeroVINES);
        component.set("v.fechaVenta",fechaVenta);
        component.set("v.codigoDealer",codigoDealer);
        component.set("v.rangoPrograma",rangoPrograma);
        
    },
    
    
    
    onSingleSelectCMotivos : function(component, event, helper) {       
        
        let motivoSeleccionado 						= event.getSource().get("v.value");
        let motivoSeleccionadoValue 				= event.getSource().get("v.name");
        let validateMotivo;
        let tipoExcepcion = component.get("v.tipoExcepcion");
        
        
        if(motivoSeleccionado == 'Elegir un Motivo'){
            component.set("v.validaPickListMotivo",false);
            validateMotivo = false;
            
        }
        if(motivoSeleccionado == 'Se desea dar el incentivo retail al cliente.'){
            component.set("v.motivosSeleccionado",motivoSeleccionado);
            component.set("v.excpecionRetailFlotilla",true);
            component.set("v.validaPickListMotivo",true);
            validateMotivo = false;
            
        }
        else{
            
            component.set("v.motivosSeleccionado",motivoSeleccionado);
            component.set("v.excpecionRetailFlotilla",false);
            component.set("v.validaPickListMotivo",true);
            validateMotivo = true;
        }
        
        if(validateMotivo == true){
            helper.obtenListaPrecios(component, event, helper);
            helper.getPoliticaCorrespodienteIncentivos(component, event, helper);
            //helper.getPoliticasHistoricas(component, event, helper);
            helper.getIncentivoFechaCierre(component, event, helper);
            component.find("selectMesList").set("v.value", 'Mes Política Incentivo');
            component.set("v.validaPickListMesIncentivo",false);
            component.set("v.validaPickListPrecio",false);
        }
        
        
    },
    
    selectPrecioVIN : function(component, event, helper) {
        let precioSeleccionado 						= event.getSource().get("v.value");
        let precioSeleccionadoValue 				= event.getSource().get("v.name");
        let validatePrecio;
        
        
        component.set("v.calculoManual",false);
        component.find("incentivosDisponibles").set("v.value", 'Elegir Incentivo');
        component.set("v.validaSeleccionPolitica",'empty');
        
        if(precioSeleccionado == 'Elegir Precio'){
            component.find("incentivosDisponibles").set("v.value", 'Elegir Incentivo');
            component.set("v.validaSeleccionPolitica",'empty');
            component.set("v.validaPickListPrecio",false);
            component.set("v.validarPoliticaSeleccionada",null);
            component.set("v.calculoManual",null);
            component.set("v.calculoSistema",null);
            validateMotivo = false;
            
            
        }else{
            component.set("v.precioSeleccionado",precioSeleccionado);
            component.set("v.validaPickListPrecio",true);
            component.set("v.validaPickListMesIncentivo",false);
            component.find("selectMesList").set("v.value", 'Mes Política Incentivo');
            validateMotivo = true;
        }
        
        
        
    },
    
    
    selectMesIncentivo : function(component, event, helper) {
        let mesSeleccionado 						= event.getSource().get("v.value");
        let mesSeleccionadoValue 				= event.getSource().get("v.name");
        let validateMes;
        
        component.set("v.calculoManual",false);
        component.find("incentivosDisponibles").set("v.value", 'Elegir Incentivo');
        component.set("v.validaSeleccionPolitica",'empty');
        component.set("v.mesSeleccionado",mesSeleccionado)
        if(mesSeleccionado == 'Mes Política Incentivo'){
            component.find("incentivosDisponibles").set("v.value", 'Elegir Incentivo');
            component.set("v.validaSeleccionPolitica",'empty');
            component.set("v.validaPickListMesIncentivo",false);
            component.set("v.validarPoliticaSeleccionada",null);
            component.set("v.calculoManual",null);
            component.set("v.calculoSistema",null);
            validateMotivo = false;      
            
        }else{
            helper.getPoliticasHistoricas(component, event, helper);
            component.set("v.mesSeleccionado",mesSeleccionado)
            component.set("v.validaPickListMesIncentivo",true);
            validateMotivo = true;
            
        }
        
    },
    
    onSingleTipoExcepcion : function(component, event, helper) {
        let tipoSeleccionado 						= event.getSource().get("v.value");
        let tipoSeleccionadoValue 					= event.getSource().get("v.name");
        let validateTipo;
        
        
        if(tipoSeleccionado == 'empty'){
            component.set("v.validaPickListTipoExcep",false);
            validateTipo = false;
            
        }else{
            component.set("v.tipoExcepcion",tipoSeleccionado);
            component.set("v.validaPickListTipoExcep",true);
            validateTipo = true;
        }
        
    },
    
    onSingleTipoIncentivo : function(component, event, helper) {
        let incentivoSeleccionado 						= event.getSource().get("v.value");
        let incentivoSeleccionadoValue 					= event.getSource().get("v.name");
        let validarSeleccion;
        
        
        component.set("v.validarPoliticaSeleccionada",'empty');
        
        
        if(incentivoSeleccionado == 'manual'){
            component.set("v.validaSeleccionPolitica",'seleccionPolitica');
            component.set("v.incentivoManual",true);
            component.set("v.calculoSistema",false);
            component.set("v.politicaSeleccinada",null);
            
        }
        if(incentivoSeleccionado != 'manual' && incentivoSeleccionado != 'Elegir Incentivo' ){
            component.set("v.validaSeleccionPolitica",'seleccionPolitica');
            component.set("v.politicaSeleccinada",incentivoSeleccionado);
            component.set("v.incentivoManual",false);
            component.set("v.calculoSistema",true);
            
            validarSeleccion = true;
            
        }
        
        if(incentivoSeleccionado == 'Elegir Incentivo'){
            component.set("v.calculoSistema",null);
            component.set("v.validarPoliticaSeleccionada",null); 
            component.set("v.validaSeleccionPolitica",'empty');
            component.set("v.calculoManual",null);
            
        }
        
        if(validarSeleccion == true){
            helper.calculoIncentivoSeleciconado(component,event,helper);
            helper.calculoTotal(component,event,helper);
            
        }
        
    },
    
    
    incentivoManual : function(component, event, helper) {
        let incentivoTMEXManual = component.get("v.incentivoTMEXManual");
        let incentivoDealerManual = component.get("v.incentivoDealerManual");
        let validarCampos = false;
        
        if(incentivoTMEXManual == undefined || incentivoDealerManual == undefined){
            component.find('incentivoDealer').showHelpMessageIfInvalid();
            component.find('incentivoTMEX').showHelpMessageIfInvalid();
            validarCampos = false;
            
        }
        
        
        if(incentivoTMEXManual != undefined && incentivoTMEXManual != '' && incentivoDealerManual != '' && incentivoDealerManual != undefined){
            validarCampos = true;
            
        }
        
        
        if(validarCampos == true){
            helper.calculoIncentivoManual(component,event,helper);
            
            
            
        }
        
        
        
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    
    cancelar : function(component, event, helper) {
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.fire();
        
    },
    
    
    clickSave  : function(component, event, helper) {
        helper.guardaSolicitudFlotilla(component, event, helper);
        
        
    },

    subirDocumento : function(component, event, helper) {
        var checkCmp = component.find("checkbox");
         resultCmp = component.find("checkResult");
         resultCmp.set("v.value", ""+checkCmp.get("v.value"));

    },

    handleUploadFinished : function(component, event, helper) {
        var idRecord = component.get("v.recordId");
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var id       = uploadedFiles[0].Id;
        var toastEvent = $A.get("e.force:showToast");
        
        component.set("v.documentId",documentId);
        component.set("v.nameFile",fileName);
        
        /*   
        var action = component.get("c.saveDocumentId");
        action.setParams({ 
            idRecord : idRecord,
            idDocument : uploadedFiles[0].documentId,
            nombreDocumento : fileName
            
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File "+documentId+" Uploaded successfully."
                });
                toastEvent.fire();
                component.set("v.nameFile",fileName);
                component.set("v.documentId",documentId);
                component.set("v.archivoCargado",true);
                component.set("v.SolicitudIncentivos.TAMDocumento_Id__c",documentId)
                component.set("v.solicitudEnviada",false);
                console.log("id de documento"+documentId);
                console.log("nombre del documento"+fileName);
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);*/
        
        
    },

    previewDocumento : function(component, event, helper) {
        var idDocument;
        
        if(component.get("v.documentId") != null){
            idDocument  = component.get("v.documentId");
        }
        
        $A.get('e.lightning:openFiles').fire({
            recordIds: [idDocument]
        });
        
    },

    eliminarDocumento : function(component, event, helper) {
        var idDocument;
        var idRecord = component.get("v.recordId");        

        if(component.get("v.documentId") != null){ 
            idDocument  = component.get("v.documentId");
        }
        
        var action = component.get("c.deleteDocument");
        action.setParams({ 
            documentId : idDocument
        
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nameFile",null);
                component.set("v.documentId",null);
                //component.set("v.SolicitudIncentivos.TAM_NombreFacturaRetail__c ",null);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Documento Eliminado."
                });
                toastEvent.fire();
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    
    }
    
})