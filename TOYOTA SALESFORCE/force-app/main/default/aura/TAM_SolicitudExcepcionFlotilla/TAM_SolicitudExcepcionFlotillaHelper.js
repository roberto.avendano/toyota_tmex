({
    
    getIncentivoFechaCierre : function(component, event) {
        let modelo = component.get("v.modeloIn");
        let anioModelo = component.get("v.anioModeloIn");
        let rangoPrograma = component.get("v.rangoPrograma");
        let tipoVenta = component.get("v.TipoVenta");
        let tipoExcepcion = component.get("v.tipoExcepcion");
        let fechaVenta = component.get("v.fechaVenta"); 
        
        let action = component.get("c.getPoliticaFechaCierre");
        
        action.setParams({
            modelo : modelo,
            anioModelo : anioModelo,
            rangoPrograma : rangoPrograma,
            tipoVenta     : tipoVenta,
            fechaVenta	  : fechaVenta
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var politicaFechaCierre = response.getReturnValue();
                component.set("v.politicaCorrespondiente",politicaFechaCierre);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    getMesesPoliticas : function(component, event) {
        let action = component.get("c.getMesesPoliticaIncentivos");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.mesesPoliticasIncentivos",respuesta);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);   
        
    },
    
    getMotivoSolicitud : function(component, event) {
        let action = component.get("c.obtieneMotivosSolicitud");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.motivosSolicitud",respuesta);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
    getPoliticasHistoricas : function(component, event) {
        let action = component.get("c.getPoliticasHistoricas");
        let modelo = component.get("v.modeloIn");
        let anioModelo = component.get("v.anioModeloIn");
        let rangoPrograma = component.get("v.rangoPrograma");
        let tipoVenta = component.get("v.TipoVenta");
        let tipoExcepcion = component.get("v.tipoExcepcion");
        let fechaVenta = component.get("v.fechaVenta");
        let mesSeleccionado = component.get("v.mesSeleccionado");
        
        action.setParams({
            modelo : modelo,
            anioModelo : anioModelo,
            rangoPrograma : rangoPrograma,
            tipoVenta     : tipoVenta,
            fechaVenta	  : fechaVenta,
            mesSeleccionado : mesSeleccionado
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.politicasHistoricas",respuesta);
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.existenPoliticasHistoricas","ConPoliticas");
                }else{
                    component.set("v.existenPoliticasHistoricas","SinPoliticas");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action); 
        
        
    },    
    
    
    getPoliticaCorrespodientePrecio : function(component, event) {
        
        let action = component.get("c.getPoliticasVigentesPrecio");
        let modelo = component.get("v.modeloIn");
        let anioModelo = component.get("v.anioModeloIn");
        let rangoPrograma = component.get("v.rangoPrograma");
        let tipoVenta = component.get("v.TipoVenta");
        let tipoExcepcion = component.get("v.tipoExcepcion");
        
        
        action.setParams({
            modelo : modelo,
            anioModelo : anioModelo,
            rangoPrograma : rangoPrograma,
            tipoVenta     : tipoVenta
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.politicaVigente",respuesta);
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.existePoliticaVigente","conPolitica");
                }else{
                    component.set("v.existePoliticaVigente","sinPolitica");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        //$A.enqueueAction(action);        
        
        
    },
    
    getPoliticaCorrespodienteIncentivos : function(component, event) {
        
        let action = component.get("c.getPoliticasVigentesPorcentaje");
        let modelo = component.get("v.modeloIn");
        let anioModelo = component.get("v.anioModeloIn");
        let rangoPrograma = component.get("v.rangoPrograma");
        let tipoVenta = component.get("v.TipoVenta");
        let tipoExcepcion = component.get("v.tipoExcepcion");
        
        
        action.setParams({
            modelo : modelo,
            anioModelo : anioModelo,
            rangoPrograma : rangoPrograma,
            tipoVenta     : tipoVenta
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.politicaVigente",respuesta);
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.existePoliticaVigente","conPolitica");
                }else{
                    component.set("v.existePoliticaVigente","sinPolitica");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);        
        
        
        
    },
    
    calculoIncentivoSeleciconado : function(component, event) {
        let politicaSeleccionada = component.get("v.politicaSeleccinada");
        let action = component.get("c.calculoPoliticaSeleccionada");
        
        action.setParams({
            politicaSeleccionada : politicaSeleccionada
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.politicaSeleccionadaResumen",respuesta);
                    component.set("v.validarPoliticaSeleccionada",'politicaSeleccionada');
                    
                }else{
                    component.set("v.validarPoliticaSeleccionada","empty");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);        
        
        
        
    },
    
    
    calculoTotal : function(component, event) {
        let politicaSeleccionada = component.get("v.politicaSeleccinada");
        let action = component.get("c.calculoTotal");
        let precioSeleccionado = component.get("v.precioSeleccionado");
        
        
        action.setParams({
            politicaSeleccionada : politicaSeleccionada,
            precioSeleccionado	 : precioSeleccionado
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.calculoTotal",respuesta);
                    //component.set("v.validarPoliticaSeleccionada",'politicaSeleccionada');
                    
                }else{
                    //component.set("v.validarPoliticaSeleccionada","empty");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);        
        
        
        
    },
    
    calculoIncentivoManual : function(component, event, helper) {
        let precioElegido = component.get("v.precioSeleccionado");
        let incentivoDealerManual = component.get("v.incentivoDealerManual");
        let incentivoTMEXManual  = component.get("v.incentivoTMEXManual");
        
        let action = component.get("c.getCalculoManual");
        
        action.setParams({
            precioElegido : precioElegido,
            incentivoDealerManual : incentivoDealerManual,
            incentivoTMEXManual : incentivoTMEXManual
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "{}"){
                    component.set("v.calculoTotal",respuesta);
                    component.set("v.calculoManual",true);
                }else{
                    component.set("v.calculoManual",false);
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    obtenListaPrecios : function(component, event, helper) {
        let modelNumber = component.get("v.modeloIn");
        let modelYear   = component.get("v.anioModeloIn");	           
        
        let action = component.get("c.getPreciosVIN");
        action.setParams({
            modelNumber : modelNumber,
            modelYear : modelYear
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "{}"){
                    
                    component.set("v.preciosVigenteLista", response.getReturnValue());
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    
    guardaSolicitudFlotilla : function(component, event, helper) {
        
        let wrapSolicitud = component.get("v.objModeloSelExcepCte");
        let motivoSolicitud = component.get("v.motivosSeleccionado"); 
        let comentario = component.get("v.comentario");
        let precioSolicitado = component.get("v.precioSeleccionado");
        let incentivoManual = component.get("v.incentivoManual");
        let politicaSeleccionada = component.get("v.politicaSeleccinada");
        let incentivoTotalManual = component.get("v.incentivoTotalManual");
        let incentivoDealerManual = component.get("v.incentivoDealerManual");
        let incentivoTMEXManual = component.get("v.incentivoTMEXManual");
        let incentivoFechaCierre = component.get("v.politicaCorrespondiente");
        let codigoDelaer =  component.get("v.codigoDealer");
        let listaVINESIn = component.get("v.listaVINES");
        let fechaVenta  = component.get("v.fechaVenta");
        let datosValidos = false;
        let documentId =  component.get("v.documentId");

        let action = component.get("c.guardaSolicitudExcepcion");
        
        let autorizacionDTM = false;
        
        if(motivoSolicitud == 'El cliente cuenta con apartado u orden de compra del mes anterior.' || motivoSolicitud == 'El cliente adquiere la unidad en el mes anterior y solicita condiciones actuales.'){
            autorizacionDTM = true;
            
        }else{
            autorizacionDTM = false;
        }
        
        //Validación de Comentario 
        if(comentario == undefined || comentario == '' || comentario.includes('<p><br></p><p><br></p><p><br></p><p><br></p>')){
            component.set("v.comentarioDealer",true);
            datosValidos == false;            
        }else{
            datosValidos = true;
        }

        //lógica para las aprobaciónes en automatico
        let incentivoCorrespondienteTMEX = component.get("v.incentivoFechaCierre");
        let incentivoSeleccionadoDealer = component.get("v.politicaSeleccionada");
        
        action.setParams({
            wrapSolicitud           : wrapSolicitud,
            motivoSolicitud         : motivoSolicitud,
            comentario              : comentario,
            precioSolicitado        : precioSolicitado,
            incentivoManual	        : incentivoManual,
            politicaSeleccionada    : politicaSeleccionada,
            incentivoTotalManual    : incentivoTotalManual,
            incentivoDealerManual   : incentivoDealerManual,
            incentivoTMEXManual     : incentivoTMEXManual,
            incentivoFechaCierre    : incentivoFechaCierre,
            codigoDelaer            : codigoDelaer,
            listaVINESIn		    : listaVINESIn.toString(),
            fechaVenta              : fechaVenta,
            autorizacionDTM		    : autorizacionDTM,
            documentId              : documentId
            
        }); 
    if(datosValidos === true){
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                
                Component.set("v.blnShowVinesDOD", false);
                Component.set("v.blnCreaExcepcionFlotillaPrograma", false);
                Component.set("v.blnShowBtnCreaExcep", true);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
        
            $A.enqueueAction(action);
        }      
        
    }  
    
})