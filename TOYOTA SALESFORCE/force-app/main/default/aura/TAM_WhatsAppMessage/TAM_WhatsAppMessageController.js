({
    
    doInit : function(component, event, helper) {
        helper.typeDevice(component, event, helper);
        helper.getInformationLead(component, event, helper);
    },
    
    saveAction : function(component, event, helper) {
        helper.saveActionWhatsApp(component, event, helper);
        
    },
    refreshPage: function(component, event, helper) {
        //$A.get('e.force:refreshView').fire();
        location.reload();
    },
})