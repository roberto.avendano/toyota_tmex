({
    typeDevice : function(component, event, helper) {
        var deviceMobile = $A.get("$Browser.isPhone");
        if(deviceMobile === true){
            component.set("v.isMobile",true);
            
        }
        
    },
    
    getInformationLead  : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getInformationByLead");
        
        action.setParams({ 
            recordId : recordId
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.lead',result);
                
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    saveActionWhatsApp  : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.saveLogRecord");
        
        action.setParams({ 
            recordId : recordId
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('OK');
            }
        });
        
        $A.enqueueAction(action);
        
        
    }
    
})