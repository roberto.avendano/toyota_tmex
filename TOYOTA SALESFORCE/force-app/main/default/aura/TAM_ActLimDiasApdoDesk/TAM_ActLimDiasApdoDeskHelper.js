({

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
    buscaNoDistUserActual : function(objComponent, objEvent) {
        console.log("EN buscaNoDistUserActual...");
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 
        //Llama la función 
        let action = objComponent.get("c.getNoDistlUserActual");
        action.setParams({
            userId 		: userId
        });
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            
                let getNoDistlUserActualResult = objResponse.getReturnValue();
                objComponent.set("v.strNoDist", getNoDistlUserActualResult);                
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(action);
    },

    consultaMaxNoDiasAdo :  function(component, event, helper) {
        let userId = $A.get("$SObjectType.CurrentUser.Id");
        
        let action = component.get("c.consultaMaxNoDiasAdo");
        action.setParams({
            userId 		: userId
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                let parametroResponse = response.getReturnValue();
                component.set("v.numeroMaximoNoDias",parametroResponse);
                component.set("v.numeroMaximoNoDiasField",parametroResponse);
            } 
        });
        $A.enqueueAction(action);        
    },

    guardarMaxNoDiasAdo : function(component, event, helper) {
        let numeroMaximoNoDiasField = component.get("v.numeroMaximoNoDiasField");
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 

        let action = component.get("c.seteaValorMaximoLeads");        
        action.setParams({
            userId 		: userId,
            numeroMaximoNoDiasField  : numeroMaximoNoDiasField
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.numeroMaximoNoDiasField",numeroMaximoNoDiasField);
                let strMensaje = 'Registro actualizado !'
                this.showToastSuccess(component, event, strMensaje); 
                //helper.buscaNoDistUserActual(component,event);
            } 
        });
        $A.enqueueAction(action);
    },    
    
    cancelar : function(Component, Event, strMensaje) {
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
        
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },
    
    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1500,
            "type": "error"
        });
        toastEvent.fire();
    },

})