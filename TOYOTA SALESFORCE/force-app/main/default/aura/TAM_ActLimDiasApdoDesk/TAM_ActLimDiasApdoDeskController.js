({

    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.buscaNoDistUserActual(objComponent, objEvent);
        objHelper.consultaMaxNoDiasAdo(objComponent, objEvent, objHelper);
    },

    guardarML : function(component,event,helper){    
     	helper.guardarMaxNoDiasAdo(component,event,helper);
    },
    
     cancelar : function(component,event,helper){    
     	helper.cancelar(component,event,helper);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

})