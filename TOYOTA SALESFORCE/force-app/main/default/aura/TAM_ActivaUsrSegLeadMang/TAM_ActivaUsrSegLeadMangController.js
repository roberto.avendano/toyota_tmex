({
    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.hpBuscaNoDistUserActual(objComponent, objEvent);
        objHelper.hpConsultaMaximoLeads(objComponent, objEvent, objHelper);
    },

   /** Funcion Inicial */
	onActivaUsrSegLead : function(objComponent, objEvent, objHelper) {
		objHelper.hpActivaUsrSegLead(objComponent, objEvent);
    },


   /** Funcion Inicial */
	btnActivaUserSegLeads : function(objComponent, objEvent, objHelper) {
		objHelper.hpActivaUserSegLeads(objComponent, objEvent);
    },

	//La función ocSelecDealer
    ocSelecDealer : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecDealer(objComponent, objEvent);
    },

	//La función onDespMsgAdv
    onDespMsgAdv : function(objComponent, objEvent, objHelper) {
        objHelper.hpDespMsgAdv(objComponent, objEvent);
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {
        // Close the action panel
        let dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    guardarML : function(component,event,helper){    
     	helper.guardarMaximoLeads(component,event,helper);
    },
    
     cancelar : function(component,event,helper){    
     	helper.cancelar(component,event,helper);
    }

})