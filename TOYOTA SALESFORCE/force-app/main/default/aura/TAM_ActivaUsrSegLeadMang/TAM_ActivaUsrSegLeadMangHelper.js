({
    
    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
    hpBuscaNoDistUserActual : function(objComponent, objEvent) {
        console.log("EN hpBuscaNoDistUserActual...");
        //Llama la función 
        let objAction = objComponent.get("c.getNoDistlUserActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            
                let objWrpResActResult = objResponse.getReturnValue();
                console.log("EN hpBuscaNoDistUserActual objWrpResActResult: ", objWrpResActResult);
                objComponent.set("v.blnNoUserAdmin", objWrpResActResult.blnEstatus);
                if (objWrpResActResult.blnEstatus == false){
                    objComponent.set("v.strNoDist", objWrpResActResult.strParamOpcional2);
                    this.getUsuarosDealer(objComponent, objEvent);
                }
                if (objWrpResActResult.blnEstatus == true){
                    objComponent.set("v.sDetalleErrorConsCte", objWrpResActResult.strDetalle);
                    this.showToastError(objComponent, objEvent, objWrpResActResult.strDetalle);
                }
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },
    
    
    /** */
    getUsuarosDealer : function(objComponent, objEvent){
        console.log("EN SolCompEspDist Helper.getUsuarosDealer...");
        //Obtener lista de distribuidores disponibles.
        var objAction = objComponent.get("c.getUsuarosDealer");
        objAction.setParams({   			
            noDistAct : objComponent.get("v.strNoDist") 
        });        
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var lUsuariosActivosLeadsRes = objResponse.getReturnValue();
                objComponent.set("v.lUsuariosActivosLeads", lUsuariosActivosLeadsRes);
                //Recorre la lista de lUsuariosActivosLeadsRes y ve puede despliegar el cod de Deal
                for (let idUsr in lUsuariosActivosLeadsRes){
                	//Ve si esta prendido blnMuestCodDealer
                	if (lUsuariosActivosLeadsRes[idUsr].blnMuestCodDealer){
                		objComponent.set("v.blnMuestraCodDealer", true);
                		break;
                	}//Fin si lUsuariosActivosLeadsRes[idUsr].blnMuestCodDealer
                }//Fin del for para lUsuariosActivosLeadsRes
                console.log("EN getUsuarosDealer lUsuariosActivosLeads: ", objComponent.get("v.lUsuariosActivosLeads") );
                console.log("EN getUsuarosDealer blnMuestraCodDealer: ", objComponent.get("v.blnMuestraCodDealer") );
            }
        });
        $A.enqueueAction(objAction);
    }, 
    
    //Funcion que sirve para seleccionar el tipo de Excepción
    hpActivaUsrSegLead: function(objComponent, objEvent) {
        console.log('EN activaUsrSegLead...');
        console.log('EN activaUsrSegLead lUsuariosActivosLeads: ', objComponent.get("v.lUsuariosActivosLeads"));
    },
    
    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
    hpActivaUserSegLeads : function(objComponent, objEvent) {
        console.log("EN activaUserSegLeads...");
        //Llama la función 
        let objAction = objComponent.get("c.clsAcctUsrSegLead");
        objAction.setParams({
            "strNoDist" : objComponent.get("v.strNoDist"),
            "lUsuariosActivosLeads" : objComponent.get("v.lUsuariosActivosLeads"),
            "sCodDealerSelPrm" : objComponent.get("v.sCodDealerSel"),
            "objWrpDelaerUsruarioPrm" : objComponent.get("v.objWrpDelaerUsruario") 	
        });		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {		       		
                let objWrpResAct = objResponse.getReturnValue();
                if (objWrpResAct.blnEstatus == false){
                    this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
                }
                if (objWrpResAct.blnEstatus == true)
                    this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);  		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },
        
    hpConsultaMaximoLeads :  function(component, event, helper) {
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 
        let action = component.get("c.consultaMaximoLeads");
        action.setParams({
            userId 		: userId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let parametroResponse = response.getReturnValue();
                component.set("v.numeroMaximoLeads",parametroResponse);
                component.set("v.numeroMaximoLeadsField",parametroResponse);
                //Consulta los datos de clsGetListaDealerRedirect
                this.clsGetListaDealerRedirect(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },

    clsGetListaDealerRedirect : function(component, event, helper) {
        console.log('EN Save hpBuscaUsuariosDealer objWrpDelaerUsruarioResult1...');
        let numeroMaximoLead = component.get("v.numeroMaximoLeadsField");
        let userId = $A.get("$SObjectType.CurrentUser.Id");        
        
        let action = component.get("c.clsGetListaDealerRedirect");
        action.setParams({
            "IdUsrActual" : userId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	let objWrpDelaerUsruarioResult = response.getReturnValue();            
                component.set("v.objWrpDelaerUsruario", objWrpDelaerUsruarioResult);
                console.log('EN Save hpBuscaUsuariosDealer objWrpDelaerUsruarioResult2: ', objWrpDelaerUsruarioResult);
	        	let blnMuestraCodDealerPaso = false;
	        	//Dale la vuelta a la lista de lUsuariosRedirect y crea el msg en strMsgAdvert
	        	for (let nomDealerPaso in objWrpDelaerUsruarioResult.lUsuariosRedirect){
        			console.log('EN Save hpBuscaUsuariosDealer blnActivo: ', objWrpDelaerUsruarioResult.lUsuariosRedirect[nomDealerPaso].blnActivo);
	        		if (objWrpDelaerUsruarioResult.lUsuariosRedirect[nomDealerPaso].blnActivo){
	        			blnMuestraCodDealerPaso = true;
	        			//Salte	
	        			break;
	        	 	}//Fin si objWrpDelaerUsruarioPaso.lUsuariosRedirect[nomDealerPaso].blnActivo
	        	}//Fin del for para lDealersPaso
      			component.set("v.blnMuestraCodDealer", blnMuestraCodDealerPaso);
	        	console.log('EN Save hpBuscaUsuariosDealer blnMuestraCodDealer: ', component.get("v.blnMuestraCodDealer"));
            }//Fin si state === "SUCCESS"
        });
        $A.enqueueAction(action);  
    },    
    
    guardarMaximoLeads : function(component, event, helper) {
        let numeroMaximoLead = component.get("v.numeroMaximoLeadsField");
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 
        
        let action = component.get("c.seteaValorMaximoLeads");        
        action.setParams({
            userId 		: userId,
            numeroMaximoLead  : numeroMaximoLead
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.numeroMaximoLeads",numeroMaximoLead);
                var strMensaje = 'Registro actualizado !'
                this.showToastSuccess(component, event, strMensaje); 
                helper.buscaNoDistUserActual(component,event);
            }
        });
        $A.enqueueAction(action);
    },    

    /** Funcion hpSelecDealer */
    hpSelecDealer: function(objComponent, objEvent) {
    	console.log('EN hpSelecDealer...');
        let value = objEvent.getSource().get("v.value");
        let lWrpDealersUsuariosPaso = [];
        
        //Ve si el codigo del dealer seleccionado es diferente de 57999
        if (value !== '57999')
        	objComponent.set("v.sCodDealerSel", value);        
        if (value === '57999'){
        	objComponent.set("v.sCodDealerSel", '57999');        
        }//Fin si value === '57999'
    	console.log('EN hpSelecDealer sCodDealerSel: ' + objComponent.get("v.sCodDealerSel"));
    },

    /** Funcion hpDespMsgAdv */
    hpDespMsgAdv: function(objComponent, objEvent) {
    	console.log('EN hpSelecDealer...');
        let objWrpDelaerUsruarioPaso = objComponent.get("v.objWrpDelaerUsruario");        
        let lUsuariosActivosLeadsPaso = objComponent.get("v.lUsuariosActivosLeads");        
    	console.log('EN hpSelecDealer objWrpDelaerUsruarioPaso: ', objWrpDelaerUsruarioPaso);

        let blnSelec = objEvent.getSource().get("v.value");        
        let strMsgAdvert = '';
    	console.log('EN hpSelecDealer blnSelec: ' + blnSelec);
        
        //Ve si selecciono este usuario
        if (blnSelec){
        	strMsgAdvert = 'Si activas este usuario todos los leads que llegan a los siguientes Dealers: \n\n';
        	//Dale la vuelta a la lista de lDealersPaso y crea el msg en strMsgAdvert
        	for (let nomDealerPaso in objWrpDelaerUsruarioPaso.lDealers){
        		strMsgAdvert += objWrpDelaerUsruarioPaso.lDealers[nomDealerPaso].nombreDist + '. \n';
        	}//Fin del for para lDealersPaso
        	strMsgAdvert += '\n Serán reasignados a este usuario seleccionado.';
        	//Bloquea los usuarios del lead actual en la lista de lUsuarios
        	for (let nomUsuarioPaso in lUsuariosActivosLeadsPaso){
        		lUsuariosActivosLeadsPaso[nomUsuarioPaso].blnMuestCodDealer = true;
        	}//Fin del for para lDealersPaso
        	//Despliega el msg
        	this.showToastWarning(objComponent, objEvent, strMsgAdvert);
        }//Fin si 

        //Ve si deselecciono este usuario
        if (!blnSelec){
        	let blnActivoPaso = false;
        	//Dale la vuelta a la lista de lUsuariosRedirect y crea el msg en strMsgAdvert
        	for (let nomDealerPaso in objWrpDelaerUsruarioPaso.lUsuariosRedirect){
        		if (objWrpDelaerUsruarioPaso.lUsuariosRedirect[nomDealerPaso].blnActivo){
        			blnActivoPaso = true;
        			break;
        	 	}//Fin si objWrpDelaerUsruarioPaso.lUsuariosRedirect[nomDealerPaso].blnActivo
        	}//Fin del for para lDealersPaso
        	//Activa todos los usuarios de lUsuariosActivosLeadsPaso
        	if (!blnActivoPaso){
        		//Inicializa la variable de blnMuestraCodDealer
        		objComponent.set("v.blnMuestraCodDealer", false);
	        	//Bloquea los usuarios del lead actual en la lista de lUsuarios
	        	for (let nomUsuarioPaso in lUsuariosActivosLeadsPaso){
	        		lUsuariosActivosLeadsPaso[nomUsuarioPaso].blnMuestCodDealer = false;
	        	}//Fin del for para lDealersPaso
	        	//Inicializa el mensaje de salida	
	        	strMsgAdvert = 'Si desactivas este usuario todos los leads seran asiginados a los usuarios de los Dealers: \n\n';
	        	//Dale la vuelta a la lista de lDealersPaso y crea el msg en strMsgAdvert
	        	for (let nomDealerPaso in objWrpDelaerUsruarioPaso.lDealers){
	        		strMsgAdvert += objWrpDelaerUsruarioPaso.lDealers[nomDealerPaso].nombreDist + '. \n';
	        	}//Fin del for para lDealersPaso
	        	//Despliega el msg
	        	this.showToastWarning(objComponent, objEvent, strMsgAdvert);
        	}//Fin si !blnActivoPaso
        }//Fin si !blnSelec
        objComponent.set("v.lUsuariosActivosLeads", lUsuariosActivosLeadsPaso);        
    	console.log('EN hpSelecDealer strMsgAdvert: ' + strMsgAdvert);
    	console.log('EN hpSelecDealer lUsuariosActivosLeads Final: ', objComponent.get("v.lUsuariosActivosLeads"));
    },
    
    cancelar : function(Component, Event, strMensaje) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
    },
    
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },
    
    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2500,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Toast Error */
   showToastWarning: function(Component, Event, strMensaje) {
    let toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Advertencia",
        "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
        "duration": 10000,
        "type": "warning"
    });
    toastEvent.fire();
   },
    
    
})