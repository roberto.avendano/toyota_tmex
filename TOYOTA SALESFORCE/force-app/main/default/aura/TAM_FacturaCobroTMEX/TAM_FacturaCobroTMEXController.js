({
    
    doInit : function(component, event, helper) {
        helper.verifyCheckList(component,event,helper);
        helper.getResultOfList(component,event,helper);
        
    },
    
    guardarController : function(component, event, helper) {
        helper.guardaRegistro(component, event, helper);
    },
    
    closeModel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();    
    },
    
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    }
})