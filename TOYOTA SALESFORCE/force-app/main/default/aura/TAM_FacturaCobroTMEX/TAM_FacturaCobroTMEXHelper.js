({
    
    verifyCheckList  : function(component,event,helper) {
        let edoCtaIN = component.get("v.lineasVINESProvisionados");
        let action = component.get('c.verifyChecksVIN'); 
        
        action.setParams({ 
            edoCtaIN : edoCtaIN
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var response = response.getReturnValue();
                component.set("v.checkListVINES",response);
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    getResultOfList : function(component,event,helper) {
        let edoCtaIN = component.get("v.lineasVINESProvisionados");
        let action = component.get('c.getSUMofList'); 
        
        action.setParams({ 
            edoCtaIN : edoCtaIN
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var response = response.getReturnValue();
                component.set("v.sumaListaVINES",response);
            }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    guardaRegistro : function(component,event,helper) {
        let documentId 			= component.get('v.documentId');
        let facturaCobroTMEX 	= component.get('v.facturaCobroTMEX');
        let objIN 				= component.get('v.checkListVINES');
        let subtotalFactura 	= component.get('v.sumaListaVINES');
        let recordId         	= component.get('v.recordId');
        let tipoProvision 	 	= component.get('v.tipoProvision');
        let validaInputs 		= false; 
        
        if(facturaCobroTMEX == undefined){
          	component.set('v.validarFolioFactura',false);
        	validaInputs = false;                                  
        }
        
        if(facturaCobroTMEX != undefined){
            component.set('v.validarFolioFactura',true); 
            validaInputs = true;
        }
        
        
        if(validaInputs == true){
            let action = component.get('c.guardaFacturaDetalleEdoCta'); 
            
            action.setParams({ 
                documentId 			:documentId,
                facturaCobroTMEX 	:facturaCobroTMEX,
                objIN  				:objIN,
                subtotalFactura 	:subtotalFactura,
                tipoProvision    	:tipoProvision
            });
            
            action.setCallback(this, function(response){
                var objState = response.getState();
                if (objState === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "Success",
                        "title": "Exitoso!",
                        "message": "Se registro correctamente."
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                    component.find("overlayLib").notifyClose();  
                    location.reload(); 
                    
                }
            });
            
            $A.enqueueAction(action);
            
        }
    }
})