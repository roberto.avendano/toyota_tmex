({
	updateSerie : function(component, serieProducto) {
		var action = component.get("c.insertSerie");
		action.setParams({
			"item" : serieProducto
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	console.log(response.getReturnValue());
            	var ftp = {};
            	ftp.Id = response.getReturnValue().FichaTecnica__c;
                this.getSeries(component, ftp);
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
	deleteSerie : function(component, serieProducto) {
		var action = component.get("c.deleteSerie");
		action.setParams({
			"item" : serieProducto
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	console.log(response.getReturnValue());
            	var ftp = {};
            	ftp.Id = response.getReturnValue().FichaTecnica__c;
                this.getSeries(component, ftp);
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
	},
    getSeries: function(component, ftp) {
        console.log("helper.getSeries",ftp.Id);
        //var isComparative = ftp.Versions_comparative__c == "true";
        console.log(ftp.Versions_comparative__c);
        var action = ftp.Versions_comparative__c? component.get("c.getSeriesComparative") :component.get("c.getSeries");
        action.setParams({
            "idFTP": ftp.Id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	console.log(response.getReturnValue());
                component.set("v.series", response.getReturnValue());
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    updateSeriesOrder: function(component, series){
        var action = component.get("c.saveSeriesOrder");
        
        action.setParams({
            "series": series
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Result update series order: "+ state);
            
            if(component.isValid() && state === "SUCCESS") {
                component.find('notifLib').showToast({
                    "variant": "success",
                    "title": "Se actualizo correctamente.",
                    "message": ""
                });
                component.find("overlayLib").notifyClose();
                //component.set("v.series", response.getReturnValue());
            }

            else if(component.isValid() && state === "ERROR"){
                console.log("Failed with state: " + state);
                var errores = response.getError();
                var mensajes = "";

                for(var j = 0; j < errores.length; j++){
                    var messages = errores[j].pageErrors;
                    for(var k = 0; k < messages.length; k++){
                        mensajes += messages[k].message +" : "+messages[k].statusCode +"\n";
                        console.log(messages[k].message);
                        console.log(messages[k].statusCode);
                    }
                }

                this.handleShowNotice(component, mensajes);
            }
        });

        $A.enqueueAction(action);
    },
    updateFTP: function(component, versionesAdd, ftpID){
        var action = component.get("c.saveFTP");
        
        action.setParams({
            "idFTP": ftpID,
            "versionsAdded" : versionesAdd
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("Result update FTP: "+ state);
            
            if(component.isValid() && state === "SUCCESS") {
                component.find('notifLib').showToast({
                    "variant": "success",
                    "title": "Se actualizo correctamente.",
                    "message": ""
                });
                component.find("overlayLib").notifyClose();
                //component.set("v.series", response.getReturnValue());
            }

            else if(component.isValid() && state === "ERROR"){
                console.log("Failed with state: " + state);
                var errores = response.getError();
                var mensajes = "";

                for(var j = 0; j < errores.length; j++){
                    var messages = errores[j].pageErrors;
                    for(var k = 0; k < messages.length; k++){
                        mensajes += messages[k].message +" : "+messages[k].statusCode +"\n";
                        console.log(messages[k].message);
                        console.log(messages[k].statusCode);
                    }
                }

                this.handleShowNotice(component, mensajes);
            }
        });

        $A.enqueueAction(action);
    },

    handleShowNotice : function(component, errorMessages) {
        component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Ups!... Algo salio mal.",
            "message": errorMessages,
            closeCallback: function() {
                //alert('You closed the alert!');
            }
        });
    }
})