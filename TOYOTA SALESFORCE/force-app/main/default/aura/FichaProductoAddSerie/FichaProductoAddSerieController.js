({
	doInit : function(component, event, helper) {
		var ftp = component.get("v.ftp");
		helper.getSeries(component,ftp);
	},
	handleUpdateSerie : function(component, event, helper) {
		console.log("handleUpdateSerie");
		var updatedSerie = event.getParam("serieProducto");
		console.log("updatedSerie", updatedSerie);
		helper.updateSerie(component,updatedSerie);
	},
	handleDeleteSerie : function(component, event, helper) {
		console.log("handleDeleteSerie");
		var deleteSerie = event.getParam("serieProducto");
		console.log("deleteSerie", deleteSerie);
		helper.deleteSerie(component,deleteSerie);
	},
	handleUpdateSeriesOrder: function(component, event, helper) {
		console.log("Updating serie's order.....");
		var seriesList = component.get("v.series");
		var ftp = component.get("v.ftp");
		var series = [];
		var concatVersions = '';

        for(var i = 0; i < seriesList.length; i++){
            if(seriesList[i].isSelected != undefined && seriesList[i].isSelected){
            	concatVersions+= seriesList[i].serie.Name +',';

            } else if(seriesList[i].Id != null){
                series.push(seriesList[i]);
            }
        }

		if(series.length > 0){
			helper.updateSeriesOrder(component, series);
		}

		if(ftp.Versions_comparative__c){
			helper.updateFTP(component, concatVersions, ftp.Id);
		}
	}
})