({
	/** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
	},

    /* Buscador de VIN*/
    SearchVIN : function(objComponent, objEvent, objHelper){
        var searchField = objComponent.find('searchField');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            objHelper.SearchHelper(objComponent, objEvent);
        }
    },

    /* Buscador de Dealer*/
    SearchDealer : function(objComponent, objEvent, objHelper){
        var searchField = objComponent.find('searchField_Dealer');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            objHelper.SearchHelper_Dealer(objComponent, objEvent);
        }
    },

     /*Guardado de Provisión*/
     Save : function(objComponent, objEvent, objHelper){
        objHelper.SaveList(objComponent, objEvent);
    },

    /*Cerrar Provisión */
    cerrar : function(objComponent, objEvent, objHelper){
        objHelper.cerrarProvision(objComponent, objEvent);
    },

    /** Navegación */
    navigation : function(objComponent, objEvent, objHelper){
        var sObjectList = objComponent.get("v.lstRetail_ConReversa");
        var end = objComponent.get("v.endPage");
        var start = objComponent.get("v.startPage");
        var pageSize = objComponent.get("v.pageSize");
        var whichBtn = objEvent.getSource().get("v.name");

        if(whichBtn == 'next'){
            objComponent.set("v.currentPage", objComponent.get("v.currentPage") + 1);
            objHelper.next(objComponent, objEvent, sObjectList, end, start, pageSize);
        } else if(whichBtn == 'previous'){
            objComponent.set("v.currentPage", objComponent.get("v.currentPage") - 1);
            objHelper.previous(objComponent, objEvent, sObjectList, end, start, pageSize);
        }
    }
})