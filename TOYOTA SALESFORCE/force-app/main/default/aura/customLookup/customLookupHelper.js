({
	searchHelper : function(component,event,getInputkeyWord) {
     var action = component.get("c.fetchVehiculo");
     var recordId =  component.get("v.recordId");   
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            recordId 		: recordId
          });
   
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();

                if (storeResponse.length == 0) {
                    component.set("v.Message", 'VIN no encontrado');
                } else {
                    component.set("v.Message", 'Resultado de la busqueda');
                }
                
                component.set("v.listOfSearchRecords", storeResponse);
            }
 
        });

        $A.enqueueAction(action);
    
	},
})