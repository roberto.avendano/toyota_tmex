({
    doInit : function(component, event, helper) {
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        var action = component.get("c.getEvalInfo");
        action.setParams({
            "UserId": userId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.evalCandidato",response.getReturnValue());  
                var evalIniciada = component.get("v.evaluacionIniciada");
                if(evalIniciada == true){
                    $A.util.removeClass(component.find("nuevoCorreo"), "slds-hide");
                    
                }
                
                
            } 
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },    
    
    openModel: function(component, event, helper) {
        component.set("v.isOpen", true);
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        var action = component.get("c.getPickListValuesOrg");
        action.setParams({
            "userId": userId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var campaigns = response.getReturnValue();
                var options = [];
                campaigns.forEach(function(campaign)  { 
                    options.push({ value: campaign, label: campaign});
                });
                component.set("v.organigrama", options);
                
                
            } 
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.isOpen", false);
    },
    
    enviarCorreo: function(component, event, helper) {
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        var Para = component.find('selectedOptionsPara');
        var nuevoCorreo = component.get("v.nuevoCorreo");
        var opcionesPara = component.get("v.selectedOptionsPara");
        if (opcionesPara == "null" || opcionesPara === undefined || opcionesPara === '' ) {
            component.set("v.showError",true);
            var correctValues = false;
        } else {
            var correctValues = true;
            component.set("v.showError",false);
        }
        
        if(correctValues === true){
            var opcionesDe = component.get("v.selectedOptionsDe");
            var opcionesCC = component.get("v.selectedOptionsCC");
            var opcionesCCO = component.get("v.selectedOptionsCCO");
            
            var action = component.get("c.createNewEmail");
            action.setParams({
                "nuevoCorreo": nuevoCorreo,
                "userId"     : userId,
                "deRespuesta" : opcionesDe,
                "opcionesPara" : opcionesPara,
                "opcionesCC"   : opcionesCC,
                "opcionesCCO"	: opcionesCCO	
                
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Correo enviado exitosamente!!",
                        type: 'success',
                        duration:' 4000'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                    
                    
                } 
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(action);
            
        }
        
    }
})