({
    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
       
    },
    
    close : function(objComponent, objEvent, objHelper) {
		objHelper.close(objComponent, objEvent);
	},
    
    guardar : function(objComponent, objEvent, objHelper) {
		objComponent.find("recordEditForm").submit();       
	},
   
    handleSuccess : function(objComponent, objEvent, objHelper) {
        var record = objEvent.getParams().response;
        var navEvt = $A.get("e.force:navigateToSObject");
        var strRecordID = objComponent.get("v.recordId");
        navEvt.setParams({
            "recordId": record.id,
            "slideDevName": "detail"
        });
        navEvt.fire(); 
        
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Correcto.",
            "message": "Se genero correctamente la solicitud.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },
    
    handleError : function(objComponent, objEvent, objHelper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }

})