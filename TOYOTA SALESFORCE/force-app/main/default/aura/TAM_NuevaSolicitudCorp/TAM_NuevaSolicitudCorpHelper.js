({
    initializeComponent : function(objComponent, objEvent) {
        console.log('Entro getDatosCuenta');
        var objAction = objComponent.get("c.getDatosCuenta");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                objComponent.set("v.objAccount", result);
            }
        });
        $A.enqueueAction(objAction);
        this.getRecordTypeName(objComponent, objEvent);
    },
    
    getRecordTypeName : function(objComponent, objEvent){
        console.log('Entro getRecordId');
        var objAction = objComponent.get("c.getRecordId");
        var objAccount = objComponent.get("v.objAccount");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                console.log('Id Rango/Programa' + result);
                objComponent.set("v.strRecordTypeName", result);
            }
        });
        $A.enqueueAction(objAction);
    },
    
    close : function(objComponent, objEvent) {
        var navEvt = $A.get("e.force:navigateToSObject");
        var strRecordID = objComponent.get("v.recordId");
        navEvt.setParams({
            "recordId": strRecordID,
            "slideDevName": "detail"
        });
        navEvt.fire();
    }
})