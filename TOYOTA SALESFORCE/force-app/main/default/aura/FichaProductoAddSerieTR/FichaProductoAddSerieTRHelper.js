({
	addRecord : function(component,serie) {
		var updateEvent = component.getEvent("updateSerie");
        updateEvent.setParams({ "serieProducto": serie });
        updateEvent.fire();
        
	},
	deleteRecord : function(component,serie) {
		console.log("deleteRecord", serie);
		var deleteSerieEvent = component.getEvent("deleteSerie");
        deleteSerieEvent.setParams({ "serieProducto": serie });
        deleteSerieEvent.fire();
	}
})