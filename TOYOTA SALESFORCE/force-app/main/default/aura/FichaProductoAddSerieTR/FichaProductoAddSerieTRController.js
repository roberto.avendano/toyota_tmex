({
    doInit : function(component, event, helper) {
        
        var action = component.get("c.getProfileUser");
       
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.activaFuncionalidad',result);
            }
        });
        
        $A.enqueueAction(action);
        
    },
    handleAddRecord : function(component, event, helper) {
        var serie = component.get("v.serie");
        var btn = event.getSource();
        btn.set("v.disabled",true);
        helper.addRecord(component,serie);
    },
    handleDeleteRecord : function(component, event, helper) {
        console.log("handleDeleteRecord");
        if(confirm("¿Esta seguro de borrar el registro?")){
            var serie = component.get("v.serie");
            var btn = event.getSource();
            btn.set("v.disabled",true);
            helper.deleteRecord(component,serie);
        }
    }
})