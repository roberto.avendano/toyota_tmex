({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
		console.log("EN SolCompEspFlotProg Helper.initializeComponent...");
		
		this.getEstatusSol(objComponent, objEvent);
		//this.gettipoRegistroPrograma(objComponent, objEvent);
		this.gettipoRegistroSolicitud(objComponent, objEvent);		
        this.getSeries(objComponent, objEvent);
        //this.getSolDistribuidores(objComponent, objEvent);
        
    	let mRandom = Math.floor(Math.random() * 10000000000) + 1;
    	let strRandom = mRandom.toString();
    	console.log("EN Helper.initializeComponent mRandom: ", mRandom + ' strRandom: ' + strRandom);
    	objComponent.set("v.objPrevPedido", strRandom);
        
        let strIdSol = objComponent.get("v.recordId");
        let solicitudRTIdPaso = objComponent.get("v.strSolRecortTypeId"); 
        //Llama la función 
		let objAction = objComponent.get("c.getModelos");
        objAction.setParams({   			
        	recordId : strIdSol,
        	solicitudRTId : solicitudRTIdPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                objComponent.set("v.lstModelos", objResponse.getReturnValue());

                this.createObjectData(objComponent, objEvent);
            }
        });
        $A.enqueueAction(objAction); 
    },

    getEstatusSol : function(objComponent, objEvent){
    	console.log('EN getEstatusSol...'); 
        let objAction = objComponent.get("c.getEstatusSol");
        objAction.setParams(
        	{  
        		recordId : objComponent.get("v.recordId") 
        	}
        );  
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let sEstatusSol = objResponse.getReturnValue();
                if (sEstatusSol == 'Cerrada')
                    objComponent.set("v.bSolCerrada",true);
                console.log('EN obtSolicitudCompra bSolCerrada: ', objComponent.get("v.bSolCerrada"));    
            }
        });
        $A.enqueueAction(objAction); 
    },

    getSeries : function(objComponent, objEvent){
    	console.log("EN SolCompEspFlotProg Helper.getSeries...");
    	
        let objAction = objComponent.get("c.getSeries");
        objAction.setParams({
        	recordId : objComponent.get("v.recordId")
   		});        
   		objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let bTieneReg = false;            	
                let result = objResponse.getReturnValue();
                let arrayMapKeys = [];
                for(let key in result){  
                    arrayMapKeys.push({key: key, value: result[key]});
                    bTieneReg = true;
                }
                console.log(arrayMapKeys.sort());
                console.log(arrayMapKeys.reverse());
                objComponent.set("v.mapValues", arrayMapKeys.sort());
               	objComponent.set("v.bSeExistenModelos", bTieneReg);
            }
        });
        $A.enqueueAction(objAction);
    },

    gettipoRegistroPrograma : function(objComponent, objEvent){
    	console.log("EN SolCompEspFlotProg Helper.gettipoRegistroPrograma...");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipRegPrograma");
        objAction.setParams({   			
        	strTipRegNombre : 'Programa'
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspFlotProg Helper.gettipoRegistroPrograma strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 

    gettipoRegistroSolicitud : function(objComponent, objEvent){
    	console.log("EN SolCompEspFlotProg Helper.gettipoRegistroPrograma...");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipoRegSol");
        objAction.setParams({   			
        	recordId : objComponent.get("v.recordId")
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspFlotProg Helper.gettipoRegistroPrograma strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 
    
    getSolDistribuidores : function(objComponent, objEvent){
    	console.log("EN SolCompEspFlotProg Helper.getSolDistribuidores...");
    	let strIdSol = objComponent.get("v.recordId");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipRegPrograma");
        objAction.setParams({   			
        	recordId : strIdSol
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                //objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspFlotProg Helper.getSolDistribuidores strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 


    /** */
    addDealer : function(objComponent, objEvent){
        let strButtonId = objEvent.target.id;
        console.log(strButtonId);

        //Obtener información del registro Seleccionado
        let objModelo = objComponent.get("v.objModeloSeleccionado");

        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getDistribuidores");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstDistribuidores = objResponse.getReturnValue();
                objComponent.set("v.lstDistribuidores", lstDistribuidores);
                //this.createObjectData(objComponent, objEvent);
            }
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** */
    removeDatosModelo : function(Component, Event){
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo...");
    	
    	let varIndex = Event.getSource().get("v.value");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo varIndex: " + varIndex);    	
    	let lstModelosPaso = Component.get("v.lstModelos");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo lstModelosPaso: " + lstModelosPaso.length);
        let objSelecPaso = lstModelosPaso[parseInt(varIndex)];
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo objSelecPaso0: ", objSelecPaso);
    	let sstrIdCatModelos = objSelecPaso.strIdCatModelos;
    	let sstrIdExterno = objSelecPaso.strIdExterno;

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.eliminaDatosModelos");
        objAction.setParams({   			
        	"recordId" : Component.get("v.recordId"),
        	"sstrIdCatModelos" : sstrIdCatModelos,
        	"sstrIdExterno" : sstrIdExterno
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();            	
            	if (objWrpResAct.blnEstatus == false){
            		//Recorre la lista de lstModelosPaso
			     	for(var a in lstModelosPaso){
			    		if (lstModelosPaso[a].strIdExterno == objSelecPaso.strIdExterno){
			    			lstModelosPaso[a].strCantidad = '';
			    			lstModelosPaso[a].boolDistribuidoraActual = false;
			    			console.log("EN SolCompEspFlotProg Helper.removeDatosModelo lstModelosPaso[a]: ", lstModelosPaso[a]);
			    			console.log("EN SolCompEspFlotProg Helper.removeDatosModelo lstModelosPaso.length: ", lstModelosPaso.length);
			    			break;
			    		}//Fin si lstWrpDistPaso[a].intIndex == varIndex
			    	}//Fin del for para lstWrpDistPaso 
			    	//Actaliza la lista de lstModelos con la lista de lstModelosPaso
            		Component.set("v.lstModelos", lstModelosPaso);
            		//Despliega el msg de exito
            		this.showToastSuccess(Component, Event, objWrpResAct.strDetalle);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(Component, Event, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

	}, 
	
	close : function(objComponent, objEvent) {
		document.getElementById("backgroudModalId").classList.remove("slds-backdrop_open");
        document.getElementById("distribuidoresModalId").classList.remove("slds-fade-in-open");
        let RowItemList = objComponent.get("v.lstAsignacionDistribuidores");
        objComponent.set("v.lstAsignacionDistribuidores", '');
    },

    createObjectData: function(objComponent, objEvent) {
        // get the contactList from component and add(push) New Object to List  
        let RowItemList = objComponent.get("v.lstAsignacionDistribuidores");
        let lstDistribuidoes = objComponent.get("v.lstDistribuidores");
        RowItemList.push({
            'sobjectType': 'TAM_DistribuidoresFlotillaPrograma__c',
            'Name': '',
            'TAM_Cantidad__c': '',
            'TAM_DetalleSolicitudCompra_FLOTILLA__c':'',
            'TAM_Estatus__c':'Pendiente',
            'TAM_IdExterno__c':'',
            'TAM_SolicitudCompraFLOTILLA__c':'',

        });
        // set the updated list to attribute (contactList) again    
        objComponent.set("v.lstAsignacionDistribuidores", RowItemList);
    },
    
    showVinesDOD: function(objComponent, objEvent) {   
    	console.log("EN Helper.showVinesDOD..");
    	objComponent.set("v.blnShowVinesDOD", true); 
    	objComponent.set("v.blnShowDistCmp", false);  
    	objComponent.set("v.blnShowPrevPedidoCmp", false);  
    	   
    	console.log("EN Helper.showVinesDOD blnShowVinesDOD: ", objComponent.get("v.blnShowVinesDOD"));
        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelName = objEvent.getSource().get("v.name");
        //console.log("EN Helper.showVinesDOD varObjModeloSelValue: ", JSON.stringify(varObjModeloSelValue) + ' varObjModeloSelName: ' + JSON.stringify(varObjModeloSelName));
        objComponent.set('v.objModeloSel', objEvent.getSource().get("v.value"));
        console.log("EN Helper.showVinesDOD objModeloSel Final: ", objEvent.getSource().get("v.value"));
    },

    showWindowPrevPedido: function(objComponent, objEvent) {
    	console.log("EN Helper.showWindowPrevPedido..");
    	objComponent.set("v.blnShowPrevPedidoCmp", true);  
    	objComponent.set("v.blnShowDistCmp", false);
    	objComponent.set("v.blnShowVinesDOD", false);
    	console.log("EN Helper.showWindowPrevPedido blnShowPrevPedidoCmp: ", objComponent.get("v.blnShowPrevPedidoCmp"));
    	let mRandom = Math.floor(Math.random() * 10000000000) + 1;
    	let strRandom = mRandom.toString();
    	console.log("EN Helper.showWindowPrevPedido mRandom: ", mRandom + ' strRandom: ' + strRandom);
    	objComponent.set("v.objPrevPedido", mRandom.toString());
    	console.log("EN Helper.showWindowPrevPedido objPrevPedido Final: ", objComponent.get("v.objPrevPedido"));
    },
    
    showWindowAddDealer: function(objComponent, objEvent) {
    	console.log("EN Helper.showWindowAddDealer..");
    	objComponent.set("v.blnShowDistCmp", true);  
    	objComponent.set("v.blnShowPrevPedidoCmp", false);  
    	objComponent.set("v.blnShowVinesDOD", false);    
    	  
    	console.log("EN Helper.showWindowAddDealer blnShowDistCmp: ", objComponent.get("v.blnShowDistCmp"));
        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelName = objEvent.getSource().get("v.name");
        //console.log("EN Helper.showWindowAddDealer varObjModeloSelValue: ", JSON.stringify(varObjModeloSelValue) + ' varObjModeloSelName: ' + JSON.stringify(varObjModeloSelName));
        objComponent.set('v.objModeloSel', objEvent.getSource().get("v.value"));
        console.log("EN Helper.showWindowAddDealer objModeloSel Final: ", objEvent.getSource().get("v.value"));
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }          
    
})