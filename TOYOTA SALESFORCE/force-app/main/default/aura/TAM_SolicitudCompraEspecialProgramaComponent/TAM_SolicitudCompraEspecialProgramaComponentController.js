({

    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
    },

    /** openWindowPrevPedido */
    openWindowPrevPedido : function(objComponent, objEvent, objHelper) {        
        objHelper.showWindowPrevPedido(objComponent, objEvent);
    },
    
    /** openWindowAddDealer */
    openWindowAddDealer : function(objComponent, objEvent, objHelper) {        
        objHelper.showWindowAddDealer(objComponent, objEvent);
    },

    /** close */
    close : function(objComponent, objEvent, objHelper) {
        objHelper.close(objComponent, objEvent);
    },
    
    /** addNewRow */
    addNewRow: function(objComponent, objEvent, objHelper) {
        // call the comman "createObjectData" helper method for add new Object Row to List  
        objHelper.createObjectData(objComponent, objEvent);
    },
    
    /** removeDeletedRow */
    removeDeletedRow: function(objComponent, objEvent, objHelper) {
        // get the selected row Index for delete, from Lightning Event Attribute  
        var index = objEvent.getParam("indexVar");
        // get the all List (contactList attribute) and remove the Object Element Using splice method    
        var AllRowsList = objComponent.get("v.lstAsignacionDistribuidores");
        AllRowsList.splice(index, 1);
        // set the contactList after remove selected row element  
        objComponent.set("v.lstAsignacionDistribuidores", AllRowsList);
    },

   /** Funcion Inicial */
	removeRow : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.removeRow..");
		Helper.removeDatosModelo(Component, Event);
    },

    /** GuardarDetalleDistribuidores */
    GuardarDetalleDistribuidores: function(objComponent, objEvent, objHelper) {        
    },

    consultaVinesDOD : function(objComponent, objEvent, objHelper) {        
        objHelper.showVinesDOD(objComponent, objEvent);
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
      // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})