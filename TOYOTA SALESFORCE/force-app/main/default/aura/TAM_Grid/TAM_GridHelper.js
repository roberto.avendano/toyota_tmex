({
	/* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
        
        this.getRecordType(objComponent, objEvent);
        var objAction = objComponent.get("c.getDetalleJunction");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                }
                if(arrayMapKeys.length == 0){
                   objComponent.set("v.disponibleEnInventario", false); 
                }
                
                arrayMapKeys.reverse();
                objComponent.set("v.mapValues", arrayMapKeys);
                this.getSeries(objComponent, objEvent);
                this.getCurrentUser(objComponent, objEvent);
            }else {
                objComponent.set("v.disponibleEnInventario", false);
            }
        });
        $A.enqueueAction(objAction);
    },
    
    /**Obtener Tipo de Registro de la Politica */
    getRecordType : function(objComponent, objEvent){
        var objAction = objComponent.get("c.TipoRegistroPolitica");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var strResult = objResponse.getReturnValue();
                objComponent.set("v.recordTypeName", strResult);
                if(strResult == 'Venta Corporativa'){
                    this.getRangos(objComponent, objEvent);
                }
            }
        });
        $A.enqueueAction(objAction);
    },

    /** Obtener lista de Rangos Disponibles */
    getRangos : function(objComponent, objEvent){
        var objAction = objComponent.get("c.getRangos");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                lstResult.sort();
                objComponent.set("v.lstRangos", lstResult);
            }
        });
        $A.enqueueAction(objAction);

    },
    
    /** Obtener mapa de años y series */
    getSeries : function(objComponent, objEvent) {
        var objAction = objComponent.get("c.getSeries");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                }
                if(arrayMapKeys.length > 0){
                   objComponent.set("v.loaded", true); 
                }
                arrayMapKeys.reverse();
                objComponent.set("v.mapAnioSerie", arrayMapKeys);
            }
        });
        $A.enqueueAction(objAction);
    },
    
    /** Actualizar Model Year*/
    updateMY : function(objComponent, objEvent){
        var objAction = objComponent.get("c.actualizarModelos");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                this.showToastSuccess(objComponent, objEvent);
                location.reload();
            } else {
                this.showToastError(objComponent, objEvent);
            }
        });
        $A.enqueueAction(objAction);
    },
    
        /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Modelos actualizados correctamente.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con la actualización, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Obtener Current User */
    getCurrentUser : function(objComponent, objEvent) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var objAction = objComponent.get("c.getCurrentUser");
        objAction.setParams({  userId :userId  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                if(result){
                    objComponent.set("v.boolUsuarioSoloLectura", true);
                }
            }
        });
        $A.enqueueAction(objAction);
    }
    
})