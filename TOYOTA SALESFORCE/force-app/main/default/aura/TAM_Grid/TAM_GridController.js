({
	/** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
       
    },
    
    /** Obtener información de la lista wrapper */
    getInfoWrapper: function (objComponent, objEvent, objHelper) {
        objHelper.getInfoWrapper(objComponent, objEvent, openSections, openSeccionAnios);
    },
    
    /** Actualizar modelos faltantes del catálogo centralizado */
    updateMY: function (objComponent, objEvent, objHelper) {
        objHelper.updateMY(objComponent, objEvent);
    },
    
    duplicar : function(objComponent) {
		objComponent.set("v.showModalDuplicate", true);
    },

    
})