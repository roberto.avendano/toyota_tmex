({

    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.buscaPerfilUserActual(objComponent, objEvent);
		objHelper.buscaNombreCliente(objComponent, objEvent);
    },

   /** Funcion Inicial */
	buscaClientes : function(objComponent, objEvent, objHelper) {
		objHelper.helperBuscaClientes(objComponent, objEvent);
    },

	onRadioCliente: function(objComponent, objEvent) {
		console.log("EN Controller onRadioCliente...");	
		let varNameCteSel = objEvent.getSource().get("v.name");
		let varTextCteSel = objEvent.getSource().get("v.text");
		console.log("EN Controller onRadioCliente varTextCteSel: ", varTextCteSel);
   		objComponent.set("v.strClienteSeleccionadoMerge", varTextCteSel);
   		objComponent.set("v.SelCteMerge", false);
   		let llClientesDealerPaso = objComponent.get("v.lClientesDealer");
   		//Recorre la lista de reg de lClientesDealer
   		for (let clienteId in llClientesDealerPaso){
   			console.log("EN Controller onRadioCliente strIdCliente: ", llClientesDealerPaso[clienteId].strIdCliente);
   			console.log("EN Controller onRadioCliente ", varTextCteSel);
    		if (llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel){
    			llClientesDealerPaso[clienteId].blnBloqueado = true;
    			llClientesDealerPaso[clienteId].blnSeleccionaCte = true;
    			llClientesDealerPaso[clienteId].blnSeleccionado = false;
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel
    		if (llClientesDealerPaso[clienteId].strIdCliente != varTextCteSel){
    			llClientesDealerPaso[clienteId].blnBloqueado = false;
    			llClientesDealerPaso[clienteId].blnSeleccionado = false;    		
    			llClientesDealerPaso[clienteId].blnSeleccionaCte = false;
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente != varTextCteSel
   		}//Fin del for para llClientesDealerPaso
   		objComponent.set("v.lClientesDealer", llClientesDealerPaso);
	},

	onCheckClienteMerge: function(objComponent, objEvent) {
		console.log("EN Controller onCheckClienteMerge...");	
   		//objComponent.set("v.strClienteSeleccionadoMerge", varTextCteSel);   		
		objComponent.set("v.SelCtesMerge", false);
   		let llClientesDealerPaso = objComponent.get("v.lClientesDealer");
   		//Recorre la lista de reg de lClientesDealer
   		for (let clienteId in llClientesDealerPaso){
    		if (llClientesDealerPaso[clienteId].blnSeleccionado){
    			objComponent.set("v.SelCtesMerge", false);
    			break;    		
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso   		
		console.log("EN Controller onCheckClienteMerge SelCtesMerge: ", objComponent.get("v.SelCtesMerge"));
	},

	unirRegistros: function(objComponent, objEvent, objHelper) {
		console.log("EN Controller unirRegistros...");
		objHelper.helperMergeClientes(objComponent, objEvent);		
	},
	 
    closeQuickAction : function(objComponent, objEvent, objHelper) {
        // Close the action panel
        let dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

})