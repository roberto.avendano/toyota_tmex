({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	buscaPerfilUserActual : function(objComponent, objEvent) {
		console.log("EN buscaPerfilUserActual...");
        //Llama la función 
		let objAction = objComponent.get("c.getPerfilUserActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let blnAdminUser = objResponse.getReturnValue();
           		console.log("EN buscaPerfilUserActual blnAdminUser: ", blnAdminUser);
            	if (blnAdminUser == true){
            		objComponent.set("v.blnUserAdmin", true);
            		objComponent.set("v.blnNoUserAdmin", false);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (blnAdminUser == false){
            		objComponent.set("v.blnUserAdmin", false);
            		objComponent.set("v.blnNoUserAdmin", true);
            	}
           		console.log("EN buscaPerfilUserActual blnUserAdmin: ", objComponent.get("v.blnUserAdmin") );
           		console.log("EN buscaPerfilUserActual blnNoUserAdmin: ", objComponent.get("v.blnNoUserAdmin") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	buscaNombreCliente : function(objComponent, objEvent) {
		console.log("EN buscaNombreCliente...");
        //Llama la función 
		let objAction = objComponent.get("c.getNombreClienteActual");
        objAction.setParams({
        	"IdClienteActual" : objComponent.get("v.recordId")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let strCadenaDeBusquedaResult = objResponse.getReturnValue();
           		console.log("EN buscaNombreCliente strCadenaDeBusquedaResult: ", strCadenaDeBusquedaResult);
           		objComponent.set("v.strCadenaDeBusqueda", strCadenaDeBusquedaResult);
           		console.log("EN buscaNombreCliente strCadenaDeBusqueda: ", objComponent.get("v.strCadenaDeBusqueda") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	helperBuscaClientes : function(objComponent, objEvent) {
		console.log("EN helperBuscaClientes...");
        //Llama la función 
		let objAction = objComponent.get("c.getClientesCorporativos");
        objAction.setParams({
        	"IdRegistroPrincipal" : objComponent.get("v.recordId"),
        	"strCadenaDeBusqueda" : objComponent.get("v.strCadenaDeBusqueda")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lClientesDealerResult = objResponse.getReturnValue();
           		objComponent.set("v.lClientesDealer", lClientesDealerResult);
           		if (lClientesDealerResult.length == 0)
           			objComponent.set("v.blnNoTieneReg", true);
           		else
           			objComponent.set("v.blnNoTieneReg", false);
           		let mainRecordId = objComponent.get("v.recordId");
           		let IdObjSolActCteCorp = $A.get("$Label.c.TAM_IdObjSolActCteCorp");
           		let blnDesdeSolicitudFinal = mainRecordId.startsWith(IdObjSolActCteCorp);
       			objComponent.set("v.blnDesdeSolicitud", blnDesdeSolicitudFinal);            		
           		console.log("EN helperBuscaClientes lClientesDealerResult: ", lClientesDealerResult);
           		console.log("EN helperBuscaClientes blnNoTieneReg: ", objComponent.get("v.blnNoTieneReg"));
           		console.log("EN helperBuscaClientes blnDesdeSolicitud: ", objComponent.get("v.blnDesdeSolicitud"));
           		console.log("EN helperBuscaClientes blnDesdeSolicitudFinal: ", blnDesdeSolicitudFinal);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	helperMergeClientes : function(objComponent, objEvent) {
		console.log("EN helperMergeClientes...");
		console.log("EN helperMergeClientes UsarDartosCteSol: ", objComponent.get("v.UsarDartosCteSol"));
		let SelCteMerge = objComponent.get("v.SelCteMerge");
		let SelCtesMerge = objComponent.get("v.SelCtesMerge");
		//Ve si ya tiene seleccionado algo
		if (!SelCteMerge && !SelCtesMerge)
			this.MergeClientesFinal(objComponent, objEvent);
		if (SelCteMerge)
       		this.showToastError(objComponent, objEvent, 'Debes seleccionar al cliente final para el Merge.');
		if (SelCtesMerge)
       		this.showToastError(objComponent, objEvent, 'Debes seleccionar al menos un cliente para el Merge.');			
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	MergeClientesFinal : function(objComponent, objEvent) {
		console.log("EN MergeClientesFinal...");
		console.log("EN MergeClientesFinal UsarDartosCteSol: ", objComponent.get("v.UsarDartosCteSol"));
        //Llama la función 
		let objAction = objComponent.get("c.mergeClientes");
        objAction.setParams({
        	"IdRegistroPrincipal" : objComponent.get("v.recordId"),
        	"strClienteSeleccionadoMerge" : objComponent.get("v.strClienteSeleccionadoMerge"),
        	"lClientesDealer" : objComponent.get("v.lClientesDealer"),
        	"UsarDartosCteSol" : objComponent.get("v.UsarDartosCteSol")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResAct = objResponse.getReturnValue();
            	console.log("EN Helper.MergeClientesFinal objWrpResAct: ", objWrpResAct);
            	if (objWrpResAct.blnEstatus == false){
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);
            		let blnDesdeSolicitud = objComponent.get("v.blnDesdeSolicitud");
            		//Recarga la pagina
            		if (blnDesdeSolicitud == true) 
            			location.reload();
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1500,
            "type": "error"
        });
        toastEvent.fire();
    },

})