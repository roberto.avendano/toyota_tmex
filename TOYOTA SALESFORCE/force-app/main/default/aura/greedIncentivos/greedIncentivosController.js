/** 
    Descripción General: Controlador del componente greedIncentivos
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz            14/Nov/2019         Versión Inicial
    ________________________________________________________________
*/
({
	/** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
    },

    /** Obtener información de la lista wrapper */
    getInfoWrapper: function (objComponent, objEvent, objHelper) {
        var openSeccionAnios = objComponent.get("v.activeSectionsAnios");
        var openSections = objComponent.get("v.activeSectionsSeries");

        console.log(openSeccionAnios);
        console.log(openSections);
        if(openSections.length > 0 && openSeccionAnios.length > 0){
            if(openSections.length >= 2 ){
                openSections.shift();
            }
            
            if(openSeccionAnios.length >= 2 ){
                openSeccionAnios.shift();
            }
            console.log('LISTA LIMPIA AÑOS: ' + openSeccionAnios);
            console.log('LISTA LIMPIA SERIE: ' + openSections);

            objComponent.set("v.activeSectionsAnios", openSeccionAnios);
            objComponent.set("v.activeSectionsSeries", openSections);

            objHelper.getInfoWrapper(objComponent, objEvent, openSections, openSeccionAnios);
        }

    },

    /** Añadir fila en la tabla */
    addRow : function (objComponent, objEvent, objHelper) {
        objHelper.addRow(objComponent, objEvent);
    },

    /** Remover fila de la tabla */
    deleteRow : function (objComponent, objEvent, objHelper) {
        objHelper.deleteRow(objComponent, objEvent);
    },

    /** Guardar */
    saveSeccion : function(objComponent, objEvent, objHelper){
        objHelper.saveSeccion(objComponent, objEvent);
    }
        
})