/** 
    Descripción General: Helper del componente greedIncentivos
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz            14/Nov/2019         Versión Inicial
    ________________________________________________________________
*/
({
	/* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
        this.getSeries(objComponent, objEvent);
        var objAction = objComponent.get("c.TipoRegistroPolitica");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var strResult = objResponse.getReturnValue();
                objComponent.set("v.recordTypeName", strResult);
                if(strResult == 'Venta Corporativa'){
                    this.getRangos(objComponent, objEvent);
                }
            }
        });
        $A.enqueueAction(objAction);
    },

    /** Obtener lista de Rangos Disponibles */
    getRangos : function(objComponent, objEvent){
        var objAction = objComponent.get("c.getRangos");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                lstResult.sort();
                objComponent.set("v.lstRangos", lstResult);
            }
        });
        $A.enqueueAction(objAction);

    },

    /** Obtener listado de series y año modelo de vehiculos para desplegar el acordión */
    getSeries : function(objComponent, objEvent){
        var objAction = objComponent.get("c.getSeries");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
               
                var result = objResponse.getReturnValue();
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                }
                arrayMapKeys.reverse();
                
                
                objComponent.set("v.mapValues", arrayMapKeys);
                //objComponent.set("v.activeSectionsAnios", arrayMapKeys[0].key);
                //objComponent.set("v.activeSectionsSeries", arrayMapKeys[0].value[0]);
                //this.getInfoWrapper(objComponent, objEvent, arrayMapKeys[0].value[0], arrayMapKeys[0].key);
                
                
            }
        });
        $A.enqueueAction(objAction);
      
    },

    /** Obtener información a mostrar mediante lista wrapper */
    getInfoWrapper : function(objComponent, objEvent, openSections, openSeccionAnios){

        objComponent.set("v.loaded", false);
        var objAction = objComponent.get("c.setWrapper");
        var seccionSelected = openSections + openSeccionAnios;

        objAction.setParams({   openSections : seccionSelected,
                                recordId : objComponent.get("v.recordId")
                            });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var strResult = objResponse.getReturnValue();
                
                strResult.sort(function (a, b) {
                    return (a.intClaveOrdenamiento - b.intClaveOrdenamiento)
                })
                
                objComponent.set("v.lstWrapperContent", strResult);
                objComponent.set("v.loaded", true);
                
            }
        });
        $A.enqueueAction(objAction);
    },

    /** Añadir fila en tabla */
    addRow : function(objComponent, objEvent){
        var lstWrapper = objComponent.get("v.lstWrapperContent");
        var intConsecutivo = objComponent.get("v.numConsecutivo");
        var intConsecutivoFinal = intConsecutivo + 1;
        var buttonId = objEvent.target.id;
        var objWrapperAux = lstWrapper[buttonId];
        var objWrapperClone = Object.assign({}, objWrapperAux);
        
        objWrapperClone.boolClonado = true;
        objWrapperClone.intConsecutivo = intConsecutivoFinal;
        
        objComponent.set("v.numConsecutivo", intConsecutivoFinal);
        lstWrapper.push(objWrapperClone);

    
		
        objComponent.set("v.lstWrapperContent", lstWrapper);
    },

    /** Remover fila en tabla */
    deleteRow : function(objComponent, objEvent){
        var buttonId = objEvent.target.id;
        var lstWrapper = objComponent.get("v.lstWrapperContent");
        var objWrapperAux = lstWrapper[buttonId];
        var index = lstWrapper.indexOf(objWrapperAux);
        if (index > -1) {
            lstWrapper.splice(index, 1);
        }
        objComponent.set("v.lstWrapperContent", lstWrapper);
    },

    /** Guardado de sección */
    saveSeccion : function(objComponent, objEvent){
        var lstWrapper = objComponent.get("v.lstWrapperContent");

        //validar información
        lstWrapper.forEach(function(objWrapper, index){
            
            //Pesos
            if(objWrapper.intIncentivoPropuesto_Pesos != null){
                var incentivoPesos =  objWrapper.intIncentivoTMEX_Pesos + objWrapper.intIncentivoDealer_Pesos;
                if(objWrapper.intIncentivoPropuesto_Pesos != incentivoPesos){
                    
                    alert('Favor de verificar la distribución de incentivo para el vehículo:\n' + objWrapper.strSerie + ' ' + objWrapper.strClaveModelo + ' ' + objWrapper.strVersion + ' ' + objWrapper.strAnioModelo);

                }
            }

            //Porcentaje
            if(objWrapper.intIncentivoPropuesto_Porcentaje != null){
                var incentivoPorcentaje =  objWrapper.intIncentivoTMEX_Porcentaje + objWrapper.intIncentivoDealer_Porcentaje;
                if(objWrapper.intIncentivoPropuesto_Porcentaje != incentivoPorcentaje){
                    
                    alert('Favor de verificar la distribución de incentivo para el vehículo:\n' + objWrapper.strSerie + ' ' + objWrapper.strClaveModelo + ' ' + objWrapper.strVersion + ' ' + objWrapper.strAnioModelo);
                    
                }
            }

        });


        var objAction = objComponent.get("c.guardarPolitica");
        objAction.setParams(
        		{   			
                    lstWrapper : JSON.stringify(lstWrapper),
                    recordId : objComponent.get("v.recordId")
        		}
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0 ){
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);


    },

    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
})