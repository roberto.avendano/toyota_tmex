({

    /** getDistribEntrega */
    getDistribEntrega : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega objModeloSelVinExcep0: ", JSON.stringify(Component.get("v.objModeloSelVinExcep")) );
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strAnioModelo0.1: " + Component.get("v.objModeloSelVinExcep").strAnioModelo);
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidores");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte") 
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);

            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl lstDistriEntregaResult[0].blnFlotillaPrograma: ", lstDistriEntregaResult[0].blnFlotillaPrograma);
            	Component.set("v.blnPedidoEspecial", lstDistriEntregaResult[0].blnFlotillaPrograma);
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl blnPedidoEspecial: ", Component.get("v.blnPedidoEspecial"));

		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdSolicitud;
		    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strAnioModelo1: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strIdRegistro1: " + Component.get("v.strIdRegistro")); 			
		        
		        //Llama a la funcion de hpGetFilialesGpoCorp
		        this.hpGetFilialesGpoCorp(Component);

 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** getDistribEntregaInv */
    getDistribEntregaInv : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntregaInv objModeloSelVinExcep0: ", JSON.stringify(Component.get("v.objModeloSelVinExcep")) );
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntregaInv strAnioModelo0.1: " + Component.get("v.objModeloSelVinExcep").strAnioModelo);
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidoresInv");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte") 
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);

            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl lstDistriEntregaResult[0].blnFlotillaPrograma: ", lstDistriEntregaResult[0].blnFlotillaPrograma);
            	Component.set("v.blnPedidoEspecial", lstDistriEntregaResult[0].blnFlotillaPrograma);
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl blnPedidoEspecial: ", Component.get("v.blnPedidoEspecial"));
            	
		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin[0].strIdSolicitud;
		    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntregaInv strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntregaInv strAnioModelo2: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntregaInv strIdRegistro2: " + Component.get("v.strIdRegistro"));

		        //Llama la funcion para poder consultar las filiales
		        //this.getFilialesGpoCorp(Component);

 			}
        });
        $A.enqueueAction(objAction);
        
	}, 

	//Llama a la función de hpGetFilialesGpoCorp	
    hpGetFilialesGpoCorp: function (Component) {
	   console.log("EN Heler.hpGetFilialesGpoCorp recordId: ", Component.get("v.recordId"));
       let action = Component.get("c.getFilialesGpoCorp");  
       action.setParams({  
            "recordId" : Component.get("v.recordId")
       });
       action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){
                let listFilialesGpoCorpResult = response.getReturnValue();
                console.log("EN Heler.hpGetFilialesGpoCorp listFilialesGpoCorpResult1: ", listFilialesGpoCorpResult);
                if(listFilialesGpoCorpResult != null){
                    Component.set("v.listFilialesGpoCorp", listFilialesGpoCorpResult);    
                    console.log('EN Save hpGetFilialesGpoCorp listFilialesGpoCorpResult2: ' + listFilialesGpoCorpResult);
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si state=='SUCCESS'
       });  
       $A.enqueueAction(action);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpSelecFilialGpoCorp: function(objComponent, Event) {
    	console.log('EN hpSelecFilialGpoCorp...');

        let indexFilialPaso = Event.getSource().get("v.text");
        let srtFilialPaso = Event.getSource().get("v.value");
    	console.log('EN hpSelecFilialGpoCorp indexFilialPaso: ', indexFilialPaso);
    	console.log('EN hpSelecFilialGpoCorp srtFilialPaso: ', srtFilialPaso);
      	objComponent.set("v.strFilial", srtFilialPaso);    
    },

    searchHelper : function(component,event,getInputkeyWord) {
    	console.log("EN Helper.searchHelper getInputkeyWord: ", getInputkeyWord);    

    	let lstFilialEncontFinal = [];
    	let blnSelAcuseFact = false;
    	
    	//Toma la lista de reg que ya tienes en la variable de listFilialesGpoCorp
    	let listFilialesGpoCorpPaso = component.get("v.listFilialesGpoCorp");
    	let getInputkeyWordPaso = getInputkeyWord.toUpperCase();
    	console.log("EN Helper.searchHelper getInputkeyWordPaso: ", getInputkeyWordPaso);    
    		
    	//Recorre la lista de listFilialesGpoCorpPaso y busca el valor getInputkeyWord
    	for (let strFilial in listFilialesGpoCorpPaso){
    		let strNombreFilialPaso = listFilialesGpoCorpPaso[strFilial].sNombreFilial.toUpperCase();
    		console.log("EN Helper.searchHelper strNombreFilialPaso: ", strNombreFilialPaso);    
    		if (strNombreFilialPaso.includes(getInputkeyWordPaso) && strNombreFilialPaso != '--- FILIAL ---'){
    			lstFilialEncontFinal.push(listFilialesGpoCorpPaso[strFilial]);
    			blnSelAcuseFact = true;
    			console.log("EN Helper.searchHelper YA LA ENCONTRO strNombreFilialPaso: ", strNombreFilialPaso);
    		}//Fin si listFilialesGpoCorpPaso[strFilial].sNombreFilial
    	}//Fin del for para listFilialesGpoCorpPaso
    	console.log("EN Helper.searchHelper blnSelAcuseFact: ", blnSelAcuseFact);    
    	console.log("EN Helper.searchHelper lstFilialEncontFinal: ", lstFilialEncontFinal);    
    	
        if (!blnSelAcuseFact)
        	component.set("v.Message", 'No se encontraron Filiales con este texto: ', getInputkeyWord);
        if (blnSelAcuseFact){
        	component.set("v.Message", 'Resultado de la busqueda');
        	component.set("v.listFilialesGpoCorpBusq", lstFilialEncontFinal); 
  			console.log("EN Helper.searchHelper lstFilialEncontFinal FINAL: ", lstFilialEncontFinal);
        }
                
    },

    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	let wrpDistr = [];
    	Component.set("v.lstDistriEntrega", wrpDistr); 
    	Component.set("v.blnShowVinesDOD", false);
    	Component.set("v.blnCreaExcepcionFlotillaPrograma", false);
    	Component.set("v.blnShowVinesConfirma", false);
    },

    /** Guardado de sección */
    hpGuardar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	
    	let blnCapturoVin = false;
    	let intCntReg = 1;
    	let lstDistriEntrega = Component.get("v.lstDistriEntrega");
    	let blnPedidoEspecial = Component.get("v.blnPedidoEspecial");
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntrega){
    		if (lstDistriEntrega[strVin].blnConfirmaFactura && (lstDistriEntrega[strVin].strNomFilGpoCorp == '' || lstDistriEntrega[strVin].strNomFilGpoCorp == null)){
    			this.showToastWarning(Component, Event,'En el registro : ' + intCntReg + ' debes capturar la Filial.', 'warning');
    			return;
    		}//Fin si lstDistriEntrega[strVin].bolSeleccionar && (lstDistriEntrega[strVin].strVin == '' || lstDistriEntrega[strVin].strVin == null    		
    		if (lstDistriEntrega[strVin].blnConfirmaFactura && lstDistriEntrega[strVin].strNomFilGpoCorp != '' && lstDistriEntrega[strVin].strNomFilGpoCorp != null){
    			blnCapturoVin = true;
    		}//Fin si lstDistriEntrega[strVin].bolSeleccionar && (lstDistriEntrega[strVin].strVin == '' || lstDistriEntrega[strVin].strVin == null    		
    		//Sumale 1 al contador
    		intCntReg++;
    	}//Fin del for para lstDistriEntrega
    	    	
    	//Ve si seleciono un VIN por lo menos
    	if (!blnCapturoVin){
    		this.showToastWarning(Component, Event,'Debes seleccionar al menos un VIN para Facturar.', 'warning');
    		return;
    	}//fin si !blnCapturoVin
    	
    	//Manda llamar la funcion que va a actualizar la fecha de recibo
        let objAction = Component.get("c.updateFechaRecibo");
        objAction.setParams({
        	"lstDistriEntrega" : Component.get("v.lstDistriEntrega"),
        	"blnPedidoEspecial" : blnPedidoEspecial,
        	"recordId" : Component.get("v.recordId")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResultadoActualizacionResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.objWrpResultadoActualizacionResult: ", objWrpResultadoActualizacionResult);
            	//Manda el correo hpEnviaCorreoNotificacion
            	this.hpEnviaCorreoNotificacionSave(Component, Event);
            	//Inicializa la lista de lstWrpDistAct a null
            	let wrpDistr = [];            	
		    	Component.set("v.lstDistriEntrega", wrpDistr); 
		    	Component.set("v.blnShowVinesDOD", false);
		    	Component.set("v.blnCreaExcepcionFlotillaPrograma", false);
		    	Component.set("v.blnShowVinesConfirma", false);
		    	Component.set("v.blnShowFacturaOtroClienteFlotProg", false);
		    	Component.set("v.blnSelectTodosCE", false);
		    	
            	//Despliega el mensaje de exito	
            	this.showToastWarning(Component, Event,'Los datos se actualizarón con exito.', 'success');
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    	
    },
    
    /** Guardado de sección */
    desabilitaEstatus : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.desabilitaEstatus...");    
        let indexPaso = Event.getSource().get("v.text");
        let srtValorPaso = Event.getSource().get("v.value");
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.desabilitaEstatus indexPaso: " + indexPaso);
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.desabilitaEstatus srtValorPaso: " + srtValorPaso);
        let lista = Component.get("v.lstDistriEntrega");
        let objSelecPaso = lista[parseInt(indexPaso)];
        Component.set("v.lstDistriEntrega",lista);
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.desabilitaEstatus objSelecPaso: ", objSelecPaso);
    },

    /** Guardado de sección */
    bloqueaCampos : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.bloqueaCampos...");    
        let indexPaso = Event.getSource().get("v.name");
        let srtValorPaso = Event.getSource().get("v.value");
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.bloqueaCampos indexPaso: " + indexPaso);
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.bloqueaCampos srtValorPaso: " + srtValorPaso);
        let lista = Component.get("v.lstDistriEntrega");
        if (srtValorPaso == 'Cancelado'){
        	lista[parseInt(indexPaso)].bolCancelada = true;
        	lista[parseInt(indexPaso)].bolConfirmaEntrega = false;
        	lista[parseInt(indexPaso)].dtFechaEntrega = null;	
        	lista[parseInt(indexPaso)].strStyleCancelacion = 'position: absolute; top: 8px';
        }else{
        	lista[parseInt(indexPaso)].bolCancelada = false;
        	lista[parseInt(indexPaso)].strStyleCancelacion = 'position: absolute; top: -8px';
        }        	
        let objSelecPaso = lista[parseInt(indexPaso)];	
        Component.set("v.lstDistriEntrega",lista);
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.bloqueaCampos objSelecPaso: ", objSelecPaso);
    },

    /** hpSeleFact */
    hpSeleFact : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact...");    
        let indexPaso = Event.getSource().get("v.name");
        let srtValorPaso = Event.getSource().get("v.value");
    	let lstVinSelecFactPaso = Component.get("v.lstVinSelecFact");

    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact indexPaso: " + indexPaso);
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact srtValorPaso: " + srtValorPaso);
        let lista = Component.get("v.lstDistriEntrega");
       	let sVinSelFact = lista[parseInt(indexPaso)].strVin;	
       	lstVinSelecFactPaso.push(sVinSelFact);       
       	Component.set("v.lstVinSelecFact", lstVinSelecFactPaso);
       	if (srtValorPaso) 
       		Component.set("v.blnEnviarCorreo", false); 
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact blnEnviarCorreo: ", Component.get("v.blnEnviarCorreo"));       	       	
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact lstDistriEntrega: ", Component.get("v.lstVinSelecFact"));       	
    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.hpSeleFact objSelecPaso: ", objSelecPaso);
    },

    /** hpEnviaCorreoNotificacion */
    hpEnviaCorreoNotificacionSave: function(objComponent, Event) {
	    console.log('EN hpEnviaCorreoNotificacion...: ');
	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    		   
		let objAction = objComponent.get("c.enviaCorreoInfo");
		objAction.setParams({  
			"solicitudId" : objComponent.get("v.recordId"),
			"lstDistriEntregaPrm" : lstDistriEntregaFinal,
			"sCorreoAdici1" : objComponent.get("v.sCorreoAdici1")
		});
		objAction.setCallback(this, function(objResponse){
		let objState = objResponse.getState();
		if (objState === "SUCCESS"){
		  	let objWrpResAct = objResponse.getReturnValue();
		   	console.log('EN hpEnviaCorreoNotificacion objWrpResAct: ' + objWrpResAct);
		    //if (objWrpResAct.blnEstatus == false)
		    //   	this.showToastSuccess(objComponent, Event, objWrpResAct.strDetalle);
		    if (objWrpResAct.blnEstatus == true)
		       	this.showToastError(objComponent, Event, objWrpResAct.strDetalle);
		   }//Fin si objState === "SUCCESS"
	    });
	    $A.enqueueAction(objAction);
       
    },

    /** hpEnviaCorreoNotificacion */
    hpEnviaCorreoNotificacion: function(objComponent, Event) {
	    console.log('EN hpEnviaCorreoNotificacion...: ');
	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    	let lstDistriEntregaSelFinal = [];
    	let blnSelAcuseFact = false;
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){    	
    		//Ve si seleciono a un reg 
    		if (lstDistriEntregaFinal[strVin].blnConfirmaFactura){
    			lstDistriEntregaSelFinal.push(lstDistriEntregaFinal[strVin]);
    			blnSelAcuseFact = true;
    		}//Fin si lstDistriEntregaFinal[strVin].blnConfirmaFactura
    	}//Fin del for para lstDistriEntrega
    	    	
    	//Ve si seleciono un VIN por lo menos
    	if (!blnSelAcuseFact){
    		this.showToastWarning(objComponent, Event,'Debes seleccionar al menos un VIN para el acuse de factura.', 'warning');
    		return;
    	}//fin si !blnCapturoVin

	   //selecciono al menos un Vin para facturar
	   if (blnSelAcuseFact){
		   console.log('EN hpEnviaCorreoNotificacion blnSelAcuseFact: ' + blnSelAcuseFact);
	   
		   let objAction = objComponent.get("c.enviaCorreoInfo");
		   objAction.setParams(
			   {  
				   "solicitudId" : objComponent.get("v.recordId"),
				   "lstDistriEntregaPrm" : lstDistriEntregaSelFinal,
				   "sCorreoAdici1" : objComponent.get("v.sCorreoAdici1")
			   }
		   );
		   objAction.setCallback(this, function(objResponse){
		   let objState = objResponse.getState();
		   if (objState === "SUCCESS"){
			   	let objWrpResAct = objResponse.getReturnValue();
			   	console.log('EN hpEnviaCorreoNotificacion objWrpResAct: ' + objWrpResAct);
		        if (objWrpResAct.blnEstatus == false)
		          	this.showToastSuccess(objComponent, Event, objWrpResAct.strDetalle);
		        if (objWrpResAct.blnEstatus == true)
		          	this.showToastError(objComponent, Event, objWrpResAct.strDetalle);
			   }//Fin si objState === "SUCCESS"
	       });
	       $A.enqueueAction(objAction);	   
	   }//Fin si blnSelAcuseFact
       
    },

    /** hpSeleTodos */
    hpSeleTodos: function(objComponent, Event) {
	    console.log('EN hpSeleTodos...: ');

        let blnSelectTodosPaso = Event.getSource().get("v.value");
	    console.log('EN hpSeleTodos blnSelectTodosPaso: ', blnSelectTodosPaso);
	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    	let lstDistriEntregaSelFinal = [];
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '')
    			lstDistriEntregaFinal[strVin].blnConfirmaFactura = blnSelectTodosPaso;
    	}//Fin del for para lstDistriEntrega
    	
    	//Actualiza la lista final
    	objComponent.set("v.lstDistriEntrega", lstDistriEntregaFinal);
    },


    /** hpSeleTodosCR */
    hpSeleTodosCR: function(objComponent, Event) {
	    console.log('EN hpSeleTodosCR...: ');

        let hpSeleTodosCRPaso = Event.getSource().get("v.value");
        let dtFechaReciboCRPaso = objComponent.get("v.dtFechaReciboCR");        
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    	let lstDistriEntregaSelFinal = [];
	    console.log('EN hpSeleTodosCR hpSeleTodosCRPaso: ', hpSeleTodosCRPaso);
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''){
    			lstDistriEntregaFinal[strVin].bolSeleccionar = hpSeleTodosCRPaso;
    			if (!lstDistriEntregaFinal[strVin].bolSeleccionar && dtFechaReciboCRPaso !== null && dtFechaReciboCRPaso !== '')
    				lstDistriEntregaFinal[strVin].dtFechaRecibo = null;
    			if (lstDistriEntregaFinal[strVin].bolSeleccionar && dtFechaReciboCRPaso !== null && dtFechaReciboCRPaso !== '')
    				lstDistriEntregaFinal[strVin].dtFechaRecibo = dtFechaReciboCRPaso;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    	}//Fin del for para lstDistriEntrega
    	
    	//Actualiza la lista final
    	objComponent.set("v.lstDistriEntrega", lstDistriEntregaFinal);
    },

    /** hpSeleTodosFECR */
    hpSeleTodosFECR: function(objComponent, Event) {
	    console.log('EN hpSeleTodosFECR...: ');

        let dtFechaReciboCRPaso = objComponent.get("v.dtFechaReciboCR");	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    	let lstDistriEntregaSelFinal = [];
	    console.log('EN hpSeleTodosFECR dtFechaReciboCRPaso: ', dtFechaReciboCRPaso);
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){
    		//Si tiene un VIN y tiene algo la fecha dtFechaReciboCRPaso
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '' && dtFechaReciboCRPaso !== null && dtFechaReciboCRPaso !== ''){
    			console.log('EN hpSeleTodosFECR dtFechaReciboCRPaso VAL 1 dtFechaRecibo: ', lstDistriEntregaFinal[strVin].dtFechaRecibo);
    			if (lstDistriEntregaFinal[strVin].bolSeleccionar && (lstDistriEntregaFinal[strVin].dtFechaRecibo === undefined || lstDistriEntregaFinal[strVin].dtFechaRecibo === null))
    				lstDistriEntregaFinal[strVin].dtFechaRecibo = dtFechaReciboCRPaso;
    			if (!lstDistriEntregaFinal[strVin].bolSeleccionar)
    				lstDistriEntregaFinal[strVin].dtFechaRecibo = null;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    		//Si tiene un VIN y esta vacia la fecha dtFechaReciboCRPaso	
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '' && (dtFechaReciboCRPaso === null || dtFechaReciboCRPaso === '')){
    			if (!lstDistriEntregaFinal[strVin].bolSeleccionar)
    				lstDistriEntregaFinal[strVin].dtFechaRecibo = null;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    	}//Fin del for para lstDistriEntrega
    	
    	//Actualiza la lista final
    	objComponent.set("v.lstDistriEntrega", lstDistriEntregaFinal);
    },

    /** Guardado de sección */
    hpLipiaFilial : function(Component, Event){
    	console.log("EN Helper.hpLipiaFilial...");    
        let indexPaso = Event.getSource().get("v.text");
        let srtValorPaso = Event.getSource().get("v.value");
        let strFilialPaso = Component.get("v.strFilial");
        let lista = Component.get("v.lstDistriEntrega");
        let objSelecPaso = lista[parseInt(indexPaso)];

    	console.log("EN Helper.hpLipiaFilial indexPaso: " + indexPaso);
    	console.log("EN Helper.hpLipiaFilial srtValorPaso: " + srtValorPaso);
    	console.log("EN Helper.hpLipiaFilial strFilialPaso: " + strFilialPaso);
    	console.log("EN Helper.hpLipiaFilial objSelecPaso: " + objSelecPaso);

        //Actualiza el campo de fecha de recibo y limpiala
        if (srtValorPaso && strFilialPaso !== '--- FILIAL ---')
        	objSelecPaso.strNomFilGpoCorp = strFilialPaso;        	
        //Actualiza el campo de fecha de recibo y limpiala
        if (srtValorPaso && strFilialPaso == '--- FILIAL ---')
        	objSelecPaso.strNomFilGpoCorp = null;        	
        if (!srtValorPaso)
        	objSelecPaso.strNomFilGpoCorp = null;        	
        Component.set("v.lstDistriEntrega",lista);
    	console.log("EN Helper.hpLipiaFilial objSelecPaso: ", objSelecPaso);
    },

    /** hpSeleTodosCE */
    hpSeleTodosCE: function(objComponent, Event) {
	    console.log('EN hpSeleTodosCE...: ');

        let blnSelectTodosCE = Event.getSource().get("v.value");
        let strFilialPaso = objComponent.get("v.strFilial");	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");    	    	
    	let lstDistriEntregaSelFinal = [];
    	
	    console.log('EN hpSeleTodosCE strFilialPaso: ', strFilialPaso);
	    console.log('EN hpSeleTodosCE blnSelectTodosCE: ', blnSelectTodosCE);
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '' && !lstDistriEntregaFinal[strVin].bolConfirmado){
    			lstDistriEntregaFinal[strVin].blnConfirmaFactura = blnSelectTodosCE;
    			if (!lstDistriEntregaFinal[strVin].blnConfirmaFactura && strFilialPaso !== null && strFilialPaso !== '')
    				lstDistriEntregaFinal[strVin].strNomFilGpoCorp = null;
    			if (lstDistriEntregaFinal[strVin].blnConfirmaFactura && strFilialPaso !== null && strFilialPaso !== '')
    				lstDistriEntregaFinal[strVin].strNomFilGpoCorp = strFilialPaso;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    	}//Fin del for para lstDistriEntrega
    	
    	//Actualiza la lista final
    	objComponent.set("v.lstDistriEntrega", lstDistriEntregaFinal);
    },

    /** hpSeleTodosFECE */
    hpSeleTodosFECE: function(objComponent, Event) {
	    console.log('EN hpSeleTodosFECE...: ');

        let dtFechaEntregaCEPaso = objComponent.get("v.dtFechaEntregaCE");	   
    	let lstDistriEntregaFinal = objComponent.get("v.lstDistriEntrega");
    	let lstDistriEntregaSelFinal = [];
	    console.log('EN hpSeleTodosFECR dtFechaEntregaCEPaso: ', dtFechaEntregaCEPaso);
    	
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntregaFinal){
    		//Si tiene un VIN y tiene algo la fecha dtFechaEntregaCEPaso
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '' 
    			&& dtFechaEntregaCEPaso !== null && dtFechaEntregaCEPaso !== '' && !lstDistriEntregaFinal[strVin].bolConfirmado){
    			console.log('EN hpSeleTodosFECR dtFechaEntregaCEPaso VAL 2 dtFechaEntrega: ', lstDistriEntregaFinal[strVin].dtFechaEntrega);
    			if (lstDistriEntregaFinal[strVin].bolConfirmaEntrega && (lstDistriEntregaFinal[strVin].dtFechaEntrega === undefined 
    				|| lstDistriEntregaFinal[strVin].dtFechaEntrega === null))
    				lstDistriEntregaFinal[strVin].dtFechaEntrega = dtFechaEntregaCEPaso;
    			if (!lstDistriEntregaFinal[strVin].bolConfirmaEntrega)
    				lstDistriEntregaFinal[strVin].dtFechaEntrega = null;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    		//Si tiene un VIN y esta vacia la fecha dtFechaEntregaCEPaso	
    		if (lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== '' 
    			&& (dtFechaEntregaCEPaso === null || dtFechaEntregaCEPaso === '') && !lstDistriEntregaFinal[strVin].bolConfirmado){
    			if (!lstDistriEntregaFinal[strVin].bolConfirmaEntrega)
    				lstDistriEntregaFinal[strVin].dtFechaEntrega = null;
    		}//Fin si lstDistriEntregaFinal[strVin].strVin !== null && lstDistriEntregaFinal[strVin].strVin !== ''
    	}//Fin del for para lstDistriEntrega
    	
    	//Actualiza la lista final
    	objComponent.set("v.lstDistriEntrega", lstDistriEntregaFinal);
    },

     /** Guardado de sección */
    hpLipiaFechaEntregaFECE : function(Component, Event){
    	console.log("EN Helper.hpLipiaFechaEntregaFECE...");    
        let indexPaso = Event.getSource().get("v.text");
        let srtValorPaso = Event.getSource().get("v.value");
    	console.log("EN Helper.hpLipiaFechaEntregaFECE indexPaso: " + indexPaso);
    	console.log("EN Helper.hpLipiaFechaEntregaFECE srtValorPaso: " + srtValorPaso);
        let dtFechaEntregaCEPaso = Component.get("v.dtFechaEntregaCE");
        let lista = Component.get("v.lstDistriEntrega");
        let objSelecPaso = lista[parseInt(indexPaso)];
        //Actualiza el campo de fecha de recibo y limpiala
        if (srtValorPaso)
        	objSelecPaso.dtFechaEntrega = dtFechaEntregaCEPaso;        	
        if (!srtValorPaso)
        	objSelecPaso.dtFechaEntrega = null;        	
        Component.set("v.lstDistriEntrega",lista);
    	console.log("EN Helper.hpLipiaFechaEntregaFECE objSelecPaso: ", objSelecPaso);
    },


    
    
    /** Toast Error */
    showToastWarning: function(Component, Event, strMensaje, strType) {
    	let toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": strType,
    		"message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
    		"duration": 2000,
    		"type": strType
    	});
    	toastEvent.fire();
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
    
})