({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit..");
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit objModeloSelVinExcep001: ", Component.get("v.objModeloSelVinExcep") );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit objModeloSelExcepCte002: ", Component.get("v.objModeloSelExcepCte") );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit recordId: ", Component.get("v.recordId"));
		Component.set('v.recordId', Component.get("v.recordId"));
    	Component.set("v.blnShowVinesConfirma", true);
    	Component.set("v.blnBloquearVines", false);
    	Component.set("v.blnSelectTodosCE", false);

		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl lModelosSeleccionados.length: ", Component.get("v.objModeloSelExcepCte").lModelosSeleccionados.length );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl lModelosSeleccionadosVin.length: ", Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin.length );
    	
		//Llama la función de getDistribEntrega
		if ( Component.get("v.objModeloSelExcepCte").lModelosSeleccionados.length > 0 ){
			Helper.getDistribEntrega(Component, Event);
		    //Helper.hpGetFilialesGpoCorp(Component);
		}//Fin si  Component.get("v.objModeloSelExcepCte").lModelosSeleccionados.length > 0 
		if ( Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin.length > 0 ){
			Helper.getDistribEntregaInv(Component, Event);		
		    //Helper.hpGetFilialesGpoCorp(Component);
		}//Fin si  Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin.length > 0 

    },

	//La función SeleccionaMesFechaCierre
    jsSelecFilialGpoCorp : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecFilialGpoCorp(objComponent, objEvent);
    },

    //Evento para el componente de busqueda
    keyPressController : function(component, event, helper) {  
		console.log("EN Contoller.keyPressController...");    
        let getInputkeyWord = component.get("v.strNomFilGpoCorpBusq");
		console.log("EN Contoller.keyPressController getInputkeyWord: ", getInputkeyWord);
        let srtValorPaso = event.getSource().get("v.value");
		console.log("EN Contoller.keyPressController srtValorPaso: ", srtValorPaso);        
        
        if( getInputkeyWord.length > 0 ){
            let forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }else{  
            component.set("v.listFilialesGpoCorpBusq", null ); 
            let forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },

    //Eevento para el componente de busqueda
    handleComponentEvent : function(component, event, helper) {
		console.log("EN handleComponentEvent selectedAccountGetFromEvent...");
        
        let selectedAccountGetFromEvent = event.getParam("filialByEvent");
        component.set("v.objFilialesGpoCorpParent" , selectedAccountGetFromEvent); 
		console.log("EN handleComponentEvent selectedAccountGetFromEvent: ", selectedAccountGetFromEvent.sNombreFilial);
        component.set("v.strFilial" , selectedAccountGetFromEvent.sNombreFilial); 
        component.set("v.strNomFilGpoCorpBusq", null); 
        component.set("v.listFilialesGpoCorpBusq", null); 
                
        let forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        /*let lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');  */
        
    },


   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

   /** Funcion guardar */
	jsGuardar : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.cancelar..");
		Helper.hpGuardar(Component, Event);
    },

   /** Funcion Inicial */
	desabilitaEstatus : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.desabilitaEstatus..");
		Helper.desabilitaEstatus(Component, Event);
    },

   /** Funcion Inicial */
	bloqueaCampos : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.bloqueaCampos..");
		Helper.bloqueaCampos(Component, Event);
    },

   /** ocSeleFact */
	ocSeleFact : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocSeleFact..");
		Helper.hpSeleFact(Component, Event);
    },

   /** ocSeleFact */
	ocSeleTodos : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodos..");
		Helper.hpSeleTodos(Component, Event);
    },



   /** ocSeleTodosCR */
	ocSeleTodosCR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosCR..");
		Helper.hpSeleTodosCR(Component, Event);
    },

   /** ocSeleTodosFECR */
	ocSeleTodosFECR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosFECR..");
		Helper.hpSeleTodosFECR(Component, Event);
    },

   /** ocLipiaFilial */
	ocLipiaFilial : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocLipiaFilial..");
		Helper.hpLipiaFilial(Component, Event);
    },


   /** ocSeleTodosCE */
	ocSeleTodosCE : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosCE..");
		Helper.hpSeleTodosCE(Component, Event);
    },

   /** ocSeleTodosFECE */
	ocSeleTodosFECE : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosFECE..");
		Helper.hpSeleTodosFECE(Component, Event);
    },

   /** ocLipiaFechaReciboFECE */
	ocLipiaFechaReciboFECE : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocLipiaFechaReciboFECE..");
		Helper.hpLipiaFechaEntregaFECE(Component, Event);
    },



   /** ocEnviaCorreoNotificacion */
    ocEnviaCorreoNotificacion : function(objComponent, objEvent, objHelper) {
    	//Manda llamarl al metodo de guardar y y despues cierra la ventana
        objHelper.hpEnviaCorreoNotificacion(objComponent, objEvent);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

    
})