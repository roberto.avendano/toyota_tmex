({
    searchHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.vinNotaCredito");
        var userId = $A.get("$SObjectType.CurrentUser.Id"); 
        var selectedRecordOriginal = component.get("v.selectedRecordOriginal");
        console.log('userId: ' + userId);
        console.log('searchKeyWord: ' + getInputkeyWord);
        action.setParams({
            'searchKeyWord': getInputkeyWord,
             userId 		: userId,
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'Nota de Crédito no encontrada');
                } else {
                    component.set("v.Message", 'Resultado de la búsqueda');
                }
                
                component.set("v.listOfSearchRecords", storeResponse);
            } else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            
            
        });
        
        $A.enqueueAction(action);
        
    },
})