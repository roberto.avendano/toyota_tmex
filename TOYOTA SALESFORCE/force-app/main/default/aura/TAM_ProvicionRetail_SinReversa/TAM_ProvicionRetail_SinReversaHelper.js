({
    /** Función Inicial */
    initializeComponent : function(objComponent, objEvent){
        objComponent.set('v.loaded', true);
        var objAction = objComponent.get("c.getRetailSinReversa");
        objAction.setParams({ recordId : objComponent.get("v.recordId") });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0){
                    lstResult.sort();
                    objComponent.set('v.lstRetail_SinReversa', lstResult);
                    //objComponent.set('v.lstRetail_SinReversa_AUX', lstResult);

                    /**Paginación */
                    var pageList = [];
                    lstResult.forEach(function(objResult){
                        pageList.push(objResult);
                    });
                    var pageSize = objComponent.get("v.pageSize");
                    var totalRecordsList = pageList;
                    var totalLenght = totalRecordsList.length;

                    objComponent.set("v.totalRecordsCount", totalLenght);
                    objComponent.set("v.startPage", 0);
                    objComponent.set("v.endPage", pageSize - 1);

                    var paginationList = [];
                    for(var i=0; i<pageSize; i++){
                        if(pageList.length > i){
                            paginationList.push(pageList[i]);
                        }
                    }
                    objComponent.set("v.lstRetail_SinReversa_AUX", paginationList);
                    objComponent.set("v.selectedCount", 0);
                    objComponent.set("v.totalPagesCount", Math.ceil(totalLenght / pageSize));
                    
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', true);
                } else {
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', false);
                } 
            }
        });
        $A.enqueueAction(objAction);
    },

    /* Buscador de VIN */
    SearchHelper: function(objComponent, objEvent) {
       
        var SearchKey = objComponent.get("v.SearchKey");
        var lstRetailSinReversa = objComponent.get("v.lstRetail_SinReversa");
        var PagList = [];
        lstRetailSinReversa.forEach(function(objSinReversa) {
            var strVINRecord = objSinReversa.TAM_VIN__r.Name;
            if(strVINRecord.includes(SearchKey)){
                PagList.push(objSinReversa);
            }
        });
        
        objComponent.set('v.lstRetail_SinReversa_AUX', PagList);
    },

     /* Buscador de Dealer */
     SearchHelper_Dealer: function(objComponent, objEvent) {
        var SearchKey = objComponent.get("v.search_dealer");
        var lstRetailSinReversa = objComponent.get("v.lstRetail_SinReversa");
        var PagList = [];
        lstRetailSinReversa.forEach(function(objSinReversa) {
            var strRecord = objSinReversa.TAM_CodigoDealer__c;
            if(strRecord.includes(SearchKey)){
                PagList.push(objSinReversa);
            }
        });
        
        objComponent.set('v.lstRetail_SinReversa_AUX', PagList);
    },
    
    /*Guardar Provisión*/
    SaveList : function(objComponent, objEvent){
        objComponent.set('v.loaded2', true);
        var lstRetailSinReversa = objComponent.get('v.lstRetail_SinReversa');
        var objAction = objComponent.get("c.setRecordsRetail");
        objAction.setParams(
        		{   			
                    lstDetalleProvision : lstRetailSinReversa,
                    recordId : objComponent.get("v.recordId")
        		}
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var boolResult = objResponse.getReturnValue();
                if(boolResult){
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);
    },

    /*cerrar Provisión*/
    cerrarProvision : function(objComponent, objEvent){
        objComponent.set('v.loaded2', true);
        var lstRetailSinReversa = objComponent.get('v.lstRetail_SinReversa');
        var objAction = objComponent.get("c.cerrarProvision");
        objAction.setParams(
                {   			
                    lstDetalleProvision : lstRetailSinReversa,
                    recordId : objComponent.get("v.recordId")
                }
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var boolResult = objResponse.getReturnValue();
                if(boolResult){
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastSuccess_Cerrar(objComponent, objEvent);
                } else {
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastError_Cerrar(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);
    },
    
    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Provisión de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Toast Success */
    showToastSuccess_Cerrar : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Provisión de Incentivos Cerrada Correctamente.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError_Cerrar : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el cierre de provisión, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Navegar a la sig pagina */
    next : function(objComponent, objEvent, sObjectList, end, start, pageSize){
        var sObjectList = objComponent.get("v.lstRetail_SinReversa");
        var sObjectLis_AUX = objComponent.get("v.lstRetail_SinReversa_AUX");
        var end = objComponent.get("v.endPage");
        var start = objComponent.get("v.startPage");
        var pageSize = objComponent.get("v.pageSize");
        var pageList=[];
        var counter = 0;
        for(var i = end + 1; i < end + pageSize + 1; i++){
            if(sObjectList.length > i){
                pageList.push(sObjectList[i]);
            }
            counter ++;
        }
        start = start + counter;
        end = end + counter;
        objComponent.set("v.startPage", start);
        objComponent.set("v.endPage", end);
        objComponent.set("v.lstRetail_SinReversa_AUX", pageList);
        
    },

    /** Navegar a la  pagina anterior*/
    previous : function(objComponent, objEvent, sObjectList, end, start, pageSize){
        var sObjectList = objComponent.get("v.lstRetail_SinReversa");
        var sObjectLis_AUX = objComponent.get("v.lstRetail_SinReversa_AUX");
        var end = objComponent.get("v.endPage");
        var start = objComponent.get("v.startPage");
        var pageSize = objComponent.get("v.pageSize");
        var pageList=[];
        var counter = 0;
        var pageListAll_AUX = objComponent.get("v.lstRetail_SinReversa"); // = sObjectList.splice(start,pageSize);
        var n = start;
        sObjectLis_AUX.forEach(function(objResult){
            pageListAll_AUX.splice(n, 1, objResult);
            n++;
        });
        objComponent.set("v.lstRetail_SinReversa", pageListAll_AUX);
        for(var i = start - pageSize; i < start ; i++){
            if(i > -1){
                pageList.push(sObjectList[i]);
                counter ++;
            } else{
                start ++;
            }
        }
        start = start - counter;
        end = end - counter;
        objComponent.set("v.startPage", start);
        objComponent.set("v.endPage", end);
        objComponent.set("v.lstRetail_SinReversa_AUX", pageList);
    },
    
})