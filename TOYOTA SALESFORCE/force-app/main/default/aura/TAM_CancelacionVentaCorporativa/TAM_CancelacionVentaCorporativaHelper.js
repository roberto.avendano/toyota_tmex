({
    
    validarGrupo : function(component, event,helper) {
    	var recordIdSolicitud = component.get("v.recordId");
    	var userId = $A.get("$SObjectType.CurrentUser.Id");  
        var action =    component.get("c.checkPermissionsUser"); 

        action.setParams({ 
            userId : userId,
            recordIdSolicitud : recordIdSolicitud
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.activarFuncionalidad',result);
                
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    tipoUsuario : function(component, event,helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");    
        var action =    component.get("c.getCurrentUser"); 
        
        action.setParams({ 
            userId : userId
        });
        
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.wrapperUsuario',result);
                var usuarioInfo = component.get("v.wrapperUsuario");
                //Si el usuario es Edoardo  +
                if(usuarioInfo.nivelAprobacion == 'Aprobador DTM (nivel 1)'){
                    component.set("v.comentarioEdu",false);
                    component.set("v.comentarioLuc",true);
                }
                //Si es usuario es Lucio  
                if(usuarioInfo.nivelAprobacion == 'Aprobador DTM (Nivel 2)'){
                    component.set("v.comentarioEdu",true);
                    component.set("v.comentarioLuc",false);
                }
                
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    datosDeSolicitud : function(component, event,helper) {
        let recordIdSolicitud = component.get("v.recordId");
        let action = component.get("c.getSolicitud");
        
        action.setParams(
            {  
                recordIdSolicitud : recordIdSolicitud
            }
        );
        
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                component.set("v.Solicitud", objSolicitud);
                let nombreCliente = objSolicitud.TAM_Cliente__r.Name;
                let nombreClienteLong = nombreCliente.length;
                let nombreClienteFinalCorto = nombreCliente.substring(0,35);
                let nombreClienteFinalLargo = nombreCliente.substring(0,nombreClienteLong);
                component.set("v.sNombreClienteCorto", nombreClienteFinalCorto);
                component.set("v.sNombreClienteLargo", nombreClienteFinalLargo);
                if (objSolicitud.TAM_Estatus__c == 'Cerrada'){
                    component.set("v.bSolCerrada", true);
                    component.set("v.bEnviarCorreo", false);	     
                }
                console.log('EN obtSolicitudCompra bSolCerrada: ', component.get("v.bSolCerrada"));
            }
        });
        $A.enqueueAction(action);  
        
    },    
    
    getSolPedidoEspecial: function(component, event,helper) { 
        let recordIdSolicitud = component.get("v.recordId");
        let action = component.get("c.getRegistrosPedidoEspecial");
        
        action.setParams(
            {  
                recordIdSolicitud : recordIdSolicitud
            }
        );
        
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                component.set("v.lstData", objSolicitud);	     
                
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    
    cancelarVentaHelper: function(component, event,helper) { 
        let motivoCancelacion = component.get("v.comentario");
        let datosValidos;
        let recordId = component.get("v.recordId");
        let lstDataAux = component.get("v.lstDataAux");
        let comentario  =  component.get("v.comentario");
        let solicitud  = component.get("v.Solicitud");
        let correoUsuario = component.get("v.wrapperUsuario.correoAprobador");
        
        if(motivoCancelacion == undefined || motivoCancelacion == ''){
            component.set("v.mensajeCancelacion",true);
            datosValidos == false;            
        }else{
            datosValidos = true;
        }
        
        //Llamadas a metodos APEX
        if(datosValidos === true){
            $A.createComponent("c:TAM_ConfirmarCancelacionVC", {
                recordId            : recordId,
                lstDataAux          : lstDataAux,
                comentario			: comentario,
                solicitud           : solicitud,
                correoUsuario      : correoUsuario
            },
                               function(content, status) {
                                   if (status === "SUCCESS") {
                                       var modalBody = content;
                                       component.find('overlayLib1').showCustomModal({
                                           header: "Confirmar ",
                                           body: modalBody, 
                                           showCloseButton: false,
                                           closeCallback: function(ovl) {
                                               console.log('Overlay is closing');
                                           }
                                       }).then(function(overlay){
                                           console.log("Overlay is made");
                                       });
                                   }
                               });
            
        }
        
        
    },
    
    checkRegistro : function(component, event,helper) {
        var listRegMarcados =  component.get("v.lstDataAux");
        var registroCheck = event.getSource().get("v.labelClass");
        var statusCheck = event.getSource().get("v.value");
        
        
        if(statusCheck == true){
            listRegMarcados.push(registroCheck);
            component.set('v.regCheck',true);
            
        }
        if(statusCheck == false){
            component.set('v.regCheck',false);
            var index = listRegMarcados.indexOf(registroCheck);
            listRegMarcados.splice(index, 1);
            component.set('v.lstDataAux',listRegMarcados);
            
        }
        var listaAux = component.get("v.lstDataAux");
        component.set('v.lstDataAuxFilter',listaAux);
        
    },
    
    guardarDTM : function(component,event,helper) {  
        var tipoUsuario;
        var usuarioInfo = component.get("v.wrapperUsuario");
        //Si el usuario es Edoardo  
        if(usuarioInfo.nivelAprobacion == 'Aprobador DTM (nivel 1)'){
            tipoUsuario = true;
            
        }
        //Si es usuario es Lucio  
        if(usuarioInfo.nivelAprobacion === 'Aprobador DTM (Nivel 2)'){
            tipoUsuario = false;
        }
        
        var recordIdSolicitud = component.get("v.recordId");
        var comentarioDTM1 =  component.get("v.Solicitud.TAM_ComentarioDTM1__c");
        var comentarioDTM2 =  component.get("v.Solicitud.TAM_ComentarioDTM2__c");
        var aprobacionDTM1  =  component.get("v.aprobacionDTM1");
        var aprobacionDTM2  =  component.get("v.aprobacionDTM2");
        var registrosCancelar =  component.get("v.lstData");
        
        var action = component.get("c.guardarAprobacionDTM");
        
        action.setParams(
            {  
                tipoUsuario : tipoUsuario,
                recordIdSolicitud : recordIdSolicitud,
                comentarioDTM1   : comentarioDTM1,
                comentarioDTM2   : comentarioDTM2,
                aprobacionDTM1   : aprobacionDTM1,
                aprobacionDTM2   : aprobacionDTM2,
                registrosCancelar : registrosCancelar
                
            }
        );
        
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                this.showToast(component, event, helper);
                //Se cierre el modal
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
        });
        $A.enqueueAction(action);
        
        
        
    },
    
    aprobacionDTM1 : function(component,event,helper) {  
        let aprobacionDTM1 =  component.find("aprobacionDTM1").get("v.value");
        component.set("v.aprobacionDTM1",aprobacionDTM1);
    },
    
    aprobacionDTM2 : function(component,event,helper) {  
        let aprobacionDTM2 =  component.find("aprobacionDTM2").get("v.value");
        component.set("v.aprobacionDTM2",aprobacionDTM2);
    },
    
    
    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso!",
            "message": "La solicitud de cancelación aprobo correctamente."
        });
        toastEvent.fire();
    }
    
})