({
    
    doInit : function(component, event, helper) {
        helper.validarGrupo(component,event,helper);
        helper.tipoUsuario(component,event,helper);
        helper.datosDeSolicitud(component,event,helper);
        helper.getSolPedidoEspecial(component,event,helper);
    },
    
    guardarDTM : function(component, event, helper) {
        helper.guardarDTM(component,event,helper);
        
    },
    
    aprobacionDTM1 : function(component, event, helper) {
        helper.aprobacionDTM1(component,event,helper);
        
    },
    
    aprobacionDTM2 : function(component, event, helper) {
        helper.aprobacionDTM2(component,event,helper);
        
    },
    
    onCheck : function(component, event, helper) {
        helper.checkRegistro(component,event,helper);
    },
    
    cancelarVenta : function(component, event, helper) {
        helper.cancelarVentaHelper(component,event,helper);
    },
    
    CloseWindow : function(objComponent, objEvent, objHelper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    }
    
})