({
    typeDevice : function(component, event, helper) {
        var deviceMobile = $A.get("$Browser.isPhone");
        if(deviceMobile === true)
            component.set("v.isMobile",true);   
    },
})