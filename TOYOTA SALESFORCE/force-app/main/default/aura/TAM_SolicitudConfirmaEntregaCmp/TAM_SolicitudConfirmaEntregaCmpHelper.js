({
    // Funcion de inicializado del componente
    initializeComponent : function(objComponent, objEvent) {
        console.log('EN initializeComponent...');
        objComponent.set("v.sTipoExcepcion",'--- NINGUNO ---');
        objComponent.set("v.sMesFechaCierre",'--- NINGUNO ---');
        objComponent.set("v.blnShowDatosFotProg",false);
        objComponent.set("v.blnShowDatosRetail",false);
        objComponent.set("v.blnShowConfirmaReciboFlotProg", false);
        objComponent.set("v.blnNoRegistrosSolicitud", false);
        //Manda llamar la funcion de SeleccionaTipoExcepcion
        this.ActualizaMeses(objComponent, objEvent);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    ActualizaMeses: function(objComponent, objEvent) {
    	console.log('EN Save ActualizaMeses...');
       	//Llama a la función de getMesesSolicitud 	
       	this.getMesesSolicitud(objComponent);
       	//Llama a la función de getMesesSolicitud 	
       	this.getMapMesesSolicitud(objComponent);        
    },
       
    //Funcion que sirve para seleccionar el tipo de Excepción
    SeleccionaTipoExcepcion: function(objComponent, objEvent) {
    	console.log('EN Save SeleccionaTipoExcepcion...');
    	objComponent.set("v.blnNoRegistrosSolicitud", false);
    	objComponent.set("v.blnShowDatosRetail", false);
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sTipoExcepcion",value);
        console.log('EN Save SeleccionaTipoExcepcion sTipoExcepcion: ', objComponent.get("v.sTipoExcepcion"));
        if (value == 'Flotilla / Programa'){
	       	//Llama a la función de getMesesSolicitud 	
	       	this.getMesesSolicitud(objComponent);
	       	//Llama a la función de getMesesSolicitud 	
	       	this.getMapMesesSolicitud(objComponent);        
        }//Fin si value == 'Flotilla / Programa'
        if (value == 'Retail'){
        	let listMesesSolPaso = [];
        	objComponent.set("v.listMesesSolicitd", listMesesSolPaso);
        }//Fin si value == 'Flotilla / Programa'
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SeleccionaMesFechaCierre: function(objComponent, objEvent) {
    	console.log('EN Save SeleccionaMesFechaCierre...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sMesFechaCierre",value);
        console.log('EN Save SeleccionaMesFechaCierre sMesFechaCierre: ', objComponent.get("v.sMesFechaCierre"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    consultaDatosSolicitudes: function(objComponent, objEvent) {
    	console.log('EN Save consultaDatosSolicitudes...');

        //let sTipoExcepcion = objComponent.get("v.sTipoExcepcion");    	
        let sTipoExcepcion = 'Flotilla / Programa';
        let sMesFechaCierre = objComponent.get("v.sMesFechaCierre");
        let strFolioSolicitud = objComponent.get("v.strFolioSolicitud");
        let strNombreClienteSolicitud = objComponent.get("v.strNombreClienteSolicitud");
        console.log('EN Save consultaDatosSolicitudes sTipoExcepcion: ', sTipoExcepcion);
        console.log('EN Save consultaDatosSolicitudes sMesFechaCierre: ', sMesFechaCierre);
        console.log('EN Save consultaDatosSolicitudes strFolioSolicitud: ', strFolioSolicitud);
        console.log('EN Save consultaDatosSolicitudes strNombreClienteSolicitud: ', strNombreClienteSolicitud);
        
        //No a selecionado ningun valor en los campos
        if (sTipoExcepcion == '--- NINGUNO ---' && sMesFechaCierre == '--- NINGUNO ---'){
            this.showToastWarning(objComponent, objEvent,'Favor de seleccionar un tipo y una Fecha de Alta.');
            return;
        }//Fin si sTipoExcepcion == '--- NINGUNO ---' && sMesFechaCierre == '--- NINGUNO ---'
        
        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---' && strFolioSolicitud == '' 
        	&& strNombreClienteSolicitud == ''){
            this.showToastWarning(objComponent, objEvent,'Favor de seleccionar algun parámetro de búsqueda.');
            return;
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'

        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre != '--- NINGUNO ---' && strFolioSolicitud != ''){
            this.showToastWarning(objComponent, objEvent,'Solo puede seleccionar un parámetro de búsqueda.');
            return;
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'

        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre != '--- NINGUNO ---' && strNombreClienteSolicitud != ''){
            this.showToastWarning(objComponent, objEvent,'Solo puede seleccionar un parámetro de búsqueda.');
            return;
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'

        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Flotilla / Programa' && strFolioSolicitud != '' && strNombreClienteSolicitud != '' ){
            this.showToastWarning(objComponent, objEvent,'Solo puede seleccionar un parámetro de búsqueda.');
            return;
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'

        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Flotilla / Programa' && (sMesFechaCierre != '--- NINGUNO ---' || strFolioSolicitud != ''
        	|| strNombreClienteSolicitud != '') ){
        	
        	let strTipoconsulta = '';
        	let strValor = '';
        	if (sMesFechaCierre != '--- NINGUNO ---'){
        		strTipoconsulta = 'FechaCierre';
        		strValor = sMesFechaCierre;
        	}
        	if (strFolioSolicitud != ''){
        		strTipoconsulta = 'FolioSolicutd';
        		strValor = strFolioSolicitud;        	
        	}
        	if (strNombreClienteSolicitud != ''){
        		strTipoconsulta = 'NombreClienteSolicitud';
        		strValor = strNombreClienteSolicitud;        	
        	}
        	//Llama a la funcion consultaDatosFlotillaPrograma con los parametros 
            this.consultaDatosFlotillaPrograma(objComponent, objEvent, strTipoconsulta, strValor);
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'

        //Ya selecciono un valor en sTipoExcepcion
        if (sTipoExcepcion == 'Retail'){
            this.consultaRetail(objComponent, objEvent);
        }//Fin si sTipoExcepcion == 'Flotilla / Programa' && sMesFechaCierre == '--- NINGUNO ---'
                
    },

    //Obtener lista de distribuidores disponibles.
    consultaDatosFlotillaPrograma : function(objComponent, objEvent, strTipoconsulta, strValor){
    	console.log('EN consultaDatosFlotillaPrograma...');

        let mapMesesSolicitud = objComponent.get("v.mapMesesSolicitud");         
        let objAction = objComponent.get("c.getSolicitudesFlotillaPrograma");
        objAction.setParams(
        	{  
        		"strTipoconsulta" : strTipoconsulta,
        		"strValor" : strValor,
        		"mapMesesSolicitud" : objComponent.get("v.mapMesesSolicitud")
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstSolFlotPrograMod = objResponse.getReturnValue();
                console.log('EN consultaDatosFlotillaPrograma lstSolFlotPrograMod: ', lstSolFlotPrograMod);
                if(lstSolFlotPrograMod.length > 0){
                	//Inciailiza las variables
                	objComponent.set("v.lstWrpSolFlotProg",lstSolFlotPrograMod);
                	objComponent.set("v.blnShowDatosFotProg",true);
                	objComponent.set("v.blnShowDatosRetail",false);
                	objComponent.set("v.blnNoRegistrosSolicitud",false);                	
                }//Fin si lstSolFlotPrograMod != null
                if(lstSolFlotPrograMod.length == 0){
                	let lstWrpSolFlotProgPaso = [];
                	objComponent.set("v.lstWrpSolFlotProg",lstWrpSolFlotProgPaso);                	
                	objComponent.set("v.blnNoRegistrosSolicitud", true);
                }//Fin si lstSolFlotPrograMod.length == 0
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    },

    //Obtener lista de distribuidores disponibles.
    getMesesSolicitud : function(objComponent){
    	console.log('EN Save getMesesSolicitud...');
        let objAction = objComponent.get("c.getMesesSolicitud");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let listMesesResult = objResponse.getReturnValue();
                if(listMesesResult != null){
                    objComponent.set("v.listMesesSolicitd",listMesesResult);    
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
		$A.enqueueAction(objAction);
    },

    //Obtener lista de distribuidores disponibles.
    getMapMesesSolicitud : function(objComponent){
    	console.log('EN Save getMapMesesSolicitud...');
        let objAction = objComponent.get("c.getMapMesesSolicitud");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let mapMesesSolicitudResult = objResponse.getReturnValue();
                if(mapMesesSolicitudResult != null){
                    objComponent.set("v.mapMesesSolicitud", mapMesesSolicitudResult);    
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
		$A.enqueueAction(objAction);
    },

    //Obtener lista de distribuidores disponibles.
    consultaRetail : function(objComponent, objEvent){
    	console.log('EN Save consultaRetail...');
        objComponent.set("v.blnShowDatosRetail",true);
        objComponent.set("v.blnShowDatosFotProg",false);
    },
    	
    /** Toast Error */
    showToastWarning: function(Component, Event, strMensaje) {
    	let toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": "Warning.",
    		"message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
    		"duration": 3000,
    		"type": "warning"
    	});
    	toastEvent.fire();
    },

    showVinesDOD: function(objComponent, objEvent) {   
    	console.log("EN Helper.showVinesDOD..");

    	objComponent.set("v.blnShowVinesDOD", true);
        objComponent.set("v.blnShowExcepFlotProg", false);
        objComponent.set("v.blnShowConfirmaReciboFlotProg", false);
    	objComponent.set("v.blnShowFacturaOtroClienteFlotProg", false);        

        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelExcep = objEvent.getSource().get("v.name");
        console.log("EN Helper.showVinesDOD varObjModeloSelValue1: " + JSON.stringify( varObjModeloSelValue ) );
        console.log("EN Helper.showVinesDOD varObjModeloSelExcep1: " + JSON.stringify( varObjModeloSelExcep ) );
        
        let sDatosModelo = varObjModeloSelValue.strNombre;
        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
        let arrDatosModelo = sDatosModelo.split("-");
        let strAnioModelo = arrDatosModelo[2]; 
        let strModelo = arrDatosModelo[1]; 
        let strSerie = arrDatosModelo[0]; 
        let strCveColorExteriorCustom = arrDatosModelo[3];
        let strCveColorInteriorCustom = arrDatosModelo[4];
        
        let lstModelosPaso = objComponent.get("v.lstModelos");
        lstModelosPaso.push({
        	'sobjectType': 'TAM_OrdenDeCompraWrapperClass',
	        'strAnioModelo': strAnioModelo,
	        'strModelo': strModelo,
	        'strSerie': strSerie,
	        'strVersion': varObjModeloSelValue.strVersion,
	        'strCveColorExteriorCustom': strCveColorExteriorCustom,
	        'strColorExteriorCustom': varObjModeloSelValue.strDescripcionColorExterior,
	        'strCveColorInteriorCustom': strCveColorInteriorCustom,
	        'strColorInteriorCustom': varObjModeloSelValue.strDescripcionColorInterior,
	        'strIdCatModelos': varObjModeloSelValue.strIdCatCentrModelos,
	        'strCantidad': varObjModeloSelValue.strCantidadSolicitada,
	        'strIdRegistro': varObjModeloSelValue.strIdSolicitud
        });
        let sObjectModelosPaso = lstModelosPaso[0];
        console.log("EN Helper.showVinesDOD sObjectModelosPaso: ", sObjectModelosPaso);

        let strIdExterno = varObjModeloSelValue.strIdExterno;
        let objAction = objComponent.get("c.getWrpSolFlotProgFinal");
        objAction.setParams(
        	{  
        		"strIdExterno" : strIdExterno,
        		"varObjModeloSelExcep" : varObjModeloSelExcep
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objObjModeloSelExcepResult = objResponse.getReturnValue();
                if(objObjModeloSelExcepResult != null){                
			    	objComponent.set("v.objModeloSelExcep", objObjModeloSelExcepResult );    	
			    	objComponent.set("v.objModeloSel", sObjectModelosPaso);
			        objComponent.set("v.recordId", varObjModeloSelValue.strIdSolicitud);
			        objComponent.set("v.strIdCatCentMod", varObjModeloSelValue.strIdCatCentrModelos);
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
		$A.enqueueAction(objAction);
      	        
    },

    creaExcepcionFlotillaPrograma: function(objComponent, objEvent) {   
    	console.log("EN Helper.creaExcepcionFlotillaPrograma..");

    	objComponent.set("v.blnShowExcepFlotProg", true);
    	objComponent.set("v.blnShowVinesDOD", false);
        objComponent.set("v.blnShowConfirmaReciboFlotProg", false);

        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelExcep = objEvent.getSource().get("v.name");
        //console.log("EN Helper.creaExcepcionFlotillaPrograma varObjModeloSelValue1: " + JSON.stringify( varObjModeloSelValue ) );
        //console.log("EN Helper.creaExcepcionFlotillaPrograma varObjModeloSelExcep1: " + JSON.stringify( varObjModeloSelExcep ) );
        
        let sDatosModelo = varObjModeloSelValue.strNombre;
        //console.log("EN Helper.creaExcepcionFlotillaPrograma sDatosModelo2: " + sDatosModelo );
        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
        let arrDatosModelo = sDatosModelo.split("-");
        let strAnioModelo = arrDatosModelo[2];
        let strModelo = arrDatosModelo[1];
        let strSerie = arrDatosModelo[0];
        let strCveColorExteriorCustom = arrDatosModelo[3];
        let strCveColorInteriorCustom = arrDatosModelo[4];
        
        let lstModelosPaso = objComponent.get("v.lstModelos");
        lstModelosPaso.push({
        	'sobjectType': 'TAM_OrdenDeCompraWrapperClass',
	        'strAnioModelo': strAnioModelo,
	        'strModelo': strModelo,
	        'strSerie': strSerie,
	        'strVersion': varObjModeloSelValue.strVersion,
	        'strCveColorExteriorCustom': strCveColorExteriorCustom,
	        'strColorExteriorCustom': varObjModeloSelValue.strDescripcionColorExterior,
	        'strCveColorInteriorCustom': strCveColorInteriorCustom,
	        'strColorInteriorCustom': varObjModeloSelValue.strDescripcionColorInterior,
	        'strIdCatModelos': varObjModeloSelValue.strIdCatCentrModelos,
	        'strCantidad': varObjModeloSelValue.strCantidadSolicitada,
	        'strIdRegistro': varObjModeloSelValue.strIdSolicitud
        });
        let sObjectModelosPaso = lstModelosPaso[0];
        //console.log("EN Helper.creaExcepcionFlotillaPrograma sObjectModelosPaso: " + sObjectModelosPaso);
        
        let strIdExterno = varObjModeloSelValue.strIdExterno;
        let objAction = objComponent.get("c.getWrpSolFlotProgFinal");
        objAction.setParams(
        	{  
        		"strIdExterno" : strIdExterno,
        		"varObjModeloSelExcep" : varObjModeloSelExcep
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objObjModeloSelExcepResult = objResponse.getReturnValue();
                if(objObjModeloSelExcepResult != null){                
			    	objComponent.set("v.objModeloSelExcep", objObjModeloSelExcepResult );    	
			    	objComponent.set("v.objModeloSel", sObjectModelosPaso);
			        objComponent.set("v.recordId", varObjModeloSelValue.strIdSolicitud);
			        objComponent.set("v.strIdCatCentMod", varObjModeloSelValue.strIdCatCentrModelos);
			        //console.log("EN Helper.creaExcepcionFlotillaPrograma objObjModeloSelExcepResult: " + JSON.stringify( objComponent.get("v.objModeloSelExcep") ) );
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    	
    },

    confirmaReciboVines: function(objComponent, objEvent) {   
    	console.log("EN Helper.confirmaReciboVines..");

        objComponent.set("v.blnShowConfirmaReciboFlotProg", true);
    	objComponent.set("v.blnShowFacturaOtroClienteFlotProg", false);
    	objComponent.set("v.blnShowExcepFlotProg", false);
    	objComponent.set("v.blnShowVinesDOD", false);

        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelExcep = objEvent.getSource().get("v.name");
        //console.log("EN Helper.confirmaReciboVines varObjModeloSelValue1: " + JSON.stringify( varObjModeloSelValue ) );
        console.log("EN Helper.confirmaReciboVines varObjModeloSelExcep1: " + JSON.stringify( varObjModeloSelExcep ) );
        
        let sDatosModelo = varObjModeloSelValue.strNombre;
        //console.log("EN Helper.confirmaReciboVines sDatosModelo2: " + sDatosModelo );
        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
        let arrDatosModelo = sDatosModelo.split("-");
        let strAnioModelo = arrDatosModelo[2];
        let strModelo = arrDatosModelo[1];
        let strSerie = arrDatosModelo[0];
        let strCveColorExteriorCustom = arrDatosModelo[3];
        let strCveColorInteriorCustom = arrDatosModelo[4];
        
        let lstModelosPaso = objComponent.get("v.lstModelos");
        lstModelosPaso.push({
        	'sobjectType': 'TAM_OrdenDeCompraWrapperClass',
	        'strAnioModelo': strAnioModelo,
	        'strModelo': strModelo,
	        'strSerie': strSerie,
	        'strVersion': varObjModeloSelValue.strVersion,
	        'strCveColorExteriorCustom': strCveColorExteriorCustom,
	        'strColorExteriorCustom': varObjModeloSelValue.strDescripcionColorExterior,
	        'strCveColorInteriorCustom': strCveColorInteriorCustom,
	        'strColorInteriorCustom': varObjModeloSelValue.strDescripcionColorInterior,
	        'strIdCatModelos': varObjModeloSelValue.strIdCatCentrModelos,
	        'strCantidad': varObjModeloSelValue.strCantidadSolicitada,
	        'strIdRegistro': varObjModeloSelValue.strIdSolicitud
        });
        let sObjectModelosPaso = lstModelosPaso[0];
        
        let strIdExterno = varObjModeloSelValue.strIdExterno;
        let objAction = objComponent.get("c.getWrpSolFlotProgFinal");
        objAction.setParams(
        	{  
        		"strIdExterno" : strIdExterno,
        		"varObjModeloSelExcep" : varObjModeloSelExcep
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objObjModeloSelExcepResult = objResponse.getReturnValue();
                if(objObjModeloSelExcepResult != null){
                	console.log("EN Helper.confirmaReciboVines objObjModeloSelExcepResult: ", objObjModeloSelExcepResult);                
			    	objComponent.set("v.objModeloSelExcep", objObjModeloSelExcepResult );    	
			    	objComponent.set("v.objModeloSel", sObjectModelosPaso);
			        objComponent.set("v.recordId", varObjModeloSelValue.strIdSolicitud);
			        objComponent.set("v.strIdCatCentMod", varObjModeloSelValue.strIdCatCentrModelos);
                	console.log("EN Helper.confirmaReciboVines sObjectModelosPaso: ", sObjectModelosPaso);
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    	
    },
    
    hpFacturarOtroCliente: function(objComponent, objEvent) {   
    	console.log("EN Helper.confirmaReciboVines..");

        objComponent.set("v.blnShowFacturaOtroClienteFlotProg", true);
        objComponent.set("v.blnShowConfirmaReciboFlotProg", false);
    	objComponent.set("v.blnShowExcepFlotProg", false);
    	objComponent.set("v.blnShowVinesDOD", false);

        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelExcep = objEvent.getSource().get("v.name");
        //console.log("EN Helper.confirmaReciboVines varObjModeloSelValue1: " + JSON.stringify( varObjModeloSelValue ) );
        console.log("EN Helper.confirmaReciboVines varObjModeloSelExcep1: " + JSON.stringify( varObjModeloSelExcep ) );
        
        let sDatosModelo = varObjModeloSelValue.strNombre;
        //console.log("EN Helper.confirmaReciboVines sDatosModelo2: " + sDatosModelo );
        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
        let arrDatosModelo = sDatosModelo.split("-");
        let strAnioModelo = arrDatosModelo[2];
        let strModelo = arrDatosModelo[1];
        let strSerie = arrDatosModelo[0];
        let strCveColorExteriorCustom = arrDatosModelo[3];
        let strCveColorInteriorCustom = arrDatosModelo[4];
        
        let lstModelosPaso = objComponent.get("v.lstModelos");
        lstModelosPaso.push({
        	'sobjectType': 'TAM_OrdenDeCompraWrapperClass',
	        'strAnioModelo': strAnioModelo,
	        'strModelo': strModelo,
	        'strSerie': strSerie,
	        'strVersion': varObjModeloSelValue.strVersion,
	        'strCveColorExteriorCustom': strCveColorExteriorCustom,
	        'strColorExteriorCustom': varObjModeloSelValue.strDescripcionColorExterior,
	        'strCveColorInteriorCustom': strCveColorInteriorCustom,
	        'strColorInteriorCustom': varObjModeloSelValue.strDescripcionColorInterior,
	        'strIdCatModelos': varObjModeloSelValue.strIdCatCentrModelos,
	        'strCantidad': varObjModeloSelValue.strCantidadSolicitada,
	        'strIdRegistro': varObjModeloSelValue.strIdSolicitud
        });
        let sObjectModelosPaso = lstModelosPaso[0];
        
        let strIdExterno = varObjModeloSelValue.strIdExterno;
        let objAction = objComponent.get("c.getWrpSolFlotProgFinal");
        objAction.setParams(
        	{  
        		"strIdExterno" : strIdExterno,
        		"varObjModeloSelExcep" : varObjModeloSelExcep
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objObjModeloSelExcepResult = objResponse.getReturnValue();
                if(objObjModeloSelExcepResult != null){
                	console.log("EN Helper.confirmaReciboVines objObjModeloSelExcepResult: ", objObjModeloSelExcepResult);                
			    	objComponent.set("v.objModeloSelExcep", objObjModeloSelExcepResult );    	
			    	objComponent.set("v.objModeloSel", sObjectModelosPaso);
			        objComponent.set("v.recordId", varObjModeloSelValue.strIdSolicitud);
			        objComponent.set("v.strIdCatCentMod", varObjModeloSelValue.strIdCatCentrModelos);
                	console.log("EN Helper.confirmaReciboVines sObjectModelosPaso: ", sObjectModelosPaso);
                }//Fin si lstSolFlotPrograMod != null
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    	
    },
        
})