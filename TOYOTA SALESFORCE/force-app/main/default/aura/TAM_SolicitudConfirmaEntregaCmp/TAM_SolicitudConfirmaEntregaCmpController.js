({
    //La funcion para inicializar el componente
    doInit : function(objComponent, objEvent, objHelper) {
        objHelper.initializeComponent(objComponent, objEvent);
    },
	
	//La función SeleccionaTipoExcepcion
    SeleccionaTipoExcepcion : function(objComponent, objEvent, objHelper) {
        objHelper.SeleccionaTipoExcepcion(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SeleccionaMesFechaCierre : function(objComponent, objEvent, objHelper) {
        objHelper.SeleccionaMesFechaCierre(objComponent, objEvent);
    },

	//La función consultaDatosSolicitudes
    consultaDatosSolicitudes : function(objComponent, objEvent, objHelper) {
        objHelper.consultaDatosSolicitudes(objComponent, objEvent);
    },

    consultaVinesDOD : function(objComponent, objEvent, objHelper) {        
        objHelper.showVinesDOD(objComponent, objEvent);
    },

    creaExcepcionFlotillaPrograma : function(objComponent, objEvent, objHelper) {        
        objHelper.creaExcepcionFlotillaPrograma(objComponent, objEvent);
    },

    confirmaReciboVines : function(objComponent, objEvent, objHelper) {        
        objHelper.confirmaReciboVines(objComponent, objEvent);
    },

    jsFacturarOtroCliente : function(objComponent, objEvent, objHelper) {        
        objHelper.hpFacturarOtroCliente(objComponent, objEvent);
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
      // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

})