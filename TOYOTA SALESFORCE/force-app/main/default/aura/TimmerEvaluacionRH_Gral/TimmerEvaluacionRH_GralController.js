({
    doInit : function(component, event, helper) {
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        component.set("v.ltngTimmer","00:00:00");
        var action = component.get("c.getEvalInfo");
        action.setParams({
            "UserId": userId
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.evalCandidato",response.getReturnValue());  
                var evalIniciada = component.get("v.evaluacionIniciada");
                if(evalIniciada == true){
                    $A.util.addClass(component.find("iniciarEval"), "slds-hide");
                    $A.util.removeClass(component.find("contadorEval"), "slds-hide");
                    var duracionEva = component.get("v.DuracionEvaluacion");
                    var duracionExamenInt = parseInt(duracionEva);
                    var fechaAssestment = component.get("v.fechaAssestment");
                    var fechaDateAssestment = new Date(fechaAssestment);               
                    var currentdate = new Date(); 
                    var datetime =  currentdate.getDate() + "-"
                    + (currentdate.getMonth()+1)  + "-" 
                    + currentdate.getFullYear() + " "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();
                    var Christmas = new Date(fechaAssestment);
                    var fechaFinExamen = Christmas.setMinutes(Christmas.getMinutes()+duracionExamenInt);
                    var fechaFinDate = new Date(fechaFinExamen);
                    var fechaToday = new Date(datetime);
                    var today = new Date();
                    var diffMs = (fechaFinExamen - today); 
                    var diffDays = Math.floor(diffMs / 86400000);
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); 
                    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); 
                    var timer = setInterval(function() {
                        var today = new Date();
                        
                    }, 1000);
                    
                    component.set("v.ltngHour",diffHrs);
                    component.set("v.ltngMinute",diffMins);
                    var hours=component.get("v.ltngHour");
                    var minutes=component.get("v.ltngMinute");
                    var seconds=component.get("v.ltngSecond");
                    var tt=component.get("v.ltngHour")+":"+component.get("v.ltngMinute")+":"+component.get("v.ltngSecond");
                    component.set("v.ltngTimmer",hours+":"+minutes+":"+seconds);
                    helper.setStartTimeOnUI(component);
                }
                
                if(evalIniciada == false){		
                    console.log("evaluacion no Iniciada");
                    $A.util.addClass(component.find("contadorEval"), "slds-hide");
                    $A.util.removeClass(component.find("iniciarEval"), "slds-hide");
                }
                
            } 
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },
    
    IniciarEval : function(component, event, helper) {
        //Validación para que la fecha definida por RH sea igual a cuando se pusla el boton
        var fechaRH = component.get("v.fechaRH");
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy  = today.getFullYear();
        if(dd<10) 
        {
            dd='0'+dd;
        } 
        
        if(mm<10) 
        {
            mm='0'+mm;
        } 
        today = yyyy+'-'+mm+'-'+dd;
        //La fecha de clic en el boton es diferente a la fecha definiada en Salesforce por RH
        if(fechaRH.toString()  != today.toString()){
            component.set("v.isTimeEval",false);
        }
        // Si la fecha de clic en boton es igual a la ficha definida por RH , se procede a liberar la evaluación
        if(fechaRH.toString()  == today.toString()){     
            var userId = $A.get( "$SObjectType.CurrentUser.Id" );
            component.set("v.ltngTimmer","00:00:00");
            var action = component.get("c.getEvalInfo");
            action.setParams({
                "UserId": userId
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.evalCandidato",response.getReturnValue());   
                    var fechaEvaluacion = component.get("v.fechaEvaluacion");
                    var duracionEva = component.get("v.DuracionEvaluacion");
                    var duracionExamenInt = parseInt(duracionEva);
                    var fechaAssestment = component.get("v.fechaAssestment");
                    var fechaDateAssestment = new Date(fechaAssestment);               
                    var currentdate = new Date(); 
                    var datetime =  currentdate.getDate() + "-"
                    + (currentdate.getMonth()+1)  + "-" 
                    + currentdate.getFullYear() + " "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();
                    var Christmas = new Date(fechaAssestment);
                    var fechaFinExamen = Christmas.setMinutes(Christmas.getMinutes()+duracionExamenInt);
                    var fechaFinDate = new Date(fechaFinExamen);
                    var fechaToday = new Date(datetime);
                    var today = new Date();
                    var diffMs = (fechaFinExamen - today); 
                    var diffDays = Math.floor(diffMs / 86400000);
                    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); 
                    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); 
                    var timer = setInterval(function() {
                        var today = new Date();
                        
                    }, 1000);
                    
                    $A.util.addClass(component.find("iniciarEval"), "slds-hide");
                    console.log("comenzo EVALUACION");
                    component.set("v.ltngHour",diffHrs);
                    component.set("v.ltngMinute",diffMins);
                    var hours=component.get("v.ltngHour");
                    var minutes=component.get("v.ltngMinute");
                    var seconds=component.get("v.ltngSecond");
                    var tt=component.get("v.ltngHour")+":"+component.get("v.ltngMinute")+":"+component.get("v.ltngSecond");
                    component.set("v.ltngTimmer",hours+":"+minutes+":"+seconds);
                    helper.changeOwnerEval(component);
                    helper.setStartTimeOnUI(component);
                    location.reload();
                    //  }
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            $A.enqueueAction(action);  
        }      
    },
    
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
    
    handleShowModal: function(component) {
        $A.createComponent("c:ConfirmarCerrarEvaluacionRH", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "La evaluación finalizara y será enviada al área de RH!!!",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
    },
    
    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        if(message == 'Ok')
        {
            helper.FinalizarEval(component);
        }
        else if(message == 'Cancel')
        {
            // if the user clicked the Cancel button do your further Action here for canceling
        }
    }
})