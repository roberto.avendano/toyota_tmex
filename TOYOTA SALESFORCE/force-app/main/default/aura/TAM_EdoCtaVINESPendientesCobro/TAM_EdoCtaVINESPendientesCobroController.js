({
    doInit : function(component, event, helper) {
        helper.getInfoGralEdoCta(component,helper);
        helper.getProvisionesByDealer(component, helper);
        helper.getOpcionesPickListEstatusReg(component, helper);
        helper.getOpcionesPickListEstatusFinanzas(component, helper);
        helper.getLabels(component, helper);
    }, 
    
    doFilter: function(component, event, helper) {  
        helper.busquedaVIN(component); 
    },
    
    onSelectAllChange: function(component, event, helper) {
        helper.handleSelectAllChange(component);
    },
    
    saveController: function(component, event, helper) {
        helper.saveRecords(component,event,helper);
    },
    
    saveControllerFinanzasTMEX: function(component, event, helper) {
        helper.saveRecordsFinanzasTMEX(component,event,helper);
    },
    
    
    cerrarEdoCtaController: function(component, event, helper) {
        helper.cerrarEdoCta(component,event,helper);
    },
    
    agregarVIN: function(component, event, helper) {
        helper.handleShowModalVIN(component,event,helper);
    },    
    
    cargaFactura: function(component, event, helper) {
        helper.handleShowModalCargaFact(component,event,helper);
        
    },    
    
    validarMonto: function(component, event, helper) {
        helper.validarCantidades(component, event, helper);
    },
    
    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        if(message == 'Ok'){
            
        }
        else if(message == 'Cancel'){
            
        }
    },
    
    checkTFS: function(component, event, helper) {
        helper.getValueGrid(component, event, helper);
    },
    
    transferirRetail: function(component, event, helper) {
        helper.transferirRegistro(component, event, helper);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    event: function(component, event, helper) {
        helper.getProvisionesByDealer(component, helper);
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    
    aprobacionTMEXEstatus : function(component,event,helper){
        helper.actualizaEstatusPorFactura(component,event,helper);
        
    },
    
    actualizaFechaPago  : function(component,event,helper){
        helper.actualizaFechaPagoPorFactura(component,event,helper);
        
    },
    
    actualizaEdoCta : function(component,event,helper){
        helper.actualizaFilas(component,event,helper);

    },
    
    validarEstatus : function(component,event,helper){
        helper.validarEstatusRegistro(component,event,helper);
        
    }	
    
})