({
    
    getInfoGralEdoCta : function(component,event,helper) {
        var recordId =  component.get("v.recordId");
        var action =    component.get("c.getInfoGralEdoCta"); 
        
        action.setParams({ 
            recordId : recordId
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.estadoCuentaGralInfo',result);
                component.set('v.loaded',true);
            }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    
    getProvisionesByDealer : function(component,event,helper) {
        var recordId =  component.get("v.recordId");
        var action =    component.get("c.getProvisionByDealer"); 
        var tipoOperacion = component.get("v.tipoOpcion");
        var tipoProvision = component.get("v.tipoProvision");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        
        action.setParams({ 
            recordId : recordId,
            tipoOperacion :tipoOperacion,
            tipoProvision : tipoProvision,
            userId        : userId
        });
        
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.lineasVINESProvisionados',result);
                component.set("v.UnfilteredData",result);
                
            }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    busquedaVIN: function(component,objEvent,lstResult) {  
        console.log('Entro a helper de busqueda');
        var data = component.get("v.lineasVINESProvisionados");
        var allData = component.get("v.UnfilteredData");  
        var searchKey = component.get("v.filter"); 
        if(data != null || data.length>0){ 
            var filtereddata = allData.filter(word => (!searchKey) || word.VIN.toLowerCase().indexOf(searchKey.toLowerCase()) > -1);  
            console.log('** '+filtereddata); 
            component.set("v.lineasVINESProvisionados", filtereddata);  
        }  
        
        
    },
    
    
    getOpcionesPickListEstatusFinanzas: function(component,objEvent,lstResult) {  
        var action = component.get("c.getPickListEstatusFinanzas");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.pickListEstatusFinanzasTMEX",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    
    getOpcionesPickListEstatusReg: function(component,objEvent,lstResult) {  
        
        var action = component.get("c.getPickListEsatusReg");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.pickListEstatusRegistroOpc",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    handleSelectAllChange: function(component) {
        let estatusBandera = component.get("v.isAllSelected");
        
        if(component.get('v.isAllSelected') == true) {
            component.set('v.isAllSelected', true);
            const myCheckboxes = component.find('myCheckboxes'); 
            let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
            chk.forEach(checkbox => checkbox.set('v.checked',false));
            
        }
        
        
        if(component.get('v.isAllSelected') == false) {
            component.set('v.isAllSelected', true);
            const myCheckboxes = component.find('myCheckboxes'); 
            let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
            chk.forEach(checkbox => checkbox.set('v.checked', component.get('v.isAllSelected')));
        }
    },
    
    savePoint : function(component,event,helper) {
        let edoCtaIN  = component.get("v.lineasVINESProvisionados");
        let edoCtaCerrado = false;
        let action = component.get("c.saveEdoCta");
        
        
        action.setParams({ 
            edoCtaIN : edoCtaIN,
            edoCtaCerrado : edoCtaCerrado
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    saveRecordsFinanzasTMEX: function(component,event,helper) {
        let edoCtaIN  = component.get("v.lineasVINESProvisionados");
        let edoCtaCerrado = false;
        let revisionFinanzasTMEX = true;
        let action = component.get("c.saveEdoCta");
        
        action.setParams({ 
            edoCtaIN : edoCtaIN,
            edoCtaCerrado : edoCtaCerrado,
            revisionFinanzasTMEX : revisionFinanzasTMEX
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Success",
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                this.getProvisionesByDealer(component, event, helper);
                toastEvent.fire();
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    
    saveRecords: function(component,event,helper) {
        
        let edoCtaIN  = component.get("v.lineasVINESProvisionados");
        let edoCtaCerrado = false;
        let revisionFinanzasTMEX = false;
        let action = component.get("c.saveEdoCta");
        
        
        action.setParams({ 
            edoCtaIN : edoCtaIN,
            edoCtaCerrado : edoCtaCerrado,
            revisionFinanzasTMEX : revisionFinanzasTMEX
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                //Setear los registros actualizados a la lista lineasVINESPorCobrarUpd para mandar al otro cmpt
                component.set("v.test","nuevo Valor");
                component.set("v.lineasVINESPorCobrarUpd",response);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Success",
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                this.getProvisionesByDealer(component, event, helper);
                toastEvent.fire();
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    handleShowModalVIN: function(component,event,helper) {
        var recordId = component.get("v.recId");
        var lineasVINESProvisionados = component.get('v.lineasVINESProvisionados');
        var estadoCuentaGralInfo = component.get('v.estadoCuentaGralInfo');
        var tipoSeccion = component.get('v.tipoProvision');
        
        $A.createComponent("c:TAM_AgregarVINEdoCta", {
            recordId            	: recordId,
            lineasVINESProvisionados : lineasVINESProvisionados,
            estadoCuentaGralInfo   : estadoCuentaGralInfo,
            tipoSeccion          : tipoSeccion    
        },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib1').showCustomModal({
                                       header: "Agregar VIN",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
        
    },    
    
    handleShowModalCargaFact: function(component,event,helper) {
        var recordId = component.get("v.recordId");
        var estadoCuentaGralInfo = component.get("v.estadoCuentaGralInfo");
        var lineasVINESProvisionados = component.get("v.lineasVINESProvisionados");
        var tipoProvision = component.get("v.tipoProvision");
        $A.createComponent("c:TAM_FacturaCobroTMEX", {
            recordId            	: recordId,
            estadoCuentaGralInfo    : estadoCuentaGralInfo,
            lineasVINESProvisionados : lineasVINESProvisionados,
            tipoProvision			:  tipoProvision
            
        },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib1').showCustomModal({
                                       header: "Detalle Estado de Cuenta a Facturar",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
        
        
    },
    
    cerrarEdoCta: function(component,event,helper) {
        
        var lineasVINESProvisionados = component.get('v.lineasVINESProvisionados');
        var recordId = component.get("v.recId");
        
        $A.createComponent("c:TAM_CerrarEdoCta", {
            lineasVINESProvisionados: lineasVINESProvisionados,
            recordId            	: recordId
            
            
        },
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib1').showCustomModal({
                                       header: "Términos y Condiciones ",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
        
    },
    
    transferirRegistro: function(component,event,helper) {
        var detalleEdoCtaIn = event.getSource().get("v.text");
        var checkVar=component.find("v.chkBoxId");
        var listaRecords =  component.get('v.lineasVINESProvisionados');
        var index = listaRecords.indexOf(detalleEdoCtaIn);
        
        var action = component.get("c.compararIncentivos");
        
        action.setParams({ 
            detalleEdoCtaIn : detalleEdoCtaIn
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if (index > -1) {
                    listaRecords.splice(index, 1);
                    listaRecords.splice(index,0,respuesta);
                    component.set('v.lineasVINESProvisionados',listaRecords);
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },   
    
    getLabels : function(component,event,helper) {	
        var labelBanderaTFS = $A.get("$Label.c.TAM_LabelEdoCtaTFS");
        component.set("v.labelBanderaTFS",labelBanderaTFS);
    
        //Reemboslo de interes AutoDemo
        var reembolsoInteres = $A.get("$Label.c.TAM_ReembolsoInteresAutoDemo");
        component.set("v.reembolsoInteres",reembolsoInteres);
    
    },
    
    validarCantidades: function(component,event,helper) {
        var edoCtaIN = event.getSource().get("v.labelClass");
        var listaRecords =  component.get('v.lineasVINESProvisionados');
        var index = listaRecords.indexOf(edoCtaIN);
        
        var action = component.get("c.savePointEdoCta");
        
        action.setParams({ 
            edoCtaIN : edoCtaIN
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if (index > -1) {
                    listaRecords.splice(index, 1);
                    listaRecords.splice(index,0,respuesta);
                    component.set('v.lineasVINESProvisionados',listaRecords);
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    getValueGrid: function(component,event,helper) {
        var detalleEdoCtaIn = event.getSource().get("v.text");
        var checkVar=component.find("v.chkBoxId");
        var listaRecords =  component.get('v.lineasVINESProvisionados');
        var index = listaRecords.indexOf(detalleEdoCtaIn);
        
        var action = component.get("c.getIncentivoCorrespondiente");
        
        action.setParams({ 
            detalleEdoCtaIn : detalleEdoCtaIn
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                
                if (index > -1) {
                    listaRecords.splice(index, 1);
                    listaRecords.splice(index,0,respuesta);
                    component.set('v.lineasVINESProvisionados',listaRecords);
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    actualizaEstatusPorFactura : function(component,event,helper) {
        var detalleEdoCtaIn = event.getSource().get("v.name");
        var estatus = event.getSource().get("v.value");
        var objWrapper = component.get("v.lineasVINESProvisionados");
        var i;       
        var newObj = [];
        for(i in objWrapper){
            var record = objWrapper[i];
            var space = objWrapper[i];
            var estado = record.estatusFinanzasTMEX;
            if(detalleEdoCtaIn == record.facturaCobroTMEX){
                
                space.estatusFinanzasTMEX = estatus;
                
            }
            newObj.push(space);
        }
        
        component.set('v.lineasVINESProvisionados',newObj); 
        
    },
    
    actualizaFechaPagoPorFactura : function(component,event,helper) {
        var factura = event.getSource().get("v.labelClass");
        var fechaPago = event.getSource().get("v.value");
        var objWrapper = component.get("v.lineasVINESProvisionados");
        var i;       
        var newObj = [];
        for(i in objWrapper){
            var record = objWrapper[i];
            var space = objWrapper[i];
            var estado = record.estatusFinanzasTMEX;
            if(factura == record.facturaCobroTMEX){
                space.fechaTentativaPago = fechaPago;
                
            }
            newObj.push(space);
        }
        
        component.set('v.lineasVINESProvisionados',newObj);
        
    },
    
    validarEstatusRegistro : function(component,event,helper) {
        var registroCheck = event.getSource().get("v.labelClass");
        var objWrapper = component.get("v.lineasVINESProvisionados");
        var index = objWrapper.indexOf(registroCheck);
        
        if(registroCheck.VINEntregado === false){
            registroCheck.enviarFacturar = false;
            
        }
        
        objWrapper.splice(index, 1);
        objWrapper.splice(index,0,registroCheck);
        component.set('v.lineasVINESProvisionados',objWrapper);
        
        
    },
    
    actualizaFilas : function(component,event,helper) {
        var listaModificada = component.get("v.lineasVINESPendientesCobroUpd");  
        var listLength = listaModificada.length;
        var listaCargada = component.get("v.lineasVINESProvisionados");   
        var listaOriginal =  component.get("v.lineasVINESProvisionados");
        
        //Si la lista lineasVINESProvisionados es igual a 0
        if(listaOriginal == null){
            helper.getProvisionesByDealer(component,event,helper);
            
        }
        
        //Si la lista lineasVINESProvisionados es mayor a 0 solo se agregan los nuevos valores a la lista 
        if(listaOriginal != null){
            for (var i = 0; i < listLength; i++) {
                //Registros con estatus pendiente de cobro
                if(listaModificada[i].estatusVINEdoCta == 'Pendiente de cobro'){
                    listaCargada.push(listaModificada[i]);
                }
                
            }  
            //Se actualiza la lista lineasVINESProvisionados con los nuevos valores
            component.set("v.lineasVINESProvisionados",listaCargada); 
        }
        
    }
    
})