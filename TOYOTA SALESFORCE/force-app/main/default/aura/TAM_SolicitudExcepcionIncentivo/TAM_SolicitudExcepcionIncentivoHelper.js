({
    
    validarPolitica : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo");
        let anioModelo   = component.get("v.selectedRecord.TAM_AnioModelo");	
        let fechaVentaVIN   = component.get("v.selectedRecord.Submitted_Date");	
        let solicitaBono	= "SinBono";
        
        let tipoVenta = component.get("v.tipoVenta");
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.getPoliticasVigentes");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaVIN : fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : tipoVenta
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","SinBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{         
                        helper.obtenListaIncentivos(component, event, helper);
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
         
    },
    
    validarPoliticaBonoLealtad : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN + Bono de lealtad
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo");
        let anioModelo   = component.get("v.selectedRecord.TAM_AnioModelo");	
        let fechaVentaVIN   = component.get("v.selectedRecord.Submitted_Date");
        let solicitaBono	= "ConBono";
        let tipoVenta = component.get("v.tipoVenta");
        
        
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;

        }     
        
        if(validaDatos == true){
            var action = component.get("c.getPoliticasVigentes");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaVIN :fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : tipoVenta
            });
           
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","ConBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{     
                      helper.obtenListaIncentivos(component, event, helper); 
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
        
        
        
    },
    
    guarda : function(component, event, helper) {
        let vin = component.get("v.selectedRecord.TAM_VIN");
        let serie = component.get("v.selectedRecord.TAM_Serie");
        let incentivoVigente = component.get("v.politicaVigente.TAM_IncentivoTMEX_Cash__c");
        let incentivoPropuesto = component.get("v.incentivoPropuesto");
        let comentario 		 = component.get("v.comentario");
        let motivoSelect = component.get("v.motivoSeleccionado");
        let politicaSeleccionada = component.get("v.politicaSeleccionada");
        let dealerId = component.get("v.selectedRecord.Distribuidor");
        let politicasCorrespondientes = component.get("v.politicaVigente");
        let tipoVenta = component.get("v.tipoVenta");
        let tipoRegistro = 'Retail';
        let fechaVentaVIN   = component.get("v.selectedRecord.Submitted_Date");
        let anioModelo = component.get("v.selectedRecord.TAM_AnioModelo");
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo");
        let version = component.get("v.selectedRecord.TAM_VersionVIN");
        let datosValidos = false;
        
        //Varible para validar de la policitca seleccionada es igual o menor a la correspondiente
        let aprobacionAutomatica = false;
        //Política correspondiente fecha de venta TMEX
        let parteTMEXCorrespondiente = component.get("v.politicaVigente[0].incentivoTMEX");
        //Política Seleccionada por dealer TMEX
        let parteTMEXSeleccionada = component.get("v.resumenPrecio.incentivoTMEX");

        //Se valida si la aprobación sera en automatico
        if(parteTMEXSeleccionada <= parteTMEXCorrespondiente){
            aprobacionAutomatica = true;
            
        }

        if(comentario == undefined || comentario == ''){
            component.set("v.comentarioDealer",true);
            datosValidos == false;            
        }else{
            datosValidos = true;
        }

        let action = component.get("c.registraSolicitudExcepcion");
        action.setParams({
            vin : vin,
            serie : serie,
            incentivoVigente : incentivoVigente,
            incentivoPropuesto : incentivoPropuesto,
            comentario         : comentario,
            motivoSelect	   : motivoSelect,
            politicaSeleccionada : politicaSeleccionada,
            dealerId				: dealerId,
            politicasCorrespondientes : politicasCorrespondientes,
            tipoVenta	: tipoVenta,
            tipoRegistro : tipoRegistro,
            fechaVentaVIN : fechaVentaVIN,
            anioModelo  : anioModelo,
            modelo     : modelo,
            version : version,
            aprobacionAutomatica :aprobacionAutomatica
        });
        
    if(datosValidos === true){
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                component.set("v.isIncentivo",false);
                component.set("v.isPrecio",false);
                
                var idRegistro = response.getReturnValue(); 
                helper.relatedDocument(component, event, helper,idRegistro);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
         $A.enqueueAction(action);
        }
    }, 
    
    relatedDocument : function(component, event, helper,idRegistro) {
        let idSolicitudCreada = idRegistro;
        let idDocumento = component.get("v.documentId");

        var action = component.get("c.relatedDocumentWithRequestException");
        action.setParams({
            idSolicitudCreada : idSolicitudCreada,
            idDocumento : idDocumento
            
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": idRegistro,
                    "slideDevName": "related"
                });
                navEvt.fire();
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    
    },
    
    
    obtenListaIncentivos : function(component, event, helper) {
        let modelNumber = component.get("v.selectedRecord.TAM_codigoModelo");
        let modelYear   = component.get("v.selectedRecord.TAM_AnioModelo");	           
        
        var action = component.get("c.getPoliticasVIN");
        action.setParams({
            modelNumber : modelNumber,
            modelYear : modelYear
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                 var respuesta = response.getReturnValue();
         
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.politicaVigenteLista", response.getReturnValue());
      
                    component.set("v.existeListaPoliticas","ConPoliticas");
                }else{
                
                      component.set("v.existeListaPoliticas","SinPoliticas");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    obtenMotivosSolicitud : function(component, event, helper) {
        var action = component.get("c.obtieneMotivosSolicitud");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.motivosSolicitud",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
    
})