({
	save : function(component, event, helper) {
		component.find("edit").get("e.recordSave").fire();
	},

    handleSaveSuccess : function(component, event, helper) {
        console.log("success");
        component.find('notifLib').showNotice({
            "variant": "info",
            "header": "Success",
            "message": 'Registro guardado con exito',
            closeCallback: function() {
                //alert('You closed the alert!');
            }
        });
        /*component.find('notifLib').showToast({
            "variant": "success",
            "title": "Se actualizo correctamente.",
            "message": ""
        });*/
        
        ///component.find("overlayLib").notifyClose();
    }
})