({
    
    doInit: function(component, event, helper) {
        
        var userId = $A.get("$SObjectType.CurrentUser.Id");    
        var action =    component.get("c.getCurrentUser"); 
        
        action.setParams({ 
            userId : userId
        });
        
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.isUserFinanzas',result);
            }
        });
        
        $A.enqueueAction(action);
        
        //helper.getInfoGralEdoCta(component,helper);
        
    },
    
    toggleSection : function(component, event, helper) {
        var sectionAuraId = event.target.getAttribute("data-auraId");
        var sectionDiv = component.find(sectionAuraId).getElement();
        var sectionState = sectionDiv.getAttribute('class').search('slds-is-open'); 
        
        if(sectionState == -1){
            sectionDiv.setAttribute('class' , 'slds-section slds-is-open');
        }else{
            sectionDiv.setAttribute('class' , 'slds-section slds-is-close');
        }

    },
    
    refreshCmpt : function(component, event, helper) {
            var event = $A.get("e.c:TAM_EventSaveEdoCta");
            event.setParam("message", "the message to send" );	
            event.fire();

    },
    
    generaXLS: function(component, event, helper) {
        var recordId = component.get("v.recordId");
        var tipoProvision = component.get("v.tipoProvision");
        var tipoUsuario = component.get("v.isUserFinanzas"); 
        
        //Estado de cuenta es Retail
        if(tipoProvision == 'Retail'){
            var url;
            var urlEvent = $A.get("e.force:navigateToURL");
            if(tipoUsuario == 'DealerUser'){
                url = "/ToyotaCRM/apex/EstadoCuentaExportarXLS?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");
            }
            if(tipoUsuario != 'DealerUser'){
              	url = "/apex/EstadoCuentaExportarXLS?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");  
            }
            
            urlEvent.setParams({
                "url": url,
                "isredirect": "true"
            });
            //urlEvent.fire();
            window.open(url, '_blank');
        }
        
        //Estado de cuenta es Venta Corporativa
        if(tipoProvision == 'VentaCorporativa'){
            var url;
            var urlEvent = $A.get("e.force:navigateToURL");
            if(tipoUsuario == 'DealerUser'){
                url = "/ToyotaCRM/apex/EstadoCuentaVCxls?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");
            }
            if(tipoUsuario != 'DealerUser'){
              	url = "/apex/EstadoCuentaVCxls?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");  
            }
            urlEvent.setParams({
                "url": url,
                "isredirect": "true"
            });
            //urlEvent.fire();
            window.open(url, '_blank');
        }
        
        //Estado de cuenta es Bono
        if(tipoProvision == 'Bono'){
            var url;
            var urlEvent = $A.get("e.force:navigateToURL");
            if(tipoUsuario == 'DealerUser'){
                url = "/ToyotaCRM/apex/EstadoCuentaExportarBono?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");
            }
            if(tipoUsuario != 'DealerUser'){
              	url = "/apex/EstadoCuentaExportarBono?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");  
            }
            
            urlEvent.setParams({
                "url": url,
                "isredirect": "true"
            });
            //urlEvent.fire();
            window.open(url, '_blank');
        }
        
        //Estado de cuenta es Auto Demo
        if(tipoProvision == 'AutoDemo'){
            var url;
            var urlEvent = $A.get("e.force:navigateToURL");
             if(tipoUsuario == 'DealerUser'){
                url = "/ToyotaCRM/apex/EstadoCuentaExportarAutoDemo?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");
            }
            if(tipoUsuario != 'DealerUser'){
              	url = "/apex/EstadoCuentaExportarAutoDemo?f=xls&id="+component.get("v.recordId")+'&provision='+component.get("v.tipoProvision");
            }
            urlEvent.setParams({
                "url": url,
                "isredirect": "true"
            });
            //urlEvent.fire();
            window.open(url, '_blank');
        }
        
    },
    
    
    handleEvent : function(objComponent, objEvent, objHelper) {
        $A.get('e.force:refreshView').fire();
        
    } 
})