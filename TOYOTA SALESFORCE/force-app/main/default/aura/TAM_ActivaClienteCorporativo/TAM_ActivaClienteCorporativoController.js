({

    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecRango : function(objComponent, objEvent, objHelper) {
        objHelper.selecRango(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecGpoCorp : function(objComponent, objEvent, objHelper) {
        objHelper.selecGpoCorp(objComponent, objEvent);
    },

    /** Funcion Inicial */
	actualizarDatosClienteCorp : function(objComponent, objEvent, objHelper) {
		objHelper.actualizarDatosClienteCorp(objComponent, objEvent);
    },

    /** Funcion Inicial */
	confirmaDatosCteCorp : function(objComponent, objEvent, objHelper) {
		objHelper.confirmaDatosCteCorp(objComponent, objEvent);
    },

    /** Funcion Inicial */
	validaTipoPersonaFact : function(objComponent, objEvent, objHelper) {
		objHelper.validaTipoPersonaFact(objComponent, objEvent);
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
      // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

})