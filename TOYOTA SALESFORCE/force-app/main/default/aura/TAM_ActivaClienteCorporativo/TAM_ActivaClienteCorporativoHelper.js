({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.initializeComponent...");
		                
        let strIdCliente = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getDatosCliente");
        objAction.setParams({   			
        	recordId : strIdCliente
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objDatosClienteCorp = objResponse.getReturnValue();
            	objComponent.set("v.DatosClienteCorporativo", objDatosClienteCorp);
            	console.log("EN initializeComponent DatosClienteCorporativo: ", objComponent.get("v.DatosClienteCorporativo"));            	
            	console.log("EN initializeComponent objDatosClienteCorp: ", objDatosClienteCorp);
            	//Ve si los datos ya estan validados para poder facturar
            	if (objDatosClienteCorp.strTipoClienteCorp === 'Cliente - Persona Moral' || objDatosClienteCorp.strTipoClienteCorp === 'Cliente - Persona Moral No Vigente'){
            		objComponent.set("v.blnPersonaMoral", true);
            		objComponent.set("v.blnPersonaFisica", false);
            	}
            	if (objDatosClienteCorp.strTipoClienteCorp === 'Cliente - Persona Fisica' || objDatosClienteCorp.strTipoClienteCorp === 'Cliente - Persona Fisica No Vigente'){
            		objComponent.set("v.blnPersonaFisica", true);
            		objComponent.set("v.blnPersonaMoral", false);            		            	
            	}

           		objComponent.set("v.blnTieneSolicitud", objDatosClienteCorp.blnTieneSolProc);
                objComponent.set("v.bActCons", objDatosClienteCorp.objClienteActual.TAM_ActaConstitutiva__c);
                objComponent.set("v.bCedFisc", objDatosClienteCorp.objClienteActual.TAM_CedulaFiscal__c);
                objComponent.set("v.bOrdComp", objDatosClienteCorp.objClienteActual.TAM_OrdenCompraCartaCompromiso__c);
                objComponent.set("v.bPdfDatAdic", objDatosClienteCorp.objClienteActual.TAM_PdfDatosAdicionalesRepLegal__c);
                objComponent.set("v.bIdentOfic", objDatosClienteCorp.objClienteActual.TAM_IdentificacionOficial__c);
                objComponent.set("v.bComppDomc", objDatosClienteCorp.objClienteActual.TAM_ComprobanteDomicilio__c);
                objComponent.set("v.bDocmSopor", objDatosClienteCorp.objClienteActual.TAM_DocumentacionSoporte__c);
            	
            	//Ve si objDatosClienteCorp.blnNoEsProp
            	if (objDatosClienteCorp.blnNoEsProp){
            		objComponent.set("v.blnSinPermisoReactCte", true);
            		this.showToastError(objComponent, objEvent, objDatosClienteCorp.strNoEsProp);            	
            	}//Fin si objDatosClienteCorp.blnNoEsProp
            	            
            	if (!objDatosClienteCorp.blnNoEsProp){
            		objComponent.set("v.blnSinPermisoReactCte", objDatosClienteCorp.blnNoEsProp);
	            	//consulta los rangos
	            	this.consultaRangos(objComponent, objEvent, objDatosClienteCorp);            	
	            	//consulta los Grupos Corporativos
	            	this.consultaGruposCorporativos(objComponent, objEvent, objDatosClienteCorp);            	            	
            	}//Fin si !objDatosClienteCorp.blnNoEsProp

            	console.log("EN Consulta Datos Cliente Helper.initializeComponent blnTieneSolicitud: ", objComponent.get("v.blnTieneSolicitud"));            	
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent blnPersonaMoral: ", objComponent.get("v.blnPersonaMoral"));
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent blnPersonaFisica: ", objComponent.get("v.blnPersonaFisica"));
            	
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    validaTipoPersonaFact: function(objComponent, objEvent) {
    	console.log('EN Save validaTipoPersonaFact...');
       	let objDatosClienteCorp = objComponent.get("v.DatosClienteCorporativo");
        let value = objEvent.getSource().get("v.value");
       	this.consultaRangos(objComponent, objEvent, objDatosClienteCorp);        
        console.log('EN Save validaTipoPersonaFact value: ', value);
    },


   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	consultaRangos : function(objComponent, objEvent, objDatosClienteCorporativo) {
		console.log("EN consultaRangos...");				                
        let strIdCliente = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getRangos");
        objAction.setParams({
        	"sRango" : objDatosClienteCorporativo.strRango,
        	"sGpoCorp" : objDatosClienteCorporativo.strGpoCorp,
        	"recordId" : strIdCliente,
        	"datosClienteCorp" : objDatosClienteCorporativo
   		});				
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lRangosResult = objResponse.getReturnValue();
            	let sRango = objDatosClienteCorporativo.objClienteActual.TAM_ProgramaRango__c; 
            	objComponent.set("v.lRangos", lRangosResult);
            	console.log("EN consultaRangos lRangosResult: ", lRangosResult);
            	if (objDatosClienteCorporativo.strRango === undefined)
            		objComponent.set("v.sRango", '');   
            	if (objDatosClienteCorporativo.strRango !== undefined)
            		objComponent.set("v.sRango", objDatosClienteCorporativo.strRango);
           		console.log("EN consultaRangos sRango: ", objComponent.get("v.sRango") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    selecRango: function(objComponent, objEvent) {
    	console.log('EN Save selecRango...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sRango", value);        
        console.log('EN Save selecRango value: ', value);
        console.log('EN Save selecRango sRango: ', objComponent.get("v.sRango"));
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	consultaGruposCorporativos : function(objComponent, objEvent, objDatosClienteCorporativo) {
		console.log("EN consultaGruposCorporativos...");				                
        let strIdCliente = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getGruposCorporativos");
        objAction.setParams({
        	"sRango" : objDatosClienteCorporativo.strRango,
        	"sGpoCorp" : objDatosClienteCorporativo.strGpoCorp        	
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lGpoCorpResult = objResponse.getReturnValue();
            	let sGpoCorp = objDatosClienteCorporativo.objClienteActual.TAM_GrupoCorporativo__c; 
            	objComponent.set("v.lGpoCorporativo", lGpoCorpResult);
            	console.log("EN consultaRangos lGpoCorpResult: ", lGpoCorpResult);
            	if (objDatosClienteCorporativo.strGpoCorp === undefined)
            		objComponent.set("v.sGpoCorp", '');   
            	if (objDatosClienteCorporativo.strGpoCorp  !== undefined)
            		objComponent.set("v.sGpoCorp", objDatosClienteCorporativo.strGpoCorp );
           		console.log("EN consultaGruposCorporativos sGpoCorp: ", objComponent.get("v.sGpoCorp") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    selecGpoCorp: function(objComponent, objEvent) {
    	console.log('EN Save selecGpoCorp...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sGpoCorp", value);        
        console.log('EN Save selecGpoCorp value: ', value);
        console.log('EN Save selecGpoCorp sGpoCorp: ', objComponent.get("v.sGpoCorp"));
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	actualizarDatosClienteCorp : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.actualizarDatosClienteCorp...");
		                
		//Valida que todos los datos esten capturados
		let camposValidosBase = objComponent.find('validaCampo').reduce(
			function (validSoFar, inputCmp) {
				inputCmp.showHelpMessageIfInvalid(); //set("v.errors", [{message:"Debes capturar un valor." + value}]);
				return validSoFar && inputCmp.get('v.validity').valid;
			}
		, true);

		console.log("EN Consulta Datos Cliente Helper.actualizarDatosClienteCorp camposValidosBase: " + camposValidosBase);
        //Ve si hubo un error a la hora de validar 
        if (!camposValidosBase){
       		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en los campos que faltan.');
        }else{
        	this.upsertDatosClienteCorp(objComponent, objEvent);
        }
		        
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	upsertDatosClienteCorp : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.upsertDatosClienteCorp...");
		                        
        //Llama la función 
		let objAction = objComponent.get("c.upsDatosCliente");
        objAction.setParams({
        	"recordId" : objComponent.get("v.recordId"),
        	"datosClienteCorp" : objComponent.get("v.DatosClienteCorporativo"),
        	"sRango" : objComponent.get("v.sRango"),
        	"sGpoCorp" : objComponent.get("v.sGpoCorp")        	
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResAct = objResponse.getReturnValue();
            	console.log("EN Helper.upsertDatosClienteCorp objWrpResAct: ", objWrpResAct);
            	objComponent.set("v.DatosClienteCorporativo", objWrpResAct.objParamOpcional);
            	if (objWrpResAct.blnEstatus == false)
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	confirmaDatosCteCorp : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp...");

        //Llama la función 
		let objAction = objComponent.get("c.getArchivosCargardos");
        objAction.setParams({
        	"recordId" : objComponent.get("v.recordId"),
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let ArchichivosRequeridosResult = objResponse.getReturnValue();
            	console.log("EN Helper.creaSolicutidClienteCorp ArchichivosRequeridosResult: ", ArchichivosRequeridosResult);
            	objComponent.set("v.ArchichivosRequeridos", ArchichivosRequeridosResult);
            	
            	let strRango = objComponent.get("v.sRango"); 
				let bActCons = ArchichivosRequeridosResult.blnActaConstitutiva;
				let bCedFisc = ArchichivosRequeridosResult.blnCedulaFiscal;
				let bOrdComp = ArchichivosRequeridosResult.blnOrdenCompraCartaCompromiso;				
				let bPdfDatAdic = ArchichivosRequeridosResult.blnPdfDatosAdicionalesRepLegal;    
				let bIdentOfic = ArchichivosRequeridosResult.blnIdentificacionOficial;
				let bComppDomc = ArchichivosRequeridosResult.blnComprobanteDomicilio;
				let bDocmSopor = ArchichivosRequeridosResult.blnDocumentacionSoporte;

				let blnValidaRFC = ArchichivosRequeridosResult.blnValidaRFC;
				let strMsgValidaRfc = ArchichivosRequeridosResult.strMsgValidaRfc;
				
				let DatosClienteCorporativoPaso = objComponent.get("v.DatosClienteCorporativo"); 
				let sResulFinal = '';		
				let blnPersonaMoral = objComponent.get("v.blnPersonaMoral");
				let sActCons = 'Acta Constitutiva';
				let sCedFisc = 'Cedula Fiscal';
				let sOrdComp = 'Orden de Compra';
				let sPdfDatAdic = 'Datos Adicionales';
				
				let blnPersonaFisica = objComponent.get("v.blnPersonaFisica");
				let sIdentOfic = 'Identificación Oficial';
				let sComppDomc = 'Comprobante de Domicilio';
				let sDocmSopor = 'Documentos de Soporte';
				let bError = false;
				
				//Valida que todos los datos esten capturados
				let camposValidosBase = objComponent.find('validaCampo').reduce(
					function (validSoFar, inputCmp) {
						inputCmp.showHelpMessageIfInvalid(); //set("v.errors", [{message:"Debes capturar un valor." + value}]);
						return validSoFar && inputCmp.get('v.validity').valid;
					}
				, true);
		
				console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp camposValidosBase: " + camposValidosBase);
		        //Ve si hubo un error a la hora de validar 
		        if (camposValidosBase){
		        	//Ve si tiene los archivos que se requieren para la solicitar la activación
		        	//Persona moral
		        	if(blnPersonaMoral && !bActCons)
		        		sResulFinal += sActCons + ', ';	
		        	if(blnPersonaMoral && !bCedFisc)
		        		sResulFinal += sCedFisc + ', ';	
		        	if(blnPersonaMoral && !bOrdComp)
		        		sResulFinal += sOrdComp + ', ';	
		        	//Persona fisica        	
		        	if(blnPersonaFisica && !bIdentOfic)
		        		sResulFinal += sIdentOfic + ', ';	
		        	if(blnPersonaFisica && !bComppDomc)
		        		sResulFinal += sComppDomc + ', ';	
		        	if(blnPersonaFisica && !bDocmSopor)
		        		sResulFinal += sDocmSopor + ', ';
		        	console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp bIdentOfic: " + bIdentOfic);

		        	//Ve si selecciono el Rango
		        	if (strRango === '' || strRango === null){
		        		this.showToastError(objComponent, objEvent, 'Debes seleccionar un Rango.');
		        		bError = true;
		        	}
		        	
		        	//Si hay error despliega el mensaje
		        	if(blnPersonaMoral && (!bActCons || !bCedFisc || !bOrdComp)){
		        		this.showToastError(objComponent, objEvent, 'Debes cargar los archivos: ' + sResulFinal);
		        		bError = true;
		        	}
		        	//Si hay error despliega el mensaje
		        	if(blnPersonaFisica && (!bIdentOfic || !bComppDomc || !bDocmSopor)){
		        		this.showToastError(objComponent, objEvent, 'Debes cargar los archivos: ' + sResulFinal);
		        		bError = true;
					}
					//Ya tienes todos archivos que se necesitan, Crea el reg de la solicitud
					let ProgramaRangoPaso = DatosClienteCorporativoPaso.objClienteCorporativo.TAM_ProgramaRango__c;
					console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp ProgramaRangoPaso: " + ProgramaRangoPaso);
					if (ProgramaRangoPaso === undefined){
		        		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en el campo: Programa / Rango.');
		        		bError = true;
		        	}
		        	//Ve si ya existe una solicitud pendiente por aprobar para este cliente
					if (blnValidaRFC){
		        		this.showToastError(objComponent, objEvent, strMsgValidaRfc);
		        		bError = true;
		        	}
					//Si no hay error entonces crea el reg de TAM_ReactivarCliente__c
					if (!bError){
						let ProgramaRangoFinal = DatosClienteCorporativoPaso.objClienteCorporativo.TAM_ProgramaRango__c;
						let GrupoCorporativoFinal = DatosClienteCorporativoPaso.objClienteCorporativo.TAM_GrupoCorporativo__c;
						console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp ProgramaRangoFinal: " + ProgramaRangoFinal);
						console.log("EN Consulta Datos Cliente Helper.confirmaDatosCteCorp GrupoCorporativoFinal: " + GrupoCorporativoFinal);						
						this.creaSolicutidClienteCorp(objComponent, objEvent, ProgramaRangoFinal, GrupoCorporativoFinal);
						//this.showToastSuccess(objComponent, objEvent, 'Todo bien hasta antes de la crecion de la solicitud');
					}//Fin si !bError
		        }else{
		       		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en los campos que faltan.');
		        }				
				
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 		
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	creaSolicutidClienteCorp : function(objComponent, objEvent, ProgramaRangoFinal, GrupoCorporativoFinal) {
		console.log("EN Consulta Datos Cliente Helper.creaSolicutidClienteCorp...");
		                        
        //Llama la función 
		let objAction = objComponent.get("c.creaSolCteCorp");
        objAction.setParams({
        	"recordId" : objComponent.get("v.recordId"),
        	"datosClienteCorp" : objComponent.get("v.DatosClienteCorporativo"),
        	"sRango" : ProgramaRangoFinal,
        	"sGpoCorp" : GrupoCorporativoFinal
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResAct = objResponse.getReturnValue();
            	console.log("EN Helper.creaSolicutidClienteCorp objWrpResAct: ", objWrpResAct);
            	objComponent.set("v.DatosClienteCorporativo", objWrpResAct.objParamOpcional);
            	if (objWrpResAct.blnEstatus == false){
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);
            		objComponent.set("v.blnTieneSolicitud", true);
            	}
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 1000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2000,
            "type": "error"
        });
        toastEvent.fire();
    },

})