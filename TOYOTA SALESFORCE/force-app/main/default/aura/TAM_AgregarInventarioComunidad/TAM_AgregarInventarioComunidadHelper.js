({
    
    estatusFacturacion : function(component, event, helper) {
        
        let leadId = component.get("v.recordId");
        let action = component.get("c.estatusFacturacionByLead");
        action.setParams({
            leadId : leadId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set('v.datosValidosFacturacion',respuesta); 
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    getListOfVINES : function(component, event, helper) {
        let leadId = component.get("v.recordId");
        
        let action = component.get("c.getInventariosByLead");
        action.setParams({
            leadId : leadId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set('v.listaVINESAgregados',respuesta); 
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    agregarVIN : function(component, event, helper) {
        //Recuperamos el VIN seleccionado
        let VINInventario = component.get("v.selectedRecord");
        let leadId = component.get("v.recordId");
        
        let action = component.get("c.saveRecordInventario");
        action.setParams({
            VINInventario : VINInventario,
            leadId : leadId
        });
        
        var newObj = component.get("v.listaVINESAgregados");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                newObj.push(respuesta);
                component.set('v.listaVINESAgregados',newObj); 
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Success",
                    "title": "Exitoso!",
                    "message": "VIN Agregado existosamente."
                });
                toastEvent.fire();
                
                component.set("v.selectedRecord",null);
                
                //Seteo de varibale true
                component.set("v.VINagregado",true);
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    eliminarRecord : function(component, event, helper) {
        var record = event.getSource().get("v.title");
        var idToDelete = record.IdInventarioLead; 
        
        let action = component.get("c.deleteRecordInventario");
        action.setParams({
            idToDelete : idToDelete
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var objWrapper = component.get("v.listaVINESAgregados");
                var index = objWrapper.indexOf(record);
                var listaVines = component.get("v.listaVINESAgregados");
                listaVines.splice(index, 1);
                component.set('v.listaVINESAgregados',objWrapper);   
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type" : "Success",
                    "title": "Exitoso!",
                    "message": "VIN Eliminado existosamente."
                });
                toastEvent.fire();
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
    
    
})