({
    doInit : function(component, event, helper) {
        helper.estatusFacturacion(component, event, helper);
        helper.getListOfVINES(component, event, helper);
    },
    
    agragrVINCtrl : function(component, event, helper) {
        helper.agregarVIN(component, event, helper);
    },
    
    eliminarReg : function(component, event, helper) {
        helper.eliminarRecord(component, event, helper);
    },
    
    closeQuickAction : function(objComponent, objEvent, objHelper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    
    
})