({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.doInit..");
		Component.set('v.recordId', Component.get("v.recordId"));
    	//Busca la lusra de clientes
    	Helper.getLstDistribInvent(Component, Event);
    	Component.set("v.blnShowDist", true);
    },

   /** Funcion despliegaDatosDistClick */
	jsDespliegaDatosDistClick : function(Component, Event, Helper) {
		console.log("EN jsDespliegaDatosDistClick...");
    	Helper.hpDespliegaDatosDist(Component, Event);
    },

   /** Funcion despliegaDatosDistClick */
	jsRestauraValoresDefaultClick : function(Component, Event, Helper) {
		console.log("EN jsRestauraValoresDefaultClick...");
    	Helper.hpRestauraValoresDefaultClick(Component, Event);
    },

   /** Funcion ocDistRel */
	ocDistRel : function(Component, Event, Helper) {
		console.log("EN ocDistRel...");		
    	Helper.actDistRel(Component, Event);
    },

   /** Funcion ocFuncPrinc */
	ocFuncPrinc : function(Component, Event, Helper) {
		console.log("EN ocFuncPrinc...");
    	Helper.actFuncPrinc(Component, Event);
    },

   /** Funcion ocTipoInvPrinc */
	ocTipoInvPrinc : function(Component, Event, Helper) {
		console.log("EN ocTipoInvPrinc...");
    	Helper.actTipoInvPrinc(Component, Event);
    },

   /** Funcion cancelar */
	cancelar : function(Component, Event, Helper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();		
    },

   /** Funcion jsCreaColaboracionClick */
	jsCreaColaboracionClick : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.jsCreaColaboracionClick..");
		Helper.hpCreaColaboracionClick(Component, Event);
    },

   /** Funcion jsActDefGptPub */
	jsActDefGptPub : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.jsActDefGptPub..");
		Helper.hpActDefGptPub(Component, Event);
    },

	jsActDefGpoPub : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.jsActDefGpoPub..");
		Helper.hpActDefGpoPub(Component, Event);
	},

   /** Funcion guardar */
	ocActualizarDatos : function(Component, Event, Helper) {
		console.log("EN TAM_AdminVisibInventarioToyota Contoller.ocActualizarDatos..");
		Helper.heActualizarDatos(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})