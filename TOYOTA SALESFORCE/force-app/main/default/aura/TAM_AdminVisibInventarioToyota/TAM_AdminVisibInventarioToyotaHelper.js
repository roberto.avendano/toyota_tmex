({
    /** getLstDistribInvent */
    getLstDistribInvent : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getLstDistribInvent..");
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDistDispCons");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistrRelResult = objResponse.getReturnValue();
            	Component.set("v.lstDistrRel", lstDistrRelResult);
            	this.getDistribInvent(Component, Event, lstDistrRelResult);
            	this.getUserAdminInven(Component, Event);
 			}
        });
        $A.enqueueAction(objAction);
	}, 

    /** getDistribInvent */
    getDistribInvent : function(Component, Event, lstDistrRelResult){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDistribInvent lstDistrRelResult: ", lstDistrRelResult);
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribInv");
        objAction.setParams({
        	"recordId" : Component.get("v.strIdRegistro"), 
        	"lstDistrRel" : lstDistrRelResult
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstWrpMapDatosDistResult = objResponse.getReturnValue();
            	//Inicializa el objeto de lWrpMapDatosDist
            	Component.set("v.lWrpMapDatosDist", lstWrpMapDatosDistResult);
            	//Llama a la funcion de getDistribInventSelec
            	this.getDistribInventSelec(Component, Event, lstWrpMapDatosDistResult);
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
	}, 

    /** getDistribInventSelec */
    getDistribInventSelec : function(Component, Event, lstWrpMapDatosDistResult){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getDistribInventSelec lstWrpMapDatosDistResult: ", lstWrpMapDatosDistResult);

        let optDistRel = [];
            	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribInvSelecc");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lWrpMapDatosDistCapResult = objResponse.getReturnValue();
            	
            	//Recorre la lista que viene en setSelectDistrRelde cada dist para bloqueasr 
            	for (let objDistPaso in lWrpMapDatosDistCapResult){
    				//Un arreglo de paso para los dist selecc
    				let optDistRelSelPaso = [];
    				//Toma la lista que viene en el campo setSelectDistrRel
    				let ssetSelectDistrRel = lWrpMapDatosDistCapResult[objDistPaso].setSelectDistrRel;
    				if (ssetSelectDistrRel.includes(';')){
	    				let arrSetSelectDistrRel = ssetSelectDistrRel.split(";");
	    				//Recorre la lista de los vines 
	    				for (let cdDeaSel in arrSetSelectDistrRel){
	    					let arrObjDeaSel = arrSetSelectDistrRel[cdDeaSel].split(",");
	    					optDistRel.push(arrObjDeaSel[0]);
	    				}//Fin del for para arrSetSelectDistrRel
    				}//Fin si ssetSelectDistrRel.includes(';')
            	}//Fin del for para lWrpMapDatosDistCapResult
            	
            	//Ve si tiene algo la lista de optDistRel
            	if (optDistRel.length > 0){
            		console.log("EN TAM_AdminVisibInventarioToyota Helper.getDistribInvent optDistRel: ", optDistRel);            	
    				for (let cdDeaSel in optDistRel){
    					let strCodDistPaso = optDistRel[cdDeaSel];
    					//Recorre la lista de todos los dist
	            		for (let objDistPaso in lstWrpMapDatosDistResult){
	            			let strNumeroDist = lstWrpMapDatosDistResult[objDistPaso].NumeroDist;
	            			//console.log("EN TAM_AdminVisibInventarioToyota Helper.getDistribInvent strNumeroDist: " + strNumeroDist + ' strCodDistPaso: ' + strCodDistPaso);
	            			if (strNumeroDist == strCodDistPaso){
	            				lstWrpMapDatosDistResult[objDistPaso].blnBlocDistRel = true;
	            				console.log("EN TAM_AdminVisibInventarioToyota Helper.getDistribInvent YA LO ENCONTRE strCodDistPaso: " + strCodDistPaso + ' NumeroDist: ' + lstWrpMapDatosDistResult[objDistPaso].blnBlocDistRel);
	            				break;		
	            			}//Fin si lWrpMapDatosDistCapResult[objDistPaso].NumeroDist == cdDeaSel
	            		}//Fin del fro para lWrpMapDatosDistCapResult    					
    				}//Fin del for optDistRel	
    			}//Fin si optDistRel.length > 0
                //Inicializa el objeto de lWrpMapDatosDistCap
            	Component.set("v.lWrpMapDatosDistCap", lWrpMapDatosDistCapResult);
                //Inicializa el objeto de lWrpMapDatosDist
            	Component.set("v.lWrpMapDatosDist", lstWrpMapDatosDistResult);
            	
 			}//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
	}, 

    /** getUserAdminInven */
    getUserAdminInven : function(Component, Event){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.getUserAdminInven...");
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getUserAdminInven");
        objAction.setParams({
        	"recordId" : Component.get("v.strIdRegistro")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let blnUserAdminInvResult = objResponse.getReturnValue();
            	//Ve si no puede usar este modulo
            	if (!blnUserAdminInvResult){
            		let strMsgAdvert = 'No tienes permiso para modificar información de este modulo.\n';
            		strMsgAdvert += 'Ponte en contacto con el administrador.';
            		this.showToastWarning(Component, Event, strMsgAdvert);
            	}//Fin si !blnUserAdminInvResult
            	//Actualiza la variable blnUserAdminInv
            	Component.set("v.blnUserAdminInv", blnUserAdminInvResult);
 			}
        });
        $A.enqueueAction(objAction);
	}, 

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpDespliegaDatosDist: function(objComponent, objEvent) {
    	console.log('EN Save hpDespliegaDatosDist...');
    	
    	let lstDistrRelPaso = objComponent.get("v.lstDistrRel");
        let strLabel = objEvent.getSource().get("v.label");
        let strValue = objEvent.getSource().get("v.value");
    	console.log('EN Save hpDespliegaDatosDist strLabel: ', strLabel);
    	console.log('EN Save hpDespliegaDatosDist strValue: ', strValue);
    	console.log('EN Save hpDespliegaDatosDist lstDistrRelPaso: ', lstDistrRelPaso);

        let lWrpMapDatosDistCapPaso = objComponent.get("v.lWrpMapDatosDistCap");
    	console.log('EN Save hpDespliegaDatosDist lWrpMapDatosDistCapPaso: ', lWrpMapDatosDistCapPaso);
    		
        let optDistRel = [];
        let opTFuncDistPrinc = [];
        let optTipInvPrinc = [];
        let optDistRelSelPaso = [];	
        	        
        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
   		//Recorre la lista de reg de lClientesDealer
   		for (let objDistPaso in lWrpMapDatosDistPaso){
    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == strValue){
    			if (lWrpMapDatosDistPaso[objDistPaso].blnMuestra)
    				lWrpMapDatosDistPaso[objDistPaso].blnMuestra = false;
    			else if (!lWrpMapDatosDistPaso[objDistPaso].blnMuestra){
    				//Inicializa el campo de blnMuestra
    				lWrpMapDatosDistPaso[objDistPaso].blnMuestra = true;
    				
    				//Recorre la lista de lstDistrRelPaso del dist 
    				for (let accountId in lstDistrRelPaso){
    					let strName = lstDistrRelPaso[accountId].Name;
    					let strNameFinal = strName.replace("TOYOTA", "")
    					optDistRel.push({ value: lstDistrRelPaso[accountId].Codigo_Distribuidor__c, label: strNameFinal});
    				}//Fin si
    				//Inicializa la lista de listopTFuncDistPrinc
    				lWrpMapDatosDistPaso[objDistPaso].setDistrRel = optDistRel; //items;
					//lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel = objComponent.get("v.values");
    				console.log('EN Save hpDespliegaDatosDist setDistrRel: ', lWrpMapDatosDistPaso[objDistPaso].setDistrRel);   
    				
    				//Recorre la lista de lWrpMapDatosDistCapPaso y ve si existe strValue
    				for (let coddealer in lWrpMapDatosDistCapPaso){
    					if (lWrpMapDatosDistCapPaso[coddealer].NumeroDist == strValue){
    						//Una lista para los dealer que correspen a strValue	
    						let optDistRelSelPaso = [];
    						//Toma la lista que viene en el campo setSelectDistrRel
    						let ssetSelectDistrRel = lWrpMapDatosDistCapPaso[coddealer].setSelectDistrRel;
    						let arrSetSelectDistrRel = ssetSelectDistrRel.split(";");
    						//Recorre la lista de los vines 
    						for (let cdDeaSel in arrSetSelectDistrRel){
    							let arrObjDeaSel = arrSetSelectDistrRel[cdDeaSel].split(",");
    							optDistRelSelPaso.push(arrObjDeaSel[0]);
    						}//Fin del for para arrSetSelectDistrRel
    						lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel = optDistRelSelPaso;
    						console.log('EN Save hpDespliegaDatosDist YA LO ENCONTRE : ', optDistRelSelPaso);
    						break;	
    					}//Fin si lWrpMapDatosDistCapPaso[coddealer].NumeroDist == strValue
    				}//Fin si
    				console.log('EN Save hpDespliegaDatosDist setSelectDistrRel: ', lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel);    				
    				    					
    				//Una variable para la lista de setFuncDistPrinc
    				let lstFunctionsDistPric = lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc; 
    				//Recorre la lista de setFuncDistPrinc del dist 
    				for (let functName in lstFunctionsDistPric){
    					opTFuncDistPrinc.push({ value: lstFunctionsDistPric[functName], label: lstFunctionsDistPric[functName]});
    				}//Fin si
    				//Inicializa la lista de listopTFuncDistPrinc
    				lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc = opTFuncDistPrinc;
    				console.log('EN Save hpDespliegaDatosDist setFuncDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc);   

    				//Una variable para la lista de lstTiposInvenDistPrinc
    				let lstTiposInvenDistPrinc = lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc; 
    				//Recorre la lista de lstTiposInvenDistPrinc del dist 
    				for (let tipInvName in lstTiposInvenDistPrinc){
    					optTipInvPrinc.push({ value: lstTiposInvenDistPrinc[tipInvName], label: lstTiposInvenDistPrinc[tipInvName]});
    				}//Fin si
    				//Inicializa la lista de listOptions
    				lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc = optTipInvPrinc;
    				console.log('EN Save hpDespliegaDatosDist setTiposInvenDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc);
    				
    				//Inicializa strCodDistSel
    				objComponent.set("v.strCodDistSel", strValue);
    			}//Fin si !lWrpMapDatosDistPaso[objDistPaso].blnMuestra
    			//Salte	
    			break;    		
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso

    	console.log('EN Save hpDespliegaDatosDist AL FINAL strValue123: ', strValue);
		//Recorre la lista de distribuidoes y esconde los demas
   		for (let objDistPaso in lWrpMapDatosDistPaso){
   			let strNumeroDistPaso = lWrpMapDatosDistPaso[objDistPaso].NumeroDist; 
    		if (strNumeroDistPaso != strValue){
    			console.log('EN Save hpDespliegaDatosDist AL FINAL ES DIFERENTE strValue: ' + strValue + ' strNumeroDistPaso: ' + strNumeroDistPaso);    						
   				//Inicializa el campo de blnMuestra
   				lWrpMapDatosDistPaso[objDistPaso].blnMuestra = false;
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso
   				
   		//Actualiza la lista de lWrpMapDatosDist
   		objComponent.set("v.lWrpMapDatosDist", lWrpMapDatosDistPaso);
		console.log('EN Save hpDespliegaDatosDist lWrpMapDatosDist: ', objComponent.get("v.lWrpMapDatosDist"));
   		
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpRestauraValoresDefaultClick: function(objComponent, objEvent) {
    	console.log('EN Save hpRestauraValoresDefaultClick...');
    	
    	let lstDistrRelPaso = objComponent.get("v.lstDistrRel");
        let strLabel = objEvent.getSource().get("v.label");
        let strValue = objEvent.getSource().get("v.value");
    	console.log('EN Save hpRestauraValoresDefaultClick strLabel: ', strLabel);
    	console.log('EN Save hpRestauraValoresDefaultClick strValue: ', strValue);
    	console.log('EN Save hpRestauraValoresDefaultClick lstDistrRelPaso: ', lstDistrRelPaso);

        let lWrpMapDatosDistCapPaso = objComponent.get("v.lWrpMapDatosDistCap");
    	console.log('EN Save hpRestauraValoresDefaultClick lWrpMapDatosDistCapPaso: ', lWrpMapDatosDistCapPaso);
    		        
        let msgTexto = 'Las reglas de colaboración actuales seran eliminadas y regresaras a la configuración por default para este distribuidor: ' + strValue + ', estas seguro?';	
        let blnRes = confirm(msgTexto);
        if (blnRes == true) {        	
        	//Llama a la funcion de eliminar los datos del distribuidor strValue
	        this.fnRestauraValoresDefault(objComponent, objEvent, strValue);
        }//Fin si  blnRes == true
        	        		   		
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    fnRestauraValoresDefault: function(objComponent, objEvent, strCodDistPrm) {
    	console.log('EN Save fnRestauraValoresDefault strCodDistPrm: ', strCodDistPrm);

        let optDistRel = [];
        let optDistRelSelPaso = [];	
        let opTFuncDistPrinc = [];
        let optTipInvPrinc = [];
        
      	//Llama a la funcion de eliminar los datos del distribuidor strValue
        let objAction = objComponent.get("c.clsRestauraValoresDefault");
        objAction.setParams({
        	"strCodDistPrm" : strCodDistPrm
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();
            	if (objWrpResAct.blnEstatus == false){
			        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
			   		let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
			   		//Recorre la lista de reg de lClientesDealer
			   		for (let objDistPaso in lWrpMapDatosDistPaso){
			    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == strCodDistPrm){
		    				lWrpMapDatosDistPaso[objDistPaso].blnMuestra = false;
		    				//Inicializa la lista de setDistrRel
		    				lWrpMapDatosDistPaso[objDistPaso].setDistrRel = optDistRel; //items;
		    				console.log('EN Save hpDespliegaDatosDist setDistrRel: ', lWrpMapDatosDistPaso[objDistPaso].setDistrRel);   
		    				//Inicializa la lista de setSelectDistrRel
    						lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel = optDistRelSelPaso;
		    				console.log('EN Save hpDespliegaDatosDist setSelectDistrRel: ', lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel);    				
		    				//Inicializa la lista de setFuncDistPrinc
		    				lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc = opTFuncDistPrinc;
		    				console.log('EN Save hpDespliegaDatosDist setFuncDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc);   
		    				//Inicializa la lista de setTiposInvenDistPrinc
		    				lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc = optTipInvPrinc;
		    				console.log('EN Save hpDespliegaDatosDist setTiposInvenDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc);
		    				//Inicializa strCodDistSel
		    				objComponent.set("v.strCodDistSel", strCodDistPrm);
			    			console.log('EN Save hpDespliegaDatosDist lWrpMapDatosDistPaso[objDistPaso].blnMuestra: ', lWrpMapDatosDistPaso[objDistPaso].blnMuestra);
			    			//Salte	
			    			break;    		
			    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
			   		}//Fin del for para llClientesDealerPaso
			   		//Despliega el mensaje de confirmación		
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
            	}//Fin si objWrpResAct.blnEstatus == false
            	//Si objWrpResAct.blnEstatus == true
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
 			}
        });
        $A.enqueueAction(objAction);   		   		
    },
	
    //Funcion que sirve para seleccionar el tipo de Excepción
    actDistRel: function(objComponent, objEvent) {
    	console.log('EN actDistRel...');
    	
    	let optDistRelSel = [];
    	let strCodDistSelPaso = objComponent.get("v.strCodDistSel");
		let distSelecList = objEvent.getParam("value");
    	let optDistRelSelDel = [];
		
		let paramStr = JSON.stringify(objEvent.getParams(), null, 4); 
		console.log('EN Save actDistRel paramStr:  ', paramStr);
    	console.log('EN Save actDistRel strCodDistSelPaso: ', strCodDistSelPaso);
    	console.log('EN Save actDistRel distSelecList: ', distSelecList);

        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
   		let lWrpMapDatosDistCap = objComponent.get("v.lWrpMapDatosDistCap");
   		let lDealesDelPaso = objComponent.get("v.lDealesDel");
   		
   		let sNumeroDist = '';
   		let blnExiste = false;
   		
    	//Recorre la lista de reg y busca distSelecList
   		for (let objDistPaso in lWrpMapDatosDistPaso){
   			let setSelectDistrRelPaso = lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel;
   			//console.log('EN Save actDistRel length: ', setSelectDistrRelPaso.length);
   			if (distSelecList.length > 0){
				for (let codDistIdSel in setSelectDistrRelPaso){
					console.log('EN Save actDistRel YA LO ECONTRE : ' + setSelectDistrRelPaso[codDistIdSel]);
					//Recorre la lista de distSelecList y compara el dato en setSelectDistrRelPaso
					for (let codDistIdDel in distSelecList){
						if (setSelectDistrRelPaso[codDistIdSel] == distSelecList[codDistIdDel]){
							sNumeroDist = lWrpMapDatosDistPaso[objDistPaso].NumeroDist;
							blnExiste = true;
							break;
						}//Fin si setSelectDistrRelPaso[codDistIdSel] == distSelecList[codDistIdDel]
					}//Fin del for para distSelecList
				}//Fin si
   			}//Fin si distSelecList.length > 0
   		}//Fin del for para llClientesDealerPaso
   		
   		//Ya esta vacio el distSelecList
		if (distSelecList.length == 0){
			for (let codDistIdCap in lWrpMapDatosDistCap){
				//Recorre la lista de distSelecList y compara el dato en setSelectDistrRelPaso
				for (let codDistIdDel in lWrpMapDatosDistPaso){
					console.log('EN Save actDistRel NumeroDist0: ' + lWrpMapDatosDistCap[codDistIdCap].NumeroDist + ' NumeroDist1: ' + lWrpMapDatosDistPaso[codDistIdDel].NumeroDist);
					if (lWrpMapDatosDistCap[codDistIdCap].NumeroDist == lWrpMapDatosDistPaso[codDistIdDel].NumeroDist){
						//Ve si la lista de setSelectDistrRel esta vacia
						if (lWrpMapDatosDistPaso[codDistIdDel].setSelectDistrRel.length == 0){
							optDistRelSelDel.push(lWrpMapDatosDistCap[codDistIdCap].NumeroDist);
							lDealesDelPaso.push(optDistRelSelDel);
							objComponent.set("v.lDealesDel", lDealesDelPaso);
							break;
						}//Fin si lWrpMapDatosDistPaso[codDistIdDel].setSelectDistrRel.length == 0
					}//Fin si setSelectDistrRelPaso[codDistIdSel] == distSelecList[codDistIdDel]
				}//Fin del for para distSelecList
			}//Fin si
		}//Fin si distSelecList.length > 0
    	console.log('EN Save actDistRel sNumeroDist: ' + sNumeroDist + ' blnExiste: ' + blnExiste + ' optDistRelSelDel: ' + optDistRelSelDel);
    	console.log('EN Save actDistRel lDealesDelFinal: ' + objComponent.get("v.lDealesDel"));
				
   		//Recorre la lista de reg de lClientesDealer
   		for (let objDistPaso in lWrpMapDatosDistPaso){
    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == sNumeroDist){
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let codDistId in distSelecList){
   					optDistRelSel.push(distSelecList[codDistId]);
   				}//Fin si
   				//Inicializa la lista de listopTFuncDistPrinc
   				lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel = optDistRelSel;
   				lWrpMapDatosDistPaso[objDistPaso].blnModDistRel = true;
   				console.log('EN actDistRel setSelectDistrRel.setSelectDistrRel: ', lWrpMapDatosDistPaso[objDistPaso].setSelectDistrRel);
   				console.log('EN actDistRel setSelectDistrRel.NumeroDist: ', lWrpMapDatosDistPaso[objDistPaso].NumeroDist);
    			//Salte	
    			break;    		
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso
   		
   		/*//Recorre la lista de reg de lClientesDealer
   		for (let objDistPaso in lWrpMapDatosDistPaso){   			
   			//Recorre la lista de setFuncDistPrinc del dist 
   			for (let codDistId in distSelecList){
   				if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == codDistId){
   					lWrpMapDatosDistPaso[objDistPaso].blnTieneReglas = true;
   					console.log('EN actDistRel setSelectDistrRel.setSelectDistrRel: ' + lWrpMapDatosDistPaso[objDistPaso].NumeroDist + ' codDistId: ' + codDistId);
   				}
   			}//Fin si
   		}//Fin del for para llClientesDealerPaso*/   		
   		
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    actFuncPrinc: function(objComponent, objEvent) {
    	console.log('EN actFuncPrinc...');
    	
    	let optFuncSel = [];
    	let strCodDistSelPaso = objComponent.get("v.strCodDistSel");
		let distSelecList = objEvent.getParam("value");
    	console.log('EN Save actFuncPrinc strCodDistSelPaso: ', strCodDistSelPaso);
    	console.log('EN Save actFuncPrinc distSelecList: ', distSelecList);
		
        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
   		//Recorre la lista de reg de lClientesDealer
   		for (let objDistPaso in lWrpMapDatosDistPaso){
    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == strCodDistSelPaso){
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let codDistId in distSelecList){
   					optFuncSel.push(distSelecList[codDistId]);
   				}//Fin si
   				//Inicializa la lista de listopTFuncDistPrinc
   				lWrpMapDatosDistPaso[objDistPaso].setSelectFuncDistPrinc = optFuncSel;
   				console.log('EN actFuncPrinc setSelectFuncDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setSelectFuncDistPrinc);
    			//Salte	
    			break;    		
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso
		
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    actTipoInvPrinc: function(objComponent, objEvent) {
    	console.log('EN actTipoInvPrinc...');
    	
    	let optFuncSel = [];
    	let strCodDistSelPaso = objComponent.get("v.strCodDistSel");
		let distSelecList = objEvent.getParam("value");
    	console.log('EN Save actTipoInvPrinc strCodDistSelPaso: ', strCodDistSelPaso);
    	console.log('EN Save actTipoInvPrinc distSelecList: ', distSelecList);
		
        //Toma la lista de lWrpMapDatosDist y busca el que le corresponde a strLabel
   		let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
   		//Recorre la lista de reg de lClientesDealer
   		for (let objDistPaso in lWrpMapDatosDistPaso){
    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == strCodDistSelPaso){
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let codDistId in distSelecList){
   					optFuncSel.push(distSelecList[codDistId]);
   				}//Fin si
   				//Inicializa la lista de listopTFuncDistPrinc
   				lWrpMapDatosDistPaso[objDistPaso].setSelectTiposInvenDistPrinc = optFuncSel;
   				console.log('EN actTipoInvPrinc setSelectTiposInvenDistPrinc: ', lWrpMapDatosDistPaso[objDistPaso].setSelectTiposInvenDistPrinc);
    			//Salte	
    			break;    		
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso
		
    },

    /** hpCreaColaboracionClick */
    hpCreaColaboracionClick : function(objComponent, objEvent){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpCreaColaboracionClick...");

    	let strDistSelecValue  = objEvent.getSource().get("v.value");    	
    	console.log('EN TAM_AdminVisibInventarioToyota hpCreaColaboracionClick strDistSelecValue: ', strDistSelecValue);

    	let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
    	let lDealesDelFinal = objComponent.get("v.lDealesDel");
    	console.log('EN TAM_AdminVisibInventarioToyota hpCreaColaboracionClick lDealesDelFinal: ', lDealesDelFinal);

        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.acCreaColaboraDealer");
        objAction.setParams({
        	"strDistSelecPrm" : strDistSelecValue,
        	"lDealesDelFinalPrm" : JSON.stringify(lDealesDelFinal),
        	"lWrpMapDatosDistPrm" : JSON.stringify(lWrpMapDatosDistPaso)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();
            	if (objWrpResAct.blnEstatus == false){
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
            	}
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
 			}
        });
        $A.enqueueAction(objAction);

    },

    /** hpActDefGptPub */
    hpActDefGptPub : function(objComponent, objEvent){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpActDefGptPub...");

    },

    /** hpActDefGpoPub */
    hpActDefGpoPub : function(objComponent, objEvent){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.hpActDefGpoPub...");
        let strLabel = objEvent.getSource().get("v.label");
        let strName = objEvent.getSource().get("v.name");
        let strValue = objEvent.getSource().get("v.value");		
		console.log("EN TAM_AdminVisibInventarioToyota Helper.hpActDefGpoPub strLabel: ", strLabel);
		console.log("EN TAM_AdminVisibInventarioToyota Helper.hpActDefGpoPub strName: ", strName);
		console.log("EN TAM_AdminVisibInventarioToyota Helper.hpActDefGpoPub strValue: ", strValue);
		
		//Ve si activo la colaboración por defautl
		if (strValue){
       		let strMsgAdvert = 'Si activas la colaboración por default.\n';
       		strMsgAdvert += 'Todas las reglas aplicadas a este distribuidor seran elimindadas.\n';
       		strMsgAdvert += 'Y la colaboración por default será recalculada.';
       		this.showToastWarning(objComponent, Event, strMsgAdvert);			
		}//Fin si strValue

		//Ve si activo la colaboración por defautl
		if (!strValue){
       		let strMsgAdvert = 'Si desactivas la colaboración por default.\n';
       		strMsgAdvert += 'El usuario tendrá que crear sus propias reglas de colaboración.';
       		this.showToastWarning(objComponent, Event, strMsgAdvert);			
		}//Fin si strValue
		
		//Recorre la lista de lWrpMapDatosDist y busca al que corresponde a strName
    	let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
    	//Recorre la lista de lWrpMapDatosDistPaso y seleciona los que cambiaron
   		for (let objDistPaso in lWrpMapDatosDistPaso){
   			//console.log("EN TAM_AdminVisibInventarioToyota NumeroDist: ", lWrpMapDatosDistPaso[objDistPaso].NumeroDist);
    		if (lWrpMapDatosDistPaso[objDistPaso].NumeroDist == strName){
    			lWrpMapDatosDistPaso[objDistPaso].blnTieneReglas = false;
    			console.log("EN TAM_AdminVisibInventarioToyota YA LO ENCONTRO NumeroDist: ", lWrpMapDatosDistPaso[objDistPaso].NumeroDist);
    			objComponent.set("v.lWrpMapDatosDist", lWrpMapDatosDistPaso);
    			break;
    		}//Fin si
   		}//Fin del for lWrpMapDatosDistPaso
   		
    },

    /** getUserAdminInven */
    heActualizarDatos : function(objComponent, objEvent){
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.heActualizarDatos...");

    	//let lWrpMapDatosDistPaso = JSON.stringify(objComponent.get("v.lWrpMapDatosDist"));
    	let lWrpMapDatosDistPaso = objComponent.get("v.lWrpMapDatosDist");
    	let lWrpMapDatosDistFinal = [];
    	let lDealesDelFinal = objComponent.get("v.lDealesDel");
    	
    	//Recorre la lista de lWrpMapDatosDistPaso y seleciona los que cambiaron
   		for (let objDistPaso in lWrpMapDatosDistPaso){
    		if (lWrpMapDatosDistPaso[objDistPaso].blnModDistRel){
				console.log('EN actTipoInvPrinc objWrpMapDatosDist0.NumeroDist: ' , lWrpMapDatosDistPaso[objDistPaso].NumeroDist);
				console.log('EN actTipoInvPrinc objWrpMapDatosDist0.blnDefaultColaboraGrupo: ' + lWrpMapDatosDistPaso[objDistPaso].blnDefaultColaboraGrupo);
				
				//Declara las listas que se requieren	
		    	let optFuncDistPrinc = [];
		    	let optTiposInvenDistPrinc = [];
		    	let optDistrRel = [];
    			
    			//Para la lista de setFuncDistPrinc
    			let setFuncDistPrincPaso = lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc;
    			let arrFuncDistPrincPaso = ''; //'{';
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let sFuncDistPrinc in setFuncDistPrincPaso){
   					let objFuncDistPrincPaso = setFuncDistPrincPaso[sFuncDistPrinc];   					
   					console.log('EN actTipoInvPrinc value: ', objFuncDistPrincPaso.value);
   					arrFuncDistPrincPaso += '"' + objFuncDistPrincPaso.value + '",';
   				}//Fin si
   				//arrFuncDistPrincPaso += ''; //'}';
   				optFuncDistPrinc.push(arrFuncDistPrincPaso);
				console.log('EN actTipoInvPrinc arrFuncDistPrincPaso: ', arrFuncDistPrincPaso);

    			//Para la lista de setTiposInvenDistPrinc
    			let setTiposInvenDistPrincPaso = lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc;
    			let arrsetTiposInvenDistPrincPaso = ''; //'{';
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let sTiposInvenDistPrinc in setTiposInvenDistPrincPaso){
   					let objTiposInvenDistPrincPaso = setTiposInvenDistPrincPaso[sTiposInvenDistPrinc];   					
   					console.log('EN TiposInvenDistPrinc value: ', objTiposInvenDistPrincPaso.value);
   					arrsetTiposInvenDistPrincPaso += '"' + objTiposInvenDistPrincPaso.value + '",';
   				}//Fin si
   				//arrsetTiposInvenDistPrincPaso += ''; //'}';
   				optTiposInvenDistPrinc.push(arrsetTiposInvenDistPrincPaso);
				console.log('EN actTipoInvPrinc arrsetTiposInvenDistPrincPaso: ', arrsetTiposInvenDistPrincPaso);

    			/*//Para la lista de setDistrRel
    			let setDistrRelPaso = lWrpMapDatosDistPaso[objDistPaso].setDistrRel;
    			let arrDistrRelPaso = '{';
   				//Recorre la lista de setFuncDistPrinc del dist 
   				for (let sDistrRel in setDistrRelPaso){
   					let objDistrRelPaso = setDistrRelPaso[sDistrRel];   					
   					console.log('EN TiposInvenDistPrinc objDistrRelPaso: ', objDistrRelPaso);
   					arrDistrRelPaso += '"' + objDistrRelPaso + '",';
   				}//Fin si
   				arrDistrRelPaso += '}';
   				optDistrRel.push(arrDistrRelPaso);
				console.log('EN actTipoInvPrinc arrDistrRelPaso: ', arrDistrRelPaso);*/
				
   				//Inicializa la lista de listopTFuncDistPrinc
   				lWrpMapDatosDistPaso[objDistPaso].setFuncDistPrinc = optFuncDistPrinc;
   				lWrpMapDatosDistPaso[objDistPaso].setTiposInvenDistPrinc = optTiposInvenDistPrinc;
   				lWrpMapDatosDistPaso[objDistPaso].setDistrRel = optDistrRel;
   				
   				//Agregalo a la lista final de los que vas a procesar lWrpMapDatosDistFinal
   				lWrpMapDatosDistFinal.push(lWrpMapDatosDistPaso[objDistPaso]);
				//console.log('EN actTipoInvPrinc objWrpMapDatosDist1: ' + JSON.stringify(lWrpMapDatosDistPaso[objDistPaso]));   				
   					
    		}//Fin si llClientesDealerPaso[objDistPaso].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso    	
    	console.log("EN TAM_AdminVisibInventarioToyota Helper.lWrpMapDatosDistFinal " + JSON.stringify(lWrpMapDatosDistFinal));
        	
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.acActualizarDatos");
        objAction.setParams({
        	"slWrpMapDatosDistPasoPrm" : JSON.stringify(lWrpMapDatosDistPaso), //lWrpMapDatosDistFinal
        	"slDealesDelFinalPrm" : JSON.stringify(lDealesDelFinal)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();
            	if (objWrpResAct.blnEstatus == false){
            		objComponent.set("v.blnConfirmaDatosFact",true);
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
            	}
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
 			}
        });
        $A.enqueueAction(objAction);

	}, 

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
      	console.log("EN showToastSuccess strMensaje: ", strMensaje);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 1000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
      	console.log("EN showToastError strMensaje: ", strMensaje);    
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2000,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Toast Error */
   showToastWarning: function(Component, Event, strMensaje) {
    let toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Advertencia",
        "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
        "duration": 7000,
        "type": "warning"
    });
    toastEvent.fire();
   },

})