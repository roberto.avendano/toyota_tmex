({
    consultaDeBono : function(component, event, helper) {
        let serie = component.get("v.selectedRecord.Serie");
        let codigoModelo = component.get("v.selectedRecord.codigoModelo");
        let anioModelo = component.get("v.selectedRecord.anioModelo");
        let VIN = component.get("v.selectedRecord.VIN");
        
        let action = component.get("c.consultarBonosVIN");
        action.setParams({
            serie : serie,
            codigoModelo : codigoModelo,
            anioModelo : anioModelo,
            VIN : VIN
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuestaBonos = response.getReturnValue();
                component.set("v.listaBonos",respuestaBonos);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    bonoCorrespondiente : function(component, event, helper) {
        let serie = component.get("v.selectedRecord.Serie");
        let codigoModelo = component.get("v.selectedRecord.codigoModelo");
        let anioModelo = component.get("v.selectedRecord.anioModelo");
        let VIN = component.get("v.selectedRecord.VIN");
        let fechaVenta =  component.get("v.selectedRecord.fechaVentaVIN");
        let tipoSolicitud;
        let action
        
        let vinDealerDaily =  component.get("v.selectedRecord.vinDealerDaily");
        let vinDealerInventarioG =  component.get("v.selectedRecord.vinBDDG");
        
        if(vinDealerDaily == true && vinDealerInventarioG == false){
            tipoSolicitud  = 'VINDealerDaily';
            
        }
        
        if(vinDealerDaily == false && vinDealerInventarioG == true){
            tipoSolicitud  = 'VINInventarioG';
            
        }
        
        if(tipoSolicitud  == 'VINDealerDaily'){
            action = component.get("c.getIncentivoCorrespondienteFechaVenta");
            
            action.setParams({
                serie : serie,
                codigoModelo : codigoModelo,
                anioModelo : anioModelo,
                VIN : VIN,
                fechaVenta : fechaVenta
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuestaBonos = response.getReturnValue();
                    component.set("v.bonoCorrespondiente",respuestaBonos);
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        //EL VIN ES DE INVENTARIO G
        if(tipoSolicitud  == 'VINInventarioG'){
            action = component.get("c.getIncentivoCorrespondienteVIN");
            
            action.setParams({
                serie : serie,
                codigoModelo : codigoModelo,
                anioModelo : anioModelo,
                VIN : VIN,
                fechaVenta : fechaVenta
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuestaBonos = response.getReturnValue();
                    component.set("v.bonoCorrespondiente",respuestaBonos);
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
    },    
    
    
    guardaSolicitud : function(component, event, helper) {
        
        let VIN = component.get("v.selectedRecord.VIN");
        let serie = component.get("v.selectedRecord.Serie");
        let codigoModelo = component.get("v.selectedRecord.codigoModelo");
        let anioModelo  =  component.get("v.selectedRecord.anioModelo");
        let codigoDealer =  component.get("v.selectedRecord.codigoDealer");
        let nombreDealer = component.get("v.selectedRecord.nombreDealer");
        let cumpleCondiciones = component.get("v.cumpleCondiciones");
        let fechaVenta  =  component.get("v.selectedRecord.fechaVentaVIN");
        let comentarioDelaer = component.get("v.comentario");
        let idBonoSolicitado = component.get("v.bonoSeleccionadoId");
        let codigoVenta = component.get("v.selectedRecord.codigoVenta");
        let idBonoCorrespondiente = component.get("v.bonoCorrespondiente.idBono");
        
        let statusVIN;
        let vinDealerDaily = component.get("v.selectedRecord.vinDealerDaily");
        let vinInventario  = component.get("v.selectedRecord.vinBDDG");
        
        if(vinDealerDaily == true && vinInventario == false){
            statusVIN = 'VIN en Dealer Daily';
            
        }
        if(vinDealerDaily == false && vinInventario == true){
            statusVIN = 'VIN en Inventario';
            
        }
        
        let BonoSeleccionado;
        let pickListBonoElegido = component.get("v.bonoSeleccionadoEfectivoOFinanciero"); 
        
        if(pickListBonoElegido != 'emptyOpcion'){
            BonoSeleccionado = pickListBonoElegido;
            
        }
        
        //Logica de asignación de solicitud
        let asignacionVIN;
        let tipoMovimiento;
        let VINDealerDaily = component.get("v.selectedRecord.vinDealerDaily");
        let VINInventarioG = component.get("v.selectedRecord.vinBDDG");
        
        if(VINDealerDaily == true && VINInventarioG == false && codigoVenta == '01' &&(idBonoCorrespondiente == idBonoSolicitado)){
            asignacionVIN = 'Aprobado Estado Cuenta (Finanzas)';
            tipoMovimiento = 'Sin Excepción';
        }
        
        if(VINDealerDaily == true && VINInventarioG == false && codigoVenta == '01' &&(idBonoCorrespondiente != idBonoSolicitado)){
            asignacionVIN = 'En proceso aprobación (Ventas)';	
            tipoMovimiento = 'Con Excepción';
        }
        
        if(VINDealerDaily == true && VINInventarioG == false && codigoVenta != '01'){
            asignacionVIN = 'En proceso aprobación (Ventas)';
            tipoMovimiento = 'Con Excepción';
        }
        
        
        if(VINDealerDaily == false && VINInventarioG == true){
            asignacionVIN = 'En proceso aprobación (Ventas)';
            tipoMovimiento = 'Con Excepción';
        }
        
        let mapa = component.get("v.candidate");
        let sanitizeComentario = comentarioDelaer.replace('</p>',' ');
        var newMap = { 
            
            VIN: VIN, 
            TAM_Serie: serie,
            TAM_codigoModelo : codigoModelo,
            TAM_anioModelo	: anioModelo,
            TAM_codigoDealer : codigoDealer,
            TAM_nombreDealer : nombreDealer,
            TAM_cumpleConCondiciones : cumpleCondiciones,
            TAM_fechaVenta : fechaVenta,
            TAM_comentarioDelaer : sanitizeComentario.replace('<p>',' '),
            TAM_bonoSolicitado  : idBonoSolicitado,
            TAM_estatusVIN 	: statusVIN,
            TAM_bonoElegido : BonoSeleccionado,
            TAM_codigoVenta : codigoVenta,
            TAM_BonoCorrespondiente : idBonoCorrespondiente

        };
        
        let objSolicitudBono =  Object.assign(mapa, newMap);
        
        let action = component.get("c.guardaSolicitudBono");
        
        action.setParams({
            objSolicitudBono : JSON.stringify(objSolicitudBono),
            asignacionVIN    : asignacionVIN,
            tipoMovimiento 		: tipoMovimiento
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.selectedRecord",null);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
                
                var idRegistro = response.getReturnValue(); 
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": idRegistro,
                    "slideDevName": "related"
                });
                navEvt.fire();
                
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    getBonoFromIdBono : function(component, event, helper) {
        let idDetalleBonoSeleccionado =  component.get("v.bonoSeleccionadoId");
        let action = component.get("c.obtieneResumenBonoSeleccionado");
        action.setParams({
            idDetalleBonoSeleccionado : idDetalleBonoSeleccionado
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resumenBonoSeleccionado = response.getReturnValue();
                component.set("v.resumenBonoSeleccionado",resumenBonoSeleccionado);
                let nombreBono =  component.get("v.nombreBono");
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    }
    
})