({
    doInit : function(component, event, helper) {
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        component.set("v.recordId",userId);
        
    },
    
    consultaBonos : function(component, event, helper) {
        helper.consultaDeBono(component, event, helper);
        helper.bonoCorrespondiente(component, event, helper);
        
        
    },
    
    selectBono  : function(component, event, helper) {
        var valueSelectBono 			= event.getSource().get("v.value");
        var nameSelectBono 				= event.getSource().get("v.name");
        var validaBono;
        
        component.set("v.validaPickListBono",false);
        if(valueSelectBono == 'emptyOpcion'){
            component.set("v.validaPickListBono",false);
            validaBono = false;
        }else{
            //component.set("v.bonoSeleccionadoEfectivoOFinanciero",valueSelectBono);
            component.set("v.validaPickListBono",true);  
            component.set("v.bonoSeleccionadoId",valueSelectBono);
            component.set("v.nombreBono",nameSelectBono);
            validaBono = true;
        }
        
        if(validaBono == true){
            helper.getBonoFromIdBono(component, event, helper);
            
        }
        
    },
    
    seleccionBonoEfectivoOFinanciero : function(component, event, helper) {
        var valueSelectEfectivoOFinanciero			= event.getSource().get("v.value");
        var nameSelectEfectivoOFinanciero			= event.getSource().get("v.name");
        var validaSeleccionBono;
        
        if(valueSelectEfectivoOFinanciero == 'emptyOpcion'){
            component.set("v.validaPickListEfectivoOFinanciero",false);
            validaSeleccionBono = false;
        }else{
            component.set("v.validaPickListEfectivoOFinanciero",true);  
            component.set("v.bonoSeleccionadoEfectivoOFinanciero",valueSelectEfectivoOFinanciero);
            validaSeleccionBono = true;
        }      
        
    },
    
    
    cancelar : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        component.set("v.isIncentivo",false);
        component.set("v.isPrecio",false);
        navEvent.setParams({
            "listViewName": null,
            "scope": "TAM_SolicitudDeBono__c"
        });
        navEvent.fire();
        
        
        
    },
    
    guardarRegistro: function(component, event, helper) {
        helper.guardaSolicitud(component, event, helper);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    }
    
})