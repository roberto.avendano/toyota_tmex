({
 
    /* Funcion de inicializado que obtiene la información a Solicitud Pedido Especial y Solicitud Pedido Inventario*/
    initializeComponent : function(objComponent, objEvent) {
        console.log('EN initializeComponent...');
        
        let objAction = objComponent.get("c.getTipoRegFlotilla");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let sTipoRegFlotillaPaso = objResponse.getReturnValue();
                if(sTipoRegFlotillaPaso != null)
                    objComponent.set("v.sTipoRegFlotilla", sTipoRegFlotillaPaso);
                //Llama al los metodos para consultar los datos		
		        this.obtDistribuidores(objComponent, objEvent);
		        this.obtSolicitudCompra(objComponent, objEvent);
		        this.obtRegCheckout(objComponent, objEvent);
		        this.obtDetalleOrdenCompra(objComponent, objEvent);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
        
    },
    
    //Obtener lista de distribuidores disponibles.
    obtDetalleOrdenCompra : function(objComponent, objEvent) {
    	console.log('EN obtDetalleOrdenCompra...');    	
        let objAction = objComponent.get("c.getDetalleOrdenCompra");
        objAction.setParams({
        	"solicitudId" : objComponent.get("v.recordId") 
        });
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                objComponent.set("v.lstData", objResponse.getReturnValue());
                let lstDistribuidores = objComponent.get("v.lstDistribuidores");
                console.log('EN obtDetalleOrdenCompra lstDistribuidores: ', lstDistribuidores);    	
                let lstData = objComponent.get("v.lstData");
                //Recorre la lista de lstData        
                for(let key in lstData){
                    for(let key2 in lstDistribuidores){
                        if(this.strContainsStr(lstDistribuidores[key2].TAM_IdExterno__c,lstData[key].strIdExterno))
                            lstData[key].strDistribuidor = lstDistribuidores[key2].TAM_NombreDistribuidor__c;
                            //lstData[key].strDistribuidor = lstDistribuidores[key2].TAM_Cuenta__r.Name;
                    }
                }
                objComponent.set("v.lstData",lstData);
                objComponent.set("v.showTd",true);
                let lstDataFinal = objComponent.get("v.lstData");
                if (lstDataFinal.length == 0)
                	objComponent.set("v.bSolCerrada",true);
                console.log('EN obtDetalleOrdenCompra lstData: ', lstDataFinal);
                console.log('EN obtDetalleOrdenCompra lstDataFinal.length(): ', lstDataFinal.length);
                console.log(objResponse.getReturnValue());
                console.log(lstDistribuidores);
            }
        });
        $A.enqueueAction(objAction);

    },
    
    //Obtener lista de distribuidores disponibles.
    obtTipoRegFlotilla : function(objComponent, objEvent) {
    	console.log('EN obtTipoRegFlotilla...');
    	let sTipoRegFlotillaPaso = '';
    	
        let objAction = objComponent.get("c.getTipoRegFlotilla");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                sTipoRegFlotillaPaso = objResponse.getReturnValue();
                if(sTipoRegFlotillaPaso != null)
                    objComponent.set("v.sTipoRegFlotilla", sTipoRegFlotillaPaso);
                //console.log('EN obtTipoRegFlotilla sTipoRegFlotilla: ', objComponent.get("v.sTipoRegFlotilla"));            
            }
        });
        $A.enqueueAction(objAction);
    },
    
    strContainsStr: function( str1, str2){
        return ( str1.indexOf(str2) != -1 ? true : false);
    },
    
    //Obtener lista de distribuidores disponibles.
    obtDistribuidores : function(objComponent, objEvent) {
    	console.log('EN obtDistribuidores...');    
        let objAction = objComponent.get("c.getDistribuidoresFlotilla");
        objAction.setParams(
        	{  
        		"solicitudEspId" : objComponent.get("v.recordId")  
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstDistribuidores = objResponse.getReturnValue();
                if(lstDistribuidores != null){
                    objComponent.set("v.lstDistribuidores",lstDistribuidores);
                }
                console.log(lstDistribuidores);
            }
        });
        $A.enqueueAction(objAction); 
    },
    
    obtSolicitudCompra : function(objComponent, objEvent) {
    	console.log('EN obtSolicitudCompra...');    
    
        let objAction = objComponent.get("c.getSolicitud");
        objAction.setParams(        	{  
        	"solicitudEspId" : objComponent.get("v.recordId")  
        });
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                console.log('EN obtSolicitudCompra Solicitud: ', objSolicitud);
                objComponent.set("v.Solicitud", objSolicitud);
                
                let nombreCliente = objSolicitud.TAM_Cliente__r.Name;
                let nombreClienteLong = nombreCliente.length;
                let nombreClienteFinalCorto = nombreCliente.substring(0,35);
                let nombreClienteFinalLargo = nombreCliente.substring(0,nombreClienteLong);
                objComponent.set("v.sNombreClienteCorto", nombreClienteFinalCorto);
                objComponent.set("v.sNombreClienteLargo", nombreClienteFinalLargo);
                if (objSolicitud.TAM_Estatus__c == 'Cerrada'){
                  	objComponent.set("v.bSolCerrada", true);
                	objComponent.set("v.bEnviarCorreo", false);	     
                }
                console.log('EN obtSolicitudCompra bSolCerrada: ', objComponent.get("v.bSolCerrada"));
            }
        });
        $A.enqueueAction(objAction);
    },
            
    //Obtener lista de distribuidores disponibles.
    obtRegCheckout : function(objComponent, objEvent) {
    	console.log('EN obtRegCheckout...');
    	
        let objAction = objComponent.get("c.getReistrosCheckOutSolicitud");
        objAction.setParams(
        {  
        	"solicitudEspId" : objComponent.get("v.recordId")  
        });
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let mapRegActSolPaso = objResponse.getReturnValue();
                if(mapRegActSolPaso != null)
                    objComponent.set("v.mapRegActSol",mapRegActSolPaso);
                console.log('EN obtRegCheckout mapRegActSolPaso: ', mapRegActSolPaso);
            }
        });
        $A.enqueueAction(objAction); 
    },

    SeleccionarPoliticasIncentivos: function(objComponent, objEvent) {
    	console.log('EN SeleccionarPoliticasIncentivos...');
    	
    	let strPorcentaje = '% 0.00';
    	let strMonto = '$ 0.00';
        let indexvar = objEvent.getSource().get("v.label");
        let value = objEvent.getSource().get("v.value");
        let lista = objComponent.get("v.lstData");
        let seleccionado = lista[parseInt(indexvar)];
        let idExternoPaso = seleccionado.strModelo + '' + seleccionado.strAnio;
        
        console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos indexvar: ', indexvar);
        console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos value: ', value);
        console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos seleccionado: ', seleccionado);
        console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos idExternoPaso: ', idExternoPaso);

        let objAction = objComponent.get("c.getIncentivosFlotillaModelo");
        objAction.setParams({
        	"solicitudId" : objComponent.get("v.recordId"), 
        	"idExterno" : idExternoPaso
        });        
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lPoliticasIncentivosResult = objResponse.getReturnValue();
                objComponent.set("v.lPoliticasIncentivos", lPoliticasIncentivosResult);
                //Ve si se trata de Financiamiento TFS
                if (seleccionado.blnValidaTfs){ 
                	if (seleccionado.strTipoPago == 'Financiamiento TFS'){
                		let strPorDescDTM = parseFloat(lPoliticasIncentivosResult[0].TAM_IncPropuesto_Porcentaje__c);
                		let strPorDescDTMFinal = strPorDescDTM.toFixed(2);
                		let strPorDescTMEX = parseFloat(lPoliticasIncentivosResult[0].TAM_IncentivoTMEX_Porcentaje__c);
                		let strPorDescTMEXFinal = strPorDescTMEX.toFixed(2);
                		let strPorDescDLER = parseFloat(lPoliticasIncentivosResult[0].TAM_IncDealer_Porcentaje__c);
                		let strPorDescDLERFinal = strPorDescDLER.toFixed(2);
                		
                		let strMontoDescDTM = parseFloat(lPoliticasIncentivosResult[0].TAM_IncPropuesto_Cash__c);
                		let strMontoDescDTMFinal = strMontoDescDTM.toFixed(2);
                		let strMontoDescTMEX = parseFloat(lPoliticasIncentivosResult[0].TAM_IncentivoTMEX_Cash__c);
                		let strMontoDescTMEXFinal = strMontoDescTMEX.toFixed(2);
                		let strMontoDescDLER = parseFloat(lPoliticasIncentivosResult[0].TAM_IncDealer_Cash__c);
                		let strMontoDescDLERFinal = strMontoDescDLER.toFixed(2);
                		
                		console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos strPorDescDTMFinal: ' + strPorDescDTMFinal + ' strPorDescTMEXFinal: ' + strPorDescTMEXFinal + ' strPorDescDLERFinal: ' + strPorDescDLERFinal);
                		console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos strPorDescDTMFinal: ' + strPorDescDTMFinal + ' strPorDescTMEXFinal: ' + strPorDescTMEXFinal + ' strPorDescDLERFinal: ' + strPorDescDLERFinal);

	                	//Actualiza el resto de los datos 
	                	lista[parseInt(indexvar)].strIdPoliticaIncent = lPoliticasIncentivosResult[0].TAM_PoliticaIncentivos__c;
	                	lista[parseInt(indexvar)].strPorDescDTM = '% ' + strPorDescDTMFinal; //lPoliticasIncentivosResult[0].TAM_IncPropuesto_Porcentaje__c
	                	lista[parseInt(indexvar)].strPorDescTMEX = '% ' + strPorDescTMEXFinal; //lPoliticasIncentivosResult[0].TAM_IncentivoTMEX_Porcentaje__c;
	                	lista[parseInt(indexvar)].strPorDescDLER = '% ' + strPorDescDLERFinal; //lPoliticasIncentivosResult[0].TAM_IncDealer_Porcentaje__c;
	                	
	                	lista[parseInt(indexvar)].strMontoDescDTM = '$ ' + strMontoDescDTMFinal;
	                	lista[parseInt(indexvar)].strMontoDescTMEX = '$ ' + strMontoDescTMEXFinal;
	                	lista[parseInt(indexvar)].strMontoDescDLER = '$ ' + strMontoDescDLERFinal;
                	}//Fin si seleccionado.strTipoPago == 'Financiamiento TFS'
                	if (seleccionado.strTipoPago != 'Financiamiento TFS'){
	                	//Actualiza el resto de los datos 
	                	lista[parseInt(indexvar)].strIdPoliticaIncent = null;
	                	lista[parseInt(indexvar)].strPorDescDTM = strPorcentaje;
	                	lista[parseInt(indexvar)].strPorDescTMEX = strPorcentaje;
	                	lista[parseInt(indexvar)].strPorDescDLER = strPorcentaje;
	                		                	
	                	lista[parseInt(indexvar)].strMontoDescDTM = strMonto;
	                	lista[parseInt(indexvar)].strMontoDescTMEX = strMonto;
	                	lista[parseInt(indexvar)].strMontoDescDLER = strMonto;
                	}//Fin si seleccionado.strTipoPago == 'Financiamiento TFS'
                	console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos strPorDescDTM: ', lista[parseInt(indexvar)].strPorDescDTM);
                	console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos strPorDescTMEX: ', lista[parseInt(indexvar)].strPorDescTMEX);
                	console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos strPorDescDLER: ', lista[parseInt(indexvar)].strPorDescDLER);                
                }//Fin si 
                //TAM_IncPropuesto_Porcentaje__c, TAM_IncentivoTMEX_Porcentaje__c, TAM_IncDealer_Porcentaje__c,
                //TAM_IncPropuesto_Cash__c, TAM_IncentivoTMEX_Cash__c, TAM_IncDealer_Cash__c, TAM_Pago_TFS__c
                
		        //Ve si value es diferente de '--- NINGUNO --- '
		        if (value != '--- NINGUNO --- '){        
		           objComponent.set("v.showTd",false);                
		           lista[parseInt(indexvar)].strTipoPago = value;
		           console.log('EN SeleccionarPoliticasIncentivos SeleccionarPoliticasIncentivos lista[parseInt(indexvar)].strTipoPago2: ', lista[parseInt(indexvar)].strTipoPago);
		           objComponent.set("v.lstData",lista);            
		        }else{ 
		            lista[parseInt(indexvar)].strCash = '';
		            lista[parseInt(indexvar)].strPorcentaje = '';
		            lista[parseInt(indexvar)].strTipoPago = undefined;
		            objComponent.set("v.lstData",lista);
		        }
                console.log('EN obtTipoRegFlotilla sTipoRegFlotilla: ', objComponent.get("v.lPoliticasIncentivos"));            
            }
        });
        $A.enqueueAction(objAction);
        
    },
    
    /** Añadir fila en tabla */
    addRow : function(objComponent, objEvent){
        
        let lstWrapper = objComponent.get("v.lstData");
        let buttonId = objEvent.target.id;
        let objWrapperAux = lstWrapper[buttonId];
        let objWrapperClone = Object.assign({}, objWrapperAux);
        
        objWrapperClone.boolClonado = true;
        objWrapperClone.strSolicitados = undefined;
        objWrapperClone.strTipoPago = undefined;
        objWrapperClone.strCash = undefined;
        objWrapperClone.strPorcentaje = undefined;
        lstWrapper.push(objWrapperClone);
        objComponent.set("v.lstData", lstWrapper);
    },

    /** Remover fila en tabla */
    deleteRow : function(objComponent, objEvent){
        let buttonId = objEvent.target.id;
        let lstWrapper = objComponent.get("v.lstData");
        let objWrapperAux = lstWrapper[buttonId];
        let index = lstWrapper.indexOf(objWrapperAux);
        if (index > -1) {
            lstWrapper.splice(index, 1);
        }
        objComponent.set("v.lstData", lstWrapper);
    },
    
    Save : function(objComponent, objEvent){
    	console.log('EN Save SinTipoPago...');
    	
        let solicitudID =  objComponent.get("v.recordId");
        let lstWrapper = objComponent.get("v.lstData");
        let listaFiltrados = objComponent.get("v.lstVacia");
        let sCorreoAdici1Paso = objComponent.get("v.sCorreoAdici1");
        let sCorreoAdici2Paso = objComponent.get("v.sCorreoAdici2");
                
        let SinTipoPago = lstWrapper.filter(function (row) {
            return row.strTipoPago == '--- NINGUNO ---';
        });
        console.log('EN Save SinTipoPago: ', SinTipoPago);
        console.log('EN Save SinTipoPago.length: ' + SinTipoPago.length + ' length: ' + (SinTipoPago.length == 0));
        
        if(SinTipoPago.length > 0){
            this.showToastWarning(objComponent, objEvent,'Favor de seleccionar un tipo de pago');
            return;
        }
        let RowPedidoEspecial = lstWrapper.filter(function (row) {
            return row.strTipo == 'Pedido Especial';
        });
        
        let sinCantidad = RowPedidoEspecial.filter(function (row) {
            return row.strSolicitados == undefined || row.strSolicitados <= 0;
        });
       
        if(sinCantidad.length  > 0){
            this.showToastWarning(objComponent, objEvent,'Favor de capturar una cantidad valida');
            return;
        }
        
        let mapCantidad = this.createMapSumCantidad(objComponent, objEvent);
        let toSend = true;
        
        RowPedidoEspecial.forEach(function (rowPedido) {
            let solicitado = parseInt(mapCantidad.get(rowPedido.strIdExterno));
            let total = parseInt(rowPedido.strCantidad);
            console.log('EN Save rowPedido.strIdExterno: ' + rowPedido.strIdExterno + ' solicitado: ' + solicitado + ' total: ' + total);
            if(solicitado != total){
                toSend = false;
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning.",
                    "message": 'La cantidad Capturada debe de ser igual a la cantidad total para el pedido especial: ' + rowPedido.strName + ' Solicitado: ' + solicitado + ' Total: ' + total , //"Surgio un problema con el guardado, favor de contactar a su administrador.",
                    "duration": 4000,
                    "type": "warning"
                });
                toastEvent.fire();
                return;
            }
        });
        
        console.log('EN Save toSend: ', toSend);
        if(toSend){
        	console.log('EN Save sCorreoAdici1Paso: ' + sCorreoAdici1Paso);
        	console.log('EN Save sCorreoAdici2Paso: ' + sCorreoAdici2Paso);
        	//Manda llamas rl save	
            this.SendToSave(objComponent, objEvent, sCorreoAdici1Paso, sCorreoAdici2Paso);        
        }
       
    },
       
   SendToSave: function(objComponent, Event, sCorreoAdici1Paso, sCorreoAdici2Paso) {
	   console.log('EN SendToSave...: ');
	   let lstWrapper = objComponent.get("v.lstData");
	   let objAction = objComponent.get("c.SaveCheckout");
	   objAction.setParams(
		   {  
			   "solicitudId" : objComponent.get("v.recordId"), 
			   "listCheckout" : lstWrapper,
			   "sCorreoAdici1" : sCorreoAdici1Paso,
			   "sCorreoAdici2" : sCorreoAdici2Paso			   
		   }
	   );
	   objAction.setCallback(this, function(objResponse){
		   let objState = objResponse.getState();
		   if (objState === "SUCCESS"){
			   	let objWrpResAct = objResponse.getReturnValue();
	            if (objWrpResAct.blnEstatus == false){
	            	objComponent.set("v.bSolCerrada", true);
	            	objComponent.set("v.bEnviarCorreo", false);	            	
	            	this.showToastSuccess(objComponent, Event, objWrpResAct.strDetalle);
	            }//Fin si objWrpResAct.blnEstatus == false
	            if (objWrpResAct.blnEstatus == true){
	            	objComponent.set("v.bSolCerrada", false);
	            	objComponent.set("v.bEnviarCorreo", true);
	            	this.showToastError(objComponent, Event, objWrpResAct.strDetalle);	            
	            }//Fin si objWrpResAct.blnEstatus == true
		   }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);   
    },
       
   createMapSumCantidad: function(objComponent, Event) {
    let lstWrapper = objComponent.get("v.lstData");
    let myMap = new Map();
    let d = new Object();
    lstWrapper.forEach(function (rowPedido) {
        if(rowPedido.strTipo == 'Pedido Especial'){
            if(myMap.has(rowPedido.strIdExterno)){
                let solicitado = parseInt(myMap.get(rowPedido.strIdExterno)) + parseInt(rowPedido.strSolicitados == undefined ? 0: rowPedido.strSolicitados);
                myMap.set(rowPedido.strIdExterno, solicitado);
            }else{
                myMap.set(rowPedido.strIdExterno, rowPedido.strSolicitados == undefined ? 0 : rowPedido.strSolicitados);
            }
        }
        
    });
    return myMap;
   },
    
    hEnviaCorreoNotificacion: function(objComponent, Event) {
	   console.log('EN hEnviaCorreoNotificacion...: ');
	   let lstWrapper = objComponent.get("v.lstData");
	   let objAction = objComponent.get("c.enviaCorreoInfo");
	   objAction.setParams(
		   {  
			   "solicitudId" : objComponent.get("v.recordId"),
			   "sCorreoAdici1" : objComponent.get("v.sCorreoAdici1"),
			   "sCorreoAdici2" : objComponent.get("v.sCorreoAdici2")
		   }
	   );
	   objAction.setCallback(this, function(objResponse){
		   let objState = objResponse.getState();
		   if (objState === "SUCCESS"){
			   	let objWrpResAct = objResponse.getReturnValue();
	            if (objWrpResAct.blnEstatus == false)
	            	this.showToastSuccess(objComponent, Event, objWrpResAct.strDetalle);
	            if (objWrpResAct.blnEstatus == true)
	            	this.showToastError(objComponent, Event, objWrpResAct.strDetalle);
		   }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);   
    },
 
    showVinesDOD: function(objComponent, objEvent) {   
    	console.log("EN Helper.showVinesDOD..");
    	objComponent.set("v.blnShowVinesDOD", true);    
    	console.log("EN Helper.showVinesDOD blnShowVinesDOD: ", objComponent.get("v.blnShowVinesDOD"));
        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelName = objEvent.getSource().get("v.name");
        //console.log("EN Helper.showVinesDOD varObjModeloSelValue: ", JSON.stringify(varObjModeloSelValue) + ' varObjModeloSelName: ' + JSON.stringify(varObjModeloSelName));
        objComponent.set('v.objModeloSel', objEvent.getSource().get("v.value"));
        console.log("EN Helper.showVinesDOD objModeloSel Final: ", objEvent.getSource().get("v.value"));
    },
 
    /** Toast Error */
   showToastWarning: function(Component, Event, strMensaje) {
    let toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        "title": "Warning.",
        "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
        "duration": 4000,
        "type": "warning"
    });
    toastEvent.fire();
   },
 
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
    
})