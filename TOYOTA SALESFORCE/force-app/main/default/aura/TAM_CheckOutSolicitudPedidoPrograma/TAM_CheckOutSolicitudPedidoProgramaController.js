({

    /** Funcion Inicial */
    doInit : function(objComponent, objEvent, objHelper) {
        objHelper.initializeComponent(objComponent, objEvent);
    },
    
    /** Funcion de modal Distribuidores seleccionados y VIN Seleccionados */
    addDistribuidores : function(objComponent, objEvent, objHelper) {
        objHelper.addDistribuidores(objComponent, objEvent);
    },
    
    SeleccionarPoliticasIncentivos : function(objComponent, objEvent, objHelper) {
        objHelper.SeleccionarPoliticasIncentivos(objComponent, objEvent);
    },
     /** Añadir fila en la tabla */
     addRow : function (objComponent, objEvent, objHelper) {
        objHelper.addRow(objComponent, objEvent);
    },
    
    /** Añadir fila en la tabla */
    deleteRow : function (objComponent, objEvent, objHelper) {
        objHelper.deleteRow(objComponent, objEvent);
    },
    
    Save : function (objComponent, objEvent, objHelper) {
        objHelper.Save(objComponent, objEvent);
    },

    consultaVinesDOD : function(objComponent, objEvent, objHelper) {        
        objHelper.showVinesDOD(objComponent, objEvent);
    },

    CloseWindow : function(objComponent, objEvent, objHelper) {
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    SaveCloseWindow : function(objComponent, objEvent, objHelper) {
    	//Manda llamarl al metodo de guardar y y despues cierra la ventana
        objHelper.Save(objComponent, objEvent);
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    EnviaCorreoNotificacion : function(objComponent, objEvent, objHelper) {
    	//Manda llamarl al metodo de guardar y y despues cierra la ventana
        objHelper.hEnviaCorreoNotificacion(objComponent, objEvent);
    },




    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})