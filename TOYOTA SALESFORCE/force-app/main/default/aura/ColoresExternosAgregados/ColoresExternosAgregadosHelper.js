({
	showSpinner : function(component) {
		component.set('v.spinner', true);
	},

	hideSpinner : function(component) {
		component.set('v.spinner', false);
	}
})