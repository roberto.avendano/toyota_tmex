({
	doInit : function(component, event, helper) {
		var accion = component.get("c.getColoresAgregados");
		var fichaProducto = component.get("v.fichaProductoID");

		accion.setParams({
			"fichaProdID": fichaProducto
		});

		accion.setCallback(this, function(response){
			var state = response.getState();			

			if(state === "SUCCESS"){
				//console.log(response.getReturnValue());
				component.set("v.coloresList", response.getReturnValue());
			}
		});

		$A.enqueueAction(accion);
	},

	saveAggregateColors : function(component, event, helper) {
		helper.showSpinner(component);
		var accion = component.get("c.guardaColoresAgregados");
		var coloresLista = component.get("v.coloresList");
		var fichaProducto = component.get("v.fichaProductoID");

		var concatColors = '';

		if(coloresLista != null){
			if(coloresLista.length > 0){
				for(var i = 0; i < coloresLista.length; i++){
					if(coloresLista[i].selected){
						concatColors+= coloresLista[i].colorExterno.CodigoColor__c +',';
					}
				}
			}
		}
		
		accion.setParams({
			"colores": concatColors,
			"fichaProdID": fichaProducto
		});

		accion.setCallback(this, function(response){
			var state = response.getState();
			helper.hideSpinner(component);
			if(state === "SUCCESS"){
				
				//location.reload();
				/*var navEvt = $A.get("e.force:navigateToSObject");
				navEvt.setParams({
					"recordId": fichaProducto,
				});

				navEvt.fire();*/
			} else {
				
				console.log(response.getError());
			}
		});

		$A.enqueueAction(accion);

	}
})