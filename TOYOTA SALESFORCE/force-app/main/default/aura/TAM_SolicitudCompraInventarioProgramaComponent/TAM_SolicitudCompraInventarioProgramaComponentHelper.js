({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
		console.log("EN SolCompEspDist Helper.initializeComponent...");
		
		//this.showSpinner(objComponent, objEvent);
		this.getEstatusSol(objComponent, objEvent);
		this.gettipoRegistroPrograma(objComponent, objEvent);
		this.gettipoRegistroSolicitud(objComponent, objEvent);
		this.getSeries(objComponent, objEvent);

		let strIdSol = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getModelos");
        objAction.setParams({   			
        	recordId : strIdSol,
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                objComponent.set("v.lstModelos", objResponse.getReturnValue());
                this.createObjectData(objComponent, objEvent);
                //this.hideSpinner(objComponent, objEvent);                
            }
        });
        $A.enqueueAction(objAction);
		
    },

    getInventarioPiso : function(objComponent, objEvent){
    	console.log('EN getInventarioPiso...');
    	let sTipo = 'Piso'; 
        let objAction = objComponent.get("c.getInventarioPiso");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let ListInventarioPisoPaso = objResponse.getReturnValue();
                objComponent.set("v.ListInventarioPiso", ListInventarioPisoPaso);
                //console.log('EN getInventarioPiso ListInventarioPiso: ', objComponent.get("v.ListInventarioPiso"));    
            }
        });
        $A.enqueueAction(objAction); 
    },

    getInventarioTransito : function(objComponent, objEvent){
    	console.log('EN getInventarioTransito...'); 
        let objAction = objComponent.get("c.getInventarioTransito");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let ListInventarioTransitoPaso = objResponse.getReturnValue();
                objComponent.set("v.ListInventarioTransito", ListInventarioTransitoPaso);
                //console.log('EN getInventarioTransito ListInventarioTransito: ', objComponent.get("v.ListInventarioTransito"));    
            }
        });
        $A.enqueueAction(objAction); 
    },

    getEstatusSol : function(objComponent, objEvent){
    	console.log('EN getEstatusSol...'); 
    	objComponent.set("v.bSolCerrada", false);
        let objAction = objComponent.get("c.getEstatusSol");
        objAction.setParams({  
        		recordId : objComponent.get("v.recordId") 
        });  
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let sEstatusSol = objResponse.getReturnValue();
                if (sEstatusSol == 'Cerrada')
                    objComponent.set("v.bSolCerrada",true);
                console.log('EN getEstatusSol bSolCerrada: ', objComponent.get("v.bSolCerrada"));    
            }
        });
        $A.enqueueAction(objAction); 
    },

    getSeries : function(objComponent, objEvent){
    	console.log("EN SolCompEspDist Helper.getSeries...");
    	            	
        let objAction = objComponent.get("c.getSeries");
        objAction.setParams({   			
        	recordId : objComponent.get("v.recordId"),
        	bSolCerrada : objComponent.get("v.bSolCerrada")
   		});        
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let bTieneReg = false;
                let result = objResponse.getReturnValue();
                let arrayMapKeys = [];
                for(let key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                    bTieneReg = true;
                }
                console.log(arrayMapKeys.sort());
                console.log(arrayMapKeys.reverse());
                objComponent.set("v.mapValues", arrayMapKeys.sort());
               	objComponent.set("v.bSeExistenModelos", bTieneReg);                
                //objComponent.set("v.bConsultaModelos", true);
            }
        });
        $A.enqueueAction(objAction);
    },

    gettipoRegistroPrograma : function(objComponent, objEvent){
    	console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma...");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipRegPrograma");
        objAction.setParams({   			
        	strTipRegNombre : 'Programa'
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 

    gettipoRegistroSolicitud : function(objComponent, objEvent){
    	console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma...");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipoRegSol");
        objAction.setParams({   			
        	recordId : objComponent.get("v.recordId")
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 

    getSolDistribuidores : function(objComponent, objEvent){
    	console.log("EN SolCompEspDist Helper.getSolDistribuidores...");
    	let strIdSol = objComponent.get("v.recordId");
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getTipRegPrograma");
        objAction.setParams({   			
        	recordId : strIdSol
   		});                
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let strTipoRegPrograma = objResponse.getReturnValue();
                //objComponent.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspDist Helper.getSolDistribuidores strTipoRegPrograma: " + objComponent.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 


    /** */
    addDealer : function(objComponent, objEvent){
        let strButtonId = objEvent.target.id;
        console.log(strButtonId);

        //Obtener información del registro Seleccionado
        let objModelo = objComponent.get("v.objModeloSeleccionado");

        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getDistribuidores");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstDistribuidores = objResponse.getReturnValue();
                objComponent.set("v.lstDistribuidores", lstDistribuidores);
                //this.createObjectData(objComponent, objEvent);
            }
        });
        $A.enqueueAction(objAction);
        
	}, 
	
	close : function(objComponent, objEvent) {
		document.getElementById("backgroudModalId").classList.remove("slds-backdrop_open");
        document.getElementById("distribuidoresModalId").classList.remove("slds-fade-in-open");
        let RowItemList = objComponent.get("v.lstAsignacionDistribuidores");
        objComponent.set("v.lstAsignacionDistribuidores", '');
    },

    createObjectData: function(objComponent, objEvent) {
        // get the contactList from component and add(push) New Object to List  
        let RowItemList = objComponent.get("v.lstAsignacionDistribuidores");
        let lstDistribuidoes = objComponent.get("v.lstDistribuidores");
        RowItemList.push({
            'sobjectType': 'TAM_DistribuidoresFlotillaPrograma__c',
            'Name': '',
            'TAM_Cantidad__c': '',
            'TAM_DetalleSolicitudCompra_FLOTILLA__c':'',
            'TAM_Estatus__c':'Pendiente',
            'TAM_IdExterno__c':'',
            'TAM_SolicitudCompraFLOTILLA__c':'',

        });
        // set the updated list to attribute (contactList) again    
        objComponent.set("v.lstAsignacionDistribuidores", RowItemList);
    },

    showWindowPrevPedido: function(objComponent, objEvent) {
    	console.log("EN Helper.showWindowPrevPedido..");
    	objComponent.set("v.blnShowPrevPedidoCmp", true);    
    	console.log("EN Helper.showWindowPrevPedido blnShowPrevPedidoCmp: ", objComponent.get("v.blnShowPrevPedidoCmp"));
    	let mRandom = Math.floor(Math.random() * 10000000000) + 1;
    	let strRandom = mRandom.toString();
    	console.log("EN Helper.showWindowPrevPedido mRandom: ", mRandom + ' strRandom: ' + strRandom);
    	objComponent.set("v.objPrevPedido", mRandom.toString());
    	console.log("EN Helper.showWindowPrevPedido objPrevPedido Final: ", objComponent.get("v.objPrevPedido"));
    },
    
    showWindowInvVines: function(objComponent, objEvent) {
    	console.log("EN Helper.showWindowInvVines..");
    	objComponent.set("v.blnShowDistCmp", true);    
    	console.log("EN Helper.showWindowInvVines blnShowDistCmp: ", objComponent.get("v.blnShowDistCmp"));
        let varObjModeloSelValue = objEvent.getSource().get("v.value");
        let varObjModeloSelName = objEvent.getSource().get("v.name");
        //console.log("EN Helper.showWindowInvVines varObjModeloSelValue: ", JSON.stringify(varObjModeloSelValue) + ' varObjModeloSelName: ' + JSON.stringify(varObjModeloSelName));
        objComponent.set('v.objModeloSel', objEvent.getSource().get("v.value"));
        console.log("EN Helper.showWindowInvVines objModeloSel Final: ", objEvent.getSource().get("v.value"));
    },


})