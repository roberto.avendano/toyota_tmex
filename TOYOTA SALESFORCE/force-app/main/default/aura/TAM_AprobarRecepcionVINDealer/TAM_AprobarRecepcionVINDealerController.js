({
	 doInit : function(component, event, helper) {
        helper.getUserSession(component,event,helper);
        helper.getlineasAutorizar(component,event,helper);
        helper.getEstatusAprobacion(component,event,helper);
    },
    
    guardar: function(component, event, helper) {
        helper.guardarCambios(component,event,helper);
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
    
    CloseWindow : function(objComponent, objEvent, objHelper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
    
    
})