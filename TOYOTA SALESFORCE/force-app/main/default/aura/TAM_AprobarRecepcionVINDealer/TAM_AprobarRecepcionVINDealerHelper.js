({
	
    getUserSession: function(component, event,helper) {
        let recordIdSolicitud = component.get("v.recordId");
        let userId = $A.get("$SObjectType.CurrentUser.Id");   
        
        let action = component.get("c.getTypeOfUser");
        
        action.setParams(
            {  
                recordIdSolicitud : recordIdSolicitud,
                userId 			  : userId
            }
        );
        
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                component.set("v.tipoUsuario", objSolicitud);
                if(objSolicitud == true){
                    component.set('v.tituloTabla','Vehículos a Recibir');
                   
                }
                if(objSolicitud == false){
                    component.set('v.tituloTabla','Vehículos enviados a otro distribuidor');
                    
                }
                
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    getlineasAutorizar: function(component, event,helper) { 
        let recordIdSolicitud = component.get("v.recordId");
        let action = component.get("c.getLineasDealerFlotillaPrograma");
        
        action.setParams(
            {  
                recordIdSolicitud : recordIdSolicitud
            }
        );
        
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                component.set("v.lstData", objSolicitud);	     
                
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    guardarCambios : function(component, event,helper) { 
        var listaIn = component.get("v.lstData");
        let recordIdSolicitud = component.get("v.recordId");
        
        var action = component.get("c.guardarAprobacion");
        
         action.setParams(
            {  
                listaIn : listaIn,
                recordIdSolicitud : recordIdSolicitud
            }
        );
        
         action.setCallback(this, function(objResponse){
            let objState = objResponse.getState(); 
            if (objState === "SUCCESS"){
                this.showToast(component,event,helper);	    
     			var dismissActionPanel = $A.get("e.force:closeQuickAction");
        		dismissActionPanel.fire();
                
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    getEstatusAprobacion: function(component, event,helper) { 
        let action = component.get("c.valoresAprobacionDealer");
 
        action.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let objSolicitud = objResponse.getReturnValue();
                component.set("v.listaValoresAprobacion", objSolicitud);	     
                
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type" : "success",
            "title": "Exitoso!",
            "message": "Cambios guardados correctamente!"
        });
        toastEvent.fire();
    }
})