({
    waitingTimeId: null,
    changeOwnerEval: function(component){
        var UserId = $A.get( "$SObjectType.CurrentUser.Id" );
        var action = component.get("c.changeOwnerEval");
        var FechaActivacionEval = new Date().toISOString();;
        action.setParams({
            "UserId": UserId,
            "FechaActivacionEval" : FechaActivacionEval
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //   window.location.href = "https://devtm-toyotadealerconnect.cs92.force.com/ToyotaRecursosHumanos/logout.jsp";
                // $A.get('e.force:refreshView').fire();
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    setStartTimeOnUI : function(component) { 
        var fechaAssestment = component.get("v.fechaAssestment");
        var fechaDateAssestment = new Date(fechaAssestment); 
        var duracionEva = component.get("v.DuracionEvaluacion");
        var duracionExamenInt = parseInt(duracionEva);
        var Christmas = new Date(fechaAssestment);
        var fechaFinExamen = Christmas.setMinutes(Christmas.getMinutes()+duracionExamenInt);
        var fechaFinDate = new Date(fechaFinExamen);
        component.set("v.ltngIsDisplayed",true);
        var currTime =component.get("v.ltngTimmer");
        var ss = currTime.split(":");
        var dt = new Date();
        dt.setHours(ss[0]);
        dt.setMinutes(ss[1]);
        dt.setSeconds(ss[2]);
        var dt2 = new Date(dt.valueOf() - 1000);
        var temp = dt2.toTimeString().split(" ");
        var ts = temp[0].split(":");
        component.set("v.ltngTimmer",ts[0] + ":" + ts[1] + ":" + ts[2]);
        this.waitingTimeId =setTimeout($A.getCallback(() => this.setStartTimeOnUI(component)), 1850);
        //Si la fecha de hoy es mayor a la de evaluación se mandara a llamar metodo para cambiar de owner la evaluacion
        if(dt.getTime() >= fechaFinDate.getTime()){
            component.set("v.DuracionEvaluacion",0);
            component.set("v.ltngTimmer","00:00:00");
            window.clearTimeout(this.waitingTimeId);
            component.set("v.ltngIsDisplayed",false);
            //Mandar a llamar al metodo APEX . para cuando finalice el tiempo de la evaluación cambiar el password del usuario
            var UserId = $A.get( "$SObjectType.CurrentUser.Id" );
            var action = component.get("c.changeOwnerEvalAdmin");
            action.setParams({
                "UserId": UserId
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    window.location.replace("/ToyotaRecursosHumanos/secur/logout.jsp?retUrl=%2Flogin");
                }
                else if (state === "INCOMPLETE") {
                    console.log("Password actualizado");
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
            
        }
        
    }
})