({
	 /** Guardado de sección */
    saveSeccion : function(objComponent, objEvent){
        var lstWrapper = objComponent.get("v.lstDetalle");

       //validar información
       lstWrapper.forEach(function(objWrapper, index){
            
            //Bono en Efectivo
            if(objWrapper.TAM_BonoTotalEfectivo__c != null){
                var bonoEfectivo =  objWrapper.TAM_BonoTMEXEfectivo__c + objWrapper.TAM_BonoDealerEfectivo__c + objWrapper.TAM_BonoTFSEfectivo__c;
                if(objWrapper.TAM_BonoTotalEfectivo__c != bonoEfectivo){
                    alert('Favor de verificar la distribución de BONO EFECTIVO para el vehículo:\n' + objWrapper.TAM_Serie__c + ' ' + objWrapper.TAM_Clave__c + ' ' + objWrapper.TAM_Version__c + ' ' + objWrapper.TAM_AnioModelo__c);
                    
                }
            }

            //Producto Financiero
            if(objWrapper.TAM_BonoTotalProductoFinanciero__c != null){
                var bonoProdFinanciero =  objWrapper.TAM_BonoTMEXProdFinanciero__c + objWrapper.TAM_BonoDealerProdFinanciero__c + objWrapper.TAM_BonoTFSProdFinanciero__c;
                if(objWrapper.TAM_BonoTotalProductoFinanciero__c != bonoProdFinanciero){
                    
                    alert('Favor de verificar la distribución del PRODUCTO FINANCIERO para el vehículo:\n' + objWrapper.TAM_Serie__c + ' ' + objWrapper.TAM_Clave__c + ' ' + objWrapper.TAM_Version__c + ' ' + objWrapper.TAM_AnioModelo__c);
                    
                }
            }

        });


        var objAction = objComponent.get("c.guardarDetalleBonos");
        objAction.setParams(
        		{   			
                    lstDetalleBonos : lstWrapper,
                    recordId : objComponent.get("v.recordId")
        		}
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var booleanResult = objResponse.getReturnValue();
                if(booleanResult){
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);


    },

    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Bonos guardados correctamente.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
})