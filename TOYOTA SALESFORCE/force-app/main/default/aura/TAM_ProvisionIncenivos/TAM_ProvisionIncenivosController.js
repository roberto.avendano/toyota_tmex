/** 
    Descripción General: Pantalla de solicitud de incetivos de distribuidores.
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz            03/Ene/2020         Versión Inicial
    ________________________________________________________________
**/
({
	/** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
    }
})