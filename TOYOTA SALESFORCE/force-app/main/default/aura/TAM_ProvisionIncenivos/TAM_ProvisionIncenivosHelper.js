/** 
    Descripción General: Pantalla de solicitud de incetivos de distribuidores.
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz            03/Ene/2020         Versión Inicial
    ________________________________________________________________
**/
({
    /* Funcion de inicio*/
    initializeComponent : function(objComponent, objEvent) {
        var objAction = objComponent.get("c.getProvisionInfo");
        objAction.setParams({ recordId : objComponent.get("v.recordId") });
        objAction.setCallback(this, function(objResponse){
            
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var strResult = objResponse.getReturnValue();
                objComponent.set('v.objProvision',strResult);
            }
        });
        $A.enqueueAction(objAction);
    }
})