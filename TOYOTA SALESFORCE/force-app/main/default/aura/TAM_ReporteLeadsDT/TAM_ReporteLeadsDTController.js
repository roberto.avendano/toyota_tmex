({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_ReporteLeadsDTCtrl Contoller.doInit..");
		Component.set('v.recordId', Component.get("v.recordId"));
    	//Busca la lusra de clientes
		Helper.hpPerfilUserActual(Component, Event);    	
    },

   /** Funcion cancelar */
	jsBuscaLeads : function(Component, Event, Helper) {
		console.log("EN TAM_ReporteLeadsDTCtrl Contoller.jsBuscaLeads..");

		let dtFechaInicioBusPaso = Component.get("v.dtFechaInicioBus");
		let dtFechaFinBusPaso = Component.get("v.dtFechaFinBus");
		let sMesSelPaso = Component.get("v.sMesSel");
		console.log("EN TAM_ReporteLeadsDTCtrl Contoller.jsBuscaLeads dtFechaInicioBusPaso: " + dtFechaInicioBusPaso + ' dtFechaFinBusPaso: ' + dtFechaFinBusPaso + ' sMesSelPaso: ' + sMesSelPaso);

		let blnSinFechas = false;
		let blnSinMes = false;

		//Ve si capturo algo en las fechas
		if (dtFechaInicioBusPaso === null || dtFechaInicioBusPaso === '' || dtFechaInicioBusPaso === null || dtFechaInicioBusPaso === ''){
			//Helper.showToastError(Component, Event, 'Falta capturar alguna fecha para la consulta', 'warning');
			blnSinFechas = true;	
		}//Fin si dtFechaInicioBusPaso === null || dtFechaInicioBusPaso === ''

		//Ve si capturo algo en las fechas
		if (sMesSelPaso === null || sMesSelPaso === '' || sMesSelPaso === '57999'){
			//Helper.showToastError(Component, Event, 'Falta capturar alguna fecha para la consulta', 'warning');
			blnSinMes = true;	
		}//Fin si dtFechaInicioBusPaso === null || dtFechaInicioBusPaso === ''

		console.log("EN TAM_ReporteLeadsDTCtrl Contoller.jsBuscaLeads blnSinFechas: " + blnSinFechas + ' blnSinMes: ' + blnSinMes);

		//Ve si selecciono un filtro por lo menos
		if ( blnSinFechas && blnSinMes )	
			Helper.showToastError(Component, Event, 'Debes seleccionar un filtro para la consulta', 'warning');			

		//Ve si selecciono un filtro por lo menos
		if ( !blnSinFechas && !blnSinMes )	
			Helper.showToastError(Component, Event, 'Solo debes seleccionar un filtro para la consulta', 'warning');			
				
		//Si no hubo error entonces manda a hacer la busqueda:
		if ( (blnSinFechas && !blnSinMes) || (!blnSinFechas && blnSinMes)  )	
			Helper.hpBuscaTotPorUsuario(Component, Event);
		
    },

	//La función ocSelecDealer
    ocSelecDealer : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecDealer(objComponent, objEvent);
    },

	//La función ocSelecUsuario
    ocSelecUsuario : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecUsuario(objComponent, objEvent);
    },

	//La función ocSelMes
    ocSelMes : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelMes(objComponent, objEvent);
    },

   /** Funcion cancelar */
	jsCancelar : function(Component, Event, Helper) {
		console.log("EN TAM_ReporteLeadsDTCtrl Contoller.cancelar..");
		Helper.hpCancelar(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})