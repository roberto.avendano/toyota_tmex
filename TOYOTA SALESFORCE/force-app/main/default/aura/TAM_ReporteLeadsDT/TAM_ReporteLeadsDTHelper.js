({

    /** Funcion hpPerfilUserActual */
	hpPerfilUserActual : function(objComponent, objEvent) {
		console.log("EN hpPerfilUserActual...");

        //Llama la función 
		let objAction = objComponent.get("c.clsGetPerfilUserActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            	
            	let blnAdminUser = objResponse.getReturnValue();
           		console.log("EN hpPerfilUserActual blnAdminUser: ", blnAdminUser);
            	if (blnAdminUser == true){
            		objComponent.set("v.blnUserAdmin", true);
            		objComponent.set("v.blnNoUserAdmin", false);
            		//Consulta los datos por default
            		this.hpGetDatosUsuariosActual(objComponent, objEvent);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (blnAdminUser == false){
            		objComponent.set("v.blnUserAdmin", false);
            		objComponent.set("v.blnNoUserAdmin", true);
            	}//Fin si blnAdminUser == false
           		console.log("EN hpPerfilUserActual blnUserAdmin: ", objComponent.get("v.blnUserAdmin") );
           		console.log("EN hpPerfilUserActual blnNoUserAdmin: ", objComponent.get("v.blnNoUserAdmin") );               		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Funcion hpGetDatosUsuariosActual */
	hpGetDatosUsuariosActual : function(objComponent, objEvent) {
		console.log("EN hpGetDatosUsuariosActual...");

        //Llama la función 
		let objAction = objComponent.get("c.clsGetDatosUsuariosActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            	
            	let objWrpUsuarioResult = objResponse.getReturnValue();
          		objComponent.set("v.objWrpUsuario", objWrpUsuarioResult);
          		objComponent.set("v.strCodDist", objWrpUsuarioResult.codigo );
          		objComponent.set("v.strNomUsr", objWrpUsuarioResult.nombreUsuario);
				objComponent.set("v.sCodDealerSel", '57999');
				objComponent.set("v.sIdUsrSel", '57999');
				console.log("EN hpGetDatosUsuariosActual objWrpUsuarioResult: " + objWrpUsuarioResult);
				//Ve si se trata del gestor para que mandes a llamar la función de hpBuscaUsuariosDealer
				if (objWrpUsuarioResult.funcionUsuario == 'Manager')
					this.hpBuscaUsuariosDealer(objComponent, objEvent, objWrpUsuarioResult.codigo);
           		console.log("EN hpGetDatosUsuariosActual objWrpUsuario: ", objComponent.get("v.objWrpUsuario"));
           		console.log("EN hpGetDatosUsuariosActual strCodDist: ", objComponent.get("v.strCodDist"));
           		console.log("EN hpGetDatosUsuariosActual strNomUsr: ", objComponent.get("v.strNomUsr"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

    },

    /** Funcion hpBuscaTotPorUsuario */
	hpBuscaTotPorUsuario : function(objComponent, objEvent) {
		console.log("EN hpBuscaTotPorUsuario...");
		
		let sCodDealerSelPaso =	objComponent.get("v.sCodDealerSel");
		let sIdUsrSelPaso = objComponent.get("v.sIdUsrSel");		
        let dtFechaInicioBusPaso = objComponent.get("v.dtFechaInicioBus");	   
        let dtFechaFinBusPaso = objComponent.get("v.dtFechaFinBus");	   
        let sMesSelPaso = objComponent.get("v.sMesSel");
		console.log("EN hpBuscaTotPorUsuario sCodDealerSelPaso: " + sCodDealerSelPaso);
		console.log("EN hpBuscaTotPorUsuario sIdUsrSelPaso: " + sIdUsrSelPaso);
		console.log("EN hpBuscaTotPorUsuario dtFechaInicioBusPaso: " + dtFechaInicioBusPaso);
		console.log("EN hpBuscaTotPorUsuario dtFechaFinBusPaso: " + dtFechaFinBusPaso);
		console.log("EN hpBuscaTotPorUsuario sMesSelPaso: " + sMesSelPaso);
		
		let objWrpUsuarioPaso = objComponent.get("v.objWrpUsuario");
        //Llama la función 
		let objAction = objComponent.get("c.clsBuscaTotPorUsuario");
        objAction.setParams({
        	"IdClienteActual" : objComponent.get("v.userId"),
        	"objWrpUsuarioPrm" : objWrpUsuarioPaso,
        	"sCodDealerSelPrm" : sCodDealerSelPaso,
        	"sIdUsrSelPrm" : sIdUsrSelPaso,
        	"dtFechaInicioBusPrm" : dtFechaInicioBusPaso,
        	"dtFechaFinBusPrm" : dtFechaFinBusPaso,
        	"sMesSelPrm" : sMesSelPaso
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            	
            	let llWrpListaTotalesUserResult = objResponse.getReturnValue();
          		objComponent.set("v.lWrpListaTotalesUser", llWrpListaTotalesUserResult);
           		console.log("EN hpBuscaTotPorUsuario lWrpListaTotalesUser: ", objComponent.get("v.lWrpListaTotalesUser"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
		
    },

    /** Funcion hpSelecDealer */
    hpSelecDealer: function(objComponent, objEvent) {
    	console.log('EN Save hpSelecDealer...');
        let value = objEvent.getSource().get("v.value");
        let lWrpDealersUsuariosPaso = [];
    	console.log('EN Save hpSelecDealer value: ', value);
        
        //Ve si el codigo del dealer seleccionado es diferente de 57999
        if (value !== '57999'){
        	objComponent.set("v.sIdUsrSel", '57999');        
        	this.hpBuscaUsuariosDealer(objComponent, objEvent, value);        
        }//Fin si value !== '57999'
        if (value === '57999'){
        	objComponent.set("v.sIdUsrSel", '57999');        
        	objComponent.set("v.lWrpDealersUsuarios", lWrpDealersUsuariosPaso);        
        }//Fin si value === '57999'

    },

    /** Funcion hpSelecUsuario */
    hpSelecUsuario: function(objComponent, objEvent) {
    	console.log('EN Save hpSelecUsuario...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sIdUsrSel", value);
        console.log('EN Save hpSelecUsuario sIdUsrSel: ', objComponent.get("v.sIdUsrSel"));
    },

    /** Funcion hpSelMes */
    hpSelMes: function(objComponent, objEvent) {
    	console.log('EN Save hpSelMes...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sMesSel", value);
        if (value !== '57999'){
        	Component.set("v.dtFechaInicioBus", null);
        	Component.set("v.dtFechaFinBus", null);
        }//Fin si value !== '57999'
        console.log('EN Save hpSelMes sIdUsrSel: ', objComponent.get("v.sMesSel"));
    },

    /** Funcion hpSelecDealer */
    hpBuscaUsuariosDealer: function(objComponent, objEvent, strCodDealer) {
    	console.log('EN Save hpBuscaUsuariosDealer...');
        let value = strCodDealer;
		let objWrpUsuarioPrm = objComponent.get("v.objWrpUsuario");
        //Llama la función 
		let objAction = objComponent.get("c.clsGetListaUsr");
        objAction.setParams({
        	"codDealerPrm" : value,
        	"objWrpUsuarioPrm" : objWrpUsuarioPrm
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            	
            	let lWrpListaDealersUsuariosResult = objResponse.getReturnValue();
          		objComponent.set("v.sCodDealerSel", value);
          		objComponent.set("v.lWrpDealersUsuarios", lWrpListaDealersUsuariosResult);
          		console.log('EN Save hpBuscaUsuariosDealer sCodDealerSel: ', objComponent.get("v.sCodDealerSel"));
          		console.log('EN Save hpBuscaUsuariosDealer lWrpDealersUsuarios: ', objComponent.get("v.lWrpDealersUsuarios"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Funcion hpCancelar */
    hpCancelar : function(Component, Event, strMensaje) {
    	//Inicializa la lista de lstWrpDistAct a null
    	let wrpDistr = [];    
    	Component.set("v.strCodDist", null);
    	Component.set("v.strNomUsr", null);
    	Component.set("v.dtFechaInicioBus", null);
    	Component.set("v.dtFechaFinBus", null);    
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje, strType) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2000,
            "type": strType
        });
        toastEvent.fire();
    }

})