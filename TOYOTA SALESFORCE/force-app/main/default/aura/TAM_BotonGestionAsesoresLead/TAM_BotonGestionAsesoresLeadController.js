({
    
    doInit : function(component, event, helper) {
        helper.getInformationByUser(component, event, helper);    
    },
    
    openModal: function(component, event, helper) {
        helper.openComponentChild(component, event, helper);
       
    },
    
    openModalDiasApdo: function(component, event, helper) {
        helper.openComponentChildDiasApdo(component, event, helper);
    },
    
    openModalReasigaProsp: function(component, event, helper) {
        helper.openComponentChildReasigProsp(component, event, helper);
    },

    openModalGestionaInventario: function(component, event, helper) {
        helper.openModalGestionaInventario(component, event, helper);
    },

    openModalReporteLeadsDT: function(component, event, helper) {
        helper.hpOpenModalReporteLeadsDT(component, event, helper);
    },
    
})