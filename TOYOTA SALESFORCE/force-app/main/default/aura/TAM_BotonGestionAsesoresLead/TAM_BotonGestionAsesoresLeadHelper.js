({
    getInformationByUser : function(component, event, helper) {
        let action = component.get("c.getGestorIdByAccount");
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        action.setParams({             
            userId : userId            
        });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state == "SUCCESS"){
                let result = response.getReturnValue();
                component.set("v.account", result);
                /*let gestorLeadTMEX = component.get("v.account.TAM_GestorLeads__c");
                let gestorLeadTFS = component.get("v.account.TAM_GestorLeadsTFS__c");
                console.log('EN getInformationByUser userId: ' + userId);
                console.log('EN getInformationByUser gestorLeadTMEX: ' + gestorLeadTMEX);
                console.log('EN getInformationByUser gestorLeadTFS: ' + gestorLeadTFS);*/
                this.getUsuarioRedirect(component, event, result);                
                /*//Ve si es el mismo usuario
                if(userId == gestorLeadTMEX){
                    let elements = document.getElementsByClassName("hide");
                    elements[0].style.display = 'none';
                    elements[0].style.display = 'block';
                    
                    let elementsApartado = document.getElementsByClassName("hideApartado");
                    elementsApartado[0].style.display = 'none';
                    elementsApartado[0].style.display = 'block';
                }else{
                    let elements = document.getElementsByClassName("hide");
                    elements[0].style.display = 'none';
                    
                    let elementsApartado = document.getElementsByClassName("hideApartado");
                    elementsApartado[0].style.display = 'none';
                }*/
            }else{
                console.error("fail:" + response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
        
    },

    /** */
    getUsuarioRedirect : function(objComponent, objEvent, objAccount){
        console.log("EN SolCompEspDist Helper.getUsuarioRedirect " + objAccount);
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );        
        //Obtener lista de distribuidores disponibles.
        let objAction = objComponent.get("c.getUsuarioRedirect");
        objAction.setParams({   			
            noDistAct : objComponent.get("v.strNoDist") 
        });        
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let UsuarioResult = objResponse.getReturnValue();
                objComponent.set("v.Usuario", UsuarioResult);
                
                let gestorLeadTMEX = objAccount.TAM_GestorLeads__c;
                let gestorLeadTFS = objAccount.TAM_GestorLeadsTFS__c;
                let blnLeadReasigLead = UsuarioResult.TAM_ReasignacionMasivaLeads__c;
                let blnLeadRedirect = UsuarioResult.TAM_RedireccionaLeads__c;
                let blnLeadGestionaInvent = UsuarioResult.TAM_ColaborarInventario__c;

                console.log('EN getUsuarioRedirect userId: ' + userId);
                console.log('EN getUsuarioRedirect gestorLeadTMEX: ' + gestorLeadTMEX);
                console.log('EN getUsuarioRedirect gestorLeadTFS: ' + gestorLeadTFS);
                console.log('EN getUsuarioRedirect blnLeadReasigLead: ' + blnLeadReasigLead);
                console.log('EN getUsuarioRedirect blnLeadRedirect: ' + blnLeadRedirect);
                
                //Desabilita los botones por default	
                let elementsGestAsesor = document.getElementsByClassName("hideGestAsesor");
                elementsGestAsesor[0].style.display = 'none';                    
                let elementsApartado = document.getElementsByClassName("hideApartado");
                elementsApartado[0].style.display = 'none';
                let elementsReasignProsp = document.getElementsByClassName("hideResigProsp");
                elementsReasignProsp[0].style.display = 'none';
                let elementsGestionaInvent = document.getElementsByClassName("hideGestionaInvent");
                elementsGestionaInvent[0].style.display = 'none';
                
                //Hailiita el boton de Gestión Asesores
                if(userId == gestorLeadTMEX || blnLeadRedirect){
                    let elementsGestAsesor = document.getElementsByClassName("hideGestAsesor");
                    elementsGestAsesor[0].style.display = 'none';
                    elementsGestAsesor[0].style.display = 'block';
                }//Fin si userId == gestorLeadTMEX || blnLeadRedirect

                //Hailiita el boton de Días Apartado
                if(userId == gestorLeadTMEX){
                	let elementsApartado = document.getElementsByClassName("hideApartado");
                    elementsApartado[0].style.display = 'none';
                    elementsApartado[0].style.display = 'block';
                }//Fin si userId == gestorLeadTMEX
    
                //Hailiita el boton de Resignar prospectos
                if(blnLeadReasigLead){
                    let elementsReasignProsp = document.getElementsByClassName("hideResigProsp");
                    elementsReasignProsp[0].style.display = 'none';
                    elementsReasignProsp[0].style.display = 'block';
                }//Fin si blnLeadReasigLead

                //Hailiita el boton de Gestionar Inventario
                if(blnLeadGestionaInvent){
                    let elementsGestionaInvent = document.getElementsByClassName("hideGestionaInvent");
                    elementsGestionaInvent[0].style.display = 'none';
                    elementsGestionaInvent[0].style.display = 'block';
                }//Fin si blnLeadGestionaInvent
    
                /*//Hailiita el boton de Días Apartado
                if(userId == gestorLeadTMEX){
                    let elements = document.getElementsByClassName("hideGestAsesor");
                    elements[0].style.display = 'none';
                    elements[0].style.display = 'block';
                    
                    let elementsApartado = document.getElementsByClassName("hideApartado");
                    elementsApartado[0].style.display = 'none';
                    elementsApartado[0].style.display = 'block';
                }else{
                    let elements = document.getElementsByClassName("hideGestAsesor");
                    elements[0].style.display = 'none';
                    
                    let elementsApartado = document.getElementsByClassName("hideApartado");
                    elementsApartado[0].style.display = 'none';
                }//Fin si userId == gestorLeadTMEX*/

            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    }, 
    
    openComponentChild  : function(component, event, helper) {
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        $A.createComponent( 'c:TAM_ActUsrLeadManagMobDesk', {
            userId:userId
        },
        function(modalComponent, status, errorMessage) {
        	if (status === "SUCCESS") {
        		//Appending the newly created component in div
                let body = component.find( 'showChildModal' ).get("v.body");
                body.push(modalComponent);
                component.find( 'showChildModal' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
            	console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
            	console.log('error');
                console.log("Error: " + errorMessage);
            }
        }
        );
    },
    
    openComponentChildDiasApdo  : function(component, event, helper) {
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        $A.createComponent( 'c:TAM_ActLimDiasApdoMobDesk', {
            userId:userId
        },
        function(modalComponent, status, errorMessage) {
        	if (status === "SUCCESS") {
        		let body = component.find( 'showChildModalDiasApdo' ).get("v.body");
        		body.push(modalComponent);
                component.find( 'showChildModalDiasApdo' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
            	console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
            	console.log('error');
            	console.log("Error: " + errorMessage);
            }
        }
        );
    },    

    openComponentChildReasigProsp  : function(component, event, helper) {
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        $A.createComponent( 'c:TAM_ReasignaMasivaLeadsComV2', {
            userId:userId
        },
        function(modalComponent, status, errorMessage) {
        	if (status === "SUCCESS") {
        		let body = component.find( 'showChildModalReasigaProsp' ).get("v.body");
        		body.push(modalComponent);
                component.find( 'showChildModalReasigaProsp' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
            	console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
            	console.log('error');
            	console.log("Error a la hora de cargar TAM_ReasignaMasivaLeadsCom: " + errorMessage);
            }
        }
        );
    },    

    openModalGestionaInventario  : function(component, event, helper) {
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        $A.createComponent( 'c:TAM_GestionaInventario', {
            userId:userId
        },
        function(modalComponent, status, errorMessage) {
        	if (status === "SUCCESS") {
        		let body = component.find( 'showChildModalGestionaInventario' ).get("v.body");
        		body.push(modalComponent);
                component.find( 'showChildModalGestionaInventario' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
            	console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
            	console.log('error');
            	console.log("Error a la hora de cargar TAM_ReporteLeadsDTCtrl: " + errorMessage);
            }
        }
        );
    },    
    
    hpOpenModalReporteLeadsDT  : function(component, event, helper) {
        let userId = $A.get( "$SObjectType.CurrentUser.Id" );
        $A.createComponent( 'c:TAM_ReporteLeadsDT', {
            userId:userId
        },
        function(modalComponent, status, errorMessage) {
        	if (status === "SUCCESS") {
        		let body = component.find( 'showChildModalReporteLeadsDP' ).get("v.body");
        		body.push(modalComponent);
                component.find( 'showChildModalReporteLeadsDP' ).set("v.body", body);
            } else if (status === "INCOMPLETE") {
            	console.log('Server issue or client is offline.');
            } else if (status === "ERROR") {
            	console.log('error');
            	console.log("Error a la hora de cargar TAM_ReporteLeadsDTCtrl: " + errorMessage);
            }
        }
        );
    },    
    
})