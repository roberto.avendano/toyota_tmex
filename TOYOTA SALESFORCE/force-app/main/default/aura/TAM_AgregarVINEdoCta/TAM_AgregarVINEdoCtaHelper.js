({
    saveRegistroVIN : function(component,event,helper) {
        let idMovimiento = component.get("v.selectedRecord.Id");
        let edoCta = component.get("v.estadoCuentaGralInfo");
        let incSolicitado = component.get("v.incSolicitado");
        let lineasVINESProvisionados =  component.get("v.lineasVINESProvisionados");
        let tipoEdoCta = component.get("v.tipoSeccion"); 
        let ubicacionVIN = component.get("v.selectedRecord.ubicacionVIN");
        let datosCorrectos;
        
        if(incSolicitado > 0){
            datosCorrectos = false;
            component.set("v.validarIncentivoSolicitado",false);
        }else{
            datosCorrectos = true;
            component.set("v.validarIncentivoSolicitado",true);
        }
        
        if(datosCorrectos == true){
            
            let action = component.get("c.saveRecordVIN");
            
            action.setParams({ 
                tipoEdoCta  : tipoEdoCta,
                incSolicitado : incSolicitado ,
                idMovimiento : idMovimiento,
                edoCta       : edoCta,
                ubicacionVIN :  ubicacionVIN
                
            });
            
            action.setCallback(this, function(response){
                var objState = response.getState();
                if (objState === "SUCCESS") {
                    var result = response.getReturnValue();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "Success",
                        "title": "Exitoso!",
                        "message": "Se registro correctamente."
                    });
                    toastEvent.fire();
                    component.find("overlayLib").notifyClose();  
                    //location.reload(); 
                }
            });
            
            $A.enqueueAction(action);
            
        } 
        
    }
})