({
    
    politicaCorrespondienteFlotilla : function(component, event, helper) {
        let modelo = component.get("v.modelo");
        let anioModelo = component.get("v.anioModelo");
        let rangoPrograma = component.get("v.rangoPrograma");
        let tipoVenta = component.get("v.tipoVentaCorporativa");
        let fechaVenta = component.get("v.fechaCierreSolicitudFlotilla");
        
        var action = component.get("c.getPoliticaFechaCierre");
        action.setParams({
            modelo : modelo,
            anioModelo : anioModelo,
            rangoPrograma : rangoPrograma,
            tipoVenta : tipoVenta,
            fechaVenta : fechaVenta
            
        }); 
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.politicaCorrespondienteFlotilla", response.getReturnValue());
                }
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },    
    
    validarPolitica : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN__c");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.modelo");
        let anioModelo   = component.get("v.anioModelo");	
        let fechaVentaVIN   = component.get("v.fechaCierreSolicitudFlotilla");	
        let solicitaBono	= "SinBono";
        let tipoVenta = component.get("v.tipoVenta");
        
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.RetailFlot");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaIN : fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : 'SINTFS'
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","SinBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{         
                        helper.obtenListaIncentivos(component, event, helper);
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
    },
    
    validarPoliticaBonoLealtad : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN + Bono de lealtad
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN__c");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo__c");
        let anioModelo   = component.get("v.selectedRecord.TAM_AnioModelo__c");	
        let fechaVentaVIN   = component.get("v.selectedRecord.Submitted_Date__c");
        let solicitaBono	= "ConBono";
        let tipoVenta = component.get("v.tipoVenta");
        
        
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.getPoliticasVigentes");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaVIN :fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : tipoVenta
            });
            
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","ConBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{     
                        helper.obtenListaIncentivos(component, event, helper); 
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
        
        
        
    },
    
    guardaSolicitudFlotilla : function(component, event, helper) {
        let wrapSolicitud = component.get("v.objModeloSelExcepCte");
        let motivoSolicitud = component.get("v.motivosSeleccionado"); 
        let comentario = component.get("v.comentario");
        let precioSolicitado = component.get("v.politicaCorrespondienteFlotilla.precioPublico");
        let incentivoManual = false;
        let politicaSeleccionada = component.get("v.politicaSeleccionada");
        let incentivoTotalManual = 10;
        let incentivoDealerManual = 5;
        let incentivoTMEXManual = 5;
        let incentivoFechaCierre = component.get("v.politicaCorrespondienteFlotilla.idPolitica");
        let codigoDelaer =  component.get("v.codigoDealer");
        let listaVINESIn = component.get("v.listaVINES");
        let fechaVenta  = component.get("v.fechaCierreSolicitudFlotilla");
        let documentId =  component.get("v.documentId");
        
        
        let action = component.get("c.guardaSolicitudExcepcion");
        
        action.setParams({
            wrapSolicitud           : wrapSolicitud,
            motivoSolicitud         : motivoSolicitud,
            comentario              : comentario,
            precioSolicitado        : precioSolicitado,
            incentivoManual	        : incentivoManual,
            politicaSeleccionada    : politicaSeleccionada,
            incentivoTotalManual    : incentivoTotalManual,
            incentivoDealerManual   : incentivoDealerManual,
            incentivoTMEXManual     : incentivoTMEXManual,
            incentivoFechaCierre    : incentivoFechaCierre,
            codigoDelaer            : codigoDelaer,
            listaVINESIn		    : listaVINESIn.toString(),
            fechaVenta              : fechaVenta,
            documentId              : documentId
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                
                Component.set("v.blnShowVinesDOD", false);
                Component.set("v.blnCreaExcepcionFlotillaPrograma", false);
                Component.set("v.blnShowBtnCreaExcep", true);
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    
    obtenListaIncentivos : function(component, event, helper) {
        let modelNumber = component.get("v.modelo");
        let modelYear   = component.get("v.anioModelo");	         
        
        var action = component.get("c.getPoliticasVIN");
        action.setParams({
            modelNumber : modelNumber,
            modelYear : modelYear
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.politicaVigenteLista", response.getReturnValue());
                    
                    component.set("v.existeListaPoliticas","ConPoliticas");
                }else{
                    
                    component.set("v.existeListaPoliticas","SinPoliticas");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    obtenMotivosSolicitud : function(component, event, helper) {
        var action = component.get("c.obtieneMotivosSolicitud");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.motivosSolicitud",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
    
})