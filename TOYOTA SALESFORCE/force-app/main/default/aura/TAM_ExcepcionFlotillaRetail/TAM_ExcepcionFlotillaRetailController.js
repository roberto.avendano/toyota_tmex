({
    
    inicializacion: function(component, event, helper) {
      var valSelected = event.getSource().get("v.value");
        component.set("v.motivoSeleccionado",valSelected);                 
        //component.set("v.solicitaBono",null);
        var validaDatos;
        
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);
            component.set("v.solicitaBono",'SinBono');
            helper.validarPolitica(component, event, helper);            
      		helper.politicaCorrespondienteFlotilla(component, event, helper);   
    },
    
    
    tipoExcepcion : function(component, event, helper) {
        var expSelected = event.getSource().get("v.value");  
        if(expSelected == 'Incentivo'){
            component.set("v.isIncentivo",true);
            component.set("v.isPrecio",false)
            component.set("v.isAutoDemo",false);
        }
        
        if(expSelected == 'Precio'){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",true);
            component.set("v.isAutoDemo",false);
        }  
        
        
        if(expSelected == 'AutoDemo'){
            component.set("v.isPrecio",false);
            component.set("v.isIncentivo",false);
            component.set("v.isAutoDemo",true);
        }  
        
        if(expSelected == ''){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",false);
            
        }  
        
    },
    
    //En caso de no estar en dealer daily se verifica si existe un pago de factura
    InfoVINFacturaNoDD: function(component, event, helper) {
        var VIN = component.get("v.SearchKeyWord");
        var respuestaFacturas;
        var validaVINFactura = component.get("c.buscaFacturasVIN");
        validaVINFactura.setParams({
            VIN : VIN
            
        });
        
        validaVINFactura.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                respuestaFacturas = response.getReturnValue();
                if(respuestaFacturas.length > 0){
                    component.set("v.facturasVIN",respuestaFacturas);
                    component.set("v.existeFactura",true);
                    component.set("v.existeEnDD",true);
                    
                }else{
                    component.set("v.existeFactura",false);
                    //helper.validarPolitica(component, event, helper);
                    helper.obtenMotivosSolicitud(component, event, helper); 
                    component.set("v.existeEnDD",true);
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(validaVINFactura);
        


    },

    //En caso de estar en dealer daily validar que no tenga factura de pago asociada
    getInfoVIN : function(component, event, helper) {
        //Validar que no exista un VIN en alguna factura cargada en Salesforce

        let VIN = component.get("v.selectedRecord.TAM_VIN__c");
        var respuestaFacturas;
        var validaVINFactura = component.get("c.buscaFacturasVIN");
        validaVINFactura.setParams({
            VIN : VIN
            
        });
        
        validaVINFactura.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                respuestaFacturas = response.getReturnValue();
                if(respuestaFacturas.length > 0){
                    component.set("v.facturasVIN",respuestaFacturas);
                    component.set("v.existeFactura",true);
                    
                }else{
                    component.set("v.existeFactura",false);
                    //helper.validarPolitica(component, event, helper);
                    helper.obtenMotivosSolicitud(component, event, helper); 
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(validaVINFactura);
        
    },
    
    guardarRegistro : function(component, event, helper) {
        helper.guardaSolicitudFlotilla(component, event, helper);   
    },
    
    cancelar : function(component, event, helper) {
        component.set("v.isIncentivo",false);
        component.set("v.isPrecio",false);
        var urlHome = $A.get("$Label.c.TAM_Home_CRM");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": urlHome
        });
        urlEvent.fire();
        
        
        
    },
    
    onSingleSelectChange  : function(component, event, helper) {
        component.set('v.validaResumenPrecio',false);
        var idPoliticaIncentivo 		= event.getSource().get("v.value");
        var capturedOpcion 				= event.getSource().get("v.name");
        var modelo						= component.get("v.modelo");
        var anioModelo					= component.get("v.anioModelo");
        var validaDatos;
        var solicitaBono				= component.get("v.solicitaBono");
        
        if(idPoliticaIncentivo == 'Elegir Incentivo'){
            component.set("v.politicaSeleccionada","Elegir Incentivo");
            validaDatos = false;
            component.set("v.validaPickList",false);
            
        }else{
            component.set("v.politicaSeleccionada",idPoliticaIncentivo);
            validaDatos = true;
            component.set("v.validaPickList",true);            
            
        }
        
        
        if(validaDatos == true){
            var action = component.get("c.calculoPrecioIncentico");
            action.setParams({
                idPoliticaIncentivo : idPoliticaIncentivo,
                modelo : modelo,
                anioModelo : anioModelo,
                solicitaBono : solicitaBono
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    component.set('v.resumenPrecio',respuesta);
                    component.set('v.validaResumenPrecio',true);
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
            
        }
        
    },
    
    onSingleSelectVenta  : function(component, event, helper) {
        var valSelected = event.getSource().get("v.value");
        component.set("v.tipoVenta",valSelected); 
        var bono = component.get("v.solicitaBono");
        component.find("selectIncentivos").set("v.value",'Elegir un Motivo');
        component.set("v.solicitaBono",null);
        
        if(valSelected == 'default'){
            
            component.set("v.solicitaBono",null);
            component.find("selectIncentivos").set("v.value",'Elegir un Motivo');
            
        }
        
    },
    
    onSingleSelectCMotivos  : function(component, event, helper) {
        var valSelected = event.getSource().get("v.value");
        component.set("v.motivoSeleccionado",valSelected);                 
        //component.set("v.solicitaBono",null);
        var validaDatos;
        
        if(valSelected == 'Elegir un Motivo'){
            validaDatos = false;
            component.set("v.validaPickListMotivo",false);
            component.set("v.solicitaBono",null);
            
        }
        if(valSelected == 'El cliente apartó su unidad en el mes de la promoción, pero se reportó hasta el siguiente. (Incentivo+Bono Lealtad)' || valSelected == 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.(Incentivo+Bono Lealtad)' ){
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);
            component.set("v.solicitaBono",'ConBono');
            helper.validarPoliticaBonoLealtad(component, event, helper);
            
        }
        
        if(valSelected == 'Otros motivos.' || valSelected == 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.' || valSelected == 'El cliente apartó su unidad en el mes de la promoción, pero se reportó hasta el siguiente.'){
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);
            component.set("v.solicitaBono",'SinBono');
            helper.validarPolitica(component, event, helper);            
            
        }
        
    }, // function automatic called by aura:waiting event  
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },

    subirDocumento : function(component, event, helper) {
        var checkCmp = component.find("checkbox");
         resultCmp = component.find("checkResult");
         resultCmp.set("v.value", ""+checkCmp.get("v.value"));

    },

    handleUploadFinished : function(component, event, helper) {
        var idRecord = component.get("v.recordId");
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var id       = uploadedFiles[0].Id;
        var toastEvent = $A.get("e.force:showToast");
        
        component.set("v.documentId",documentId);
        component.set("v.nameFile",fileName);
        
        /*   
        var action = component.get("c.saveDocumentId");
        action.setParams({ 
            idRecord : idRecord,
            idDocument : uploadedFiles[0].documentId,
            nombreDocumento : fileName
            
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File "+documentId+" Uploaded successfully."
                });
                toastEvent.fire();
                component.set("v.nameFile",fileName);
                component.set("v.documentId",documentId);
                component.set("v.archivoCargado",true);
                component.set("v.SolicitudIncentivos.TAMDocumento_Id__c",documentId)
                component.set("v.solicitudEnviada",false);
                console.log("id de documento"+documentId);
                console.log("nombre del documento"+fileName);
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);*/
        
        
    },

    previewDocumento : function(component, event, helper) {
        var idDocument;
        
        if(component.get("v.documentId") != null){
            idDocument  = component.get("v.documentId");
        }
        
        $A.get('e.lightning:openFiles').fire({
            recordIds: [idDocument]
        });
        
    },

    eliminarDocumento : function(component, event, helper) {
        var idDocument;
        var idRecord = component.get("v.recordId");        

        if(component.get("v.documentId") != null){ 
            idDocument  = component.get("v.documentId");
        }
        
        var action = component.get("c.deleteDocument");
        action.setParams({ 
            documentId : idDocument
        
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nameFile",null);
                component.set("v.documentId",null);
                //component.set("v.SolicitudIncentivos.TAM_NombreFacturaRetail__c ",null);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Documento Eliminado."
                });
                toastEvent.fire();
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    
    }
    
})