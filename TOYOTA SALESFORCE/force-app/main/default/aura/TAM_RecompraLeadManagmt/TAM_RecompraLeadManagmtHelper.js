({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.initializeComponent...");
		                
        let strIdCliente = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getDatosLead");
        objAction.setParams({   			
        	recordId : strIdCliente
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpDatosRecompraLeadResult = objResponse.getReturnValue();
            	objComponent.set("v.DatosLeadRecompra", objWrpDatosRecompraLeadResult);
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent objWrpDatosRecompraLeadResult: ", objWrpDatosRecompraLeadResult);
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent objWrpDatosRecompraLeadResult: ", objWrpDatosRecompraLeadResult.objRecompraLead);
            	if (objWrpDatosRecompraLeadResult.objRecompraLead !== undefined && objWrpDatosRecompraLeadResult.objRecompraLead !== null){
	            	if (objWrpDatosRecompraLeadResult.objRecompraLead.RecordType.Name === 'Cotización')
	            		objComponent.set("v.strEstadoNvoCandidato", 'Prospecto Caliente');
	            	if (objWrpDatosRecompraLeadResult.objRecompraLead.RecordType.Name === 'Cotización TFS')
	            		objComponent.set("v.strEstadoNvoCandidato", 'Cotización');            	
            	}//Fin si objWrpDatosRecompraLeadResult.objRecompraLead
            	console.log("EN initializeComponent objWrpDatosRecompraLeadResult: ", objWrpDatosRecompraLeadResult);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 

    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpCrearNvoCand : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.hpCrearNvoCand...");

        let DatosLeadRecompraPaso = objComponent.get("v.DatosLeadRecompra");
        let DatosLeadRecompraPasoFinal = objComponent.get("v.DatosLeadRecompra");
		let faltanCampos = false;
		let blnEmail = true;
		let blnPhone = true;

		let camposValidosBase = objComponent.find('validaCampo').reduce(
			function (validSoFar, inputCmp) {
				inputCmp.showHelpMessageIfInvalid(); //set("v.errors", [{message:"Debes capturar un valor." + value}]);
				return validSoFar && inputCmp.get('v.validity').valid;
			}
		, true);

        //Ve si hubo un error a la hora de validar 
        if (camposValidosBase) {

        	/*//Valida el campo de la Pais
        	let cmpPaisid = objComponent.find("paisid");
        	let strPaisid = cmpPaisid.get("v.value");
        	alert('EN Helper.hpCrearNvoCand strPaisid: ' + strPaisid);
        	// is input valid text?
        	if (strPaisid === "" || strPaisid == null){
           		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en el campo de: Pais');
        		faltanCampos = true;
        	}*/
        	
        	//Ve si capturo alguno de los campos de Email
        	if (DatosLeadRecompraPaso.objRecompraLead.Email === null || DatosLeadRecompraPaso.objRecompraLead.Email === '')
        		blnEmail = false;
        	//Ve si capturo alguno de los campos de Phone
        	if (DatosLeadRecompraPaso.objRecompraLead.Phone === null || DatosLeadRecompraPaso.objRecompraLead.Phone === '')
        		blnPhone = false;
        	//Ahora si ve si ninguno de los dos se capturo
        	if (!blnEmail && !blnPhone){
        		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en alguno de los campos: Correo electrónico o Teléfono');
       			faltanCampos = true;        	
        	}//Fin si !blnEmail && !blnPhone        
        
        } else{
       		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en los campos que faltan');
       		faltanCampos = true;
        } 

        //Si no hubo error guarda los datos
        if (!faltanCampos){
        	console.log("EN Consulta Datos Cliente Helper.hpCrearNvoCand DatosLeadRecompra: ", objComponent.get("v.DatosLeadRecompra"));
	        
	        let strIdCandidato = objComponent.get("v.recordId");
	        //Llama la función 
			let objAction = objComponent.get("c.creaNuevoProspecto");
	        objAction.setParams({
	        	"recordId" : strIdCandidato,
	        	"DatosLeadRecompraPrm" : objComponent.get("v.DatosLeadRecompra"),
	        	"strEstadoNvoCandidatoPrm" : objComponent.get("v.strEstadoNvoCandidato")
	   		});		
	        objAction.setCallback(this, function(objResponse){
	            let objState = objResponse.getState();
	            if (objState === "SUCCESS") {
	            	let objWrpResAct = objResponse.getReturnValue();
	            	console.log("EN Consulta objWrpResAct: ", objWrpResAct);
	            	if (objWrpResAct.blnEstatus == false){
				        var recordId = objWrpResAct.strParamOpcional2;
				        console.log("EN Consulta Datos Cliente Helper.hpCrearNvoCand recordId: ", recordId);
				        //Manda llamar el evento para abrir el nuevo lead
				        var navEvt = $A.get("e.force:navigateToSObject");
				        navEvt.setParams({
				            "recordId": recordId,
				            "slideDevName": "detail"
				        });
				        //Llama al evento
				        navEvt.fire();
				        //Despliega el msg de exito		
	            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
	            	}//Fin si objWrpResAct.blnEstatus == false
	            	if (objWrpResAct.blnEstatus == true)
	            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
	            }//Fin si objState === "SUCCESS"
	        });
	        $A.enqueueAction(objAction);        
        }//Fin si faltanCampos
        		
    },


    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
      	console.log("EN showToastSuccess strMensaje: ", strMensaje);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 1000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
      	console.log("EN showToastError strMensaje: ", strMensaje);    
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2000,
            "type": "error"
        });
        toastEvent.fire();
    },
    
})