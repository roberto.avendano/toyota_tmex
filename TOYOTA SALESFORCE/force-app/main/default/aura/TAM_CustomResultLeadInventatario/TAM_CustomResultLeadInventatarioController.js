({
    selectAccount : function(component, event, helper){      
        var getSelectAccount = component.get("v.VIN");  
        var compEvent = component.getEvent("oSelectedAccountEvent");
        compEvent.setParams({"accountByEvent" : getSelectAccount });  
        compEvent.fire();
    },
})