({
    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit..");
		//console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit objModeloSelAddDist: ", Component.get("v.objModeloSelAddDist"));
		//console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit recordId: ", Component.get("v.recordId"));
		//console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit objModeloSelExcepCte: ", Component.get("v.objModeloSelExcepCte"));
		Component.set('v.recordId', Component.get("v.recordId"));
		Component.set('v.blnShowVinesDOD', true);		
		var blnCheckOutDODPaso = Component.get("v.blnCheckOutDOD");
		var blnSolFlotillaPaso = Component.get("v.blnSolFlotilla");
		var blnSolProgramaPaso = Component.get("v.blnSolPrograma");
		var blnSolFlotillaProgramaExcepcionPaso = Component.get("v.blnSolFlotillaProgramaExcepcion");
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit blnShowVinesDOD: ", Component.get("v.blnShowVinesDOD"));		
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit blnCheckOutDODPaso: ", blnCheckOutDODPaso);
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit blnSolFlotillaPaso: ", blnSolFlotillaPaso);
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit blnSolProgramaPaso: ", blnSolProgramaPaso);
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit blnSolFlotillaProgramaExcepcionPaso: ", blnSolFlotillaProgramaExcepcionPaso);
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit objModeloSelExcepCte: ", JSON.stringify(Component.get("v.objModeloSelExcepCte")));
		
		let strObjModeloSelExcepCte	= JSON.stringify(Component.get("v.objModeloSelExcepCte"));		
		let index = strObjModeloSelExcepCte.indexOf("strNombreCorto");
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit strObjModeloSelExcepCte: " + strObjModeloSelExcepCte + ' index: ' + index);

		//Viene desde la solicitud de programa o Venta especial
		if (blnSolProgramaPaso)
			Helper.getWrpModelo(Component, Event);
		//Viene desde la pagina de blnCheckOutDODPaso
		if (blnCheckOutDODPaso)
			Helper.getWrpModeloCheckOut(Component, Event);
		//Viene desde la pagina de blnSolFlotillaProgramaExcepcion
		if (blnSolFlotillaProgramaExcepcionPaso)
			Helper.getDistribEntrega(Component, Event);
							
		//Tiene algo el objeto objModeloSelExcepCte
		if (index > 0){
			//console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.doInit SI EXISTE EN strObjModeloSelExcepCte....");
			//Helper.getDistribEntrega(Component, Event);
		}
    },

   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})