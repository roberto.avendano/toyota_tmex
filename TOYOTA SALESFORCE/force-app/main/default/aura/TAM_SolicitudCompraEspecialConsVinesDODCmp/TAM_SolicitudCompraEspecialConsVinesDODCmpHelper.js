({

    /** */
    getWrpModelo : function(Component, Event){
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getWrpModelo...");
    	let objModeloSelAddDistPaso = Component.get("v.objModeloSelAddDist");
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getWrpModelo : ", objModeloSelAddDistPaso);
    	//sWrpModelo : JSON.stringify(Component.get("v.objModeloSelAddDist"))
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidoresFinal");
        objAction.setParams({   			
        	sWrpModelo : JSON.stringify(objModeloSelAddDistPaso)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstDistriEntregaResult = objResponse.getReturnValue();
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);            
                Component.set("v.objModeloSelFinal", Component.get("v.objModeloSelAddDist"));
                Component.set("v.strAnioModelo", Component.get("v.objModeloSelFinal").strAnioModelo);
                Component.set("v.strSerie", Component.get("v.objModeloSelFinal").strSerie);
                Component.set("v.strModelo", Component.get("v.objModeloSelFinal").strModelo);
                Component.set("v.strVersion", Component.get("v.objModeloSelFinal").strVersion);
                Component.set("v.strColorExteriorCustom", Component.get("v.objModeloSelFinal").strColorExteriorCustom);
                Component.set("v.strColorInteriorCustom", Component.get("v.objModeloSelFinal").strColorInteriorCustom);
                Component.set("v.strCveColorExteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorExterior);
                Component.set("v.strCveColorInteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorInterior);
                Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);
                Component.set("v.strCantidad", Component.get("v.objModeloSelFinal").strCantidad);
                console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.objModeloSelFinal : " + Component.get("v.strAnioModelo"));
            }
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** */
    getWrpModeloCheckOut : function(Component, Event){
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getWrpModeloCheckOut...");
    	let objModeloSelAddDistPaso = Component.get("v.objModeloSelAddDist");
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getWrpModeloCheckOut : ", objModeloSelAddDistPaso);
    	//sWrpModelo : JSON.stringify(Component.get("v.getWrpModeloCheckOut"))
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getWrpModeloCheckOut");
        objAction.setParams({   			
        	sWrpModelo : JSON.stringify(objModeloSelAddDistPaso)
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
                let lstDistriEntregaResult = objResponse.getReturnValue();
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);                
                Component.set("v.objModeloSelFinal", Component.get("v.objModeloSelAddDist"));
                Component.set("v.strAnioModelo", Component.get("v.objModeloSelFinal").strAnio);
                Component.set("v.strSerie", Component.get("v.objModeloSelFinal").strSerie);
                Component.set("v.strModelo", Component.get("v.objModeloSelFinal").strModelo);
                Component.set("v.strVersion", Component.get("v.objModeloSelFinal").strVersion);
                Component.set("v.strColorExteriorCustom", Component.get("v.objModeloSelFinal").strDescripcionColorExterior);
                Component.set("v.strColorInteriorCustom", Component.get("v.objModeloSelFinal").strDescripcionColorInterior);
                Component.set("v.strCveColorExteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorExterior);
                Component.set("v.strCveColorInteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorInterior);
                Component.set("v.strPrecioSinIVA", '0.00');
                Component.set("v.strCantidad", Component.get("v.objModeloSelFinal").strCantidad);
                Component.set("v.strRecordTypeID", Component.get("v.objModeloSelFinal").strRecordTypeID);
                Component.set("v.strsTipoRegFlotProg", Component.get("v.objModeloSelFinal").strsTipoRegFlotProg);
                Component.set("v.strIdCatCentMod", Component.get("v.objModeloSelFinal").strIdCatCentMod);
                console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getWrpModeloCheckOut : " + Component.get("v.strAnioModelo"));
            }
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** */
    getDistribEntrega : function(Component, Event){
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega objModeloSelFinal: ", Component.get("v.objModeloSelFinal"));
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega objModeloSelExcepCte: ", Component.get("v.objModeloSelExcepCte"));
    	
    	//Serializa el objeto del tipo objModeloSelAddDist
    	let strObjMod = JSON.stringify(Component.get("v.objModeloSelAddDist"))
    	let ObjMod = JSON.parse(strObjMod); 
    	Component.set("v.objModeloSelFinal", ObjMod);
    	    			    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidores");
        objAction.setParams({
        	recordId : Component.get("v.recordId"),
        	intIndex : Component.get("v.intIndex"),
        	strAnioModelo : Component.get("v.strAnioModelo"),
        	strSerie : Component.get("v.strSerie"),
        	strModelo : Component.get("v.strModelo"),
        	strVersion : Component.get("v.strVersion"),
        	strIdColExt : Component.get("v.strCveColorExteriorCustom"),
        	strIdColInt : Component.get("v.strCveColorInteriorCustom"),
        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	strsTipoRegFlotProg : Component.get("v.strsTipoRegFlotProg"),
        	blnSolFlotilla : Component.get("v.blnSolFlotilla"),
        	blnSolPrograma : Component.get("v.blnSolPrograma"),
        	strIdCatCentModFinal : Component.get("v.strIdCatCentModFinal"),
        	blnSolFlotillaProgramaExcepcion : Component.get("v.blnSolFlotillaProgramaExcepcion"),
        	strIdRegistro : Component.get("v.strIdRegistro"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);

		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdSolicitud;
		    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strsTipoRegFlotProg", Component.get("v.objModeloSelFinal").strsTipoRegFlotProg);
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);        
		        console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strAnioModelo: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strIdRegistro: " + Component.get("v.strIdRegistro"));
            	
            }
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** */
    getDistribEntregaFinal : function(Component, Event){
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega objModeloSelFinal: ", Component.get("v.objModeloSelAddDist"));
    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega objModeloSelFinal: ", JSON.stringify(Component.get("v.objModeloSelAddDist")) );
    	
		//Inicializa la variables con los datos de objModeloSelFinal
        /*Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
        Component.set("v.strsTipoRegFlotProg", Component.get("v.objModeloSelFinal").strsTipoRegFlotProg);
        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
        Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);*/
    	                    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidores");
        objAction.setParams({
        	recordId : Component.get("v.recordId"),
        	intIndex : Component.get("v.intIndex"),
        	strAnioModelo : Component.get("v.strAnioModelo"),
        	strSerie : Component.get("v.strSerie"),
        	strModelo : Component.get("v.strModelo"),
        	strVersion : Component.get("v.strVersion"),
        	strIdColExt : Component.get("v.strCveColorExteriorCustom"),
        	strIdColInt : Component.get("v.strCveColorInteriorCustom"),
        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	strsTipoRegFlotProg : Component.get("v.strsTipoRegFlotProg"),
        	blnSolFlotilla : Component.get("v.blnSolFlotilla"),
        	blnSolPrograma : Component.get("v.blnSolPrograma"),
        	strIdCatCentModFinal : Component.get("v.strIdCatCentModFinal"),
        	blnSolFlotillaProgramaExcepcion : Component.get("v.blnSolFlotillaProgramaExcepcion"),
        	strIdRegistro : Component.get("v.strIdRegistro"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelAddDist")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult);

		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdSolicitud;
		    	console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strsTipoRegFlotProg", Component.get("v.objModeloSelFinal").strsTipoRegFlotProg);
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);        
		        console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strAnioModelo: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolCompraEspecConsVinesDODCmpCtrl Helper.getDistribEntrega strIdRegistro: " + Component.get("v.strIdRegistro"));
            	
            }
        });
        $A.enqueueAction(objAction);
        
	}, 


    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	let wrpDistr = [];
    	Component.set("v.lstDistriEntrega", wrpDistr); 
    	Component.set("v.blnShowVinesDOD", false);        	        
    },

})