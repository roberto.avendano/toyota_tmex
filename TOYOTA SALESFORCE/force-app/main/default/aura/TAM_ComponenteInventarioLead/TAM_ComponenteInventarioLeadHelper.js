({
    typeDevice : function(component, event, helper) {
        var deviceMobile = $A.get("$Browser.isPhone");
        if(deviceMobile === true){
            component.set("v.isMobile",true);
            
        }
        
    },
    
    statusDealer :function(component, event, helper) {
        let leadId = component.get("v.recordId");
        let action = component.get("c.estatusLead");
        action.setParams({
            leadId : leadId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set('v.leadRecord',respuesta); 
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
})