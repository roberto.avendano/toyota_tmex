({
    doInit : function(component, event, helper) {
        let recordId = component.get("v.recordId");
        helper.statusDealer(component, event, helper);
        helper.typeDevice(component, event, helper);
    },
    
    closeQuickAction : function(component, event, helper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    }
})