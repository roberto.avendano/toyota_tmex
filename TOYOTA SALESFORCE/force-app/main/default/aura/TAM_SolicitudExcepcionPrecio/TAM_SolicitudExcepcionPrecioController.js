({
    
    tipoExcepcion : function(component, event, helper) {
        var expSelected = event.getSource().get("v.value");  
        if(expSelected == 'Incentivo'){
            component.set("v.isIncentivo",true);
            component.set("v.isPrecio",false);
        }
        
        if(expSelected == 'Precio'){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",true);
        }  
        
        if(expSelected == ''){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",false);
        }  
        
    },
    
    getInfoVIN : function(component, event, helper) {
        //Validar que no exista un VIN en alguna factura cargada en Salesfor        
        
        let VIN = component.get("v.selectedRecord.VIN");
        var respuestaFacturas;
        var validaVINFactura = component.get("c.buscaFacturasVIN");
        validaVINFactura.setParams({
            VIN : VIN
            
        });
        
        validaVINFactura.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                respuestaFacturas = response.getReturnValue();
                if(respuestaFacturas.length > 0){
                    console.log("existe factura");
                    component.set("v.facturasVIN",respuestaFacturas);
                    component.set("v.existeFactura",true);
                    
                }else{
                    console.log("No existe factura");
                    component.set("v.existeFactura",false);
                    helper.validarPrecioVigente(component, event, helper);
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(validaVINFactura);
        
    },
    
    guardarRegistro : function(component, event, helper) {
        let vinID = component.get("v.selectedRecord.VIN");
        let politicaSelect = component.get("v.precioElegido");
        let motivoSelect = component.get("v.motivoSeleccionado");
        let validaDatos;
        
        if(vinID == null ){
            component.set("v.validaVin",true);
        }else{
            component.set("v.validaVin",false);
            validaDatos = true;
        }
        
        if(politicaSelect == 'Elegir Incentivo'){
            component.set("v.validaPickList",false);
        }else{
            component.set("v.validaPickList",true);
            validaDatos = true;
        }
        
        if(motivoSelect == 'Elegir un Motivo'){
            component.set("v.validaPickListMotivo",false);
            validaDatos = false;
        }else{
            component.set("v.validaPickListMotivo",true);
            validaDatos = true;
        }
        
        
        if(validaDatos == true){
            helper.guarda(component, event, helper);
        }
        
        
    },
    
    cancelar : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        component.set("v.isIncentivo",false);
        component.set("v.isPrecio",false);
        navEvent.setParams({
            "listViewName": null,
            "scope": "TAM_SolicitudesParaAprobar__c"
        });
        navEvent.fire();
        
        
        
    },
    
    onSingleSelectChange  : function(component, event, helper) {
        component.set('v.validaResumenPrecio',false);
        var idPoliticaIncentivo 		= event.getSource().get("v.value");
        var capturedOpcion 				= event.getSource().get("v.name");
        var modelo						= component.get("v.selectedRecord.TAM_codigoModelo__c");
        var anioModelo					= component.get("v.selectedRecord.TAM_AnioModelo__c")
        var validaDatos;
        
        if(idPoliticaIncentivo == 'Elegir Incentivo'){
            validaDatos = false;
            component.set("v.validaPickList",false);
            component.set("v.precioSeleccionado",false);
            
        }else{
            
            component.set("v.precioSolicitado",idPoliticaIncentivo);
            component.set("v.precioElegido",capturedOpcion);
            validaDatos = true;
            component.set("v.validaPickList",true); 
            component.set("v.precioSeleccionado",true);
            
        }
        
        
    },
    
    onSingleSelectCMotivos  : function(component, event, helper) {
        var valSelected = event.getSource().get("v.value");
        component.set("v.motivoSeleccionado",valSelected);                 
        var validaDatos;
        
        if(valSelected == 'Elegir un Motivo'){
            validaDatos = false;
            component.set("v.validaPickListMotivo",false);
            
        }else{
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);            
            
        }
        
    }
    
})