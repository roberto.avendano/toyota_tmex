({
    
    validarPrecioVigente : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.VIN");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelNumber = component.get("v.selectedRecord.codigoModelo");
        let modelYear   = component.get("v.selectedRecord.anioModelo");	
        let fechaVenta  = component.get("v.selectedRecord.fechaVentaVIN");	
        
        if(modelNumber != null && modelYear != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.calculoPrecioVigente");
            action.setParams({
                modelNumber : modelNumber,
                modelYear : modelYear,
                fechaVenta : fechaVenta
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "{}"){
                        component.set("v.precioVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{                        
                        //component.set("v.politicaNoExiste","SinPolitica");  
                        
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
    },
    guarda : function(component, event, helper) {
        let codigoDealer =  component.get("v.selectedRecord.codigoDealer");
        let vin = component.get("v.selectedRecord.VIN");
        let serie = component.get("v.selectedRecord.Toms_Series_Name__c");
        let precioVigente = component.get("v.precioVigente.precioPublico");
        let precioSolicitado = component.get("v.precioSolicitado");
        let comentario 		 = component.get("v.comentario");
        let motivoSelect = component.get("v.motivoSeleccionado");
        let politicaSeleccionada = component.get("v.politicaSeleccionada");
        
        console.log("codigo dealer"+codigoDealer);
        
        var action = component.get("c.registraSolicitudExcepcion");
        action.setParams({
            vin : vin,
            precioVigente : precioVigente,
            precioSolicitado : precioSolicitado,
            comentario         : comentario,
            motivoSelect	   : motivoSelect,
            codigoDealer			   : codigoDealer
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.selectedRecord",null);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
                
                var idRegistro = response.getReturnValue(); 
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": idRegistro,
                    "slideDevName": "related"
                });
                navEvt.fire();
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    obtenListaIncentivos : function(component, event, helper) {
        let modelNumber = component.get("v.selectedRecord.codigoModelo");
        let modelYear   = component.get("v.selectedRecord.anioModelo");	           
        
        var action = component.get("c.getPreciosVIN");
        action.setParams({
            modelNumber : modelNumber,
            modelYear : modelYear
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                if(JSON.stringify(respuesta) != "{}"){
                    component.set("v.preciosVigenteLista", response.getReturnValue());
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    obtenMotivosSolicitud : function(component, event, helper) {
        var action = component.get("c.obtieneMotivosSolicitud");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.motivosSolicitud",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
    
})