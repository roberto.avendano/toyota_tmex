({
    
    validarPolitica : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN__c");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo__c");
        let anioModelo   = component.get("v.selectedRecord.TAM_AnioModelo__c");	
        let fechaVentaVIN   = component.get("v.selectedRecord.Sale_Date__c");	
        let solicitaBono	= "SinBono";
        
        let tipoVenta = component.get("v.tipoVenta");
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.getPoliticasVigentes");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaVIN : fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : tipoVenta
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","SinBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{         
                        helper.obtenListaIncentivos(component, event, helper);
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
    },
    
    validarPoliticaBonoLealtad : function(component, event, helper) {
        //Validar si existe politica de incentivos vigente para el VIN + Bono de lealtad
        component.set("v.politicaNoExiste",false);
        let existePolitica = false;
        let vinID = component.get("v.selectedRecord.TAM_VIN__c");
        let validaDatos = false;
        if(vinID == null){
            component.set("v.validaVin",true);
            component.set("v.politicaVigente",null);
        }else{
            component.set("v.validaVin",false);
        }
        
        let modelo = component.get("v.selectedRecord.TAM_codigoModelo__c");
        let anioModelo   = component.get("v.selectedRecord.TAM_AnioModelo__c");	
        let fechaVentaVIN   = component.get("v.selectedRecord.Sale_Date__c");
        let solicitaBono	= "ConBono";
        let tipoVenta = component.get("v.tipoVenta");
        
        
        
        if(modelo != null && anioModelo != null){
            validaDatos = true;
            
        }     
        
        if(validaDatos == true){
            var action = component.get("c.getPoliticasVigentes");
            action.setParams({
                modelo : modelo,
                anioModelo : anioModelo,
                fechaVentaVIN :fechaVentaVIN,
                solicitaBono : solicitaBono,
                tipoVenta : tipoVenta
            });
            
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    if(JSON.stringify(respuesta) != "[]"){
                        component.set("v.politicaVigente", response.getReturnValue());
                        component.set("v.politicaNoExiste","ConPolitica");
                        component.set("v.solicitaBono","ConBono");
                        existePolitica = true;
                        helper.obtenListaIncentivos(component, event, helper); 
                        //helper.obtenMotivosSolicitud(component, event, helper); 
                    }else{     
                        helper.obtenListaIncentivos(component, event, helper); 
                        component.set("v.politicaNoExiste","SinPolitica");  
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
        }
        
        
        
        
        
    },
    
    guarda : function(component, event, helper) {
        let vin = component.get("v.selectedRecord.TAM_VIN__c");
        let serie = component.get("v.selectedRecord.Toms_Series_Name__c");
        let incentivoVigente = component.get("v.politicaVigente.TAM_IncentivoTMEX_Cash__c");
        let incentivoPropuesto = component.get("v.incentivoPropuesto");
        let comentario 		 = component.get("v.comentario");
        let motivoSelect = component.get("v.motivoSeleccionado");
        let politicaSeleccionada = component.get("v.politicaSeleccionada");
        let dealerId = component.get("v.selectedRecord.Distribuidor__c");
        let politicasCorrespondientes = component.get("v.politicaVigente");
        let tipoVenta = component.get("v.tipoVenta");
        let tipoRegistro = 'AutoDemo';
        
        let action = component.get("c.registraSolicitudExcepcion");
        action.setParams({
            vin : vin,
            serie : serie,
            incentivoVigente : incentivoVigente,
            incentivoPropuesto : incentivoPropuesto,
            comentario         : comentario,
            motivoSelect	   : motivoSelect,
            politicaSeleccionada : politicaSeleccionada,
            dealerId				: dealerId,
            politicasCorrespondientes : politicasCorrespondientes,
            tipoVenta	: tipoVenta,
            tipoRegistro : tipoRegistro
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Exitoso!",
                    "message": "Se registro correctamente."
                });
                toastEvent.fire();
                component.set("v.isIncentivo",false);
                component.set("v.isPrecio",false);
                
                var idRegistro = response.getReturnValue(); 
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": idRegistro,
                    "slideDevName": "related"
                });
                navEvt.fire();
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    obtenListaIncentivos : function(component, event, helper) {
        let modelNumber = component.get("v.selectedRecord.TAM_codigoModelo__c");
        let modelYear   = component.get("v.selectedRecord.TAM_AnioModelo__c");	           
        
        var action = component.get("c.getPoliticasVIN");
        action.setParams({
            modelNumber : modelNumber,
            modelYear : modelYear
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                
                if(JSON.stringify(respuesta) != "[]"){
                    component.set("v.politicaVigenteLista", response.getReturnValue());
                    
                    component.set("v.existeListaPoliticas","ConPoliticas");
                }else{
                    
                    component.set("v.existeListaPoliticas","SinPoliticas");
                    
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
        
    },
    
    obtenMotivosSolicitud : function(component, event, helper) {
        var action = component.get("c.obtieneMotivosSolicitud");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var respuesta = response.getReturnValue();
                component.set("v.motivosSolicitud",respuesta);
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
    
})