({
    
    tipoExcepcion : function(component, event, helper) {
        var expSelected = event.getSource().get("v.value");  
        if(expSelected == 'Incentivo'){
            component.set("v.isIncentivo",true);
            component.set("v.isPrecio",false)
            component.set("v.isAutoDemo",false);
        }
        
        if(expSelected == 'Precio'){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",true);
            component.set("v.isAutoDemo",false);
        }  
        
        
        if(expSelected == 'AutoDemo'){
            component.set("v.isPrecio",false);
            component.set("v.isIncentivo",false);
            component.set("v.isAutoDemo",true);
        }  
        
        if(expSelected == ''){
            component.set("v.isIncentivo",false);
            component.set("v.isPrecio",false);
            
        }  
        
    },
    
    getInfoVIN : function(component, event, helper) {
        //Validar que no exista un VIN en alguna factura cargada en Salesfor
        
        let VIN = component.get("v.selectedRecord.TAM_VIN__c");
        var respuestaFacturas;
        var validaVINFactura = component.get("c.buscaFacturasVIN");
        validaVINFactura.setParams({
            VIN : VIN
            
        });
        
        validaVINFactura.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                respuestaFacturas = response.getReturnValue();
                if(respuestaFacturas.length > 0){
                    component.set("v.facturasVIN",respuestaFacturas);
                    component.set("v.existeFactura",true);
                    
                }else{
                    component.set("v.existeFactura",false);
                    //helper.validarPolitica(component, event, helper);
                    helper.obtenMotivosSolicitud(component, event, helper); 
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(validaVINFactura);
        
    },
    
    guardarRegistro : function(component, event, helper) {
        let vinID = component.get("v.selectedRecord.Id");
        let politicaSelect = component.get("v.politicaSeleccionada");
        let motivoSelect = component.get("v.motivoSeleccionado");
        let validaDatos;
        
        /*
        if(vinID == null ){
            component.set("v.validaVin",true);
        }else{
            component.set("v.validaVin",false);
            validaDatos = true;
        }*/
        
        if(politicaSelect === 'Elegir Incentivo'){
            component.set("v.validaPickList",false);
        }else{
            component.set("v.validaPickList",true);
            validaDatos = true;
        }
        
        /*
        if(motivoSelect == 'Elegir un Motivo'){
            component.set("v.validaPickListMotivo",false);
            validaDatos = false;
        }else{
            component.set("v.validaPickListMotivo",true);
            validaDatos = true;
        }*/
        
        
        if(validaDatos == true){
            helper.guarda(component, event, helper);
        }
        
        
    },
    
    cancelar : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        component.set("v.isIncentivo",false);
        component.set("v.isPrecio",false);
        navEvent.setParams({
            "listViewName": null,
            "scope": "TAM_SolicitudesParaAprobar__c"
        });
        navEvent.fire();
        
        
        
    },
    
    onSingleSelectChange  : function(component, event, helper) {
        component.set('v.validaResumenPrecio',false);
        var idPoliticaIncentivo 		= event.getSource().get("v.value");
        var capturedOpcion 				= event.getSource().get("v.name");
        var modelo						= component.get("v.selectedRecord.TAM_codigoModelo__c");
        var anioModelo					= component.get("v.selectedRecord.TAM_AnioModelo__c")
        var validaDatos;
        var solicitaBono	= component.get("v.solicitaBono");
        
        if(idPoliticaIncentivo == 'Elegir Incentivo'){
            component.set("v.politicaSeleccionada","Elegir Incentivo");
            validaDatos = false;
            component.set("v.validaPickList",false);
            
        }else{
            component.set("v.politicaSeleccionada",idPoliticaIncentivo);
            validaDatos = true;
            component.set("v.validaPickList",true);            
            
        }
        
        
        if(validaDatos == true){
            var action = component.get("c.calculoPrecioIncentico");
            action.setParams({
                idPoliticaIncentivo : idPoliticaIncentivo,
                modelo : modelo,
                anioModelo : anioModelo,
                solicitaBono : solicitaBono
                
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var respuesta = response.getReturnValue();
                    component.set('v.resumenPrecio',respuesta);
                    component.set('v.validaResumenPrecio',true);
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
            
        }
        
    },
    
    onSingleSelectVenta  : function(component, event, helper) {
        var valSelected = event.getSource().get("v.value");
        component.set("v.tipoVenta",valSelected); 
        var bono = component.get("v.solicitaBono");
        component.find("selectIncentivos").set("v.value",'Elegir un Motivo');
        component.set("v.solicitaBono",null);
        
        if(valSelected == 'default'){
            
            component.set("v.solicitaBono",null);
            component.find("selectIncentivos").set("v.value",'Elegir un Motivo');
            
        }
        
    },
    
    onSingleSelectCMotivos  : function(component, event, helper) {
        var valSelected = event.getSource().get("v.value");
        component.set("v.motivoSeleccionado",valSelected);                 
        //component.set("v.solicitaBono",null);
        var validaDatos;
        
        if(valSelected == 'Elegir un Motivo'){
            validaDatos = false;
            component.set("v.validaPickListMotivo",false);
            component.set("v.solicitaBono",null);
            
        }
        if(valSelected == 'El cliente apartó su unidad en el mes de la promoción, pero se reportó hasta el siguiente. (Incentivo+Bono Lealtad)' || valSelected == 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.(Incentivo+Bono Lealtad)' ){
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);
            component.set("v.solicitaBono",'ConBono');
            helper.validarPoliticaBonoLealtad(component, event, helper);
            
        }
        
        if(valSelected == 'Otros motivos.' || valSelected == 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.' || valSelected == 'El cliente apartó su unidad en el mes de la promoción, pero se reportó hasta el siguiente.'){
            validaDatos = true;
            component.set("v.validaPickListMotivo",true);
            component.set("v.solicitaBono",'SinBono');
            helper.validarPolitica(component, event, helper);            
            
        }
        
    }, // function automatic called by aura:waiting event  
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    cancelar : function(component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        component.set("v.isIncentivo",false);
        component.set("v.isPrecio",false);
        navEvent.setParams({
            "listViewName": null,
            "scope": "TAM_SolicitudesParaAprobar__c"
        });
        navEvent.fire();
        
        
        
    }
    
})