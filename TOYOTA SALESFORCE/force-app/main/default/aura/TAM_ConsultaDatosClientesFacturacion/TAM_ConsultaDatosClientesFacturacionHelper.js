({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
		console.log("EN Consulta Datos Cliente Helper.initializeComponent...");
		                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getDatosCandidato");
        objAction.setParams({   			
        	recordId : strIdCandidato
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objDatosCandidato = objResponse.getReturnValue();
            	//Inicializa los datos del candidato	
            	objComponent.set("v.DatosCandidato", objDatosCandidato);
            	console.log("EN initializeComponent DatosCandidato: ", objComponent.get("v.DatosCandidato"));            	
            	this.ValidaRolUsr(objComponent, objEvent);
            	this.ValidaEstatusLead(objComponent, objEvent);
            	this.PermiteConsultarDatosCteDealer(objComponent, objEvent, objDatosCandidato);
            	this.VinesSeleccionadosProspectoFact(objComponent, objEvent, objDatosCandidato);
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent objDatosCandidato.TAM_TipoPersonaFact__c: ", objDatosCandidato.TAM_TipoPersonaFact__c);
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent objDatosCandidato.TAM_NombreContactoServicio__c: ", objDatosCandidato.TAM_NombreContactoServicio__c);
            	//Ve si los datos ya estan validados para poder facturar
            	if (objDatosCandidato.TAM_NombreContactoServicio__c !== undefined && objDatosCandidato.TAM_NombreContactoServicio__c != '' && objDatosCandidato.TAM_NombreContactoServicio__c != null)
            		objComponent.set("v.blnMuestraCamposContacto",true);
            	//Ve si los datos ya estan validados para poder facturar
            	if (objDatosCandidato.TAM_ValidaDatosFactura__c)
            		objComponent.set("v.blnConfirmaDatosFact",true);
            	//Ve si los datos ya estan validados para poder facturar
            	if (objDatosCandidato.TAM_TipoPersonaFact__c === 'Persona moral' || objDatosCandidato.TAM_TipoPersonaFact__c === 'Persona Moral' || objDatosCandidato.TAM_TipoPersonaFact__c === 'Fisica con actividad empresarial'){
            		objComponent.set("v.blnMoralFisicaActEmp", true);
            		objDatosCandidato.TAM_ApellidoPaternoRL__c = null;
            		objDatosCandidato.TAM_ApellidoMaternoRL__c = null;            		
            	}//Fin si objDatosCandidato.TAM_TipoPersonaFact__c === 'Persona moral' || objDatosCandidato.TAM_TipoPersonaFact__c === 'Persona Moral' || objDatosCandidato.TAM_TipoPersonaFact__c === 'Fisica con actividad empresarial'
            	if (objDatosCandidato.TAM_TipoPersonaFact__c === 'Person física' || objDatosCandidato.TAM_TipoPersonaFact__c === 'Persona Física')    	
            		objComponent.set("v.blnMoralFisicaActEmp", false);
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent blnMoralFisicaActEmp: ", objComponent.get("v.blnMoralFisicaActEmp"));
            	console.log("EN Consulta Datos Cliente Helper.initializeComponent blnMuestraCamposContacto: ", objComponent.get("v.blnMuestraCamposContacto"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	ValidaRolUsr : function(objComponent, objEvent) {
		console.log("EN ValidaRolUsr...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getValidaRolUsr");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let bTieneRolAdecuadoResult = objResponse.getReturnValue();
           		objComponent.set("v.bTieneRolAdecuado", bTieneRolAdecuadoResult);
           		console.log("EN ValidaRolUsr bTieneRolAdecuado: ", objComponent.get("v.bTieneRolAdecuado") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	ValidaEstatusLead : function(objComponent, objEvent) {
		console.log("EN ValidaEstatusLead...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getEstatusLead");
        objAction.setParams({
        	recordId : strIdCandidato
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let strEstatusLeadResult = objResponse.getReturnValue();
            	if (strEstatusLeadResult === 'Pedido en Proceso')
            		objComponent.set("v.blnPedidoProceso", false);
           		console.log("EN TipoOfertaFact sTipoOfertaFact: ", objComponent.get("v.blnPedidoProceso") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	PermiteConsultarDatosCteDealer : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN PermiteConsultarDatosCteDealer...");		
       	console.log("EN PermiteConsultarDatosCteDealer DatosCandidato: ", objComponent.get("v.DatosCandidato"));            	
				                
        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidatoPaso = objComponent.get("v.DatosCandidato");
        
        //Llama la función 
		let objAction = objComponent.get("c.getPermiteConsultarDatosCteDealer");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objComponent.get("v.DatosCandidato")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let blnConsultaCtesDealerResult = objResponse.getReturnValue();
            	objComponent.set("v.DatosDistribuidor", blnConsultaCtesDealerResult);
           		objComponent.set("v.blnConsultaCtesDealer", false);
            	//Ve si tiene permiso para poder consultar los datos del cliente}
            	if (blnConsultaCtesDealerResult.TAM_UrlConsultaCtesDealer__c != '' && blnConsultaCtesDealerResult.TAM_UrlConsultaCtesDealer__c != null
            		&& blnConsultaCtesDealerResult.TAM_UrlConsultaCtesDealer__c != 'null')
            		objComponent.set("v.blnConsultaCtesDealer", true);              		
            	let DatosDistribuidor = objComponent.get("v.DatosDistribuidor");
            	console.log("EN PermiteConsultarDatosCteDealer DatosDistribuidor: ", DatosDistribuidor);
            	//Ve si se trata de autosystem y entonces deja que consulte los datos del catalogo de Vendedores
            	if (DatosDistribuidor.TAM_TipoIntegreacion__c == 'Autosystem')
            		this.AsesorVentasFactAuto(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
            	//Ve si se trata de Quiter y entonces cosnulta el resto de los catalogos
            	if (DatosDistribuidor.TAM_TipoIntegreacion__c == 'Quiter'){
            	
					this.TipoOfertaFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));            	
	            	this.AsesorVentasFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	this.OrigenFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	this.AgenciaFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	this.EdoPedInicialFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	this.TipoVentaFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	this.TipoVentaDestinoFact(objComponent, objEvent, objComponent.get("v.DatosCandidato"));
	            	
	            	//Prende la bandera que permite actualizar los datosl del cliente
    				objComponent.set("v.blnUpdDatosCliente", true);
	            	
            		//Ve si ya tiene un numero de cliente asignado            		
            		if (objComponent.get("v.DatosCandidato").FWY_ID_de_cliente__c != null ){
		            	//TAM_IdColoniaQuiter__c, TAM_IdDelMunQuiter__c, TAM_IdCiudadQuiter__c, TAM_IdEstadoQuiter__c
		            	//Llama los catalogos para la direccion en base a la llave de cada catalogo
	    				//Manda llamar el sevvicio de la colonia
	    				this.IniListaColoniaFact(objComponent, objEvent, objComponent.get("v.DatosCandidato").TAM_IdColoniaQuiter__c, objComponent.get("v.DatosCandidato").TAM_Colonia__c);
	    				//Manda llamar el sevvicio de la Del o Municipio
	    				this.IniListaDelMunFact(objComponent, objEvent, objComponent.get("v.DatosCandidato").TAM_IdDelMunQuiter__c, objComponent.get("v.DatosCandidato").TAM_DelegacionMunicipio__c);
	    				//Manda llamar el sevvicio de la ciudad
	    				this.IniListaCiudadFact(objComponent, objEvent, objComponent.get("v.DatosCandidato").TAM_IdCiudadQuiter__c, objComponent.get("v.DatosCandidato").TAM_Ciudad__c);
	    				//Manda llamar el sevvicio del Estado
	    				this.IniListaEstadoFact(objComponent, objEvent, objComponent.get("v.DatosCandidato").TAM_IdEstadoQuiter__c, objComponent.get("v.DatosCandidato").TAM_Estado__c);
	    				//Ya tiene un numero de cliente en quiter entonces no dejes que actualice
	    				objComponent.set("v.blnUpdDatosCliente", false);
	    				objComponent.set("v.blnDesacCampQuiter", true);
    				}//Fin si objComponent.get("v.DatosCandidato").TAM_IdColoniaQuiter__c !== null
    				
	            	//Inicializa los datos para Quiter
	            	objComponent.set("v.blnQuiter", true);
            	}//Fin si TAM_TipoIntegreacion__c
           		console.log("EN PermiteConsultarDatosCteDealer sTipoOfertaFact: ", objComponent.get("v.blnConsultaCtesDealer") );
           		console.log("EN PermiteConsultarDatosCteDealer DatosCandidato: ", objComponent.get("v.DatosCandidato") );

            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	TipoOfertaFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN TipoOfertaFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getTipoOfertaFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lTipoOfertaFatResult = objResponse.getReturnValue();
            	let sTipoOfertaFactPaso = objDatosCandidatoPaso.TAM_TipoOfertaFac__c; 
            	objComponent.set("v.lTipoOfertaFact", lTipoOfertaFatResult);
            	if (sTipoOfertaFactPaso === undefined){
            		if (lTipoOfertaFatResult.length > 0)
            			objComponent.set("v.sTipoOfertaFact", lTipoOfertaFatResult[0].strCodigo);
            	}
            	if (sTipoOfertaFactPaso !== undefined)
            		objComponent.set("v.sTipoOfertaFact", sTipoOfertaFactPaso);
           		console.log("EN TipoOfertaFact sTipoOfertaFact: ", objComponent.get("v.sTipoOfertaFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	AsesorVentasFactAuto : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN AsesorVentasFactAuto...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let objDatosDistribuidor = objComponent.get("v.DatosDistribuidor");
             
        //Llama la función 
		let objAction = objComponent.get("c.getAsesorVentasFactAuto");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objDatosCandidatoPaso,
        	"datosDistrib" : objDatosDistribuidor
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let llAsesorVentasFactAuto = objResponse.getReturnValue();
            	let sAsesorVentasFactAutoPaso = objDatosCandidatoPaso.TAM_AsesorVentasFac__c; 
            	objComponent.set("v.lAsesorVentasFactAuto", llAsesorVentasFactAuto);
            	if (sAsesorVentasFactAutoPaso === undefined){
            		if (llAsesorVentasFactAuto.length > 0)
            			objComponent.set("v.sAsesorVentasFactAuto", llAsesorVentasFactAuto[0].strCodigo);            	
            	}
            	if (sAsesorVentasFactAutoPaso !== undefined)
            		objComponent.set("v.sAsesorVentasFactAuto", sAsesorVentasFactAutoPaso);            		
           		console.log("EN AsesorVentasFactAuto lAsesorVentasFactAuto: ", objComponent.get("v.lAsesorVentasFactAuto") );            	
           		console.log("EN AsesorVentasFactAuto sAsesorVentasFactAuto: ", objComponent.get("v.sAsesorVentasFactAuto") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	AsesorVentasFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN AsesorVentasFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let objDatosDistribuidor = objComponent.get("v.DatosDistribuidor");        
        
        //Llama la función 
		let objAction = objComponent.get("c.getAsesorVentasFact");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objDatosCandidatoPaso,
        	"datosDistrib" : objDatosDistribuidor        	
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let llAsesorVentasFact = objResponse.getReturnValue();
            	let sAsesorVentasFactPaso = objDatosCandidatoPaso.TAM_AsesorVentasFac__c; 
            	objComponent.set("v.lAsesorVentasFact", llAsesorVentasFact);
            	if (sAsesorVentasFactPaso === undefined){
            		if (llAsesorVentasFact.length > 0)
            			objComponent.set("v.sAsesorVentasFact", llAsesorVentasFact[0].strCodigo);            	
            	}
            	if (sAsesorVentasFactPaso !== undefined)
            		objComponent.set("v.sAsesorVentasFact", sAsesorVentasFactPaso);            		
           		console.log("EN TipoOfertaFact sAsesorVentasFact: ", objComponent.get("v.sAsesorVentasFact") );            	
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	OrigenFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN OrigenFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getOrigenFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lOrigenFatResult = objResponse.getReturnValue();
            	let sOrigemFactPaso = objDatosCandidatoPaso.TAM_OrigenFac__c;
            	objComponent.set("v.lOrigenFact", lOrigenFatResult);            	 
            	if (sOrigemFactPaso === undefined){
            		if (lOrigenFatResult.length > 0)
            			objComponent.set("v.sOrigemFact", lOrigenFatResult[0].strCodigo);
            	}
            	if (sOrigemFactPaso !== undefined)
            		objComponent.set("v.sOrigemFact", sOrigemFactPaso);
           		console.log("EN TipoOfertaFact sOrigemFact: ", objComponent.get("v.sOrigemFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	AgenciaFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN AgenciaFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let objDatosDistribuidor = objComponent.get("v.DatosDistribuidor");        
        
        //Llama la función 
		let objAction = objComponent.get("c.getAgenciaFact");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objDatosCandidatoPaso,
        	"datosDistrib" : objDatosDistribuidor        	
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lAgenciaFatResult = objResponse.getReturnValue();
            	let sAgenciaFactPaso = objDatosCandidatoPaso.TAM_AgenciaFac__c;
            	objComponent.set("v.lAgenciaFact", lAgenciaFatResult);
            	if (sAgenciaFactPaso === undefined){
            		if (lAgenciaFatResult.length > 0)
            			objComponent.set("v.sAgenciaFact", lAgenciaFatResult[0].strCodigo);
            	}
            	if (sAgenciaFactPaso !== undefined)
            		objComponent.set("v.sAgenciaFact", sAgenciaFactPaso);
           		console.log("EN TipoOfertaFact sAgenciaFact: ", objComponent.get("v.sAgenciaFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	EdoPedInicialFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN EdoPedInicialFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getEdoPedInicialFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lEdoPedInicialFatResult = objResponse.getReturnValue();
            	let sEdoPedInicialFactPaso = objDatosCandidatoPaso.TAM_EstadoPedidoInicialFac__c;
           		console.log("EN EdoPedInicialFact lEdoPedInicialFatResult: ", lEdoPedInicialFatResult);            	
            	objComponent.set("v.lEdoPedInicialFact", lEdoPedInicialFatResult);
            	if (sEdoPedInicialFactPaso === undefined){
            		if (lEdoPedInicialFatResult.length > 0)
            			objComponent.set("v.sEdoPedInicialFact", lEdoPedInicialFatResult[0].strCodigo);
            	}
            	if (sEdoPedInicialFactPaso !== undefined)
            		objComponent.set("v.sEdoPedInicialFact", sEdoPedInicialFactPaso);
           		console.log("EN TipoOfertaFact sEdoPedInicialFact: ", objComponent.get("v.sEdoPedInicialFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	TipoVentaFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN TipoVentaFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getTipoVentaFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lTipoVentaFactResult = objResponse.getReturnValue();
            	let sTipoVentaFactPaso = objDatosCandidatoPaso.TAM_TipoVentaFac__c;
            	objComponent.set("v.lTipoVentaFact", lTipoVentaFactResult);
            	if (sTipoVentaFactPaso === undefined){
            		if (lTipoVentaFactResult.length > 0)
            			objComponent.set("v.sTipoVentaFact", lTipoVentaFactResult[0].strCodigo);
            	}
            	if (sTipoVentaFactPaso !== undefined)
            		objComponent.set("v.sTipoVentaFact", sTipoVentaFactPaso);
           		console.log("EN TipoOfertaFact sTipoVentaFact: ", objComponent.get("v.sTipoVentaFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	TipoVentaDestinoFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN TipoVentaDestinoFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getTipoVentaDestinoFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lTipoVentaDestinoFactResult = objResponse.getReturnValue();
            	let sTipoVentaDestinoFactPaso = objDatosCandidatoPaso.TAM_TipoVentaDestinoFac__c;
            	objComponent.set("v.lTipoVentaDestinoFact", lTipoVentaDestinoFactResult);
            	if (sTipoVentaDestinoFactPaso === undefined){
            		if (lTipoVentaDestinoFactResult.length > 0)
            			objComponent.set("v.sTipoVentaDestinoFact", lTipoVentaDestinoFactResult[0].strCodigo);
            	}
            	if (sTipoVentaDestinoFactPaso !== undefined)
            		objComponent.set("v.sTipoVentaDestinoFact", sTipoVentaDestinoFactPaso);
           		console.log("EN TipoOfertaFact sTipoVentaDestinoFact: ", objComponent.get("v.sTipoVentaDestinoFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	VinesSeleccionadosProspectoFact : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN TipoVentaDestinoFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getVinesSelecProspectoFact");
        objAction.setParams({
        	recordId : strIdCandidato,
        	datosCandidato : objDatosCandidatoPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lVinesSeleProspFactResult = objResponse.getReturnValue();
            	objComponent.set("v.lVinesSeleProspFact", lVinesSeleProspFactResult);
           		console.log("EN VinesSeleccionadosProspectoFact lVinesSeleProspFact: ", objComponent.get("v.lVinesSeleProspFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },



    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecAsesorVetaFactAuto: function(objComponent, objEvent) {
    	console.log('EN Save SelecAsesorVetaFactAuto...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sAsesorVentasFactAuto",value);        
        console.log('EN Save SelecAsesorVetaFactAuto value: ', value);
        console.log('EN Save SelecAsesorVetaFactAuto sAsesorVentasFactAuto: ', objComponent.get("v.sAsesorVentasFactAuto"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecTipoOfertaFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecTipoOfertaFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sTipoOfertaFact",value);        
        console.log('EN Save SelecTipoOfertaFact value: ', value);
        console.log('EN Save SelecTipoOfertaFact sTipoOfertaFact: ', objComponent.get("v.sTipoOfertaFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecAsesorVentasFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecAsesorVentasFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sAsesorVentasFact",value);
        console.log('EN Save SelecAsesorVentasFact sAsesorVentasFact: ', objComponent.get("v.sAsesorVentasFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecOrigenFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecOrigenFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sOrigemFact",value);
        console.log('EN Save SelecOrigenFact sOrigemFact: ', objComponent.get("v.sOrigemFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecAgenciaFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecAgenciaFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sAgenciaFact",value);
        console.log('EN Save SelecAgenciaFact sAgenciaFact: ', objComponent.get("v.sAgenciaFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecEdoPedInicialFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecEdoPedInicialFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sEdoPedInicialFact",value);
        console.log('EN Save SelecEdoPedInicialFact sEdoPedInicialFact: ', objComponent.get("v.sEdoPedInicialFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecTipoVentaFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecTipoVentaFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sTipoVentaFact",value);
        console.log('EN Save SelecTipoVentaFact sTipoVentaFact: ', objComponent.get("v.sTipoVentaFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecTipoVentaDestinoFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecTipoVentaDestinoFact...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sTipoVentaDestinoFact",value);
        console.log('EN Save SelecTipoVentaDestinoFact sTipoVentaDestinoFact: ', objComponent.get("v.sTipoVentaDestinoFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    SelecMonedaVentaFact: function(objComponent, objEvent) {
    	console.log('EN Save SelecMonedaVentaFact...');
        let value = objEvent.getSource().get("v.value");        
        let indexvar = objEvent.getSource().get("v.label");
    	console.log('EN Save SelecMonedaVentaFact value: ' + value);
    	console.log('EN Save SelecMonedaVentaFact indexvar: ' + indexvar);
        objComponent.set("v.sMonedaVentaFact",value);
        console.log('EN Save SelecMonedaVentaFact sMonedaVentaFact: ', objComponent.get("v.sMonedaVentaFact"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    validaTipoPersonaFact: function(objComponent, objEvent) {
    	console.log('EN Save validaTipoPersonaFact...');
        let value = objEvent.getSource().get("v.value");        
        let indexvar = objEvent.getSource().get("v.label");
    	console.log('EN Save validaTipoPersonaFact value: ' + value);
    	console.log('EN Save validaTipoPersonaFact indexvar: ' + indexvar);
    	if (value === 'Persona Moral') //|| value === 'Fisica con actividad empresarial'    	
    		objComponent.set("v.blnMoralFisicaActEmp", true);
    	if (value === 'Persona Física' || value === 'Fisica con actividad empresarial')    	
    		objComponent.set("v.blnMoralFisicaActEmp", false);
        console.log('EN Save validaTipoPersonaFact sMonedaVentaFact: ', objComponent.get("v.blnMoralFisicaActEmp"));
    },


    //Funcion que sirve para seleccionar el tipo de Excepción
    usarDatosCteSol: function(objComponent, objEvent) {
    	console.log('EN usarDatosCteSol...');
		var checkCmp = objComponent.find("chkUsarDatosCteSol");
        let blnUsaDatosCte = checkCmp.get("v.value");
    	objComponent.set("v.UsarDatosCteSol", blnUsaDatosCte)	
    	console.log('EN usarDatosCteSol UsarDatosCteSol: ' + objComponent.get("v.UsarDatosCteSol"));
    	console.log('EN usarDatosCteSol blnUsaDatosCte: ' + blnUsaDatosCte);
        let DatosCandidatoPaso = objComponent.get("v.DatosCandidato");       
    	
    	//Ve si selecciono usar los datos del cliente para los del contacto.
    	if (blnUsaDatosCte === true){
   			//Actualiza los datos del cliente 
   			DatosCandidatoPaso.TAM_NombreContFact__c = DatosCandidatoPaso.TAM_NombreRL__c;
   			DatosCandidatoPaso.TAM_ApellidoPaternoContFact__c = DatosCandidatoPaso.TAM_ApellidoPaternoRL__c;
   			DatosCandidatoPaso.TAM_ApellidoMaternoContFact__c = DatosCandidatoPaso.TAM_ApellidoMaternoRL__c;
   			DatosCandidatoPaso.TAM_RFCContFact__c = DatosCandidatoPaso.TAM_RFCRL__c;
   			DatosCandidatoPaso.TAM_TelefonoContFact__c = DatosCandidatoPaso.TAM_TelefonoRL__c;
   			DatosCandidatoPaso.TAM_TelefonoCelularContFact__c = DatosCandidatoPaso.TAM_TelefonoCelularRL__c;
   			DatosCandidatoPaso.TAM_CorreoElectronicoContFact__c = DatosCandidatoPaso.TAM_CorreoElectronicoRL__c;
   			DatosCandidatoPaso.TAM_CalleContFact__c = DatosCandidatoPaso.TAM_Calle__c;
   			DatosCandidatoPaso.TAM_NoExtContacto__c = DatosCandidatoPaso.TAM_NoExt__c;
   			DatosCandidatoPaso.TAM_NoIntContacto__c = DatosCandidatoPaso.TAM_NoInt__c;
   			DatosCandidatoPaso.TAM_CodigoPostalContFact__c = DatosCandidatoPaso.TAM_CodigoPostal__c;
   			DatosCandidatoPaso.TAM_ColoniaContFact__c = DatosCandidatoPaso.TAM_Colonia__c;
   			DatosCandidatoPaso.TAM_DelegacionMunicipioContFact__c = DatosCandidatoPaso.TAM_DelegacionMunicipio__c;
   			DatosCandidatoPaso.TAM_CiudadContFact__c = DatosCandidatoPaso.TAM_Ciudad__c;
   			DatosCandidatoPaso.TAM_EstadoContFact__c = DatosCandidatoPaso.TAM_Estado__c;
   			DatosCandidatoPaso.TAM_PaisContFact__c = DatosCandidatoPaso.TAM_Pais__c;
   			console.log('EN usarDatosCteSol DatosCandidatoPaso: ', DatosCandidatoPaso);
   			objComponent.set("v.DatosCandidato", DatosCandidatoPaso);
   			console.log('EN usarDatosCteSol DatosCandidato: ', objComponent.get("v.DatosCandidato"));
           	console.log("EN usarDatosCteSol DatosCandidatoInicial FIN: " , objComponent.get("v.DatosCandidatoInicial"));
    	}//Fin si blnUsaDatosCte === true

    	//Ve si selecciono usar los datos del cliente para los del contacto.
    	if (blnUsaDatosCte === false){
   			//Actualiza los datos del cliente 
   			DatosCandidatoPaso.TAM_NombreContFact__c = null;
   			DatosCandidatoPaso.TAM_ApellidoPaternoContFact__c = null;
   			DatosCandidatoPaso.TAM_ApellidoMaternoContFact__c = null;
   			DatosCandidatoPaso.TAM_RFCContFact__c = null;
   			DatosCandidatoPaso.TAM_TelefonoContFact__c = null;
   			DatosCandidatoPaso.TAM_TelefonoCelularContFact__c = null;
   			DatosCandidatoPaso.TAM_CorreoElectronicoContFact__c = null;
   			DatosCandidatoPaso.TAM_CalleContFact__c = null;
   			DatosCandidatoPaso.TAM_NoExtContacto__c =  null;
   			DatosCandidatoPaso.TAM_NoIntContacto__c = null;
   			DatosCandidatoPaso.TAM_CodigoPostalContFact__c = null;
   			DatosCandidatoPaso.TAM_ColoniaContFact__c = null;
   			DatosCandidatoPaso.TAM_DelegacionMunicipioContFact__c = null;
   			DatosCandidatoPaso.TAM_CiudadContFact__c = null;
   			DatosCandidatoPaso.TAM_EstadoContFact__c = null;
   			DatosCandidatoPaso.TAM_PaisContFact__c = null;
   			console.log('EN usarDatosCteSol DatosCandidatoPaso: ', DatosCandidatoPaso);
   			objComponent.set("v.DatosCandidato", DatosCandidatoPaso);
   			console.log('EN usarDatosCteSol DatosCandidato: ', objComponent.get("v.DatosCandidato"));
           	console.log("EN usarDatosCteSol DatosCandidatoInicial FIN: " , objComponent.get("v.DatosCandidatoInicial"));
    	}//Fin si blnUsaDatosCte === true    	
    	
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    muestraDatosContacto: function(objComponent, objEvent) {
    	console.log('EN Save muestraDatosContacto...');
    	let blnMuestraCamposContacto = objComponent.get("v.blnMuestraCamposContacto");
		let checkCmpContResp = objComponent.find("chkMttoDifUsr");
        let checkCmpContRespValue = checkCmpContResp.get("v.value");
    	console.log('EN Save muestraDatosContacto checkCmpContRespValue: ' + checkCmpContRespValue);
        objComponent.set("v.blnMuestraCamposContacto", checkCmpContRespValue);
        //Si lo desmarco entonces limpia los campos del objeto DatosCandidato
        if (!checkCmpContRespValue){
        	let DatosCandidatoPaso = objComponent.get("v.DatosCandidato"); 
        	DatosCandidatoPaso.TAM_NombreContactoServicio__c = null;
        	DatosCandidatoPaso.TAM_ApellidoPaternoContactoServ__c = null;
        	DatosCandidatoPaso.TAM_ApellidoMaternoContactoServ__c = null;
        	DatosCandidatoPaso.TAM_TelefonoContactoServicio__c = null;
        	DatosCandidatoPaso.TAM_TelefonoCelularContactoServicio__c = null;
        	DatosCandidatoPaso.TAM_CorreoElectronicoContactoServicio__c = null;
        	objComponent.set("v.DatosCandidato", DatosCandidatoPaso);
        	console.log('EN Save muestraDatosContacto blnMuestraCamposContacto: ', objComponent.get("v.DatosCandidato"));
        }
        console.log('EN Save muestraDatosContacto blnMuestraCamposContacto: ' + objComponent.get("v.blnMuestraCamposContacto"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    actualizaDatosClienteSel: function(objComponent, objEvent) {
    	console.log('EN actualizaDatosClienteSel...');
       	let DatosCandidatoInicial = objComponent.get("v.DatosCandidatoInicial");
       	console.log("EN actualizaDatosClienteSel DatosCandidatoInicial INI: " , DatosCandidatoInicial);
        let value = objEvent.getSource().get("v.value");
    	console.log('EN actualizaDatosClienteSel value: ' + value);
    	
    	let blnQuiter = objComponent.get("v.blnQuiter");
        let DatosCandidatoPaso = objComponent.get("v.DatosCandidato");       
    	//Ya tienes los datos ahora ecorre la lista de reg de lClientesDealer
    	let lClientesDealerPaso = objComponent.get("v.lClientesDealer"); 
    	//Recorre la lista de reg hasta que encentres el que se necesita
    	for (let noClientePaso in lClientesDealerPaso){
    		//Ve si ya encontraste al clente
    		if (lClientesDealerPaso[noClientePaso].noCliente == value){
    		
    			//Actualiza los datos del cliente 
    			DatosCandidatoPaso.FWY_ID_de_cliente__c = lClientesDealerPaso[noClientePaso].noCliente
    			DatosCandidatoPaso.TAM_NombreRL__c = lClientesDealerPaso[noClientePaso].nombreClienteSFDC;
    			DatosCandidatoPaso.TAM_ApellidoPaternoRL__c = lClientesDealerPaso[noClientePaso].apellidoPaterno;
    			DatosCandidatoPaso.TAM_ApellidoMaternoRL__c = lClientesDealerPaso[noClientePaso].apellidoMaterno;
    			DatosCandidatoPaso.TAM_RFCRL__c = lClientesDealerPaso[noClientePaso].rfc;
    			DatosCandidatoPaso.TAM_TelefonoRL__c = lClientesDealerPaso[noClientePaso].telefono;
    			DatosCandidatoPaso.TAM_TelefonoCelularRL__c = lClientesDealerPaso[noClientePaso].celular;
    			DatosCandidatoPaso.TAM_CorreoElectronicoRL__c = lClientesDealerPaso[noClientePaso].Email;
    				
    			//Ve si se trata de Quiter la intregracion para que consultes los catalogos correspondientes
    			if (blnQuiter){
    				
    				console.log('EN actualizaDatosClienteSel SE TRATA DE QUITER lClientesDealerPaso: ', lClientesDealerPaso[noClientePaso]);
    				//Manda llamar el sevvicio de la colonia
    				//this.BuscaColoniaFact(objComponent, objEvent, lClientesDealerPaso[noClientePaso].colony, lClientesDealerPaso[noClientePaso].colony, true);
    				//Manda llamar el sevvicio de la Del o Municipio
    				this.BuscaDelMunFact(objComponent, objEvent, lClientesDealerPaso[noClientePaso].delegation, lClientesDealerPaso[noClientePaso].delegation, true);
    				//Manda llamar el sevvicio de la ciudad
    				this.BuscaCiudadFact(objComponent, objEvent, lClientesDealerPaso[noClientePaso].city, lClientesDealerPaso[noClientePaso].city, true);
    				//Manda llamar el sevvicio del Estado
    				this.BuscaEstadoFact(objComponent, objEvent, lClientesDealerPaso[noClientePaso].state, lClientesDealerPaso[noClientePaso].state , true);

	    			//Ve si se trata de una Persona Moral	
	    			if (lClientesDealerPaso[noClientePaso].category == 'R')	
	    				DatosCandidatoPaso.TAM_TipoPersonaFact__c = 'Persona Moral';
	    			//Ve si se trara de una Persona Fisica
	    			if (lClientesDealerPaso[noClientePaso].category == 'P')	
	    				DatosCandidatoPaso.TAM_TipoPersonaFact__c = 'Persona Física';
    				
    				//Inicializa la direccion del cliente
   					DatosCandidatoPaso.TAM_Calle__c = lClientesDealerPaso[noClientePaso].fiscalAddress; //street; 
   					DatosCandidatoPaso.TAM_NoExt__c = lClientesDealerPaso[noClientePaso].fiscalAddressNumber; //streetNumber; 
   					DatosCandidatoPaso.TAM_NoInt__c = ''; 
   					DatosCandidatoPaso.TAM_CodigoPostal__c = lClientesDealerPaso[noClientePaso].postalCode; 
   					DatosCandidatoPaso.TAM_CorreoElectronicoRL__c = lClientesDealerPaso[noClientePaso].email; 
    				
    				//Limpia los campos del contacto que ya se tenian previamente capturados
   					DatosCandidatoPaso.TAM_RFCContFact__c = '';

   					DatosCandidatoPaso.TAM_CalleContFact__c = '';
   					DatosCandidatoPaso.TAM_NoExtContacto__c = '';
   					DatosCandidatoPaso.TAM_NoIntContacto__c = '';
   					DatosCandidatoPaso.TAM_CodigoPostalContFact__c = '';
   					DatosCandidatoPaso.TAM_ColoniaContFact__c = '';
   					DatosCandidatoPaso.TAM_DelegacionMunicipioContFact__c = '';
   					DatosCandidatoPaso.TAM_CiudadContFact__c = '';
   					DatosCandidatoPaso.TAM_EstadoContFact__c = '';
   					DatosCandidatoPaso.TAM_PaisContFact__c = '';
    				
    				//Inicializa la lista de contactos asociadoa al cliente
    				let lClienteContactosPaso = lClientesDealerPaso[noClientePaso].contacts;
    				console.log('EN actualizaDatosClienteSel SE TRATA DE QUITER lClienteContactosPaso: ', lClienteContactosPaso);
    			
    				//Ve si tienes contactos asociados en el objeto de lClientesDealerPaso[noClientePaso].contacts
    				for (let noContactoPaso in lClienteContactosPaso){
    					let NombreContacto = lClienteContactosPaso[noContactoPaso].name;
    					let arrNombreCont = NombreContacto.split(" ");
    					console.log('EN actualizaDatosClienteSel SE TRATA DE QUITER NombreContacto: ', NombreContacto);
    					console.log('EN actualizaDatosClienteSel SE TRATA DE QUITER arrNombreCont: ', arrNombreCont);
    					//Inicializa los campos del contacto asociado
    					DatosCandidatoPaso.TAM_NombreContFact__c = arrNombreCont.length > 0 ? arrNombreCont[0] : '';
    					DatosCandidatoPaso.TAM_ApellidoPaternoContFact__c = arrNombreCont.length > 1 ? arrNombreCont[1] : '';
    					DatosCandidatoPaso.TAM_ApellidoMaternoContFact__c = arrNombreCont.length > 2 ? arrNombreCont[2] : '';
    					DatosCandidatoPaso.TAM_TelefonoContFact__c = lClienteContactosPaso[noContactoPaso].telephone;
    					DatosCandidatoPaso.TAM_TelefonoCelularContFact__c = lClienteContactosPaso[noContactoPaso].telephoneTime;
    					DatosCandidatoPaso.TAM_CorreoElectronicoContFact__c = lClienteContactosPaso[noContactoPaso].email;
    				}//Fin del for para lClientesDealerPaso[noClientePaso].contacts
    				
    				//Desactiva los campos ya que se trata de quiter
    				objComponent.set("v.blnDesacCampQuiter", true);
	            	//Prende la bandera que permite actualizar los datosl del cliente
    				objComponent.set("v.blnUpdDatosCliente", false);
    				
    			}//Fin si es QUITER
    			
    			//Ve si se trata de Quiter la intregracion para que consultes los catalogos correspondientes
    			if (!blnQuiter){
	    			DatosCandidatoPaso.TAM_Calle__c = lClientesDealerPaso[noClientePaso].direccion;
	    			DatosCandidatoPaso.TAM_CodigoPostal__c = lClientesDealerPaso[noClientePaso].cp;
	    			DatosCandidatoPaso.TAM_Colonia__c = lClientesDealerPaso[noClientePaso].colonia;
	    			DatosCandidatoPaso.TAM_DelegacionMunicipio__c = lClientesDealerPaso[noClientePaso].delegacionMunicipio;
	    			DatosCandidatoPaso.TAM_Ciudad__c = lClientesDealerPaso[noClientePaso].localidad;
	    			DatosCandidatoPaso.TAM_Estado__c = lClientesDealerPaso[noClientePaso].estado;
	    			DatosCandidatoPaso.TAM_Pais__c = lClientesDealerPaso[noClientePaso].pais;
    			}//Fin si no es QUITER

    			console.log('EN actualizaDatosClienteSel DatosCandidatoPaso: ', DatosCandidatoPaso);
    			objComponent.set("v.DatosCandidato", DatosCandidatoPaso);
    			console.log('EN actualizaDatosClienteSel DatosCandidato: ', objComponent.get("v.DatosCandidato"));
            	console.log("EN actualizaDatosClienteSel DatosCandidatoInicial FIN: " , objComponent.get("v.DatosCandidatoInicial"));    			
    			
    			//Salte
    			break;
    			
    		}//Fin si lClientesDealerPaso[noClientePaso].noCliente == value
    	}//Fin del for para lClientesDealerPaso
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	ocBuscaColoniaFact : function(objComponent, objEvent) {
		console.log("EN ocBuscaColoniaFact...");				                

        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidato = objComponent.get("v.DatosCandidato");
        let blnQuiterFinal = objComponent.get("v.blnQuiter");
  		console.log("EN ocBuscaColoniaFact DatosCandidato: ", DatosCandidato);	
  		console.log("EN ocBuscaColoniaFact DatosCandidato.FWY_ID_de_cliente__c: ", DatosCandidato.FWY_ID_de_cliente__c);	
       
        //Ve si se trata de Quiter 
        if (blnQuiterFinal && DatosCandidato.FWY_ID_de_cliente__c == null)
        	this.BuscaColoniaFact(objComponent, objEvent, null, null, false);
       
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	IniListaColoniaFact : function(objComponent, objEvent, strClaveColonia, strNombreColonia) {
		console.log("EN IniListaColoniaFact...");				                

  		console.log("EN IniListaColoniaFact strClaveColonia: ", strClaveColonia);	
  		console.log("EN IniListaColoniaFact strNombreColonia: ", strNombreColonia);	
       
        //Llama la función 
		let objAction = objComponent.get("c.getLstColoniasQuiter");
        objAction.setParams({
        	"strClaveColonia" : strClaveColonia,
        	"strNombreColonia" : strNombreColonia 
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lColoniasQuiterFactResult = objResponse.getReturnValue();
            	let sColoniasQuiterFact = strNombreColonia;           	
            	objComponent.set("v.lColoniasQuiterFact", lColoniasQuiterFactResult);
           		console.log("EN IniListaColoniaFact lColoniasQuiterFactResult: ", lColoniasQuiterFactResult );            	
           		console.log("EN IniListaColoniaFact sColoniasQuiterFact: ", objComponent.get("v.sColoniasQuiterFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	BuscaColoniaFact : function(objComponent, objEvent, strClaveColonia, strNombreColonia, blnQuiter) {
		console.log("EN BuscaColoniaFact...");				                
   		console.log("EN BuscaColoniaFact strClaveColonia0: " + strClaveColonia );	
  		console.log("EN BuscaColoniaFact strNombreColonia0: " + strNombreColonia );	
  		console.log("EN BuscaColoniaFact blnQuiter0: " + blnQuiter );	

        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidato = objComponent.get("v.DatosCandidato");
        let blnQuiterFinal = objComponent.get("v.blnQuiter");

        //Llama la función 
		let objAction = objComponent.get("c.getColoniaQuiter");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : DatosCandidato,
        	"strClaveColonia" : strClaveColonia,
        	"blnQuiter" : blnQuiterFinal 
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lColoniasQuiterFactResult = objResponse.getReturnValue();
            	let sColoniasQuiterFact = strNombreColonia;           	
            	objComponent.set("v.lColoniasQuiterFact", lColoniasQuiterFactResult);
           		console.log("EN BuscaColoniaFact lColoniasQuiterFactResult: ", lColoniasQuiterFactResult );            	
            	if (sColoniasQuiterFact === undefined){
            		if (lColoniasQuiterFactResult.length > 0){
            			objComponent.set("v.sColoniasQuiterFact", lColoniasQuiterFactResult[0].strCodigo);
            			if (blnQuiterFinal){
            				DatosCandidato.TAM_Colonia__c = lColoniasQuiterFactResult[0].strDescripcion;
            				DatosCandidato.TAM_IdColoniaQuiter__c = lColoniasQuiterFactResult[0].strCodigo;
            			}//Fin si blnQuiterFinal
            		}//Fin si lColoniasQuiterFactResult.length > 0
            	}
            	if (sColoniasQuiterFact !== undefined){
            		objComponent.set("v.sColoniasQuiterFact", sColoniasQuiterFact);
         			if (blnQuiterFinal){
         				objComponent.set("v.sColoniasQuiterFact", lColoniasQuiterFactResult[0].strCodigo);
           				DatosCandidato.TAM_IdColoniaQuiter__c = lColoniasQuiterFactResult[0].strCodigo;
           				DatosCandidato.TAM_Colonia__c = lColoniasQuiterFactResult[0].strDescripcion;
           			}//Fin si blnQuiterFinal
            	}//Fin si sColoniasQuiterFact !== undefined
            	objComponent.set("v.DatosCandidato", DatosCandidato);
           		console.log("EN BuscaColoniaFact sColoniasQuiterFact: ", objComponent.get("v.sColoniasQuiterFact") );
           		console.log("EN BuscaColoniaFact DatosCandidato: ", objComponent.get("v.DatosCandidato") );            
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	IniListaDelMunFact : function(objComponent, objEvent, strClaveDelMun, strNombreDelMun) {
		console.log("EN IniListaDelMunFact...");				                

  		console.log("EN IniListaDelMunFact strClaveDelMun: ", strClaveDelMun);	
  		console.log("EN IniListaDelMunFact strNombreDelMun: ", strNombreDelMun);	
              
        //Llama la función 
		let objAction = objComponent.get("c.getLstDelMunQuiter");
        objAction.setParams({
        	"strClaveDelMun" : strClaveDelMun,
        	"strNombreDelMun" : strNombreDelMun 
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lDelegationQuiterFactResult = objResponse.getReturnValue();
            	let sDelegationQuiterFact = strNombreDelMun;
            	objComponent.set("v.lDelegationQuiterFact", lDelegationQuiterFactResult);
            	objComponent.set("v.sDelegationQuiterFact", sDelegationQuiterFact);
           		console.log("EN IniListaDelMunFact lDelegationQuiterFact: ", lDelegationQuiterFactResult );            	
           		console.log("EN IniListaDelMunFact sDelegationQuiterFact: ", objComponent.get("v.sDelegationQuiterFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	BuscaDelMunFact : function(objComponent, objEvent, strClaveDelMun, strNombreDelMun, blnQuiter) {
		console.log("EN BuscaDelMunFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidato = objComponent.get("v.DatosCandidato");
        let blnQuiterFinal = objComponent.get("v.blnQuiter");
        
        let value = objEvent.getSource().get("v.value");
        let sColoniaQuiterFact = strClaveDelMun === undefined ? value : strClaveDelMun;        
        objComponent.set("v.sColoniasQuiterFact",value);
        let lColoniasQuiterFactPaso = objComponent.get("v.lColoniasQuiterFact");
        //Recorre la lista de colonias y selecciona la que le corresponde de acuerdo a sColoniaQuiterFact
    	for (let noColPaso in lColoniasQuiterFactPaso){
    		if (lColoniasQuiterFactPaso[noColPaso].strCodigo == value){
    			DatosCandidato.TAM_IdColoniaQuiter__c = value;
    			DatosCandidato.TAM_Colonia__c = lColoniasQuiterFactPaso[noColPaso].strDescripcion;
    			break;
    		}//Fin si lColoniasQuiterFactPaso[noColPaso].strCodigo == value
    	}//Fin del for para lColoniasQuiterFactPaso
    	
    	//Ya esta registrado en QUITER
    	if (blnQuiter){
    		DatosCandidato.TAM_IdDelMunQuiter__c = strClaveDelMun;
    		DatosCandidato.TAM_DelegacionMunicipio__c = strNombreDelMun;
    	}//Fin si blnQuiter
    	
		console.log("EN BuscaDelMunFact sColoniaQuiterFact123: " + sColoniaQuiterFact);				                
		console.log("EN BuscaDelMunFact DatosCandidato.TAM_Colonia__c: " + DatosCandidato.TAM_Colonia__c);				                
		console.log("EN BuscaDelMunFact DatosCandidato.TAM_IdColoniaQuiter__c: " + DatosCandidato.TAM_IdColoniaQuiter__c);				                
                
        //Llama la función 
		let objAction = objComponent.get("c.getDelMunQuiter");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : DatosCandidato,
        	"strClaveColonia" : sColoniaQuiterFact,
        	"strNombreDelMun" : strNombreDelMun,
        	"blnQuiter" : blnQuiterFinal
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lDelegationQuiterFactResult = objResponse.getReturnValue();
            	let sDelegationQuiterFact = strNombreDelMun;
            	objComponent.set("v.lDelegationQuiterFact", lDelegationQuiterFactResult);
            	if (sDelegationQuiterFact === undefined){
            		if (lDelegationQuiterFactResult.length > 0){
            			objComponent.set("v.sDelegationQuiterFact", lDelegationQuiterFactResult[0].strCodigo);
            			if (blnQuiterFinal){
            				DatosCandidato.TAM_IdDelMunQuiter__c = lDelegationQuiterFactResult[0].strCodigo;
            				DatosCandidato.TAM_DelegacionMunicipio__c = lDelegationQuiterFactResult[0].strDescripcion;
            			}//Fin si blnQuiterFinal            		
            		}//Fin si lDelegationQuiterFactResult.length > 0
            	}//Fin si sDelegationQuiterFact === undefined
            	if (sDelegationQuiterFact !== undefined){
            		objComponent.set("v.sDelegationQuiterFact", sDelegationQuiterFact);
            		if (blnQuiterFinal){
            			DatosCandidato.TAM_IdDelMunQuiter__c = lDelegationQuiterFactResult[0].strCodigo;
            			DatosCandidato.TAM_DelegacionMunicipio__c = lDelegationQuiterFactResult[0].strDescripcion;
            		}//Fin si blnQuiterFinal
            	}//Fin si sDelegationQuiterFact !== undefined
            	objComponent.set("v.DatosCandidato", DatosCandidato);            		
           		console.log("EN BuscaDelMunFact sDelegationQuiterFact: ", objComponent.get("v.sDelegationQuiterFact") );
           		console.log("EN BuscaDelMunFact DatosCandidato: ", objComponent.get("v.DatosCandidato") );           		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	IniListaCiudadFact: function(objComponent, objEvent, strClaveCiudadFact, strNombreCiudadFact) {
		console.log("EN IniListaDelMunFact...");				                

  		console.log("EN IniListaDelMunFact strClaveCiudadFact: ", strClaveCiudadFact);	
  		console.log("EN IniListaDelMunFact strNombreCiudadFact: ", strNombreCiudadFact);	
              
        //Llama la función 
		let objAction = objComponent.get("c.getLstCiudadQuiter");
        objAction.setParams({
        	"strClaveCiudadFact" : strClaveCiudadFact,
        	"strNombreCiudadFact" : strNombreCiudadFact 
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lCiudadQuiterFactResult = objResponse.getReturnValue();
            	let sCiudadQuiterFact = strNombreCiudadFact;
            	objComponent.set("v.lCiudadQuiterFact", lCiudadQuiterFactResult);
            	objComponent.set("v.sCiudadQuiterFact", strNombreCiudadFact);
           		console.log("EN IniListaDelMunFact lCiudadQuiterFact: ", lCiudadQuiterFactResult );            	
           		console.log("EN IniListaDelMunFact sCiudadQuiterFact: ", objComponent.get("v.sCiudadQuiterFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	BuscaCiudadFact : function(objComponent, objEvent, strClaveDelMun, strNombreDelMun, blnQuiter) {
		console.log("EN BuscaCiudadFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidato = objComponent.get("v.DatosCandidato");
        let blnQuiterFinal = objComponent.get("v.blnQuiter");
        
        let value = objEvent.getSource().get("v.value");
        let sDelegationQuiterFact = value;        
        objComponent.set("v.sDelegationQuiterFact",value);
        
        //Inicializa la lista de lDelegationQuiterFact para obtener el reg que se selecciono
        let lDelegationQuiterFactPaso = objComponent.get("v.lDelegationQuiterFact");
        //Recorre la lista de colonias y selecciona la que le corresponde de acuerdo a sColoniaQuiterFact
    	for (let noDelMunPaso in lDelegationQuiterFactPaso){
    		if (lDelegationQuiterFactPaso[noDelMunPaso].strCodigo == value){
    			DatosCandidato.TAM_DelegacionMunicipio__c = lDelegationQuiterFactPaso[noDelMunPaso].strDescripcion;
    			DatosCandidato.TAM_IdDelMunQuiter__c = value;
    			break;
    		}//Fin si lColoniasQuiterFactPaso[noColPaso].strCodigo == value
    	}//Fin del for para lColoniasQuiterFactPaso        

		console.log("EN BuscaCiudadFact sDelegationQuiterFact: " + sDelegationQuiterFact);				                
		console.log("EN BuscaCiudadFact DatosCandidato.TAM_DelegacionMunicipio__c: " + DatosCandidato.TAM_DelegacionMunicipio__c);				                
		console.log("EN BuscaCiudadFact DatosCandidato.TAM_IdDelMunQuiter__c: " + DatosCandidato.TAM_IdDelMunQuiter__c);				                
                
        //Llama la función 
		let objAction = objComponent.get("c.getCiudadQuiter");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : DatosCandidato,
        	"strClaveDelMun" : sDelegationQuiterFact,
        	"strNombreCiudad" : strNombreDelMun,        	
        	"blnQuiter" : blnQuiter
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lCiudadQuiterFactResult = objResponse.getReturnValue();
            	let sCiudadQuiterFact = strNombreDelMun;
            	objComponent.set("v.lCiudadQuiterFact", lCiudadQuiterFactResult);
           		console.log("EN BuscaCiudadFact sCiudadQuiterFact: ", sCiudadQuiterFact);            	
            	if (sCiudadQuiterFact === undefined){
            		if (lCiudadQuiterFactResult.length > 0){
            			objComponent.set("v.sCiudadQuiterFact", lCiudadQuiterFactResult[0].strCodigo);
            			if (blnQuiterFinal){
            				DatosCandidato.TAM_IdCiudadQuiter__c = lCiudadQuiterFactResult[0].strCodigo;
            				DatosCandidato.TAM_Ciudad__c = lCiudadQuiterFactResult[0].strDescripcion;
            			}//Fin si blnQuiterFinal
            		}//Fin si lCiudadQuiterFactResult.length > 0)
            	}
            	if (sCiudadQuiterFact !== undefined){
            		objComponent.set("v.sCiudadQuiterFact", sCiudadQuiterFact);
	       			if (blnQuiterFinal){
	       				DatosCandidato.TAM_IdCiudadQuiter__c = lCiudadQuiterFactResult[0].strCodigo;
	       				DatosCandidato.TAM_Ciudad__c = lCiudadQuiterFactResult[0].strDescripcion;
	       			}//Fin si blnQuiterFinal            	
            	}//Fin si sCiudadQuiterFact !== undefined
            	objComponent.set("v.DatosCandidato", DatosCandidato);
           		console.log("EN BuscaCiudadFact sCiudadQuiterFact: ", objComponent.get("v.sCiudadQuiterFact") );
           		console.log("EN BuscaCiudadFact DatosCandidato: ", objComponent.get("v.DatosCandidato") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	IniListaEstadoFact: function(objComponent, objEvent, strClaveEstadoFact, strNombreEstadoFact) {
		console.log("EN IniListaEstadoFact...");				                

  		console.log("EN IniListaEstadoFact strClaveEstadoFact: ", strClaveEstadoFact);	
  		console.log("EN IniListaEstadoFact strNombreEstadoFact: ", strNombreEstadoFact);	
              
        //Llama la función 
		let objAction = objComponent.get("c.getLstEstadoQuiter");
        objAction.setParams({
        	"strClaveEstadoFact" : strClaveEstadoFact,
        	"strNombreEstadoFact" : strNombreEstadoFact 
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lEstadoQuiterFactResult = objResponse.getReturnValue();
            	let sEstadoQuiterFact = strNombreEstadoFact;
            	objComponent.set("v.lEstadoQuiterFact", lEstadoQuiterFactResult);
            	objComponent.set("v.sEstadoQuiterFact", strNombreEstadoFact);
           		console.log("EN IniListaEstadoFact lEstadoQuiterFact: ", lEstadoQuiterFactResult );            	
           		console.log("EN IniListaEstadoFact sEstadoQuiterFact: ", objComponent.get("v.sEstadoQuiterFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	BuscaEstadoFact : function(objComponent, objEvent, strClaveCiudad, strNombreEstado, blnQuiter) {
		console.log("EN BuscaEstadoFact...");				                
   		console.log("EN BuscaEstadoFact strClaveCiudad0: " + strClaveCiudad );	
  		console.log("EN BuscaEstadoFact strNombreEstado0: " + strNombreEstado );	
  		console.log("EN BuscaEstadoFact blnQuiter0: " + blnQuiter );	

        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidatoPaso = objComponent.get("v.DatosCandidato");
        let blnQuiterFinal = objComponent.get("v.blnQuiter");
        
        let value = objEvent.getSource().get("v.value");
        let sCiudadQuiterFact = value;
        objComponent.set("v.sCiudadQuiterFact",value);  
        
        //Inicializa la lista de lCiudadQuiterFact para obtener el reg que se selecciono
        let lCiudadQuiterFactPaso = objComponent.get("v.lCiudadQuiterFact");
        //Recorre la lista de colonias y selecciona la que le corresponde de acuerdo a sColoniaQuiterFact
    	for (let noCiudadPaso in lCiudadQuiterFactPaso){
    		if (lCiudadQuiterFactPaso[noCiudadPaso].strCodigo == value){
    			DatosCandidatoPaso.TAM_Ciudad__c = lCiudadQuiterFactPaso[noCiudadPaso].strDescripcion;
    			DatosCandidatoPaso.TAM_IdCiudadQuiter__c = value;
    			break;
    		}//Fin si lColoniasQuiterFactPaso[noColPaso].strCodigo == value
    	}//Fin del for para lColoniasQuiterFactPaso
		console.log("EN BuscaEstadoFact sCiudadQuiterFact: " + sCiudadQuiterFact);				                
		console.log("EN BuscaEstadoFact DatosCandidato.TAM_Ciudad__c: " + DatosCandidatoPaso.TAM_Ciudad__c);				                        
		console.log("EN BuscaEstadoFact DatosCandidato.TAM_IdCiudadQuiter__c: " + DatosCandidatoPaso.TAM_IdCiudadQuiter__c);				                        
                
        //Llama la función 
		let objAction = objComponent.get("c.getEstadoQuiter");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : DatosCandidatoPaso,
        	"strClaveCiudad" : sCiudadQuiterFact,
        	"strNombreEstado" : strNombreEstado,
        	"blnQuiterFinal" : blnQuiterFinal
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lEstadoQuiterFactResult = objResponse.getReturnValue();
            	let sEstadoQuiterFact = strNombreEstado;
           		console.log("EN BuscaEstadoFact sEstadoQuiterFact: " + sEstadoQuiterFact );	
           		console.log("EN BuscaEstadoFact blnQuiterFinal: " + blnQuiterFinal );	
            	objComponent.set("v.lEstadoQuiterFact", lEstadoQuiterFactResult);
            	if (sEstadoQuiterFact === undefined){
            		if (lEstadoQuiterFactResult.length > 0){
            			objComponent.set("v.sEstadoQuiterFact", lEstadoQuiterFactResult[0].strCodigo);
            			if (blnQuiterFinal){
            				DatosCandidatoPaso.TAM_IdEstadoQuiter__c = lEstadoQuiterFactResult[0].strCodigo;
            				DatosCandidatoPaso.TAM_Estado__c = lEstadoQuiterFactResult[0].strDescripcion;
            			}//Fin si blnQuiterFinal
            		}//Fin si lEstadoQuiterFactResult.length > 0
            	}//Fin si sEstadoQuiterFact === undefined
            	if (sEstadoQuiterFact !== undefined){
            		objComponent.set("v.sEstadoQuiterFact", sEstadoQuiterFact);
           			if (blnQuiterFinal){
           				DatosCandidatoPaso.TAM_IdEstadoQuiter__c = lEstadoQuiterFactResult[0].strCodigo;
           				DatosCandidatoPaso.TAM_Estado__c = lEstadoQuiterFactResult[0].strDescripcion;
           			}//Fin si blnQuiterFinal            	
            	}//Fin si sEstadoQuiterFact !== undefined
            	objComponent.set("v.DatosCandidato", DatosCandidatoPaso);
           		console.log("EN BuscaEstadoFact DatosCandidato: ", objComponent.get("v.DatosCandidato") );
           		console.log("EN BuscaEstadoFact sEstadoQuiterFact: ", objComponent.get("v.sEstadoQuiterFact") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	actualizarDatos : function(objComponent, objEvent) {
		console.log("EN Helper.actualizarDatos...");
		let faltanCampos = false; 		                
        let strIdCandidato = objComponent.get("v.recordId");
        let DatosCandidatoFinal = objComponent.get("v.DatosCandidato");
		console.log("EN Helper.actualizarDatos DatosCandidatoFinal: ", DatosCandidatoFinal);
        
        //Llama la función 
		let objAction = objComponent.get("c.actualizarDatos");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : DatosCandidatoFinal,
        	"vinesSeleProspFact" : objComponent.get("v.lVinesSeleProspFact"),        	
        	"sTipoOfertaFact" : objComponent.get("v.sTipoOfertaFact"),
        	"sAsesorVentasFact" : objComponent.get("v.sAsesorVentasFact"),
        	"sOrigemFact" : objComponent.get("v.sOrigemFact"),
        	"sAgenciaFact" : objComponent.get("v.sAgenciaFact"),
        	"sEdoPedInicialFact" : objComponent.get("v.sEdoPedInicialFact"),
        	"sTipoVentaFact" : objComponent.get("v.sTipoVentaFact"),
        	"sTipoVentaDestinoFact" : objComponent.get("v.sTipoVentaDestinoFact"),
        	"sAsesorVentasFactAuto" : objComponent.get("v.sAsesorVentasFactAuto"),
        	"DatosDistribuidor" : objComponent.get("v.DatosDistribuidor")        	        	
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResAct = objResponse.getReturnValue();
            	console.log("EN Helper.actualizarDatos objWrpResAct: ", objWrpResAct);
            	if (objWrpResAct.blnEstatus == false)
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
        
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	confirmaDatosFactura : function(objComponent, objEvent) {
		console.log("EN Helper.confirmaDatosFactura...");
		
		let faltanCampos = false;
		let blnQuiter = objComponent.get("v.blnQuiter");
		let blnMoralFisicaActEmp = objComponent.get("v.blnMoralFisicaActEmp");
		console.log("EN Helper.confirmaDatosFactura blnQuiter: ", blnQuiter);
		console.log("EN Helper.confirmaDatosFactura blnMoralFisicaActEmp: ", blnMoralFisicaActEmp);
		
		let camposValidosBase = objComponent.find('validaCampo').reduce(
			function (validSoFar, inputCmp) {
				inputCmp.showHelpMessageIfInvalid(); //set("v.errors", [{message:"Debes capturar un valor." + value}]);
				return validSoFar && inputCmp.get('v.validity').valid;
			}
		, true);
		
		//Si se trata de una persona fisica
		if (!blnMoralFisicaActEmp){
			let camposValidosPersonFisica = objComponent.find('validaCampoFisica').reduce(
				function (validSoFar, inputCmp) {
					inputCmp.showHelpMessageIfInvalid(); 
					return validSoFar && inputCmp.get('v.validity').valid;
				}
			, true);
			console.log("EN Helper.confirmaDatosFactura camposValidosPersonFisica: ", camposValidosPersonFisica);
		}//Fin si blnMoralFisicaActEmp

		/*
		//Si se trata de una persona moral o fisica con actividad empresarial
		if (blnMoralFisicaActEmp){
			let camposValidosPersonMoral = objComponent.find('validaCampoContac').reduce(
				function (validSoFar, inputCmp) {
					inputCmp.showHelpMessageIfInvalid(); 
					return validSoFar && inputCmp.get('v.validity').valid;
				}
			, true);
			console.log("EN Helper.confirmaDatosFactura camposValidosPersonMoral: ", camposValidosPersonMoral);			
		}//Fin si blnMoralFisicaActEmp
		*/
        
        //Ve si hubo un error a la hora de validar 
        if (camposValidosBase) {
        
	        /*
			//Valida el campo de la Ciudad
        	let cmpCiudadId = objComponent.find("ciudadid");
        	let strCiudadId = cmpCiudadId.get("v.value");
        	//alert('EN Helper.confirmaDatosFactura strCiudadId: ' + strCiudadId);
        	// is input valid text?
        	if (strCiudadId === "" || strCiudadId == null){
           		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en el campo de: Ciudad'); 
        		faltanCampos = true;
        	}

        	//Valida el campo de la Estado
        	let cmpEstadoid = objComponent.find("estadoid");
        	let strEstadoid = cmpEstadoid.get("v.value");
        	//alert('EN Helper.confirmaDatosFactura strEstadoid: ' + strEstadoid);
        	// is input valid text?
        	if (strEstadoid === "" || strEstadoid == null){
           		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en el campo de: Estado'); 
        		faltanCampos = true;
        	}
        	*/
        	
        	/*//Valida el campo de la Pais
        	let cmpPaisid = objComponent.find("paisid");
        	let strPaisid = cmpPaisid.get("v.value");
        	alert('EN Helper.confirmaDatosFactura strPaisid: ' + strPaisid);
        	// is input valid text?
        	if (strPaisid === "" || strPaisid == null){
           		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en el campo de: Pais');
        		faltanCampos = true;
        	}*/
        	
        } else{
       		this.showToastError(objComponent, objEvent, 'Debes capturar un valor en los campos que faltan');
       		faltanCampos = true;
        } 

        //Si no hubo error guarda los datos
        if (!faltanCampos){
	        let strIdCandidato = objComponent.get("v.recordId");
	        //Llama la función 
			let objAction = objComponent.get("c.verificaEstadoInventario");
	        objAction.setParams({
	        	"recordId" : strIdCandidato,
	        	"datosCandidato" : objComponent.get("v.DatosCandidato")
	   		});		
	        objAction.setCallback(this, function(objResponse){
	            let objState = objResponse.getState();
	            if (objState === "SUCCESS") {
	            	let objWrpResAct = objResponse.getReturnValue();
	            	if (objWrpResAct.blnEstatus == false){
	            		this.saveDatosFactura(objComponent, objEvent);
	            		//this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
	            	}
	            	if (objWrpResAct.blnEstatus == true)
	            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);            
	            }//Fin si objState === "SUCCESS"
	        });
	        $A.enqueueAction(objAction);        
        }//Fin si faltanCampos
         
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	saveDatosFactura : function(objComponent, objEvent) {
		console.log("EN Helper.saveDatosFactura...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.confirmaDatosCandidato");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objComponent.get("v.DatosCandidato"),
        	"vinesSeleProspFact" : objComponent.get("v.lVinesSeleProspFact"),
        	"sTipoOfertaFact" : objComponent.get("v.sTipoOfertaFact"),
        	"sAsesorVentasFact" : objComponent.get("v.sAsesorVentasFact"),
        	"sOrigemFact" : objComponent.get("v.sOrigemFact"),
        	"sAgenciaFact" : objComponent.get("v.sAgenciaFact"),
        	"sEdoPedInicialFact" : objComponent.get("v.sEdoPedInicialFact"),
        	"sTipoVentaFact" : objComponent.get("v.sTipoVentaFact"),
        	"sTipoVentaDestinoFact" : objComponent.get("v.sTipoVentaDestinoFact"),
        	"blnMoralFisicaActEmp" : objComponent.get("v.blnMoralFisicaActEmp"),
        	"blnQuiter" : objComponent.get("v.blnQuiter"), 
        	"blnBussinesPro" : objComponent.get("v.blnBussinesPro"),        	
        	"sAsesorVentasFactAuto" : objComponent.get("v.sAsesorVentasFactAuto"),
        	"DatosDistribuidor" : objComponent.get("v.DatosDistribuidor"),
        	"lColoniasQuiterFact" : objComponent.get("v.lColoniasQuiterFact"),
        	"lDelegationQuiterFact" : objComponent.get("v.lDelegationQuiterFact"),
        	"lCiudadQuiterFact" : objComponent.get("v.lCiudadQuiterFact"),
        	"lEstadoQuiterFact" : objComponent.get("v.lEstadoQuiterFact"),
        	"blnUpdDatosCliente" : objComponent.get("v.blnUpdDatosCliente")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResAct = objResponse.getReturnValue();
            	if (objWrpResAct.blnEstatus == false){
            		objComponent.set("v.blnConfirmaDatosFact",true);
            		this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
            	}
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);            
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	loginClienteDealer : function(objComponent, objEvent) {
		console.log("EN loginClienteDealer...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        let strNoClientePrueba = objComponent.get("v.strNoClientePrueba");
        //Llama la función 
		let objAction = objComponent.get("c.loginClienteDealer");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objComponent.get("v.DatosCandidato")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objRespCallWebService = objResponse.getReturnValue();
            	if (objRespCallWebService.Error == false){
            		console.log("EN loginClienteDealer SIN ERROR objRespCallWebService: "+ JSON.stringify(objRespCallWebService));				                
            		//Manda llamar el metodo de buscaClienteDedaler con el parametro del TKN
            		this.buscaClienteDedaler(objComponent, objEvent, objRespCallWebService.loginResponse.Token, objRespCallWebService.loginResponse.access_token, strNoClientePrueba);
            	}//Fin si objRespCallWebService.Error == false
            	if (objRespCallWebService.Error == true){
            		console.log("EN loginClienteDealer CON ERROR objRespCallWebService: "+ JSON.stringify(objRespCallWebService));
            		this.showToastError(objComponent, objEvent, objRespCallWebService.Detalle);            						                
            	}//Fin si objRespCallWebService.Error == false
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	buscaClienteDedaler : function(objComponent, objEvent, strToken, strAccessToken, strNoClientePrueba) {
		console.log("EN TipoVentaDestinoFact...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getClientesDealer");
        objAction.setParams({
        	"recordId" : strIdCandidato,
        	"datosCandidato" : objComponent.get("v.DatosCandidato"),
        	"strToken" : strToken,
        	"strAccessToken" : strAccessToken,
        	"DatosDistribuidor" : objComponent.get("v.DatosDistribuidor"),
        	"strNoCliente" : strNoClientePrueba        	
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lClientesDealerResult = objResponse.getReturnValue();
            	
            	//Toma el primer reg de la lista y ver si tiene error
            	if (lClientesDealerResult[0].blnError){
            		objComponent.set("v.blnErrorConsultaCtes", true);
            		objComponent.set("v.sDetalleErrorConsCte", lClientesDealerResult[0].bDetalleError);            		
            		objComponent.set("v.lClientesDealer", lClientesDealerResult);
            	}//Fin si lClientesDealerResult[0].blnError
            	if (!lClientesDealerResult[0].blnError){
            		objComponent.set("v.blnErrorConsultaCtes", false);
            		objComponent.set("v.sDetalleErrorConsCte", '');
            		objComponent.set("v.lClientesDealer", lClientesDealerResult);            		
            	}//Fin si lClientesDealerResult[0].blnError            	
           		console.log("EN VinesSeleccionadosProspectoFact lClientesDealer: ", objComponent.get("v.lClientesDealer") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	restauraDatosCliente : function(objComponent, objEvent) {
		console.log("EN restauraDatosCliente...");
		console.log("EN restauraDatosCliente DatosCandidato: ", objComponent.get("v.DatosCandidato"));

        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getRetauraDatosCandidato");
        objAction.setParams({   			
        	recordId : strIdCandidato
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objDatosCandidato = objResponse.getReturnValue();
            	objComponent.set("v.DatosCandidato", objDatosCandidato);
            	console.log("EN restauraDatosCliente DatosCandidato Act: ", objComponent.get("v.DatosCandidato"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
      	
    },


    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
      	console.log("EN showToastSuccess strMensaje: ", strMensaje);
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 1000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
      	console.log("EN showToastError strMensaje: ", strMensaje);    
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 2000,
            "type": "error"
        });
        toastEvent.fire();
    },
})