({
    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecAsesorVetaFactAuto : function(objComponent, objEvent, objHelper) {
        objHelper.SelecAsesorVetaFactAuto(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecTipoOfertaFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecTipoOfertaFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecAsesorVentasFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecAsesorVentasFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecOrigenFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecOrigenFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecAgenciaFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecAgenciaFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecEdoPedInicialFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecEdoPedInicialFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecTipoVentaFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecTipoVentaFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecTipoVentaDestinoFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecTipoVentaDestinoFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    SelecMonedaVentaFact : function(objComponent, objEvent, objHelper) {
        objHelper.SelecMonedaVentaFact(objComponent, objEvent);
    },

   /** Funcion Inicial */
	onUsaDatosCliente : function(objComponent, objEvent, objHelper) {
		objHelper.usarDatosCteSol(objComponent, objEvent);
    },

   /** Funcion Inicial */
	onMuestraDatosContacto : function(objComponent, objEvent, objHelper) {
		objHelper.muestraDatosContacto(objComponent, objEvent);
    },

   /** Funcion Inicial */
	actualizarDatosLead : function(objComponent, objEvent, objHelper) {
		objHelper.actualizarDatos(objComponent, objEvent);
    },

   /** Funcion Inicial */
	confirmaDatosFactura : function(objComponent, objEvent, objHelper) {
		console.log("EN Ctlr.confirmaDatosFactura...");	
		//Verifica el campo  de validaCampoFechaNac
       	let DatosDistribuidor = objComponent.get("v.DatosDistribuidor");	    
		let blnCampoFechaNac = false;
		//Ve si se trata de BussinesPro para que validemos el campo de Fecha validaCampoFechaNac
       	if (DatosDistribuidor.TAM_TipoIntegreacion__c == 'BussinesPro'){
	        let validaCampoFechaNacCmp = objComponent.find("validaCampoFechaNac");
	        let fechaNacVal = objComponent.get('v.value');
	        if(fechaNacVal == null) {
	            validaCampoFechaNacCmp.setCustomValidity('Debes capturar un valor.');
	            blnCampoFechaNac = true;
	        }//Fin si fechaNacVal == null
			validaCampoFechaNacCmp.reportValidity();
		}
		objHelper.confirmaDatosFactura(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    jsHandleBlur : function(objComponent, objEvent, objHelper) {
    	let fechaNacCmp = objComponent.find("validaCampoFechaNac");
        fechaNacCmp.setCustomValidity('');
        let chckvalididty = fechaNacCmp.get("v.validity");
        if(!chckvalididty.valid)
            fechaNacCmp.setCustomValidity('El formato de la fecha debe ser: dd/MM/yyyy');
        else
            fechaNacCmp.setCustomValidity('');
        fechaNacCmp.reportValidity();    
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

	//La función SeleccionaMesFechaCierre
    validaTipoPersonaFact : function(objComponent, objEvent, objHelper) {
        objHelper.validaTipoPersonaFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    buscaClienteDedaler : function(objComponent, objEvent, objHelper) {
        objHelper.buscaClienteDedaler(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    autenticaClienteDealer : function(objComponent, objEvent, objHelper) {
        objHelper.loginClienteDealer(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    ocSelecColFact : function(objComponent, objEvent, objHelper) {
        objHelper.ocBuscaColoniaFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    ocSelecDelMunFact : function(objComponent, objEvent, objHelper) {
        objHelper.BuscaDelMunFact(objComponent, objEvent);
    },
    
	//La función SeleccionaMesFechaCierre
    ocSelecCiudadFact : function(objComponent, objEvent, objHelper) {
        objHelper.BuscaCiudadFact(objComponent, objEvent);
    },
    

	//La función SeleccionaMesFechaCierre
    ocSelecEstadoFact : function(objComponent, objEvent, objHelper) {
        objHelper.BuscaEstadoFact(objComponent, objEvent);
    },

	//La función SeleccionaMesFechaCierre
    actualizaDatosClienteSel : function(objComponent, objEvent, objHelper) {
        objHelper.actualizaDatosClienteSel(objComponent, objEvent);
    },
    
	//La función SeleccionaMesFechaCierre
    btnRestauraDatosCliente : function(objComponent, objEvent, objHelper) {
        objHelper.restauraDatosCliente(objComponent, objEvent);
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {
        //Recarga la pagina
        //location.reload();
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },

    
})