({
    getAnoFichaProd : function(component, event, helper) {
        helper.versionesAnioModelo(component, event, helper);
    },
    
    asignarValorSelec : function(component, event, helper) {
        var capturedCheckboxName 		= event.getSource().get("v.value");
        var capturedOpcion 				= event.getSource().get("v.name");
        if(capturedCheckboxName 		!= 'Elegir Modelo'){
            var selectedCheckBoxes 		=  component.get("v.selectedCheckBoxes");
            selectedCheckBoxes.push(capturedCheckboxName);
            var myMap 					= component.get("v.sectionLabels");
            myMap[capturedOpcion] 		= capturedCheckboxName;
            if(selectedCheckBoxes.includes("Elegir")){
                component.set("v.validaOpcionSelec", true);
                var selectedCheckBoxes 	= [];
            }else{
                component.set("v.validaOpcionSelec", false); 
                
            }
            
            component.set("v.selectedCheckBoxes", selectedCheckBoxes);
        }  
    },
    
    clonarFicha : function(component, event, helper) {
        helper.clonarFichaHelper(component, event, helper);
        
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.Spinner", false);
    }
    
})