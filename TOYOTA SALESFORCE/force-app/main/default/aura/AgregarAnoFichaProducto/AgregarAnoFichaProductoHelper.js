({
    helperMethod : function() {
        
    },
    
    versionesAnioModelo: function(component, event, helper){
        var idFichaProd 		=  component.get("v.FTid")
        var anoFichaTecnica  	= component.find("AnoFichaProducto").get("v.value");
        var serieModelo    		= component.get("v.SerieFT");
        var action 				= component.get("c.obtenerVeriones");
        
        
        if(anoFichaTecnica == '' || anoFichaTecnica == null){
            component.set("v.validaAnioFicha",true);
            console.log("Validacion true");
        }
        else{
            component.set("v.validaAnioFicha",false);
            action.setParams({ anoFichaTecnica : anoFichaTecnica,
                              serieModelo : serieModelo,
                              idFichaProd : idFichaProd
                             });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.Versiones",response.getReturnValue());
                    if(response.getReturnValue() == null){
                        component.set("v.validaResultados",true);        
                        
                    }else{
                        component.set("v.validaResultados",false); 
                        
                    }
                    
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);        
        }    
        
    },
    
    
    clonarFichaHelper : function(component, event, helper){
        var validaOpciones 	    = component.get("v.validaOpcionSelec");
        var myMap 					= component.get("v.sectionLabels");
        var jsonVersiones 			= myMap;
        var listaValores        	=  component.get("v.listValues");
        var fichaOriginal 			=  component.get("v.FTid")
        var anioNuevaFicha  		=  component.find("AnoFichaProducto").get("v.value");
        var serieModelo    			=  component.get("v.SerieFT");
        var validarSeleccion;
        var modeloVersionSelect 	= event.target.name;
        var modeloIn 				= event.target.value;
        var action 					= component.get("c.clonacionFichaTP");
        
        if(typeof modeloVersionSelect == "undefined"){
            component.set("v.validaOpcionSelec", true);
        }else{
            component.set("v.validaOpcionSelec", false);
            
        }
        
        

        if(validaOpciones == false){	
            action.setParams({ 
                fichaOriginal 			: fichaOriginal,
                modeloIn 				: modeloIn,
                modeloVersionSelect 	: modeloVersionSelect,
                anioNuevaFicha 			: anioNuevaFicha,
                jsonVersiones			: jsonVersiones
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.Versiones",response.getReturnValue());
                    var navEvt = $A.get("e.force:navigateToSObject");
                    console.log("clone",response.getReturnValue());
                    navEvt.setParams({
                        "recordId": response.getReturnValue(),
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                }
                else if (state === "INCOMPLETE") {
                    
                }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log("Error message: " + 
                                            errors[0].message);
                            }
                        } else {
                            console.log("Unknown error");
                        }
                    }
            });
            
            $A.enqueueAction(action);
            
        }
        
    }
    
})