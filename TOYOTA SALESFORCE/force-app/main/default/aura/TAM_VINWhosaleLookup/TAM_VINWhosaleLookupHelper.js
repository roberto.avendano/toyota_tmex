({
    searchHelper : function(component,event,getInputkeyWord) {
        var tipoCmpt = component.get("v.tipoComponenteVINLookup");
        if(tipoCmpt == 'excepcion'){
            var action = component.get("c.vinDealerDaily");
            var userId = $A.get("$SObjectType.CurrentUser.Id"); 
            var isAutoDemo =  component.get("v.isAutoDemo");
            action.setParams({
                'searchKeyWord': getInputkeyWord,
                userId 		: userId,
                isAutoDemo  : isAutoDemo
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    
                    if (storeResponse.length == 0) {
                        component.set("v.Message", 'VIN no encontrado en Dealer Daily');
                        component.set("v.existeEnDD",false);
                    } else {
                        component.set("v.Message", 'Resultado de la busqueda');
                    }
                    
                    component.set("v.listOfSearchRecords", storeResponse);
                }
                
            });
            
            $A.enqueueAction(action);
        }
        
        if(tipoCmpt == 'edoCta'){
            var tipoEdoCta = component.get("v.tipoSeccion");
            var listaVINES = component.get("v.lineasVINESProvisionados");
            var action = component.get("c.agregarVINEdoCta");
            var userId = $A.get("$SObjectType.CurrentUser.Id"); 
            var isAutoDemo = false;
            
            action.setParams({
                'searchKeyWord': getInputkeyWord,
                userId 		: userId,
                isAutoDemo  : isAutoDemo,
                listaVINES: listaVINES,
                tipoEdoCta : tipoEdoCta
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    
                    if (storeResponse.length == 0) {
                        component.set("v.Message", 'VIN no encontrado');
                    } else {
                        component.set("v.Message", 'Resultado de la busqueda');
                    }
                    
                    component.set("v.listOfSearchRecords", storeResponse);
                }
                
            });
            $A.enqueueAction(action);
        }
        
    },
})