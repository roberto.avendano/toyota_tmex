({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit..");
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit objModeloSelVinExcep001: ", Component.get("v.objModeloSelVinExcep") );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit objModeloSelExcepCte002: ", Component.get("v.objModeloSelExcepCte") );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.doInit recordId: ", Component.get("v.recordId"));
		Component.set('v.recordId', Component.get("v.recordId"));
    	Component.set("v.blnShowVinesConfirma", true);
    	Component.set("v.blnBloquearVines", false);

		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl lModelosSeleccionados.length: ", Component.get("v.objModeloSelExcepCte").lModelosSeleccionados.length );
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl lModelosSeleccionadosVin.length: ", Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin.length );
    	
		//Llama la función de getDistribEntrega
		if ( Component.get("v.objModeloSelExcepCte").lModelosSeleccionados.length > 0 )
			Helper.getDistribEntrega(Component, Event);
		if ( Component.get("v.objModeloSelExcepCte").lModelosSeleccionadosVin.length > 0 )
			Helper.getDistribEntregaInv(Component, Event);

    },

   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

   /** Funcion Inicial */
	guardar : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.cancelar..");
		Helper.guardar(Component, Event);
    },

   /** Funcion Inicial */
	desabilitaEstatus : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.desabilitaEstatus..");
		Helper.desabilitaEstatus(Component, Event);
    },

   /** Funcion Inicial */
	bloqueaCampos : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.bloqueaCampos..");
		Helper.bloqueaCampos(Component, Event);
    },

   /** ocSeleFact */
	ocSeleFact : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocSeleFact..");
		Helper.hpSeleFact(Component, Event);
    },

   /** ocSeleFact */
	ocSeleTodos : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodos..");
		Helper.hpSeleTodos(Component, Event);
    },



   /** ocSeleTodosCR */
	ocSeleTodosCR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosCR..");
		Helper.hpSeleTodosCR(Component, Event);
    },

   /** ocSeleTodosFECR */
	ocSeleTodosFECR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosFECR..");
		Helper.hpSeleTodosFECR(Component, Event);
    },

   /** ocLipiaFechaReciboCR */
	ocLipiaFechaReciboCR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocLipiaFechaReciboCR..");
		Helper.hpLipiaFechaReciboCR(Component, Event);
    },


   /** ocSeleTodosCE */
	ocSeleTodosCE : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosCE..");
		Helper.hpSeleTodosCE(Component, Event);
    },

   /** ocSeleTodosFECE */
	ocSeleTodosFECE : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl ocSeleTodosCE..");
		Helper.hpSeleTodosFECE(Component, Event);
    },

   /** ocLipiaFechaReciboCR */
	ocLipiaFechaReciboCR : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Contoller.ocLipiaFechaReciboCR..");
		Helper.hpLipiaFechaEntregaFECE(Component, Event);
    },



   /** ocEnviaCorreoNotificacion */
    ocEnviaCorreoNotificacion : function(objComponent, objEvent, objHelper) {
    	//Manda llamarl al metodo de guardar y y despues cierra la ventana
        objHelper.hpEnviaCorreoNotificacion(objComponent, objEvent);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})