({
    handleCancel : function(component, event, helper) {
        var appEvent = $A.get("e.c:OverlayLibraryModalEvent");
        appEvent.setParams({
            "message" : "Cancel" });
        appEvent.fire();
        component.find("overlayLib").notifyClose();
    },
    handleOK : function(component, event, helper) {
        var appEvent = $A.get("e.c:OverlayLibraryModalEvent");
        appEvent.setParams({
            "message" : "Ok" });
        appEvent.fire();
        component.find("overlayLib").notifyClose();
    }
})