({
    searchHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.vinBDDealerDaily");
        var userId = $A.get("$SObjectType.CurrentUser.Id"); 
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            userId 		: userId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'VIN no encontrado');
                } else {
                    component.set("v.Message", 'Resultado de la búsqueda');
                }
                
                component.set("v.listOfSearchRecords", storeResponse);
            }
            
        });
        
        $A.enqueueAction(action);
        
    },
})