({
	initComponent : function(component) {
		var accion = component.get("c.getModelosFichaProducto");
		var fichaProducto = component.get("v.productTabID");

		accion.setParams({
			"fichaProdID": fichaProducto
		});

		accion.setCallback(this, function(response){
			var state = response.getState();			
			console.log(state);
			if(state === "SUCCESS"){
				console.log(response.getReturnValue());
				component.set("v.productosList", response.getReturnValue());
			}
		});

		$A.enqueueAction(accion);
	},

	showSpinner : function(component) {
		component.set('v.spinner', true);
	},

	hideSpinner : function(component) {
		component.set('v.spinner', false);
	}
})