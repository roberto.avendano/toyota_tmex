({
	doInit : function(component, event, helper) {
		helper.initComponent(component);
	},

	addProductVersion : function(component, event, helper) {
		helper.showSpinner(component);
		var productID = event.currentTarget.dataset.id;
		var versionId = component.get("v.versionID");
		var productTab = component.get("v.productTabID");

		var accion = component.get("c.agregaProductoVersion");		

		accion.setParams({
			"prodID": productID,
			"verID": versionId,
		});

		accion.setCallback(this, function(response){
			var state = response.getState();			
			helper.hideSpinner(component);

			if(state === "SUCCESS"){
			    helper.initComponent(component);
			    //location.reload();	
			}
		});

		$A.enqueueAction(accion);

	}
})