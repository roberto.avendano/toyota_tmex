({
    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN SolPrevPedido Contoller.doInit..");
		//console.log("EN SolPrevPedido Contoller.doInit listModeloSelAddDist: " + JSON.stringify(Component.get("v.listModeloSelAddDist")));
		console.log("EN SolPrevPedido Contoller.doInit recordId: " + Component.get("v.recordId"));
		Component.set('v.recordId', Component.get("v.recordId"));
		Component.set('v.blnShowPrevPedidoCmp', true);	
		//Yama al metodo updListaFinalAutosPed para seleccionar los que ya tienen una cantidad
		Helper.updListaFinalAutosPed(Component, Event);	
    },

   /** Funcion Inicial */
	guardar : function(Component, Event, Helper) {
		console.log("EN SolPrevPedido Contoller.removeRow..");
		Helper.save(Component, Event);
    },

   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN SolPrevPedido Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

   /** Funcion Inicial */
	removeRow : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.removeRow..");
		Helper.removeDatosModelo(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
    
})