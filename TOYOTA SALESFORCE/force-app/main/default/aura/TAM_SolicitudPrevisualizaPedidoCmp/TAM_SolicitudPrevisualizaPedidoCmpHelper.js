({

    /** Guardado de sección */
    updListaFinalAutosPed : function(Component, Event){
    	console.log("EN Helper.updListaFinalAutosPed...: ");  
		let listModeloSelFinalesPaso = [];
		let listModeloSelPrev = Component.get("v.listModeloSelAddDist");
		//Recorre la lista de modelos que vienen en listModeloSelAddDist y toma solo los que tienen un cantidad
		for(let a in listModeloSelPrev){
			//Ve si tiene una cantidad
			if (listModeloSelPrev[a].strCantidad > 0){
				console.log("EN Helper.updListaFinalAutosPed listModeloSelPrev[a]: " + JSON.stringify(listModeloSelPrev[a])); 
				listModeloSelFinalesPaso.push(listModeloSelPrev[a]);
			}
		}//Fin del for para listModeloSelPrev
		//Inicializa la lista final
		Component.set('v.listModeloSelFinales', listModeloSelFinalesPaso);
		console.log("EN Helper.updListaFinalAutosPed listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinales")));  
    },

    /** Guardado de sección */
    save : function(Component, Event){
    
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.saveDatosModelos");
        objAction.setParams({   			
        	sWrpDist : JSON.stringify(Component.get("v.listModeloSelFinales")),
        	recordId : Component.get("v.recordId"),
        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId"),
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();
            	
            	if (objWrpResAct.blnEstatus == false){
            		let wrpDistr = [];
            		Component.set("v.lstWrpDistAct", wrpDistr); 
            		Component.set("v.blnShowDistCmp", false);
            		Component.set("v.intIndex", '0');
            		//alert('blnShowDistCmp: ' +  Component.get("v.blnShowDistCmp"))	            	
            		this.showToastSuccess(Component, Event, objWrpResAct.strDetalle);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(Component, Event, objWrpResAct.strDetalle);

            		Component.set("v.blnShowPrevPedidoCmp", false);
            		console.log("EN Helper.cancelar blnShowPrevPedidoCmp: " + Component.get("v.blnShowPrevPedidoCmp"));        	        
            		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);    
    
    },

    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	//let listModeloSelAddDist = [];
    	//Component.set("v.listModeloSelAddDist", listModeloSelAddDist); 
    	Component.set("v.blnShowPrevPedidoCmp", false);
    	console.log("EN Helper.cancelar blnShowPrevPedidoCmp: " + Component.get("v.blnShowPrevPedidoCmp"));        	        
    },

    /** */
    removeDatosModelo : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.removeDatosModelo...");
    	
    	let varIndex = Event.getSource().get("v.value");
    	console.log("EN SolCompEspDist Helper.removeDatosModelo varIndex: " + varIndex);    	
    	let listModeloSelFinalesPaso = Component.get("v.listModeloSelFinales");
    	console.log("EN SolCompEspDist Helper.removeDatosModelo listModeloSelFinalesPaso: " + listModeloSelFinalesPaso.length);
        let objSelecPaso = listModeloSelFinalesPaso[parseInt(varIndex)];

    	console.log("EN SolCompEspDist Helper.removeDatosModelo objSelecPaso: ", objSelecPaso);
    	let sstrIdCatModelos = objSelecPaso.strIdCatModelos;
    	let sstrIdExterno = objSelecPaso.strIdExterno;

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.eliminaDatosModelos");
        objAction.setParams({   			
        	"recordId" : Component.get("v.recordId"),
        	"sstrIdCatModelos" : sstrIdCatModelos,
        	"sstrIdExterno" : sstrIdExterno
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();            	
            	if (objWrpResAct.blnEstatus == false){
            		let wrpDistr = [];
            		listModeloSelFinalesPaso.splice(varIndex, 1);            		           		
            		Component.set("v.listModeloSelFinales", listModeloSelFinalesPaso); 
            		this.showToastSuccess(Component, Event, objWrpResAct.strDetalle);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(Component, Event, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

	}, 
		
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }  

})