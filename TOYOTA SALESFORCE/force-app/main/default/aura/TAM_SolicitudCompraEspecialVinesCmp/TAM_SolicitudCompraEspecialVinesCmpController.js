({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.doInit..");
		console.log("EN SolCompEspDist Contoller.doInit objModeloSelAddDist: " + Component.get("v.objModeloSelAddDist"));
		console.log("EN SolCompEspDist Contoller.doInit recordId: " + Component.get("v.recordId"));
		Component.set('v.recordId', Component.get("v.recordId"));
		Component.set('v.blnShowDistCmp', true);
		Helper.getWrpModelo(Component, Event);
		Helper.getDistribuidores(Component, Event);
		Helper.getDistribDatosModelo(Component, Event);		
    },

   /** Funcion Inicial */
	getDistribDatosModelo : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.getDistribDatosModelo..");
		Helper.getDistribDatosModelo(Component, Event);
    },

   /** Funcion Inicial */
	agregaVines : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.agregaVines..");
		Helper.addVines(Component, Event);
    },

   /** Funcion Inicial */
	removeRow : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.removeRow..");
		Helper.removeDistribuidor(Component, Event);
    },

   onSingleSelectChange: function(Component, Event) {
	    console.log("EN SolCompEspDist Contoller.onSingleSelectChange..");   
        //var selectCmp = Component.find("InputSelectSingle");   
        var varValueDistSel = Event.getSource().get("v.value");
        var varNameDistSel = Event.getSource().get("v.name"); 
        var varrowIndex = Event.getSource().get("v.intIndex"); 
        console.log("EN SolCompEspDist Contoller.onSingleSelectChange varValueDistSel: " + varValueDistSel + ' varNameDistSel: ' + varNameDistSel + ' varrowIndex: ' + varrowIndex);

    	var lstWrpDistPaso = Component.get('v.lstWrpDistAct');
    	//Recorre la lista de distribuisores y qita el que no se necesita
    	for(var a in lstWrpDistPaso){
    		if (lstWrpDistPaso[a].intIndex == varNameDistSel){
    			lstWrpDistPaso[a].strIdDistrib = varValueDistSel;
    			console.log("EN SolCompEspDist Helper.removeDistribuidor YA LO ENCONTRE intIndex: " + lstWrpDistPaso[a].intIndex + ' varNameDistSel: ' + varNameDistSel + ' ELEMNT: ' + lstWrpDistPaso[a].strIdDistrib);
    			break;	
    		}//Fin si lstWrpDistPaso[a].intIndex == varIndex
    	}//Fin del for para lstWrpDistPaso
    	//Actualiliza la lista final
    	Component.set("v.lstWrpDistAct", lstWrpDistPaso);
        
    },

   /** Funcion Inicial */
	guardar : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.removeRow..");
		Helper.save(Component, Event);
    },

   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN SolCompEspDist Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
    
})