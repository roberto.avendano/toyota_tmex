({
    sectionActive: function(objComponent, objEvent, objHelper) {
        var sectionActive = objComponent.get("v.activeSection");
        
        if(sectionActive.includes('Retail')){
            objComponent.set("v.boolFiltrado_Retail",true);
        }
        
        if(sectionActive.includes('Bonos')){
        	objComponent.set("v.boolFiltrado_Bonos",true);
        }
        
        if(sectionActive.includes('VetaCorp_a_Retail')){
        	objComponent.set("v.boolFiltrado_VtaCorp_a_Retail",true);
        }

        if(sectionActive.includes('Flotillas')){
        	objComponent.set("v.boolFiltrado_Flotillas",true);
        }
       
    }
})