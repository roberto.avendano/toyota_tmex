({
	doInit : function(component,event,helper){ 
		console.log("EN Comntroller.doInit doInit..."); 
		//debugger;
		//Consulta los datos del reistro y ve de que tipo de cliente se trata
		helper.getTipoPersona(component);
    },

	actualizaDatos : function(component,event,helper){ 
		console.log("EN Comntroller.actualizaDatos..."); 
		//Consulta los datos del reistro y ve de que tipo de cliente se trata
		helper.getTipoPersona(component);
		helper.doConsultaDatos(component,event,helper);
		//Para mostrar el mensaje que se esta actualizando la información...
		helper.showToastSuccess(component, event, "Los datos se actualizaron con exito...");
    },

	doConsultaDatos : function(component,event,helper){ 
		console.log("EN Comntroller.doConsultaDatos..."); 
		//debugger;
		console.log("EN Comntroller.doInit component.get('v.bCteCorpPM'): ", component.get('v.bCteCorpPM')); 
		console.log("EN Comntroller.doInit component.get('v.bCteCorpPF'): ", component.get('v.bCteCorpPF')); 
		console.log("EN Comntroller.doInit component.get('v.bIniTipoVenta'): ", component.get('v.bIniTipoVenta'));
		
		//Carca solo los que corresponden a persona Moral
		if (component.get('v.bCteCorpPM') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para flotilla..."); 
			var strActaConst = "Acta Constitutiva";
			helper.getUploadedFiles(component, strActaConst);
			var strCedFis = "Cedula Fiscal";
			helper.getUploadedFiles(component, strCedFis);
			var strOrdenCompra = "Orden de compra";
			helper.getUploadedFiles(component, strOrdenCompra);
			var strDatosAdicionales = "PDF datos adicionales";
			helper.getUploadedFiles(component, strDatosAdicionales);
		}//Fin si omponent.get('v.bCteCorpPM')

		//Carca solo los que corresponden a Persona Fisica
		if (component.get('v.bCteCorpPF') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para Programa..."); 
			var strIdenOfic = "Identificación oficial";
			helper.getUploadedFiles(component, strIdenOfic);
			var strCompDom = "Comprobante de domicilio";
			helper.getUploadedFiles(component, strCompDom);
			var strDocSop = "Documentación de soporte";
			helper.getUploadedFiles(component, strDocSop);						
		}//Fin si omponent.get('v.bCteCorpPM')

		//Carca solo los que corresponden a Persona Fisica
		if (component.get('v.bClienteVencido') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para Cliente Vencido..."); 
			var strCartComp = "Carta Compromiso";
			helper.getUploadedFiles(component, strCartComp);
			var strOrdenCompra = "Orden de compra";
			helper.getUploadedFiles(component, strOrdenCompra);
		}//Fin si omponent.get('v.bClienteVencido')
		
    },

    previewFile : function(component,event,helper){  
        var rec_id = event.currentTarget.id;  
        $A.get('e.lightning:openFiles').fire({ 
            recordIds: [rec_id]
        });  
    },  
    
    
    //PARA LA PARTE DE LOS ARCHIVOS DE FLOTILLA
    UploadFinishedActCons : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedActCons...");
    	var strActaConst = "Acta Constitutiva";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        console.log("EN Comntroller.UploadFinishedActCons fileUploaded: ", fileUploaded);
        console.log("EN Comntroller.UploadFinishedActCons documentId: ", documentId); 
        console.log("EN Comntroller.UploadFinishedActCons fileName: ", fileName);
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
    }, 
 
    UploadFinishedCedFiscal : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedCedFiscal...");
    	var strActaConst = "Cedula Fiscal";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
    }, 
 
    UploadFinishedOrdenCompra : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedrdenCompra...");
    	var strActaConst = "Orden de compra";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
    }, 
    
   UploadFinishedDatosAdicion : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedrdenCompra...");
    	var strActaConst = "PDF datos adicionales";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
    }, 

    UploadFinishedCartaComp : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedCartaComp...");
    	var strCartaCompro = "Carta Compromiso";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        console.log("EN Comntroller.UploadFinishedCartaComp fileUploaded: ", fileUploaded);
        console.log("EN Comntroller.UploadFinishedCartaComp documentId: ", documentId); 
        console.log("EN Comntroller.UploadFinishedCartaComp fileName: ", fileName);
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strCartaCompro, documentId);
    	helper.getUploadedFiles(component, strCartaCompro);
    }, 


   //PARA LA PARTE DE LOS ARCHIVOS DE PROGRAMA
   UploadFinishedIdeOfi : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedIdeOfi...");
    	var strActaConst = "Identificación oficial";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
    }, 

   UploadFinishedCompDom : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedIdeOfi...");
    	var strActaConst = "Comprobante de domicilio";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
   }, 

   UploadFinishedDocSop : function(component, event, helper) {  
    	console.log("EN Comntroller.UploadFinishedIdeOfi...");
    	var strActaConst = "Documentación de soporte";  
    	var uploadedFiles = event.getParam("files"); 
    	var fileUploaded = uploadedFiles[0];
    	var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
    	//Llama la funcion que va a actualizar el campo de Descripción
    	helper.upDateFile(component, strActaConst, documentId);
    	helper.getUploadedFiles(component, strActaConst);
   }, 
    
    //PARA LA PARTE DE LOS ARCHIVOS DE FLOTILLA    
    delFilesActCons : function(component,event,helper){
    	console.log("EN Comntroller.delFilesActCons...");
        component.set("v.Spinner", true);
        var strActaConst = "Acta Constitutiva"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesActCons ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLib').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    delFilesCedFiscal : function(component,event,helper){
    	console.log("EN Comntroller.delFilesCedFiscal...");
        component.set("v.Spinner", true);
        var strActaConst = "Cedula Fiscal"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesCedFiscal ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLibCF').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },
      
    delFilesOrdenCompra : function(component,event,helper){
    	console.log("EN Comntroller.delFilesOrdenCompra...");
        component.set("v.Spinner", true);
        var strActaConst = "Orden de compra"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesOrdenCompra ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLibOC').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    delFilesDatosAdicion : function(component,event,helper){
    	console.log("EN Comntroller.delFilesDatosAdicion...");
        component.set("v.Spinner", true);
        var strActaConst = "PDF datos adicionales"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesDatosAdicion ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLibDA').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    delFilesCartaCompro : function(component,event,helper){
    	console.log("EN Comntroller.delFilesCartaCompro...");
        component.set("v.Spinner", true);
        var strCartaComp = "Carta Compromiso"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesCartaCompro ANTES DE ELIMINAR EL ARCHIVO " + strCartaComp + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strCartaComp);
        component.find('notifLibCC').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    //PARA LA PARTE DE LOS ARCHIVOS DE PROGRAMA    
    delFilesIdeOfi : function(component,event,helper){
    	console.log("EN Comntroller.delFilesIdeOfi...");
        component.set("v.Spinner", true);
        var strActaConst = "Identificación oficial"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesIdeOfi ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLIO').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    delFilesCompDom : function(component,event,helper){
    	console.log("EN Comntroller.delFilesCompDom...");
        component.set("v.Spinner", true);
        var strActaConst = "Comprobante de domicilio"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesCompDom ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLCD').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    delFilesDocSop : function(component,event,helper){
    	console.log("EN Comntroller.delFilesDocSop...");
        component.set("v.Spinner", true);
        var strActaConst = "Documentación de soporte"; 
        var documentId = event.currentTarget.id;
        console.log("EN Comntroller.delFilesDocSop ANTES DE ELIMINAR EL ARCHIVO " + strActaConst + ' documentId: ' + documentId);
        helper.delUploadedfiles(component, documentId, strActaConst);
        component.find('notifLDS').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Archivo eliminado!!",
            closeCallback: function() {}
        });    	          
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
      // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
         
    UploadFinished : function(component, event, helper) {  
        var uploadedFiles = event.getParam("files");  
        helper.getUploadedFiles(component);         
        component.find('notifLib').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "File Uploaded successfully!!",
            closeCallback: function() {}
        });
    }, 
    
    handleUploadFinished : function(component, event, helper) {
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "File "+fileName+" Uploaded successfully."
        });
        toastEvent.fire();
        
        $A.get('e.lightning:openFiles').fire({
            recordIds: [documentId]
        });
    }

})