({
   getTipoPersona: function (component){
   
	   console.log("EN Heler.getTipoPersona.recordId ", component.get("v.recordId"));
	   console.log("EN Heler.getTipoPersona.leadRecord ", component.get('v.leadRecord')); 
	   
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.consultDatosCand");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){  
                var strTipoCand = response.getReturnValue();
                console.log("EN Heler. strTipoCand ", strTipoCand);
                if (strTipoCand == 'ClienteCorpPM'){
                	component.set("v.bCteCorpPM", true);
                	component.set("v.bCteCorpPF", false);
                }//Fin si strTipoCand == 'ClienteCorpPM'
                if (strTipoCand == 'ClienteCorpPF'){
                	component.set("v.bCteCorpPF", true);
                	component.set("v.bCteCorpPM", false);
                }//Fin si strTipoCand == 'ClienteCorpPF'
                       
                //Inicializa bIniTipoVenta con component.get('v.bIniTipoVenta')
                var bIniTipoVenta = component.get('v.bIniTipoVenta');
                //Inicializa la variable de bIniTipoVenta
                if (bIniTipoVenta) 
                	component.set("v.bIniTipoVenta", false);
                if (!bIniTipoVenta) 
                	component.set("v.bIniTipoVenta", true);

                //Despliega los mensajes de log
                console.log("EN Heler.getTipoPersona.bCteCorpPM ", component.get('v.bCteCorpPM'));
                console.log("EN Heler.getTipoPersona.bCteCorpPF ", component.get('v.bCteCorpPF'));	
                console.log("EN Heler.getTipoPersona.bIniTipoVenta ", component.get('v.bIniTipoVenta'));
                console.log("EN Heler.getTipoPersona.bClientePreAutorizado ", component.get('v.bClientePreAutorizado'));
                
            }  
        });  
        $A.enqueueAction(action);
    },
    
	upDateFile : function(component, strActaConst, documentId){
		//debugger;
		console.log("EN Heler.upDateFile strActaConst: " + strActaConst + ' documentId: ' + documentId); 
		var strActaConst = strActaConst;
		var strDocumentId = documentId;
        var action = component.get("c.updFileDesc");  
        action.setParams({  
            "recordId" : component.get("v.recordId"),
            "nombreArchivo" : strActaConst,
            "documentId" : documentId  
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                var result = response.getReturnValue();
                if (strActaConst == 'Acta Constitutiva')
                	component.set("v.filesActCons",result);  
                if (strActaConst == 'Cedula Fiscal')
                	component.set("v.filesCedFiscal",result);
                if (strActaConst == 'Orden de compra')
                	component.set("v.filesOrdenCompra",result);
                if (strActaConst == 'PDF datos adicionales')
                	component.set("v.filesDatosAdicion",result);
                if (strActaConst == 'Identificación oficial')
                	component.set("v.filesDatosIdenOfi",result);
                if (strActaConst == 'Comprobante de domicilio')
                	component.set("v.filesDatosCompDom",result);
                if (strActaConst == 'Documentación de soporte')
                	component.set("v.filesDatosDocSop",result);
                	
                if (strActaConst == 'Carta Compromiso')
                	component.set("v.filesDatosCarComp",result);
                	
            }  
        });  
        $A.enqueueAction(action);  
    },
        
	getUploadedFiles : function(component, strActaConst){
		//debugger;
		console.log("EN Heler.getUploadedFiles strActaConst: ", strActaConst); 
		var strActaConst = strActaConst;  
        var action = component.get("c.getFiles");  
        action.setParams({  
            "recordId" : component.get("v.recordId"),
            "nombreArchivo" : strActaConst  
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                var result = response.getReturnValue();
                if (strActaConst == 'Acta Constitutiva')
                	component.set("v.filesActCons",result);  
                if (strActaConst == 'Cedula Fiscal')
                	component.set("v.filesCedFiscal",result);  
                if (strActaConst == 'Orden de compra')
                	component.set("v.filesOrdenCompra",result);   
                if (strActaConst == 'PDF datos adicionales')
                	component.set("v.filesDatosAdicion",result);          
                if (strActaConst == 'Identificación oficial')
                	component.set("v.filesDatosIdenOfi",result);
               if (strActaConst == 'Comprobante de domicilio')
                	component.set("v.filesDatosCompDom",result);
                if (strActaConst == 'Documentación de soporte')
                	component.set("v.filesDatosDocSop",result);   

                if (strActaConst == 'Carta Compromiso')
                	component.set("v.filesDatosCarComp",result);
            }  
        });  
        $A.enqueueAction(action);  
    },
    
    delUploadedfiles : function(component, documentId, strNombreArchivo) {
    	console.log("EN Helper.delUploadedfiles..." + strNombreArchivo + ' documentId: ' + documentId);
        var action = component.get("c.deleteFiles");           
        action.setParams({
            "sdocumentId":documentId
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){ 
            	console.log("EN Helper.delUploadedfiles getUploadedFiles...");
                this.getUploadedFiles(component, strNombreArchivo);
                component.set("v.Spinner", false); 
            }  
        });  
        $A.enqueueAction(action);  
    },  

	doConsultaDatos : function(component,event,helper){ 
		console.log("EN Comntroller.doConsultaDatos..."); 
		//debugger;
		console.log("EN Comntroller.doInit component.get('v.bCteCorpPM'): ", component.get('v.bCteCorpPM')); 
		console.log("EN Comntroller.doInit component.get('v.bCteCorpPF'): ", component.get('v.bCteCorpPF')); 
		console.log("EN Comntroller.doInit component.get('v.bIniTipoVenta'): ", component.get('v.bIniTipoVenta'));
		
		//Carca solo los que corresponden
		if (component.get('v.bCteCorpPM') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para flotilla..."); 
			var strActaConst = "Acta Constitutiva";
			helper.getUploadedFiles(component, strActaConst);
			var strCedFis = "Cedula Fiscal";
			helper.getUploadedFiles(component, strCedFis);
			var strOrdenCompra = "Orden de compra";
			helper.getUploadedFiles(component, strOrdenCompra);
			var strDatosAdicionales = "PDF datos adicionales";
			helper.getUploadedFiles(component, strDatosAdicionales);
		}//Fin si omponent.get('v.bCteCorpPM')

		//Carca solo los que corresponden
		if (component.get('v.bCteCorpPF') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para Programa..."); 
			var strIdenOfic = "Identificación oficial";
			helper.getUploadedFiles(component, strIdenOfic);
			var strCompDom = "Comprobante de domicilio";
			helper.getUploadedFiles(component, strCompDom);
			var strDocSop = "Documentación de soporte";
			helper.getUploadedFiles(component, strDocSop);						
		}//Fin si omponent.get('v.bCteCorpPM')
		
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje,
            "duration": 500,
            "type": "success"
        });
        toastEvent.fire();
    },
        
})