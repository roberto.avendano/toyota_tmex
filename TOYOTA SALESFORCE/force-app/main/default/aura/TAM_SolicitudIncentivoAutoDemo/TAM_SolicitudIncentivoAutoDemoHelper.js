({
    /*Cancelar*/
    close : function(objComponent, objEvent) {
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "TAM_SolicitudAutoDemo__c"
        });
        homeEvt.fire();
    },
    
    /*Guardar*/
    guardarRegistro : function(objComponent, objEvent){
        
        var objSolicitud = objComponent.get('v.selectedRecord');
        var objAction = objComponent.get("c.saveRecord");
        objAction.setParams({ objRecord : JSON.stringify(objSolicitud)});
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                
                var booleanResult = objResponse.getReturnValue();
                if(booleanResult){
                    this.showToastSuccess(objComponent, objEvent);
                    
                } else {
                    this.showToastError(objComponent, objEvent); 
                }  
            }
            this.close(objComponent, objEvent);
        });
        $A.enqueueAction(objAction);
    },
     /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Solicitud Enviada Correctamente.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el envio de la solicitud, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
})