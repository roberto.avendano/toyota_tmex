({

    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, Helper) {
		console.log("EN doInit...");
		let blnblnDespDealerOrigenPaso = false;
		
		try {

		Helper.hpPerfilUserActual(objComponent, objEvent);
		console.log("EN doInit objWrpListaDealersUsuarios: ", objComponent.get("v.objWrpListaDealersUsuarios"));
  		console.log("EN doInit blnDespDealerOrigen: ", objComponent.get("v.blnDespDealerOrigen") );           		
		blnblnDespDealerOrigenPaso = objComponent.get("v.blnDespDealerOrigen");
  		console.log("EN doInit blnblnDespDealerOrigenPaso: ", blnblnDespDealerOrigenPaso);
  		

        
		objComponent.set("v.sPropietario", '57999');
		objComponent.set("v.sDistribuidor", '57999');
		objComponent.set("v.sPropietarioFinal", '57999');
		
		} catch (error) {
			//alert('ERROR EN doInit: ' + error);
			console.error('ERROR EN doInit: ' + error);
		}
    },


    updateColumnSorting: function (cmp, event, helper) {
        cmp.set('v.isLoading', true);
        // We use the setTimeout method here to simulate the async
        // process of the sorting data, so that user will see the
        // spinner loading when the data is being sorted.
        setTimeout($A.getCallback(function() {
            let fieldName = event.getParam('fieldName');
            let sortDirection = event.getParam('sortDirection');
            cmp.set("v.sortedBy", fieldName);
            cmp.set("v.sortedDirection", sortDirection);
            helper.sortData(cmp, fieldName, sortDirection);
            cmp.set('v.isLoading', false);
        }), 0);
    },


   /** Funcion Inicial */
	buscaClientes : function(objComponent, objEvent, objHelper) {
		objHelper.helperBuscaClientes(objComponent, objEvent);
    },

	onRadioCliente: function(objComponent, objEvent) {
		console.log("EN Controller onRadioCliente...");	
		let varNameCteSel = objEvent.getSource().get("v.name");
		let varTextCteSel = objEvent.getSource().get("v.text");
		console.log("EN Controller onRadioCliente varTextCteSel: ", varTextCteSel);
   		objComponent.set("v.strClienteSeleccionadoMerge", varTextCteSel);
   		objComponent.set("v.SelCteMerge", false);
   		let llClientesDealerPaso = objComponent.get("v.lClientesDealer");
   		//Recorre la lista de reg de lClientesDealer
   		for (let clienteId in llClientesDealerPaso){
   			console.log("EN Controller onRadioCliente strIdCliente: ", llClientesDealerPaso[clienteId].strIdCliente);
   			console.log("EN Controller onRadioCliente ", varTextCteSel);
    		if (llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel){
    			llClientesDealerPaso[clienteId].blnBloqueado = true;
    			llClientesDealerPaso[clienteId].blnSeleccionaCte = true;
    			llClientesDealerPaso[clienteId].blnSeleccionado = false;
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel
    		if (llClientesDealerPaso[clienteId].strIdCliente != varTextCteSel){
    			llClientesDealerPaso[clienteId].blnBloqueado = false;
    			llClientesDealerPaso[clienteId].blnSeleccionado = false;    		
    			llClientesDealerPaso[clienteId].blnSeleccionaCte = false;
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente != varTextCteSel
   		}//Fin del for para llClientesDealerPaso
   		objComponent.set("v.lClientesDealer", llClientesDealerPaso);
	},

	onCheckClienteMerge: function(objComponent, objEvent) {
		console.log("EN Controller onCheckClienteMerge...");	
   		//objComponent.set("v.strClienteSeleccionadoMerge", varTextCteSel);   		
		objComponent.set("v.SelCtesMerge", false);
   		let llClientesDealerPaso = objComponent.get("v.lClientesDealer");
   		//Recorre la lista de reg de lClientesDealer
   		for (let clienteId in llClientesDealerPaso){
    		if (llClientesDealerPaso[clienteId].blnSeleccionado){
    			objComponent.set("v.SelCtesMerge", false);
    			break;    		
    		}//Fin si llClientesDealerPaso[clienteId].strIdCliente == varTextCteSel
   		}//Fin del for para llClientesDealerPaso   		
		console.log("EN Controller onCheckClienteMerge SelCtesMerge: ", objComponent.get("v.SelCtesMerge"));
	},

	//La función ocSelecPropietario
    ocSelecPropietario : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecPropietario(objComponent, objEvent);
    },

	//La función ocSelecDistribuidor
    ocSelecDistribuidor : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecDistribuidor(objComponent, objEvent);
    },

	//La función ocActTexto
    ocActTexto : function(objComponent, objEvent, objHelper) {
        objHelper.hpActTexto(objComponent, objEvent);
    },

	//La función ocSelecNvoPropie
    ocSelecNvoPropie : function(objComponent, objEvent, objHelper) {
        objHelper.hpSelecNvoPropie(objComponent, objEvent);
    },

	//La función ocBuscarCandidatos
	ocBuscarCandidatos: function(objComponent, objEvent, objHelper) {
		console.log("EN Controller ocBuscarCandidatos...");
		objHelper.hpBuscarCandidatos(objComponent, objEvent);		
	},

	//La función ocUpdateSelectedText	 
	ocUpdateSelectedText: function(objComponent, objEvent, objHelper) {
		console.log("EN Controller ocUpdateSelectedText...");
        let selectedRows = objEvent.getParam('selectedRows');
		console.log("EN Controller ocUpdateSelectedText selectedRows: ", selectedRows);
        objComponent.set('v.selectedRowsCount', selectedRows.length);	

        let lCandidSelecPaso = [];
        
        //Recorre la lista de  selectedRows
    	for (let cntCandSel in selectedRows){
    		//Agregalo a la lista de lCandidSelecPaso
    		lCandidSelecPaso.push(selectedRows[cntCandSel]);
        }//Fin del for para selectedRows
        
        //No tiene nada la lita de selectedRows
        if (selectedRows.length > 0)
        	objComponent.set('v.blnSelec', false);	
        //No tiene nada la lita de selectedRows
        if (selectedRows.length == 0)
        	objComponent.set('v.blnSelec', true);	
        
        //Setea finalmente la lista de 
        objComponent.set('v.lCandidSelec', lCandidSelecPaso);	
		console.log("EN Controller ocUpdateSelectedText selectedRows: ", objComponent.get('v.lCandidSelec'));
        
    },
	  
	//La función ocConfirmaReasigReg
	ocConfirmaReasigReg: function(objComponent, objEvent, objHelper) {
		console.log("EN Controller ocConfirmaReasigReg...");
		//Ve si seleciono algo en el campo de sPropietarioFinal
		let ssPropietarioFinal = objComponent.get("v.sPropietarioFinal");
		console.log("EN Controller ocConfirmaReasigReg ssPropietarioFinal: ", ssPropietarioFinal);
		if (ssPropietarioFinal === '57999' || ssPropietarioFinal === null || ssPropietarioFinal === '' || ssPropietarioFinal === undefined)
       		objHelper.showToastError(objComponent, objEvent, 'Debes seleccionar al usuario destino.');			
		else
			objHelper.hpConfirmaReasignacion(objComponent, objEvent);
		console.log("EN Controller ocConfirmaReasigReg ssPropietarioFinal2: ", ssPropietarioFinal);		
	},


    ocCloseQuickAction : function(Component, Event, strMensaje) {
        let navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
})