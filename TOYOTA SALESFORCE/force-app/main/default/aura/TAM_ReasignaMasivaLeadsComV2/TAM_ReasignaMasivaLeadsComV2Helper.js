({

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpPerfilUserActual : function(objComponent, objEvent) {
		console.log("EN hpPerfilUserActual...");

        //Llama la función 
		let objAction = objComponent.get("c.getPerfilUserActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            	
            	let blnAdminUser = objResponse.getReturnValue();
           		console.log("EN hpPerfilUserActual blnAdminUser: ", blnAdminUser);
            	if (blnAdminUser == true){
            		objComponent.set("v.blnUserAdmin", true);
            		objComponent.set("v.blnNoUserAdmin", false);
            		//Consulta los datos por default
            		this.hpBuscaUsuariosActivos(objComponent, objEvent);
            		//Consulta los datos por default
            		this.hpConsultaLeadDefault(objComponent, objEvent);
            	}//Fin si objWrpResAct.blnEstatus == false
            	if (blnAdminUser == false){
            		objComponent.set("v.blnUserAdmin", false);
            		objComponent.set("v.blnNoUserAdmin", true);
            	}//Fin si blnAdminUser == false
           		console.log("EN hpPerfilUserActual blnUserAdmin: ", objComponent.get("v.blnUserAdmin") );
           		console.log("EN hpPerfilUserActual blnNoUserAdmin: ", objComponent.get("v.blnNoUserAdmin") );               		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpBuscaUsuariosActivos : function(objComponent, objEvent) {
		console.log("EN hpBuscaUsuariosActivos...");
        //Llama la función 
		let objAction = objComponent.get("c.getUsuariosActivos ");
        objAction.setParams({
        	"IdClienteActual" : objComponent.get("v.recordId")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpListaDealersUsuarios = objResponse.getReturnValue();
           		console.log("EN hpBuscaUsuariosActivos objWrpListaDealersUsuarios: ", objWrpListaDealersUsuarios);
           		objComponent.set("v.objWrpListaDealersUsuarios", objWrpListaDealersUsuarios);           		
           		console.log("EN hpBuscaUsuariosActivos objWrpListaDealersUsuarios: ", objComponent.get("v.objWrpListaDealersUsuarios") );
           		if (objWrpListaDealersUsuarios !== null){
           			if (objWrpListaDealersUsuarios.lWrpListaDealers !== null){
           				console.log("EN hpBuscaUsuariosActivos objWrpListaDealersUsuarios: ", objWrpListaDealersUsuarios.lWrpListaDealers.length );
           				//Si es mas de un Dealer
           				if (objWrpListaDealersUsuarios.lWrpListaDealers.length > 1){
           					objComponent.set("v.blnDespDealerOrigen", true);
					        objComponent.set('v.columns', [
					            { label: 'Nombre candidato', fieldName: 'sName', type: 'text', sortable: true, fixedWidth: 185},
					            { label: 'Vehículo', fieldName: 'sVehiculo', type: 'text', sortable: true },
					            { label: 'Dealer', fieldName: 'sCodDist', type: 'text', sortable: true, fixedWidth: 75},
					            { label: 'Dler. Origen', fieldName: 'sCodDistOrig', type: 'text', sortable: true, fixedWidth: 110},
					            { label: 'Nom prop kban', fieldName: 'sNomPropKban', type: 'text', sortable: true},
					            { label: 'Fecha de creación', fieldName: 'sFechaCreacion', type: 'dateTime', sortable: true},
					            { label: 'Origen del candidato', fieldName: 'sOrigenCand', type: 'text', sortable: true},
					            { label: 'Estado del candidato', fieldName: 'sEstadoCand', type: 'text', sortable: true},
					            { label: 'Teléfono', fieldName: 'sTelefono', type: 'text'}
					        ]);           				
           				}//Fin si 
           				//Si solo es un Dealer
           				if (objWrpListaDealersUsuarios.lWrpListaDealers.length == 1){
           					objComponent.set("v.blnDespDealerOrigen", false);
					        objComponent.set('v.columns', [
					            { label: 'Nombre candidato', fieldName: 'sName', type: 'text', sortable: true, fixedWidth: 185},
					            { label: 'Vehículo', fieldName: 'sVehiculo', type: 'text', sortable: true },
					            { label: 'Dealer', fieldName: 'sCodDist', type: 'text', sortable: true},
					            { label: 'Nom prop kban', fieldName: 'sNomPropKban', type: 'text', sortable: true},
					            { label: 'Fecha de creación', fieldName: 'sFechaCreacion', type: 'dateTime', sortable: true},
					            { label: 'Origen del candidato', fieldName: 'sOrigenCand', type: 'text', sortable: true},
					            { label: 'Estado del candidato', fieldName: 'sEstadoCand', type: 'text', sortable: true},
					            { label: 'Teléfono', fieldName: 'sTelefono', type: 'text'}
					        ]);		
           				}//Fin si objWrpListaDealersUsuarios.lWrpListaDealers.length == 1
           			}//Fin si objWrpListaDealersUsuarios.lWrpListaDealers !== null
           		}//Fin si objWrpListaDealersUsuarios !== null
           		console.log("EN hpBuscaUsuariosActivos blnDespDealerOrigen: ", objComponent.get("v.blnDespDealerOrigen") );           		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpConsultaLeadDefault : function(objComponent, objEvent) {
		console.log("EN hpConsultaLeadDefault...");
		let stotCandCons = $A.get("$Label.c.TAM_TotRegConsReasigLeads");
		console.log("EN hpConsultaLeadDefault stotCandCons: " + stotCandCons);
		//Llama al metodo de getCandidatos
		this.getCandidatos(objComponent, objEvent, true, stotCandCons);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpBuscarCandidatos : function(objComponent, objEvent) {
		console.log("EN hpBuscarCandidatos...");
		let stotCandCons = $A.get("$Label.c.TAM_TotRegConsReasigLeads");
		//Llama al metodo de getCandidatos
		this.getCandidatos(objComponent, objEvent, false, stotCandCons);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	getCandidatos : function(objComponent, objEvent, defaultPrm, totCandCons) {
		console.log("EN getCandidatos...");
        //Llama la función 
		let objAction = objComponent.get("c.buscaCandidatos");
        objAction.setParams({
        	"sPropietario" : objComponent.get("v.sPropietario"),
        	"sDistribuidor" : objComponent.get("v.sDistribuidor"),
        	"strCadenaDeBusqueda" : objComponent.get("v.strCadenaDeBusqueda"),
        	"defaultPrm" : defaultPrm,
        	"totCandConsPrm" : totCandCons
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lCandidatosResult = objResponse.getReturnValue();
            	console.log("EN Helper.getCandidatos lCandidatosResult: ", lCandidatosResult);
       			objComponent.set("v.lCandidatos", lCandidatosResult);
            	console.log("EN Helper.getCandidatos lCandidatosResult.length: ", lCandidatosResult.length);
       			//Ve si la lista esta vacia para que inicialices el campo de blnNoTieneReg
       			if (lCandidatosResult.length == 0)
       				objComponent.set("v.blnNoTieneReg", true);
       			if (lCandidatosResult.length > 0)
       				objComponent.set("v.blnNoTieneReg", false);       			
       			console.log("EN getCandidatos lCandidatos: ", objComponent.get("v.lCandidatos"));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	hpConfirmaReasignacion : function(objComponent, objEvent) {
		console.log("EN hpConfirmaReasignacion...");
		let lCandidSelecPaso = [];		
		
        //Llama la función 
		let objAction = objComponent.get("c.tamConfirmaReasignacion");
        objAction.setParams({
        	"sPropietarioFinalPrm" : objComponent.get("v.sPropietarioFinal"),
        	"lCandidSelecPrm" : objComponent.get("v.lCandidSelec")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpResActResult = objResponse.getReturnValue();
            	if (objWrpResActResult.blnEstatus == false){
            		objComponent.set("v.blnSelec", true);
					objComponent.set("v.sPropietario", '57999');
					objComponent.set("v.sDistribuidor", '57999');
					objComponent.set("v.sPropietarioFinal", '57999');
					objComponent.set("v.lCandidatos", lCandidSelecPaso);
					objComponent.set("v.lCandidSelec", lCandidSelecPaso);
            		this.showToastSuccess(objComponent, objEvent, objWrpResActResult.strDetalle);            	
            	}//Fin si objWrpResActResult.blnEstatus == false
            	if (objWrpResActResult.blnEstatus == true)
            		this.showToastError(objComponent, objEvent, objWrpResActResult.strDetalle);            	
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


    //Funcion que sirve para seleccionar el tipo de Excepción
    hpSelecPropietario: function(objComponent, objEvent) {
    	console.log('EN Save hpSelecPropietario...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sPropietario",value);
		objComponent.set("v.sDistribuidor", '57999');
		objComponent.set("v.sPropietarioFinal", '57999');
		objComponent.set("v.strCadenaDeBusqueda", null);
        console.log('EN Save hpSelecPropietario sPropietario: ', objComponent.get("v.sPropietario"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpSelecDistribuidor: function(objComponent, objEvent) {
    	console.log('EN Save hpSelecDistribuidor...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sDistribuidor",value);
		objComponent.set("v.sPropietario", '57999');
		objComponent.set("v.sPropietarioFinal", '57999');
		objComponent.set("v.strCadenaDeBusqueda", null);
        console.log('EN Save hpSelecDistribuidor sDistribuidor: ', objComponent.get("v.sDistribuidor"));
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpActTexto: function(objComponent, objEvent) {
    	console.log('EN Save hpActTexto...');
        let strCadenaDeBusquedaPaso = objComponent.get("v.strCadenaDeBusqueda");
        objComponent.set("v.sDistribuidor",'57999');
		objComponent.set("v.sPropietario", '57999');
		objComponent.set("v.sPropietarioFinal", '57999');
        console.log('EN Save hpActTexto strCadenaDeBusquedaPaso: ', strCadenaDeBusquedaPaso);
    },

    //Funcion que sirve para seleccionar el tipo de Excepción
    hpSelecNvoPropie: function(objComponent, objEvent) {
    	console.log('EN Save hpSelecNvoPropie...');
        let value = objEvent.getSource().get("v.value");
        objComponent.set("v.sPropietarioFinal",value);
        console.log('EN Save hpSelecNvoPropie sPropietario: ', objComponent.get("v.sPropietarioFinal"));
    },

    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.lCandidatos");
        var reverse = sortDirection !== 'asc';

        data = Object.assign([],
            data.sort(this.sortBy(fieldName, reverse ? -1 : 1))
        );
        cmp.set("v.lCandidatos", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ? function(x) {
        	return primer(x[field]);
        } : function(x) {
          return x[field];
        };

        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1500,
            "type": "error"
        });
        toastEvent.fire();
    },

})