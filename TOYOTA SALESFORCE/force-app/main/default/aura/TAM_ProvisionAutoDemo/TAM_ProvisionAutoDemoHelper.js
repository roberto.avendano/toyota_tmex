({
    /** Función Inicial */
    initializeComponent : function(objComponent, objEvent){
        objComponent.set('v.loaded', true);
        var objAction = objComponent.get("c.getAutosDemo");
        objAction.setParams({ recordId : objComponent.get("v.recordId") });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0){
                    lstResult.sort();
                    objComponent.set('v.lstDemos', lstResult);
                    objComponent.set('v.lstDemos_AUX', lstResult);
                    
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', true);
                } else {
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', false);
                } 
            }
        });
        $A.enqueueAction(objAction);
    },

    /* Buscador de VIN */
    SearchHelper: function(objComponent, objEvent) {
        var SearchKey = objComponent.get("v.SearchKey");
        var lstRetailSinReversa = objComponent.get("v.lstDemos");
        var PagList = [];
        lstRetailSinReversa.forEach(function(objSinReversa) {
            var strVINRecord = objSinReversa.TAM_VIN__r.Name;
            if(strVINRecord.includes(SearchKey)){
                PagList.push(objSinReversa);
            }
        });
        objComponent.set('v.lstDemos_AUX', PagList);
    },

    /* Buscador de Dealer */
    SearchHelper_Dealer: function(objComponent, objEvent) {
        var SearchKey = objComponent.get("v.search_dealer");
        var lstRetailSinReversa = objComponent.get("v.lstDemos");
        var PagList = [];
        lstRetailSinReversa.forEach(function(objSinReversa) {
            var strRecord = objSinReversa.TAM_CodigoDealer__c;
            if(strRecord.includes(SearchKey)){
                PagList.push(objSinReversa);
            }
        });
        
        objComponent.set('v.lstDemos_AUX', PagList);
    },
    
    /*Guardar Provisión*/
    SaveList : function(objComponent, objEvent){
        var lstAutosDemo = objComponent.get('v.lstDemos_AUX');
        var objAction = objComponent.get("c.setRecordAutoDemo");
        objAction.setParams(
        		{   			
                    lstDetalleProvision : lstAutosDemo,
                    recordId : objComponent.get("v.recordId")
        		}
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0 ){
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);
    },
    
    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Provisión de Autos Demo guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },

     /*cerrar Provisión*/
     cerrarProvision : function(objComponent, objEvent){
        
        var lstAutoDemo = objComponent.get('v.lstDemos_AUX');
        var objAction = objComponent.get("c.cerrarProvisionAutoDemo");
        objAction.setParams(
                {   			
                    lstDetalleProvision : lstAutoDemo,
                    recordId : objComponent.get("v.recordId")
                }
        );
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var lstResult = objResponse.getReturnValue();
                if(lstResult.length > 0 ){
                    this.showToastSuccess_Cerrar(objComponent, objEvent);
                } else {
                    this.showToastError_Cerrar(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);
    },

     /** Toast Success */
     showToastSuccess_Cerrar : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Provisión de Auto Demo Cerrada Correctamente.",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError_Cerrar : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el cierre de provisión, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }
    
})