({

	initializeComponent : function(component, event) {
		console.log("EN Helper.initializeComponent...");

        component.set('v.columns', [
            { label: 'VIN', fieldName: 'sVin', type: 'text', fixedWidth: 185},
            { label: 'Apartado', fieldName: 'sApartado', type: 'Boolean'},
            { label: 'Serie', fieldName: 'sSerie', type: 'text'},
            { label: 'Versión', fieldName: 'sVersion', type: 'text'},
            { label: 'Modelo', fieldName: 'sModelo', type: 'text'},
            { label: 'Año', fieldName: 'sAnio', type: 'text'},
            { label: 'Col. Ext.', fieldName: 'sColExt', type: 'text'},
            { label: 'Col. Int.', fieldName: 'sColInt', type: 'text'},
            { label: 'Días', fieldName: 'sDias', type: 'text'},
            { label: 'Tipo', fieldName: 'sTipo', type: 'text'}      
        ]);
     
        //Llama la función 
		let objAction = component.get("c.getLeadInventario");
        objAction.setParams({   			
        	"recordId" : component.get("v.recordId")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lVinesSeleProspFactResult = objResponse.getReturnValue();
            	component.set("v.lVinesSeleProspFact", lVinesSeleProspFactResult);
		        //Ya tienes los datos del nuevo lead Inv que se acaba de insertar buscalo en lLedInvExist
		    	for(var a in lVinesSeleProspFactResult){
		    		if (lVinesSeleProspFactResult[a].bCerrada === true){
		    			component.set("v.blnCerrada", true);
		    			break;
		    		}//Fin si 
		    	}//Fin del for para lstWrpDistPaso
		    	//Valida el perfi del usuario	
            	this.getProfileUser(component, event);
		    	//Comsulta el estatus del candidato
            	this.ValidaEstatusLead(component, event);		    	
            	console.log("EN Helper.initializeComponent lVinesSeleProspFactResult: ", JSON.stringify(lVinesSeleProspFactResult));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
		
	},

   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	getProfileUser : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN getProfileUser...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getProfileuser");
        objAction.setParams({
        	recordId : strIdCandidato
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let blnManagerResult = objResponse.getReturnValue();
           		console.log("EN getProfileUser blnManagerResult: ", blnManagerResult );
            	objComponent.set("v.blnUsrGestor", blnManagerResult);
           		console.log("EN TipoOfertaFact blnUsrGestor: ", objComponent.get("v.blnUsrGestor") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },


   /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	ValidaEstatusLead : function(objComponent, objEvent, objDatosCandidatoPaso) {
		console.log("EN PermiteConsultarDatosCteDealer...");				                
        let strIdCandidato = objComponent.get("v.recordId");
        //Llama la función 
		let objAction = objComponent.get("c.getEstatusLead");
        objAction.setParams({
        	recordId : strIdCandidato
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let strEstatusLeadResult = objResponse.getReturnValue();
           		console.log("EN TipoOfertaFact sTipoOfertaFact: ", strEstatusLeadResult );
            	if (strEstatusLeadResult != 'Pedido en Proceso'){
            		objComponent.set("v.blnPedidoProceso", true);
            	}
           		console.log("EN TipoOfertaFact blnPedidoProceso: ", objComponent.get("v.blnPedidoProceso") );
           		console.log("EN TipoOfertaFact blnConsultaCtesDealer: ", objComponent.get("v.blnConsultaCtesDealer") );
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },
	
	handleOnSuccess : function(component, event) {
		console.log("EN Helper.handleOnSuccess...");

        let param = event.getParams(); //get event params
        let fields = param.response.fields; //get all field info
        let ledInvNewId = param.response.id; //get record id
        let strIdCandidato = component.get("v.recordId");
        //let sInventarioVehiculosFyG = fields.TAM_InventarioVehiculosFyG__c.value; //get TAM_InventarioVehiculosFyG__c
       	/*console.log("EN Helper.handleOnSuccess Param - ", param);
        console.log('EN Helper.handleOnSuccess Fields: ', JSON.stringify(fields)); 
        console.log('EN Helper.handleOnSuccess ledInvNewId: ', ledInvNewId); 
        console.log('EN Helper.handleOnSuccess strIdCandidato: ', strIdCandidato); */
        
        //Manda llamar la consulta para el nuevo registro creado
		let objAction = component.get("c.consultaLeadInventario");
        objAction.setParams({   			
        	"recordId" : strIdCandidato,
        	"strIdledInvNewId" : ledInvNewId
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {            
                let objWrpResAct = objResponse.getReturnValue();
			    console.log('EN Helper.handleOnSuccess objWrpResAct: ', objWrpResAct); 
                if (objWrpResAct.blnEstatus == false){
                    //this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
	            	let sInventarioVehiculosFyGResult = objResponse.getReturnValue();
	            	let sInventarioVehiculosFyG = sInventarioVehiculosFyGResult; 
			        console.log('EN Helper.handleOnSuccess sInventarioVehiculosFyG Final: ', sInventarioVehiculosFyG); 
			        //Actualiza el lead recien creado
			        this.updateLeadInventario(component, event, ledInvNewId, sInventarioVehiculosFyG);
                }//Fin si objWrpResAct.blnEstatus == false
                if (objWrpResAct.blnEstatus == true)
                    this.showToastError(component, event, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
        
	},
	
	updateLeadInventario : function(component, event, ledInvNewId, sInventarioVehiculosFyG) {
		let cntTotRegs = 0;	
		let strIdCandidato = component.get("v.recordId");

		console.log("EN Helper.updateLeadInventario ledInvNewId: " + ledInvNewId);
		console.log("EN Helper.updateLeadInventario sInventarioVehiculosFyG: " + sInventarioVehiculosFyG);
		console.log("EN Helper.updateLeadInventario strIdCandidato: " + strIdCandidato);
		
        //Llama la función 
		let objAction = component.get("c.updateLeadInventario");
        objAction.setParams({   			
        	"recordId" : component.get("v.recordId"),
        	"strIdledInvNewId" : ledInvNewId
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lVinesSeleProspFactResult = objResponse.getReturnValue();
            	component.set("v.lVinesSeleProspFact", lVinesSeleProspFactResult);
            	let lLedInvExist = lVinesSeleProspFactResult; //get event params
            	
		        //Ya tienes los datos del nuevo lead Inv que se acaba de insertar buscalo en lLedInvExist
		    	for(var a in lLedInvExist){
		    		console.log("EN Helper.updateLeadInventario lLedInvExist[a].TAM_Prospecto__c: " + lLedInvExist[a].LeadInventario.TAM_Prospecto__c + ' strIdCandidato: ' + strIdCandidato + ' lLedInvExist[a].TAM_InventarioVehiculosFyG__c: ' + lLedInvExist[a].LeadInventario.TAM_InventarioVehiculosFyG__c + ' sInventarioVehiculosFyG: ' + sInventarioVehiculosFyG);		    	
		    		if (lLedInvExist[a].LeadInventario.TAM_Prospecto__c == strIdCandidato && lLedInvExist[a].LeadInventario.TAM_InventarioVehiculosFyG__c == sInventarioVehiculosFyG )
		    			cntTotRegs++;
		    	}//Fin del for para lstWrpDistPaso
				console.log("EN Helper.updateLeadInventario cntTotRegs: ", cntTotRegs);
				
				//Hay mas de un reg elimina el que se acaba de crear ledInvNewId
		    	if (cntTotRegs > 1){
		    		this.deleteLeadInventario(component, event, ledInvNewId);
		    		this.showToastError(component, event, 'Este VIN ya lo seleccionaste.');
		    	}//Fin si cntTotRegs > 1
		    	
				//Hay mas de un reg elimina el que se acaba de crear ledInvNewId
		    	if (cntTotRegs === 1 || cntTotRegs === 0)
		    		this.showToastSuccess(component, event, 'El VIN fue agregado con exito.');
		       	console.log("EN Helper.updateLeadInventario lVinesSeleProspFactResult: ", JSON.stringify(component.get("v.lVinesSeleProspFact")));            	
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
		
	},

	deleteLeadInventario : function(component, event, ledInvNewId) {
		console.log("EN Helper.deleteLeadInventario ledInvNewId: " + ledInvNewId);

        //Llama la función 
		let objAction = component.get("c.deleteLeadInventario");
        objAction.setParams({   			
        	"recordId" : component.get("v.recordId"),
        	"strIdledInvNewId" : ledInvNewId
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lVinesSeleProspFactResult = objResponse.getReturnValue();
            	component.set("v.lVinesSeleProspFact", lVinesSeleProspFactResult);
            	console.log("EN Helper.deleteLeadInventario lVinesSeleProspFactResult: ", JSON.stringify(lVinesSeleProspFactResult));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
		
	},

	eliminaVinSel : function(component, event) {
		console.log("EN Helper.eliminaVinSel");

        //Llama la función 
		let objAction = component.get("c.deleteLeadInventarioSel");
        objAction.setParams({
        	"recordId" : component.get("v.recordId"),        
        	"lVinesSeleProspFactDel" : component.get("v.lVinesSeleProspFactDel")
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let lVinesSeleProspFactResult = objResponse.getReturnValue();
            	component.set("v.lVinesSeleProspFact", lVinesSeleProspFactResult);
            	this.showToastSuccess(component, event, 'Los registros fueron eliminados.');
            	console.log("EN Helper.eliminaVinSel lVinesSeleProspFactResult: ", JSON.stringify(lVinesSeleProspFactResult));
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
		
	},

	hpBuscaVinEnDD : function(component, event) {
		console.log("EN Helper.hpBuscaVinEnDD");
		let lVinesSeleProspFactPaso = component.get("v.lVinesSeleProspFact");
        let sstrVinBusqPaso = component.get("v.sstrVinBusq");
		console.log("EN Contoller.hpBuscaVinEnDD sstrVinBusqPaso: " + sstrVinBusqPaso);		
		let strMsgSalida = '';
		let strMsgSalidaFinal = '';
		
        //Llama la función 
		let objAction = component.get("c.buscaVinDD");
        objAction.setParams({
        	"recordId" : component.get("v.recordId"),        
        	"sstrVinBusqPrm" : sstrVinBusqPaso
   		});		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
            	let objWrpInventarioFyGResult = objResponse.getReturnValue();
            	console.log("EN Contoller.hpBuscaVinEnDD objWrpInventarioFyGResult: ", objWrpInventarioFyGResult);		
            	if (objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && !objWrpInventarioFyGResult.blnExisteDDMismoDealer
            		&& !objWrpInventarioFyGResult.blnExisteDDLeadInventDifDealer){
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 1...");
            		strMsgSalidaFinal = 'El VIN: ' + sstrVinBusqPaso + ' existe en DD, tiene una venta asociada pero es de otro Distribuidor: ' + objWrpInventarioFyGResult.sCodDistFinal;            	
            	}//Fin si objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && objWrpInventarioFyGResult.blnExisteDDMismoDealer
            	if (objWrpInventarioFyGResult.blnExisteDD && !objWrpInventarioFyGResult.blnExisteDDNoVta && !objWrpInventarioFyGResult.blnExisteDDMismoDealer){
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 2...");
            		strMsgSalidaFinal = 'El VIN: ' + sstrVinBusqPaso + ' existe en DD, pero no tiene una venta asociada';            	
            	}//Fin si objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && objWrpInventarioFyGResult.blnExisteDDMismoDealer
            	if (!objWrpInventarioFyGResult.blnExisteDD && !objWrpInventarioFyGResult.blnExisteDDNoVta && !objWrpInventarioFyGResult.blnExisteDDMismoDealer){
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 3...");
            		strMsgSalidaFinal = 'El VIN: ' + sstrVinBusqPaso + ' no tiene una venta asociada en DD';
            	}//Fin si objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && objWrpInventarioFyGResult.blnExisteDDMismoDealer
            	if (objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && objWrpInventarioFyGResult.blnExisteDDMismoDealer
            		&& objWrpInventarioFyGResult.blnExisteDDLeadInventDifDealer){
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 4...");
            		strMsgSalidaFinal = 'El VIN: ' + sstrVinBusqPaso + ' existe en DD, y esta apartado por el Distribuidor: ' + objWrpInventarioFyGResult.sCodDistFinal + ' y el usuario: ' + objWrpInventarioFyGResult.strNombreUserActual;            	
            	}//Fin si objWrpInventarioFyGResult.blnExisteDD && objWrpInventarioFyGResult.blnExisteDDNoVta && objWrpInventarioFyGResult.blnExisteDDMismoDealer
            	console.log("EN Contoller.hpBuscaVinEnDD strMsgSalidaFinal: " + strMsgSalidaFinal + ' length : ' + strMsgSalidaFinal.length );		
            	            	
            	//Paso las validaciones 
            	if (strMsgSalidaFinal === ''){
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 5 " + sstrVinBusqPaso);
            		//Recorre la lista de vines que ya estan selecionados 
			    	for(var a in lVinesSeleProspFactPaso){
			    		console.log("EN Helper.updateLeadInventario lLedInvExist[a].sVin: " + lVinesSeleProspFactPaso[a].sVin + ' sstrVinBusqPaso: ' + sstrVinBusqPaso);		    	
			    		if (lVinesSeleProspFactPaso[a].sVin == sstrVinBusqPaso){
			    			strMsgSalidaFinal = 'El VIN: ' + sstrVinBusqPaso + ' ya esta seleccionado en la lista actual.';   
			    			break;         	
			    		}//Fin si lVinesSeleProspFactPaso[a].sVin == sstrVinBusqPaso
			    	}//Fin del for para lstWrpDistPaso
            	}//Fin si strMsgSalidaFinal === ''

            	//No pasao las validaciones
            	if (strMsgSalidaFinal.length > 0){
            		this.showToastWarning(component, event, strMsgSalidaFinal);            	
            	}//Fin si strMsgSalidaFinal.length > 0

            	//Paso las validaciones 
            	if (strMsgSalidaFinal === ''){
            		//Agrega el objeto objWrpInventarioFyGResult a la lista de lVinesSeleProspFactPaso
            		lVinesSeleProspFactPaso.push(objWrpInventarioFyGResult);
            		component.set('v.lVinesSeleProspFact', lVinesSeleProspFactPaso); 
            		console.log("EN Contoller.hpBuscaVinEnDD VAL 6 " + component.get("v.lVinesSeleProspFact"));
            		this.showToastSuccess(component, event, 'El Vin: ' + sstrVinBusqPaso + ' fue agregado a la lista');            	
			        //Recarga la pagina
			        location.reload();
			        // Close the action panel
			        var dismissActionPanel = $A.get("e.force:closeQuickAction");
			        dismissActionPanel.fire();
            	}//Fin si strMsgSalidaFinal === ''
            			
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction); 
		
	},




    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 1000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastWarning : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Advertencia.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1000,
            "type": "warning"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1000,
            "type": "error"
        });
        toastEvent.fire();
    },

	
})