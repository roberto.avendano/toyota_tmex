({
 
     /** Funcion Inicial */
	doInit : function(component, event, helper) {
		helper.initializeComponent(component, event);
    },
 
    handleOnSuccess : function(component, event, helper) {
		helper.handleOnSuccess(component, event);
    },

    jsBuscaVinEnDD : function(component, event, helper) {
		helper.hpBuscaVinEnDD(component, event);
    },

    //Evento para el componente de busqueda
    keyPressController : function(component, event, helper) {  
		console.log("EN Contoller.keyPressController...");    
        let getInputkeyWord = component.get("v.sstrVinBusq");
		console.log("EN Contoller.keyPressController getInputkeyWord: ", getInputkeyWord);
        let srtValorPaso = event.getSource().get("v.value");
		console.log("EN Contoller.keyPressController srtValorPaso: ", srtValorPaso);				
    },

    //Evento para el componente de busqueda
    jsBuscaVinEnDD : function(component, event, helper) {  
		console.log("EN Contoller.jsBuscaVinEnDD...");
		
		let lblExiste = false;
		let strMsgError = '';
        let sstrVinBusqPaso = component.get("v.sstrVinBusq");
		console.log("EN Contoller.jsBuscaVinEnDD sstrVinBusqPaso: " + sstrVinBusqPaso);		
		//Ve si tiene algo entonces busca el vin en la lista de 
		let lVinesSeleProspFactPasi = component.get("v.lVinesSeleProspFact");
		console.log("EN Contoller.jsBuscaVinEnDD lVinesSeleProspFactPasi: ", lVinesSeleProspFactPasi);
		
		//Ve si capturo algo en srtValorPaso
		if (sstrVinBusqPaso.length > 0){
			//Busca el vin en la lita de lVinesSeleProspFactPasi en el campo de VON
	    	for(var a in lVinesSeleProspFactPasi){
	    		console.log("EN Contoller.jsBuscaVinEnDD lVinesSeleProspFactPasi[a].sVin: " + lVinesSeleProspFactPasi[a].sVin + ' sstrVinBusqPaso: ' + sstrVinBusqPaso);		    	
	    		if (lVinesSeleProspFactPasi[a].sVin == sstrVinBusqPaso){
	    			console.log("EN Contoller.jsBuscaVinEnDD YA LO ENCONTRO sstrVinBusqPaso: ", sstrVinBusqPaso);
	    			strMsgError = 'El VIN: ' + 	sstrVinBusqPaso + ', ya esta seleccionado en la lista';
	    			lblExiste = true;
	    			break;
	    		}//Fin si lVinesSeleProspFactPasi[a].sVin == srtValorPaso
	    	}//Fin del for para lstWrpDistPaso
		} //Fin si srtValorPaso.size > 0
		
		//Ya existe en la lista de vines seleccionados despliega un error
		if (lblExiste)
           helper.showToastWarning(component, event, strMsgError);

		//No existe en la lista de vines seleccionados valida en Apex 
		if (!lblExiste)
           helper.hpBuscaVinEnDD(component, event);

    },

    updateSelectedText: function (component, event) {
        var selectedRows = event.getParam('selectedRows');
        component.set('v.lVinesSeleProspFactDel', selectedRows);        
        component.set('v.selectedRowsCount', selectedRows.length);
        let intSelectedRows = selectedRows.length;
       	console.log("EN Controller.updateSelectedText intSelectedRows: ", intSelectedRows);        
        if (intSelectedRows > 0){
        	component.set('v.blnConfirmaDatosFactDel', false);
        	console.log("EN Controller.updateSelectedText YA SELECCIONO ALGO intSelectedRows: ", intSelectedRows);
        }
        if (intSelectedRows === 0){
        	component.set('v.blnConfirmaDatosFactDel', true);        
        	console.log("EN Controller.updateSelectedText NO HA SELECCIONADO NADA intSelectedRows: ", intSelectedRows);        
        }
        	
       	console.log("EN Controller.updateSelectedText blnConfirmaDatosFactDel: ", component.get('v.blnConfirmaDatosFactDel'));        	
       	console.log("EN Controller.updateSelectedText selectedRows: ", JSON.stringify(selectedRows));
    },
    
    eliminaVin: function (component, event, helper) {
    	helper.eliminaVinSel(component, event);
    },
    
      
    onSuccess : function(component, event, helper) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been Saved successfully."
        });
        toastEvent.fire();
    },
    
    onSubmit : function(component, event, helper) {
    },
    
    onLoad : function(component, event, helper) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Loaded!",
            "message": "The record has been Loaded successfully ."
        });
        toastEvent.fire();
    },
    
    onError : function(component, event, helper) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": "Error."
        });
        toastEvent.fire();
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
        //Recarga la pagina
        location.reload();
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },    
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }    
    
})