({    
   
    handleShowModalEdit: function(component, event, helper) {
        var modalBody;
        var fila = component.get("v.fila");
        var indiceFila = component.get("v.indiceFila");
        var User = component.get("v.User");
        
        console.log("modal2 fila indiceFila",fila,indiceFila);
        
        var optAtributos = [];
        var optAtributosUnicos = [];
        var optAtributosSelect = [];
        var noAplica = false;
        if(fila.atributos.length > 0){
        	var opciones = fila.atributos[0].EspecificacionAtributo__r.Opciones__c; 
			if(!$A.util.isUndefined(opciones) && !$A.util.isEmpty(opciones)){
				for(var i = 0 ; i < fila.atributos.length ; i++){
					optAtributos.push(fila.atributos[i].Atributo__c);
				}
				var catOpciones =  opciones.split("|");
				for(var i = 0; i < catOpciones.length; i++) {
					optAtributos.push(catOpciones[i]);
				}
			}
			optAtributosUnicos = Array.from(new Set(optAtributos));
			for(var i = 0; i < optAtributosUnicos.length; i++) {
				optAtributosSelect.push({etiqueta:optAtributosUnicos[i],valor:optAtributosUnicos[i]});
			}
	        for(var i=0 ; i<fila.atributos.length ; i++){
	        	if(fila.atributos[i].NoAplica__c){
	        		noAplica = true;
	        		break;
	        	}
	        }
		}
        
        
        $A.createComponent(
        		"c:FichaProductoTablaCategoriaEdit", 
        		{
        			"fila": component.getReference("v.fila"),
        			"optAtributos": optAtributosSelect,
        			"indiceFila" : indiceFila,
        			"noAplica" : noAplica,
                    "User" : User
        		},
               function(content, status) {
                   if (status === "SUCCESS") {
                       modalBody = content;
                       component.find('overlayLib').showCustomModal({
                           header: "Editar Fila",
                           body: modalBody, 
                           showCloseButton: true,
                           cssClass: "mymodal",
                           closeCallback: function() {
                               console.log('$A.createComponent close', fila, indiceFila);
                               var cmpEvent = component.getEvent("updateFila");
                               var cmpHeaderEvent = component.getEvent("updateTotalesPreliminares");
                               
                               cmpEvent.setParams({
						            "fila" : fila,
						            "indiceFila" : indiceFila
						       });
						       cmpEvent.fire();
                               cmpHeaderEvent.fire();
                           }
                       }).then(function (overlay) {
                    	   component.set('v.overlayPanel', overlay);
                       });
                   }
               });
    }
})