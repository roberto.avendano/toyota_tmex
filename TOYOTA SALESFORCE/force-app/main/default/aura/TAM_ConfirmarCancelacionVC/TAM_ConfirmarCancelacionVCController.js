({
    doInit: function(component, event, helper) {
        helper.validarFechaCierreSolicitud(component, event, helper);
    },
    
    guardarRegistros: function(component, event, helper) {
        helper.enviarAprobacion(component, event, helper);
    },
    
	closeModel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();    
    }
})