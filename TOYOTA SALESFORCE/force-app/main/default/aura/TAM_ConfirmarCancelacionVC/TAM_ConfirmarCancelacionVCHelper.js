({
    validarFechaCierreSolicitud : function(component, event, helper) {
        let recordIdSolicitud = component.get("v.recordId");
        let action  = component.get("c.validaAprobacion");
        
        action.setParams(
            {  
                recordIdSolicitud        : recordIdSolicitud
            }
        );
        
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS"){
                let respuesta = response.getReturnValue();
                component.set("v.necesitaAprobacion",respuesta);
            }
        });
        $A.enqueueAction(action);  
        
    },
    
    enviarAprobacion : function(component, event, helper) {
        let recordIdSolicitud = component.get("v.recordId");
        let motivoCancelacion = component.get("v.comentario");
        let registrosCancelar = component.get("v.lstDataAux");
        let solicitud = component.get("v.solicitud");
        let nivelAprobacion = 'Nivel1'
        let bodyEmail = 'Estimado usuario tienes una solicitud de cancelación por aprobar';
        let necesitaAprobacion =  component.get("v.necesitaAprobacion");
        
        let action;
        
        if(necesitaAprobacion == true){
            action  = component.get("c.enviarSolicitudCancelacion");
            
            action.setParams(
                {  
                    bodyEmail        : bodyEmail,
                    nivelAprobacion   : nivelAprobacion,
                    recordIdSolicitud : recordIdSolicitud,
                    motivoCancelacion : motivoCancelacion,
                    registrosCancelar        : registrosCancelar,
                    solicitud  : solicitud
                }
            );
            
        }
        
        if(necesitaAprobacion == false){
            action  = component.get("c.guardarAutomaticamente");
            
            action.setParams(
                {  
                    recordIdSolicitud        : recordIdSolicitud,
                    registrosCancelar   : registrosCancelar
                    
                }
            );
            
            
        }
        
        
        action.setCallback(this, function(response){
            let state = response.getState();
            if (state === "SUCCESS"){
                let respuesta = response.getReturnValue();
                this.showToast(component, event, helper);
                this.closeModel(component, event, helper);
                //Se cierra la quick action
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
        });
        $A.enqueueAction(action);  
    },
    
    
    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso!",
            "message": "La solicitud se guardo correctamente."
        });
        toastEvent.fire();
    },
    
    closeModel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();    
    }
})