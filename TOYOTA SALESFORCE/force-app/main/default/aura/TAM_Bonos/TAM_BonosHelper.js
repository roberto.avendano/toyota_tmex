({
	/* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
        var objAction = objComponent.get("c.getDetalleBonos");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                }
                if(arrayMapKeys.length == 0){
                   objComponent.set("v.disponibleEnInventario", false); 
                }
                
                arrayMapKeys.reverse();
                objComponent.set("v.mapValues", arrayMapKeys);
                this.getSeries(objComponent, objEvent);
                this.getCurrentUser(objComponent, objEvent);
            }else {
                objComponent.set("v.disponibleEnInventario", false);
            }
        });
        $A.enqueueAction(objAction);
    },
    
    /** Obtener mapa de años y series */
    getSeries : function(objComponent, objEvent) {
        var objAction = objComponent.get("c.getSeries");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                var arrayMapKeys = [];
                for(var key in result){
                    arrayMapKeys.push({key: key, value: result[key]});
                }
                if(arrayMapKeys.length > 0){
                   objComponent.set("v.loaded", true); 
                }
                arrayMapKeys.reverse();
                objComponent.set("v.mapAnioSerie", arrayMapKeys);
            }
        });
        $A.enqueueAction(objAction);
    },

    /** Obtener Current User */
    getCurrentUser : function(objComponent, objEvent) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var objAction = objComponent.get("c.getCurrentUser");
        objAction.setParams({  userId :userId  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
                if(result){
                    objComponent.set("v.boolUsuarioSoloLectura", true);
                }
            }
        });
        $A.enqueueAction(objAction);
    }
})