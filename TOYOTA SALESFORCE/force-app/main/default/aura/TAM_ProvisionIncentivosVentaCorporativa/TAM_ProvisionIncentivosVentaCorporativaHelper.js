({
	
	/** Función Inicial */
    initializeComponent : function(objComponent, objEvent){
        objComponent.set('v.loaded', true);
        let objAction = objComponent.get("c.getRetailFlotilla");
        objAction.setParams({ recordId : objComponent.get("v.recordId") });
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                let lstResult = objResponse.getReturnValue();                
                if(lstResult.length > 0){
                    lstResult.sort();
                    objComponent.set('v.lstFlotilla', lstResult);
                    //objComponent.set('v.lstFlotilla_AUX', lstResult);
                    
                    /**Paginación */
                    var pageList = [];
                    lstResult.forEach(function(objResult){
                        pageList.push(objResult);
                    });
                    var pageSize = objComponent.get("v.pageSize");
                    var totalRecordsList = pageList;
                    var totalLenght = totalRecordsList.length;

                    objComponent.set("v.totalRecordsCount", totalLenght);
                    objComponent.set("v.startPage", 0);
                    objComponent.set("v.endPage", pageSize - 1);

                    var paginationList = [];
                    for(var i=0; i<pageSize; i++){
                        if(pageList.length > i){
                            paginationList.push(pageList[i]);
                        }
                    }
                    objComponent.set("v.lstFlotilla_AUX", paginationList);
                    objComponent.set("v.selectedCount", 0);
                    objComponent.set("v.totalPagesCount", Math.ceil(totalLenght / pageSize));


                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', true);
                    
                    //Recorre la lista de reg y ve si ya esta cerrada
                    for (let Vin in lstResult){
                        
                        //Ve si ya esta cerrada en el campo de TAM_Cerrada__c
                        console.log("EN Helper.SaveCerrarProvision TAM_Cerrada__c: " + lstResult[Vin].TAM_Cerrada__c);
                        if (lstResult[Vin].TAM_Cerrada__c){
                        	objComponent.set('v.bCerrada', true);
                        	break;
                        }//Fin si lstResult[Vin].TAM_Cerrada__c
                    }//Fin del for para la lista de lstResult
                } else {
                    objComponent.set('v.loaded', false);
                    objComponent.set('v.boolVentas', false);
                }
            }
        });
        $A.enqueueAction(objAction);
    },

    /* Buscador de VIN */
    SearchHelper: function(objComponent, objEvent) {
        let SearchKey = objComponent.get("v.SearchKey");
        let lstRetailConReversa = objComponent.get("v.lstFlotilla");
        let PagList = [];
        lstRetailConReversa.forEach(function(objConReversa) {
            let strVINRecord = objConReversa.TAM_VIN__r.Name;
            if(strVINRecord.includes(SearchKey)){
                PagList.push(objConReversa);
            }
        });
        objComponent.set('v.lstFlotilla_AUX', PagList);
    },

    /* Buscador de Dealer */
    SearchHelper_Dealer: function(objComponent, objEvent) {
        var SearchKey = objComponent.get("v.search_dealer");
        var lstRetailSinReversa = objComponent.get("v.lstFlotilla");
        var PagList = [];
        lstRetailSinReversa.forEach(function(objSinReversa) {
            var strRecord = objSinReversa.TAM_CodigoDealer__c;
            if(strRecord.includes(SearchKey)){
                PagList.push(objSinReversa);
            }
        });
        
        objComponent.set('v.lstFlotilla_AUX', PagList);
    },
    
	/** Guardar */
    SaveProvision : function(objComponent, objEvent){
        objComponent.set('v.loaded2', true);
        let lstFlotillaPaso = objComponent.get('v.lstFlotilla_AUX');        
        let objAction = objComponent.get("c.saveProvision");
        objAction.setParams(
        	{ 
        		recordId : objComponent.get("v.recordId"),
                lstDetalleProvision : lstFlotillaPaso        		 
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                let lstResult = objResponse.getReturnValue();                
                if(lstResult.length > 0 ){
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastError(objComponent, objEvent); 
                }
            }
        });
        $A.enqueueAction(objAction);
    },

	/** Cerrar */
    SaveCerrarProvision : function(objComponent, objEvent){
        objComponent.set('v.loaded2', true);
        let lstFlotillaPaso = objComponent.get('v.lstFlotilla_AUX');        
        let objAction = objComponent.get("c.saveCerrarProvision");
        objAction.setParams(
        	{ 
        		recordId : objComponent.get("v.recordId"),
                lstDetalleProvision : lstFlotillaPaso        		 
        	}
        );
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var boolResult = objResponse.getReturnValue();
                if(boolResult){
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastSuccess(objComponent, objEvent);
                } else {
                    setTimeout(function(){
                        objComponent.set('v.loaded2', false);
                    }, 4000);
                    this.showToastError(objComponent, objEvent); 
                }  
            } 
        });
        $A.enqueueAction(objAction);
    },

    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Provisión de Incentivos Venta Corporativa guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },

    /** Navegar a la sig pagina */
    next : function(objComponent, objEvent, sObjectList, end, start, pageSize){
        var sObjectList = objComponent.get("v.lstFlotilla");
        var sObjectLis_AUX = objComponent.get("v.lstFlotilla_AUX");
        var end = objComponent.get("v.endPage");
        var start = objComponent.get("v.startPage");
        var pageSize = objComponent.get("v.pageSize");
        var pageList=[];
        var counter = 0;

       
        for(var i = end + 1; i < end + pageSize + 1; i++){
            if(sObjectList.length > i){
                pageList.push(sObjectList[i]);
                
            }
            counter ++;
        }

        start = start + counter;
        end = end + counter;
        objComponent.set("v.startPage", start);
        objComponent.set("v.endPage", end);
        objComponent.set("v.lstFlotilla_AUX", pageList);
        
    },

    /** Navegar a la  pagina anterior*/
    previous : function(objComponent, objEvent, sObjectList, end, start, pageSize){
        var sObjectList = objComponent.get("v.lstFlotilla");
        var sObjectLis_AUX = objComponent.get("v.lstFlotilla_AUX");
        var end = objComponent.get("v.endPage");
        var start = objComponent.get("v.startPage");
        var pageSize = objComponent.get("v.pageSize");
        var pageList=[];
        var counter = 0;

        console.log(start);
        console.log(end);
        console.log(pageSize);

        var pageListAll_AUX = objComponent.get("v.lstFlotilla"); // = sObjectList.splice(start,pageSize);
        var n = start;
        sObjectLis_AUX.forEach(function(objResult){
            pageListAll_AUX.splice(n, 1, objResult);
            n++;
        });
        objComponent.set("v.lstFlotilla", pageListAll_AUX);

        

        for(var i = start - pageSize; i < start ; i++){
            if(i > -1){
                pageList.push(sObjectList[i]);
                counter ++;
            } else{
                start ++;
            }
            
        }

        start = start - counter;
        end = end - counter;
        objComponent.set("v.startPage", start);
        objComponent.set("v.endPage", end);
        objComponent.set("v.lstFlotilla_AUX", pageList);
    },

    
})