({
	doInit : function(component,event,helper){ 
		console.log("EN Comntroller.doInit doInit..."); 
		//debugger;
		//Consulta los datos del reistro y ve de que tipo de cliente se trata
		helper.getTipoPersona(component);
		helper.getDatosCandidato(component);
    },
    
	enviarSolicitudAproba : function(component,event,helper){ 
		console.log("EN Comntroller.enviarSolicitudAproba..."); 
		//debugger;
		//Envia los datos del cliente pra que pueda hacer la solicitud de autorización
		helper.enviarSolicitudAproba(component);
        component.find('notifLibSol').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "La solicitud se envio de forma correcta!!!",
            closeCallback: function() {}
        });    	          		
    },

	enviarSolicitudPreAutoriza : function(component,event,helper){ 
		console.log("EN Comntroller.enviarSolicitudPreAutoriza..."); 
		//debugger;
		//Envia los datos del cliente pra que pueda hacer la solicitud de autorización
		helper.enviarSolicitudPreAuto(component);
        component.find('notifLibSol').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "La solicitud se envio de forma correcta!!!",
            closeCallback: function() {}
        });    	          		
    },

	enviarSolicitudActivacion : function(component,event,helper){ 
		console.log("EN Comntroller.enviarSolicitudActivacion..."); 
		//debugger;
		//Envia los datos del cliente pra que pueda hacer la solicitud de autorización
		helper.enviarSolicitudActiva(component);
        component.find('notifLibSol').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "La solicitud se envio de forma correcta!!!",
            closeCallback: function() {}
        });    	          		
    },

	actualizaDatos : function(component,event,helper){ 
		console.log("EN Comntroller.actualizaDatos..."); 
		//Consulta los datos del reistro y ve de que tipo de cliente se trata
		helper.getTipoPersona(component);
		helper.getDatosCandidato(component);
		//Para mostrar el mensaje que se esta actualizando la información...
		helper.showToastSuccess(component, event, "Los datos se actualizaron con exito...");
		
		/*component.find('notifLib').showNotice({
            "variant": "info",
            "header": "Success",
            "message": "Acualizando Información...",
            closeCallback: function() {}
        });*/    
    },

    closeQuickAction : function(objComponent, objEvent, objHelper) {        
      // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
})