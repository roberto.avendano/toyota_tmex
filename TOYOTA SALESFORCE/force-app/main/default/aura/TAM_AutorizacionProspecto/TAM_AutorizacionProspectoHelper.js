({
    
  getTipoPersona: function (component) {
	   console.log("EN Heler.getTipoPersona.recordId ", component.get("v.recordId"));
	   console.log("EN Heler.getTipoPersona.leadRecord ", component.get('v.leadRecord')); 
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.consultDatosCand");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){  
                var strTipoCand = response.getReturnValue();
                console.log("EN Heler. strTipoCand ", strTipoCand);
                
                if (strTipoCand == 'FlotillaMoral'){
                	component.set("v.bVentaFlotilla", true);
                	component.set("v.bVentaFlotillaArchivos", true);

                	component.set("v.bPreAutorizoPerMoral", false);                	
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bTieneAutoizacionProceso", false);
                }
                
                if (strTipoCand == 'FlotillaFisica'){
                	component.set("v.bVentaPrograma", true);
                	component.set("v.bVentaProgramaArchivos", true);

                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bPreAutorizoPerMoral", false);                	
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bTieneAutoizacionProceso", false);
                }
                
                if (strTipoCand == 'ProgramaFisica'){
                	component.set("v.bVentaPrograma", true);
                	component.set("v.bVentaProgramaArchivos", true);

                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bPreAutorizoPerMoral", false);                	
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bTieneAutoizacionProceso", false);                	
                }
                
                if (strTipoCand == 'ProgramaMoral'){
                	component.set("v.bVentaFlotilla", true);
                	component.set("v.bVentaFlotillaArchivos", true);

                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.bPreAutorizoPerMoral", false);                	
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bTieneAutoizacionProceso", false);                	
                }

                if (strTipoCand == 'ClienteNoVigenteMoral'){
                	component.set("v.bAutorizaNoVigPerMoral", true);
                	
                	//Ya apagas las bvanderas para que no se vaya a mistrar algo que no debe ser
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.ClienteNoVigenteFisico", false);
                }

                if (strTipoCand == 'ClienteNoVigenteFisico'){
                	component.set("v.bAutorizaNoVigPerFisica", true);
                	
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.ClienteNoVigenteMoral", false);
                }

                if (strTipoCand == 'ClientePreautorizado'){
                	component.set("v.bPreAutorizoPerMoral", true);
                	
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bTieneAutoizacionProceso", false);
                }

                if (strTipoCand == 'ClienteActivación'){
                	component.set("v.bActivacionPerMoral", true);
                	
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaFlotillaArchivos", false);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaProgramaArchivos", false);
                	component.set("v.ClienteNoVigenteMoral", false);
                	component.set("v.bAutorizaNoVigPerFisica", false);
                	component.set("v.bPreAutorizoPerMoral", false);
                	component.set("v.bTieneAutoizacionProceso", false);
                }
                
                //Inicializa la variable de bIniTipoVenta
                component.set("v.bIniTipoVenta", true);
                
                //Despliega los mensajes de log
                console.log("EN Heler.getTipoPersona.bVentaFlotilla ", component.get('v.bVentaFlotilla'));
                console.log("EN Heler.getTipoPersona.bVentaPrograma ", component.get('v.bVentaPrograma'));	
                console.log("EN Heler.getTipoPersona.bIniTipoVenta ", component.get('v.bIniTipoVenta'));	
                console.log("EN Heler.getTipoPersona.bAutorizaNoVigPerFisica ", component.get('v.bAutorizaNoVigPerFisica'));	
                console.log("EN Heler.getTipoPersona.bAutorizaNoVigPerMoral ", component.get('v.bAutorizaNoVigPerMoral'));	
                console.log("EN Heler.getTipoPersona.bPreAutorizoPerMoral ", component.get('v.bPreAutorizoPerMoral'));
            }  
        });  
        $A.enqueueAction(action);
    },

    getDatosCandidato: function (component) {
	   console.log("EN Heler.getDatosCandidato.recordId ", component.get("v.recordId"));
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.getDatosCandidato");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.getDatosCandidato.state ", state);             
            if(state=='SUCCESS'){  
                var wrpDatosCndidato = response.getReturnValue();
                component.set("v.wrpDatosCantidato", wrpDatosCndidato);
                component.set("v.leadRecord", wrpDatosCndidato.Candidato);
                component.set("v.AccountRecord", wrpDatosCndidato.Cliente);
                
                component.set("v.bTieneAutoizacionProceso", wrpDatosCndidato.Candidato.TAM_EnviarAutorizacion__c);
                console.log("EN Heler.getDatosCandidato SolicitaAutoriza: ", wrpDatosCndidato.Candidato.TAM_EnviarAutorizacion__c);
                component.set("v.bTieneAutoizacionProceso", wrpDatosCndidato.Cliente.TAM_EnviarPreautorizacion__c);
                console.log("EN Heler.getDatosCandidato SolicitaAutoriza2: ", wrpDatosCndidato.Cliente.TAM_EnviarPreautorizacion__c);
                component.set("v.SolicitaAutoriza", wrpDatosCndidato.SolicitudAprobar);
                
                component.set("v.bActCons", wrpDatosCndidato.Candidato.TAM_ActaConstitutiva__c);
                component.set("v.bCedFisc", wrpDatosCndidato.Candidato.TAM_CedulaFiscal__c);
                component.set("v.bOrdComp", wrpDatosCndidato.Candidato.TAM_OrdenCompraCartaCompromiso__c);
                component.set("v.bPdfDatAdic", wrpDatosCndidato.Candidato.TAM_PdfDatosAdicionalesRepLegal__c);

                component.set("v.bIdentOfic", wrpDatosCndidato.Candidato.TAM_IdentificacionOficial__c);
                component.set("v.bComppDomc", wrpDatosCndidato.Candidato.TAM_ComprobanteDomicilio__c);
                component.set("v.bDocmSopor", wrpDatosCndidato.Candidato.TAM_DocumentacionSoporte__c);
                
                console.log("EN Heler.getDatosCandidato wrpDatosCndidato: ", wrpDatosCndidato);
                let bPreAutorizoPerMoral = component.get("v.bPreAutorizoPerMoral");
                let bActivacionPerMoral =  component.get("v.bActivacionPerMoral");
                console.log("EN Heler.getDatosCandidato bPreAutorizoPerMoral: ", bPreAutorizoPerMoral);
                console.log("EN Heler.getDatosCandidato bActivacionPerMoral: ", bActivacionPerMoral);
               	component.set("v.bTieneAutoizacionProceso", false);
                
                if (bPreAutorizoPerMoral === true || bActivacionPerMoral === true)
                	component.set("v.bTieneAutoizacionProceso", true);

                console.log("EN Heler.getDatosCandidato wrpDatosCndidato.bSolicitudesAprobar: ", wrpDatosCndidato.bSolicitudesAprobar);
                //si ya tiene solicitudes enviadas
                if (wrpDatosCndidato.bSolicitudesAprobar)	
                	component.set("v.bTieneAutoizacionProceso", true);
                
                console.log("EN Heler.getDatosCandidato Candidato0000: ", wrpDatosCndidato.Candidato);
                console.log("EN Heler.getDatosCandidato TAM_EnviarAutorizacion__c: ", wrpDatosCndidato.Candidato.TAM_EnviarAutorizacion__c);
                console.log("EN Heler.getDatosCandidato SolicitudAprobar: ", wrpDatosCndidato.SolicitudAprobar);
                console.log("EN Heler.getDatosCandidato.bTieneAutoizacionProceso ", component.get('v.bTieneAutoizacionProceso'));
                
            }  
        });  
        $A.enqueueAction(action);
    },

    enviarSolicitudAproba: function (component) {
	   console.log("EN Heler.recordId ", component.get("v.recordId"));
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.enviaSolicitudAproba");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){
                component.set("v.bTieneAutoizacionProceso", true);
                console.log("EN Heler.enviarSolicitud bTieneAutoizacionProceso: ", component.get("v.bTieneAutoizacionProceso"));
            }  
        });  
        $A.enqueueAction(action);
    },

    enviarSolicitudPreAuto: function (component) {
	   console.log("EN Heler.recordId ", component.get("v.recordId"));
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.enviaSolicitudPreAuto");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){
                component.set("v.bTieneAutoizacionProceso", true);
                console.log("EN Heler.enviarSolicitud bTieneAutoizacionProceso: ", component.get("v.bTieneAutoizacionProceso"));
            }  
        });  
        $A.enqueueAction(action);
    },

    enviarSolicitudPreAuto: function (component) {
	   console.log("EN Heler.recordId ", component.get("v.recordId"));
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.enviaSolicitudPreAuto");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){
                component.set("v.bTieneAutoizacionProceso", true);
                console.log("EN Heler.enviarSolicitud bTieneAutoizacionProceso: ", component.get("v.bTieneAutoizacionProceso"));
            }  
        });  
        $A.enqueueAction(action);
    },
    
    enviarSolicitudActiva: function (component) {
	   console.log("EN Heler.recordId ", component.get("v.recordId"));
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.enviaSolicitudActiva");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){
                component.set("v.bTieneAutoizacionProceso", true);
                console.log("EN Heler.enviarSolicitud bTieneAutoizacionProceso: ", component.get("v.bTieneAutoizacionProceso"));
            }  
        });  
        $A.enqueueAction(action);
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje,
            "duration": 500,
            "type": "success"
        });
        toastEvent.fire();
    },
    
})