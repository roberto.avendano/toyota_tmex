({
	doInit : function(component, event, helper) {
		var mapa = component.get("v.coloresSeleccionadosMap");
		var intColorKey = component.get("v.colorInt");
		var extColorKey = component.get("v.colorExt");
		
		var colorInterior = mapa[intColorKey];
		var colorSelect = colorInterior[extColorKey];

		//console.log(colorSelect);
		component.set("v.colorSeleccionado" ,colorSelect);
	},

	updateSelectColor : function(component, event, helper) {
		helper.showSpinner(component);

		var selectedColor = component.get("v.colorSeleccionado");
			selectedColor.Agregado__c = false;
			selectedColor.DPMS__c = false;
            selectedColor.TAM_OrdEspecial__c = false;

		var selectedRadio = event.getParam("value");;	
		if(selectedRadio == "add"){
			selectedColor.Agregado__c = true;

		} else if(selectedRadio == "dpms"){
			selectedColor.DPMS__c = true;

		}else if(selectedRadio == "OrdEsp"){
			selectedColor.TAM_OrdEspecial__c = true;

		} else {
			
		}

		var idFicha = component.get("v.fichaID");

		var accion = component.get("c.guardarColor");
		accion.setParams({
			"color": selectedColor
		});

		accion.setCallback(this, function(response){
			var state = response.getState();
			helper.hideSpinner(component);
			if(state === "SUCCESS"){
				 //$A.get('e.force:refreshView').fire();
				//location.reload();
				/*var navEvt = $A.get("e.force:navigateToSObject");
				navEvt.setParams({
					"recordId": idFicha,
				});

				navEvt.fire();*/
			} else{				
				console.log(response.getError());
			}
		});

		$A.enqueueAction(accion);		
	
	},

	showFields : function(component, event, helper) {
		var modalBody;		
		var mapa = component.get("v.coloresSeleccionadosMap");
		var ficha = component.get("v.fichaID");
		var colorVersionID = component.get("v.colorInt");
		var colorExternoID = component.get("v.colorExt");
		var statusFicha = component.get("v.estatusFicha");
		
       	if(statusFicha != 'Final'){
	        $A.createComponent("c:ColorExternoSeleccionado_Cmp", 
	        	{	

	        		"coloresSeleccionadosMap": mapa,
	        		"colorInt": colorVersionID,
	        		"colorExt": colorExternoID,
	        		"fichaID" : ficha,
	            	"estatusFicha" : '',
	        		"soloLectura" : false
	        	},

	           function(content, status) {
	               if (status === "SUCCESS") {
	                   modalBody = content;

	                   component.find('overlayLibFC').showCustomModal({
	                       header: "Seleccionar",
	                       body: modalBody, 
	                       showCloseButton: true,
	                       cssClass: "mymodal",
	                       closeCallback: function() {
	                   			var cmpEvent = component.getEvent("updatingColorTab");
	                   			cmpEvent.fire();
	                       }
	                   })
	               }
	           }
	        );
      	}
	}

})