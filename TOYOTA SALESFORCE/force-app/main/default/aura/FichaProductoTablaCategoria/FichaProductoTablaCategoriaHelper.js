({
	funcionConteoPreliminares : function(component) {
		var fila = component.get("v.filas");
		var columnas = component.get("v.columnas");

		var preliminares = [];

		preliminares.push("Preliminares Faltantes: ");

		for(var i = 1; i < columnas.length; i++){
			preliminares.push(0)			
		}


		for(var j = 0; j< fila.length; j++){
			var atributosList = fila[j].atributos;
			//console.log("atributos tabla",atributosList);
			//console.log("Aquí entro");
			for(var k = 0; k < atributosList.length; k++){
				//console.log(atributosList[k].Atributo__c);

				if(atributosList[k].EstatusEspecificacion__c == 'Preliminar' && atributosList[k].NoAplica__c == false){
					preliminares[k+1]++;
				}
			}		
		}

		//console.log(preliminares);
			
		component.set("v.conteoPreliminares", preliminares);
	}
})