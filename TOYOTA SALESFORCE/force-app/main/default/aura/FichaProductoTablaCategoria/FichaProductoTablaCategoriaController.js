({
	handleUpdateFila : function(component, event, helper) {
		console.log("handleUpdateFila TablaCategoria");
		var fila = event.getParam("fila");
		var indiceFila = event.getParam("indiceFila");
		console.log("handleUpdateFila event.fila event.indiceFila",fila,indiceFila);
		var filas = component.get("v.filas");
		console.log("conponent filas",filas);
		
		//console.log(filas[indiceFila]);
		//console.log(fila);
		filas[indiceFila] = fila;
		console.log("conponent filas upd",filas);
		component.set("v.filas",filas);
        helper.funcionConteoPreliminares(component);
	},
	handleUpdateFiltro : function(component, event, helper) {
		/* se maneja desde FichaProductoComp
		console.log("handleUpdateFiltro TablaCategoria");
		var checkCmp = component.find("ftsu");
		for(var i=0 ; i < checkCmp.length ; i++){
			console.log(checkCmp[i].get("v.value"));
		}*/
	},

	selectAllOptions : function(component, event, helper) {
		var checkGral = component.find("ftsu-gral");
		var checkCmp = component.find("ftsu");
		if(!$A.util.isEmpty(checkCmp)){
            for(var i=0 ; i < checkCmp.length ; i++){
                checkCmp[i].set("v.value", checkGral.get("v.value"));
            }
    	}
	},

	doInit : function(component, event, helper) {
		helper.funcionConteoPreliminares(component);
	}
})