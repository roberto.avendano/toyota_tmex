({
    selectFilial : function(component, event, helper){      
        var getSelectFilial = component.get("v.objFilialesGpoCorp");  
        var compEvent = component.getEvent("oSelectedFilialEvent");
        compEvent.setParams({"filialByEvent" : getSelectFilial });  
        compEvent.fire();
    },
})