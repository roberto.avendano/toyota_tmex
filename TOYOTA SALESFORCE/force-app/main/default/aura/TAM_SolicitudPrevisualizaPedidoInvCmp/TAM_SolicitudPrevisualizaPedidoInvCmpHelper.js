({

    /** Guardado de sección */
    consultaListaFinalAutosPed : function(Component, Event){
    	console.log("EN Helper.consultaListaFinalAutosPed...");    
		let listModeloSelFinalesPaso = [];

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.consultaDatosVines");
        objAction.setParams({
        	recordId : Component.get("v.recordId")
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lObjWrpResAct = objResponse.getReturnValue();
            	Component.set("v.listModeloSelFinales", objResponse.getReturnValue());
				console.log("EN Helper.updListaFinalAutosPed listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinales")));     
				this.consultaDatosVinesInter(Component, Event);		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

    },

    /** Guardado de sección */
    consultaDatosVinesInter : function(Component, Event){
    	console.log("EN Helper.consultaDatosVinesInter...");    
		let listModeloSelFinalesPaso = [];

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.consultaDatosVinesInterc");
        objAction.setParams({
        	recordId : Component.get("v.recordId")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let listModeloSelFinalesInterResul = objResponse.getReturnValue();
            	Component.set("v.listModeloSelFinalesInter", listModeloSelFinalesInterResul);
				console.log("EN Helper.consultaDatosVinesInter listModeloSelFinalesInter: " + JSON.stringify(Component.get("v.listModeloSelFinalesInter")));     
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);

    },

    /** Guardado de sección */
    agregarVinIntercambioHp : function(Component, Event){
    	console.log("EN Helper.agregarVinIntercambioHp...");  
		let listModeloSelFinalesPaso = [];
        let blnError = false;
        
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.agregaVinInter");
        objAction.setParams({
        	"recordId" : Component.get("v.recordId"),
        	"lstVinInter" : Component.get("v.listModeloSelFinalesInter"),
        	"strNvoVinIntercambio" : Component.get("v.strNvoVinIntercambio")        	        	
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lObjWrpResAct = objResponse.getReturnValue();            	
            	//Recorre la lista de vines y ve si alguno tiene un error en el campo de blnExisteInvGDealer
				for(let a in lObjWrpResAct){
					//Ve si tiene una cantidad
					if (lObjWrpResAct[a].blnExisteInvGDealer){
						this.showToastError(Component, Event, lObjWrpResAct[a].strInvGDealerMsg);
						blnError = true;
					}//Fin si lObjWrpResAct[a].blnExisteInvGDealer
				}//Fin del for para listModeloSelPrev
				console.log("EN Helper.agregarVinIntercambioHp blnError: " + blnError);            	
            	if (!blnError)
            		Component.set("v.listModeloSelFinalesInter", lObjWrpResAct);
           		//Limpia el campo del inventario buscado
           		Component.set("v.strNvoVinIntercambio", '');
				console.log("EN Helper.agregarVinIntercambioHp listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinalesInter")));            	            		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Guardado de sección */
    removeRowPisoHP : function(Component, Event){
    	console.log("EN Helper.removeRowPisoHP...");  

    	let varIndex = Event.getSource().get("v.value");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo varIndex: " + varIndex);    	
    	let listModeloSelFinalesPaso = Component.get("v.listModeloSelFinales");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo listModeloSelFinalesPaso: " + listModeloSelFinalesPaso.length);
        let objSelecPaso = listModeloSelFinalesPaso[parseInt(varIndex)];
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo objSelecPaso: " + objSelecPaso);    	

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.removeRowPisoHP");
        objAction.setParams({
        	"recordId" : Component.get("v.recordId"),
        	"lstVinInter" : listModeloSelFinalesPaso,
        	"objSelecPaso" : objSelecPaso       	        	
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let listModeloSelFinalesResult = objResponse.getReturnValue();
            	Component.set("v.listModeloSelFinales", listModeloSelFinalesResult);
				console.log("EN Helper.removeRowPisoHP listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinales")));            	            		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Guardado de sección */
    removeRowHp : function(Component, Event){
    	console.log("EN Helper.removeRowHp...");  

    	let varIndex = Event.getSource().get("v.value");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo varIndex: " + varIndex);    	
    	let lstModelosPasoInter = Component.get("v.listModeloSelFinalesInter");
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo lstModelosPasoInter: " + lstModelosPasoInter.length);
        let objSelecPaso = lstModelosPasoInter[parseInt(varIndex)];
    	console.log("EN SolCompEspFlotProg Helper.removeDatosModelo objSelecPaso: " + objSelecPaso);    	

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.removeRowHp");
        objAction.setParams({
        	"recordId" : Component.get("v.recordId"),
        	"lstVinInter" : lstModelosPasoInter,
        	"objSelecPaso" : objSelecPaso       	        	
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lObjWrpResAct = objResponse.getReturnValue();
            	Component.set("v.listModeloSelFinalesInter", lObjWrpResAct);
				console.log("EN Helper.removeRowHp listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinalesInter")));            	            		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },

    /** Guardado de sección */
    updListaFinalAutosPed : function(Component, Event){
    	console.log("EN Helper.updListaFinalAutosPed...: ");  
		let listModeloSelFinalesPaso = [];
		let listModeloSelPrev = Component.get("v.listModeloSelAddDist");
		//Recorre la lista de modelos que vienen en listModeloSelAddDist y toma solo los que tienen un cantidad
		for(let a in listModeloSelPrev){
			//Ve si tiene una cantidad
			if (listModeloSelPrev[a].strCantidad > 0){
				console.log("EN Helper.updListaFinalAutosPed listModeloSelPrev[a]: " + JSON.stringify(listModeloSelPrev[a])); 
				listModeloSelFinalesPaso.push(listModeloSelPrev[a]);
			}
		}//Fin del for para listModeloSelPrev
		//Inicializa la lista final
		Component.set('v.listModeloSelFinales', listModeloSelFinalesPaso);
		console.log("EN Helper.updListaFinalAutosPed listModeloSelFinales: " + JSON.stringify(Component.get("v.listModeloSelFinales")));  
    },
    
    /** Guardado de sección */
    save : function(Component, Event){
    
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.saveDatosModelos");
        objAction.setParams({   			
        	"sWrpDist" : JSON.stringify(Component.get("v.listModeloSelFinales")),
        	"recordId" : Component.get("v.recordId"),
        	"sWrpDistInter" : Component.get("v.listModeloSelFinalesInter")        	
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objWrpResAct = objResponse.getReturnValue();
            	
            	if (objWrpResAct.blnEstatus == false){
            		let wrpDistr = [];
            		Component.set("v.lstWrpDistAct", wrpDistr); 
            		Component.set("v.blnShowDistCmp", false);
            		Component.set("v.intIndex", '0');
            		//alert('blnShowDistCmp: ' +  Component.get("v.blnShowDistCmp"))	            	
            		this.showToastSuccess(Component, Event, objWrpResAct.strDetalle);
            		Component.set("v.blnShowPrevPedidoCmp", false);
            		console.log("EN Helper.cancelar blnShowPrevPedidoCmp: " + Component.get("v.blnShowPrevPedidoCmp"));        	        
            	}//Fin si objWrpResAct.blnEstatus == false
            	//Hubo error	
            	if (objWrpResAct.blnEstatus == true)
            		this.showToastError(Component, Event, objWrpResAct.strDetalle);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);    
    
    },

    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	//let listModeloSelAddDist = [];
    	//Component.set("v.listModeloSelAddDist", listModeloSelAddDist); 
    	Component.set("v.blnShowPrevPedidoCmp", false);
    	console.log("EN Helper.cancelar blnShowPrevPedidoCmp: " + Component.get("v.blnShowPrevPedidoCmp"));        	        
    },
		
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }  

})