({

    /** */
    getTipoSolicitud : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getTipoSolicitud recordId: ", Component.get("v.recordId"));

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.consTipoSolicitud");
        objAction.setParams({
        	"sRecordId" : Component.get("v.recordId")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let strTipoSolicitudResult = objResponse.getReturnValue();
            	console.log("EN getSolicitudExcepcionesVigentes Helper.getTipoSolicitud: ", strTipoSolicitudResult);
            	Component.set("v.strTipoSolicitud", strTipoSolicitudResult);
            }
        });
        $A.enqueueAction(objAction);        

    },


    /** */
    getSolicitudExcepcionesVigentes : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getSolicitudExcepcionesVigentes objModeloSelVinExcep0: ", JSON.stringify(Component.get("v.objModeloSelVinExcep")) );

        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getSolicitudExcepcionVigente");
        objAction.setParams({
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte")        	
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let mapNoDistVinesResult = objResponse.getReturnValue();
            	console.log("EN getSolicitudExcepcionesVigentes Helper.mapNoDistVinesResult: ", mapNoDistVinesResult);
            	Component.set("v.mapNoDistVines", mapNoDistVinesResult); //strTipoSolicitud
            	let strTipoSolicitudPaso = Component.get("v.strTipoSolicitud");
            	console.log("EN getSolicitudExcepcionesVigentes Helper.strTipoSolicitudPaso: ", strTipoSolicitudPaso);
           		this.getDistribEntrega(Component, Event, Component.get("v.mapNoDistVines"));
            }
        });
        $A.enqueueAction(objAction);

    },

    /** */
    getListaInventarioEntrega : function(Component, Event, sobjMapNoDistVines){
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getListaInventarioEntrega objModeloSelVinExcep0: ", JSON.stringify(Component.get("v.objModeloSelVinExcep")) );
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getListaInventarioEntrega strAnioModelo0.1: " + Component.get("v.objModeloSelVinExcep").strAnioModelo);
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getListaInventarioEntrega mapNoDistVines.1: ", Component.get("v.mapNoDistVines"));
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getListaInventarioEntrega sobjMapNoDistVines.1: " + sobjMapNoDistVines);
    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.consListaInventarioEntrega");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte"),
        	mapNoDistVines :  Component.get("v.mapNoDistVines")
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult); 

		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdSolicitud;
		    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getListaInventarioEntrega strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getListaInventarioEntrega strAnioModelo: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getListaInventarioEntrega strIdRegistro: " + Component.get("v.strIdRegistro"));
            	
            }
        });
        $A.enqueueAction(objAction);
        
	}, 
    
    /** */
    getDistribEntrega : function(Component, Event, sobjMapNoDistVines){
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getDistribEntrega objModeloSelVinExcep0: ", JSON.stringify(Component.get("v.objModeloSelVinExcep")) );
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getDistribEntrega strAnioModelo0.1: " + Component.get("v.objModeloSelVinExcep").strAnioModelo);
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getDistribEntrega mapNoDistVines.1: ", Component.get("v.mapNoDistVines"));
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.getDistribEntrega sobjMapNoDistVines.1: " + sobjMapNoDistVines);
    	
        //Obtener lista de distribuidores disponibles.
        let objAction = Component.get("c.getDatosDistribuidores");
        objAction.setParams({
        	recordId : Component.get("v.strIdRegistro"),
        	strIdCatCentMod : Component.get("v.strIdCatCentMod"),
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte"),
        	mapNoDistVines :  Component.get("v.mapNoDistVines")
   		});   		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDistriEntregaResult = objResponse.getReturnValue();
            	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.lstDistriEntregaResult: ", lstDistriEntregaResult);
            	Component.set("v.lstDistriEntrega", lstDistriEntregaResult); 

		    	let strDatosModelo = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strNombre;
		        //Aplica un split a sDatosModelo.split();  Avanza-2205-2020-X12-10
		        let arrDatosModelo = strDatosModelo.split("-");
		        //Inicializa los datos del modelo
		        let strAnioModelo = arrDatosModelo[2]; 
		        let strModelo = arrDatosModelo[1]; 
		        let strSerie = arrDatosModelo[0]; 
		        let strCveColorExteriorCustom = arrDatosModelo[3];
		        let strCveColorInteriorCustom = arrDatosModelo[4];
		        let strDescripcionColorExterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorExterior;
		        let strDescripcionColorInterior = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strDescripcionColorInterior;
		        let strVersion = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strVersion;
		        let strCantidad = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strCantidadSolicitada;
		        let strIdCatCentrModelos = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdCatCentrModelos;
		        let strIdSolicitud = Component.get("v.objModeloSelExcepCte").lModelosSeleccionados[0].strIdSolicitud;
		    	console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strDatosModelo: " + strDatosModelo);

		    	//Inicializa la variables con los datos de objModeloSelFinal
		        Component.set("v.strAnioModelo", strAnioModelo); //Component.get("v.objModeloSelFinal").strAnioModelo
		        Component.set("v.strSerie", strSerie); //Component.get("v.objModeloSelFinal").strSerie
		        Component.set("v.strModelo", strModelo); //Component.get("v.objModeloSelFinal").strModelo
		        Component.set("v.strVersion", strVersion); //Component.get("v.objModeloSelFinal").strVersion
		        Component.set("v.strColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strColorExteriorCustom
		        Component.set("v.strColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strColorInteriorCustom
		        Component.set("v.strCveColorExteriorCustom", strDescripcionColorExterior); //Component.get("v.objModeloSelFinal").strCveColorExteriorCustom
		        Component.set("v.strCveColorInteriorCustom", strDescripcionColorInterior); //Component.get("v.objModeloSelFinal").strCveColorInteriorCustom        
		        Component.set("v.strCantidad", strCantidad); //Component.get("v.objModeloSelFinal").strCantidad
		        Component.set("v.strIdCatCentMod", strIdCatCentrModelos); //Component.get("v.objModeloSelFinal").strIdCatCentMod
		        Component.set("v.strIdRegistro", strIdSolicitud); //Component.get("v.objModeloSelFinal").strIdRegistro
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strAnioModelo: " + Component.get("v.strAnioModelo"));
		        console.log("EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl Helper.getDistribEntrega strIdRegistro: " + Component.get("v.strIdRegistro"));
            	
            }
        });
        $A.enqueueAction(objAction);
        
	}, 

    /** Guardado de sección */
    openWindowExcepFlotillaPrograma : function(Component, Event){
    	console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.openWindowExcepFlotillaPrograma...");
    	
    	let blnCapturoVin = false;
    	let intCntReg = 1;
    	let lstDistriEntrega = Component.get("v.lstDistriEntrega");
    	// Ve si selecciono algun VIN
    	for (let strVin in lstDistriEntrega){
    		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.openWindowExcepFlotillaPrograma lstDistriEntrega[strVin]: ", lstDistriEntrega[strVin]);  
    		//Ve si seleciono a un reg 
    		if (lstDistriEntrega[strVin].bolSeleccionar && (lstDistriEntrega[strVin].strVin == '' 
    			|| lstDistriEntrega[strVin].strVin == null) && !lstDistriEntrega[strVin].bolSelBloqueado ){
    			this.showToastWarning(Component, Event,'En el registro :' + intCntReg + ' Debes capturar un VIN.');
    			return;
    		}
    		if (lstDistriEntrega[strVin].bolSeleccionar && lstDistriEntrega[strVin].strVin != '' 
    			&& lstDistriEntrega[strVin].strVin != null && !lstDistriEntrega[strVin].bolSelBloqueado)
    			blnCapturoVin = true;
    		//Sumale 1 al contador
    		intCntReg++;
    	}//Fin del for para lstDistriEntrega
    	
    	//Ve si seleciono un VIN por lo menos
    	if (!blnCapturoVin){
    		this.showToastWarning(Component, Event,'Debes seleccionar al menos un VIN para crear una Excepción.');
    		return;
    	}
    		
    	//No hubo error entonces mete a la lista de vines lVinesSeleccionados que biene en el onjeto de objModeloSelExcepCte
        let objAction = Component.get("c.updateVinesLista");
        objAction.setParams({
        	objModeloSelExcepCte : Component.get("v.objModeloSelExcepCte"),
        	lstDistriEntrega : Component.get("v.lstDistriEntrega")
   		});
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let objModeloSelExcepCteResult = objResponse.getReturnValue();
            	//console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.objModeloSelExcepCte: ", objModeloSelExcepCteResult);
            	Component.set("v.objModeloSelExcepCteFinal", objModeloSelExcepCteResult); 
            	//console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Helper.objModeloSelExcepCteFinal: ", Component.get("v.objModeloSelExcepCteFinal") );
            	//Inicializa la lista de lstWrpDistAct a null
            	Component.set("v.blnCreaExcepcionFlotillaPrograma", true);
            	Component.set("v.blnShowBtnCreaExcep", false);
            	Component.set("v.blnBloquearVines", true);
            }
        });
        $A.enqueueAction(objAction);
    		
    },

    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	let wrpDistr = [];
    	Component.set("v.lstDistriEntrega", wrpDistr); 
    	Component.set("v.blnShowVinesDOD", false);
    	Component.set("v.blnCreaExcepcionFlotillaPrograma", false);
    	Component.set("v.blnShowBtnCreaExcep", true);
    },

    /** Toast Error */
    showToastWarning: function(Component, Event, strMensaje) {
    	let toastEvent = $A.get("e.force:showToast");
    	toastEvent.setParams({
    		"title": "Warning.",
    		"message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
    		"duration": 2000,
    		"type": "warning"
    	});
    	toastEvent.fire();
    },

})