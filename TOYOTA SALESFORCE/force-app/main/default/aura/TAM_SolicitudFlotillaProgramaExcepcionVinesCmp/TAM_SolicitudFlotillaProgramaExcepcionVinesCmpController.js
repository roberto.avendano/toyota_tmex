({

    /** Funcion Inicial */
	doInit : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.doInit..");
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.doInit objModeloSelVinExcep: ", Component.get("v.objModeloSelVinExcep") );
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.doInit objModeloSelExcepCte: ", JSON.stringify( Component.get("v.objModeloSelExcepCte") ) );
		//console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.doInit strIdSolicitud: " + Component.get("v.objModeloSelExcep").strIdSolicitud );		
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.doInit recordId: ", Component.get("v.recordId"));
		Component.set('v.recordId', Component.get("v.recordId"));
		Component.set("v.blnCreaExcepcionFlotillaPrograma", false);		
    	Component.set("v.blnShowVinesDOD", true);
    	Component.set("v.blnBloquearVines", false);
    	//Ve que tipo de solicitud es
		Helper.getTipoSolicitud(Component, Event);    	
    	//Consulta todos los reg que ya tienen una excepcion creada en el objeto de TAM_SolicitudExpecionIncentivo__c
		Helper.getSolicitudExcepcionesVigentes(Component, Event);    	    	
		//Llama la getDistribEntrega
		//Helper.getDistribEntrega(Component, Event);
    },

   /** Funcion Inicial */
	cancelar : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.cancelar..");
		Helper.cancelar(Component, Event);
    },

   /** Funcion Inicial */
	openWindowExcepFlotillaPrograma : function(Component, Event, Helper) {
		console.log("EN TAM_SolicitFlotProgExcepcionVinesCmpCtrl Contoller.openWindowExcepFlotillaPrograma..");
		Helper.openWindowExcepFlotillaPrograma(Component, Event);
    },

    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
   // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }

})