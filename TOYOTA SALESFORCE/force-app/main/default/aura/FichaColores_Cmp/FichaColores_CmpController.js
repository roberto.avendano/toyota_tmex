({
	doInit : function(component, event, helper) {
      helper.initColorTab(component);
	},

	createColorTab : function(component, event, helper) {
	    var createRecordEvent = $A.get("e.force:createRecord");
	    var windowHash = window.location.hash;
      createRecordEvent.setParams({
	        "entityApiName": "FichaColores__c",
          "panelOnDestroyCallback": function(event) {
            window.location.hash = windowHash;
          },
          
          "defaultFieldValues": {
              'FichaTcnicaProducto__c': component.get("v.recordId")
          }

	    });
	    createRecordEvent.fire();
	},

	showFields : function(component, event, helper) {
		var modalBody;		
		var mapa = component.get("v.seleccionadosMap");
		var ficha = component.get("v.currentTab");
		var colorVersionID = event.currentTarget.dataset.value;
		var colorExternoID = event.currentTarget.dataset.title;
		
      if(ficha.FichaProductoEstatus__c != 'Final'){  
        $A.createComponent("c:ColorExternoSeleccionado_Cmp", 
        	{	

        		"coloresSeleccionadosMap": mapa,
        		"colorInt": colorVersionID,
        		"colorExt": colorExternoID,
        		"fichaID" : ficha.FichaTcnicaProducto__c,
            "estatusFicha" : '',
        		"soloLectura" : false
        	},

           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   //var container = component.find("compColores");
                   //var body = container.get("v.body");
                   //body.push(modalBody);
                   //container.set("v.body", body);
                   component.find('overlayLibFC').showCustomModal({
                       header: "Seleccionar",
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function() {
                          helper.initColorTab(component);
                       		//console.log('hola');
                           //helper.getTablasFicha(component,component.get("v.recordId"));
                           //helper.getFTProducto(component,component.get("v.recordId"));
                       }
                   })
               }
           }
        );
      }
	},

	showSelectedColorsTable : function(component, event, helper) {
		var ficha = component.get("v.currentTab");
		var modalBody;

        $A.createComponent("c:ColoresExternosAgregados", 
        	{	
        		"fichaProductoID" : ficha.FichaTcnicaProducto__c
        	},

           function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   /*var container = component.find("compColores");
                   var body = container.get("v.body");
                   body.push(modalBody);
                   container.set("v.body", body);*/
                   
                   component.find('overlayLibFC').showCustomModal({
                       header: "Colores",
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "mymodal",
                       closeCallback: function() {
                       		helper.initColorTab(component);
                       }
                   })
               }
           }
        );
	},

  editColorTab : function(component, event, helper) {
      var ficha = component.get("v.currentTab");
      var editRecordEvent = $A.get("e.force:editRecord");
      editRecordEvent.setParams({
         "recordId": ficha.Id
      });
      editRecordEvent.fire();
  },

  addVersion : function(component, event, helper) {
      var ficha = component.get("v.currentTab");
      var createRecordEvent = $A.get("e.force:createRecord");
      var windowHash = window.location.hash;
      createRecordEvent.setParams({
          "entityApiName": "Versiones_Ficha_Colores__c",
          "panelOnDestroyCallback": function(event) {
            window.location.hash = windowHash;
          },

          "defaultFieldValues": {
              'Ficha_Colores__c': ficha.Id
          }

      });
      createRecordEvent.fire();
  },

  addIntColor : function(component, event, helper) {
      var modalBody;
      $A.createComponent("c:AddRecordsFichaColores", 
        { 
            "typeObject": "Color_version__c"
        },

         function(content, status) {
             if (status === "SUCCESS") {
                 modalBody = content;

                 
                 component.find('overlayLibFC').showCustomModal({
                     header: "Crear color interior",
                     body: modalBody, 
                     showCloseButton: true,
                     cssClass: "mymodal",
                     closeCallback: function() {
                        //helper.initColorTab(component);
                     }
                 })
             }
         }
      );
      //var idVersion = event.getSource().get("v.value");
      /*var idVersion = event.target.value;
      var createRecordEvent = $A.get("e.force:createRecord");
      var windowHash = window.location.hash;
      createRecordEvent.setParams({
          "entityApiName": "Color_version__c",
          "panelOnDestroyCallback": function(event) {
            window.location.hash = windowHash;
          },

          "defaultFieldValues": {
              'Version__c': idVersion
          }

      });
      createRecordEvent.fire();*/
  },

  addProductVersion : function(component, event, helper) {      
      var ficha = component.get("v.currentTab");
      var idVersion = event.target.value;
      //console.log(idVersion);
      var modalBody;
      
      $A.createComponent("c:AddVersionProduct", 
        { 
          "productTabID" : ficha.FichaTcnicaProducto__c,
          "versionID" : idVersion
        },

         function(content, status) {
             if (status === "SUCCESS") {
                 modalBody = content;

                 
                 component.find('overlayLibFC').showCustomModal({
                     header: "Colores",
                     body: modalBody, 
                     showCloseButton: true,
                     cssClass: "mymodal",
                     closeCallback: function() {
                        helper.initColorTab(component);
                     }
                 })
             }
         }
      );
  },

  /*deleteVersion : function(component, event, helper) {
    var idVersion = event.target.value;
    var modalBody;

    $A.createComponent("c:DeleteRecordsFichaColores", 
        { 
          "recordID": idVersion
        },

         function(content, status) {
             console.log(status);

           if (status === "SUCCESS") {
              modalBody = content;
              component.find('overlayLibFC').showCustomModal({
                   header: "Colores",
                   body: modalBody, 
                   showCloseButton: true,
                   cssClass: "mymodal",
                   closeCallback: function() {
                      
                   }
               })
           }
         }
    );
  },*/

  modifyVersion : function(component, event, helper){
      console.log('Extraste a: modifyVersion');
      var verID = event.target.value;
      var modalBody;

      $A.createComponent("c:EditRecordsFichaColores", 
        { 
            "recordID": verID
        },

         function(content, status) {
             if (status === "SUCCESS") {
                 modalBody = content;
                 
                 component.find('overlayLibFC').showCustomModal({
                     header: "Detalle de registro",
                     body: modalBody, 
                     showCloseButton: true,
                     cssClass: "mymodal",
                     closeCallback: function() {
                        helper.initColorTab(component);
                     }
                 })
             }
         }
      );
  }

  /*deleteVersionProduct : function(component, event, helper) {
    console.log('Extraste a: deleteVersionProduct');
    var idprod = event.getSource().get("v.value");

    $A.createComponent("c:DeleteRecordsFichaColores", 
        { 
          "recordID": idprod
        },

         function(content, status) {
             console.log(status);
         }
    );
  }*/

})