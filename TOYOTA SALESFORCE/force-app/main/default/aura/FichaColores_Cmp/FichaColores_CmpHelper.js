({
	initColorTab : function(component){
	    console.log('doInit: Ficha colores');
    
    	var accion = component.get("c.getFichaActual");
		accion.setParams({
			"fichaProdID": component.get("v.recordId")
		});

		accion.setCallback(this, function(response){
			var state = response.getState();
			if(state === "SUCCESS"){			
				var fichaActual = response.getReturnValue();				
				component.set("v.currentTab", fichaActual);
				
				if(fichaActual != null && fichaActual.Id != null){
					this.getVersiones(component, fichaActual.Id);	
					this.coloresSeleccionados(component, fichaActual.Id);				
					this.getColoresExternos(component, fichaActual);					
				}											
			}
		});

		$A.enqueueAction(accion);
	},
	
	getVersiones : function(component, fichaID) {
		//console.log('Obtiene versiones colores:');
		var accion = component.get("c.getVersionesColores");
		accion.setParams({
			"fichaID": fichaID
		});

		accion.setCallback(this, function(response){
			var state = response.getState();			
			if(state === "SUCCESS"){
				component.set("v.versiones", response.getReturnValue());												
			}

		});

		$A.enqueueAction(accion);	
	},

	getColoresExternos : function(component, fichaActual) {
		//console.log('Obtiene colores externos:');
		var accion = component.get("c.getColoresExternos");
		var fichaActual = component.get("v.currentTab");
		
		accion.setParams({
			"ficha": fichaActual
		});

		accion.setCallback(this, function(response){
			var state = response.getState();			
			if(state === "SUCCESS"){
				component.set("v.coloresExternos", response.getReturnValue());				
			}

		});

		$A.enqueueAction(accion);	
	},

	coloresSeleccionados : function(component, idFicha) {
		//console.log('Obtiene mapa seleccionados:');
		var accion = component.get("c.getColoresSeleccionados");
		accion.setParams({
			"fichaID": idFicha
		});

		accion.setCallback(this, function(response){
			var state = response.getState();
			if(state === "SUCCESS"){
				component.set("v.seleccionadosMap", response.getReturnValue());				
			}
		});

		$A.enqueueAction(accion);	
	}
})