({

    /** */
    getWrpModelo : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.getWrpModelo : " + JSON.stringify(Component.get("v.objModeloSelAddDist")));
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.getWrpModelo");
        objAction.setParams({   			
        	sWrpModelo : JSON.stringify(Component.get("v.objModeloSelAddDist")) 
   		});        
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var objOrdenDeCompraWrapperClass = objResponse.getReturnValue();
                Component.set("v.objModeloSelFinal", objOrdenDeCompraWrapperClass);
                Component.set("v.strAnioModelo", Component.get("v.objModeloSelFinal").strAnioModelo);
                Component.set("v.strSerie", Component.get("v.objModeloSelFinal").strSerie);
                Component.set("v.strModelo", Component.get("v.objModeloSelFinal").strModelo);
                Component.set("v.strVersion", Component.get("v.objModeloSelFinal").strVersion);
                Component.set("v.strColorExteriorCustom", Component.get("v.objModeloSelFinal").strColorExteriorCustom);
                Component.set("v.strColorInteriorCustom", Component.get("v.objModeloSelFinal").strColorInteriorCustom);
                Component.set("v.strCveColorExteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorExterior);
                Component.set("v.strCveColorInteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorInterior);
                Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);
                Component.set("v.strCantidad", Component.get("v.objModeloSelFinal").strCantidad);
                console.log("EN SolCompEspDist Helper.objModeloSelFinal : " + Component.get("v.strAnioModelo"));
                //Manda llamar el resto de las consultas
                this.getDistribuidores(Component, Event);
                this.getCatalogoDistribuidores(Component, Event);
                this.getDistribDatosModelo(Component, Event);
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
        
	}, 

   /** */
    getDistribuidores : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.getDistribuidores...");
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.getDistribuidores");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var lstDistribuidores = objResponse.getReturnValue();
                Component.set("v.lstDistribuidores", lstDistribuidores);
                console.log("EN SolCompEspDist Helper.getDistribuidores lstDistribuidores: " + Component.get("v.lstDistribuidores"));
            }
        });
        $A.enqueueAction(objAction);
	}, 


   /** */
    getCatalogoDistribuidores : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.getCatalogoDistribuidores...");
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.getCatalogoDistribuidores");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var lstCatalogoDistribuidores = objResponse.getReturnValue();
                Component.set("v.lstCatalogoDistribuidores", lstCatalogoDistribuidores);
                console.log("EN SolCompEspDist Helper.getCatalogoDistribuidores lstCatalogoDistribuidores: " + Component.get("v.lstCatalogoDistribuidores"));
            }
        });
        $A.enqueueAction(objAction);
	}, 


    /** */
    getDistribDatosModelo : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.getDistribDatosModelo: " + Component.get("v.objModeloSelFinal"));
    	
    	//Serializa el objeto del tipo objModeloSelAddDist
    	var strObjMod = JSON.stringify(Component.get("v.objModeloSelAddDist"))
    	var ObjMod = JSON.parse(strObjMod); 
    	Component.set("v.objModeloSelFinal", ObjMod);
    	
    	//Inicializa la variables con los datos de objModeloSelFinal
        Component.set("v.strAnioModelo", Component.get("v.objModeloSelFinal").strAnioModelo);
        Component.set("v.strSerie", Component.get("v.objModeloSelFinal").strSerie);
        Component.set("v.strModelo", Component.get("v.objModeloSelFinal").strModelo);
        Component.set("v.strVersion", Component.get("v.objModeloSelFinal").strVersion);
        Component.set("v.strColorExteriorCustom", Component.get("v.objModeloSelFinal").strColorExteriorCustom);
        Component.set("v.strColorInteriorCustom", Component.get("v.objModeloSelFinal").strColorInteriorCustom);
        Component.set("v.strCveColorExteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorExterior);
        Component.set("v.strCveColorInteriorCustom", Component.get("v.objModeloSelFinal").strCodigoColorInterior);        
        Component.set("v.strPrecioSinIVA", Component.get("v.objModeloSelFinal").strPrecioSinIVA);
        Component.set("v.strCantidad", Component.get("v.objModeloSelFinal").strCantidad);
        
        console.log("EN SolCompEspDist Helper.getDistribDatosModelo : " + Component.get("v.strAnioModelo") + ' strSolRecortTypeId: ' + Component.get("v.strSolRecortTypeId"));
                    	
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.getDatosDistribuidores");
        objAction.setParams({
        	recordId : Component.get("v.recordId"),
        	intIndex : Component.get("v.intIndex"),
        	strAnioModelo : Component.get("v.strAnioModelo"),
        	strSerie : Component.get("v.strSerie"),
        	strModelo : Component.get("v.strModelo"),
        	strVersion : Component.get("v.strVersion"),
        	strIdColExt : Component.get("v.strCveColorExteriorCustom"),
        	strIdColInt : Component.get("v.strCveColorInteriorCustom"),        	
        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId")
   		});   		
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	var intIndex = 0;
                var strLista = objResponse.getReturnValue();
                //Serializa el JSON que te regresa  con JSON.parse(json);
                var lObjects = JSON.parse(strLista);
                console.log("EN SolCompEspDist Helper.getDistribDatosModelo lObjects: " + JSON.stringify(lObjects));
                var lstWrapperPaso = Component.get("v.lstWrpDistAct");
                //Recorre la lista de objetos y metelos a la lista de
				for(var a in lObjects){
					lstWrapperPaso.push(lObjects[a]);
					intIndex = lstWrapperPaso[a].intIndex;
				}                
                //Ya tienes la lista de distr en lstWrapperPaso ahora metela a  lstWrpDistAct
                console.log("EN SolCompEspDist Helper.getDistribDatosModelo lstWrapperPaso: " + JSON.stringify(lstWrapperPaso) + ' intIndex: ' + intIndex);
                console.log("EN SolCompEspDist Helper.getDistribDatosModelo intIndex: " + intIndex);
                Component.set("v.lstWrpDistAct", lstWrapperPaso); 
                Component.set("v.intIndex", intIndex);
            }
        });
        $A.enqueueAction(objAction);
        
	}, 


    gettipoRegistroPrograma : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma...");
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.getTipRegPrograma");
        objAction.setParams({   			
        	strTipRegNombre : 'Programa'
   		});                
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var strTipoRegPrograma = objResponse.getReturnValue();
                Component.set("v.strSolRecortTypeId", strTipoRegPrograma);
                console.log("EN SolCompEspDist Helper.gettipoRegistroPrograma strTipoRegPrograma: " + Component.get("v.strSolRecortTypeId"));
            }
        });
        $A.enqueueAction(objAction);
	}, 

	
    /** */
    addDistrubidor : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.addDistrubidor...");
    	console.log("EN SolCompEspDist Helper.addDistrubidor lstWrpDistAct: " + Component.get("v.lstWrpDistAct"));
        //Obtener lista de distribuidores disponibles.
        var objAction = Component.get("c.addWrpDist");
        objAction.setParams({   			
        	sWrpDist : JSON.stringify(Component.get("v.lstWrpDistAct")), //JSON.stringify(Component.get("v.lstWrpDistAct"))
        	recordId : Component.get("v.recordId"),
        	intIndex : Component.get("v.intIndex"),
        	strAnioModelo : Component.get("v.strAnioModelo"),
        	strSerie : Component.get("v.strSerie"),
        	strModelo : Component.get("v.strModelo"),
        	strVersion : Component.get("v.strVersion"),
        	strIdColExt : Component.get("v.strCveColorExteriorCustom"),
        	strIdColInt : Component.get("v.strCveColorInteriorCustom"),        	
        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId")
   		});   		
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var strLista = objResponse.getReturnValue();
                //Serializa el JSON que te regresa  con JSON.parse(json);
                var object = JSON.parse(strLista);
                console.log("EN SolCompEspDist Helper.addDistrubidor object: " + JSON.stringify(object));
                console.log("EN SolCompEspDist Helper.addDistrubidor lstWrpDistAct: " + Component.get("v.lstWrpDistAct"));
                var lstWrapperPaso = Component.get("v.lstWrpDistAct");
                //Agregale a la lista el elemento que viene en object
                lstWrapperPaso.push(object);
                Component.set("v.lstWrpDistAct", lstWrapperPaso);
                //Toma el valor del indice y sumale uno
                var incIndexPaso = Component.get("v.intIndex") + 1;
                Component.set("v.intIndex", incIndexPaso);
                console.log("EN SolCompEspDist Helper.addDistrubidor lstWrpDistAct: " + JSON.stringify(Component.get("v.lstWrpDistAct")) + ' incIndexPaso: ' + incIndexPaso );
            }
        });
        $A.enqueueAction(objAction);
        
	}, 
	
    /** */
    removeDistribuidor : function(Component, Event){
    	console.log("EN SolCompEspDist Helper.removeDistribuidor...");
    	var varIndex = Event.getSource().get("v.value");
    	console.log("EN SolCompEspDist Helper.removeDistribuidor varIndex: " + varIndex);
    	var lstWrpDistElimPaso = Component.get("v.lstWrpDistElim");
    	var lstWrpDistPaso = Component.get('v.lstWrpDistAct');
    	var cntElimElem = 0;
    	//Recorre la lista de distribuisores y qita el que no se necesita
    	for(var a in lstWrpDistPaso){
    		if (lstWrpDistPaso[a].intIndex == varIndex){
    			lstWrpDistPaso[a].bolEliminar = true;
    			//Mandalo a la tabla donde se van a eliminar los reg
    			lstWrpDistElimPaso.push(lstWrpDistPaso[a]);
    			cntElimElem = a;
    			//console.log("EN SolCompEspDist Helper.removeDistribuidor YA LO ENCONTRE cntElimElem: " + cntElimElem );
    			//console.log("EN SolCompEspDist Helper.removeDistribuidor YA LO ENCONTRE intIndex: " + lstWrpDistPaso[a].intIndex + ' varIndex: ' + varIndex + ' ELEMNT: ' + lstWrpDistPaso[a].bolEliminar);
    			break;	
    		}//Fin si lstWrpDistPaso[a].intIndex == varIndex
    	}//Fin del for para lstWrpDistPaso
    	//Quita el elemento de lista
    	lstWrpDistPaso.splice(cntElimElem, 1);
    	//Actualiliza la lista final
    	Component.set("v.lstWrpDistAct", lstWrpDistPaso);
    	Component.set("v.lstWrpDistElim", lstWrpDistElimPaso);
    	console.log("EN SolCompEspDist Helper.removeDistribuidor YA LO ENCONTRE lstWrpDistPaso: " + JSON.stringify(lstWrpDistPaso));
    	console.log("EN SolCompEspDist Helper.removeDistribuidor YA LO ENCONTRE lstWrpDistElim: " + JSON.stringify(lstWrpDistElimPaso));
	}, 

	
    /** Guardado de sección */
    save : function(Component, Event){
        var lstWrpDistPaso = Component.get('v.lstWrpDistAct');
        var varTotAutosSol = 0;
        //Recorre la lista de los reg que si tienen algo en la cantidad
    	for(var a in lstWrpDistPaso){
    		if (lstWrpDistPaso[a].intCantidad > 0 && lstWrpDistPaso[a].bolEliminar == false){
    			//alert('intCantidad: ' + lstWrpDistPaso[a].intCantidad);
    			varTotAutosSol += parseInt(lstWrpDistPaso[a].intCantidad);
    			console.log("EN SolCompEspDist Helper.save varTotAutosSol: " + varTotAutosSol);	
    		}//Fin si lstWrpDistPaso[a].intIndex == varIndex
    	}//Fin del for para lstWrpDistPaso
        
        var intCantSolcitada = Component.get('v.strCantidad');
        //alert('varTotAutosSol: ' + varTotAutosSol + ' intCantSolcitada: ' + intCantSolcitada);
        //Compara la cantidad solicitada con varTotAutosSol
        if (varTotAutosSol > intCantSolcitada || varTotAutosSol < intCantSolcitada)
        	this.showToastError(Component, Event, 'La catidad de autos solicitada es diferente a la cantidad de autos capturada'); 	
        
        //Es la misma cantidad entonces has la actualización de los datos
        if (varTotAutosSol == intCantSolcitada){
        
	        //Obtener lista de distribuidores disponibles.
	        var objAction = Component.get("c.saveDatos");
	        objAction.setParams({   			
	        	sWrpDist : JSON.stringify(Component.get("v.lstWrpDistAct")),
	        	sWrpDistElim : JSON.stringify(Component.get("v.lstWrpDistElim")),
	        	recordId : Component.get("v.recordId"),
	        	intIndex : Component.get("v.intIndex"),
	        	strAnioModelo : Component.get("v.strAnioModelo"),
	        	strSerie : Component.get("v.strSerie"),
	        	strModelo : Component.get("v.strModelo"),
	        	strVersion : Component.get("v.strVersion"),
	        	strIdColExt : Component.get("v.strCveColorExteriorCustom"),
	        	strIdColInt : Component.get("v.strCveColorInteriorCustom"),
	        	strSolRecortTypeId : Component.get("v.strSolRecortTypeId")
	   		});   		
	        objAction.setCallback(this, function(objResponse){
	            var objState = objResponse.getState();
	            if (objState === "SUCCESS"){
	            	var objWrpResAct = objResponse.getReturnValue();
	            	if (objWrpResAct.blnEstatus == false){
	            		var wrpDistr = [];
	            		Component.set("v.lstWrpDistAct", wrpDistr); 
	            		Component.set("v.blnShowDistCmp", false);
	            		Component.set("v.intIndex", '0');
	            		//alert('blnShowDistCmp: ' +  Component.get("v.blnShowDistCmp"))	            	
	            		this.showToastSuccess(Component, Event, objWrpResAct.strDetalle);
	            	}//Fin si objWrpResAct.blnEstatus == false
	            	if (objWrpResAct.blnEstatus == true)
	            		this.showToastError(Component, Event, objWrpResAct.strDetalle);
	            }//Fin si objState === "SUCCESS"
	        });
	        $A.enqueueAction(objAction);
        
        }//Fin si varTotAutosSol == intCantSolcitada        	
        
    },

    /** Guardado de sección */
    cancelar : function(Component, Event){
    	//Inicializa la lista de lstWrpDistAct a null
    	var wrpDistr = [];
    	Component.set("v.lstWrpDistAct", wrpDistr); 
    	Component.set("v.blnShowDistCmp", false);        	        
    },
	
	
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }  
        		
})