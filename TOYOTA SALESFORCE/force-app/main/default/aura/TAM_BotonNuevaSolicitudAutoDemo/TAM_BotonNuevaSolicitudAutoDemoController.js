({
    myAction : function(component, event, helper) {
        
    },
    
    doInit : function(component, event, helper) {
        var action = component.get("c.getProfileByUser");
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        action.setParams({ 
            
            userId : userId
            
            
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS" && component.isValid()){
                var result = response.getReturnValue();
                component.set("v.user", result);
                var perfil = component.get("v.user.Profile.Name");
                if(perfil == 'Gerente de Ventas DTM'){
                    var elements = document.getElementsByClassName("hide");
                    elements[0].style.display = 'none';
                }else{
                    var elements = document.getElementsByClassName("hide");
                    elements[0].style.display = 'block';
                    
                }
                
            }else{
                console.error("fail:" + response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    openModal: function(component) {
        console.log("modal");
        var userId = $A.get( "$SObjectType.CurrentUser.Id" );
        console.log("usuario en sesion"+userId);
        $A.createComponent( 'c:TAM_SolicitudIncentivoAutoDemo', {
            userId:userId
        },
                           function(modalComponent, status, errorMessage) {
                               if (status === "SUCCESS") {
                                   //Appending the newly created component in div
                                   var body = component.find( 'showChildModal' ).get("v.body");
                                   body.push(modalComponent);
                                   component.find( 'showChildModal' ).set("v.body", body);
                               } else if (status === "INCOMPLETE") {
                                   console.log('Server issue or client is offline.');
                               } else if (status === "ERROR") {
                                   console.log('error');
                                   console.log("Error: " + errorMessage);
                               }
                           }
                          );
    },
    
    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        if(message == 'Ok')
        {
            
            
            //helper.FinalizarEval(component);
        }
        else if(message == 'Cancel')
        {
            // if the user clicked the Cancel button do your further Action here for canceling
        }
    }
})