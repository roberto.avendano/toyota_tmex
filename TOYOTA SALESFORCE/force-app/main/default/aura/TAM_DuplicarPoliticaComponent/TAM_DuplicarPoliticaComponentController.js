({
    /** Funcion Inicial */
	doInit : function(objComponent, objEvent, objHelper) {
		objHelper.initializeComponent(objComponent, objEvent);
       
    },
    
    close: function(objComponent, objEvent, objHelper){
        var navEvt = $A.get("e.force:navigateToSObject");
        var recordId = objComponent.get("v.recordId");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    },
    
    setClon: function(objComponent, objEvent, objHelper){
        objHelper.setClon(objComponent, objEvent);
    }
    
    
})