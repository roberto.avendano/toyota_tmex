({
	/* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
	initializeComponent : function(objComponent, objEvent) {
        this.getPoliticaOriginal(objComponent, objEvent);
        var objAction = objComponent.get("c.getDetalleOriginal");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
   				objComponent.set("v.lstDetalleOriginal", result);
            }else {
                
            }
        });
        $A.enqueueAction(objAction);
    },
    
    //Obtener Politica Incentivos Original
    getPoliticaOriginal : function(objComponent, objEvent){
        var objAction = objComponent.get("c.politica");
        objAction.setParams({  recordId : objComponent.get("v.recordId")  });
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                var result = objResponse.getReturnValue();
   				objComponent.set("v.objPoliticaOriginal", result);
            }else {
                
            }
        });
        $A.enqueueAction(objAction);
    },
    
    //Generar politica Clonada
    setClon : function(objComponent, objEvent){
        objComponent.set('v.loaded', true);
        console.log("ENTRO 1");
        var objAction = objComponent.get("c.clonarDetalle");
        var lstDetalles = objComponent.get("v.lstDetalleOriginal");
        objAction.setParams(
        		{   			
                    lstDetallesSeleccionados : lstDetalles,
                    objPoliticaOriginal : objComponent.get("v.objPoliticaOriginal")
        		}
        );
        console.log("ENTRO 2");
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS") {
                objComponent.set('v.loaded', false);
                var result = objResponse.getReturnValue();
                this.showToastSuccess(objComponent, objEvent);
                this.abrirRegistro(objComponent, objEvent,result);
            }else {
                objComponent.set('v.loaded', false);
                this.showToastError(objComponent, objEvent);
            }
        });
        $A.enqueueAction(objAction);
    },
    
    /** Toast Success */
    showToastSuccess : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": "Politica de Incentivos duplicado correctamente",
            "duration": 4000,
            "type": "success"
        });
        toastEvent.fire();
    },

    /** Toast Error */
    showToastError : function(objComponent, objEvent) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    },
    
    /*Abrir registro nuevo*/
    abrirRegistro: function(objComponent, objEvent, strRecordId){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": strRecordId
        });
        navEvt.fire();
    },
})