({
    
    /** Funcion Inicial */
    doInit : function(objComponent, objEvent, objHelper) {
        objHelper.buscaNoDistUserActual(objComponent, objEvent);
        objHelper.consultaMaximoLeads(objComponent, objEvent, objHelper);
        
    },
    
    /** Funcion Inicial */
    onActivaUsrSegLead : function(objComponent, objEvent, objHelper) {
        objHelper.activaUsrSegLeadMob(objComponent, objEvent);
    },
    
    /** Funcion Inicial */
    btnActivaUserSegLeads : function(objComponent, objEvent, objHelper) {
        objHelper.activaUserSegLeads(objComponent, objEvent);
    },
    
    closeQuickAction : function(objComponent, objEvent, objHelper) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
    showSpinner: function(component, event, helper) {
        component.set("v.spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){
        component.set("v.spinner", false);
    },
    
    guardarML : function(component,event,helper){    
        helper.guardarMaximoLeads(component,event,helper);
    },
    
    
    cancelar  : function(component,event,helper){
        helper.cancelar(component,event,helper);
    }
})