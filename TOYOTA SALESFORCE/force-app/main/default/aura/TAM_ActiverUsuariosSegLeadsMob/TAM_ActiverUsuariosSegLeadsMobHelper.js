({
    
    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
    buscaNoDistUserActual : function(objComponent, objEvent) {
        console.log("EN buscaNoDistUserActual...");
        //Llama la función 
        let objAction = objComponent.get("c.getNoDistlUserActual");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {
                let objWrpResActResult = objResponse.getReturnValue();
                console.log("EN buscaPerfilUserActual objWrpResActResult: ", objWrpResActResult);
                objComponent.set("v.blnNoUserAdmin", objWrpResActResult.blnEstatus);
                if (objWrpResActResult.blnEstatus == false){
                    objComponent.set("v.strNoDist", objWrpResActResult.strParamOpcional2);
                    this.getUsuarosDealer(objComponent, objEvent);
                }
                if (objWrpResActResult.blnEstatus == true){
                    objComponent.set("v.sDetalleErrorConsCte", objWrpResActResult.strDetalle);
                    this.showToastError(objComponent, objEvent, objWrpResActResult.strDetalle);
                }
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },
    
    
    /** */
    getUsuarosDealer : function(objComponent, objEvent){
        console.log("EN SolCompEspDist Helper.getUsuarosDealer...");
        //Obtener lista de distribuidores disponibles.
        var objAction = objComponent.get("c.getUsuarosDealer");
        objAction.setParams({   			
            noDistAct : objComponent.get("v.strNoDist") 
        });        
        objAction.setCallback(this, function(objResponse){
            var objState = objResponse.getState();
            if (objState === "SUCCESS"){
                var lUsuariosActivosLeadsRes = objResponse.getReturnValue();
                objComponent.set("v.lUsuariosActivosLeads", lUsuariosActivosLeadsRes);
                console.log("EN getUsuarosDealer lUsuariosActivosLeads: ", objComponent.get("v.lUsuariosActivosLeads") );
            }
        });
        $A.enqueueAction(objAction);
    }, 
    
    //Funcion que sirve para seleccionar el tipo de Excepción
    activaUsrSegLeadMob: function(objComponent, objEvent) {
        console.log('EN activaUsrSegLeadMob...');
        console.log('EN activaUsrSegLeadMob lUsuariosActivosLeads: ' + objComponent.get("v.lUsuariosActivosLeads"));
    },
    
    /* Funcion de inicializado que obtiene la información a mostrar en la tabla de modelos disponibles a producir*/
    activaUserSegLeads : function(objComponent, objEvent) {
        console.log("EN activaUserSegLeads...");
        //Llama la función 
        let objAction = objComponent.get("c.AcctUsrSegLead");
        objAction.setParams({
            "strNoDist" : objComponent.get("v.strNoDist"),
            "lUsuariosActivosLeads" : objComponent.get("v.lUsuariosActivosLeads")
        });		
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS") {		       		
                let objWrpResAct = objResponse.getReturnValue();
                if (objWrpResAct.blnEstatus == false){
                    this.showToastSuccess(objComponent, objEvent, objWrpResAct.strDetalle);            	
                }
                if (objWrpResAct.blnEstatus == true)
                    this.showToastError(objComponent, objEvent, objWrpResAct.strDetalle);  		
            }//Fin si objState === "SUCCESS"
        });
        $A.enqueueAction(objAction);
    },
    
    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje, //"Politica de Incentivos guardada correctamente",
            "duration": 2500,
            "type": "success"
        });
        toastEvent.fire();
    },
    
    /** Toast Error */
    showToastError : function(Component, Event, strMensaje) {
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 1500,
            "type": "error"
        });
        toastEvent.fire();
    },
    
    
    consultaMaximoLeads :  function(component, event, helper) {
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 
        let action = component.get("c.consultaMaximoLeads");
        
        action.setParams({
            userId 		: userId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let parametroResponse = response.getReturnValue();
                component.set("v.numeroMaximoLeads",parametroResponse);
                component.set("v.numeroMaximoLeadsField",parametroResponse);
            }
            
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    guardarMaximoLeads : function(component, event, helper) {
        let numeroMaximoLead = component.get("v.numeroMaximoLeadsField");
        let userId = $A.get("$SObjectType.CurrentUser.Id"); 
        let action = component.get("c.seteaValorMaximoLeads");
        
        action.setParams({
            userId 		: userId,
            numeroMaximoLead  : numeroMaximoLead
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.numeroMaximoLeads",numeroMaximoLead);
                var strMensaje = 'Registro actualizado !'
                this.showToastSuccess(component, event, strMensaje); 
                helper.buscaNoDistUserActual(component,event);
            }
            
        });
        $A.enqueueAction(action);
        
    },   
    
    cancelar : function(component,event,helper) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
        	"listViewId" : "00B1Y0000085D9tUAE",
            "listViewName": "Retail Nuevos",
            "scope": "Lead"
        });
        navEvent.fire();
        
    }
})