({
    doInit: function(component, event, helper) {
        //console.log('doInit');
        //helper.getFTProducto(component,component.get("v.recordId"));
        component.set("v.colEspecificaciones", [
            {label: "TipoAtributo", fieldName: "TipoAtributo__c", type: "text"},
            {label: "TipoEspecificacion", fieldName: "TipoEspecificacion__c", type: "text"}
        ]  
                     );
        helper.getTablasFicha(component,component.get("v.recordId"));
        helper.getPrimeProfileID(component);
        helper.getcfgPersonalizada(component);
        helper.getNameProfile(component);
        helper.getCountCriticalFields(component,event,helper);
        
    },
    getSelectedEspecificacion: function(component, event, helper) {
        var selectedRows = event.getParam("selectedRows");
        for (var i = 0; i < selectedRows.length; i++){
            alert("You selected: " + selectedRows[i].Atributo__c);
        }
    },
    handleEditRecord : function(component, event, helper) {
        console.log("editRecord");
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        editRecordEvent.fire();
    },
    handleDeleteRecord: function(component, event, helper) {
        if(confirm("¿Esta seguro de borrar el registro?")){
            helper.showSpinner(component);
            component.find("recordHandler").deleteRecord($A.getCallback(function(deleteResult) {
                if (deleteResult.state === "SUCCESS" || deleteResult.state === "DRAFT") {
                    var homeEvent = $A.get("e.force:navigateToObjectHome");
                    homeEvent.setParams({
                        "scope": "FichaTecnicaProducto__c"
                    });
                    homeEvent.fire();
                    console.log("Record is deleted fire.");
                } else if (deleteResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (deleteResult.state === "ERROR") {
                    console.log('Problem deleting record, error: ' + JSON.stringify(deleteResult.error));
                } else {
                    console.log('Unknown problem, state: ' + deleteResult.state + ', error: ' + JSON.stringify(deleteResult.error));
                }
                helper.hideSpinner(component);
            }));
        }
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            // record is changed
            console.log("record is changed");
            helper.getFTProducto(component,component.get("v.recordId"));
            helper.getTablasFicha(component,component.get("v.recordId"));
        } else if(eventParams.changeType === "LOADED") {
            // record is loaded in the cache
            console.log("record is loaded in the cache");
            helper.getFTProducto(component,component.get("v.recordId"));
            helper.getTablasFicha(component,component.get("v.recordId"));
        } else if(eventParams.changeType === "REMOVED") {
            // record is deleted, show a toast UI message
            console.log("record is deleted, show a toast UI message");
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "title": "Eliminado",
                "message": "El registro a sido eliminado."
            });
            resultsToast.fire();
        } else if(eventParams.changeType === "ERROR") {
            // there’s an error while loading, saving, or deleting the record
            console.log("there’s an error while loading, saving, or deleting the record");
        }
    },
    handleAddModel: function(component, event, helper) {
        var modalBody;
        var cmp =  component;
        var ftp = component.get("v.FTProducto");
        console.log("modal2",ftp);
        var id=  component.get("v.recordId");
        console.log("------------------------->>>>>>>  Id : "+id);
        $A.createComponent("c:FichaProductoAddSerie", {"ftp": ftp},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLibFC').showCustomModal({
                                       header: "Agregar Serie " + ftp.Serie__r.Name,
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "mymodal",
                                       closeCallback: function() {
                                           var a = component.get('c.loadTablas');
                                           $A.enqueueAction(a);
                                           // helper.getTablasFicha(component,id);
                                           // helper.getFTProducto(component,id);
                                       }
                                   })
                               }
                           }
                          );
    },
    handleSetFTSU: function(component, event, helper) {
        var modal = component.find('modalSeleccion');
        $A.util.removeClass(modal,'slds-fade-in-hide');
        $A.util.addClass(modal,'slds-fade-in-open');
        //        var modalBody;
        //        $A.createComponents([
        //                ["lightning:input",{
        //                    "label" : "Nombre",
        //                    "aura:id" : "txt-ftsu-name",
        //                }],
        //                ["lightning:button",{
        //                    "aura:id": "btn-new-ftsu",
        //                	"label": "Crear",
        //                	"onclick": component.getReference("c.openSetFTSU")
        //                }]
        //            ],
        //            function(components, status, errorMessage){
        //                if (status === "SUCCESS") {
        //                    modalBody = components;
        //                    var body = component.get("v.body");
        //                    body.push(components);
        //                    component.set("v.body", body);
        //                    
        //                    component.find('overlayLibFC').showCustomModal({
        //                       header: "Seleccionar",
        //                       body: modalBody, 
        //                       showCloseButton: true,
        //                       cssClass: "mymodal",
        //                       closeCallback: function() {
        //                          console.log('You closed the alert!');
        //                       }
        //                   })
        //                }
        //                
        //                else if (status === "INCOMPLETE") {
        //                    console.log("No response from server or client is offline.")
        //                    // Show offline error
        //                }
        //                else if (status === "ERROR") {
        //                    console.log("Error: " + errorMessage);
        //                    // Show error message
        //                }
        //            }
        //);
        //helper.setFTSU(component,component.get("v.recordId"));
    },
    handleSaveFTSU: function(component, event, helper) {
        var tablas = component.get("v.tablasFicha");
        var ftsu = {};
        for(var tablaFicha=0 ; tablaFicha < tablas.length ; tablaFicha++){
            for(var subcat=0 ; subcat < tablas[tablaFicha].subcategorias.length ; subcat++){
                for(var tabla=0 ; tabla < tablas[tablaFicha].subcategorias[subcat].tablas.length ; tabla++){
                    for(var fila=0 ; fila < tablas[tablaFicha].subcategorias[subcat].tablas[tabla].filas.length ; fila++){
                        var vFila = tablas[tablaFicha].subcategorias[subcat].tablas[tabla].filas[fila];
                        ftsu[vFila.nombreCampo] = vFila.selecionado;
                    }
                }
            }
        }
        console.log(ftsu);
        helper.saveFTSU(component,component.get("v.recordId"),ftsu);
    },
    generaXLS: function(component, event, helper) {
        //Guardar el XLS dentro de Salesforce
        var action = component.get("c.saveXLSFDC");
        var id = component.get("v.recordId");
        
        action.setParams({ 
            id : id
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {

            }
            
        });
        
        
        $A.enqueueAction(action);
        var urlEvent = $A.get("e.force:navigateToURL");
        var url = "/apex/FichaProductoExportar?f=xls&id="+component.get("v.recordId");
        console.log("url",url)
        urlEvent.setParams({
            "url": url,
            "isredirect": "true"
        });
        //urlEvent.fire();
        window.open(url, '_blank');
    },
    generaPDF: function(component, event, helper) {
        //Guardar el PDF dentro de Salesforce
        var action = component.get("c.savePdfSFDC");
        var id = component.get("v.recordId");
        
        action.setParams({ 
            id : id
        });
        
        action.setCallback(this, function(response){
            var objState = response.getState();
            if (objState === "SUCCESS") {

            }
            
        });
        
        
        $A.enqueueAction(action);
        
        //
        var urlEvent = $A.get("e.force:navigateToURL");
        var url = "/apex/FichaProductoPDF?id="+component.get("v.recordId");
        console.log("url",url)
        urlEvent.setParams({
            "url": url,
            "isredirect": "true"
        });
        //urlEvent.fire();
        window.open(url, '_blank');

        
    },
    
    clonarFIcha: function(component, event, helper) {
        var modalBody;
        var cmp =  component;
        var ftp = component.get("v.FTProducto");
        console.log("modal2",ftp);
        var id=  component.get("v.recordId");
        //helper.clonarFicha(component, component.get("v.recordId"));
        console.log("------------------------->>>>>>>  Id : "+id);
        $A.createComponent("c:AgregarAnoFichaProducto", {"ftp": ftp},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   modalBody = content;
                                   component.find('overlayLibFC').showCustomModal({
                                       header: ftp.Serie__r.Name,
                                       body: modalBody, 
                                       showCloseButton: true,
                                       cssClass: "mymodal",
                                       closeCallback: function() {
                                           var a = component.get('c.loadTablas');
                                           $A.enqueueAction(a);
                                           // helper.getTablasFicha(component,id);
                                           // helper.getFTProducto(component,id);
                                       }
                                   })
                               }
                           }
                          );
    },
    
    
    
    showSpinner: function(component, event, helper) { 
        helper.showSpinner(component); 
    }, 
    hideSpinner : function(component,event,helper){    
        helper.hideSpinner(component);
    },
    updateTotales : function(component,event,helper){
        helper.getFTProducto(component,component.get("v.recordId"));
    },
    openSetFTSU : function(component,event,helper){        
        var ftsuName = component.find("txt-ftsu-name").get("v.value");
        //console.log('----------------->>>> ftsuName :  '+ftsuName);
        helper.setFTSU(component,component.get("v.recordId"), ftsuName);
        component.find("txt-ftsu-name").set("v.value","");
        var modal = component.find('modalSeleccion');
        $A.util.removeClass(modal,'slds-fade-in-open');
        $A.util.addClass(modal,'slds-fade-in-hide');
    },
    closeSetFTSU : function(component,event,helper){    
        component.find("txt-ftsu-name").set("v.value","");    
        var modal = component.find('modalSeleccion');
        $A.util.removeClass(modal,'slds-fade-in-open');
        $A.util.addClass(modal,'slds-fade-in-hide');
        
    },
    loadFTSU : function(component,event,helper){
        var ftsuID = event.target.value;
        helper.activateFTSU(component,component.get("v.recordId"),ftsuID);
    },
    loadTablas : function(component,event,helper){
        helper.getTablasFicha(component,component.get("v.recordId"));
        helper.getFTProducto(component,component.get("v.recordId"));
    }
    
})