({
    
    
    getNameProfile: function(component) {
        var userId = $A.get("$SObjectType.CurrentUser.Id") ;
        component.set("v.userId",userId);
        
        var action = component.get("c.getCurrentProfile");
        
        action.setParams({ userId : userId });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                component.set("v.User",response);
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);        
            
    },

    getCountCriticalFields: function(component,helper) {
        var IdFichaProducto =  component.get("v.recordId");
        var action = component.get("c.countCriticalFields");

        action.setParams({ IdFichaProducto : IdFichaProducto }); 

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var countCriticalFields = response.getReturnValue();
                component.set("v.countCriticalFields",countCriticalFields);
                
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
              

    },    
    
    getcfgPersonalizada: function(component) {
        console.log("helper.getcfgPersonalizada");
        var action = component.get("c.getCFGPersonalizada");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var ftp = response.getReturnValue();
                console.log("ftp: "+ftp);
                
                component.set("v.btnEditarByProfile", ftp);
                
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getFTProducto: function(component, idFTProducto) {
        console.log("helper.getFTProducto",idFTProducto);
        var action = component.get("c.getFTProducto");
        action.setParams({
            "idFTP": idFTProducto
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var ftp = response.getReturnValue();
                console.log("$A.util.isEmpty(ftp.FichaTecnicaSeleccionUsuarios__r)",ftp, $A.util.isEmpty(ftp.FichaTecnicaSeleccionUsuarios__r));
                if($A.util.isEmpty(ftp.FichaTecnicaSeleccionUsuarios__r)){
                    component.set("v.tieneSeleccion", false);
                }
                component.set("v.FTProducto", ftp);
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Creado",
                    "message": "Se creo la configuración correctamente."
                });
            }else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    getTablasFicha: function(component, idFTProducto) {
        console.log("helper.getTablasFicha",idFTProducto);
        component.find("overlayLibFC").notifyClose();
        var action = component.get("c.getTablasFicha");
        action.setParams({
            "idFTP": idFTProducto
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.tablasFicha",response.getReturnValue());
                console.log("getTablasFicha",response.getReturnValue());
                //this.llenaPreliminares(component);
                
            }else {
                console.log("Failed with state: " + state);
                //console.log(response.getError());
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    clonarFicha: function(component,idFTProducto){
        console.log("helper.clonarFicha",idFTProducto);
        var action = component.get("c.clonarFicha");
        action.setParams({
            "idFTP": idFTProducto
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var navEvt = $A.get("e.force:navigateToSObject");
                console.log("clone",response.getReturnValue());
                navEvt.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "detail"
                });
                navEvt.fire();
            }else {
                console.log("Failed with state: " + state);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    setFTSU: function(component,idFTProducto, ftsuName){
        console.log("helper.setFTSU",idFTProducto);
        var action = component.get("c.setFTSU");
        action.setParams({
            "idFTP": idFTProducto,
            "ftsuDescription" : ftsuName
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.tieneSeleccion", true);
                //component.find("overlayLibFC").notifyClose();
                
                this.getTablasFicha(component,idFTProducto);
                this.getFTProducto(component,idFTProducto);
                component.find("overlayLibFC").notifyClose();
            }else {
                console.log("Failed with state: " + state);
            }
            //this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    saveFTSU: function(component,idFTProducto, seleccion){
        console.log("helper.saveFTSU",idFTProducto,seleccion);
        var action = component.get("c.saveFTSU");
        action.setParams({
            "idFTP": idFTProducto,
            "seleccion": seleccion
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log("saveFTSU",response.getReturnValue());
                
            }else {
                console.log("Failed with state: " + state);
            }
            this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    },
    showSpinner: function(component) { 
        console.log("showSpinner");
        component.set("v.Spinner", true); 
    }, 
    hideSpinner : function(component){   
        console.log("hideSpinner");  
        component.set("v.Spinner", false);
    },
    
    llenaPreliminares : function(component){
        var tablas = component.get("v.tablasFicha");
        var columnas = component.get("v.FTProducto");
        console.log("tablas",tablas);
        console.log("columnas",columnas);
        var mapa = {};
        var conteos = [];
        for(var c = 0; c < columnas.ModelosFichaProducto__r.length; c++){
            conteos.push(0);
        }
        for(var tablaFicha=0 ; tablaFicha < tablas.length ; tablaFicha++){
            for(var subcat=0 ; subcat < tablas[tablaFicha].subcategorias.length ; subcat++){
                for(var tabla=0 ; tabla < tablas[tablaFicha].subcategorias[subcat].tablas.length ; tabla++){
                    for(var fila=0 ; fila < tablas[tablaFicha].subcategorias[subcat].tablas[tabla].filas.length ; fila++){
                        var vFila = tablas[tablaFicha].subcategorias[subcat].tablas[tabla].filas[fila];
                        
                        for(var attr = 0; attr < vFila.atributos.length; attr++){
                            var atributos = vFila.atributos;
                            if(atributos[attr].EstatusEspecificacion__c == 'Preliminar' && atributos[attr].NoAplica__c == false){
                                conteos[attr]++;     
                            }
                        }
                    }
                }
                
                mapa[tablas[tablaFicha].subcategorias[subcat].nombre] = conteos;
                for(var i = 0; i< conteos.length; i++){
                    conteos[i] = 0;
                }                
            }            
        }
        
        //console.log(mapa);
        component.set("v.conteosMap", mapa);
    },
    
    
    getPrimeProfileID : function(component){
        console.log("Obteniendo Id de perfil Prime...");
        var accion = component.get("c.getPrimeProfile");
        accion.setCallback(this, function(response){
            var status = response.getState();
            if(component.isValid() && status === "SUCCESS") {
                component.set("v.primeProfileID", response.getReturnValue());
            }
        });
        
        $A.enqueueAction(accion);
    },
    activateFTSU: function(component,idFTProducto, idFTSUser){    	
        var action = component.get("c.activeFTSU");
        action.setParams({
            "idFTP": idFTProducto,
            "idFTSU" : idFTSUser
        });
        this.showSpinner(component);
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.tieneSeleccion", true);
                //component.find("overlayLibFC").notifyClose();
                
                this.getTablasFicha(component,idFTProducto);
                this.getFTProducto(component,idFTProducto);
            }else {
                console.log("Failed with state: " + state);
            }
            //this.hideSpinner(component);
        });
        $A.enqueueAction(action);
    }
})