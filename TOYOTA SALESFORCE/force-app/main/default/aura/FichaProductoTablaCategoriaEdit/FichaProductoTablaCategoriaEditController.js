({
	doInit: function(component, event, helper) {
		//var fila = component.get("v.fila");
	},
	actualizar: function(component, event, helper) {
		console.log("actializar");
        var fila = component.get("v.fila");
        var noAplica = component.get("v.noAplica");
        console.log("fila,noAplica",fila,noAplica);
        helper.actualizaFila(component, fila,noAplica);
    },
    handleCancel : function(component, event, helper) {
        document.location.reload(true);
        //component.find("overlayLib").notifyClose();
        //var popup = window.open(location, '_self', '');
        //popup.close();
    },

    handleGlobalValue : function(component, event, helper) {
        var globalValue = component.find('globalValue_txt').get('v.value');
        if(globalValue != ''){
            var fila = component.get("v.fila");
            helper.updateRowAttributes(component, fila, globalValue);            
        }
        
        //console.log(globalValue);
    }
})