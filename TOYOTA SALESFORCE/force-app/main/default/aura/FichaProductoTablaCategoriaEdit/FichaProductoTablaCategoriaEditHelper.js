({
	actualizaFila : function(component, fila, noAplica) {
		console.log("helper.actualizaFila",fila);        
        var action = component.get("c.actualizaEFTs");
        var estatusFicha=[];
        for(var i=0 ; i<fila.atributos.length ; i++){
            //Si el campo es crítico
            if(fila.atributos[i].TAM_CampoCritico__c == true){
            	estatusFicha.push(fila.atributos[i].TAM_EstatusCriticalField__c);
            }
           	//Si el campo es no crítico
        	if(fila.atributos[i].TAM_CampoCritico__c == false){
            	fila.atributos[i].NoAplica__c = noAplica;
            } 
        }

        //Si el campo es critico y no esta aprobado
        if(estatusFicha.length > 0){
        	if(estatusFicha.includes('No Aprobado (Planner)') || estatusFicha.includes('Pendiente Revisión (Planner)')){
          		for(var i=0 ; i<fila.atributos.length ; i++){
            		fila.atributos[i].NoAplica__c = true;
          		}  
        	}else{
                for(var i=0 ; i<fila.atributos.length ; i++){
            		fila.atributos[i].NoAplica__c = false;
                }
        	}
        }
        
        action.setParams({
            "efts": fila.atributos
        });
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                fila.atributos = response.getReturnValue();
                component.set("v.fila", fila);
                console.log("SUCCESS ", fila);
                //alert("value critical:"+respuesta.NoAplica__c);
                /*;
                
                var cmpEvent = component.getEvent("updateFila");
                cmpEvent.setParams({
                    "fila" : fila,
                    "indiceFila" : indiceFila
                });
                cmpEvent.fire();*/

                component.find('notifLib').showToast({
		            "variant": "success",
		            "title": "Se actualizo correctamente.",
		            "message": ""
		        });
		        component.find("overlayLib").notifyClose();
            }else {
                console.log("Failed with state: " + state);
                var errores = response.getError();
                var mensajes = "";

                for(var j = 0; j < errores.length; j++){
                    var messages = errores[j].pageErrors;
                    for(var k = 0; k < messages.length; k++){
                        mensajes += messages[k].message +" : "+messages[k].statusCode +"\n";
                        console.log(messages[k].message);
                        console.log(messages[k].statusCode);
                    }
                }

                this.handleShowNotice(component, mensajes);
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
	},


    updateRowAttributes : function(component, fila, valorGlobal){
        for(var i=0 ; i<fila.atributos.length ; i++){
            fila.atributos[i].Atributo__c = valorGlobal;
            component.set('v.fila', fila);
        }
    },

    handleShowNotice : function(component, errorMessages) {
        component.find('notifLib').showNotice({
            "variant": "error",
            "header": "Ups!... Algo salio mal.",
            "message": errorMessages,
            closeCallback: function() {
                //alert('You closed the alert!');
            }
        });
    }
})