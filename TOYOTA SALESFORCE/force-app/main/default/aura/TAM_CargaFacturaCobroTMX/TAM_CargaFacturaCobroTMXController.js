({
    
    
    doInit : function(component, event, helper) {
        var idRecord = component.get("v.recordId");
        var action = component.get("c.getDocumentByParentId");
        action.setParams({ 
            recordId : idRecord
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.SolicitudIncentivos",response.getReturnValue());
                
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    handleUploadFinished : function(component, event, helper) {
        var idRecord = component.get("v.recordId");
        var uploadedFiles = event.getParam("files");
        var documentId = uploadedFiles[0].documentId;
        var fileName = uploadedFiles[0].name;
        var id       = uploadedFiles[0].Id;
        var toastEvent = $A.get("e.force:showToast");
        
        component.set("v.documentId",documentId);
        component.set("v.nameFile",fileName);
        
        /*   
        var action = component.get("c.saveDocumentId");
        action.setParams({ 
            idRecord : idRecord,
            idDocument : uploadedFiles[0].documentId,
            nombreDocumento : fileName
            
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "File "+documentId+" Uploaded successfully."
                });
                toastEvent.fire();
                component.set("v.nameFile",fileName);
                component.set("v.documentId",documentId);
                component.set("v.archivoCargado",true);
                component.set("v.SolicitudIncentivos.TAMDocumento_Id__c",documentId)
                component.set("v.solicitudEnviada",false);
                console.log("id de documento"+documentId);
                console.log("nombre del documento"+fileName);
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);*/
        
        
    },
    
    previewDocumento : function(component, event, helper) {
        var idDocument;
        
        if(component.get("v.SolicitudIncentivos.TAM_IdDocFactRetail__c") != null){
            idDocument  = component.get("v.SolicitudIncentivos.TAM_IdDocFactRetail__c");
        }
        
        
        if(component.get("v.documentId") != null){
            idDocument  = component.get("v.documentId");
        }
        
        $A.get('e.lightning:openFiles').fire({
            recordIds: [idDocument]
        });
        
    },
    
    
    eliminarDocumento : function(component, event, helper) {
        var idDocument;
        var idRecord = component.get("v.recordId");        
        if(component.get("v.SolicitudIncentivos.TAM_IdDocFactRetail__c") != null){
            idDocument  = component.get("v.SolicitudIncentivos.TAM_IdDocFactRetail__c");
        }

        if(component.get("v.documentId") != null){
            idDocument  = component.get("v.documentId");
        }
        
        var action = component.get("c.deleteDocument");
        action.setParams({ 
            documentId : idDocument,
            idRecordSolicitud : idRecord
            
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nameFile",null);
                component.set("v.documentId",null);
                //component.set("v.SolicitudIncentivos.TAM_NombreFacturaRetail__c ",null);
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Documento Eliminado."
                });
                toastEvent.fire();
            }
            
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    
    }
})