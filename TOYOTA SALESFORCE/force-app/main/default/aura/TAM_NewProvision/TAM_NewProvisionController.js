({
    close : function(objComponent, objEvent, objHelper) {
		objHelper.close(objComponent, objEvent);
	},
    
    guardar : function(objComponent, objEvent, objHelper) {
		objComponent.find("recordEditForm").submit();        
	},
   
    handleSuccess : function(objComponent, objEvent, objHelper) {
        var record = objEvent.getParams().response;
        objComponent.set("v.recordId", record.id);
        objComponent.set("v.guardado", true);
        objHelper.executeBatch(objComponent);
    },
    
    handleError : function(objComponent, objEvent, objHelper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error.",
            "message": "Surgio un problema con el guardado, favor de contactar a su administrador.",
            "duration": 4000,
            "type": "error"
        });
        toastEvent.fire();
    }

})