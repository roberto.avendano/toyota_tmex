({
    close : function(objComponent, objEvent) {
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "TAM_ProvisionIncentivos__c"
        });
        homeEvt.fire();
    },
    executeBatch : function(objComponent){
        this.executeBatch_Retail(objComponent);
        this.executeBatch_Bonos(objComponent);
        this.executeBatch_VtaCorp(objComponent);
    },
    
    executeBatch_Retail : function(objComponent){
        var objAction = objComponent.get("c.executeBatchJob");
        objAction.setParams({ recordId : objComponent.get("v.recordId")});
        objAction.setCallback(this, function(objResponse) {
            var objState = objResponse.getState();
            
            if (objState === "SUCCESS"){
                var interval = setInterval($A.getCallback(function () {
                    var jobStatus = objComponent.get("c.getBatchJobStatus");
                    if(jobStatus != null){
                        jobStatus.setParams({ jobID : objResponse.getReturnValue()});
                        jobStatus.setCallback(this, function(jobStatusResponse){
                            var objState = jobStatus.getState();
                            if (objState === "SUCCESS"){
                                var job = jobStatusResponse.getReturnValue();
                                objComponent.set('v.apexJob',job);
                                var processedPercent = 0;
                                if(job.JobItemsProcessed != 0 && job.TotalJobItems !=0){
                                    processedPercent = (job.JobItemsProcessed / job.TotalJobItems) * 100;
                                    processedPercent = Math.round(processedPercent);
                                }
                                var progress = objComponent.get('v.progress');
                                objComponent.set('v.progress', progress === 100 ? clearInterval(interval) :  processedPercent);
                            }
                        });
                        $A.enqueueAction(jobStatus);
                    }
                }), 2000);
            }
            
            else if (objState === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error Provisión Retail",
                    "message": "Un error ha ocurrido. Por favor contacte a su administrador del Sistema."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(objAction);
    },
    
    executeBatch_Bonos : function(objComponent){
        var objAction = objComponent.get("c.executeBatchJob_Bonos");
        objAction.setParams({ recordId : objComponent.get("v.recordId")});
        objAction.setCallback(this, function(objResponse) {
            var objState = objResponse.getState();
            
            if (objState === "SUCCESS"){
                var interval = setInterval($A.getCallback(function () {
                    var jobStatus = objComponent.get("c.getBatchJobStatus_Bonos");
                    if(jobStatus != null){
                        jobStatus.setParams({ jobID : objResponse.getReturnValue()});
                        jobStatus.setCallback(this, function(jobStatusResponse){
                            var objState = jobStatus.getState();
                            if (objState === "SUCCESS"){
                                var job = jobStatusResponse.getReturnValue();
                                objComponent.set('v.apexJob_Bono',job);
                                var processedPercent = 0;
                                if(job.JobItemsProcessed != 0 && job.TotalJobItems !=0){
                                    processedPercent = (job.JobItemsProcessed / job.TotalJobItems) * 100;
                                    processedPercent = Math.round(processedPercent);
                                }
                                var progress = objComponent.get('v.progress_Bono');
                                objComponent.set('v.progress_Bono', progress === 100 ? clearInterval(interval) :  processedPercent);
                            }
                        });
                        $A.enqueueAction(jobStatus);
                    }
                }), 2000);
            }
            
            else if (objState === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error Provisión Bonos",
                    "message": "Un error ha ocurrido. Por favor contacte a su administrador del Sistema."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(objAction);
    },

    executeBatch_VtaCorp : function(objComponent){
        var objAction = objComponent.get("c.executeBatchJob_VtaCorp");
        objAction.setParams({ recordId : objComponent.get("v.recordId")});
        objAction.setCallback(this, function(objResponse) {
            var objState = objResponse.getState();
            
            if (objState === "SUCCESS"){
                var interval = setInterval($A.getCallback(function () {
                    var jobStatus = objComponent.get("c.getBatchJobStatus_VtaCorp");
                    if(jobStatus != null){
                        jobStatus.setParams({ jobID : objResponse.getReturnValue()});
                        jobStatus.setCallback(this, function(jobStatusResponse){
                            var objState = jobStatus.getState();
                            if (objState === "SUCCESS"){
                                var job = jobStatusResponse.getReturnValue();
                                objComponent.set('v.apexJob_VtaCorp',job);
                                var processedPercent = 0;
                                if(job.JobItemsProcessed != 0 && job.TotalJobItems !=0){
                                    
                                    processedPercent = (job.JobItemsProcessed / job.TotalJobItems) * 100;
                                    processedPercent = Math.round(processedPercent);
                                    
                                }
                                var progress = objComponent.get('v.progress_VtaCorp');
                                objComponent.set('v.progress_VtaCorp', progress === 100 ? clearInterval(interval) :  processedPercent);
                            }
                        });
                        $A.enqueueAction(jobStatus);
                    }
                }), 2000);
            }
            
            else if (objState === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "title": "Error Provisión Venta Corporativa",
                    "message": "Un error ha ocurrido. Por favor contacte a su administrador del Sistema."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(objAction);
    }
})