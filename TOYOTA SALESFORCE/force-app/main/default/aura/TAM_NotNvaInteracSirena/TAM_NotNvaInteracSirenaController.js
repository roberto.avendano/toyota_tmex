({
    //StreamingDemoController.js
    
    // Sets an empApi error handler on component initialization
    onInit : function(component, event, helper) {
            
        /*// Get the empApi component
        const empApi = component.find('empApi');
        // Uncomment below line to enable debug logging (optional)
        empApi.setDebugFlag(true);
        // Register error listener and pass in the error handler function
        empApi.onError($A.getCallback(error => {
            // Error can be any type of error (subscribe, unsubscribe...)
            console.error('EMP API error: ', error);
        }));*/

    	//Ve que tipo de dispositivo es	
        helper.typeDevice(component, event, helper);
        //Consulta las ult interacciones con sirena
        helper.getLstInteraccSirena(component, event, helper);
        
    },

    // Sets an empApi error handler on component initialization
    jsActualizar : function(component, event, helper) {
        //Consulta las ult interacciones con sirena
        helper.getLstInteraccSirena(component, event, helper);
    },

    // Sets an empApi error handler on component initialization
    jsRedirectLead : function(component, event, helper) {
        console.log('EN jsRedirectLead jsRedirectLead...');

        let varIdLeadValue = event.getSource().get("v.value");
        let varIdLeadName = event.getSource().get("v.name");
        console.log('EN jsRedirectLead jsRedirectLead varIdLeadValue: ' + varIdLeadValue);
        console.log('EN jsRedirectLead jsRedirectLead varIdLeadName: ' + varIdLeadName);

    	let lblDatosLeadUrl = $A.get("$Label.c.TAM_DatosLeadUrl");    	
    	//Una funcion para redotreccionar el lead a una nueva pestaña
    	let eUrl= $A.get("e.force:navigateToURL");    		
	    eUrl.setParams({
	      "url": varIdLeadValue 
	    });
	    eUrl.fire();

    },

    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },

    // Invokes the subscribe method on the empApi component
    subscribe : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');
        // Get the channel from the input box
        const channel = '/topic/AccountUpdates';
        // Replay option to get new events
        const replayId = -1;

        // Subscribe to an event
        
        empApi.subscribe(channel, replayId, $A.getCallback(eventReceived => {
            //debugger;
            // Process event (this is called each time we receive an event)
            console.log('Received event ', JSON.stringify(eventReceived));
            let obj = JSON.parse(JSON.stringify(eventReceived)).data.sobject;
            component.set("v.accName",obj.Name);
            helper.showToastNotifica(component, Event, obj.Name);
        }))
        .then(subscription => {
            // Confirm that we have subscribed to the event channel.
            // We haven't received an event yet.
            console.log('Subscribed to channel ', subscription.channel);
            // Save subscription to unsubscribe later
            component.set('v.subscription', subscription);
            alert('Subscribed to ' + channel);
        });
    },

    // Invokes the unsubscribe method on the empApi component
    unsubscribe : function(component, event, helper) {
        // Get the empApi component
        const empApi = component.find('empApi');
        // Get the subscription that we saved when subscribing
        const subscription = component.get('v.subscription');

        // Unsubscribe from event
        empApi.unsubscribe(subscription, $A.getCallback(unsubscribed => {
          // Confirm that we have unsubscribed from the event channel
          console.log('Unsubscribed from channel '+ unsubscribed.subscription);
          component.set('v.subscription', null);
        }));
    },
    
    
})