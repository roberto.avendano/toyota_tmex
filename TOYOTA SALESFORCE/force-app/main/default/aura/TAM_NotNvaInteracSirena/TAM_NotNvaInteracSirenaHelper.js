({

    typeDevice : function(component, event, helper) {
        let deviceMobile = $A.get("$Browser.isPhone");
        if(deviceMobile === true)
            component.set("v.isMobile",true);   
    },
   
    /** getLstInteraccSirena */
    getLstInteraccSirena : function(component, event, helper){
    	console.log("EN TAM_NotifNvaInteracSirena Helper.getLstInteraccSirena...");

        //Obtener lista de distribuidores disponibles.
        let objAction = component.get("c.getUltimasInteracSirena");
        objAction.setCallback(this, function(objResponse){
            let objState = objResponse.getState();
            if (objState === "SUCCESS"){
            	let lstDatosInteaccionSirenaResult = objResponse.getReturnValue();
            	component.set("v.lWrpInteracSirena", lstDatosInteaccionSirenaResult);
 			}
        });
        $A.enqueueAction(objAction);   	                    	
	}, 
   
    /** Toast Error */
   showToastNotifica: function(component, Event, strMensaje) {
	   let toastEvent = $A.get("e.force:showToast");
	   toastEvent.setParams({
		   "title": "Notificación",
		   "message": strMensaje, //"Surgio un problema con el guardado, favor de contactar a su administrador.",
		   "duration": 8000,
		   "type": "success"
	   });
	   toastEvent.fire();
   },
   
})