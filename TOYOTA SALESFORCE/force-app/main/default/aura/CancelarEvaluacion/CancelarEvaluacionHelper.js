({
    cancelarEval : function(component, event, helper) {
        var recordIdEval = component.get("v.recordId");   
        var action = component.get("c.cancela");
        console.log("recordIdEval"+recordIdEval);
        action.setParams({
            "recordIdEval": recordIdEval
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Evaluación cancelada satisfactoriamente!!",
                        type: 'success',
                        duration:' 4000'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    }
})