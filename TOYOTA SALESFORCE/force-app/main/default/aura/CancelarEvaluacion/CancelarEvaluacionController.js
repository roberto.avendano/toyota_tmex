({
    
    doInit : function(component, event, helper) {
        var recordIdEval = component.get("v.recordId");
        var action = component.get("c.getInfoRecord");
        action.setParams({
            "recordIdEval": recordIdEval
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.evalCandidato",response.getReturnValue());
                var evalEstatus = component.get("v.evalCandidatoEstatus");
                if(evalEstatus == "Evaluación cancelada" || evalEstatus == "Evaluación finalizada"){
                    $A.util.addClass(component.find("cancelaEval"), "slds-hide");
                    
                }else{
                    $A.util.removeClass(component.find("cancelaEval"), "slds-hide");
                    
                }
                
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
        
    },
    
    cancelarEval : function(component, event, helper) {
        var recordIdEval = component.get("v.recordId");
        var action = component.get("c.cancela");
        action.setParams({
            "recordIdEval": recordIdEval
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Evaluación cancelada satisfactoriamente!!",
                        type: 'success',
                        duration:' 4000'
                    });
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
        
    },
    
    cancelarEvalConfirm: function(component) {
        $A.createComponent("c:ConfirmarCerrarEvaluacionRH", {},
                           function(content, status) {
                               if (status === "SUCCESS") {
                                   var modalBody = content;
                                   component.find('overlayLib').showCustomModal({
                                       header: "¿Estás seguro de cancelar la evaluación?",
                                       body: modalBody, 
                                       showCloseButton: false,
                                       closeCallback: function(ovl) {
                                           console.log('Overlay is closing');
                                       }
                                   }).then(function(overlay){
                                       console.log("Overlay is made");
                                   });
                               }
                           });
    },
    
    handleApplicationEvent : function(component, event, helper) {
        var message = event.getParam("message");
        if(message == 'Ok')
        {
            helper.cancelarEval(component);
        }
        else if(message == 'Cancel')
        {
            
        }
    }
})