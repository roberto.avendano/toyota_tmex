({

   getTipoPersona: function (component){
   
	   console.log("EN Heler.getTipoPersona.recordId ", component.get("v.recordId"));
	   console.log("EN Heler.getTipoPersona.leadRecord ", component.get('v.leadRecord')); 
	   
	   var strIdCand = component.get('v.leadRecord');
       var action = component.get("c.consultDatosCand");  
        action.setParams({  
            "recordId" : component.get("v.recordId")
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            console.log("EN Heler.state ", state);             
            if(state=='SUCCESS'){  
                var strTipoCand = response.getReturnValue();
                console.log("EN Heler. strTipoCand ", strTipoCand);
                if (strTipoCand == 'Flotilla'){
                	component.set("v.bVentaFlotilla", true);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.FlotillaPF", false);
                	component.set("v.ProgramaPF", false);
                }  
                if (strTipoCand == 'Programa'){
                	component.set("v.bVentaPrograma", true);
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.FlotillaPF", false);
                	component.set("v.ProgramaPF", false);
                }

                if (strTipoCand == 'FlotillaPF'){
                	component.set("v.FlotillaPF", true);
                	component.set("v.ProgramaPF", false);
                	component.set("v.bVentaFlotilla", false);
                	component.set("v.bVentaPrograma", false);
                }  
                if (strTipoCand == 'ProgramaPF'){
                	component.set("v.ProgramaPF", true);
                	component.set("v.FlotillaPF", false);
                	component.set("v.bVentaPrograma", false);
                	component.set("v.bVentaFlotilla", false);
                }

                if (strTipoCand == 'AutorizacionPendiente')
                	component.set("v.bTieneSolAprob", true);
                if (strTipoCand == 'Autorizado')
                	component.set("v.bClienteAutorizado", true);
                	
                //Para el cliente vencido
                if (strTipoCand == 'ClienteVencido')
                	component.set("v.bClienteVencido", true);

                //Para el cliente preautorizado
                if (strTipoCand == 'Preautorizacion'){
                	component.set("v.bClientePreAutorizado", true);
                }
                
                //Inicializa bIniTipoVenta con component.get('v.bIniTipoVenta')
                var bIniTipoVenta = component.get('v.bIniTipoVenta');
                //Inicializa la variable de bIniTipoVenta
                if (bIniTipoVenta) 
                	component.set("v.bIniTipoVenta", false);
                if (!bIniTipoVenta) 
                	component.set("v.bIniTipoVenta", true);
                //Despliega los mensajes de log
                console.log("EN Heler.getTipoPersona.bVentaFlotilla ", component.get('v.bVentaFlotilla'));
                console.log("EN Heler.getTipoPersona.bVentaPrograma ", component.get('v.bVentaPrograma'));	
                console.log("EN Heler.getTipoPersona.bIniTipoVenta ", component.get('v.bIniTipoVenta'));
                console.log("EN Heler.getTipoPersona.bClientePreAutorizado ", component.get('v.bClientePreAutorizado'));
                
            }  
        });  
        $A.enqueueAction(action);
    },
    
	upDateFile : function(component, strActaConst, documentId){
		//debugger;
		console.log("EN Heler.upDateFile strActaConst: " + strActaConst + ' documentId: ' + documentId); 
		var strActaConst = strActaConst;
		var strDocumentId = documentId;
        var action = component.get("c.updFileDesc");  
        action.setParams({  
            "recordId" : component.get("v.recordId"),
            "nombreArchivo" : strActaConst,
            "documentId" : documentId  
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                var result = response.getReturnValue();
                if (strActaConst == 'Acta Constitutiva')
                	component.set("v.filesActCons",result);  
                if (strActaConst == 'Cedula Fiscal')
                	component.set("v.filesCedFiscal",result);
                if (strActaConst == 'Orden de compra')
                	component.set("v.filesOrdenCompra",result);
                if (strActaConst == 'PDF datos adicionales')
                	component.set("v.filesDatosAdicion",result);
                if (strActaConst == 'Identificación oficial')
                	component.set("v.filesDatosIdenOfi",result);
                if (strActaConst == 'Comprobante de domicilio')
                	component.set("v.filesDatosCompDom",result);
                if (strActaConst == 'Documentación de soporte')
                	component.set("v.filesDatosDocSop",result);
                	
                if (strActaConst == 'Carta Compromiso')
                	component.set("v.filesDatosCarComp",result);
                	
            }  
        });  
        $A.enqueueAction(action);  
    },
        
	getUploadedFiles : function(component, strActaConst){
		//debugger;
		console.log("EN Heler.getUploadedFiles strActaConst: ", strActaConst); 
		var strActaConst = strActaConst;  
        var action = component.get("c.getFiles");  
        action.setParams({  
            "recordId" : component.get("v.recordId"),
            "nombreArchivo" : strActaConst  
        });      
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){  
                var result = response.getReturnValue();
                if (strActaConst == 'Acta Constitutiva')
                	component.set("v.filesActCons",result);  
                if (strActaConst == 'Cedula Fiscal')
                	component.set("v.filesCedFiscal",result);  
                if (strActaConst == 'Orden de compra')
                	component.set("v.filesOrdenCompra",result);   
                if (strActaConst == 'PDF datos adicionales')
                	component.set("v.filesDatosAdicion",result);          
                if (strActaConst == 'Identificación oficial')
                	component.set("v.filesDatosIdenOfi",result);
               if (strActaConst == 'Comprobante de domicilio')
                	component.set("v.filesDatosCompDom",result);
                if (strActaConst == 'Documentación de soporte')
                	component.set("v.filesDatosDocSop",result);   

                if (strActaConst == 'Carta Compromiso')
                	component.set("v.filesDatosCarComp",result);
            }  
        });  
        $A.enqueueAction(action);  
    },
    
    delUploadedfiles : function(component, documentId, strNombreArchivo) {
    	console.log("EN Helper.delUploadedfiles..." + strNombreArchivo + ' documentId: ' + documentId);
        var action = component.get("c.deleteFiles");           
        action.setParams({
            "sdocumentId":documentId
        });  
        action.setCallback(this,function(response){  
            var state = response.getState();  
            if(state=='SUCCESS'){ 
            	console.log("EN Helper.delUploadedfiles getUploadedFiles...");
                this.getUploadedFiles(component, strNombreArchivo);
                component.set("v.Spinner", false); 
            }  
        });  
        $A.enqueueAction(action);  
    },  

	doConsultaDatos : function(component,event,helper){ 
		console.log("EN Comntroller.doConsultaDatos..."); 
		//debugger;
		console.log("EN Comntroller.doInit component.get('v.bVentaFlotilla'): ", component.get('v.bVentaFlotilla')); 
		console.log("EN Comntroller.doInit component.get('v.bVentaPrograma'): ", component.get('v.bVentaPrograma')); 
		console.log("EN Comntroller.doInit component.get('v.bIniTipoVenta'): ", component.get('v.bIniTipoVenta'));
		
		//Carca solo los que corresponden
		if (component.get('v.bVentaFlotilla') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para flotilla..."); 
			var strActaConst = "Acta Constitutiva";
			helper.getUploadedFiles(component, strActaConst);
			var strCedFis = "Cedula Fiscal";
			helper.getUploadedFiles(component, strCedFis);
			var strOrdenCompra = "Orden de compra";
			helper.getUploadedFiles(component, strOrdenCompra);
			var strDatosAdicionales = "PDF datos adicionales";
			helper.getUploadedFiles(component, strDatosAdicionales);
		}//Fin si omponent.get('v.bVentaFlotilla')

		//Carca solo los que corresponden
		if (component.get('v.bVentaPrograma') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para Programa..."); 
			var strIdenOfic = "Identificación oficial";
			helper.getUploadedFiles(component, strIdenOfic);
			var strCompDom = "Comprobante de domicilio";
			helper.getUploadedFiles(component, strCompDom);
			var strDocSop = "Documentación de soporte";
			helper.getUploadedFiles(component, strDocSop);
			var strDatosAdicionales = "PDF datos adicionales";
			helper.getUploadedFiles(component, strDatosAdicionales);			
		}//Fin si omponent.get('v.bVentaFlotilla')

		//Carca solo los que corresponden
		if (component.get('v.FlotillaPF') == true || component.get('v.ProgramaPF') == true){
			console.log("EN Comntroller.doInit Comnsulta datos para Fisica Act Empresarial..."); 
			var strCedFis = "Cedula Fiscal";
			helper.getUploadedFiles(component, strCedFis);
			var strIdenOfic = "Identificación oficial";
			helper.getUploadedFiles(component, strIdenOfic);
			var strCompDom = "Comprobante de domicilio";
			helper.getUploadedFiles(component, strCompDom);
			var strOrdenCompra = "Orden de compra";
			helper.getUploadedFiles(component, strOrdenCompra);
			var strDatosAdicionales = "PDF datos adicionales";
			helper.getUploadedFiles(component, strDatosAdicionales);			
		}//Fin si omponent.get('v.bVentaFlotilla')
		
    },

    /** Toast Success */
    showToastSuccess : function(Component, Event, strMensaje) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Exitoso.",
            "message": strMensaje,
            "duration": 500,
            "type": "success"
        });
        toastEvent.fire();
    },
        
})