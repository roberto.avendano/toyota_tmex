/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Solicitud de Autos Demo.
                    Se aplican reglas de negocio correspondientes, si pasan,
                    Se envian al estado de cuenta.
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		27/Agosto/2020          Versión Inicial
Cecilia Cruz Morán        		11/Agosto/2021      	Se añade validación de intereses TFS
__________________________________________________________________________________
*********************************************************************************/
public without sharing  class TAM_SearchAutoDemo {
    
    //Variable publica
    public static Map<String, Movimiento__c> mapMovimientos {get; set;}

    //Constantes
    static final String STRING_VC_EN_DEALERDAILY = 'VIN registrado como Venta Corporativa en Dealer Daily.';
    static final String STRING_MAYOR_90DIAS = 'Se ha superado los 90 días para el cobro del incentivo.';
    static final String STRING_SIN_INCENTIVO = 'VIN no cuenta con incentivo vigente.';
    static final String STRING_MAYOR_60DIAS = 'La fecha de baja o pago completado excede 60 días.';
    static final String STRING_SIN_FECHABAJA_MODULODEMO = 'Fecha Baja de Auto Demo no registrada en el módulo de Auto-Demo.';
    static final String STRING_SIN_FECHAPAGO_MODULODEMO = 'Fecha de pago completado no registrada en el módulo de Auto-Demo.';
    static final String STRING_VIN_ACTIVO_MODULODEMO = 'VIN aún se encuentra activo en el módulo de Auto-Demo.';
    static final String STRING_NO_REGISTRADO_MODULODEMO = 'VIN no registrado en el módulo de Auto-Demo.';
    static final String STRING_SIN_FACTURA = 'VIN no presenta factura generada.';
    static final String STRING_DIFERENTE_CODIGO_06 = 'VIN no registrado con código de venta 06 en Dealer Daily.';
    static final String STRING_FACTURO_PRECIO_LLENO = 'La factura se emitió a precio lleno y no se encontró una Nota de Crédito Relacionada.';
    static final String STRING_VIN_EN_EDOCTA = 'El VIN solicitado ya se encuentra registrado en un Estado de Cuenta.';

    @AuraEnabled
    public static List <wrapVIN > vinBDDealerDaily(String searchKeyWord,String userId) {
        User[] usr;
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List <Movimiento__C > returnList = new List <Movimiento__C > ();
        Set<String> lstMensajesReglas = new Set<String>();
        Date fechaHoy = system.today();
        String searchKey = searchKeyWord + '%';
        Boolean boolHistoricoCodigo_06 = false;
        TAM_DetallePoliticaJunction__c IncentivosRetail = new TAM_DetallePoliticaJunction__c();
        Boolean bool90DiasIncentivo = true;
        Boolean bool60DiasBaja= true;
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];
        
        //Consultar en Solicitudes de Auto Demo
        List<SolicitudAutoDemo__c> lstAutosDemo = [ SELECT 	Id, VIN__r.Id, VIN__r.Name, Estatus__c,FechaBajaPagoCompletado__c,FechaBajaAutoDemoSolicitud__c,FechaAltaAutoDemo__c
                                                    FROM 	SolicitudAutoDemo__c
                                                    WHERE   VIN__r.name LIKE:searchKey  
                                                    AND     Distribuidor__r.Codigo_Distribuidor__c =: usr[0].CodigoDistribuidor__c
                                                    LIMIT   5];

        Map<String, SolicitudAutoDemo__c> mapAutoDemo = new Map<String, SolicitudAutoDemo__c>();
        if(!lstAutosDemo.isEmpty()){
            for (SolicitudAutoDemo__c  objDemo: lstAutosDemo) {
                mapAutoDemo.put(objDemo.VIN__r.Name, objDemo);
            }
        }
        
        //Consultar movimientos de venta en DD
        List <Movimiento__c> lstOfVin = [SELECT id,Sale_Code__c,Distribuidor__r.name,Distribuidor__r.Codigo_Distribuidor__c,Movimiento__c.Name,
                                                Distribuidor__c,VIN__r.name,Sale_Date__c,TAM_Serie__c,TAM_VIN__c,TAM_codigoModelo__c,TAM_AnioModelo__c,
                                                Fleet__c,Month__c,Year__c
                                        FROM    Movimiento__c
                                        WHERE   VIN__r.Name IN: mapAutoDemo.KeySet()];
        
        Set<String> setVIN = new Set<String>();
        mapMovimientos = new Map<String, Movimiento__c>();
        if(!lstOfVin.isEmpty()){
            for (Movimiento__C  objMov: lstOfVin) {
                setVIN.add(objMov.VIN__r.name);
                mapMovimientos.put(objMov.VIN__r.name, objMov);
            }
        }
        
        //Consultar en Facturas XML
        Set<String> setFactura = new Set<String>();
        List<Factura__c> lstFacturasXML = [SELECT Id,Name, VIN__r.Name,TAM_FechaTimbrado__c,TAM_Total__c 
                                                    FROM Factura__c WHERE VIN__r.Name IN: setVIN];
        Map<String, Factura__c> mapFacturasXML = new Map<String, Factura__c>();
        if(!lstFacturasXML.isEmpty()){
            for (Factura__c  objXML: lstFacturasXML) {
                mapFacturasXML.put(objXML.VIN__r.Name, objXML);
                setFactura.add(objXML.Name);
            }
        }

        //Consultar si esta en un edo de cuenta previo
        Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCta = new Map<String, TAM_DetalleEstadoCuenta__c>();
        List<TAM_DetalleEstadoCuenta__c> lstEdoCta = [  SELECT  Id,TAM_VIN__c
                                                        FROM    TAM_DetalleEstadoCuenta__c
                                                        WHERE   TAM_VIN__c IN: setVIN];
        if(!lstEdoCta.isEmpty()){
            for(TAM_DetalleEstadoCuenta__c objEdoCta : lstEdoCta){
                mapEdoCta.put(objEdoCta.TAM_VIN__c,objEdoCta);
            }
        }

        //Consultar Estado de Cuenta TFS
        Map<String, TAM_EstadoCuentaTFS__c> mapEdoCtaTFS = new Map<String, TAM_EstadoCuentaTFS__c>();
        List<TAM_EstadoCuentaTFS__c> lstEdoCtaTFS = [SELECT Name,TAM_FechaUltimoPagoCapital__c,TAM_InteresesVencidos__c
                                                    FROM    TAM_EstadoCuentaTFS__c
                                                    WHERE   Name IN: setVIN];
        if(!lstEdoCtaTFS.isEmpty()){
            for(TAM_EstadoCuentaTFS__c objEdoCtaTFS : lstEdoCtaTFS){
                mapEdoCtaTFS.put(objEdoCtaTFS.Name,objEdoCtaTFS);
            }
        }

        //Validar si cuenta con Nota de Crédito
        Map<String, TAM_Notas_Credito__c> mapNotasCredito = new Map<String, TAM_Notas_Credito__c>();
        List<TAM_Notas_Credito__c> lstNotaCredito = [SELECT  SubTotal__c,TAM_UUIDRelacionado__c, TAM_UUIDRelacionado1__c,TAM_UUIDRelacionado2__c,
                                                            TAM_FacturaUUIDRelacionado__r.Name,
                                                            TAM_FacturaUUIDRelacionado1__r.Name,
                                                            TAM_FacturaUUIDRelacionado2__r.Name
                                                    FROM    TAM_Notas_Credito__c 
                                                    WHERE   TAM_FacturaUUIDRelacionado__r.Name IN: setFactura
                                                    OR      TAM_FacturaUUIDRelacionado1__r.Name IN: setFactura
                                                    OR      TAM_FacturaUUIDRelacionado2__r.Name IN: setFactura];
        if(!lstNotaCredito.isEmpty()){
            for(TAM_Notas_Credito__c objNota : lstNotaCredito){
                mapNotasCredito.put(objNota.TAM_FacturaUUIDRelacionado__r.Name, objNota);
                mapNotasCredito.put(objNota.TAM_FacturaUUIDRelacionado1__r.Name, objNota);
                mapNotasCredito.put(objNota.TAM_FacturaUUIDRelacionado2__r.Name, objNota);
            }
        }
       

        if(!lstOfVin.isEmpty()){                                           
            for (Movimiento__C  acc: lstOfVin) {
                
                //Validar en DD
                if(acc.Fleet__c == 'C'){
                    lstMensajesReglas.add(STRING_VC_EN_DEALERDAILY);
                }

                //Validar que no se encuentre en Estados de Cuenta Previos
                if(mapEdoCta.containsKey(acc.VIN__r.name)){
                    lstMensajesReglas.add(STRING_VIN_EN_EDOCTA);
                }

                if(acc.Sale_Code__c == '06' || acc.Sale_Code__c == '6'){
                    boolHistoricoCodigo_06 = true;
                    
                    //Validar en Facturas
                    if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name)){

                            //Obtener Incentivos Retail
                            String strMesFactura = String.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c.month());
                            String strAnioFactura = String.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c.year());
                            IncentivosRetail= getIncentivosRetail(strAnioFactura, strMesFactura,acc.VIN__r.name);
                            
                            if(IncentivosRetail != null){
                                Integer intDiasVigencia = (Date.valueOf(IncentivosRetail.TAM_PoliticaIncentivos__r.TAM_FinVigencia__c)).daysBetween(system.today());
                                if(intDiasVigencia > 90){
                                    lstMensajesReglas.add(STRING_MAYOR_90DIAS);
                                }
                            } else {
                                lstMensajesReglas.add(STRING_SIN_INCENTIVO);
                            }
                            //Validar en Auto Demo
                            if(mapAutoDemo != null && mapAutoDemo.containsKey(acc.VIN__r.name)){
                                    
                                //Fecha de alta vs Fecha de Timbrado no mayor a 225 dias.
                                Date fechaTimbradoval = mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c.date();
                                Date fechaAltaDemo = mapAutoDemo.get(acc.VIN__r.name).FechaAltaAutoDemo__c;
                                Integer factura225dias = (fechaAltaDemo.daysBetween(fechaTimbradoval));
                                if(factura225dias > 225){
                                    lstMensajesReglas.add('El VIN excedio los 225 días permitidos para el cobro de incentivo. (Dias transcurridos: ' + factura225dias + ').');
                                }

                                //Fecha de baja con 60 días
                                Date datFechaBaja;
                                if(mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c != null && mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c != null){
                                    if(mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c <= mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c){
                                        datFechaBaja = mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c;
                                    }else {
                                        datFechaBaja = mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c;
                                    }
                                    Integer intDiasBaja = (datFechaBaja).daysBetween(date.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c));
                                    if(intDiasBaja > 60){
                                        lstMensajesReglas.add(STRING_MAYOR_60DIAS);
                                    }
                                } else {
                                    if(mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c == null){
                                        lstMensajesReglas.add(STRING_SIN_FECHABAJA_MODULODEMO);
                                    }
                                    if(mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c == null){
                                        lstMensajesReglas.add(STRING_SIN_FECHAPAGO_MODULODEMO);
                                    }
                                }

                                //Baja del vehiculo
                                if(mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja por pago completado sin solicitud' && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Pago completado sin solicitud'
                                && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja anticipada' && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja por vigencia'){
                                    lstMensajesReglas.add(STRING_VIN_ACTIVO_MODULODEMO);
                                }
                            } else {
                                lstMensajesReglas.add(STRING_NO_REGISTRADO_MODULODEMO);
                            }
                    }else{
                        lstMensajesReglas.add(STRING_SIN_FACTURA);
                    }
                } else {
                    lstMensajesReglas.add(STRING_DIFERENTE_CODIGO_06);
                }
                
                //Llenado Wrapper
                wrapVIN newVIN = new wrapVIN();
                newVIN.VIN = acc.VIN__r.name;
                newVIN.VINID = acc.VIN__r.Id;
                newVIN.idMovimiento = acc.Id;
                newVIN.Serie = acc.TAM_Serie__c;
                newVIN.codigoModelo = acc.TAM_codigoModelo__c;
                newVIN.anioModelo = acc.TAM_AnioModelo__c;
                newVIN.codigoDealer = acc.Distribuidor__r.Codigo_Distribuidor__c;
                newVIN.fechaVentaVIN = acc.Sale_Date__c;
                
                if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name)){
                	newVIN.fechaTimbradoXML = Date.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c);
                }
                newVIN.codigoVenta	 = acc.Sale_Code__c;
                newVIN.nombreDealer  = acc.Distribuidor__r.name;
                newVIN.boolCode_06 = boolHistoricoCodigo_06;
                if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name) && IncentivosRetail != null && mapAutoDemo != null && mapAutoDemo.containsKey(acc.VIN__r.name) 
                    && lstMensajesReglas.isEmpty()){
                    
                    newVIN.solicitudAceptada = true; 
                    newVIN.idAutoDemo = mapAutoDemo.get(acc.VIN__r.name).Id;
                    newVIN.idFacturaCarga =  mapFacturasXML.get(acc.VIN__r.name).Id;
                    newVIN.dblValorFactura = mapFacturasXML.get(acc.VIN__r.name).TAM_Total__c;
                    newVIN.idDetallePolitica = IncentivosRetail.Id;
                    newVIN.dblPrecioLista = IncentivosRetail.TAM_MSRP__c;
                    newVIN.dblPrecioMenosIncentivo = IncentivosRetail.TAM_MSRP__c - IncentivosRetail.TAM_IncPropuesto_Cash__c;
                    newVIN.dblIncentivoPropuesto = IncentivosRetail.TAM_IncPropuesto_Cash__c;
                    newVIN.dblIncentivoTMEX = IncentivosRetail.TAM_IncentivoTMEX_Cash__c;
                    newVIN.dblIncentivoDealer = IncentivosRetail.TAM_IncDealer_Cash__c;
                    newVIN.intPorcentajeParticipacionTMEX = Integer.valueOf((IncentivosRetail.TAM_IncentivoTMEX_Cash__c / IncentivosRetail.TAM_IncPropuesto_Cash__c)*100);
                    newVIN.dblIncentivoOtorgado =  IncentivosRetail.TAM_MSRP__c - mapFacturasXML.get(acc.VIN__r.name).TAM_Total__c;
                    
                    //Escenario  1 "Incentivo Otorgado es mayor al definido en politicas- TMEX solo pagara su limite establecido en el Grid."
                    if(newVIN.dblIncentivoOtorgado >= newVIN.dblIncentivoPropuesto){
                        newVIN.dblIncentivoProporcionalTMEXSinIVA = (IncentivosRetail.TAM_IncentivoTMEX_Cash__c/1.16 );
                    }
                    
                    //Escenario  2 "Incentivo Otorgado es menor al de las politicas"
                    if(newVIN.dblIncentivoOtorgado < newVIN.dblIncentivoPropuesto){
                        Double participacionTMEX = Double.valueOf(IncentivosRetail.TAM_IncentivoTMEX_Cash__c / IncentivosRetail.TAM_IncPropuesto_Cash__c);
                        newVIN.dblIncentivoProporcionalTMEXSinIVA = (newVIN.dblIncentivoOtorgado * participacionTMEX)/1.16;
                    }

                    // Escenario  3 "No se dio incentivo"
                    if(newVIN.dblIncentivoOtorgado == 0){
                        //Validar si se genero una nota de credito
                        if(mapNotasCredito.containsKey(mapFacturasXML.get(acc.VIN__r.name).Name) 
                            && mapNotasCredito.get(mapFacturasXML.get(acc.VIN__r.name).Name).SubTotal__c != 0
                            && mapNotasCredito.get(mapFacturasXML.get(acc.VIN__r.name).Name).SubTotal__c != null){
                                newVIN.dblIncentivoProporcionalTMEXSinIVA = mapNotasCredito.get(mapFacturasXML.get(acc.VIN__r.name).Name).SubTotal__c;
                        } else{
                            newVIN.dblIncentivoProporcionalTMEXSinIVA = 0;
                            lstMensajesReglas.add(STRING_FACTURO_PRECIO_LLENO);
                            newVIN.solicitudAceptada = false; 
                        }
                        
                    }
                    newVIN.lstMensajes = lstMensajesReglas;

                    //Validación de Intereses TFS
                    if(!mapEdoCtaTFS.isEmpty()){
                        newVIN.intereses_fechaVenta = Date.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c);
                        newVIN.intereses_fechaPagoTFS = mapEdoCtaTFS.get(acc.VIN__r.name).TAM_FechaUltimoPagoCapital__c;
                        if(newVIN.intereses_fechaVenta < newVIN.intereses_fechaPagoTFS){
                            newVIN.intereses_diasEntreFechas_Vta_PagoTFS = (newVIN.intereses_fechaVenta).daysBetween(newVIN.intereses_fechaPagoTFS);
                            newVIN.intereses_montoInteresesTFS =  mapEdoCtaTFS.get(acc.VIN__r.name).TAM_InteresesVencidos__c;
                            newVIN.intereses_diasTranscurridosMes = newVIN.intereses_fechaPagoTFS.day();
                            newVIN.intereses_montoInteresPorDia = newVIN.intereses_montoInteresesTFS / newVIN.intereses_diasTranscurridosMes;
                            Decimal interesesAux  = newVIN.intereses_montoInteresPorDia * newVIN.intereses_diasEntreFechas_Vta_PagoTFS;
                            newVIN.intereses_totalInteres = interesesAux.setScale(1, RoundingMode.HALF_UP);
                            if(newVIN.intereses_totalInteres > 500){
                                newVIN.boolSolicitarNotaCreditoTFS = true;
                            } else {
                                newVIN.boolSolicitarNotaCreditoTFS = false;
                            }
                        }
                    }
                } else {
                    newVIN.lstMensajes = lstMensajesReglas;
                    newVIN.dblValorFactura = null;
                    newVIN.dblPrecioLista = null;
                    newVIN.dblPrecioMenosIncentivo = null;
                    newVIN.dblIncentivoPropuesto = null;
                    newVIN.dblIncentivoTMEX = null;
                    newVIN.dblIncentivoDealer = null;
                    newVIN.intPorcentajeParticipacionTMEX = null;
                    newVIN.dblIncentivoOtorgado =  null;
                    newVIN.dblIncentivoProporcionalTMEXSinIVA = null;
                    newVIN.solicitudAceptada = false;
                    newVIN.boolSolicitarNotaCreditoTFS = false;
                }
                wrapListVIN.add(newVIN); 
            }
        }
        return wrapListVIN;
    }
    
    //Wrapper
    public class wrapVIN {
        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String nombreDealer {get; set;}
        @AuraEnabled public String codigoVenta {get; set;} 
        @AuraEnabled public String VIN {get; set;} 
        @AuraEnabled public String VINID {get; set;} 
        @AuraEnabled public String Serie {get; set;}
        @AuraEnabled public String codigoModelo {get; set;}
        @AuraEnabled public String anioModelo {get; set;}
        @AuraEnabled public String idFacturaCarga {get; set;}
        @AuraEnabled public String idMovimiento{get; set;}
        @AuraEnabled public decimal precioPublicoVigente {get; set;} 
        @AuraEnabled public decimal precioPublicoDeVenta {get; set;}
        @AuraEnabled public Date fechaVentaVIN {get; set;}
        @AuraEnabled public Date fechaTimbradoXML {get; set;}
        @AuraEnabled public Set<String> lstMensajes {get; set;}
        @AuraEnabled public Boolean boolCode_06 {get;set;}
        @AuraEnabled public Double dblValorFactura {get;set;}
        @AuraEnabled public Double dblPrecioLista {get;set;}
        @AuraEnabled public Double dblPrecioMenosIncentivo {get;set;}
        @AuraEnabled public Double dblIncentivoPropuesto {get;set;}
        @AuraEnabled public Double dblIncentivoTMEX {get;set;}
        @AuraEnabled public Double dblIncentivoDealer {get;set;}
        @AuraEnabled public Integer intPorcentajeParticipacionTMEX {get;set;}
        @AuraEnabled public Double dblIncentivoOtorgado{get;set;}
        @AuraEnabled public Double dblIncentivoProporcionalTMEXSinIVA{get;set;}
        @AuraEnabled public Boolean solicitudAceptada {get;set;}
        @AuraEnabled public String idDetallePolitica {get;set;}
        @AuraEnabled public String idAutoDemo {get;set;} 
        //Intereses TFS
        @AuraEnabled public Date intereses_fechaVenta {get; set;}
        @AuraEnabled public Date intereses_fechaPagoTFS {get; set;}
        @AuraEnabled public Double intereses_montoInteresesTFS {get; set;}
        @AuraEnabled public Integer intereses_diasTranscurridosMes {get; set;}
        @AuraEnabled public Double intereses_montoInteresPorDia {get; set;}
        @AuraEnabled public Integer intereses_diasEntreFechas_Vta_PagoTFS {get; set;}
        @AuraEnabled public Decimal intereses_totalInteres {get; set;}
        @AuraEnabled public Boolean boolSolicitarNotaCreditoTFS {get;set;}
    }
    
    //Obtener Mapa de Incentivos Retail
    public static TAM_DetallePoliticaJunction__c getIncentivosRetail(String strAnio, String strMes, String strVIN){
        Integer intMes = Integer.valueOf(strMes);
        Integer intAnio = Integer.valueOf(strAnio);
       	String strAnioModelo = mapMovimientos.get(strVIN).TAM_AnioModelo__c;
        String strModelo = mapMovimientos.get(strVIN).TAM_codigoModelo__c;

        TAM_DetallePoliticaJunction__c objIncentivos;
        for(TAM_DetallePoliticaJunction__c objAUX:  [SELECT 	Id,
                                                        		TAM_AnioModelo__c,
                                                                TAM_Serie__c,
                                                                TAM_Clave__c,
                                                                Name,
                                                                TAM_MSRP__c,
                                                        		TAM_IncPropuesto_Cash__c,
                                                                TAM_IncentivoTMEX_Cash__c,
                                                        		TAM_IncDealer_Cash__c,
                                                                TAM_PoliticaIncentivos__r.Id,
                                                                TAM_PoliticaIncentivos__r.Name,
                                                                TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c,
                                                                TAM_PoliticaIncentivos__r.TAM_FinVigencia__c
                                                        FROM	  TAM_DetallePoliticaJunction__c
                                                        WHERE	  TAM_PoliticaIncentivos__r.RecordType.Name = 'Retail'
                                                        AND	  (TAM_PoliticaIncentivos__r.TAM_EstatusIncentivos__c = 'Vigente'
                                                               OR TAM_PoliticaIncentivos__r.TAM_EstatusIncentivos__c = 'Vencida')
                                                        AND     CALENDAR_MONTH(TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c) <=: intMes
                                                        AND     CALENDAR_YEAR(TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c) <=: intAnio
                                                        AND     CALENDAR_MONTH(TAM_PoliticaIncentivos__r.TAM_FinVigencia__c) >=: intMes
                                                        AND     CALENDAR_YEAR(TAM_PoliticaIncentivos__r.TAM_FinVigencia__c) >=: intAnio
                                                        AND	  TAM_IncentivoTMEX_Cash__c != null
                                                        AND	  TAM_AnioModelo__c =: strAnioModelo
                                                        AND	  TAM_Clave__c =: strModelo
                                                        ORDER BY TAM_IncPropuesto_Cash__c DESC LIMIT 1]){
            objIncentivos = objAUX;
        }
        return objIncentivos;
    } 

 
}