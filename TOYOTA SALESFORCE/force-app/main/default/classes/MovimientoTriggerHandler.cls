public class MovimientoTriggerHandler extends TriggerHandler {
    
    private List<Movimiento__c> listMovimientos;
    public MovimientoTriggerHandler(){
        this.listMovimientos = (List<Movimiento__c>) Trigger.new;
    }
    
    public override void beforeInsert(){
        validacionMovimientoManual(listMovimientos);
        validacionBefore(listMovimientos);
    }
    
    public override void beforeUpdate(){
        validacionBefore(listMovimientos);
    }
    
    public override void afterInsert(){
        validacionAfter(listMovimientos);
        actualizaDatosLead(listMovimientos);
    }
    
    public override void afterUpdate(){
        validacionAfter(listMovimientos);
        actualizaDatosLead(listMovimientos);        
    }
    
    public static void validacionMovimientoManual(List<Movimiento__c> items){
        RecordType movimientoManual = Constantes.TIPOS_REGISTRO_DEVNAME_ID.get('Movimiento__c').get('MovimientoManual');
        if(movimientoManual!=null){
            for(Movimiento__c m : items){
                if(movimientoManual.Id == m.RecordTypeId){
                    m.Submitted_Date__c = Datetime.now();
                    m.Submitted_Time__c = m.Submitted_Date__c.format('hh.mm.ss.SSSSSS'); //hh.mm.ss.000
                }
            }
        }
    }
    
    public static void  validacionBefore(List<Movimiento__c> items){
        System.debug('EN validacionBefore...');
        Set<String> setIdVeh = new Set<String>();
        Set<Id> idsModelos = new Set<Id>();
        Set<Id> idsDistribuidores = new Set<Id>();
        
        for(Movimiento__c m : items){
            if(m.VIN__c!=null){
                setIdVeh.add(m.VIN__c);
            }
            if(m.Id_Modelo__c!=null){
                idsModelos.add(m.Id_Modelo__c);
            }
            if(m.Distribuidor__c!=null){
                idsDistribuidores.add(m.Distribuidor__c);
            }
        }
        
        Map<Id,Modelo__c> mapModeloSerie = new Map<Id,Modelo__c>([SELECT Id, Name, Serie__c, Serie__r.Name FROM Modelo__c Where Id IN : idsModelos]);
        Map<Id,Account> mapDistribuidores = new Map<Id,Account>([SELECT Id, Codigo_Distribuidor__c FROM Account WHERE Id IN : idsDistribuidores]);
        
        Set<String> setIEObjetivos = new Set<String>();
        Map<String, Id> mapIEObjId = new Map<String, Id>();
        for(Movimiento__c m : items){
            if(m.Id_Modelo__c != null){
                setIEObjetivos.add(mapDistribuidores.get(m.Distribuidor__c).Codigo_Distribuidor__c + '-' + mapModeloSerie.get(m.Id_Modelo__c).Serie__r.Name + '-' +m.Submitted_Date__c.month() + '-' + m.Submitted_Date__c.year());
            }
            
        }
        
        for(Objetivo_de_Venta__c ob : [SELECT Id,Id_Externo_Objetivo__c From Objetivo_de_Venta__c WHERE Id_Externo_Objetivo__c IN : setIEObjetivos]){
            mapIEObjId.put(ob.Id_Externo_Objetivo__c,ob.Id);
        }
        
        for(Movimiento__c m : items){
            if(m.Id_Modelo__c != null){ 
                String cadena = mapDistribuidores.get(m.Distribuidor__c).Codigo_Distribuidor__c + '-' + mapModeloSerie.get(m.Id_Modelo__c).Serie__r.Name + '-' + m.Submitted_Date__c.month() + '-' + m.Submitted_Date__c.year();
                m.Objetivo_Venta__c = mapIEObjId.containsKey(cadena) ? mapIEObjId.get(cadena) : null;
            }
            
            //Actualiza el ID Externo para Flotillas
            if (m.Year__c != null && m.Month__c != null && m.Sale_Code__c != '06' && m.Sale_Code__c != '6' && m.Fleet__c == 'C'){
                String sIdExtFlot = m.Year__c + '-' + m.Month__c + '-' + m.Sale_Code__c + '-' + m.Fleet__c; 
                m.TAM_IdExternoIncentivosFlot__c = sIdExtFlot;
                System.debug('EN validacionBefore m.TAM_IdExternoIncentivosFlot__c: ' + m.TAM_IdExternoIncentivosFlot__c);
            }
            
        }
    }
    
    public static void  validacionAfter(List<Movimiento__c> items){
        System.debug('En MovimientoTriggerHandler validacionAfter...');
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        Set<String> setIdVeh = new Set<String>();
        Map<String, Movimiento__c> mapIdVeh = new Map<String, Movimiento__c>();
        Set<String> setVin = new Set<String>();
        Set<Id> idsModelos = new Set<Id>();
        Set<Id> idsDistribuidores = new Set<Id>();
        //Map<Id,Vehiculo__c> mapVehiculos = new Map<Id,Vehiculo__c>([SELECT Id, Id_Externo__c, Ultimo_Movimiento__c, Ultimo_Movimiento__r.Submitted_Date__c, Ultimo_Movimiento__r.Submitted_Time__c FROM Vehiculo__c WHERE Id IN : setIdVeh]);
        Map<Id,Movimiento__c> mapMovs = new Map<Id,Movimiento__c>();
        Map<Id,Vehiculo__c> mapVehiculos = new Map<Id,Vehiculo__c>();
        Map<Id,Vehiculo__c> mapVehiculosUps = new Map<Id,Vehiculo__c>();
        
        for(Movimiento__c m : items){
            if(m.VIN__c != null)
                mapIdVeh.put(m.VIN__c, m);

            if(m.Id_Modelo__c!=null)
                idsModelos.add(m.Id_Modelo__c);

            if(m.Distribuidor__c!=null)
                idsDistribuidores.add(m.Distribuidor__c);
        }
        System.debug('En MovimientoTriggerHandler mapIdVeh: ' + mapIdVeh.KeySet());
        System.debug('En MovimientoTriggerHandler mapIdVeh: ' + mapIdVeh.Values());
        
        //Consulta todos los Vehiculos asociados a los vines: mapIdVeh
        for (Vehiculo__c objVehiculo : [Select id,
             (Select id, VIN__c, VIN__r.Name From Movimientos__r Where RecordTypeId =: strRecordTypeId)
             From Vehiculo__c Where id IN :mapIdVeh.KeySet()]){
            //Agrega el obj de objVehiculo al mapa de mapVehiculos
            mapVehiculos.put(objVehiculo.id, objVehiculo);
            if (!objVehiculo.Movimientos__r.isEmpty()){
                for(Movimiento__c objMov : objVehiculo.Movimientos__r){     
                    if (objMov.VIN__c != null)
                        mapMovs.put(objMov.id, objMov);
                }
            }      
        }
        System.debug('En MovimientoTriggerHandler mapMovs: ' + mapMovs.KeySet());
        System.debug('En MovimientoTriggerHandler mapMovs: ' + mapMovs.Values());
        
        Set<String> setMovFal = new Set<String>();
        //Recorre todos los reg de los nuevos mov mapIdVeh de y buscalos en mapMovs
        for (Movimiento__c mov : mapIdVeh.Values()){
            if (!mapMovs.containsKey(mov.id))
                if (mov.VIN__c != null)
                    setMovFal.add(mov.id);
        }
        System.debug('En MovimientoTriggerHandler setMovFal: ' + setMovFal);

        for (Movimiento__c mov : [Select id, VIN__c, VIN__r.Name From Movimiento__c where id IN :setMovFal]){
            mapMovs.put(mov.id, mov);            
        }
        System.debug('En MovimientoTriggerHandler mapMovs: ' + mapMovs.KeySet());
        System.debug('En MovimientoTriggerHandler mapMovs: ' + mapMovs.Values());
        
        Map<Id,Modelo__c> mapModeloSerie = new Map<Id,Modelo__c>([SELECT Id, Name, Serie__c, Serie__r.Name FROM Modelo__c Where Id IN : idsModelos]);
        Map<Id,Account> mapDistribuidores = new Map<Id,Account>([SELECT Id, Codigo_Distribuidor__c FROM Account WHERE Id IN : idsDistribuidores]);
                
        for(Movimiento__c m : items){
            //Manda llamar la función de TAM_getUltimoMovVin.getUltimoMovVin(ventas.Name, dFechaFinConsultaMov, ventas.Fecha_de_Venta__c, False);
            if (!Test.isRunningTest()){
                System.debug('En MovimientoTriggerHandler m.id: ' + m.id + ' Map: ' + mapMovs.get(m.id));
                String sVinPaso = mapMovs.containsKey(m.id) ? mapMovs.get(m.id).VIN__r.Name : null;
                System.debug('EN MovimientoTriggerHandler sVinPaso: ' + sVinPaso);
	            Movimiento__c objMov = TAM_getUltimoMovVin.getUltimoMovVin(sVinPaso, m.Submitted_Date__c.Date(), m.Submitted_Date__c.Date(), True);
                if (objMov.id == null)
                    objMov = m;    
	            Vehiculo__c vehic = mapVehiculos.get(m.VIN__c);
	            vehic.Ultimo_Movimiento__c = objMov.Id;
	            vehic.Modelo_b__c = objMov.Id_Modelo__c;
	            vehic.Serie_b__c = mapModeloSerie.containsKey(objMov.Id_Modelo__c) ? mapModeloSerie.get(objMov.Id_Modelo__c).Serie__c : null;
	            vehic.Distribuidor__c = objMov.Distribuidor__c;
	            vehic.Objetivo_Venta__c = objMov.Objetivo_Venta__c;
	            System.debug('EN MovimientoTriggerHandler vehic: ' + vehic);            
	            //Agregalo al mapa de mapVehiculosUps
	            mapVehiculosUps.put(vehic.id, vehic);
            }//Fin si !Test.isRunningTest()
            
            /*Vehiculo__c v = mapVehiculos.get(m.VIN__c);
            if(v.Ultimo_Movimiento__c==null || 
               m.Submitted_Date__c > v.Ultimo_Movimiento__r.Submitted_Date__c ||
               (
                   m.Submitted_Date__c == v.Ultimo_Movimiento__r.Submitted_Date__c &&
                   m.Submitted_Time__c >= v.Ultimo_Movimiento__r.Submitted_Time__c
               )
              ){
                  v.Ultimo_Movimiento__c = m.Id;
                  v.Modelo_b__c = m.Id_Modelo__c;
                  v.Serie_b__c = mapModeloSerie.containsKey(m.Id_Modelo__c)? mapModeloSerie.get(m.Id_Modelo__c).Serie__c : null;
                  v.Distribuidor__c = m.Distribuidor__c;
                  v.Objetivo_Venta__c = m.Objetivo_Venta__c;
                  updVehiculos.put(v.Id, v);
                  mapVehiculos.put(v.Id, v);
              }*/
        }//Fin del for para los movimientos
       
        /*if(updVehiculos.size()>0){
            update updVehiculos.values();
        }*/

        //Ve si tiene algo
        if(!mapVehiculosUps.isEmpty()){
            //update mapVehiculosUps.values();
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapVehiculosUps.values(), Vehiculo__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN MovimientoTriggerHandler Hubo un error a la hora de crear/Actualizar los registros en Vehiculo__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                //if (objDtbUpsRes.isSuccess())
                //   System.debug('EN MovimientoTriggerHandler Los datos de los Modelos seleccionados se crear/Actualizar con exito ID: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si mapVehiculosUps.isEmpty()

    }
        
    private void actualizaDatosLead(List<Movimiento__c> ventasList){
    
        Set<String> setIdVinActCand = new Set<String>();
        map<String, Lead> mapLeadUpsUps = new map<String, Lead>();
        Set<String> setErroresLeadErr = new Set<String>();
        Set<String> setIdLeadSucc = new Set<String>();              

        map<String, TAM_LeadInventarios__c> mapLeadInvComs = new map<String, TAM_LeadInventarios__c>();
        map<String, String> mapVinNomCte = new map<String, String>();
        map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new map<String, TAM_LeadInventarios__c>();

        //Recorre la lista de reg que se estan nsertando
        for(Movimiento__c v: ventasList){                
            setIdVinActCand.add(v.ID);
        }        
        System.debug('EN actualizaDatosLead setIdVinActCand: '+ setIdVinActCand);

        for (Movimiento__c movCons : [Select m.Name, m.VIN__c, m.VIN__r.Name, m.NombreCliente__c From Movimiento__c m
            Where id IN :setIdVinActCand]){
            if(movCons.VIN__c != null)
               mapVinNomCte.put(movCons.VIN__r.Name, movCons.NombreCliente__c);
        }
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.KeySet());         
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.Values()); 

        //Consulta los datos de los vines que ya estan cargados
        for (TAM_LeadInventarios__c objLeadInventarios : [Select t.id, t.TAM_VINFacturacion__c, t.TAM_Prospecto__c, t.Name 
            From TAM_LeadInventarios__c t Where (Name IN :mapVinNomCte.KeySet() OR TAM_VINFacturacion__c IN :mapVinNomCte.KeySet())
            And TAM_Prospecto__c != null]){
            Lead CandUpd = new Lead(
                id = objLeadInventarios.TAM_Prospecto__c, TAM_PedidoReportado__c = true, 
                Status = 'Entregado', FWY_Estado_del_candidatoWEB__c = 'Entregado', 
                TAM_PorcentajeAvance__c = '100%'
            );
            //mete los datos en el mapa de mapIdInveCand para mandar llamar el proceso ActualizaVinCandito
            mapLeadUpsUps.put(objLeadInventarios.TAM_Prospecto__c, CandUpd);     
            mapLeadInvComs.put(objLeadInventarios.Name, objLeadInventarios);
            if (objLeadInventarios.TAM_VINFacturacion__c != null)
                mapLeadInvComs.put(objLeadInventarios.TAM_VINFacturacion__c, objLeadInventarios);
        }    
        System.debug('EN actualizaDatosLead mapLeadUpsUps: '+ mapLeadUpsUps);
        System.debug('EN actualizaDatosLead mapLeadInvComs: '+ mapLeadInvComs);

        //Ve si tiene algo el mapa de  mapLeadUpsUps             
        //mapLeadInvComs.put(objLeadInventarios.id, objLeadInventarios);
        if (!mapLeadUpsUps.isEmpty()){
                                                
           List<Lead> lLeadUpd = mapLeadUpsUps.values();
           //Recorre la lista de lLeadUpd y actualiza el campo de TAM_NombreClienteHistorico__c
           for (Lead CandUpd : lLeadUpd){
                //Busca el lead en el mapa de mapLeadInvComs
                if (mapLeadUpsUps.containsKey(CandUpd.id)){
                   String sVinLead = '';
                   String sVinLeadFact = '';
                   //Toma el nombre del objeto  objLeadInventarios
                   for (TAM_LeadInventarios__c objLeadInv : mapLeadInvComs.Values()){
                        if (objLeadInv.TAM_Prospecto__c == CandUpd.id){
                            sVinLead = objLeadInv.Name;
                            sVinLeadFact = objLeadInv.TAM_VINFacturacion__c != null ? objLeadInv.TAM_VINFacturacion__c : objLeadInv.Name;
                            break;
                        }//Fin si objLeadInv.TAM_Prospecto__c == CandUpd.id
                   }//Fin si 
                   TAM_LeadInventarios__c objLeadInvUpd;
                   //Busca el cliente asociado a sVinLead o sVinLeadFact
                   if (mapVinNomCte.containsKey(sVinLead)){ 
                        objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLead).id);
                        objLeadInvUpd.TAM_NombreClienteHistorico__c = mapVinNomCte.get(sVinLead);
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   if (!mapVinNomCte.containsKey(sVinLeadFact)) {
                      if (mapVinNomCte.containsKey(sVinLeadFact)){ 
                          objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLeadFact).id);
                          objLeadInvUpd.TAM_NombreClienteHistorico__c = mapVinNomCte.get(sVinLeadFact);
                      }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   System.debug('EN actualizaDatosLead objLeadInvUpd: ' + objLeadInvUpd);
                   //Ve si tiene algo el obj de objLeadInvUpd
                   if (objLeadInvUpd != null)
                      mapLeadInvUpd.put(objLeadInvUpd.id, objLeadInvUpd);  
                }//Fin si mapLeadInvComs.containsKey(CandUpd.id)
           }//Fin del for para lLeadUpd
                
           Integer icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lLeadUpd, Lead.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
               if (!objDtbUpsRes.isSuccess())
                   setErroresLeadErr.add('En Lead : ' + lLeadUpd.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbUpsRes              

           icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbLiUpsRes = Database.upsert(mapLeadInvUpd.values(), TAM_LeadInventarios__c.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbLiUpsRes){
               if (!objDtbUpsRes.isSuccess())
                  setErroresLeadErr.add('En Lead : ' + mapLeadInvUpd.values().get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbLiUpsRes              

        }//Fin si !mapLeadUpsUps.isEmpty()
        System.debug('EN actualizaDatosLead setErroresLeadErr: ' + setErroresLeadErr);         
        System.debug('EN actualizaDatosLead setIdLeadSucc: ' + setIdLeadSucc);
    
    }

}