global class DeleteVentasH_Batch implements Database.Batchable<sObject> {
	
    private String query;
    private List<Venta__c> ventasList;
	
	global DeleteVentasH_Batch(String q) {
        this.query = q;
        this.ventasList = new List<Venta__c>();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(scope.size() > 0){
            ventasList.addAll((List<Venta__c>)scope);

            try{
                delete ventasList;
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('Deleted VentasH Batch finished');
	}
	
}