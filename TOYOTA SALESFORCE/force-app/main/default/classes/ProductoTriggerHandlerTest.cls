@isTest
public class ProductoTriggerHandlerTest {
    
    @testSetup static void testLoadData(){
        Serie__c serieNew = new Serie__c();
        serieNew.name = 'Corolla';
        serieNew.Marca__c = 'Toyota';
        serieNew.Id_Externo_Serie__c = 'Toyota2019';
        insert serieNew;
        
        Product2 productNew = new Product2();
        productNew.Name = 'Corolla';
        productNew.Anio__c = '2019';
        productNew.ProductCode = '4010';
        productNew.Comentario__c = 'Test 2019';
        productNew.RecordTypeId = '0121Y000001clPwQAI';
        productNew.Serie__c = serieNew.id;
        productNew.Family = 'Toyota';
        productNew.NombreVersion__c = 'Confortline';
        Insert productNew;
        
    }    
    
    
    @isTest static void methodOne(){
        Product2 prodUpd = [Select id,name from Product2 WHERE name = 'Corolla'];
        prodUpd.name = 'Corolla 2019';
        update prodUpd;
        
    }
    
}