/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de Lead de Sirena

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    26-Julio-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SirenaWrpLead {

    /*
    {
    "id": "60fda1356e4b97000864dab6",
    "created": "2021-07-25T17:36:53.908Z",
    "transferredAt": null,
    "group": "Toyota Santa Fe & Puerta SantaFe",
    "groupId": "5a99b3367c6e2a005f56f129",
    "account": "Continental",
    "accountId": "586fb49a1da60a04005e3a2d",
    "initialGroup": "Toyota Santa Fe & Puerta SantaFe",
    "initialGroupId": "5a99b3367c6e2a005f56f129",
    "firstName": "Lenin",
    "lastName": "García Fernández",
    "nin": [],
    "userMade": false,
    "status": "followUp",
    "additionalData": {},
    "phones": [
        "+525562555060"
    ],
    "emails": [
        "len_garci@hotmail.com"
    ],
    "contactMediums": [
        {
            "_id": "60fda1356e4b97000864dabe",
            "type": "email"
        },
        {
            "_id": "60fda1356e4b97000864dabf",
            "type": "phone"
        }
    ],
    "leads": [
        {
            "created": "2021-07-25T17:36:53.968Z",
            "source": "GOOGLE",
            "campaign": "DTM | Google |  Julio 2021",
            "utmSource": "GOOGLE",
            "utmCampaign": "DTM | Google |  Julio 2021",
            "providerKey": "api",
            "product": {
                "make": "Toyota",
                "model": "Prius C"
            },
            "comments": "Forma de contacto preferida: Whatsapp, Tiempo estimado de compra: 1-3 meses.",
            "type": "savingPlan"
        }
    ],
    "whatsAppDueAt": "2021-07-26T20:05:41.770Z",
    "agent": {
        "id": "602c7112c606d200048d1130",
        "firstName": "Julio",
        "lastName": "Ávila",
        "phone": "+525534350884",
        "email": "julio.avila@toyotasantafe.com.mx",
        "appFields": {}
    },
    "assigned": "2021-07-25T17:48:44.282Z",
    "firstContactedAt": "2021-07-25T17:36:54.046Z"
    */
    
    //Una clase para deserarilzar los datos de SirenaWrpLead
    public class listaSirenaWrpLead{
        public List<SirenaWrpLead> candidatos;    
    }

    public class SirenaWrpLead{
        @AuraEnabled public String id {get;set;} //"60fda1356e4b97000864dab6",
        @AuraEnabled public String created {get;set;} //"2021-07-25T17:36:53.908Z",
        @AuraEnabled public String firstName {get;set;} //"Lenin",
        @AuraEnabled public String lastName {get;set;} //"García Fernández",
        @AuraEnabled public List<String> phones {get;set;} //[ +525562555060" ],
        @AuraEnabled public List<String> emails {get;set;} //[ "len_garci@hotmail.com" ],
        @AuraEnabled public List<contactMediumsObj> contactMediums {get;set;} //[ {"_id": "60fda1356e4b97000864dabe", "type": "email" }, {"_id": "60fda1356e4b97000864dabf", "type": "phone" }]
        @AuraEnabled public List<leadObj> leads {get;set;}
        @AuraEnabled public String assigned {get;set;} //"2021-07-25T17:48:44.282Z",
        @AuraEnabled public String firstContactedAt {get;set;} //"2021-07-25T17:36:54.046Z"
        //Un contructor por default
        public SirenaWrpLead(){
	        this.id = '';
	        this.created = ''; 
	        this.firstName = '';
	        this.lastName = '';
	        this.phones = new List<String>();
	        this.emails = new List<String>();
	        this.contactMediums = new List<contactMediumsObj>();
	        this.leads = new List<leadObj>();
	        this.assigned = ''; //"2021-07-25T17:48:44.282Z",
	        this.firstContactedAt = ''; //"2021-07-25T17:36:54.046Z"
        }        
    }

    public class contactMediumsObj{
        @AuraEnabled public String id {get;set;}        
        @AuraEnabled public String type {get;set;}    
        //Un contructor por default
        public contactMediumsObj(){
            this.id = '';
            this.type = '';
        }
    }

    public class productObj{
        @AuraEnabled public String make {get;set;}        
        @AuraEnabled public String model {get;set;}    
        //Un contructor por default
        public productObj(){
            this.make = '';
            this.model = '';
        }
    }
        
    public class leadObj{
        @AuraEnabled public String created {get;set;}
        @AuraEnabled public String source {get;set;}
        @AuraEnabled public String campaign {get;set;}
        @AuraEnabled public String utmSource {get;set;}
        @AuraEnabled public String utmCampaign {get;set;}
        @AuraEnabled public productObj product {get;set;}
        @AuraEnabled public String comments {get;set;}
        @AuraEnabled public String type {get;set;}
        //Un contructor por default
        public leadObj(){
	        this.created = '';
	        this.source = '';
	        this.campaign = '';
	        this.utmSource = '';
	        this.utmCampaign = '';
	        this.product = new productObj(); 
	        this.comments = '';
	        this.type = '';
        }
    }
    



    //Una clase para deserarilzar los datos de listaSirenaWrpLeadInteracciones
    public class listaSirenaWrpLeadInteracciones{
        public List<SirenaWrpLeadInteraccion> interacciones;    
    }

    public class SirenaWrpLeadInteraccion{
        @AuraEnabled public String id {get;set;} //"60fda1356e4b97000864dab6",
        @AuraEnabled public String createdAt {get;set;} //"2021-07-25T17:36:53.908Z",
        @AuraEnabled public String finishedAt {get;set;} //"2021-07-25T17:36:53.908Z",
        @AuraEnabled public String status {get;set;} //"Lenin",
        @AuraEnabled public String prospectId {get;set;} //"García Fernández",
        @AuraEnabled public outputObj output {get;set;} //[ {"_id": "60fda1356e4b97000864dabe", "type": "email" }, {"_id": "60fda1356e4b97000864dabf", "type": "phone" }]
        @AuraEnabled public String via {get;set;} //"2021-07-25T17:36:54.046Z"
        //Un contructor por default
        public SirenaWrpLeadInteraccion(){
            this.id = '';
            this.createdAt = ''; 
            this.status = '';
            this.prospectId = '';
            this.output = new outputObj();
            this.via = '';
        }        
    }

    public class outputObj{
        @AuraEnabled public String via {get;set;}
        @AuraEnabled public String body {get;set;}
        @AuraEnabled public String comment {get;set;}
        @AuraEnabled public Boolean delivered {get;set;}
        @AuraEnabled public questionObj question {get;set;}
        @AuraEnabled public messageObj message {get;set;}
        @AuraEnabled public List<conversationsObj> conversations {get;set;}
        //Un contructor por default
        public outputObj(){
            this.via = '';
            this.body = '';
            this.comment = '';
            this.delivered = false;
            this.question = new questionObj();
            this.message = new messageObj();
            this.conversations = new List<conversationsObj>();
        }
    }

    public class questionObj{
        @AuraEnabled public String id {get;set;}        
        @AuraEnabled public String question {get;set;}    
        @AuraEnabled public String answer {get;set;}    
        @AuraEnabled public Boolean delivered {get;set;}
        //Un contructor por default
        public questionObj(){
            this.id = '';
            this.question = '';
            this.answer = '';
            this.delivered = false;
        }
    }

    public class messageObj{
        @AuraEnabled public String body {get;set;}        
        @AuraEnabled public Boolean delivered {get;set;}
        @AuraEnabled public String via {get;set;}
        //Un contructor por default
        public messageObj(){
            this.body = '';
            this.delivered = false;
            this.via = '';
        }
    }

    public class conversationsObj{
        @AuraEnabled public String id {get;set;}        
        @AuraEnabled public String channel {get;set;}
        @AuraEnabled public String threadTitle {get;set;}
        @AuraEnabled public String threadSubtitle {get;set;}
        //Un contructor por default
        public conversationsObj(){
            this.id = '';
            this.channel = '';
            this.threadTitle = '';
            this.threadSubtitle = '';
        }
    }



/*
[
    {
        "id": "60fda1366e4b97000864dac7",
        "createdAt": "2021-07-25T17:36:54.046Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "finishedAt": "2021-07-25T17:41:13.103Z",
        "proactive": true,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "Hola!. Gracias por conectarse con  Toyota Santa Fe & Puerta SantaFe . Recibimos su consulta correctamente. ¿Cómo podemos ayudar?",
                "content": "Hola!. Gracias por conectarse con  Toyota Santa Fe & Puerta SantaFe . Recibimos su consulta correctamente. ¿Cómo podemos ayudar?",
                "performer": "agent",
                "delivered": true,
                "providerId": "60fda1366e4b97000864dac5",
                "sender": "5215589202565",
                "recipient": "5215562555060",
                "template": "continental-mensaje-bienvenida-15959549612461",
                "templateParameters": {
                    "groupName": "Toyota Santa Fe & Puerta SantaFe"
                },
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fda1366e4b97000864dacd",
        "createdAt": "2021-07-25T17:36:54.107Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "finishedAt": "2021-07-25T17:41:35.569Z",
        "proactive": true,
        "output": {},
        "via": "other"
    },
    {
        "id": "60fda24f41f9b5000834b9b2",
        "createdAt": "2021-07-25T17:41:35.544Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "finishedAt": "2021-07-25T17:41:35.544Z",
        "proactive": true,
        "output": {},
        "via": "other"
    },
    {
        "id": "60fda24f41f9b5000834b9b8",
        "createdAt": "2021-07-25T17:41:35.624Z",
        "status": "created",
        "prospectId": "60fda1356e4b97000864dab6",
        "finishedAt": "2021-07-25T17:41:35.624Z",
        "proactive": false,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "Me gustaría saber los requisitos para adquirir un auto",
                "content": "Me gustaría saber los requisitos para adquirir un auto",
                "performer": "integration",
                "delivered": false,
                "providerId": "60fda24fcc13eb0014648131",
                "sender": "5215562555060",
                "recipient": "5215589202565",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fda3ff7c1b7b0008efc088",
        "createdAt": "2021-07-25T17:48:47.722Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T17:48:59.305Z",
        "proactive": true,
        "output": {
            "callSuccessful": false
        },
        "via": "outboundCall"
    },
    {
        "id": "60fda4270cf041000866cb00",
        "createdAt": "2021-07-25T17:49:27.403Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:27.089Z",
        "proactive": true,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "Hola Lenin buenas tardes.\nSoy Julio Ávila y tendré el gusto de atenderte",
                "content": "Hola Lenin buenas tardes.\nSoy Julio Ávila y tendré el gusto de atenderte",
                "performer": "agent",
                "delivered": true,
                "providerId": "60fda4251ed0dd0e86d3479d",
                "sender": "5215589202565",
                "recipient": "5215562555060",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fda43e6aa1260009a5a7f1",
        "createdAt": "2021-07-25T17:49:50.559Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:27.568Z",
        "proactive": true,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "Te interesa adquirir tu auto a crédito o de contado?",
                "content": "Te interesa adquirir tu auto a crédito o de contado?",
                "performer": "agent",
                "delivered": true,
                "providerId": "60fda43c1ed0dd0e86d3479e",
                "sender": "5215589202565",
                "recipient": "5215562555060",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fda45781fb4300084424e9",
        "createdAt": "2021-07-25T17:50:15.980Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:26.660Z",
        "proactive": true,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "En funcion a eso son los requisitos necesarios",
                "content": "En funcion a eso son los requisitos necesarios",
                "performer": "agent",
                "delivered": true,
                "providerId": "60fda4561ed0dd0e86d3479f",
                "sender": "5215589202565",
                "recipient": "5215562555060",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fdb395eacb6a0008dd07b1",
        "createdAt": "2021-07-25T18:55:17.220Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:34.957Z",
        "proactive": true,
        "output": {},
        "via": "other"
    },
    {
        "id": "60fdc40ef15e83000801766b",
        "createdAt": "2021-07-25T20:05:34.902Z",
        "status": "updated",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:34.902Z",
        "proactive": true,
        "output": {},
        "via": "other"
    },
    {
        "id": "60fdc40ef15e830008017672",
        "createdAt": "2021-07-25T20:05:34.998Z",
        "status": "created",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:34.998Z",
        "proactive": false,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "A crédito",
                "content": "A crédito",
                "performer": "integration",
                "delivered": false,
                "providerId": "60fdc40e8b5d9d0014712acc",
                "sender": "5215562555060",
                "recipient": "5215589202565",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    },
    {
        "id": "60fdc4158318ef0008612050",
        "createdAt": "2021-07-25T20:05:41.770Z",
        "status": "created",
        "prospectId": "60fda1356e4b97000864dab6",
        "agentId": "602c7112c606d200048d1130",
        "agent": {
            "id": "602c7112c606d200048d1130",
            "firstName": "Julio",
            "lastName": "Ávila",
            "phone": "+525534350884",
            "email": "julio.avila@toyotasantafe.com.mx",
            "appFields": {}
        },
        "finishedAt": "2021-07-25T20:05:41.770Z",
        "proactive": false,
        "output": {
            "message": {
                "via": "whatsApp",
                "body": "Una avanza",
                "content": "Una avanza",
                "performer": "integration",
                "delivered": false,
                "providerId": "60fdc415a780ea0014bad035",
                "sender": "5215562555060",
                "recipient": "5215589202565",
                "templateConfig": null
            }
        },
        "via": "whatsApp"
    }
]
*/    
    
    
}