public without sharing class SolicitudAutoDemoTriggerHandler extends TriggerHandler{
    private List<SolicitudAutoDemo__c> autosDemoList;
    private User currentUser;
    private Contact partnerContact;
    private Map<String,Map<String,RecordType>> recordTypesMap;
    private Map<String, Map<String, Decimal>> montosUtilizadosMap;
    
    public SolicitudAutoDemoTriggerHandler() {
        this.autosDemoList = (List<SolicitudAutoDemo__c>) Trigger.new;
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        this.montosUtilizadosMap = new Map<String, Map<String, Decimal>>();
    }
    
    public override void beforeInsert(){        
        validateAutosDemo(autosDemoList);
        IDExternoAutoDemo(autosDemoList);  
        
    }
    
    public override void beforeUpdate(){
        validateTMEXTFS(autosDemoList); 
        IDExternoAutoDemo(autosDemoList);  
        validateEstatusBaja(autosDemoList);
        
    }
    
    public override void afterUpdate(){
        actualizaVIN(autosDemoList);
        
    }
    
    private void validateAutosDemo(List<SolicitudAutoDemo__c> solicitudes){
        currentUser = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];        
        Map<String, Vehiculo__c> vinesMap = new Map<String, Vehiculo__c>();
        Map<Id, Vehiculo__c> vinesMapById = new Map<Id, Vehiculo__c>();
        
        Map<Id, Account> dealersMap;
        Map<Id, CostoSerie__c> costoSeriesMap;
        Map<String, List<Date>> mesesCortes;
        Map<Id, Account> mapaAsignaciones;

        if(currentUser.ContactId!=null){
            partnerContact= [SELECT Id, LastName, AccountId FROM Contact WHERE Id=: currentUser.ContactId LIMIT 1];
            dealersMap = AutoDemoTriggerMethods.getDealerInfoMap(new Set<Id>{partnerContact.AccountId});
            mapaAsignaciones = AutoDemoTriggerMethods.getAsignacionesMap(new Set<String>{partnerContact.AccountId});

            Account dealer = dealersMap.get(partnerContact.AccountId);        
            Integer unidadesActivas = 0;
            
            for(SolicitudAutoDemo__c sad: solicitudes){
                System.debug(sad.VIN_Name__c);
                if(sad.VIN_Name__c!=null){                    
                    if(!vinesMap.containsKey(sad.VIN_Name__c)){
                        vinesMap.put(sad.VIN_Name__c, new Vehiculo__c(
                            Name = sad.VIN_Name__c,
                            Id_Externo__c = sad.VIN_Name__c,
                            Distribuidor__c = dealer.Id,
                            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id
                        ));
                    }                    
                } else{
                    sad.addError('La solicitud no tiene VIN');
                }
            }
            
            if(vinesMap.size() > 0){
                try{
                    upsert vinesMap.values() Id_Externo__c;                
                    vinesMap = AutoDemoTriggerMethods.getVinesInfoByName(vinesMap.keySet());    
                    
                    Set<String> costosSerieIDS = new Set<String>();
                    for(Vehiculo__c v: vinesMap.values()){
                        if(v.Producto__r.Serie__r.InteresSerieActiva__c != null){
                            costosSerieIDS.add(v.Producto__r.Serie__r.InteresSerieActiva__c);
                        }
                    }

                    costoSeriesMap = AutoDemoTriggerMethods.loadCostosSerie(costosSerieIDS);
                    Map<String, Boolean> mesDentroRango;

                    for(SolicitudAutoDemo__c sad: solicitudes){                    
                        if(dealer.UnidadesAutosDemoAutorizadas__c != null){
                            List<SolicitudAutoDemo__c> autosDemoDealer = dealer.SolicitudesAutosDemo__r;
                            unidadesActivas = autosDemoDealer.size();                        
                            
                            if(unidadesActivas+1 <= Integer.valueOf(dealer.UnidadesAutosDemoAutorizadas__c)){                            
                                Vehiculo__c vin = vinesMap.get(sad.VIN_Name__c);                                
                                List<SolicitudAutoDemo__c> solicitudesAsociadosVIN = vin.Solicitudes_autos_demo2__r;                            
                                
                                if(solicitudesAsociadosVIN.size()> 0){                                
                                    sad.addError('Este Vehiculo no puede ser auto demo "Tiene solicitudes pendientes o ya ha sido auto demo"'); 
                                    
                                } else{
                                    mesesCortes = AutoDemoTriggerMethods.cortesToCreate(Date.today(), Date.today().addMonths(3));
                                    mesDentroRango = new Map<String, Boolean>();
                                    System.debug(JSON.serialize(mesesCortes));

                                    if(mapaAsignaciones.containsKey(dealer.Id) && mapaAsignaciones.get(dealer.Id).AsignacionPresupuesto__r.size() > 0){

                                            for(AsignacionPresupuesto__c ap: mapaAsignaciones.get(dealer.Id).AsignacionPresupuesto__r){
                                                for(String mes: mesesCortes.keySet()){
                                                    if(!mesDentroRango.containsKey(mes)){
                                                        mesDentroRango.put(mes, false);
                                                    }

                                                    if(AutoDemoTriggerMethods.dentroDelRango(mesesCortes.get(mes), ap.InicioFY__c, ap.FinFY__c)){
                                                        mesDentroRango.put(mes, true);

                                                        if(vin.Producto__r.Serie__r.InteresSerieActiva__c != null){
                                                            List<Decimal> montosAP = AutoDemoTriggerMethods.getMontosMesAP(mes, ap);                                                        
                                                            Decimal costoSerie = AutoDemoTriggerMethods.getCostoPromedioMes(mes, costoSeriesMap.get(vin.Producto__r.Serie__r.InteresSerieActiva__c));                                                        
                                                            //System.debug(costoSerie);
                                                            Decimal utilizado = montosAP.get(0);
                                                            Decimal asignado = montosAP.get(1);
                                                            //System.debug(utilizado);
                                                            //System.debug(asignado);
                                                            //System.debug((costoSerie + utilizado) < asignado);
                                                            if((costoSerie + utilizado) <= asignado){                                                                                            
                                                                if(vin.EstatusVINAutoDemo__c!= null && vin.EstatusVINAutoDemo__c == 'H6'){
                                                                    sad.Estatus__c = Constantes.AUTO_DEMO_SIN_SOLICITUD;
                                                                    sad.VIN__c= vin.Id;
                                                                    sad.IdVIN__c = vin.Name;
                                                                    sad.Distribuidor__c = dealer.Id;
                                                                    sad.CdigoDistribuidor__c = dealer.Name;
                                                                    sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get('SolicitudltaAltaAutosDemo').Id;
                                                                    
                                                                } else{
                                                                    
                                                                    sad.Estatus__c = Constantes.AUTO_DEMO_SOLICITUD_ALTA;
                                                                    sad.VIN__c= vin.Id;
                                                                    sad.IdVIN__c = vin.Name;
                                                                    sad.Distribuidor__c = dealer.Id;
                                                                    sad.CdigoDistribuidor__c = dealer.Name;
                                                                    sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get('SolicitudltaAltaAutosDemo').Id;
                                                                }

                                                            } else {
                                                                sad.addError('Estas excediendo el límite de tu presupuesto TEST. Contacta con tu DM.');
                                                            }

                                                        } else {
                                                            sad.addError('VIN no tiene interes de serie activa.');
                                                        }
                                                    }            
                                                }
                                            }

                                            System.debug(JSON.serialize(mesDentroRango));
                                            for(String mes : mesDentroRango.keySet()){
                                                if(!mesDentroRango.get(mes)){
                                                    sad.addError(mes + ' no esta dentro del rango de asignacion.');
                                                }
                                            }

                                    } else {
                                        sad.addError('No existe asignaciones para el distribuidor.');
                                    }
                                                                      
                                }
                                
                            } else{                        
                                
                                sad.addError('Auto demo no permitido: Excede el número de unidades autorizadas');
                            }   
                            
                        } else{
                            sad.addError('Distribuidor no tiene Unidades autorizadas');
                        }
                    }
                    
                    
                } catch(Exception ex){
                    System.debug(ex.getMessage());
                }
            }

            
        } else{//Fin Usuario Comunidad
            
            Set<Id> vinesID = new Set<Id>();            
            for(SolicitudAutoDemo__c sad: solicitudes){
                if(sad.VIN__c!=null){
                    vinesID.add(sad.VIN__c);
                }
            }
            
            vinesMapById = AutoDemoTriggerMethods.getVinesInfoById(vinesID);
            
            for(SolicitudAutoDemo__c sad: solicitudes){
                if(sad.VIN__c!=null){
                    Vehiculo__c vin = vinesMapById.get(sad.VIN__c);
                    List<SolicitudAutoDemo__c> autosDemoVIN = vin.Solicitudes_autos_demo2__r;
                    if(autosDemoVIN.size()> 0){
                        sad.addError('Este Vehiculo no puede ser auto demo "Tiene solicitudes pendientes o ya ha sido auto demo" ');
                    }
                    
                } else{
                    sad.addError('Solicitud no tiene un id de VIN asociado');        
                }
            }
        
        }
        
    }
    
    private void validateTMEXTFS(List<SolicitudAutoDemo__c> solicitudes){   
        System.debug('Entraste al metodo beforeupdate VALIDA TFS Y TMEX');                
        for(SolicitudAutoDemo__c sad: solicitudes){
            System.debug(sad.SolicitudAutoDemo__c);
            if(sad.SolicitudAutoDemo__c!='true'){
                if(sad.ValidacionTMEX__c != null){
                    if(sad.ValidacionTMEX__c == 'Si'){
                        sad.Estatus__c = Constantes.AUTO_DEMO_AUTORIZACION_ALTA_TMEX;
                    } else{
                        sad.Estatus__c = Constantes.AUTO_DEMO_ALTA_RECHAZADA_TMEX;
                    }
                    System.debug(sad.Estatus__c);
                }
                if(sad.ValidacionTFS__c != null){
                    System.debug('Entro al debug de true');
                    if(sad.ValidacionTFS__c =='Si'){
                        sad.Estatus__c = Constantes.AUTO_DEMO_ALTA;
                    } else{
                        sad.Estatus__c = Constantes.AUTO_DEMO_ALTA_RECHAZADA_TFS;
                    }
                    System.debug(sad.Estatus__c);
                } 
            }     
        }
        System.debug('Termino ok');
    }
    
    private void actualizaVIN(List<SolicitudAutoDemo__c> solicitudes){
        Map<String, List<Date>> mesesCortes;
        Map<String, Boolean> mesDentroRango;
        Map<Id, CostoSerie__c> costoSeriesMap;
        Map<Id, Vehiculo__c> vinesMapById;

        Map<Id, Account> mapaAsignaciones;                
        
        List<CortePresupuesto__c> nuevosCortes = new List<CortePresupuesto__c>();
        List<CortePresupuesto__c> cortesEliminar = new List<CortePresupuesto__c>();
        

        Set<Id> vinesID = new Set<Id>();
        Set<String> distribuidoresIDS = new Set<String>();
        Set<String> costosSerieIDS = new Set<String>();
        Set<String> validStatus = new Set<String>{'Alta','Baja anticipada','Baja por pago completado sin solicitud'};
        Set<String> interesesExternalIDS = new Set<String>();

        for(SolicitudAutoDemo__c sad: solicitudes){
            //if(validStatus.contains(sad.Estatus__c)){
                if(sad.VIN__c!=null){
                    vinesID.add(sad.VIN__c);
                }
    
                if(sad.Distribuidor__c != null){
                    distribuidoresIDS.add(sad.Distribuidor__c);
                }
            //}
            
        }

        mapaAsignaciones = AutoDemoTriggerMethods.getAsignacionesMap(distribuidoresIDS);        
        vinesMapById = AutoDemoTriggerMethods.getVinesInfoById(vinesID);        
        
        for(Vehiculo__c v: vinesMapById.values()){
            if(v.Producto__r.Serie__r.InteresSerieActiva__c != null){
                costosSerieIDS.add(v.Producto__r.Serie__r.InteresSerieActiva__c);
            }
        }

        costoSeriesMap = AutoDemoTriggerMethods.loadCostosSerie(costosSerieIDS);


        for(SolicitudAutoDemo__c sad: solicitudes){
            Vehiculo__c vin;
            if(sad.VIN__c!=null){
                vin = vinesMapById.get(sad.VIN__c);
            }


            if(sad.Distribuidor__c != null && sad.VIN__c != null){
                if(validStatus.contains(sad.Estatus__c)){
                    System.debug('Intereses Autos demo Init Validation');
                        if(sad.FechaAltaAutoDemo__c != null){
                            mesesCortes = AutoDemoTriggerMethods.cortesToCreate(sad.FechaAltaAutoDemo__c, sad.FechaBajaAutoDemoVigencia__c);
                            mesDentroRango = new Map<String, Boolean>();
                            System.debug(JSON.serialize(mesesCortes));

                            List<AsignacionPresupuesto__c> asignacionesDistribuidor = mapaAsignaciones.get(sad.Distribuidor__c).AsignacionPresupuesto__r;
                            
                            if(asignacionesDistribuidor != null && asignacionesDistribuidor.size() > 0){
                                for(AsignacionPresupuesto__c ap : asignacionesDistribuidor){
                                    if(!montosUtilizadosMap.containsKey(ap.Name)){
                                        montosUtilizadosMap.put(ap.Name, new Map<String, Decimal>());
                                    }

                                    for(String mes: mesesCortes.keySet()){
                                        //Elimina cortes futuros
                                        if(sad.Estatus__c == 'Baja anticipada' || sad.Estatus__c == 'Baja por pago completado sin solicitud'){
                                            interesesExternalIDS.add(sad.Name +'-'+mes);
                                            /*for(CortePresupuesto__c cp: ap.CasosAsignacionPresupuesto__r){
                                                if(cp.Mes__c == mes && cp.SolicitudAutoDemo__c == sad.Id){
                                                    if(cp.Estatus__c == 'Asignado'){
                                                        cortesEliminar.add(cp);
                                                    }
                                                }
                                            }*/                      

                                        } else if(sad.Estatus__c == 'Alta'){                                            

                                            if(!mesDentroRango.containsKey(mes)){
                                                mesDentroRango.put(mes, false);
                                            }

                                            if(AutoDemoTriggerMethods.dentroDelRango(mesesCortes.get(mes), ap.InicioFY__c, ap.FinFY__c)){
                                                mesDentroRango.put(mes, true);
                                                if(vin.Producto__r.Serie__r.InteresSerieActiva__c != null){
                                                    List<Decimal> montosAP = AutoDemoTriggerMethods.getMontosMesAP(mes, ap);
                                                    Decimal costoSerie = AutoDemoTriggerMethods.getCostoPromedioMes(mes, costoSeriesMap.get(vin.Producto__r.Serie__r.InteresSerieActiva__c));
                                                    Decimal utilizado = montosAP.get(0);
                                                    Decimal asignado = montosAP.get(1);

                                                    if(!montosUtilizadosMap.get(ap.Name).containsKey(mes)){
                                                        montosUtilizadosMap.get(ap.Name).put(mes, utilizado);
                                                    }

                                                    if((costoSerie + montosUtilizadosMap.get(ap.Name).get(mes)) <= asignado){
                                                            CortePresupuesto__c c = new CortePresupuesto__c(
                                                                AsignacionPresupuesto__c = ap.Id,
                                                                SolicitudAutoDemo__c = sad.Id,
                                                                InicioCortePresupuesto__c = mesesCortes.get(mes).get(0),
                                                                FinCortePresupuesto__c = mesesCortes.get(mes).get(1),
                                                                IDExterno__c = sad.Name +'-'+ mes,
                                                                Mes__c = mes
                                                            );            
                                                            nuevosCortes.add(c);
														 System.debug('#### ---->>>  '+nuevosCortes.size());     
                                                        /*if(ap.CasosAsignacionPresupuesto__r.size() > 0 ){
                                                            boolean existeCorte = false;
                                                            
                                                            for(CortePresupuesto__c cp: ap.CasosAsignacionPresupuesto__r){
                                                                if(cp.Mes__c == mes && cp.SolicitudAutoDemo__c == sad.Id){
                                                                    existeCorte = true;
                                                                    nuevosCortes.add(cp);
                                                                    break;
                                                                }
                                                            }

                                                            if(!existeCorte){
                                                                CortePresupuesto__c c = new CortePresupuesto__c(
                                                                    AsignacionPresupuesto__c = ap.Id,
                                                                    SolicitudAutoDemo__c = sad.Id,
                                                                    InicioCortePresupuesto__c = mesesCortes.get(mes).get(0),
                                                                    FinCortePresupuesto__c = mesesCortes.get(mes).get(1),
                                                                    Mes__c = mes
                                                                );
                                                                nuevosCortes.add(c);
                                                            }

                                                        } else { 
                                                            CortePresupuesto__c c = new CortePresupuesto__c(
                                                                AsignacionPresupuesto__c = ap.Id,
                                                                SolicitudAutoDemo__c = sad.Id,
                                                                InicioCortePresupuesto__c = mesesCortes.get(mes).get(0),
                                                                FinCortePresupuesto__c = mesesCortes.get(mes).get(1),
                                                                Mes__c = mes
                                                            );            
                                                            nuevosCortes.add(c);
                                                        }*/


                                                        Decimal montoAnterior = montosUtilizadosMap.get(ap.Name).get(mes);
                                                        montosUtilizadosMap.get(ap.Name).put(mes, costoSerie + montoAnterior);
                                                        vin.TipoVin__c = 'Auto demo';
                                                        //sad.ActivarDesarrollosInteresesAD__c = false;
                                                        
                                                    } else {
                                                        sad.addError('Estas excediendo el límite de tu presupuesto TEST 2. Contacta con tu DM.');
                                                    }

                                                } else {
                                                    sad.addError('VIN no tiene interes de serie activa.');
                                                }
                                            }
                                            
                                        }
                                    }
                                }

                                //System.debug(JSON.serialize(mesDentroRango));
                                /*if(sad.Estatus__c == 'Alta'){
                                    for(String mes : mesDentroRango.keySet()){
                                        if(!mesDentroRango.get(mes)){
                                            sad.addError(mes + ' no esta dentro del rango de asignacion.');
                                        }
                                    }
                                }*/

                            } else {
                                //Distribuidor no tiene asignaciones de presupuesto
                            }
                        }

                    System.debug('Intereses Autos demo End Validation');
                }
            
            } else {
                sad.addError('Solicitud no tiene distribuidor y/o VIN.');
            }
        }
        
        System.debug(JSON.serialize(nuevosCortes)); 
        //System.debug(JSON.serialize(cortesEliminar)); 

        if(vinesMapById.size() > 0){
            try{
                update vinesMapById.values();
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }

        if(nuevosCortes.size() > 0){
            try{
                upsert nuevosCortes IDExterno__c;
                 System.debug('#### ---->>>    upsert nuevosCortes IDExterno__c '); 
                  // se manda a llamar la clase que actualiza los montos 
               InteresAutosDemo.montosAutoDFYA(nuevosCortes);
               InteresAutosDemo.montosAutoPAGOCompletoSolicitudes(solicitudes);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }else{
       		InteresAutosDemo.montosAutoPAGOCompletoSolicitudes(solicitudes);
        }

        
        if(interesesExternalIDS.size() > 0){
            System.enqueueJob(new QueueableCall(interesesExternalIDS));
        }

    } 
    
    private void IDExternoAutoDemo(List<SolicitudAutoDemo__c> solicitudes){
        
        Set<Id> vinesID = new Set<Id>();
        for(SolicitudAutoDemo__c sad: solicitudes){
            if(sad.VIN__c!=null){
                vinesID.add(sad.VIN__c);
            }
        } 
        System.debug(vinesID);
        Map<Id,String> vinesMap = new Map<Id,String>();
        for(Vehiculo__c vin : [SELECT Id, Name FROM Vehiculo__c WHERE Id IN:vinesID]){
            vinesMap.put(vin.Id,vin.Name);
        }
        
        for(SolicitudAutoDemo__c sad:solicitudes){
            System.debug(sad.VIN__c);
            System.debug(sad.EstatusAutoDemo__c);
            if(sad.VIN__c!=null){
                if(sad.EstatusAutoDemo__c!=null && sad.EstatusAutoDemo__c!=''){
                    if(sad.EstatusAutoDemo__c == 'Activo' || sad.EstatusAutoDemo__c == 'Inactivo' || sad.EstatusAutoDemo__c == 'Pendiente' || sad.EstatusAutoDemo__c == 'Vencido' ){
                        sad.IDExternoAutoDemo__c = vinesMap.get(sad.VIN__c);
                        System.debug(sad.IDExternoAutoDemo__c);
                    } else{
                        sad.IDExternoAutoDemo__c = '';
                    }
                }
            }
        }
        System.debug('Termina metodo VIN');
    }
    
    private void validateEstatusBaja(List<SolicitudAutoDemo__c> solicitudes){
        System.debug('Entraste al metodo beforeupdate ESTATUS BAJA');
            Set<Id> sadId =  new Set<Id>();
            
            Set<String> validStatusToBaja = new Set<String>{
                Constantes.AUTO_DEMO_BAJA_POR_VIGENCIA,
                Constantes.AUTO_DEMO_BAJA_ANTICIPADA,
                Constantes.AUTO_DEMO_SOLICITUD_BAJA,
                Constantes.AUTO_DEMO_BAJA_PAGO_COMPLETADO_SIN_SOLICITUD
            };

            for(SolicitudAutoDemo__c sad:solicitudes){
                sadId.add(sad.Id);
            }
        	System.debug(sadId);
            Map<Id,ConsolidadoPagoTFS__c> cpMap = new Map<Id,ConsolidadoPagoTFS__c>();
        	System.debug(cpMap);
            for(ConsolidadoPagoTFS__c cp: [SELECT Id, SerialNumber__c, EffectiveDate__c 
                                            FROM ConsolidadoPagoTFS__c WHERE SerialNumber__c IN :sadId ORDER BY EffectiveDate__c DESC]){
                System.debug(cp);                              
                if(cp.EffectiveDate__c!=null){              
                    cpMap.put(cp.SerialNumber__c,cp);
                }
                System.debug('cpMap'+cpMap);
            }
        
            for(SolicitudAutoDemo__c sad:solicitudes){
                System.debug(sad.Estatus__c);
                System.debug(Constantes.AUTO_DEMO_AUTORIZACION_BAJA_TMEX);
                if(sad.Estatus__c!=null && sad.Estatus__c!=''){ //Validate Status
                    if(sad.Estatus__c == 'Solicitud baja'){
                        sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_BAJA).Id;
                            if(sad.ValidacionBajaTMEX__c!=null && sad.ValidacionBajaTMEX__c!=''){
                                //if(sad.ValidacionBajaTMEX__c=='Si'){
                                    System.debug('Estatus de solicitud baja y TMEX SI, Autorización baja TMEX');    
                                    sad.Estatus__c = Constantes.AUTO_DEMO_AUTORIZACION_BAJA_TMEX;
                                    System.debug('Estatus de solicitud: ' +sad.Estatus__c);
                                //}
                            }
                        if((sad.SumatoriaTransacciones__c !=null && sad.SumatoriaTransacciones__c !=0.0) && (sad.OriginalPrincipalAmount__c!=null && sad.OriginalPrincipalAmount__c!=0.0)){
                            if(sad.SumatoriaTransacciones__c >= sad.OriginalPrincipalAmount__c){
                                sad.Estatus__c = Constantes.AUTO_DEMO_SOLICITUD_BAJA;
                                //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_BAJA).Id;
                                if(sad.ValidacionBajaTMEX__c!=null && sad.ValidacionBajaTMEX__c!=''){
                                    //if(sad.ValidacionBajaTMEX__c=='Si'){
                                        if(sad.FechaBajaAutoDemoVigencia__c>System.today()){
                                            sad.Estatus__c = Constantes.AUTO_DEMO_BAJA_ANTICIPADA;
                                            //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                            sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;  
                                        }else{
                                            sad.Estatus__c = 'Baja por vigencia';
                                            //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                            sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;  
                                        }
                                    //}
                                }
                            }
                        }
                    }
                    String estatusold = sad.Estatus__c;
                    if(sad.Estatus__c == Constantes.AUTO_DEMO_AUTORIZACION_BAJA_TMEX){
                        System.debug(sad.SumatoriaTransacciones__c);
                        System.debug(sad.OriginalPrincipalAmount__c);
                        if((sad.SumatoriaTransacciones__c !=null && sad.SumatoriaTransacciones__c !=0.0) && (sad.OriginalPrincipalAmount__c!=null && sad.OriginalPrincipalAmount__c!=0.0)){
                            if(sad.SumatoriaTransacciones__c >= sad.OriginalPrincipalAmount__c){
                                /*
                                sad.Estatus__c = Constantes.AUTO_DEMO_BAJA_ANTICIPADA;
                                sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;  
                                */
                                if(sad.FechaBajaAutoDemoVigencia__c>System.today()){
                                    System.debug('Entra al if');
                                    sad.Estatus__c = Constantes.AUTO_DEMO_BAJA_ANTICIPADA;
                                    //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                    sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;  
                                }else{
                                    System.debug('Entra al else');
                                    sad.Estatus__c = 'Baja por vigencia';
                                    //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                                    sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;
                                    System.debug(sad.RecordTypeId);
                                }
                            }
                        }
                    }  
                    System.debug(estatusold);
                    if(estatusold=='Alta' || estatusold=='Solicitud alta' || estatusold=='Autorización alta TMEX' || estatusold=='Solicitud baja' || estatusold=='Autorización baja TMEX'){
                        System.debug(sad.Estatus__c);
                        System.debug(sad.ValidacionBajaTMEX__c);
                        if(sad.ValidacionBajaTMEX__c!=null && sad.ValidacionBajaTMEX__c!=''){
                            //if(){
                                //sad.Estatus__c = Constantes.AUTO_DEMO_AUTORIZACION_BAJA_TMEX;
                                System.debug(sad.SumatoriaTransacciones__c);
                            	System.debug(sad.OriginalPrincipalAmount__c);
                                if((sad.SumatoriaTransacciones__c !=null && sad.SumatoriaTransacciones__c !=0.0) && (sad.OriginalPrincipalAmount__c!=null && sad.OriginalPrincipalAmount__c!=0.0)){
                                    if(sad.SumatoriaTransacciones__c >= sad.OriginalPrincipalAmount__c){
                                        System.debug(cpMap.get(sad.Id));
                                        System.debug('Fecha: '+sad.FechaBajaPagoCompletado__c);
                                        if(sad.FechaBajaPagoCompletado__c == null){
                                            System.debug(cpMap.get(sad.Id).EffectiveDate__c);
                                            sad.FechaBajaPagoCompletado__c  = cpMap.get(sad.Id).EffectiveDate__c;
                                        }
                                        System.debug(sad.FechaBajaPagoCompletado__c);
                                        sad.Estatus__c = Constantes.AUTO_DEMO_BAJA_PAGO_COMPLETADO_SIN_SOLICITUD;
                                        System.debug(sad.Estatus__c);
                                        System.debug('Estatus de solicitud: ' +sad.Estatus__c); 
                                    }
                                }                               
                            //}
                        }
                        if((sad.SumatoriaTransacciones__c !=null && sad.SumatoriaTransacciones__c !=0.0) && (sad.OriginalPrincipalAmount__c!=null && sad.OriginalPrincipalAmount__c!=0.0)){
                            if(sad.SumatoriaTransacciones__c >= sad.OriginalPrincipalAmount__c){
                                sad.FechaBajaPagoCompletado__c  = cpMap.get(sad.Id).EffectiveDate__c;
                                sad.Estatus__c = Constantes.AUTO_DEMO_PAGO_COMPLETADO_SIN_SOLICITUD;
                                sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_BAJA).Id;
                            }
                        }
                    }
                    
                    if(sad.Estatus__c == Constantes.AUTO_DEMO_PAGO_COMPLETADO_SIN_SOLICITUD){
                        if(sad.ValidacionBajaTMEX__c=='Si'){
                            sad.Estatus__c = Constantes.AUTO_DEMO_BAJA_PAGO_COMPLETADO_SIN_SOLICITUD;
                        }
                    }
                    
                    sad.FechaBajaAutoDemoSolicitud__c = validStatusToBaja.contains(sad.Estatus__c) ? setFechaBajaDemo(sad.FechaBajaAutoDemoSolicitud__c): null;
                }//End
                
                if(sad.EstatusAutoDemo__c!=null){
                    if(sad.EstatusAutoDemo__c=='Inactivo'){
                        //sad.FechaBajaAutoDemoSolicitud__c = System.today();
                        //sad.FechaBajaAutoDemoSolicitud__c = sad.EstatusAutoDemo__c != autosDemoOldMap.get(sad.Id).EstatusAutoDemo__c ? System.today() : sad.FechaBajaAutoDemoSolicitud__c;
                        sad.RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get(Constantes.SOLICITUD_RT_CERRADA).Id;  
                    }
                }
            } 
    		System.debug('Termino');
    }


    private Date setFechaBajaDemo(Date fechaBajaActual){
        return fechaBajaActual != null ? fechaBajaActual : System.today();
    }


    @future
    public static void futureMethodCall(Set<String> externalIDS){        
        List<CortePresupuesto__c> deleteInteresesList = [SELECT Id, Name, IDExterno__c, Estatus__c FROM CortePresupuesto__c WHERE IDExterno__c IN:externalIDS AND Estatus__c = 'Asignado'];
        
        try{
            delete deleteInteresesList;
        } catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }

    public class QueueableCall implements System.Queueable{
        private Set<String> interesesExternalIDS;

        public QueueableCall(Set<String> externalIDS){
            this.interesesExternalIDS = externalIDS;
        }

        public void execute(QueueableContext context) {
            futureMethodCall(interesesExternalIDS);
        }
    }

    
}