/**
    Descripción General: Clase wrapper para Grid
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        15/Nov/2019         Versión Inicial
    ________________________________________________________________
**/
public class TAM_GridWrapperClass {

    @AuraEnabled 
    public String strYear_Serie {get;set;}
    @AuraEnabled 
    public String strClaveModelo {get;set;}
    @AuraEnabled 
    public String strVersion {get;set;}
    @AuraEnabled 
    public Integer intInventario {get;set;}
    @AuraEnabled 
    public Integer intDiasInventario {get;set;}
    @AuraEnabled 
    public Integer intVentaMY {get;set;}
    @AuraEnabled 
    public Boolean boolBalanceOut {get;set;}
    @AuraEnabled 
    public Boolean boolPagoTFS {get;set;}
    @AuraEnabled 
    public String intIncentivoActual {get;set;}
    @AuraEnabled 
    public Double intIncentivoPropuesto_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoPropuesto_Porcentaje {get;set;}
    @AuraEnabled 
    public Double intIncentivoTMEX_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoTMEX_Porcentaje {get;set;}
    @AuraEnabled 
    public Double intIncentivoDealer_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoDealer_Porcentaje {get;set;}
    @AuraEnabled
    public TAM_GridTripePayWrapperClass objTriplePlay {get;set;}
    @AuraEnabled
    public Double intMSRP {get;set;}
    @AuraEnabled
    public Double intMSRP_Incentivo {get;set;}
    @AuraEnabled
    public Double intTMEXMargen_Pesos {get;set;}
    @AuraEnabled
    public Double intTMEXMargen_Porcentaje {get;set;}
    @AuraEnabled
    public Double intTMEXMargenAfter_Pesos {get;set;}
    @AuraEnabled
    public Double intTMEXMargenAfter_Porcentaje {get;set;}
    @AuraEnabled
    public Double intCostoTMEX {get;set;}
    @AuraEnabled
    public String strRango {get;set;}
    @AuraEnabled
    public Boolean boolClonado {get;set;}
    @AuraEnabled 
    public String strSerie {get;set;}
    @AuraEnabled 
    public String strAnioModelo {get;set;}
    @AuraEnabled 
    public Integer intConsecutivo {get;set;}
    @AuraEnabled
    public String intClaveOrdenamiento {get;set;}
    @AuraEnabled
    public Boolean boolLealtad {get;set;}
    @AuraEnabled
    public Double dblBonoLealtad {get;set;}

	//Un contructor que recive parametros
    public TAM_GridWrapperClass(String strYear_Serie, String strClaveModelo, String strVersion, Integer intInventario, Integer intDiasInventario, Integer intVentaMY,
                                Boolean boolBalanceOut, Boolean boolPagoTFS, String intIncentivoActual, Double intIncentivoPropuesto_Pesos,
                                Double intIncentivoPropuesto_Porcentaje, Double intIncentivoTMEX_Pesos, Double intIncentivoTMEX_Porcentaje,
                                Double intIncentivoDealer_Pesos, Double intIncentivoDealer_Porcentaje, TAM_GridTripePayWrapperClass objTriplePlay,
                                Double intMSRP, Double intMSRP_Incentivo, Double intTMEXMargen_Pesos, Double intTMEXMargen_Porcentaje, Double intTMEXMargenAfter_Pesos,
                                Double intTMEXMargenAfter_Porcentaje, Double intCostoTMEX, String strRango, Boolean boolClonado, String strSerie, String strAnioModelo,
                                Integer intConsecutivo, String intClaveOrdenamiento, Boolean boolLealtad, Double dblBonoLealtad) {
        
        this.strYear_Serie = strYear_Serie;
        this.strClaveModelo = strClaveModelo;
        this.strVersion = strVersion;
        this.intInventario = intInventario;
        this.intDiasInventario = intDiasInventario;
        this.intVentaMY = intVentaMY;
        this.boolBalanceOut = boolBalanceOut;
        this.boolPagoTFS = boolPagoTFS;
        this.intIncentivoActual = intIncentivoActual;
        this.intIncentivoPropuesto_Pesos = intIncentivoPropuesto_Pesos;
        this.intIncentivoPropuesto_Porcentaje = intIncentivoPropuesto_Porcentaje;
        this.intIncentivoTMEX_Pesos = intIncentivoTMEX_Pesos;
        this.intIncentivoTMEX_Porcentaje = intIncentivoTMEX_Porcentaje;
        this.intIncentivoDealer_Pesos = intIncentivoDealer_Pesos;
        this.intIncentivoDealer_Porcentaje = intIncentivoDealer_Porcentaje;
        this.objTriplePlay = objTriplePlay;
        this.intMSRP = intMSRP;
        this.intMSRP_Incentivo = intMSRP_Incentivo;
        this.intTMEXMargen_Pesos = intTMEXMargen_Pesos;
        this.intTMEXMargen_Porcentaje = intTMEXMargen_Porcentaje;
        this.intTMEXMargenAfter_Pesos = intTMEXMargenAfter_Pesos;
        this.intTMEXMargenAfter_Porcentaje = intTMEXMargenAfter_Porcentaje;
        this.intCostoTMEX = intCostoTMEX;
        this.strRango = strRango;
        this.boolClonado = boolClonado;
        this.strSerie = strSerie;
        this.strAnioModelo = strAnioModelo;
        this.intConsecutivo = intConsecutivo;
        this.intClaveOrdenamiento = intClaveOrdenamiento;
        this.boolLealtad = boolLealtad;
        this.dblBonoLealtad = dblBonoLealtad;
    }
    
	//Regresa un objeto del tipo AddTelephoneDto si no hubo error a la hora de registrar el cliente		
	public static List<TAM_GridWrapperClass> JSONParserSFDC(String sJsonResp){
		System.debug('EN JSONParserSFDC: sJsonResp: ' + sJsonResp);
		
		List<TAM_GridWrapperClass> listObjWrp = new List<TAM_GridWrapperClass>();
		try{
            JSONParser parser = JSON.createParser(sJsonResp);
            //Ve si tiene algo el objeto de parser  
            while (parser.nextToken() != null) {//[{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":10,"intInventario":8,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2201","strSerie":"AVANZA","strVersion":"LE MT","strYear_Serie":"2019-AVANZA"},'{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":2,"intInventario":1,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2202","strSerie":"AVANZA","strVersion":"LE AT","strYear_Serie":"2019-AVANZA"} '']'
				//Inicia el detalle del objeto: sNombreObj
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					//Toma el contenido del Json y conviertelo a SignInCls.class 
					TAM_GridWrapperClass objTAM_GridWrapperClass = (TAM_GridWrapperClass)parser.readValueAs(TAM_GridWrapperClass.class);
					listObjWrp.add(objTAM_GridWrapperClass);
				}//Fin si parser.getCurrentToken() == JSONToken.START_OBJECT
            }//Fin mientras parser.nextToken() != null			
		}catch(Exception ex){
			System.debug('ERROR EN JSONParserSFDC: sJsonResp: ' + ex.getMessage());
	 	}
		System.debug('ANTES DE SALIR DE JSONParserSFDC: listObjWrp: ' + listObjWrp);
			 	
		//Regresa el objeto objSignInClsPaso
		return listObjWrp;
	}    
    
}