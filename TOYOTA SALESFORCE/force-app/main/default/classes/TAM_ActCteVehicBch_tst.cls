/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        Vehiculo__c y verificar que exista el cliente y su relacion con
                        Opp, Opp-Factura, Opp-Vehic, Cte-Vehiv.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActCteVehicBch_tst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    
    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
        
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

    static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();

    @TestSetup static void loadData(){

        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;
        
        Account clienteDealer = new Account(
            Name = 'TMEX',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoDistribuidor,
            TAM_IdExternoNombre__c = 'TOYOTA PRUEBA'
        );
        insert clienteDealer;

        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_IdExternoNombre__c = 'CARSON'
        );
        insert clienteMoral;
        
        Contact con = new Contact(AccountId = clienteMoral.id,lastname = 'testdata' , firstname ='testdata1');
        insert con; 

        Contact con1 = new Contact(AccountId = clienteDealer.id,lastname = 'testdata2' , firstname ='testdata2');
        insert con1; 

        Opportunity OpportunidadPaso = new Opportunity( 
            Name = 'XXXXXXXX1' + '-' + clienteMoral.Name,
            TAM_Vin__c = 'XXXXXXXX1',                       
            CloseDate = Date.today(),
            StageName = 'Closed Won',
            Pricebook2Id = Test.getStandardPricebookId(),
            Amount = 0.00,
            TAM_IdExterno__c = 'XXXXXXXX1' + '-' + clienteMoral.Name,
            recordTypeId = VaRtOppRegVCrm,
            //AccountId = clienteMoral.id,
            TAM_Distribuidor__c = clienteDealer.id
        );                       
        upsert OpportunidadPaso Opportunity.TAM_IdExterno__c;
        
        List<TAM_HistoricoClientes__c> lHistoricoCte = new List<TAM_HistoricoClientes__c>();
        TAM_HistoricoClientes__c objHistCtes = new TAM_HistoricoClientes__c( 
            Name = 'CARSON',
            oVin__c = 'XXXXXXXX1',                      
            dNEXOS1__c = '',
            dAPELL1__c = '',
            dNEXOS2__c = '',
            dAPELL2__c = '',
            dSEXO__c = 'E'
        );                       
        lHistoricoCte.add(objHistCtes);

        TAM_HistoricoClientes__c objHistCtes2 = new TAM_HistoricoClientes__c( 
            Name = 'CARSON II',
            oVin__c = 'XXXXXXXX2',                      
            dNEXOS1__c = '',
            dAPELL1__c = '',
            dNEXOS2__c = '',
            dAPELL2__c = '',
            dSEXO__c = 'H'
        );                       
        //lHistoricoCte.add(objHistCtes2);
        insert lHistoricoCte;

        Vehiculo__c v01 = new Vehiculo__c(
            Name='XXXXXXXX1',
            Id_Externo__c='XXXXXXXX1'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        Objetivo_de_Venta__c ov01 = new Objetivo_de_Venta__c(
            Distribuidor__c = clienteDealer.Id,
            Serie__c = ser01.Id,
            Anio__c = '2015',
            Mes__c = '1',
            MK__c = 1,
            Objetivo__c = 1
        );
        insert ov01;
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = clienteDealer.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA'                      
        );
        insert m01;
        
        //Actualiza v01
        v01.Ultimo_Movimiento__c = m01.id;
        update v01;
                                
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_ActCteVehicBchOK() {
        Test.startTest();

            String sHoraZonaHoraria = TAM_ActCteVehicSch_cls.getHoraZonaHoraria();
            System.debug('EN TAM_ActCteVehicBchOK.execute sHoraZonaHoraria: ' + sHoraZonaHoraria);
        
	        //Para la fecha Ini        
	        DateTime dtFechaCreacIni = DateTime.now().addDays(-2);
	        DateTime dtFechaCreacIni2 = dtFechaCreacIni.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacIni;
	        String sdtFechaCreacIni = String.valueOf(dtFechaCreacIni2);
	        String sdtFechaCreacIniFinal = sdtFechaCreacIni.replace(' ', 'T') + 'Z';
	        //Para la fecha Fin
	        DateTime dtFechaCreacFin = DateTime.now();
	        DateTime dtFechaCreacFin2 = dtFechaCreacFin.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacFin;
	        String sdtFechaCreacFin = String.valueOf(dtFechaCreacFin2);
	        String sdtFechaCreacFinFinal = sdtFechaCreacFin.replace(' ', 'T') + 'Z';
	        System.debug('EN TAM_ActCteVehicSch_cls.execute sdtFechaCreacIniFinal: ' + sdtFechaCreacIniFinal + ' sdtFechaCreacFinFinal: ' + sdtFechaCreacFinFinal);
	            
	        String sQuery = 'Select v.Name, v.Id From Vehiculo__c v';
	        sQuery += ' Where CreatedDate >= ' + String.valueOf(sdtFechaCreacIniFinal);
	        sQuery += ' And CreatedDate <= ' + String.valueOf(sdtFechaCreacFinFinal);
            sQuery += ' LIMIT 1';
	        
            System.debug('EN TAM_ActualizaDummyVinBchOK.execute sQuery: ' + sQuery);
            
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActCteVehicBch_cls objActCteVehicBch = new TAM_ActCteVehicBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActCteVehicBch, 1);

            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_ActCteVehicSch_cls objTAM_Jobh = new TAM_ActCteVehicSch_cls();
            System.schedule('Ejecuta_TAM_ActCteVehicSch_cls', sch, objTAM_Jobh);
            
        Test.stopTest();
    }
    
}