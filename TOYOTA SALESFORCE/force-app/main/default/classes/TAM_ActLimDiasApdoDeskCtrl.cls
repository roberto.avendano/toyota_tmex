/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica de TAM_ActLimDiasApdoDeskCtrl
                        para caoturar el limite de dias permitido para tener un VIN
                        que esta en estatus de G

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Junio-2020        Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_ActLimDiasApdoDeskCtrl {

    @AuraEnabled
    public static String getNoDistlUserActual (String userId){
        System.debug('En getNoDistlUserActual userId: ' + userId);
    
        User[] usr;
        String sCodigoDistribuidor;
        
        //usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];
        for (User usuarioAct : [Select id,CodigoDistribuidor__c FROM User where id =:userId]){
            sCodigoDistribuidor = usuarioAct.CodigoDistribuidor__c != null ? usuarioAct.CodigoDistribuidor__c : '57000';
        }
        System.debug('En getNoDistlUserActual sCodigoDistribuidor: ' + sCodigoDistribuidor);
        return sCodigoDistribuidor;
    }

    @AuraEnabled
    public static String consultaMaxNoDiasAdo (String userId){
        System.debug('En consultaMaxNoDiasAdo userId: ' + userId);
    
        User[] usr;
        String sCodigoDistribuidor;
        String sNoDiasAdoDist;
        
        //usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];
        for (User usuarioAct : [Select id,CodigoDistribuidor__c FROM User where id =:userId]){
            sCodigoDistribuidor = usuarioAct.CodigoDistribuidor__c != null ? usuarioAct.CodigoDistribuidor__c : '57000';
        }
        System.debug('En consultaMaxNoDiasAdo sCodigoDistribuidor: ' + sCodigoDistribuidor);

        for (Account objDist : [Select id, TAM_NoDiasApartado__c, Codigo_Distribuidor__c 
            From Account Where Codigo_Distribuidor__c =:sCodigoDistribuidor]){
            sNoDiasAdoDist = objDist.TAM_NoDiasApartado__c != null ?  objDist.TAM_NoDiasApartado__c : '0';
        }
        System.debug('En consultaMaxNoDiasAdo sNoDiasAdoDist: ' + sNoDiasAdoDist);

        return sNoDiasAdoDist;
    }

    @AuraEnabled
    public static void seteaValorMaximoLeads(String userId, String numeroMaximoNoDiasField){   
        System.debug('En seteaValorMaximoLeads userId: ' + userId + ' numeroMaximoNoDiasField: ' + numeroMaximoNoDiasField);

        User[] usr;
        String sCodigoDistribuidor;
        String sNoDiasAdoDist;
        Account objDistUpd;
        //usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];
        for (User usuarioAct : [Select id,CodigoDistribuidor__c FROM User where id =:userId]){
            sCodigoDistribuidor = usuarioAct.CodigoDistribuidor__c != null ? usuarioAct.CodigoDistribuidor__c : '57000';
        }
        System.debug('En seteaValorMaximoLeads sCodigoDistribuidor: ' + sCodigoDistribuidor);

        for (Account objDist : [Select id, TAM_NoDiasApartado__c 
            From Account Where Codigo_Distribuidor__c =:sCodigoDistribuidor]){    
            objDist.TAM_NoDiasApartado__c = numeroMaximoNoDiasField;
            objDistUpd = objDist;
        }
        System.debug('En seteaValorMaximoLeads objDistUpd: ' + objDistUpd);

        try{
           upsert objDistUpd Id;
        }catch(Exception e){
           system.debug('Error general al actualizar seteaValorMaximoLeads: ' + e.getMessage()); 
        }
   
    }
    
}