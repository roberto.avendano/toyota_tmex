/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para eliminar la colaboración de los registros 
                        que vienen de la clases TAM_AdminVisibInventarioToyota.bpDepuraColaboracionDealer.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_DepuraColaboraInventDealerBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    global string query;
    global String sDealerColabora;
    
    //Un constructor por default
    global TAM_DepuraColaboraInventDealerBch_cls(string query, String sDealerColaboraParam){
        this.query = query;
        this.sDealerColabora = sDealerColaboraParam;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraInventDealerBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_DepuraColaboraInventDealerBch_cls.');

        Set<String> setVodDealer = new Set<String>();
        Map<String, String> mapIdUsrNomDis = new Map<String, String>();
        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        Map<String, String> mapNoDealerNomGrupo = new Map<String, String>();
        List<TAM_InventarioVehiculosToyota__Share >  lInvenVehiculoShare = new List<TAM_InventarioVehiculosToyota__Share >();
        List<TAM_InventarioVehiculosToyota__Share> lInvVehToyotaDel = new List<TAM_InventarioVehiculosToyota__Share>();
        
        Set<String> setNomDealerOrigen = new Set<String>();
        Set<String> setNomDealer = new Set<String>();
        Set<String> setIdInvDel = new Set<String>();
        
        String sFleterosColaboracionEspecial = System.Label.TAM_FleterosColaboracionEspecial;
        String sCuernavacaColaboracionEspecial = System.Label.TAM_CuernavacaColaboracionEspecial;
                        
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvVhToy : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvVhToy.Dealer_Code__c != null)
                setVodDealer.add(objInvVhToy.Dealer_Code__c);
            setIdInvDel.add(objInvVhToy.id);            
        }//Fin del for para scope
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls setVodDealer: ' + setVodDealer);           
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls setIdInvDel: ' + setIdInvDel);           

        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Codigo_Distribuidor__c = :sDealerColabora ORDER BY NAME]){
            String sName = Dealer.Name;    
            mapNoDealerNomGrupo.put(sName.toUpperCase(), Dealer.Codigo_Distribuidor__c);
            String sNombreGrupo = Dealer.Name;
            sNombreGrupo = Dealer.Name + ' ASESOR';
            mapNoDealerNomGrupo.put(sNombreGrupo.toUpperCase(), Dealer.Codigo_Distribuidor__c);
        }
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.KeySet());           
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.Values());           
                
        //Consulta los grupos 
        for (Group Grupo : [SELECT ID, Name FROM Group WHERE Name IN: mapNoDealerNomGrupo.KeySet() ORDER BY NAME]){
            String sNombreGrupo = Grupo.Name;
            mapNomGrupoIdGrupo.put(Grupo.id, sNombreGrupo);
        }
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.KeySet());           
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           
        
        //Ahora si consulta los datos de la colaboración relacionada al inventario en setIdInvDel
        //y el grupo diferente de mapNomGrupoIdGrupo.LeySet() y el tipo de colaboración Manual 
        for (TAM_InventarioVehiculosToyota__Share objInvShare : [Select t.id, t.UserOrGroupId,  
            t.ParentId From TAM_InventarioVehiculosToyota__Share t
            Where t.RowCause = 'Manual' and UserOrGroupId != :mapNomGrupoIdGrupo.KeySet()
            and ParentId IN :setIdInvDel]){
            //Agregalos a la lista de lInvVehToyotaDel
            lInvVehToyotaDel.add(new TAM_InventarioVehiculosToyota__Share(
                    id = objInvShare.id
                )
            );
        }
        System.debug('ENTRO TAM_DepuraColaboraInventDealerBch_cls lInvVehToyotaDel: ' + lInvVehToyotaDel);           
                
        Boolean blnError = false;
        Set<String> setIdOkCol = new Set<String>();
        
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();
        
        if (!lInvVehToyotaDel.isEmpty()){
            //Actualiza las Opp 
            List<Database.DeleteResult> lDtbUpsRes = Database.delete(lInvVehToyotaDel, false);
            //Ve si hubo error
            for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de eliminar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()

        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraInventDealerBch_cls.finish Hora: ' + DateTime.now());      
        System.debug('EN TAM_DepuraColaboraInventDealerBch_cls.finish this.sDealerColabora: ' + this.sDealerColabora);      
        
        Set<String> setVodDealer = new Set<String>();
        //57019, TOYOTA AGUASCALIENTES SUR;57213, TOYOTA DEL BAJÍO, TOYOTA GUNAJUATO;
        String[] ssDealerColabora = this.sDealerColabora.split(';');
        //Recorre la lista de ssDealerColabora y toma los Distribuidores asocuados a cada Delaer
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            setVodDealer.add(ssDealerColaboraFinal[0]);
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO EN TAM_DepuraColaboraInventDealerBch_cls.finish setVodDealer: ' + setVodDealer);           

        String sLDealerColaInv = '(';     
        //Recorre la lista de lClientes
        for (String sTipoInv : setVodDealer){
            sLDealerColaInv += '\'' + sTipoInv.trim() + '\',';
        }
        sLDealerColaInv = sLDealerColaInv.substring(0, sLDealerColaInv.lastIndexOf(','));
        sLDealerColaInv += ')';
        System.debug('EN EN TAM_DepuraColaboraInventDealerBch_cls.finish sLDealerColaInv: ' + sLDealerColaInv);

        String strVinPrueba = 'JTDKDTB34M1141773';
        String sQuery = 'Select Id, Name, Dealer_Code__c ';
        sQuery += ' From TAM_InventarioVehiculosToyota__c ';
        sQuery += ' where Dealer_Code__c IN ' + sLDealerColaInv;
        sQuery += ' Order by Dealer_Code__c';
        //sQuery += ' Limit 10';

        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 10';
        System.debug('EN EN TAM_DepuraColaboraInventDealerBch_cls.finish sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_CreaColaborInventDefDealerBch_cls objCreaColaborInventDefDealerBch = new TAM_CreaColaborInventDefDealerBch_cls(sQuery, this.sDealerColabora);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCreaColaborInventDefDealerBch, 50);        

    } 
    
}