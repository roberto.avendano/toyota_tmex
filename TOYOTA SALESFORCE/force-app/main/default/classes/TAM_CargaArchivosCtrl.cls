/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar la carga de los archivos para la solicitud de Flotilla

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    14-Nov-2019       Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_CargaArchivosCtrl {

	public static String VaRtLeadlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	public static String VaRtLeadPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	public static String VaRtLeadClienteCreado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cliente Creado').getRecordTypeId();    
    
    
   @AuraEnabled  
   public static List<ContentDocument> getFiles(string recordId, String nombreArchivo){ 
    	System.debug('EN TAM_CargaArchivosCtrl.getFiles recordId:  ' + recordId + ' nombreArchivo: ' + nombreArchivo);
        // TO avoid following exception 
        // System.QueryException: Implementation restriction: ContentDocumentLink requires
        // a filter by a single Id on ContentDocumentId or LinkedEntityId using the equals operator or 
        // multiple Id's using the IN operator.
        // We have to add sigle record id into set or list to make SOQL query call
        Set<Id> recordIds=new Set<Id>{recordId};
        Set<Id> documentIds = new Set<Id>(); 
        List<ContentDocumentLink> cdl=[SELECT id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : recordIds
        And ContentDocument.Description = :nombreArchivo];  //'Acta Constitutiva' 
        for(ContentDocumentLink cdLink:cdl){  
            documentIds.add(cdLink.ContentDocumentId);  
        }      
        return [SELECT Id,Title,FileType FROM ContentDocument WHERE id IN: documentIds];  
    } 
    
   @AuraEnabled  
    public static String updFileDesc(string recordId, String nombreArchivo, String documentId){ 
    	System.debug('EN TAM_CargaArchivosCtrl.updFileDesc recordId:  ' + recordId + ' nombreArchivo: ' + nombreArchivo + ' documentId: ' + documentId);
        String sFileIdUpd = '';
        Set<Id> recordIds=new Set<Id>{recordId};
        Set<Id> documentIds = new Set<Id>();
        List<ContentDocument> lContentDocumentUpd = new List<ContentDocument>();
        List<Lead> lLeadUpd = new List<Lead>();
        List<Account> lAccountUpd = new List<Account>();
         
        List<ContentDocumentLink> cdl = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : recordIds 
        And ContentDocumentId =: documentId];  //'Acta Constitutiva' 
        
        //Recorre la lista y crea el objeto del tipo : 
        for(ContentDocumentLink cdLink : cdl){
        	lContentDocumentUpd.add(new ContentDocument(id = cdLink.ContentDocumentId, Description = nombreArchivo));
        	System.debug('EN TAM_CargaArchivosCtrl.ContentDocumentLink cdLink.LinkedEntityId:  ' + cdLink.LinkedEntityId + ' nombreArchivo: ' + nombreArchivo);
        	Lead objLeadUpd;
        	Account objCliente;
        	
        	String sIdArchivo = cdLink.LinkedEntityId;
        	if (sIdArchivo.startsWith('001'))  //0013F00000URlAHQA1
        		objCliente = new Account(id = cdLink.LinkedEntityId);
        	if (sIdArchivo.startsWith('00Q'))
        		objLeadUpd = new Lead(id = cdLink.LinkedEntityId);
        	
        	//Cuando se trata de los archivos para el cliente corporativo
        	if (sIdArchivo.startsWith('00Q')){
	        	if (nombreArchivo == 'Acta Constitutiva') 
					objLeadUpd.TAM_ActaConstitutiva__c = true;        		  
	        	if (nombreArchivo == 'Cedula Fiscal') 
					objLeadUpd.TAM_CedulaFiscal__c = true;
	        	if (nombreArchivo == 'PDF datos adicionales') 
					objLeadUpd.TAM_PdfDatosAdicionalesRepLegal__c = true;  
        		if (nombreArchivo == 'Orden de compra') 
					objLeadUpd.TAM_OrdenCompraCartaCompromiso__c = true;
	        	if (nombreArchivo == 'Identificación oficial') 
					objLeadUpd.TAM_IdentificacionOficial__c = true;        		  
	        	if (nombreArchivo == 'Comprobante de domicilio') 
					objLeadUpd.TAM_ComprobanteDomicilio__c = true;        		  
	        	if (nombreArchivo == 'Documentación de soporte') 
					objLeadUpd.TAM_DocumentacionSoporte__c = true;     
        	}//Fin si sIdArchivo.startsWith('00Q')
        	
			//Cuando se trata de cliente
			if (sIdArchivo.startsWith('001')){
        		if (nombreArchivo == 'Carta Compromiso') 
					objCliente.TAM_CartaCompromiso__c = true;
        		if (nombreArchivo == 'Orden de compra') 
					objCliente.TAM_OrdenCompra__c = true; 					     
			}//Fin si sIdArchivo.startsWith('001')
				
			//Metelo a la lista de lLeadUpd
			if (sIdArchivo.startsWith('00Q'))
        		lLeadUpd.add(objLeadUpd);

			//Metelo a la lista de lLeadUpd
			if (sIdArchivo.startsWith('001'))
        		lAccountUpd.add(objCliente);
        		
        }
		
		//Ve si tiene algo la lista de lContentDocumentUpd y actualiza la Descripción del archivo
        if (!lContentDocumentUpd.isEmpty())
        	update lContentDocumentUpd;
        System.debug('EN TAM_CargaArchivosCtrl.updFileDesc lContentDocumentUpd:  ' + lContentDocumentUpd);
        
		//Ve si tiene algo la lista de lContentDocumentUpd y actualiza la Descripción del archivo
        if (!lLeadUpd.isEmpty())
        	update lLeadUpd;
        System.debug('EN TAM_CargaArchivosCtrl.updFileDesc lLeadUpd:  ' + lLeadUpd);

		//Ve si tiene algo la lista de lContentDocumentUpd y actualiza la Descripción del archivo
        if (!lAccountUpd.isEmpty())
        	update lAccountUpd;
        System.debug('EN TAM_CargaArchivosCtrl.updFileDesc lAccountUpd:  ' + lAccountUpd);
        
        //Regresa el ID del Reg actualizado 
        return sFileIdUpd;  
    } 
    
        
    @AuraEnabled  
    public static void deleteFiles(string sdocumentId){ 
    	System.debug('EN TAM_CargaArchivosCtrl.deleteFiles sdocumentId:  ' + sdocumentId);
    	String sIdLead = '';
    	String nombreArchivo = '';
    	//Busca el id del objeto relacionado en ContentDocumentLink
    	for (ContentDocumentLink objContentDocumentLink : [Select c.LinkedEntityId, c.ContentDocumentId, ContentDocument.Description
    		From ContentDocumentLink c
    		Where ContentDocumentId = :sdocumentId ] ){
    		//Ve si el campo de LinkedEntityId empieza por 00Q que se refiere al Lead
    		String sIdLeadPaso = objContentDocumentLink.LinkedEntityId;
    		if (sIdLeadPaso.startsWith('00Q')){
    			sIdLead = objContentDocumentLink.LinkedEntityId;
    			nombreArchivo = objContentDocumentLink.ContentDocument.Description;
    			break;
    		}//Fin si sIdLeadPaso.startsWith('00Q')
    	}
    	
    	System.debug('EN TAM_CargaArchivosCtrl.deleteFiles sIdLead:  ' + sIdLead + ' nombreArchivo: ' + nombreArchivo);
    	//Ya tienes el id el lead y sudescrepcion ajora apaga la bandera del archivo que correspode
    	if (sIdLead != '' && nombreArchivo != ''){
        	Lead objLeadUpd = new Lead(id = sIdLead);
        	//Cuando se trata de los archivos para el cliente corporativo
        	if (nombreArchivo == 'Acta Constitutiva') 
				objLeadUpd.TAM_ActaConstitutiva__c = false;        		  
        	if (nombreArchivo == 'Cedula Fiscal') 
				objLeadUpd.TAM_CedulaFiscal__c = false;        		  
        	if (nombreArchivo == 'Orden de compra') 
				objLeadUpd.TAM_OrdenCompraCartaCompromiso__c = false;        		  
        	if (nombreArchivo == 'PDF datos adicionales') 
				objLeadUpd.TAM_PdfDatosAdicionalesRepLegal__c = false;        		  
        	//Cuando se trata de los archivos para el cliente de Programa
        	if (nombreArchivo == 'Identificación oficial') 
				objLeadUpd.TAM_IdentificacionOficial__c = false;        		  
        	if (nombreArchivo == 'Comprobante de domicilio') 
				objLeadUpd.TAM_ComprobanteDomicilio__c = false;        		  
        	if (nombreArchivo == 'Documentación de soporte') 
				objLeadUpd.TAM_DocumentacionSoporte__c = false; 
   			System.debug('EN TAM_CargaArchivosCtrl.deleteFiles objLeadUpd:  ' + objLeadUpd);
			//Metelo a la lista de lLeadUpd
        	update objLeadUpd;
    	}//fin si sIdLead != '' && sFileDescripcion != ''
    	
    	//Finalmente elimina el reg del aechivo
        delete [SELECT Id,Title,FileType from ContentDocument WHERE id=:sdocumentId];       
    }      
    

   @AuraEnabled  
   public static String consultDatosCand(string recordId){ 
    	System.debug('EN TAM_CargaArchivosCtrl.consultDatosCand recordId:  ' + recordId);
    	String sTipoPersona = '';
    	
    	
    	for (Lead candidato : [Select id, TAM_TipoCandidato__c, FWY_Tipo_de_persona__c, TAM_EnviarAutorizacion__c,
    		RecordTypeId From Lead Where id = :recordId]){
    		System.debug('EN TAM_CargaArchivosCtrl.consultDatosCand candidato.TAM_EnviarAutorizacion__c:  ' + candidato.TAM_EnviarAutorizacion__c + ' RecordTypeId: ' + candidato.RecordTypeId );	
    		//Ve primero si ya tiene una autorización pendienye
    		if (candidato.TAM_EnviarAutorizacion__c && ( candidato.RecordTypeId == VaRtLeadlotilla ||  candidato.RecordTypeId == VaRtLeadPrograma) )
    			sTipoPersona = 'AutorizacionPendiente';
    		else if (candidato.TAM_EnviarAutorizacion__c && candidato.RecordTypeId == VaRtLeadClienteCreado )
				sTipoPersona = 'Autorizado';    		
    		//sTipoPersona = candidato.TAM_TipoCandidato__c;
    		else if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Programa')
    			sTipoPersona = 'Programa';
			else if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Flotilla')
				sTipoPersona = 'Programa';				
			else if (candidato.FWY_Tipo_de_persona__c == 'Persona moral' && candidato.TAM_TipoCandidato__c == 'Flotilla')
				sTipoPersona = 'Flotilla';
    		else if (candidato.FWY_Tipo_de_persona__c == 'Persona moral' && candidato.TAM_TipoCandidato__c == 'Programa')
				sTipoPersona = 'Flotilla';					
			else if (candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial' && candidato.TAM_TipoCandidato__c == 'Flotilla')
				sTipoPersona = 'FlotillaPF';
    		else if (candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial' && candidato.TAM_TipoCandidato__c == 'Programa')
				sTipoPersona = 'ProgramaPF';
    	}
    	
    	//Ve si no encotro nada en la consulta del Lead y entonces consulta lois datos de la cuenta 
    	if (sTipoPersona == ''){
    		for (Account objCliente : [Select id, TAM_EstatusCliente__c From Account Where ID =: recordId]){
    			if (objCliente.TAM_EstatusCliente__c == 'Preautorización')
    				sTipoPersona = 'Preautorizacion';	
    			if (objCliente.TAM_EstatusCliente__c == 'Vencido')
    				sTipoPersona = 'ClienteVencido';
    		}
    	}//Fin si sTipoPersona == ''
    	
    	System.debug('EN TAM_CargaArchivosCtrl.consultDatosCand sTipoPersona:  ' + sTipoPersona);
        return sTipoPersona;  
    } 
    
}