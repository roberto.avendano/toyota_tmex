public class SolicitudRegresarController {
    private final SolicitudInternaVehiculos__c siv;
    public ApexPages.StandardController sivController;
    
    public SolicitudRegresarController(ApexPages.StandardController stdController) {
        this.sivController = stdcontroller;
        this.siv = getSIV(stdController.getId());
    }
    
    public SolicitudInternaVehiculos__c getSIV(String sivId){
        return [SELECT Id, EnviarSolicitud__c, Regresar__c FROM SolicitudInternaVehiculos__c WHERE Id =:sivId];
    }
    
    public PageReference regresarSiv(){
        System.debug(siv.Regresar__c);
        siv.EstatusDOD__c = '';
        siv.Regresar__c = true;
        try{
            update siv;
            System.debug('Se actualizo siv: ' +siv.Regresar__c);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Se actualizó la solicitud'));
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: ' + e.getMessage()));
        }
        PageReference pageRef;
        return pageRef;
    } 
    
    public pagereference backMethod(){
        PageReference  pageRef;
        String csId = siv.Id;  
        update (SolicitudInternaVehiculos__c)sivController.getRecord();
        pageRef = new PageReference('/' +csId);
        pageRef.setRedirect(true);
        return pageRef;
    }  
}