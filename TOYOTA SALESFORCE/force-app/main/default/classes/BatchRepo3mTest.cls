@isTest
public class BatchRepo3mTest {
  	@testSetup
    public static void prueba () {
      List<fwy_ReporteComunidades__c> pruebalist = new List<fwy_ReporteComunidades__c>();
      
        for(integer a=0;a<10;a++){
            fwy_ReporteComunidades__c ej1 = new fwy_ReporteComunidades__c(); 
            ej1.fwy_Segmento__c = 'Prospectos';
            ej1.fwy_Subsegmento__c = 'Incompletos y Condicionados';
            ej1.Name = 'Reporte1'+a;
            pruebalist.add(ej1);
      }
        insert pruebalist;
 	} 
    static testmethod void test(){
        Test.startTest();
        BatchRepo3m uca = new BatchRepo3m();
        Database.executeBatch(uca);
        Test.stopTest();
    }
}