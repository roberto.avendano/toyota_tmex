public with sharing class DefaultValuesOrderEditController {
	private User currentUser;
	private String associateContact;
	private String parametros;
	private ParametrosConfiguracionToyota__c pcIFE;
	private ParametrosConfiguracionToyota__c pcCirculacion;
	private ParametrosConfiguracionToyota__c pcFormato;
	private ParametrosConfiguracionToyota__c pcRazonSocial;
	private ParametrosConfiguracionToyota__c pcFactura;
	private ParametrosConfiguracionToyota__c pcFotoVIN;
	private ParametrosConfiguracionToyota__c pcFotoParte;
	private ParametrosConfiguracionToyota__c pcFotoPlaca;
	private ParametrosConfiguracionToyota__c pcCartaRobo;
	private ParametrosConfiguracionToyota__c pcOtros;

	public DefaultValuesOrderEditController(ApexPages.StandardController controller) {
		this.pcIFE = ParametrosConfiguracionToyota__c.getInstance('FormatoPRIFE');
		this.pcCirculacion = ParametrosConfiguracionToyota__c.getInstance('FormatoPRCirculacion');
		this.pcFormato = ParametrosConfiguracionToyota__c.getInstance('FormatoPRSolicitud');
		this.pcRazonSocial = ParametrosConfiguracionToyota__c.getInstance('FormatoPRRazonSocial');
		this.pcFactura = ParametrosConfiguracionToyota__c.getInstance('FormatoPRFactura');
		this.pcFotoVIN = ParametrosConfiguracionToyota__c.getInstance('FormatoPRFotoVIN');
		this.pcFotoParte = ParametrosConfiguracionToyota__c.getInstance('FormatoPRFotoParte');
		this.pcFotoPlaca = ParametrosConfiguracionToyota__c.getInstance('FormatoPRFotoPlaca');
		this.pcCartaRobo = ParametrosConfiguracionToyota__c.getInstance('FormatoPRCartaRobo');
		this.pcOtros = ParametrosConfiguracionToyota__c.getInstance('FormatoPROtros');
	}

	public PageReference goToEdit(){		
		currentUser = [SELECT Id, ContactId FROM User WHERE Id=:UserInfo.getUserId()];
		 
		if(currentUser.ContactId!=null){
			try{
				associateContact = [SELECT Id, AccountId FROM Contact WHERE Id=: currentUser.ContactId].AccountId;
				Account cuenta = getCuenta(associateContact);

				String ife = '&'+pcIFE.Valor__c+'='+ getStatus(cuenta.IFELicencia__c);
				String circulacion = '&'+pcCirculacion.Valor__c+'='+ getStatus(cuenta.TarjetaCirculacion__c);
				String formato = '&'+pcFormato.Valor__c+'='+ getStatus(cuenta.FormatoSolicitudPartesRobadas__c);
				String razon = '&'+pcRazonSocial.Valor__c+'='+ getStatus(cuenta.CartaMembretadaRazonSocial__c);
				String factura = '&'+pcFactura.Valor__c+'='+ getStatus(cuenta.CopiaFacturaOriginal__c);
				String fotoVIN = '&'+pcFotoVIN.Valor__c+'='+ getStatus(cuenta.FotoVehiculo__c);
				String fotoParte = '&'+pcFotoParte.Valor__c+'='+ getStatus(cuenta.FotoParte__c);
				String fotoPlaca = '&'+pcFotoPlaca.Valor__c+'='+ getStatus(cuenta.FotoPlacaVIN__c);
				String cartaRobo = '&'+pcCartaRobo.Valor__c+'='+ getStatus(cuenta.CartaRobo__c);
				String otros = '&'+pcOtros.Valor__c+'='+ getStatus(cuenta.Otros__c);

				parametros = ife+circulacion+formato+razon+factura+fotoVIN+fotoParte+fotoPlaca+cartaRobo+otros;
			} catch(Exception e){
				
			}
		} else {
			associateContact='';
			parametros = '';
		}


        return new PageReference('/801/e?nooverride=1&EffectiveDate='+getCurrentDate()+'&accid_lkid='+associateContact+parametros); 
	}

	private Account getCuenta(String accountID){
		return [SELECT IFELicencia__c, TarjetaCirculacion__c, FormatoSolicitudPartesRobadas__c,
					CartaMembretadaRazonSocial__c, CopiaFacturaOriginal__c, FotoVehiculo__c,
					FotoParte__c, FotoPlacaVIN__c, CartaRobo__c, Otros__c 
				FROM Account WHERE Id=:accountID];
	}

	private String getStatus(Boolean conditional){
		return conditional? '1':'0';
	}


	public String getCurrentDate(){
		return Date.today().day() + '/'+ Date.today().month()+'/'+Date.today().year();
	}
}