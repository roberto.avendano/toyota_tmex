@isTest
public class TAM_CargaDocumentosController_Test {
    @testSetup static void setup() {
        
        //VIN de prueba
        Vehiculo__c v01 = new Vehiculo__c(
            Name='12345678901234567',
            Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        //Provisión de incentivos
        TAM_ProvisionIncentivos__c provTest01 = new TAM_ProvisionIncentivos__c();
        provTest01.name = 'Provisión Agosto de prueba';
        provTest01.TAM_AnioDeProvision__c = '2020';
        provTest01.TAM_MesDeProvision__c = 'Agosto';
        insert provTest01;
        
        //Detalle de Provisión
        String recordIdProvisionRetail  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        //Retail
        TAM_DetalleProvisionIncentivo__c detalleProv01 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv01.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv01.RecordTypeId = recordIdProvisionRetail;
        detalleProv01.RetailSinReversa_Cerrado__c = false;
        detalleProv01.TAM_AnioModelo__c = '2020';
        detalleProv01.TAM_AplicanAmbos__c = false;
        detalleProv01.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv01.TAM_CodigoContable__c = '7400B2';
        detalleProv01.TAM_CodigoDealer__c = '57002';
        detalleProv01.TAM_Factura__c = 'FVA12330';
        detalleProv01.TAM_FechaCierre__c = date.today();
        detalleProv01.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv01.TAM_FechaVenta__c = '2020-06-17';
        detalleProv01.TAM_FirstName__c = 'DORA LUZ';
        detalleProv01.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv01.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv01.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv01.TAM_Lealtad__c = false;
        detalleProv01.TAM_Modelo__c = '7495';
        detalleProv01.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv01.TAM_PagadoConTFS__c = false;
        detalleProv01.TAM_PagadoSinTFS__c = true;
        detalleProv01.TAM_ProvicionarEfectivo__c = true;
        detalleProv01.TAM_Provisionado__c = false;
        detalleProv01.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv01.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv01.TAM_ProvisionarLealtad__c = false;
        detalleProv01.TAM_ProvisionCerrada__c = true;
        detalleProv01.TAM_Serie__c	 = 'HILUX';
        detalleProv01.TAM_SolicitadoDealer__c = false;
        detalleProv01.TAM_TipoMovimiento__c = 'RDR';
        detalleProv01.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv01.TAM_TransmitirDealer__c = true;
        detalleProv01.TAM_VIN__c  =  v01.id;
        detalleProv01.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv01;
        
        
        
        TAM_DetalleEstadoCuenta__c detalleEdoCta = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCta.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCta.TAM_AnioModelo__c = '2020';
        detalleEdoCta.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCta.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCta.TAM_CodigoDealer__c = '57011';
        detalleEdoCta.TAM_Codigo_Producto__c = '7400';
        detalleEdoCta.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCta.TAM_EnviadoFacturar__c = false;
        detalleEdoCta.TAM_EdoCtaCerrado__c = false;
        detalleEdoCta.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCta.TAM_VINEntregado__c = true;
        detalleEdoCta.TAM_VIN__c = '12345678901234567';
        detalleEdoCta.TAM_pagoPorTFS__c = false;
        detalleEdoCta.TAM_Serie__c = 'HILUX';
        insert detalleEdoCta;
        
        ContentVersion cv = new ContentVersion();
		cv.Title = 'Test Document';
		cv.PathOnClient = 'TestDocument.pdf';
		cv.VersionData = Blob.valueOf('Test Content');
		cv.IsMajorVersion = true;
		Insert cv;
        
    }
    
    
    @isTest static void testMethod1(){
        TAM_DetalleEstadoCuenta__c detalleDeEstado = [select id from TAM_DetalleEstadoCuenta__c WHERE TAM_VIN__c = '12345678901234567'];
        //Se obtiene el documento
        ContentVersion documento  =  [Select id from ContentVersion where title = 'Test Document'];
        
        TAM_CargaDocumentosController.saveDocumentId(detalleDeEstado.id,documento.id,'DeTest');
        TAM_CargaDocumentosController.deleteDocument(documento.Id,detalleDeEstado.id);
        
    }
    
    
    
}