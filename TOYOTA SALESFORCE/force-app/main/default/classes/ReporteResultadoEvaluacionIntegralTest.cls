@isTest //(SeeAllData=true)
public class ReporteResultadoEvaluacionIntegralTest {
		private static String tipoEvaluacionED = 'Evaluacion_Integral';
    
    @testSetup
    static void getEvaluacion(){
        
          RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];

          RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
                
          Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];

        	  
        
        User u = new User(
                        Alias = 'ConsTSM', 
                        Email='ConsultorTSM@testorgtoyota.com',
                        LastName='TSM',
                        ProfileId = p.Id,
                        UserName='standarduser@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert u;
		
        			Document testDoc = new Document(
					Name='ToyotaReporteIntegralLogo',
					DeveloperName='ToyotaReporteIntegralLogoDe',
					FolderId= u.Id		
				);
        
				insert testDoc;
        
                Account dealer = new Account(
                        RecordTypeId = recordTypeDealer.Id,
                        Name = 'Test Record'
                );
                insert dealer;


                Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
                        RecordTypeId = recordTypeED.Id,
                        Consultor_TSM_Titular__c = u.Id,
                        Nombre_Dealer__c = dealer.Id
                );
                insert evalDealer;


                Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
                    Celula_Area__c='DO',
                    Observaciones__c='Observaciones__c',
                    Condicion_Observada__c='Condicion_Observada__c',
                    Referencia__c='TSM',
                    Cuenta__c=evalDealer.Nombre_Dealer__c                    
                );
                insert api01;
        
    }
    
    
    @isTest static void  ReporteResultadoEvaluacionIntegralTest(){
        Test.startTest();
        Evaluaciones_Dealer__c eDc=[SELECT Id,Name FROM Evaluaciones_Dealer__c LIMIT 1];
        ApexPages.StandardController sc=new ApexPages.StandardController(eDc); 
        ReporteResultadoEIntegralController rIntegral = new ReporteResultadoEIntegralController(sc);
        String aux=rIntegral.doc;
        rIntegral.getObjEval();
        ReporteResultadoEIntegralController rIntegralController = new  ReporteResultadoEIntegralController();
        Test.stopTest();
        
        
    }
}