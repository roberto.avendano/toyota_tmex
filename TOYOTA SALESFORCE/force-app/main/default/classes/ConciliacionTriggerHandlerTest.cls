@isTest
public class ConciliacionTriggerHandlerTest {
    
    @isTest
    static void test_one(){
        Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        Vehiculo__c vin = new Vehiculo__c(
            Name = 'TESTCONCILIACION',
            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id                    
        );
        insert vin;
        
        
        SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c(				
            VIN_Name__c = 'TESTCONCILIACION'
        );
        try{
            insert sad;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        SolicitudAutoDemo__c sad1 = new SolicitudAutoDemo__c(				
            VIN__c = vin.Id
        );
        try{
            insert sad1;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        test.startTest();
        Conciliacion__c con = new Conciliacion__c(
            VIN__c='TESTCONCILIACION',
            InteresesVencidos__c=20
        );
        try{
            insert con;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        con.VIN__c = 'TESTCONCILIACION';
        try{
            update con;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        Conciliacion__c con1 = new Conciliacion__c(
            VIN__c='TESTCONCILIAC',
            InteresesVencidos__c=20
        );
        try{
            insert con1;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        Conciliacion__c con2 = new Conciliacion__c(
            VIN__c='',
            InteresesVencidos__c=20
        );
        try{
            insert con2;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        Conciliacion__c con3 = new Conciliacion__c(
            VIN__c='TESTCONCILIACION',
            InteresesVencidos__c=20
        );
        try{
            insert con3;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        test.stopTest();
    }
    
    @isTest
    static void test_two(){
        Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        
        MotivoRechazo__c motivo = new MotivoRechazo__c(
            Tipo__c= Constantes.CASE_STATUS_AUTORIZACION_BAJA_TMEX
        );
        insert motivo;
        
        Account daltonDealer = new Account(
            Name='Toyota Dalton López Mateos',
            Codigo_Distribuidor__c = '57055',
            UnidadesAutosDemoAutorizadas__c = 15
        );
        insert daltonDealer;
        
        Serie__c serie = new Serie__c(
            Name = 'YARIS SD'
        );
        insert serie;
        
        Modelo__c modelo = new Modelo__c(
            Name = '1091 - Yaris Sedan',
            Tipo_de_vehiculo__c = 'Camioneta',
            ID_Modelo__c = '1091',
            Serie__c = serie.Id
        );
        insert modelo;
        
        Vehiculo__c vin1= new Vehiculo__c(
            Name='MHKMF53EXHK007709',          
            Distribuidor__c=daltonDealer.Id,
            Modelo_b__c = modelo.Id,
            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id            
        );
        insert vin1;
        
        Vehiculo__c vin2= new Vehiculo__c(
            Name='3MYDLAYV0HY167430',          
            Distribuidor__c=daltonDealer.Id,
            Modelo_b__c = modelo.Id,
            RecordTypeId= recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id     
        );
        insert vin2;
        
        PeriodoFiscal__c pf2017 = new PeriodoFiscal__c(
            Name = '2017',
            IdPeriodoFiscal__c = '2017',
            FechaInicioPeriodoFiscal__c = Date.newInstance(2016, 4, 1),
            FechaFinPeriodoFiscal__c = Date.newInstance(2017, 3, 31)			
        );
        insert pf2017;
        
        //Obtener la fecha de hoy
        date today = Date.today(); 
        //Obtener el año acual
        integer year = System.Today().year(); 
        //Obtener la fecha de inicio del periodo fiscal del año actual (1 de Abril)
        date inicioPeriodoEstandar 	= Date.newInstance(year, 4, 1); System.debug('AVX inicioPeriodoEstandar = '+inicioPeriodoEstandar);
        
        //Si la fecha actual es mayor a la fecha de inicio del periodo fiscal
        integer periodoActual= (today>inicioPeriodoEstandar) ? year+1:year; System.debug('AVX periodoActual = '+periodoActual);
        
        //Definir el periodo de acuerdo al año fiscal actual
        date inicioPeriodo  = (periodoActual == year) ? Date.newInstance(year-1, 4, 1) : Date.newInstance(year, 3, 31);
        date finPeriodo		= (periodoActual != year) ? Date.newInstance(year, 3, 31) : Date.newInstance(year+1, 4, 1);
       	System.debug('AVX inicioPeriodo = '+inicioPeriodo);
        System.debug('AVX finPeriodo = '+finPeriodo);
        PeriodoFiscal__c pf2018 = new PeriodoFiscal__c(
            Name = String.valueOf(periodoActual),
            IdPeriodoFiscal__c = String.valueOf(periodoActual),
            FechaInicioPeriodoFiscal__c = inicioPeriodo,
            FechaFinPeriodoFiscal__c = finPeriodo			
        );
        insert pf2018;
        
        Presupuesto__c p2017 = new Presupuesto__c(
            PeriodoFiscal__c = pf2017.Id,
            RecordTypeId= recordTypesMap.get('Presupuesto__c').get('AutosDemo').Id,
            PresupuestoAutorizadoFinanzas__c = true,
            PresupuestoAutorizadoEnero__c = 10000, 
            PresupuestoAutorizadoFebrero__c = 2000, 
            PresupuestoAutorizadoMarzo__c = 200, 
            PresupuestoAutorizadoAbril__c = 10000, 
            PresupuestoAutorizadoMayo__c = 10000, 
            PresupuestoAutorizadoJunio__c = 10000, 
            PresupuestoAutorizadoJulio__c = 10000,
            PresupuestoAutorizadoAgosto__c = 10000, 
            PresupuestoAutorizadoSeptiembre__c = 10000, 
            PresupuestoAutorizadoOctubre__c = 10000,
            PresupuestoAutorizadoNoviembre__c = 10000, 
            PresupuestoAutorizadoDiciembre__c = 10000            
        );
        insert p2017;
        
        Presupuesto__c p2018 = new Presupuesto__c(
            PeriodoFiscal__c = pf2018.Id,
            RecordTypeId= recordTypesMap.get('Presupuesto__c').get('AutosDemo').Id,
            PresupuestoAutorizadoFinanzas__c = true,
            Activo__c=true,
            PresupuestoAutorizadoEnero__c = 20000, 
            PresupuestoAutorizadoFebrero__c = 2000000, 
            PresupuestoAutorizadoMarzo__c = 20000, 
            PresupuestoAutorizadoAbril__c = 20000, 
            PresupuestoAutorizadoMayo__c = 20000, 
            PresupuestoAutorizadoJunio__c = 20000, 
            PresupuestoAutorizadoJulio__c = 20000,
            PresupuestoAutorizadoAgosto__c = 20000, 
            PresupuestoAutorizadoSeptiembre__c = 20000, 
            PresupuestoAutorizadoOctubre__c = 20000,
            PresupuestoAutorizadoNoviembre__c = 20000, 
            PresupuestoAutorizadoDiciembre__c = 20000            
        );
        insert p2018;	
        System.debug(JSON.serialize(p2018));
        AsignacionPresupuesto__c ap = new AsignacionPresupuesto__c(
            Distribuidor__c = daltonDealer.Id,
            Presupuesto__c = p2017.Id,			
            AsignadoEnero__c = 2000, 
            AsignadoFebrero__c = 1500, 
            AsignadoMarzo__c = 1200, 
            AsignadoAbril__c = 1100,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 1500,
            AsignadoJulio__c = 1400, 
            AsignadoAgosto__c = 2300,
            AsignadoSeptiembre__c = 4500,
            AsignadoOctubre__c = 3400,
            AsignadoNoviembre__c = 7800,
            AsignadoDiciembre__c = 4500
        );
        insert ap;
        
        
        AsignacionPresupuesto__c ap2 = new AsignacionPresupuesto__c(
            Distribuidor__c = daltonDealer.Id,
            Presupuesto__c = p2018.Id,						
            AsignadoEnero__c = 2000, 
            AsignadoFebrero__c = 1500, 
            AsignadoMarzo__c = 1200, 
            AsignadoAbril__c = 1100,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 1500,
            AsignadoJulio__c = 1400, 
            AsignadoAgosto__c = 2300,
            AsignadoSeptiembre__c = 4500,
            AsignadoOctubre__c = 3400,
            AsignadoNoviembre__c = 7800,
            AsignadoDiciembre__c = 4500
        );
        insert ap2;
        
        SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c(
            Estatus__c = 'Solicitud baja',
            VIN__c = vin1.Id
        );
        try{
            insert sad;
        }catch(Exception ex){
            
        }
        
        SolicitudAutoDemo__c sad2 = new SolicitudAutoDemo__c(
            Estatus__c = 'Solicitud baja',
            VIN__c = vin2.Id
        );
        try{
            insert sad2;
        }catch(Exception ex){
            
        }
        
        Conciliacion__c con = new Conciliacion__c(
            SolicitudAutoDemo__c = sad.Id,
            VIN__c = vin1.Name,
            CodigoDistribuidor__c = '57055',
            ValidacionTMEX__c = false,
            AsignacionPresupuesto__c = ap2.Id
        );
        try{
            insert con;
        }catch(Exception ex){
            
        }	
        
        Conciliacion__c con2 = new Conciliacion__c(
            SolicitudAutoDemo__c = sad2.Id,
            VIN__c = vin1.Name,
            CodigoDistribuidor__c = '57055',
            ValidacionTMEX__c = false,
            AsignacionPresupuesto__c = ap2.Id
        );
        try{
            insert con2;
        }catch(Exception ex){
            
        }
        
        test.startTest();
        con.ValidacionTMEX__c=true;
        try{
            update con;
        }catch(Exception ex){
            
        }	
        
        con.FechaDeConciliacion__c = Date.today().addMonths(1);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        
        con.FechaDeConciliacion__c = Date.today().addMonths(2);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        
        con.FechaDeConciliacion__c = Date.today().addMonths(3);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        
        con.FechaDeConciliacion__c = Date.today().addMonths(4);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        
        con.FechaDeConciliacion__c = Date.today().addMonths(5);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(6);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(7);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(8);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(9);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(10);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        con.FechaDeConciliacion__c = Date.today().addMonths(11);
        con.recalculateFormulas();	
        try{
            update con;
        }catch(Exception e){
            
        }
        test.stopTest();
    }
    
}