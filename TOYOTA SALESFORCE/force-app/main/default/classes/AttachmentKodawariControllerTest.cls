@isTest
private class AttachmentKodawariControllerTest {   	
   	private static String tipoEvaluacion = 'Integral';
	public static Evaluaciones_Dealer__c getTestDealer(){
		///Registro
		String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
		String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
		RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
		RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
		RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
		
		RecordType recordTypeAPI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType='Actividad_Plan_Integral__c' and DeveloperName='API_TSM' limit 1];
		RecordType recordTypePreguntaKodawari = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Evaluacion_Integral' limit 1];
		RecordType recordTypePreguntaEI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Kodawari' limit 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
		
		User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;

		Document testDoc = new Document(
			Name='testDoc',
			DeveloperName='withoutTypeImg',
			FolderId= u.Id		
		);
		insert testDoc;
		
		Seccion__c seccion01 = new Seccion__c(
			Name = 'Secccion01',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccion01;
		
		Pregunta__c pregunta01 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta01;

		Pregunta__c pregunta02 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta02;		

		Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta01.Id
		);
		insert oe01;

		Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta02.Id
		);
		insert oe02;

		Account dealer = new Account(
			RecordTypeId = recordTypeDealer.Id,
			Name = 'Test Record'
		);
		insert dealer;

		Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
			RecordTypeId = recordTypeED.Id,
			Consultor_TSM_Titular__c = u.Id,
			Nombre_Dealer__c = dealer.Id
		);
		insert evalDealer;

		Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
            Evaluacion_Dealer__c = evalDealer.Id,
            Usuario__c = u.Id,
            Seccion_Toyota_Mexico__c = seccion01.Id
        );
        insert rsc01;
        
        Respuestas_Preguntas_TSM__c resp1 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta01.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp1;
		
		Respuestas_Preguntas_TSM__c resp2 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta02.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp2;
                
        update evalDealer;

        //List<Respuestas_Preguntas_TSM__c> listrespPreg = [SELECT Id, Evaluacion_Dealer__c, Pregunta_Relacionada__c, (SELECT Id FROM Respuestas_ObjetosTSM_del__r) FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c =:evalDealer.Id limit 1];
        //if(listrespPreg.size()>0){
        	//Respuestas_Preguntas_TSM__c respPreg = listrespPreg.get(0);
	        Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
	            Celula_Area__c='DO',
	            Observaciones__c='Observaciones__c',
	            Condicion_Observada__c='Condicion_Observada__c',
	            Referencia__c='TSM',
	            Cuenta__c=evalDealer.Nombre_Dealer__c,
	            Respuestas_Preguntas_TSM__c=resp1.Id
	        );
	        insert api01;
        
	    	Respuestas_ObjetosTSM__c ro01 = new Respuestas_ObjetosTSM__c(
				Objeto_Evaluacion_Relacionado__c=oe01.Id, 
				Pregunta_Relacionada_del__c=resp1.Pregunta_Relacionada__c, 
				Respuestas_Preguntas_TSM_del__c=resp1.Id
	    	);
	    	insert ro01;
	        
		//}
		String base64Cad='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAA6AGgDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KK/nz/4Oov+CnHx8/Yz/wCCg3hvw18L/ij4m8GaBeeDLW/lsdPlRYnna6ulaQgqTuKog/4CK/M7/h/p+2J/0X7x3/3/AI//AIiqsB/Z7RX8YX/D/T9sT/ov3jv/AL/x/wDxFH/D/T9sT/ov3jv/AL/x/wDxFHKB/Z7RX8YX/D/T9sT/AKL947/7/wAf/wARWv4L/wCDib9szwNrsOoW/wAc/E180LhjDqENveQSAdmSSMgg/nRygf2TUV+Qf/BCT/g5xs/+CgPjux+Evxj0/SvCvxOvlK6NqljmPTfETqMmHYxJhuCASFyVfBA2nCn4x/4OSf8Agq9+0X+yb/wVQ8UeDvh18WvFXhPwva6Ppk8OnWMqLDE8luGcgFT1bk1IH9JVFfgH/wAGtP8AwW0+KH7Rf7YPiT4S/Grx9q3jKTxbpRvvDU+qOrPb3drlpYEIA/1kLM+PWD3r9/KACivjH/gvj/wUAuP+CdP/AATY8Z+L9E1D+zvGuuBPD/hiVceZFe3GR5yg8ZiiWWQZBGUHrX8yCf8ABfH9sQ4X/hf3jzHT/j4j/wDiKAP7PaK4H9lPxJf+Mv2Yfh3q+q3Ut9qeqeGtOu7u5k+/PK9tGzufcsSfxooA8b/bP/4I0/s6/wDBQT4pWnjT4seAv+En8R2OnppcN1/al3a7bdHd1TbFIqnDSOckZ5r4V/4K5f8ABvx+yb+zF/wTZ+MHj3wX8L/7J8VeGdBkvNNvf7avpvs0odAG2PMVbgnggiv2Mr5B/wCC+H/KHn4/f9ivL/6MSmB/IL+yL4I0v4mftV/DXw5rdt9s0bXvFOm6dfW+8p58Et1HHIm5SCMqxGQcjNfrB/wdP/8ABK34F/8ABOf4Q/CW++D/AMP18L3PijV76DU7z+0bq63pFDG0ceJpHC5LseMH5a/LT9gr/k+L4O/9jro//pbDX9df/Bar/gmdp/8AwVP/AGHda8AC4g0/xZpkg1nwtfSkBINQiRgqOe0cqs0bHtuDc7QKYH82n/Bu3/wTs+Ev/BSf9tq88G/FrxDc6fpemaO+p2OjW10LSfxFMrqphWTqAisXKp8xC8EAE1+y37e3/Bp5+zd4s/Ze8USfCXw5q3gj4gaPpk95o9zFq9zdw3s8SF1gnjmdwVkI27l2kFgecYP87fjv9i348/spfGa40nUvAPxA8MeMPDF0CstpY3Akt5FOVlhmiGCD1V0YgjkGvsj9nP8A4Lmf8FCP2eZrSI3/AI18baVa4VrDxT4ZfUFmUfwtN5Yn56Z8zNAuZH50eAfGurfCX4jaP4h0e6m07W/DeoQ39ncRtte3nhkDowPqGUGvuj/g5d+IbfF3/gpVH4saNYW8TeBfDmqNGOiGbT45CP8Ax6v1C/4JXfte/st/t2eOLLwV8Xv2L/Bnwr+ImtTbLW9fwQkuiazO2SVWV4Q0EjHJCyZUnjeSQK+Cf+DoP9mXxdr3/BWXxB/wh3gLxFe+HbHw7o9nZnStHmks4kjtVURxmNSoCgAYHTGKB3Pzx/ZO/aL1r9kf9pXwP8TPD7sureCtYt9VhUNtE4jcF4mP9103IfZjX9xP7Pfxu0X9pT4GeEfiB4cmFxofjLSbfV7JwckRzRhwp/2lzgjsQRX8PH/DIvxV/wCia+PP/BDdf/EV/Qt/wbX/ALdHin4G/wDBKj4oeGPiH4U8X22ofAeC51jRrW60u4jm1SwnSSaO2hDLl3Fwsi4GcCVO1DFdHxT/AMHhn7fP/C+v21dF+DOjXXmaB8IbYvqGx8pPqt0qO+cdfKi8tPZmkFfj7H/rF+te0/Gz4RfG74+/GDxR448QfD/x9ea54t1S41a+mbQro75ppGkb+DplsAdgBXML+yP8VFYE/Dbx3wf+gFc//EUdB3R/bZ+xd/yZ/wDCv/sUtL/9JIqKk/Y6s5tO/ZK+GNvcQyW9xB4U0yOWKRSrxsLWMFWB5BB4IPSipA9Ir5B/4L4f8oefj9/2K8v/AKMSvr6vkH/gvh/yh5+P3/Yry/8AoxKAP5Gv2Cv+T4vg7/2Ouj/+lsNf06/8FuviFo/w3/aC+Ht14i8U+H9H0e50G9iNjqXitdEa6k8xcSR5kTcUJHPIGa/mK/YK/wCT4vg7/wBjro//AKWw1/Tn/wAHPn/BLXVP+CiH7Edr4i8GWJvviH8JpJtX0+0Rcy6pZOg+12ydzIRGkiDuYto5YVyY/AwxdH2M20rp6eXqmfRcK8TYjIcwWY4WMZSSlG0r2tJOL+FxaaT0aasz5q+Bukr+1l4y1DTfhj9m+ID6TGLi8h0T4ixXFxbwMqqrND9r+VTJvGdxxuU84wd/x1/wTf8A2gte8N6eul/DnxppetW9/by3Ei+N42tbi0VQJYSpuN292y28EdcYHWv58f2Y/wBqj4jfsO/Gyz8b/DnxFqfg/wAXaOWh8+EfeU8PDNGwKuhxyjgjIHGQK+zPjZ/wdLftgfG74X3/AIVuPGui+H7XVLZrS7vdE0eK0vpI2GGCy8mMkZG6PaRngivKjwzh1tKX4f5H30vGjOHJSdGnp/19f51GfaPjj4yfDbQNU1TS7jxFocOoWbS2klvdfFi3ie0uFuiTuxdHlYh5WOxGeayPE/xH8JfDm7s4dX8TWeipqFq2oWcVz8TYpGvLSaKT7HOj/agAMmN2IBD4ONoHP5L/ALCn7D3xA/4KK/tK6H8OfAem3WoaprFwpvr4ozW+lW24ebdXD9FRBk8nLHAGSQK+mP8Ag5Q+DGlfs4/8FIYfh7oYb+x/BHgfw9olmWADNHBYpGGOO5xk+5rT/V6h/M/w/wAjzf8AiK2aWt7OH31P/lh9yfA/w1ca/wDso3Hj7UptW8WaB4P1HyfE3jDSvifEumwqXBW3c+f+7YiWFS2OhHc5rkf+F7/Cl1s2l8aLJNb26QuR8W4FWWVWjJuCPPzllRwUBA/eEgjjH0F/waX/AAN0T9pj/gj58evh94it1uND8ZeJr3SbxSoOEm022TcP9pchgexUGvwB/aY+AutfsuftB+Mvh14ihaHWvBmr3Ok3QIwHaKQqHH+ywAYHuGFNcP0NuZ/h/kTPxSzOUnJ04au+ntEvkufReR+5P7PmkWP7Uuv6hpnw7sdZ+I19o9jNfX0GjfEuLzLZTOCk0mLkhYgpEe3r7k81oy/CLXdQsNQbTPDesXF3sP2J2+JkbQpiBVYSILrLbZAXDKf4iCOmPaP+DVf9h/8A4Zg/4JaeLPizqto1r4m+LkVxqEMpTE0Ol20ciWwHfDv50o9Q6HtXwB4J/wCC5vhPQf8Agn34m8F33jjxYPiReavNNbTf2IJZmjaW3KEXxfdGqpHMPLCnO/rWNbJaMIuS5pabK3+ROC8SsxrZhRwc4QhGo7OcnU5YarWXv7a/cmf0Df8ABL/4c+KvhN+wX8NfD/jbzP8AhJ7DTD9uMl4LxizSO6nzQWD5VlOcniivQ/2Zte/4Sn9nLwDqXmSTf2h4esLnzJPvvvt42yfc5or3aMVCnGK6JI+BzLESxGLq15WvKUm7batvTy7eR3Fcf+0B8BfC37UHwb8QeAPG2m/2x4V8UWpstSs/OeH7RESCV3oQy8gcgg12FFaHGfCfgL/g2v8A2Ofhn440XxHovwqe11jw/fQ6jYznXtQfyZ4XEkbbWmIOGUHBBBxX3ZRRQB8R/tyf8G9X7Lv7fHim68R+KPA7eH/Fd8xe61rw1cf2bc3bHq0qgGKRj/eZCx9a+bfCX/Bmh+yzoGvR3WoeIPivrdrG4b7HcavbRRuM/dZo7dXwenDA1+t1FAHkf7IX7CXwl/YO8BN4b+E/gfRfB+nTENcvaxlrm+YDAaadyZJT/vMcdsV5L+1t/wAENv2Zf25PjVefEL4mfD1vEHizUIIbae9Gr3ltvjiUJGNkUqrwoxnFfW1FAHjP7Ev/AAT++FP/AATu+HmqeFfhH4ZPhfQ9Y1A6pd25vZ7rzbgxpGX3TOzD5UUYBxxXkv7Uv/BBz9lr9s3426t8RPiF8NI9Y8Xa4Ihf3sWq3lp9pMaLGrMkUqru2KoJxk4Ga+wKKAOd8J/Cfw94E+FOn+CNH0yHT/C+l6WmjWlhDlY4LRIhEsQOc4CDGc5r4gf/AINe/wBieR2Y/CN8sdx/4qHUf/j9foDRQBl+CfB2n/DvwbpOgaRb/ZdK0SzisLOHcW8qGJAiLkkk4VQMk5orUooA/9k=';
		Attachment attachment = new Attachment();		
  			attachment.Body = Blob.valueOf(base64Cad);
  			attachment.Name = String.valueOf('test.txt');
			attachment.ParentId = evalDealer.Id;
  		insert attachment;
  		
  		Attachment attachment2 = new Attachment();		
  			attachment2.Body = Blob.valueOf(base64Cad);
  			attachment2.Name = String.valueOf('test.txt');
			attachment2.ParentId = resp1.Id;
  		insert attachment2;
		
		Attachment attachment3 = new Attachment();		
  			attachment3.Body = Blob.valueOf(base64Cad);
  			attachment3.Name = String.valueOf('test.txt');
			attachment3.ParentId = oe02.Id;
  		insert attachment3;
		
		Attachment attachment4 = new Attachment();		
  			attachment4.Body = Blob.valueOf(base64Cad);
  			attachment4.Name = String.valueOf('test.txt');
			attachment4.ParentId = api01.Id;
  		insert attachment4;
  		
  		Attachment attachment5 = new Attachment();		
  			attachment5.Body = Blob.valueOf(base64Cad);
  			attachment5.Name = String.valueOf('test.txt');
			attachment5.ParentId = api01.Id;
  		insert attachment5;
		
        return evalDealer;		
	}



@isTest
	static void itShould(){
		Evaluaciones_Dealer__c evalDealer = AttachmentKodawariControllerTest.getTestDealer();
		Test.startTest();			
			ApexPages.StandardController stdController = new ApexPages.StandardController(evalDealer);
			AttachmentKodawariController evRetencion = new AttachmentKodawariController(stdController);
			evRetencion.idsMapAtt=null;
            AttachmentKodawariController.eliminarArchivo();
										
		Test.stopTest();
	}

}