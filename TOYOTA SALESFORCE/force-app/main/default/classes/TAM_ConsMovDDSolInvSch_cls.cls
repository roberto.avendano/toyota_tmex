/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_MovimientosSolicitudes__c
                        y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ConsMovDDSolInvSch_cls implements Schedulable{

    global String sQuery {get;set;}
    global Date dtFechaIniConsPrm {get;set;}
    global Date dtFechaFinConsPrm {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ConsMovDDSolInvSch_cls.execute...');

        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        String sFleet = 'N';

        String sDateFechaConsMovDD = System.Label.TAM_FechaMesToyota;        
        Date dFechaConsulta = sDateFechaConsMovDD != 'null' ? Date.valueOf(sDateFechaConsMovDD) : Date.today();
        Date dtFechaRealCons = dFechaConsulta.addDays(-5); //Date.newInstance(2021,07,01); 
        dtFechaIniConsPrm = dtFechaRealCons;
        System.debug('EN TAM_ConsMovDDSolInvSch_cls.execute dtFechaRealCons: ' + dtFechaRealCons + ' dtFechaIniConsPrm: ' + dtFechaIniConsPrm);
        
        dtFechaIniConsPrm = Date.today();
        dtFechaFinConsPrm = Date.today();
        
        //Consulta las fechas en el calendario de toyota
        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
            From TAM_CalendarioToyota__c t Where TAM_FechaInicio__c <=:dFechaConsulta
            And TAM_FechaFin__c >=:dFechaConsulta]){
            //Coincide la fecha del calendario Toyota
            dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
            dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
            System.debug('EN TAM_ActCancelSolInvSch_cls.execute dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
        }//Fin del for para TAM_CalendarioToyota__c

        //Inicializa la fecha de dtFechaIniConsPrm con dtFechaRealCons
        if (sDateFechaConsMovDD == 'null')
            dtFechaIniConsPrm = dtFechaRealCons;
        if (sDateFechaConsMovDD != 'null')
            dtFechaRealCons = dtFechaIniConsPrm;
        System.debug('EN TAM_ActCancelSolInvSch_cls.execute dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);

        //Una prueba
        if (Test.isRunningTest()){
           dtFechaIniConsPrm = Date.today();
           dtFechaFinConsPrm = Date.today();
           dtFechaRealCons = Date.today();
           System.debug('EN TAM_ActCancelSolInvSch_cls.execute dtFechaRealCons: ' + dtFechaRealCons + ' dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
        }//Fin si Test.isRunningTest()

        //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
        String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' And TAM_FechaEnvForm__c >= ' + String.valueOf(dtFechaRealCons) +
                ' And TAM_FechaEnvForm__c <= ' + String.valueOf(dtFechaFinConsPrm) +
                //' And Fleet__c = \'' + String.escapeSingleQuotes(sFleet) + '\'' +
                ' Order by Submitted_Date__c DESC';
        //Ees una prueba
        if (Test.isRunningTest())    
            queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' Order by Submitted_Date__c DESC LIMIT 2';
        System.debug('EN TAM_ConsMovDDSolInvBch_cls queryMovimiento: ' + queryMovimiento);
        
        //Si es una prueba
        if (Test.isRunningTest())
            queryMovimiento += ' Limit 1';
        System.debug('EN TAM_ConsMovDDSolInvSch_cls.execute sQuery: ' + queryMovimiento);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ConsMovDDSolInvBch_cls objConsMovDDSolInvBch = new TAM_ConsMovDDSolInvBch_cls(queryMovimiento, dtFechaIniConsPrm, dtFechaFinConsPrm);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objConsMovDDSolInvBch, 1);
                     
    }
    
}