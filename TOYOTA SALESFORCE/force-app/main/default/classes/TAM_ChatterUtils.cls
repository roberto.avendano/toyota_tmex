/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de las 
                        Interacciones_Lead__c que se crean o actualizan.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    27-Septiembre-2021   Héctor Figueroa             Modificación
******************************************************************************* */

public with sharing class TAM_ChatterUtils {

    /*// makes a simple chatter text post to the specified user from the running user 
    public static void DirectMessage(Id userId, String postText) { 
        System.debug('EN TAM_ChatterUtils.DirectMessage userId: ' + userId + ' postText: ' + postText);

		// Define the FeedItemInput object to pass to postFeedElement
		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		 
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
		textSegmentInput.text = 'Thanks for attending my presentation test run this morning. Send me any feedback.';
		 
		// The MessageBodyInput object holds the text in the post
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		messageBodyInput.messageSegments.add(textSegmentInput);
		feedItemInput.body = messageBodyInput;
		 
		// The FeedElementCapabilitiesInput object holds the capabilities of the feed item.
		// For this feed item, we define a direct message capability to hold the member(s) and the subject.
		 
		List<String> memberIds = new List<String>();
        memberIds.add(userId);
		//memberIds.add('005B00000016OUQ');
		//memberIds.add('005B0000001rIN6');
		 
		ConnectApi.DirectMessageCapabilityInput dmInput = new ConnectApi.DirectMessageCapabilityInput();
		dmInput.subject = 'Thank you!';
		dmInput.membersToAdd = memberIds;
		 
		ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
		feedElementCapabilitiesInput.directMessage = dmInput;
		 
		feedItemInput.capabilities = feedElementCapabilitiesInput;
		 
		// Post the feed item. 
		ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
        System.debug('EN TAM_ChatterUtils.DirectMessage feedElement: ' + feedElement);
    }*/

    // makes a simple chatter text post to the specified user from the running user 
    public static void CommentMentions(Id userId, String postText) { 
        System.debug('EN TAM_ChatterUtils.CommentMentions userId: ' + userId + ' postText: ' + postText);

		String communityId = null;
		String feedElementId = 'a356w00000014M0AAI';
		
		ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
		
		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		
		textSegmentInput.text = 'Does anyone in this group have an idea? ';
		messageBodyInput.messageSegments.add(textSegmentInput);
		
		mentionSegmentInput.id = '0F96w00000009ujCAA';
		messageBodyInput.messageSegments.add(mentionSegmentInput);
		
		commentInput.body = messageBodyInput;
		
		ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(communityId, feedElementId, commentInput, null);
        System.debug('EN TAM_ChatterUtils.CommentMentions commentRep: ' + commentRep);

    }


    // makes a simple chatter text post to the specified user from the running user 
    public static void CommentMentions2(Id userId, String postText) { 
        System.debug('EN TAM_ChatterUtils.CommentMentions userId: ' + userId + ' postText: ' + postText);

        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
    
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    
        textSegmentInput.text = 'Prueba de mensaje para un grupos...'; // 'Casenumber ' +objCase.CaseNumber+' with the status '+objCase.Status+' has breached SLA aggrement hours';
        messageBodyInput.messageSegments.add(textSegmentInput);

        // Mention a group.        
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        mentionSegmentInput.id = '0F96w00000009ujCAA'; //userId or Group;
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
    
        // Use a record ID for the subject ID.
        feedItemInput.subjectId = 'a356w00000014M0'; //objCase.Id;
    
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput, null);
    }

    /*// makes a simple chatter text post to the specified user from the running user 
    public static void DirectMessage2(Id userId, String postText) { 

		ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
		ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
		ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
		ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
		
		messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
		
		mentionSegmentInput.id = '005RR000000Dme9';
		messageBodyInput.messageSegments.add(mentionSegmentInput);
		
		textSegmentInput.text = 'Could you take a look?';
		messageBodyInput.messageSegments.add(textSegmentInput);
		
		feedItemInput.body = messageBodyInput;
		feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
		feedItemInput.subjectId = '0F9RR0000004CPw';
		
		ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }*/

  // makes a simple chatter text post to the specified user from the running user 
  public static void simpleTextPost(Id userId, String postText) { 
    System.debug('EN TAM_ChatterUtils.simpleTextPost userId: ' + userId + ' postText: ' + postText);
    
    ConnectApi.FeedType feedType = ConnectApi.FeedType.UserProfile;
    System.debug('EN TAM_ChatterUtils.simpleTextPost feedType: ' + feedType);

    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

    // add the text segment
    ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
    textSegment.text = postText;
    messageInput.messageSegments.add(textSegment);
    System.debug('EN TAM_ChatterUtils.simpleTextPost messageInput: ' + messageInput);

    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
    feedItemInput.body = messageInput;
    System.debug('EN TAM_ChatterUtils.simpleTextPost feedItemInput: ' + feedItemInput);

    // post it
    //ConnectApi.ChatterFeeds.postFeedItem(null, feedType, userId, feedItemInput, null);  
    ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), 'me', ConnectApi.FeedElementType.FeedItem, 'On vacation this week.');
    System.debug('EN TAM_ChatterUtils.simpleTextPost feedElement: ' + feedElement);

  }     


    


  // makes a chatter post with some text and a link
  public static void simpleLinkPost(Id userId, String postText, String url, String urlName) {    

    ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
    feedItemInput.body = new ConnectApi.MessageBodyInput();

    // add the text segment
    ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
    feedItemInput.body.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    textSegment.text = postText;
    feedItemInput.body.messageSegments.add(textSegment);

    /*// add the attachment
    ConnectApi.LinkAttachmentInput linkIn = new ConnectApi.LinkAttachmentInput();
    linkIn.urlName = urlName;
    linkIn.url = url;
    feedItemInput.attachment = linkIn;

    // post it!
    //ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.News, userId, feedItemInput, null);
    */
    
  }   
  
  // makes a simple chatter text post to the specified user from the running user 
  public static void mentionTextPost(Id userId, Id userToMentionId, String postText) { 

    ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
    messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

    // add some text before the mention
    ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
    textSegment.text = 'Hey ';
    messageInput.messageSegments.add(textSegment);

    // add the mention
    ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();
    mentionSegment.id = userToMentionId;
    messageInput.messageSegments.add(mentionSegment);

    // add the text that was passed
    textSegment = new ConnectApi.TextSegmentInput();
    textSegment.text = postText;
    messageInput.messageSegments.add(textSegment);

    ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
    input.body = messageInput;

    // post it
    //ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.UserProfile, userId, input, null);

  }     

  /*// pass the user's id or 'me' to get current running user's news 
  public static ConnectApi.FeedItemPage getNewsFeed(String userId) { 
    return ConnectApi.ChatterFeeds.getFeedItemsFromFeed(null, ConnectApi.FeedType.News, userId);
  } */ 
      
}