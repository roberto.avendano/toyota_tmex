public with sharing class CancelacionAutoDemoController {
    public ApexPages.StandardSetController setController{get;set;}
    public String solicitudID{get;set;}

    public CancelacionAutoDemoController(ApexPages.StandardController controller) {
		solicitudID='';
		setController = new ApexPages.StandardSetController(Database.getQueryLocator([
			SELECT Id, Name, VIN__r.Name, Modelo__c, DescripcionModelo__c, AnioModelo__c,
				   DescripcionColorExterior__c, Estatus__c
			FROM SolicitudAutoDemo__c WHERE Estatus__c =: Constantes.AUTO_DEMO_SOLICITUD_ALTA
		]));
		setController.setPageSize(10);
    }

	public List<SolicitudAutoDemo__c> getSolicitudes(){
		return (List<SolicitudAutoDemo__c>) setController.getRecords();
	}

	public PageReference anterior(){
		setController.previous();
		return null;
	}

	public PageReference siguiente(){
		setController.next();
		return null;
	}

	public PageReference solicitudEdit(){
		PageReference ret = new PageReference('/apex/CancelacionAutoDemoEdit?id='+solicitudID);
		ret.setRedirect(true);
		return ret;
	}
}