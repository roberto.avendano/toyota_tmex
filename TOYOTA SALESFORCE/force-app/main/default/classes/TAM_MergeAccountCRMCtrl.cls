/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladira para el componente TAM_MergeAccountCRM

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    16-Agostp-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public without sharing class TAM_MergeAccountCRMCtrl {

	public class wrpListaClientes{
	    @AuraEnabled 
	    public Account cliente {get;set;}    
	    @AuraEnabled 
	    public Boolean blnSeleccionado {get;set;}    
	    @AuraEnabled 
	    public Boolean blnBloqueado {get;set;}    
	    @AuraEnabled 
	    public Boolean blnSeleccionaCte {get;set;}    
	    @AuraEnabled 
	    public String strIdCliente {get;set;}    
	    @AuraEnabled 
	    public String intTotalVehiculos {get;set;}    
	    @AuraEnabled 
	    public String strTipoCliente {get;set;}    
	    @AuraEnabled 
	    public String strEstadoClienteListaNegra {get;set;}    
		
		//Un contructor por default
		public wrpListaClientes(){
			this.cliente = new Account();
			this.blnSeleccionado = false;	
			this.blnBloqueado = false;
			this.blnSeleccionaCte = false;
			this.strIdCliente = '';
			this.intTotalVehiculos = '0';
			this.strTipoCliente = '';
			this.strEstadoClienteListaNegra = '';
		}
		
		//Un contructor por default
		public wrpListaClientes(Account cliente, Boolean blnSeleccionado, Boolean blnBloqueado, String strIdCliente,
			Boolean blnSeleccionaCte, String intTotalVehiculos, String strTipoCliente, String strEstadoClienteListaNegra){
			this.cliente = cliente;
			this.blnSeleccionado = blnSeleccionado;	
			this.blnBloqueado = blnBloqueado;			
			this.strIdCliente = strIdCliente;
			this.blnSeleccionaCte = blnSeleccionaCte;
			this.intTotalVehiculos = intTotalVehiculos;
			this.strTipoCliente = strTipoCliente;
			this.strEstadoClienteListaNegra = strEstadoClienteListaNegra;			
		}
	}


    //Obtener getDatosCandidato.
    @AuraEnabled
    public static Boolean getPerfilUserActual() {
    	System.debug('En getPerfilUserActual...');
		Boolean bEsUserMerge = false;
		Boolean bEsUserAdmin = false;
		String sIdUser = UserInfo.getUserId();
		
		//Busca el perfil del usuario
		for (Profile usuario : [Select p.Name From Profile p
				where Name LIKE '%Administrador del sistema%'
				and id =:UserInfo.getProfileId()
				order by p.Name ]){
			bEsUserAdmin = true;
		}
		
		//Consulta los datos del Grupo y ve si el usuario tiene permisos para ejecutar ese Merge
		for (GroupMember grupoMergeClientes : [Select g.UserOrGroupId, g.GroupId, g.Group.Name From GroupMember g
				Where g.Group.Name = 'Merge Clientes']){
			//Va si el usuario: sIdUser corresponde con alguno de grupoMergeClientes.UserOrGroupId
			if (grupoMergeClientes.UserOrGroupId == sIdUser){
				bEsUserMerge = true;
				break;
			}//Fin si grupoMergeClientes.UserOrGroupId == sIdUser
		}
		if (!bEsUserMerge && bEsUserAdmin)
			bEsUserMerge = true;
    	System.debug('En getPerfilUserActual bEsUserMerge: ' + bEsUserMerge);

		return bEsUserMerge;
    }

    //Obtener getDatosCandidato.
    @AuraEnabled
    public static String getNombreClienteActual(String IdClienteActual) {
    	System.debug('En getNombreClienteActual IdClienteActual: ' + IdClienteActual);
		String strCadenaDeBusqueda = '';
		
		//Busca el perfil del usuario
		for (Account cliente : [Select id, Name, DenominacionSocial__c From Account p
				where id =: IdClienteActual ]){
			String strCadenaDeBusquedaPaso = cliente.DenominacionSocial__c != null ? cliente.DenominacionSocial__c: cliente.Name;
			strCadenaDeBusqueda = strCadenaDeBusquedaPaso.toUpperCase();
		}
    	System.debug('En getNombreClienteActual strCadenaDeBusqueda: ' + strCadenaDeBusqueda);
		return strCadenaDeBusqueda;
    }
    
    //Obtener getDatosCandidato.
    @AuraEnabled
    public static List<wrpListaClientes> getClientesCorporativos(String IdRegistroPrincipal, String strCadenaDeBusqueda) {
    	System.debug('EN getClientesCorporativos IdRegistroPrincipal: ' + IdRegistroPrincipal + ' strCadenaDeBusqueda: ' + strCadenaDeBusqueda);
    	List<wrpListaClientes> lClientes = new List<wrpListaClientes>();
		Set<String> setIdCte = new Set<String>();
		String strCadenaDeBusquedaPaso = '%' + strCadenaDeBusqueda.toUpperCase() + '%';
		Set<String> setIdcteId = new Set<String>();
		Map<String, Integer> mapIdCteTotOpp = new Map<String, Integer>();
		Map<String, Account> mapIdCteObjCte = new Map<String, Account>();
        String VaRtCteCorporativo = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();
			
		//Consulta el resto de la informacion relacionada a las cuentas
		for (Account cliente : [Select id, TAM_IdExternoNombre__c, Name, RFC__c, BillingStreet, Colonia__c, BillingCity, 
			BillingState, BillingPostalCode, Phone, Telefono_Oficina__c, Extension_Oficina__c, Correo_Electronico__c , 
			RecordTypeId, RecordType.Name, DenominacionSocial__c, IN_Estatus__c, 
			(Select Id, TAM_Distribuidor__c, TAM_IdExterno__c, TAM_Vin__c From Opportunities limit 10)
			From Account where (TAM_IdExternoNombre__c LIKE :strCadenaDeBusquedaPaso 
			OR DenominacionSocial__c LIKE :strCadenaDeBusquedaPaso) 
			And TAM_Merge__c = false ORDER BY Name LIMIT 50]){
	    	System.debug('EN getClientesCorporativos TAM_IdExternoNombre__c: ' + cliente.TAM_IdExternoNombre__c + ' DenominacionSocial__c: ' + cliente.DenominacionSocial__c + ' IN_Estatus__c: ' + cliente.IN_Estatus__c);
			//Ve si tiene OPP para que la tomes
			if (!cliente.Opportunities.isEmpty() || cliente.IN_Estatus__c == 'Boletinado'){
				setIdcteId.add(cliente.id);
				mapIdCteObjCte.put(cliente.id, cliente);
				//System.debug(LoggingLevel.INFO,'EN getClientesCorporativos objWrpListaClientesPaso: ' + objWrpListaClientesPaso);	
			}//Fin si !cliente.Opportunities.isEmpty()
            if (cliente.Opportunities.isEmpty() || cliente.RecordTypeId == VaRtCteCorporativo){
                setIdcteId.add(cliente.id);
                mapIdCteObjCte.put(cliente.id, cliente);                
            }//Fin si cliente.Opportunities.isEmpty() || cliente.RecordTypeId == VaRtCteCorporativo

		}//Fin del for para Account
	
		//Consulta todos los reg asociados a setIdcteId en Opp
		for (AggregateResult aggr : [ Select AccountId, Count(id) totClientes 
            FROM Opportunity 
            Where AccountId  IN :setIdcteId 
            Group By AccountId]){
            //Mete a un mapa el total
            String sIdCliente = (String) aggr.get('AccountId');
            Decimal dCntIntTot = (Decimal) aggr.get('totClientes'); //(decimal)agg.get('amts'); 
			mapIdCteTotOpp.put(sIdCliente, Integer.valueOf(dCntIntTot));            
	    	System.debug('EN getClientesCorporativos sIdCliente: ' + sIdCliente + ' dCntIntTot: ' + dCntIntTot);
        }
    	System.debug('EN getClientesCorporativos mapIdCteTotOpp: ' + mapIdCteTotOpp);
		
		//Recorre la lista de mapIdCteObjCte 
		for (String sCliente : mapIdCteObjCte.KeySet()){
			Account cliente = mapIdCteObjCte.get(sCliente);
			String sTpioClientePaso = cliente.RecordType.Name;
			String sTpioClienteFinal = sTpioClientePaso.replace('Cliente - ', '').replace('Cliente ', '');
			System.debug(LoggingLevel.INFO,'EN getClientesCorporativos mapIdCteTotOpp.get(sCliente): ' + String.valueOf(mapIdCteTotOpp.get(sCliente)));
			wrpListaClientes objWrpListaClientesPaso = new wrpListaClientes(cliente, false, false, cliente.id, false, 
				String.valueOf(mapIdCteTotOpp.get(sCliente)), sTpioClienteFinal, cliente.IN_Estatus__c); 
			lClientes.add(objWrpListaClientesPaso);			
			System.debug(LoggingLevel.INFO,'EN getClientesCorporativos objWrpListaClientesPaso: ' + objWrpListaClientesPaso);
		}
		
		//Rekresa la lista de clientes lClientes
		return lClientes;
    }
    
    //Obtener getDatosCandidato.
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion mergeClientes(String IdRegistroPrincipal, String strClienteSeleccionadoMerge, 
    	List<wrpListaClientes> lClientesDealer, String UsarDartosCteSol) {
    	System.debug('EN TAM_MergeAccountCRMCtrl IdRegistroPrincipal: ' + IdRegistroPrincipal + ' strClienteSeleccionadoMerge: ' + strClienteSeleccionadoMerge + ' UsarDartosCteSol: ' + UsarDartosCteSol);
    	System.debug('EN TAM_MergeAccountCRMCtrl lClientesDealer: ' + lClientesDealer);
			
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se estan actualizado y deben quedar listos en 10 minutos.');    	
		String sLClientes = '';
		List<String> lClientes = new List<String>();
		//Recorre la lista de reg selecionados para hacer merge
		for (wrpListaClientes objWrpCliente : lClientesDealer){
			if (objWrpCliente.blnSeleccionado){
				lClientes.add(objWrpCliente.strIdCliente);
			}
		}//Fin del for para lClientesDealer
    	System.debug('EN TAM_MergeAccountCRMCtrl sLClientes: ' + sLClientes);
		
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraFinal = dtHoraActual.addMinutes(1);
		String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
    	System.debug('EN TAM_MergeAccountCRMCtrl CRON_EXP: ' + CRON_EXP);
		
		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeOppSch_cls
		TAM_MergeOppSch_cls objTAMMergeOppSch = new TAM_MergeOppSch_cls();
		objTAMMergeOppSch.lClientes = lClientes;
		objTAMMergeOppSch.strClienteFinal = strClienteSeleccionadoMerge;
    	System.debug('EN TAM_MergeAccountCRMCtrl objTAMMergeOppSch: ' + objTAMMergeOppSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest()) //TAM_MergeOppSch
			System.schedule('TAM_MergeOppSch' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objTAMMergeOppSch);
		
		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeContactosSch_cls
		TAM_MergeContactosSch_cls objTAMMergeContactsSch = new TAM_MergeContactosSch_cls();
		objTAMMergeContactsSch.lClientes = lClientes;
		objTAMMergeContactsSch.strClienteFinal = strClienteSeleccionadoMerge;
    	System.debug('EN TAM_MergeAccountCRMCtrl objTAMMergeContactsSch: ' + objTAMMergeContactsSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest()) //MergContacts
			System.schedule('TAM_MergeContactosSch' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objTAMMergeContactsSch);
		
		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeContactosSch_cls
		TAM_MergeVehiculosSch_cls objTAMMergeVehiculosSch = new TAM_MergeVehiculosSch_cls();
		objTAMMergeVehiculosSch.lClientes = lClientes;
		objTAMMergeVehiculosSch.strClienteFinal = strClienteSeleccionadoMerge;
    	System.debug('EN TAM_MergeAccountCRMCtrl objTAMMergeVehiculosSch: ' + objTAMMergeVehiculosSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest()) //MergVehiculos
			System.schedule('TAM_MergeVehiculosSch' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objTAMMergeVehiculosSch);
		
		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergePropietarioSch_cls
		TAM_MergePropietarioSch_cls objTAMMergePropietariosSch = new TAM_MergePropietarioSch_cls();
		objTAMMergePropietariosSch.lClientes = lClientes;
		objTAMMergePropietariosSch.strClienteFinal = strClienteSeleccionadoMerge;
    	System.debug('EN TAM_MergeAccountCRMCtrl objTAMMergePropietariosSch: ' + objTAMMergePropietariosSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest()) //TAM_MergePropietarioSch
			System.schedule('TAM_MergePropietarioSch' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objTAMMergePropietariosSch);

		//Finalmente actualiza los datos de los reg relacionados a las solicitudes
		if (UsarDartosCteSol == 'true')
			if (!Test.isRunningTest())
				actualizaDatosClienteFinal(IdRegistroPrincipal, strClienteSeleccionadoMerge);
		
		//Finalmente actualiza los datos de los reg relacionados a las solicitudes
		actualizaSolFlotProgCliente(lClientes, strClienteSeleccionadoMerge, IdRegistroPrincipal);
		
		//Regresa el objeto con la respuesta
		return objRespoPaso;
    }

	public static void actualizaDatosClienteFinal(String IdRegistroPrincipal, String strClienteSeleccionadoMerge){
    	System.debug('EN TAM_MergeAccountCRMCtrl actualizaDatosClienteFinal strClienteSeleccionadoMerge: ' + strClienteSeleccionadoMerge + ' IdRegistroPrincipal: ' + IdRegistroPrincipal);
		
		//Crea el objeto para TAM_SolicitudesParaAprobar__c
		TAM_SolicitudesParaAprobar__c objSolUpd = new TAM_SolicitudesParaAprobar__c(id = IdRegistroPrincipal,
			TAM_ClienteCorporativoRef__c = strClienteSeleccionadoMerge);

		//Un obheto del tipo Savepoint para hacer rollback
		Savepoint sp = Database.setSavepoint();        
		
		//Actualiza la solicitud
		if (!Test.isRunningTest())
			update objSolUpd;

		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN actualizaDatosClienteFinal bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN actualizaDatosClienteFinal bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
	}
			
	public static void actualizaSolFlotProgCliente(List<String> lClientes, String strClienteSeleccionadoMerge,
		String IdRegistroPrincipal){
    	System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente lClientes: ' + lClientes);
    	System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente strClienteSeleccionadoMerge: ' + strClienteSeleccionadoMerge + ' IdRegistroPrincipal: ' + IdRegistroPrincipal);

		List<TAM_SolicitudesFlotillaPrograma__c> lSolFlotProgUps = new List<TAM_SolicitudesFlotillaPrograma__c>();
		List<TAM_SolicitudesFlotillaPrograma__Share> lSolicitudFlotillaPrograma  = new List<TAM_SolicitudesFlotillaPrograma__Share>();
		List<AccountShare> lAccountShare = new List<AccountShare>();	
		List<Account> lAccountMerge = new List<Account>();	
		List<AccountShare> lDelAccountShare = new List<AccountShare>();	
		List<TAM_MergeShareClientes__c> lMergeShareClientesShare = new List<TAM_MergeShareClientes__c>();	
		
		Set<String> setdOwnerSol = new Set<String>();
		Map<String, String> mapOwnerNoDist = new Map<String, String>();
		Map<String, String> mapNoDistNom = new Map<String, String>();
		Map<String, String> mapIdGrupoNombre = new Map<String, String>();
		
		String sQuery = '';
		String sLClientes = '(';
		
		//Recorre la lista de lClientes
		for (String sIdCliente : lClientes){
			sLClientes += '\'' + sIdCliente + '\',';
		}
		sLClientes = sLClientes.substring(0, sLClientes.lastIndexOf(','));
		sLClientes += ')';
		System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente sLClientes: ' + sLClientes);
		
		sQuery = 'Select Id, TAM_Cliente__c, OwnerId, TAM_NomDistribuidorPropietario__c, TAM_NombreCompletoDistribuidor__c ';
		sQuery += ' From TAM_SolicitudesFlotillaPrograma__c ';
		if (!Test.isRunningTest())
			sQuery += ' Where TAM_Cliente__c IN ' + sLClientes;
		sQuery += ' Order by TAM_Cliente__c ';
		if (Test.isRunningTest())
			sQuery += ' Limit 1 ';		
		System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente sQuery: ' + sQuery);

		//Ejecuta la consulta de 200 en 200 reg
		Database.QueryLocator query = Database.getQueryLocator(sQuery);
		Database.QueryLocatorIterator itQuery =  query.iterator(); 
		// Iterate over the records
		while (itQuery.hasNext()){
    		TAM_SolicitudesFlotillaPrograma__c solFlotProgObject = (TAM_SolicitudesFlotillaPrograma__c)itQuery.next();
        	TAM_SolicitudesFlotillaPrograma__c solFlotProgPaso = new TAM_SolicitudesFlotillaPrograma__c(id = solFlotProgObject.id, 
        	TAM_Cliente__c = strClienteSeleccionadoMerge, TAM_MergeCuentaAnterior__c = solFlotProgObject.TAM_Cliente__c,
        	TAM_NomDistribuidorPropietario__c = solFlotProgObject.TAM_NomDistribuidorPropietario__c, 
        	TAM_NombreCompletoDistribuidor__c = solFlotProgObject.TAM_NombreCompletoDistribuidor__c); 
        	lSolFlotProgUps.add(solFlotProgPaso);
			mapNoDistNom.put(solFlotProgObject.TAM_NombreCompletoDistribuidor__c, solFlotProgObject.TAM_NomDistribuidorPropietario__c);
        	System.debug('EN TAM_MergeAccountCRMCtrl solFlotProgObject: ' + solFlotProgObject);
		}
       	System.debug('EN TAM_MergeAccountCRMCtrl mapNoDistNom.KeySet: ' + mapNoDistNom.KeySet());
    	System.debug('EN TAM_MergeAccountCRMCtrl mapNoDistNom.Values: ' + mapNoDistNom.Values());
		
    	//Consulta los grupos asociados a setNomDist
    	if (!mapNoDistNom.isEmpty()){
	    	for (Group grupoPublico : [Select g.Name, g.Id From Group g
				where Name =: mapNoDistNom.Values()]){
	    		mapIdGrupoNombre.put(grupoPublico.Name, grupoPublico.id);
	    	}
    	}
    	System.debug('EN TAM_MergeAccountCRMCtrl mapIdGrupoNombre: ' + mapIdGrupoNombre);

		//Un obheto del tipo Savepoint para hacer rollback
		Savepoint sp = Database.setSavepoint();        
		try{
			
	        //Ve si tiene algo la lista de mapInvDummyPasoUpd
	        if (!lSolFlotProgUps.isEmpty()){
	            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lSolFlotProgUps, TAM_SolicitudesFlotillaPrograma__c.id, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						System.debug('EN TAM_MergeAccountCRMCtrl Hubo un error a la hora de crear/Actualizar los registros en SolFlotProg ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
					if (objDtbUpsRes.isSuccess())
						System.debug('EN TAM_MergeAccountCRMCtrl Los datos de la SolFlotProg se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
				}//Fin del for para lDtbUpsRes
	        }//Fin si !mapInvDummyPasoUpd.isEmpty()

			//Ahora si crea el reg para la colaboración
			for (TAM_SolicitudesFlotillaPrograma__c sOwnerIdSolFlot : lSolFlotProgUps){
				//Agrega el objeto a la lista de lMergeShareClientesShare
				lMergeShareClientesShare.add(new TAM_MergeShareClientes__c(
						Name = strClienteSeleccionadoMerge + ' - ' + sOwnerIdSolFlot.TAM_NomDistribuidorPropietario__c,
						TAM_Cliente__c = strClienteSeleccionadoMerge,
						TAM_IdUsuarioShare__c =  mapIdGrupoNombre.get(sOwnerIdSolFlot.TAM_NomDistribuidorPropietario__c),
						TAM_Procesado__c = false,
						TAM_ActivarProceso__c = false,
						TAM_IdExterno__c = strClienteSeleccionadoMerge + ' - ' +  sOwnerIdSolFlot.TAM_NombreCompletoDistribuidor__c,
						TAM_TipoObjeto__c = 'Clientes'
					)
				);
			}//Fin del for lSolFlotProgUps
	    	System.debug('EN TAM_MergeAccountCRMCtrl lMergeShareClientesShare: ' + lMergeShareClientesShare);
			
	        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.UpsertResult> lDtbUpsRes = Database.upsert(lMergeShareClientesShare, TAM_MergeShareClientes__c.TAM_IdExterno__c,  false);
			//Ve si hubo error
			for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeAccountCRMCtrl Hubo un error a la hora de crear/Actualizar los registros en MergeShareClientes ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());				
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeAccountCRMCtrl Los datos del MergeShareClientes se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes

			//Finalmente prende la bvadera de TAM_Merge__c en los cluientes que se seleccionaron para el merge
			for (String sIdCliente : lClientes){
				lAccountMerge.add(new Account(id = sIdCliente, TAM_Merge__c = true, TAM_MergeCuentaPrincipal__c = strClienteSeleccionadoMerge));
	    		System.debug('EN TAM_MergeAccountCRMCtrl sIdCliente: ' + sIdCliente);						
			}
			//Actualiza las cuentas lAccountMerge
			update lAccountMerge;

	    	System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente lClientes: ' + lClientes);			
			//Busca los registros de AccountShare para los lClientes
			for (AccountShare objAccountShare : [Select id, AccountId From AccountShare Where AccountId IN : lClientes
				and RowCause IN ('Manual')]){
				lDelAccountShare.add(objAccountShare);
			}
	    	System.debug('EN TAM_MergeAccountCRMCtrl actualizaSolFlotProgCliente lDelAccountShare: ' + lDelAccountShare);
			
			//Quita la colaboración de lDelAccountShare
			/*if (!lDelAccountShare.isEmpty())
	        	if (!Test.isRunningTest())		
					delete lDelAccountShare;*/

			//Ve si la etiqueta de TAM_HabilitaMergeAccounts
			String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
			Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
    		System.debug('EN TAM_MergeAccountCRMCtrl bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
    		if (!bHabilitaMergeAccounts){
	    		System.debug('EN TAM_MergeAccountCRMCtrl bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
	       		Database.rollback(sp);
    		}
    		
		}catch(Exception e){
			System.debug('En TAM_MergeAccountCRMCtrl error: ' + e.getMessage() + ' ' + e.getLineNumber());
       		Database.rollback(sp);
        }
				
		//*** NO SE TE OLVIDE QUITAR ESTE COMENTARIO ES PARA PRUREBAS ****///
        //Database.rollback(sp);
		
	}

}