public with sharing class AutoDemoTriggerMethods {
	
	public static Map<String, Vehiculo__c> getVinesInfoByName(Set<String> vinesName){
		Map<String, Vehiculo__c> retMap = new Map<String, Vehiculo__c>();		
		for(Vehiculo__c v: [SELECT Id, Name, EstatusVINAutoDemo__c, Producto__c, Producto__r.Serie__r.InteresSerieActiva__c,

                            (SELECT Id, Name, EstatusAutoDemo__c FROM Solicitudes_autos_demo2__r WHERE EstatusAutoDemo__c!='Cancelado') 
							FROM Vehiculo__c WHERE Name IN:vinesName]){
			if(!retMap.containsKey(v.Name)){
				retMap.put(v.Name, v);
			}
		}

		return retMap;
	}


	public static Map<Id, Vehiculo__c> getVinesInfoById(Set<Id> vinesID){
		return new Map<Id, Vehiculo__c>([SELECT Id, Name, EstatusVINAutoDemo__c, Distribuidor__c, Producto__c, Producto__r.Serie__r.InteresSerieActiva__c,
			(SELECT Id, Name, EstatusAutoDemo__c FROM Solicitudes_autos_demo2__r WHERE EstatusAutoDemo__c!='Cancelado') 
			FROM Vehiculo__c WHERE Id IN:vinesID]);
	}

	public static Map<Id, Account> getDealerInfoMap(Set<Id> dealersID){
		return new Map<Id, Account>([
			SELECT Id, Name, UnidadesAutosDemoAutorizadas__c, ActivarvalidacionesPresupuestosAD__c,
				(SELECT Id, Name, EstatusAutoDemo__c FROM SolicitudesAutosDemo__r WHERE EstatusAutoDemo__c='Pendiente' OR EstatusAutoDemo__c='Activo')
			FROM Account 
			WHERE Id IN:dealersID
		]);
	}
    
    public static Map<String, Account> getDealerCodeInfoMap(Set<String> dealersCode){
        Map<String, Account> accountInfo = new Map<String, Account>();
        for(Account a: [SELECT Id, Name,Codigo_Distribuidor__c FROM Account WHERE Codigo_Distribuidor__c IN: dealersCode]){
            if(!accountInfo.containsKey(a.Codigo_Distribuidor__c)){
                accountInfo.put(a.Codigo_Distribuidor__c, a);
            }
        }
        return accountInfo;
    }

    /*
    public static Map<String, List<AsignacionPresupuesto__c>> getAsignacionesMap(Set<String> dealersIDS){
    	Map<String, List<AsignacionPresupuesto__c>> retMap = new Map<String, List<AsignacionPresupuesto__c>>();

    	for(AsignacionPresupuesto__c ap: [SELECT Id, Name, Distribuidor__c, Estatus__c, InicioFY__c, FinFY__c,
					MontoAsignadoUtilizadoAbril__c, MontoAsignadoUtilizadoAgosto__c, MontoAsignadoUtilizadoDiciembre__c, MontoAsignadoUtilizadoEnero__c,
                    MontoAsignadoUtilizadoFebrero__c, MontoAsignadoUtilizadoJulio__c, MontoAsignadoUtilizadoJunio__c, MontoAsignadoUtilizadoMarzo__c, 
                    MontoAsignadoUtilizadoMayo__c, MontoAsignadoUtilizadoNoviembre__c, MontoAsignadoUtilizadoOctubre__c, MontoAsignadoUtilizadoSeptiembre__c, 
                    AsignadoAbril__c, AsignadoAgosto__c, AsignadoDiciembre__c, AsignadoEnero__c, AsignadoFebrero__c, AsignadoJulio__c, AsignadoJunio__c, 
                    AsignadoMarzo__c, AsignadoMayo__c, AsignadoNoviembre__c, AsignadoOctubre__c, AsignadoSeptiembre__c,
                    (SELECT Id, Name, Mes__c, Estatus__c, SolicitudAutoDemo__c, SolicitudAutoDemo__r.VIN__r.Modelo_b__r.Serie__c, 
                        	AsignacionPresupuesto__r.Presupuesto__r.PeriodoFiscal__c, IdAsignacinPresupuesto__c, IdPresupuesto__c 
            		FROM CasosAsignacionPresupuesto__r) 
				
				FROM AsignacionPresupuesto__c 
				WHERE Distribuidor__c IN:dealersIDS AND (Estatus__c='Vigente' OR Estatus__c='Asignado')]){
    		if(!retMap.containsKey(ap.Distribuidor__c)){
    			retMap.put(ap.Distribuidor__c, new List<AsignacionPresupuesto__c>());
    		}
    		
    		retMap.get(ap.Distribuidor__c).add(ap);
    	}

    	return retMap;
    }*/

    public static Map<Id, Account> getAsignacionesMap(Set<String> dealersIDS){
        return new Map<Id, Account>([SELECT Id, Name, (
            SELECT Id, Name, Distribuidor__c, Estatus__c, InicioFY__c, FinFY__c,
                    MontoAsignadoUtilizadoAbril__c, MontoAsignadoUtilizadoAgosto__c, MontoAsignadoUtilizadoDiciembre__c, MontoAsignadoUtilizadoEnero__c,
                    MontoAsignadoUtilizadoFebrero__c, MontoAsignadoUtilizadoJulio__c, MontoAsignadoUtilizadoJunio__c, MontoAsignadoUtilizadoMarzo__c, 
                    MontoAsignadoUtilizadoMayo__c, MontoAsignadoUtilizadoNoviembre__c, MontoAsignadoUtilizadoOctubre__c, MontoAsignadoUtilizadoSeptiembre__c, 
                    AsignadoAbril__c, AsignadoAgosto__c, AsignadoDiciembre__c, AsignadoEnero__c, AsignadoFebrero__c, AsignadoJulio__c, AsignadoJunio__c, 
                    AsignadoMarzo__c, AsignadoMayo__c, AsignadoNoviembre__c, AsignadoOctubre__c, AsignadoSeptiembre__c
                FROM AsignacionPresupuesto__r WHERE Estatus__c='Vigente' OR Estatus__c='Asignado'
            ) FROM Account WHERE Id IN: dealersIDS
        ]);
    }



    public static Map<String, List<Date>> cortesToCreate(Date startCase, Date endCase){
        //startCase = startCase== null? Date.today(): startCase;
        //endCase = CaseTriggerMethods.calculateEndCase(startCase, endCase);
        List<String> meses = new List<String>{'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'};                                 
        Map<String, List<Date>> mapDates= new Map<String, List<Date>>();
        
        for(Integer i=0; i<=startCase.monthsBetween(endCase);i++){
            Date currentMonth= startCase.addMonths(i);
            Integer endDay= Date.daysInMonth(currentMonth.year(), currentMonth.month()); 
            Date endDate= Date.newInstance(currentMonth.year(), currentMonth.month(), endDay);
            mapDates.put(meses.get(currentMonth.month()-1), new List<Date>{currentMonth.toStartOfMonth(), endDate});            
        }
        return mapDates;
    }

    public static boolean dentroDelRango(List<Date> cortes, Date startAP, Date endAP){       
        Date corteInicio = cortes.get(0);
        Date corteFin = cortes.get(1);
        boolean status = false;
        if((corteInicio >= startAP && corteFin > startAP) && (corteInicio < endAP && corteFin <= endAP)){
            status = true;
        }

        return status;
    }

    public static Map<Id, CostoSerie__c> loadCostosSerie(Set<String> costosIDS){
        return new Map<Id, CostoSerie__c>([SELECT Id, Name, Enero__c, Febrero__c, Marzo__c, Abril__c, Mayo__c, Junio__c, Julio__c,
            Agosto__c, Septiembre__c, Octubre__c, Noviembre__c, Diciembre__c FROM CostoSerie__c WHERE Id IN: costosIDS]);
    }

    public static Decimal getCostoPromedioMes(String mes, CostoSerie__c costo){
        Decimal retValue;
        
        if(mes=='Enero'){           
            retValue = AutoDemoTriggerMethods.validaNull(costo.Enero__c);

        } else if(mes=='Febrero'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Febrero__c);

        } else if(mes=='Marzo'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Marzo__c);
            
        } else if(mes=='Abril'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Abril__c);

        } else if(mes=='Mayo'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Mayo__c);

        } else if(mes=='Junio'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Junio__c);

        } else if(mes=='Julio'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Julio__c);

        } else if(mes=='Agosto'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Agosto__c);

        } else if(mes=='Septiembre'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Septiembre__c);                

        } else if(mes=='Octubre'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Octubre__c);

        } else if(mes=='Noviembre'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Noviembre__c);

        } else if(mes=='Diciembre'){
            retValue = AutoDemoTriggerMethods.validaNull(costo.Diciembre__c);            
        
        } else{
            retValue = AutoDemoTriggerMethods.validaNull(null);
        }

        return retValue;
    }

    public static Decimal validaNull(Decimal d){
      return d==null?0.0:d;
    }


    public static List<Decimal> getMontosMesAP(String mes, AsignacionPresupuesto__c ap){
        List<Decimal> retValue = new List<Decimal>();
        
        if(mes=='Enero'){           
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoEnero__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoEnero__c));

        } else if(mes=='Febrero'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoFebrero__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoFebrero__c));

        } else if(mes=='Marzo'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoMarzo__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoMarzo__c));
            
        } else if(mes=='Abril'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoAbril__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoAbril__c));

        } else if(mes=='Mayo'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoMayo__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoMayo__c));

        } else if(mes=='Junio'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoJunio__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoJunio__c));

        } else if(mes=='Julio'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoJulio__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoJulio__c));

        } else if(mes=='Agosto'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoAgosto__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoAgosto__c));

        } else if(mes=='Septiembre'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoSeptiembre__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoSeptiembre__c));                

        } else if(mes=='Octubre'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoOctubre__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoOctubre__c));

        } else if(mes=='Noviembre'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoNoviembre__c));
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoNoviembre__c));

        } else if(mes=='Diciembre'){
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoDiciembre__c)); 
            retValue.add(AutoDemoTriggerMethods.validaNull(ap.AsignadoDiciembre__c));            
        
        } else{
            retValue.add(0.0);
            retValue.add(0.0);
        }

        return retValue;
    }
}