/*******************************************************************************
Desarrollado por: Globant México
Autor: Roberto Carlos Avendaño Quintana
Proyecto: Assestment Center TMEX
*********************************************************************************/

public  without sharing class ControllerTimmerGralRH {  
    //Metodo utilizado en el componente para obtener la información referente a la evaluación del candidato , para utilizarlo en el cronometro
    @AuraEnabled
    public static Contact  getEvalInfo(Id UserId){
        //Se declaran estrucutras de datos para guardar la información del flujo
        Contact evalCandidato = new Contact();
        //SOQL para obtener el id del contacto relacionado al Usuario
        User[] usContact = [Select id,contactId from User where id =: UserId];  
        //Validación de consulta usContact es > a 0
        if(usContact.size() > 0){
             //Guardamos en idContact el id del Contacto 
             String idContact = String.valueOf(usContact[0].contactId).substring(0, 15);
             //Se realiza un SOQL al objeto Contact para retornar la información del contacto relacionado a una evaluación
             evalCandidato= [Select id,RH_FechaAssestment__c,RH_DuracionEvalucion__c,Inicio_Evaluacion__c,FechaActivacionEval__c,FechaEvaluacionRH__c from Contact WHERE id =: idContact limit 1];

        }
        //El metodo retorna un objeto de tipo Contacto: evalCandidato
        return evalCandidato;  
    }
    
   
    
	//Metodo desarrolado para cambiar el owner de las evaluaciónes al momento del que el candidato inicie su evaluación, los correos electronicos relacionados con su evaluación cambiaran de owner a el contacto en cuestion
    @AuraEnabled
    public static void changeOwnerEval(String UserId,DateTime FechaActivacionEval){
        //Se declaran estrucutras de datos para guardar la información del flujo
        User usrCtc = new User();
        id ContactUser;
        String idUserSFDC = UserId.subString(0,15); 
        List<RH_EvaluacionCandidato__c> evalRhUpd = new List<RH_EvaluacionCandidato__c>(); 
        //Validación si UserId es diferente de nulo
        if(UserId != null){
            //SOQL que obtiene id y contacto relacionado al Usuario que esta en el momento logueado en la instancia
             usrCtc = [Select id,contactId from User Where id =: idUserSFDC];
        }
        	//Se valida que usrCtc sea diferente de nulo
        if(usrCtc != null){
            //En la variable ContactUser se guarda el id del contacto relacionado a el usuario
            ContactUser  = usrCtc.contactId;
            
        }
        //Validamos ContactUser sea diferente de nulo
        if(ContactUser != null){
            //SOQL para obtener el id,Inicio_Evaluacion__c relacionado al id del contacto
            Contact ctcUpd = [Select id,Inicio_Evaluacion__c from Contact WHERE Id =: ContactUser];
            ctCupd.FechaActivacionEval__c = FechaActivacionEval;
            ctcUpd.Inicio_Evaluacion__c = true;
            try{
            	update ctcUpd;    
            }catch(Exception e){
                system.debug('Error al momento de actualizar el registro ctcUpd'+ e.getMessage());
                
            }
            
        }
        
        //SOQL para obtener obtener los email relacionados a la evaluación del candidato
        List<RH_EvaluacionCandidato__c> inboxEvaluacion = [Select ownerId,id from RH_EvaluacionCandidato__c WHERE  RH_evalFinalizada__c = false AND RH_candidatoEvaluacion__R.RH_Contacto__c =: ContactUser];
        //Validacion de lista inboxEvaluacion diferente a vacia
        if(!inboxEvaluacion.isEmpty()){
            //Por cada iteración se setea al owner del candidato para que pueda ver y responder sus correos relacionados a la evaluación
            for(RH_EvaluacionCandidato__c  inboxEval : inboxEvaluacion){
                RH_EvaluacionCandidato__c updOwner = new RH_EvaluacionCandidato__c();
                updOwner.id =  inboxEval.id;
                inboxEval.OwnerId = UserId;
                evalRhUpd.add(inboxEval);
            }
            
        }
        //Verificamos que la lista evalRhUpd sea diferente de vacia
        if(!evalRhUpd.isEmpty()){
            try{
              //Operación DML para modificar los correos del solicitante
              update inboxEvaluacion;  
            }catch(Exception e){
          		system.debug('Error al momento de actualizar la lista inboxEvaluacion'+ e.getMessage());
                
            }
        
        }
        
    }
    
    //Metodo utilizado para cambiar el owner de la evaluaciónes a usuario ADMIN despues de el tiempo de la evaluación finalizo.(Es necesario cambiar el owner a --> ADMIN para que el candidato no pueda ver sus correos)
    @AuraEnabled
        public static void changeOwnerEvalAdmin(String UserId){
        //****Definir cual es el usuario admin al que se le asignaran las evaluaciones ********
        String userAdminRH = System.Label.RHAssesmentCenterUserAdmin;
        id ContactUser;
        User usrCtc = new User();
        List<RH_EvaluacionCandidato__c> evalRhUpd = new List<RH_EvaluacionCandidato__c>(); 
        //Se obitne el id del usuario de Salesforce relacionado con la evaluación que finalizo
        String idUserSFDC = UserId.subString(0,15); 
        //Validacion que verifica que UserId sea diferente de nulo
        if(UserId != null){
            //SOQL que obtiene el contacto relacionado a el usuario
             usrCtc = [Select id,contactId from User Where id =: idUserSFDC];
        }
        //Validacion que verifica que usrCtc sea diferente de nulo
        if(usrCtc != null){
            //Guardamos en la variable el ContactUser el id del contacto relacionado con el usuario
            ContactUser  = usrCtc.contactId;
            //Se manda a ejecutar el metodo activarLicencia
            activarLicencia(ContactUser);
            //Se manda a ejecutar el metodo inhabilitarDocumentosGrupo
            inhabilitarDocumentosGrupo(idUserSFDC);	
        }
        //SOQL que consulta los correos relacionados con la evaluación del solicitante
        List<RH_EvaluacionCandidato__c> inboxEvaluacion = [Select ownerId,id from RH_EvaluacionCandidato__c WHERE  RH_evalFinalizada__c = true AND RH_candidatoEvaluacion__R.RH_Contacto__c =: ContactUser];
        //Se valida que la lista inboxEvaluacion sea diferente de vacia
        if(!inboxEvaluacion.isEmpty()){
            //Por cada iteración se cambia el owner por un owner admin a los correos relacionados con la evaluación del solicitante
            for(RH_EvaluacionCandidato__c  inboxEval : inboxEvaluacion){
                RH_EvaluacionCandidato__c updOwner = new RH_EvaluacionCandidato__c();
                updOwner.id =  inboxEval.id;
                inboxEval.OwnerId = userAdminRH;
                evalRhUpd.add(inboxEval);
            }
            
        }
        //Verificamos que la lista evalRhUpd sea diferente de vacia
        if(!evalRhUpd.isEmpty()){
            //Se ejecucta la operación DML
            update inboxEvaluacion;
            //El proceso manda a ejecutar el metodo changePasswUsr
            changePasswUsr(idUserSFDC);
            
        }
        
    } 
    
    //Metodo que funciona para activar la licencia utilizada , resetear los campos del contacto y ademas para marcar la evaluación como finalizada a nivel de contacto RH
	@auraEnabled
    public static void activarLicencia(id ContactoLicencia){
        //Definimos estructuras de información
        RH_Contacto__c rhContacto = new RH_Contacto__c();
        Contact contactLicencia = new Contact();
        //Validación que verifica que ContactoLicencia sea diferente de nulo
         if(ContactoLicencia != null){
            //Se obtiene el contacto relacionado a la evaluación finalizada , este contacto contiene información para el acceso del candidato
            contactLicencia = [Select id,RH_LicenciaDisponible__c From Contact Where id =: ContactoLicencia limit 1];
            //Se setean valores null , para reiniciar los valores del contacto y dejarlo activo y listo para la siguiente evaluación
            contactLicencia.RH_LicenciaDisponible__c = true;
            contactLicencia.RH_FechaAssestment__c = null;
            contactLicencia.RH_DuracionEvalucion__c = null;
            contactLicencia.Contrasena__c = null;
			contactLicencia.Inicio_Evaluacion__c = false;
            contactLicencia.FechaActivacionEval__c = null;
            contactLicencia.FechaEvaluacionRH__c = null;
            contactLicencia.CasoUsoRH__c = null;
            try{
                //Se realiza la operación DML de la lista contactLicencia
                Update contactLicencia;
            }catch(Exception e){
                system.debug('error general al momento de actualizar la lista contactLicencia'+e.getMessage());
                
            }
        }
        
        //Validacion que verifica que ContactoLicencia sea diferente de nulo
        if(ContactoLicencia != null){
            //SOQL para obtener el registro del RH_Contacto__c , relacionado a la evaluación
            rhContacto = [Select id,RH_evaluacionFinalizada__c  From RH_Contacto__c Where RH_Contacto__r.id =: ContactoLicencia AND RH_evaluacionFinalizada__c = false limit 1];
            if(rhContacto != null){ 
                //Se setean los valores nuevos para dar como finalizada la evaluación
                rhContacto.RH_evaluacionFinalizada__c = true;
                rhContacto.Estatus__c = 'Evaluación finalizada';
                update rhContacto;
                
            }
            
        }

    }
	
    //Metodo que funciona para inhabilitar a el usuario de los grupos publicos y no tener acceso a los docuementos despues de que termine la evaluación
    @future @auraEnabled 
    public static void inhabilitarDocumentosGrupo(id usuarioLicencia){
        List<GroupMember> memberDel = new List <GroupMember>(); 
        if(usuarioLicencia != null){
             memberDel = [Select Id from GroupMember where UserOrGroupID =: usuarioLicencia];
        }
        if(!memberDel.isEmpty()){
 	    	delete memberDel;       
        }
         
    }
    
    
 //Metodo APEX , que funciona para actualizar el password del usuario de Salesforce. (Se cambia el password por uno administrativo);
    @auraEnabled
    public static void changePasswUsr(String UserId){
        String idUserSFDC;
        if(UserId != null){
            idUserSFDC = UserId.subString(0,15); 
        }      
        String passwordAdmin = [Select id,PasswordAdmin__c From adminRHAssestment__c].PasswordAdmin__c;
        System.setPassword(idUserSFDC, passwordAdmin);

    }
        
     
}