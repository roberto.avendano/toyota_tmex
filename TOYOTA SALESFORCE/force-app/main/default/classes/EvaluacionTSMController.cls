public with sharing class EvaluacionTSMController {

	public Evaluaciones_Dealer__c evalDealer{get; set;}

    public Map<String,Respuestas_Preguntas_TSM__c> preguntas{get; set;}

    public Id userID {get;set;}
    //public Integer index = 1;
    //public Integer count {get;set;}
    //public Integer start {get;set;}
    public String selectedVal{get;set;}
    public Map<Id,Integer> indicePreguntas{get;set;}
    public Set<String> preguntaGuardada{get;set;}

    public List<SelectOption> comboOptions{get;set;}


    //public String obligatoria {get;set;}

    public boolean guardado {get;set;}

     // BEGIN Guardar Respuesta //
    //public String respuestaString {get;set;}
    public Boolean saveDone {get;set;}
    public String saveResult {get;set;}
    //  END  Guardar Respuesta //

    private List<Respuestas_Preguntas_TSM__c> preguntasList;
    
    public Map<String,Actividad_Plan_Integral__c> mapActInt {get;set;}
    
    public Map<String, String> mapSeccUser{get; set;}
    
    public Map<String,String> attAPI{get; set;}

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Si','Si')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }
    
    public List<SelectOption> getPrioridades(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Prioridad__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}

    public EvaluacionTSMController(ApexPages.StandardController stdController) {
        //stdController.addFields(new List<String>{'Id','Name','Nombre_Dealer__c','Consultor_TSM_Titular__c'});
        evalDealer = (Evaluaciones_Dealer__c)stdController.getRecord();
        mapSeccUser = new Map<String, String>();
        selectedVal = ApexPages.currentPage().getParameters().get('pid');
        if(evalDealer!=null && evalDealer.Id!=null){
            evalDealer = [SELECT 
            				Id,Name,Nombre_Dealer__c,Consultor_TSM_Titular__c, 
            				(select Id, 
		                        Usuario__r.Id, Usuario__r.Name, 
		                        Seccion_Toyota_Mexico__r.Id
		                    from Relaciones_Seccion_Consultor__r)
            			FROM Evaluaciones_Dealer__c WHERE Id=:evalDealer.Id];
            if(evalDealer.Relaciones_Seccion_Consultor__r!=null){
	            for(Relacion_Seccion_Consultor__c rsc : evalDealer.Relaciones_Seccion_Consultor__r){
	            	mapSeccUser.put(rsc.Seccion_Toyota_Mexico__r.Id,rsc.Usuario__r.Name);
	            }
        	}
        }
        userId = UserInfo.getUserId();
        //userId='005i0000001tQFXAA2';

        preguntaGuardada = new Set<String>();
        List<Relacion_Seccion_Consultor__c> listaSeccionesConsultor = new List<Relacion_Seccion_Consultor__c>();
        if(userId==evalDealer.Consultor_TSM_Titular__c || Constantes.EVALUACIONES_DEALER_PROFILES_ALL_ACCESS.contains(UserInfo.getProfileId())){
            listaSeccionesConsultor = [SELECT Seccion_Toyota_Mexico__c 
                    FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c=:evalDealer.Id];
        }else{
            listaSeccionesConsultor = [SELECT Seccion_Toyota_Mexico__c 
                    FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c=:evalDealer.Id and Usuario__c =:userId];
        }

        // Consulta Secciones que puede ver el usuario
        Set<Id> seccionesIds = new Set<Id>();
        for (Relacion_Seccion_Consultor__c rsc: listaSeccionesConsultor) {
            seccionesIds.add(rsc.Seccion_Toyota_Mexico__c);
        }
        System.debug(evalDealer.Id);
        System.debug(seccionesIds);
        if(seccionesIds.size()==0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No se encontraron secciones asignadas'));
        }
        // Busca preguntas y respuestas corespondientes a la evaluacion y al usuario
        preguntasList = [SELECT Id, Name, Evaluacion_Dealer__c, Total_de_Objetos_Sin_Contestar__c,
        			Pregunta_Relacionada__r.Fotografia_Obligatoria_del__c,
                    Pregunta_Relacionada__r.Id, 
                    Pregunta_Relacionada__r.No_de_Seccion__c,  
                    Pregunta_Relacionada__r.Seccion__c, 
                    Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,
                    Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,
                    Pregunta_Relacionada__r.Name, 
                    Pregunta_Relacionada__r.Titulo__c, 
                    Pregunta_Relacionada__r.Reactivo__c, 
                    Pregunta_Relacionada__r.Obligatoria__c,
                    Pregunta_Relacionada__r.DERAP__c,
                    Pregunta_Relacionada__r.DPOK__c,
                    Pregunta_Relacionada__r.NEWTSA21__c, 
                    Pregunta_Relacionada__r.Persona_Objetivo__c, 
                    Pregunta_Relacionada__r.Lugar_de_la_Evaluacion__c, 
                    Pregunta_Relacionada__r.Foto__c, 
                    Pregunta_Relacionada__r.Beneficios__c, 
                    Pregunta_Relacionada__r.Deterioro_y_KPI__c,
                    Pregunta_Relacionada__r.Orden_de_Aparicion__c,
                    (select Id, Name, Respuesta__c,Pregunta_Relacionada_del__c,Respuestas_Preguntas_TSM_del__c,
                    	Objeto_Evaluacion_Relacionado__c,
                        Objeto_Evaluacion_Relacionado__r.Name,
                        Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion__c,
                     	Objeto_Evaluacion_Relacionado__r.TAM_NotaObjetoEvaluacion__c,
                        Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion_CSV__c,
                        Objeto_Evaluacion_Relacionado__r.Metodos_de_Evaluacion__c,
                     	Objeto_Evaluacion_Relacionado__r.TAM_NotaMetodoEvaluacion__c,   
                     	Objeto_Evaluacion_Relacionado__r.Clave_Preg__c
                    from Respuestas_ObjetosTSM_del__r order by Objeto_Evaluacion_Relacionado__r.Orden__c),
                    (select Id from Attachments order by CreatedDate desc),
                    (select Id, Name, Condicion_Observada__c, Prioridad__c, Referencia__c, Actividad_Mejora__c, RespuestasObjetosTSM__c, Celula_Area__c 
                    from Actividades_Planes_Integrales__r)
                FROM Respuestas_Preguntas_TSM__c 
                WHERE Evaluacion_Dealer__c =:evalDealer.Id and Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c IN :seccionesIds
                ORDER BY Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name, Pregunta_Relacionada__r.Orden_de_Aparicion__c
            ];
        
        cargaMapActInt();
        
        valSelectPreguntas();
    }
    
    private void cargaMapActInt(){
        mapActInt = new Map<String,Actividad_Plan_Integral__c>();
        if(preguntasList.size()>0){
            for(Respuestas_Preguntas_TSM__c rp : preguntasList){
                if(rp.Actividades_Planes_Integrales__r!=null || rp.Actividades_Planes_Integrales__r.size()>0){
                    for(Actividad_Plan_Integral__c api : rp.Actividades_Planes_Integrales__r){
                    	if(api.RespuestasObjetosTSM__c!=null){
                       		mapActInt.put(api.RespuestasObjetosTSM__c,api);
                    	}
                    }
                }
                for(Respuestas_ObjetosTSM__c ro : rp.Respuestas_ObjetosTSM_del__r){
                    if(!mapActInt.containsKey(ro.Id)){
                        mapActInt.put(ro.Id,new Actividad_Plan_Integral__c(
                        	Respuestas_Preguntas_TSM__c = ro.Respuestas_Preguntas_TSM_del__c,
		                    RespuestasObjetosTSM__c = ro.Id,
		                    Cuenta__c = evalDealer.Nombre_Dealer__c,
		                    Celula_Area__c = '',
		                    Referencia__c = UserInfo.getProfileId()==Constantes.PROFILE_CONSULTOR_TSM ? Constantes.ACTIVIDAD_PLAN_INTEGRAL_REFERENCIA_TSM : null
                        ));
                    }
                }
            }
        }
        System.debug(mapActInt);
        attAPI = new Map<String, String>();
        System.debug(mapActInt.keySet());
        for(Actividad_Plan_Integral__c api : [SELECT Id, RespuestasObjetosTSM__c, (select Id from Attachments order by CreatedDate desc limit 1) FROM Actividad_Plan_Integral__c WHERE RespuestasObjetosTSM__c IN :mapActInt.keySet()]){
            if(api.Attachments!=null && api.Attachments.size()>0){
            	attAPI.put(api.RespuestasObjetosTSM__c, api.Attachments[0].Id);
            }else{
            	attAPI.put(api.RespuestasObjetosTSM__c,'-');
            }
        }
        for(String idRO : mapActInt.keySet()){
            if(!attAPI.containsKey(idRO)){
                attAPI.put(idRO,'-');
            }
        }
        for(String key : attAPI.keySet())
        	System.debug(key + '--' + attAPI.get(key));
    }
    
    public List<SelectOption> getAreas(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Celula_Area__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
    
    public List<SelectOption> getReferencia(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Referencia__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
	
    private Respuestas_Preguntas_TSM__c actualizaPregunta(String idPregunta){
    	return [SELECT Id, Name, Evaluacion_Dealer__c, Total_de_Objetos_Sin_Contestar__c,
        			Pregunta_Relacionada__r.Fotografia_Obligatoria_del__c,
                    Pregunta_Relacionada__r.Id, 
                    Pregunta_Relacionada__r.No_de_Seccion__c,  
                    Pregunta_Relacionada__r.Seccion__c, 
                    Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,
                    Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,
                    Pregunta_Relacionada__r.Name, 
                    Pregunta_Relacionada__r.Titulo__c, 
                    Pregunta_Relacionada__r.Reactivo__c, 
                    Pregunta_Relacionada__r.Obligatoria__c,
                    Pregunta_Relacionada__r.DERAP__c,
                    Pregunta_Relacionada__r.DPOK__c,
                    Pregunta_Relacionada__r.NEWTSA21__c,  
                    Pregunta_Relacionada__r.Persona_Objetivo__c, 
                    Pregunta_Relacionada__r.Lugar_de_la_Evaluacion__c, 
                    Pregunta_Relacionada__r.Foto__c, 
                    Pregunta_Relacionada__r.Beneficios__c, 
                    Pregunta_Relacionada__r.Deterioro_y_KPI__c,
                    Pregunta_Relacionada__r.Orden_de_Aparicion__c,
                    (select Id, Name, Respuesta__c,Pregunta_Relacionada_del__c,Respuestas_Preguntas_TSM_del__c,
                    	Objeto_Evaluacion_Relacionado__c,
                        Objeto_Evaluacion_Relacionado__r.Name,
                        Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion__c,
                     	Objeto_Evaluacion_Relacionado__r.TAM_NotaObjetoEvaluacion__c,
                        Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion_CSV__c,
                        Objeto_Evaluacion_Relacionado__r.Metodos_de_Evaluacion__c,
                     	Objeto_Evaluacion_Relacionado__r.TAM_NotaMetodoEvaluacion__c, 
                        Objeto_Evaluacion_Relacionado__r.Clave_Preg__c
                    from Respuestas_ObjetosTSM_del__r order by Objeto_Evaluacion_Relacionado__r.Orden__c),
                    (select Id from Attachments order by CreatedDate desc),
                    (select Id, Name, Condicion_Observada__c, Prioridad__c, Referencia__c, Actividad_Mejora__c, RespuestasObjetosTSM__c, Celula_Area__c 
                    from Actividades_Planes_Integrales__r)
                FROM Respuestas_Preguntas_TSM__c WHERE Id=:idPregunta];
    }

    private void valSelectPreguntas(){
        indicePreguntas = new Map<Id,Integer>();
        preguntas = new Map<String,Respuestas_Preguntas_TSM__c>();
        comboOptions = new List<SelectOption>();
        if(preguntasList.size()>0){
            // LLena lista de preguntas que puede ver el usuario
            Integer indicePreguntaVal = 0;
            System.debug(preguntaGuardada);
            System.debug(selectedVal);
            for(Respuestas_Preguntas_TSM__c rp : preguntasList){
                // Selecciona la primer pregunta a mostrar
                indicePreguntas.put(rp.Id,indicePreguntaVal);
                indicePreguntaVal++;
                if(selectedVal==null){
                    selectedVal = rp.Id;
                }
                String checkSave = '';
                if(rp.Total_de_Objetos_Sin_Contestar__c == 0){
                    preguntaGuardada.add(rp.Id);
                }
                if(preguntaGuardada.contains(rp.Id)){
                    checkSave = ' OK';
                }
                if(Constantes.PROFILE_CONSULTOR_TSM==UserInfo.getProfileId()){
                	for(Respuestas_ObjetosTSM__c item : rp.Respuestas_ObjetosTSM_del__r){
                		if(item.Respuesta__c==null || item.Respuesta__c==''){
                			item.Respuesta__c = 'Si';
                		}
                	}
                }
                String usuarioSecc = mapSeccUser.containsKey(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Id)?' ' + mapSeccUser.get(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Id):'';
                comboOptions.add(new SelectOption(rp.Id, rp.Pregunta_Relacionada__r.Name + ' ' + rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name + usuarioSecc + checkSave));
                preguntas.put(rp.Id,rp);
            }
        }
        System.debug(comboOptions);
    }
    
    public String getPerfilTSMId(){
    	return Constantes.PROFILE_CONSULTOR_TSM;
    }

    public Boolean getValidateAtt(){
        if(preguntaGuardada.contains(selectedVal)){
            return true;
        }else{
            return false;
        }
    }

    public void Next(){
        if((indicePreguntas.get(selectedVal)+1)==preguntas.size()){
            selectedVal = comboOptions.get(0).getValue();
        }else{
            selectedVal = comboOptions.get(indicePreguntas.get(selectedVal)+1).getValue();
        }
    }

    public void Previous(){
        if((indicePreguntas.get(selectedVal))==0){
            selectedVal = comboOptions.get(preguntas.size()-1).getValue();
        }else{
            selectedVal = comboOptions.get(indicePreguntas.get(selectedVal)-1).getValue();
        }
    }

    public void Buscar(){

    }

	private Boolean seccionTerminada(Respuestas_Preguntas_TSM__c rp){
		List<Respuestas_Preguntas_TSM__c> valST = [SELECT Id FROM Respuestas_Preguntas_TSM__c where Evaluacion_Dealer__c=:rp.Evaluacion_Dealer__c and Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c=:rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c and Total_de_Objetos_Sin_Contestar__c>0];
		if(valST==null || valST.size()==0){
			return true;
		}else{
			return false;
		}
	}

    public void Guardar(){
        
        System.debug('MAPA KEY'+mapActInt);
        try{
            guardado=true;
            preguntaGuardada.add(selectedVal);
            valSelectPreguntas();
            System.debug(selectedVal);
            System.debug(preguntas.get(selectedVal).Respuestas_ObjetosTSM_del__r);
            
            update preguntas.get(selectedVal).Respuestas_ObjetosTSM_del__r;
            preguntas.put(selectedVal,actualizaPregunta(selectedVal));
            
            List<Actividad_Plan_Integral__c> listAPA = new List<Actividad_Plan_Integral__c>();
            for(Respuestas_ObjetosTSM__c ro : preguntas.get(selectedVal).Respuestas_ObjetosTSM_del__r){
                Actividad_Plan_Integral__c apa = mapActInt.get(ro.Id);
                system.debug('Mapa values'+preguntas.get(selectedVal).Respuestas_ObjetosTSM_del__r);
                
                if(apa.Condicion_Observada__c==null || apa.Condicion_Observada__c== '' && ro.Respuesta__c == 'No'){
                    apa.Condicion_Observada__c = ro.Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion__c;
                    
                }
                
                if(apa.Id!=null || (apa.Condicion_Observada__c!=null && apa.Condicion_Observada__c!='') || (apa.Actividad_Mejora__c!=null && apa.Actividad_Mejora__c!='')){
                    System.debug(apa);
                    apa.Respuestas_Preguntas_TSM__c = preguntas.get(selectedVal).Id;
                    apa.RespuestasObjetosTSM__c = ro.Id;
                    apa.Cuenta__c = evalDealer.Nombre_Dealer__c;
                    if(UserInfo.getProfileId()==Constantes.PROFILE_CONSULTOR_TSM){
                        apa.Referencia__c = Constantes.ACTIVIDAD_PLAN_INTEGRAL_REFERENCIA_TSM;
                    }
                    listAPA.add(apa);
                }
            }
            System.debug(listAPA);
            if(listAPA.size()>0){
                upsert listAPA;
                for(Actividad_Plan_Integral__c apa : listAPA){
                    mapActInt.put(selectedVal,apa);
                }
            }
            saveDone = true;
            saveResult = 'Guardado correctamente. ';
            if(seccionTerminada(preguntas.get(selectedVal))){
            	saveResult += 'Se concluyó la sección <b>' + preguntas.get(selectedVal).Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name + '</b> '; 
            }
            this.Next();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, saveResult));
        }catch(System.DmlException ex) {
            saveDone = false;
            saveResult = 'Error al guardar: ';
            for(Integer index = 0; index < ex.getNumDml(); index++){
		      saveResult += ex.getDmlMessage(index);
		    }
            System.debug(ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, saveResult));
        }catch(Exception e) {
            System.debug(e.getMessage());
            saveDone = false;
            saveResult = 'Error al guardar: '+e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, saveResult));
        }
    }

}