public class TAM_DetallePoliticaJunctionHandler {
    
    //Variables Públicas
    public static Set<String> setIdExternoModelos {get;set;}
    public static Set<String> setCodigoModelo {get;set;}
    public static Map<String, CatalogoCentralizadoModelos__c> mapCatalogoCentralizado {get;set;}
    public static Map<String, InventarioPiso__c> mapInventarioPiso {get;set;}
    public static Map<String,PricebookEntry> mapPriceBookEntry {get;set;}
    public static Map<String,PricebookEntry> mapPriceBookEntry_Standard {get;set;}
    public static Map<String, TAM_DetallePolitica__c> mapIncentivoAnterior {get;set;}


    /***** INSERT *****/
    public static void getDetalle(List<TAM_PoliticasIncentivos__c> lstNewPoliticaIncentivos){

        for(TAM_PoliticasIncentivos__c objPolitica : lstNewPoliticaIncentivos){
            
            //Consulta de información relacionada a diferentes catálogos.
            getCatalogoCentralizado(objPolitica.TAM_Marca__c);
            getInventario(objPolitica.Id);
            getPrecios(objPolitica.Id);
            getStandardPrice();
            getDetalleAnterior (objPolitica.TAM_InicioVigencia__c, objPolitica.RecordTypeId);
            
            //Llenado de información para Detalle Politica Junction
            setDetalleJunction(objPolitica.Id);
        }
    }
    
    /***** UPDATE *****/
    public static void setDetallePoliticaIncentivos(List<TAM_PoliticasIncentivos__c> lstNewPoliticaIncentivos){
        
        for(TAM_PoliticasIncentivos__c objPolitica : lstNewPoliticaIncentivos){
            if(objPolitica.TAM_EstatusIncentivos__c == 'Vigente'){
                getDetalleJunction(objPolitica.Id);
            }
            
        }
    }
    
    /*GET DETALLE JUNCTION*/
    public static void getDetalleJunction(String recordId){
        List<TAM_DetallePoliticaJunction__c> lstDetalleJunction = [SELECT Name,
                                                                          TAM_AnioModelo__c,
                                                                          TAM_ProductoFinanciero__c,
                                                                          TAM_Descripcion__c,
                                                                          TAM_Serie__c,
                                                                          TAM_Clave__c,
                                                                          TAM_IdExterno__c,
                                                                          TAM_Version__c,
                                                                          TAM_IncentivoTMEX_Porcentaje__c,
                                                                          TAM_TMEX_Margen_Porcentaje__c,
                                                                          TAM_IncDealer_Porcentaje__c,
                                                                          TAM_IncTFS_Porcentaje__c,
                                                                          TAM_IncPropuesto_Porcentaje__c,
                                                                          TAM_TP_IncTMEX_Porcentaje__c,
                                                                          TAM_TP_IncDealer_Porcentaje__c,
                                                                          TAM_Inventario_Dealer__c,
                                                                          TAM_Dias_de_Inventario__c,
                                                                          TAM_Consecutivo__c,
                                                                          TAM_Venta_MY__c,
                                                                          TAM_TotalEstimado__c,
                                                                          TAM_TMEX_Margen_Pesos__c,
                                                                          TAM_MSRP__c,
                                                                          TAM_IncentivoTMEX_Cash__c,
                                                                          TAM_IncDealer_Cash__c,
                                                                          TAM_IncTFS_Cash__c	,
                                                                          TAM_IncPropuesto_Cash__c,
                                                                          TAM_TP_IncTMEX_Cash__c,
                                                                          TAM_TP_IncDealer_Cash__c,
                                                                          TAM_BonoLealtad__c,
                                                                          TAM_Lealtad__c,
                                                                          TAM_Duplicar__c,
                                                                          TAM_BalanceOut__c,
                                                                          TAM_Pago_TFS__c,
                                                                          TAM_Tripleplay__c,
                                                                          TAM_Rango__c,
                                                                          TAM_PoliticaIncentivos__c,
                                                                          TAM_BonoLealtadTMEX__c,
                                                                          TAM_BonoLealtadDealer__c,
                                                                          TAM_FechaInventario__c,
                                                                          TAM_Lanzamiento__c,
                                                                          TAM_MotivoIncentivo__c,
                                                                          TAM_Marca__c
                                                                  FROM	  TAM_DetallePoliticaJunction__c
                                                                  WHERE	  TAM_PoliticaIncentivos__c =:recordId
                                                                  AND	  (TAM_IncPropuesto_Cash__c != null OR TAM_IncPropuesto_Porcentaje__c != null)];
        List<TAM_DetallePolitica__c> lstDetallePolitica = new List<TAM_DetallePolitica__c>();
        for(TAM_DetallePoliticaJunction__c objDetalleJunction : lstDetalleJunction){
            TAM_DetallePolitica__c objDetallePolitica = new TAM_DetallePolitica__c();
            
            objDetallePolitica.Name = objDetalleJunction.TAM_AnioModelo__c + '-' + objDetalleJunction.TAM_Serie__c + '-' + objDetalleJunction.TAM_Clave__c + '-' + objDetalleJunction.TAM_Version__c;
            objDetallePolitica.TAM_AnioModelo__c = objDetalleJunction.TAM_AnioModelo__c;
            objDetallePolitica.TAM_ProductoFinanciero__c = objDetalleJunction.TAM_ProductoFinanciero__c;
            objDetallePolitica.TAM_Descripcion__c = objDetalleJunction.TAM_Descripcion__c;
            objDetallePolitica.TAM_Serie__c = objDetalleJunction.TAM_Serie__c;
            objDetallePolitica.TAM_Clave__c = objDetalleJunction.TAM_Clave__c;
            objDetallePolitica.TAM_IdExterno__c = objDetalleJunction.TAM_IdExterno__c;
            objDetallePolitica.TAM_Version__c = objDetalleJunction.TAM_Version__c;
            objDetallePolitica.TAM_IncentivoTMEX_Porcentaje__c = objDetalleJunction.TAM_IncentivoTMEX_Porcentaje__c;
            objDetallePolitica.TAM_TMEX_Margen_Porcentaje__c = objDetalleJunction.TAM_TMEX_Margen_Porcentaje__c;
            objDetallePolitica.TAM_IncDealer_Porcentaje__c = objDetalleJunction.TAM_IncDealer_Porcentaje__c;
            objDetallePolitica.TAM_IncTFS_Porcentaje__c = objDetalleJunction.TAM_IncPropuesto_Porcentaje__c;
            objDetallePolitica.TAM_IncPropuesto_Porcentaje__c = objDetalleJunction.TAM_IncPropuesto_Porcentaje__c;
            objDetallePolitica.TAM_TP_IncTMEX_Porcentaje__c = objDetalleJunction.TAM_TP_IncTMEX_Porcentaje__c;
            objDetallePolitica.TAM_TP_IncDealer_Porcentaje__c = objDetalleJunction.TAM_TP_IncDealer_Porcentaje__c;
            objDetallePolitica.TAM_Inventario_Dealer__c = objDetalleJunction.TAM_Inventario_Dealer__c;
            objDetallePolitica.TAM_Dias_de_Inventario__c = objDetalleJunction.TAM_Dias_de_Inventario__c;
            objDetallePolitica.TAM_Consecutivo__c = objDetalleJunction.TAM_Consecutivo__c;
            objDetallePolitica.TAM_Venta_MY__c = objDetalleJunction.TAM_Venta_MY__c;
            objDetallePolitica.TAM_TotalEstimado__c = objDetalleJunction.TAM_TotalEstimado__c;
            objDetallePolitica.TAM_TMEX_Margen_Pesos__c = objDetalleJunction.TAM_TMEX_Margen_Pesos__c;
            objDetallePolitica.TAM_TMEX_Margen_Porcentaje__c = objDetalleJunction.TAM_TMEX_Margen_Porcentaje__c;
            objDetallePolitica.TAM_MSRP__c = objDetalleJunction.TAM_MSRP__c;
            objDetallePolitica.TAM_IncentivoTMEX_Cash__c = objDetalleJunction.TAM_IncentivoTMEX_Cash__c;
            objDetallePolitica.TAM_IncDealer_Cash__c = objDetalleJunction.TAM_IncDealer_Cash__c;
            objDetallePolitica.TAM_IncTFS_Cash__c = objDetalleJunction.TAM_IncTFS_Cash__c;
            objDetallePolitica.TAM_IncPropuesto_Cash__c = objDetalleJunction.TAM_IncPropuesto_Cash__c;
            objDetallePolitica.TAM_TP_IncTMEX_Cash__c = objDetalleJunction.TAM_TP_IncTMEX_Cash__c;
            objDetallePolitica.TAM_TP_IncDealer_Cash__c = objDetalleJunction.TAM_TP_IncDealer_Cash__c;
            objDetallePolitica.TAM_BonoLealtad__c = objDetalleJunction.TAM_BonoLealtad__c;
            objDetallePolitica.TAM_Lealtad__c = objDetalleJunction.TAM_Lealtad__c;
            objDetallePolitica.TAM_Duplicar__c = objDetalleJunction.TAM_Duplicar__c;
            objDetallePolitica.TAM_BalanceOut__c = objDetalleJunction.TAM_BalanceOut__c;
            objDetallePolitica.TAM_Pago_TFS__c = objDetalleJunction.TAM_Pago_TFS__c;
            objDetallePolitica.TAM_Tripleplay__c = objDetalleJunction.TAM_Tripleplay__c;
            objDetallePolitica.TAM_Rango__c = objDetalleJunction.TAM_Rango__c;
            objDetallePolitica.TAM_PoliticaIncentivos__c = objDetalleJunction.TAM_PoliticaIncentivos__c;
            objDetallePolitica.TAM_BonoLealtadTMEX__c = objDetalleJunction.TAM_BonoLealtadTMEX__c;
            objDetallePolitica.TAM_BonoLealtadDealer__c = objDetalleJunction.TAM_BonoLealtadDealer__c;
            objDetallePolitica.TAM_FechaInventario__c = objDetalleJunction.TAM_FechaInventario__c;
            objDetallePolitica.TAM_Lanzamiento__c = objDetalleJunction.TAM_Lanzamiento__c;
            objDetallePolitica.TAM_MotivoIncentivo__c = objDetalleJunction.TAM_MotivoIncentivo__c;
            
            lstDetallePolitica.add(objDetallePolitica);
        }
        try {
            Database.upsert(lstDetallePolitica,TAM_DetallePolitica__c.TAM_IdExterno__c ,false);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        
    }
    
    /*SET DETALLE POLITICA DE INCENTIVOS JUNCTION*/
    public static void setDetalleJunction(String recordId){
        
        TAM_PoliticasIncentivos__c objPoliticaMarca = [SELECT TAM_Marca__c FROM TAM_PoliticasIncentivos__c WHERE id=: recordId];
        String strCodigoModelo = '';
        Double intMargenTMEX_Porcentaje=0;
        Integer intMargenTMEX_Pesos = 0;
        Integer intMSRP=0;
        Integer intDiasInventario;
        Integer intCantidadInventario;
        Date datFechaInventario;
        String incentivoAnterior;
        String strNombreListaPrecios;
        
        List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaJunction = new List<TAM_DetallePoliticaJunction__c>();
        for(String strIdModelos : setIdExternoModelos){

            strIdModelos= strIdModelos.toUpperCase();
            TAM_DetallePoliticaJunction__c objDetalleJunction = new TAM_DetallePoliticaJunction__c();
            
            //Validación de mapa de pricebookEntry
            strCodigoModelo = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) +  String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
            strCodigoModelo = strCodigoModelo.toUpperCase();
            if(mapPriceBookEntry.containsKey(strCodigoModelo)){
                
                //Margen de ganancia TMEX %
                if(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c != null){
                    intMargenTMEX_Porcentaje = Double.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c);
                }
                //Margen de Ganancia TMEX $
                if(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c != null){
                    intMargenTMEX_Pesos = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c);
                }
                //MSRP
                if(mapPriceBookEntry.get(strCodigoModelo).TAM_MSRP__c != null){
                    intMSRP = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MSRP__c);
                }
                //Nombre lista de precios
                strNombreListaPrecios = mapPriceBookEntry.get(strCodigoModelo).Pricebook2.Name;
                
            } else if(mapPriceBookEntry_Standard.containsKey(strCodigoModelo)){
                //Margen de ganancia TMEX %
                if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c != null){
                    intMargenTMEX_Porcentaje = Double.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c);
                }
                //Margen de ganancia TMEX $
                if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c != null){
                    intMargenTMEX_Pesos = Integer.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c);
                }
                //MSRP
                if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MSRP__c != null){
                    intMSRP = Integer.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MSRP__c);
                }
                //Nombre lista de precios
                strNombreListaPrecios = mapPriceBookEntry_Standard.get(strCodigoModelo).Pricebook2.Name;
            } else{
                intMSRP = 0;
                intMargenTMEX_Pesos = 0;
                intMargenTMEX_Porcentaje = 0;
                strNombreListaPrecios = null;
            }
            
            //Validación de mapa de inventarios
            if(mapInventarioPiso.containsKey(strIdModelos)){
                intCantidadInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).Cantidad__c);
                intDiasInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).DiasVenta__c);
                datFechaInventario = Date.valueOf(mapInventarioPiso.get(strIdModelos).FechaInventario__c);
            } else {
                intCantidadInventario = 0;
                intDiasInventario = 0;
                datFechaInventario = null;
            }

            //Validacion incentivo anterior
            if(mapIncentivoAnterior.containsKey(strIdModelos)){
                TAM_DetallePolitica__c objDetallePoliticaAnterior = mapIncentivoAnterior.get(strIdModelos);
                    if(objDetallePoliticaAnterior.TAM_IncPropuesto_Cash__c != null){
                        incentivoAnterior = '$' + objDetallePoliticaAnterior.TAM_IncPropuesto_Cash__c;
                    }
                    
                    if(objDetallePoliticaAnterior.TAM_IncPropuesto_Porcentaje__c != null){
                        incentivoAnterior = objDetallePoliticaAnterior.TAM_IncPropuesto_Porcentaje__c + '%';
                    }
                
            } else {
                incentivoAnterior = null;
            }

            
            //Llenado de lista.
            /** Nombre     */	objDetalleJunction.Name = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + '0';
            /** Politica   */	objDetalleJunction.TAM_PoliticaIncentivos__c = recordId;
            /** Año Modelo */   objDetalleJunction.TAM_AnioModelo__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
           	/** Modelo     */   objDetalleJunction.TAM_Clave__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c);
            /** Versión    */   objDetalleJunction.TAM_Version__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Version__c);
            /** Inventario */   objDetalleJunction.TAM_Inventario_Dealer__c = intCantidadInventario;
            /** Dias Venta */   objDetalleJunction.TAM_Dias_de_Inventario__c = intDiasInventario;
            /**	MSRP       */   objDetalleJunction.TAM_MSRP__c = intMSRP;
            /**	MargenTMEX $*/  objDetalleJunction.TAM_TMEX_Margen_Pesos__c = intMargenTMEX_Pesos;
            /**	MargenTMEX %*/  objDetalleJunction.TAM_TMEX_Margen_Porcentaje__c = intMargenTMEX_Porcentaje;
            /**	Serie      */   objDetalleJunction.TAM_Serie__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c);
            /**	Consecutivo*/   objDetalleJunction.TAM_Consecutivo__c = 0;
            /**	Id Externo */   objDetalleJunction.TAM_IdExterno__c = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + '0';
            /** Fecha Inve */   objDetalleJunction.TAM_FechaInventario__c = datFechaInventario;
            /**Inc. Anterior*/  objDetalleJunction.TAM_Incentivo_actual__c = String.valueOf(incentivoAnterior);
            /**ListaPrecios*/	objDetalleJunction.TAM_NomListaPreciosAux__c = strNombreListaPrecios;
            /**Marca */         objDetalleJunction.TAM_Marca__c = objPoliticaMarca.TAM_Marca__c;

            
            lstDetallePoliticaJunction.add(objDetalleJunction);
        }
        
        try {
            
            Database.insert(lstDetallePoliticaJunction);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
    }
    
    
    /*OBTENER CATÁLOGO CENTRALIZADO*/
    public static void getCatalogoCentralizado(String strMarca){
        String idExterno = '';
        String codigoModelo = '';
        setIdExternoModelos = new Set<String>();
        setCodigoModelo = new Set<String>();
        mapCatalogoCentralizado = new Map<String, CatalogoCentralizadoModelos__c>();
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT  Modelo__c,
                                                                                AnioModelo__c,
                                                                                Serie__c,
                                                                                Disponible__c,
                                                                                Version__c,
                                                                                Serie_AnioModelo__c
                                                                        FROM    CatalogoCentralizadoModelos__c
                                                                        WHERE   Marca__c =: strMarca];
        
        for(CatalogoCentralizadoModelos__c objCatalogo : lstCatalogoCentralizado){
            idExterno = objCatalogo.Serie__c + objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
            idExterno = idExterno.toUpperCase();
            setIdExternoModelos.add(idExterno);
            codigoModelo = objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
            setCodigoModelo.add(codigoModelo);
            mapCatalogoCentralizado.put(idExterno,objCatalogo);
        }        
    }
    
    /*OBTENER INVENTARIO*/
    public static void getInventario(String recordId){
        
        Date dateFechaInventario;
        Boolean boolCargaHistorica = false;
        String idExternoInventario ='';
        String idExternoInventario_CatalogoModelos ='';
        Decimal intCantidadTotal;
        Decimal intDiasInventarioAux;
        Integer intDiasInventarioTotal;
        mapInventarioPiso = new Map<String, InventarioPiso__c>();
        
        List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
        boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
        if(boolCargaHistorica){
            dateFechaInventario = CargaHistorica[0].TAM_InicioVigencia__c - 1;
        } else {
            dateFechaInventario= System.today() - 1;
        }
            
        List<InventarioPiso__c> lstInventarioPiso = [SELECT	 AnioModelo__c,
                                                             Modelo__c,
                                                             Serie__c,
                                                             Version__c,
                                                             Cantidad__c,
                                                             DiasVenta__c,
                                                             Serie_AnioModelo__c,
                                                             FechaInventario__c
                                                     FROM    InventarioPiso__c
                                                     WHERE   FechaInventario__c =:dateFechaInventario];
            
            
            if(!lstInventarioPiso.isEmpty()){
                for(InventarioPiso__c objInventario : lstInventarioPiso){
                    
                    //Llenado de Inventario
                    idExternoInventario = objInventario.Serie__c + objInventario.Modelo__c +  objInventario.AnioModelo__c;
                    idExternoInventario = idExternoInventario.toUpperCase();
                    
    
                    if(mapInventarioPiso.containsKey(idExternoInventario)){
                        intCantidadTotal =  mapInventarioPiso.get(idExternoInventario).Cantidad__c + objInventario.Cantidad__c;
                        objInventario.Cantidad__c = intCantidadTotal;
    
                        intDiasInventarioAux = mapInventarioPiso.get(idExternoInventario).DiasVenta__c + objInventario.DiasVenta__c;
                        intDiasInventarioTotal = Integer.valueOf(intDiasInventarioAux.round(System.RoundingMode.HALF_UP));
                        objInventario.DiasVenta__c = intDiasInventarioTotal;
                        
                        
                    }
                    mapInventarioPiso.put(idExternoInventario,objInventario);
                }
            }
    }
    
    /*OBTENER LISTA DE PRECIOS*/
    public static void getPrecios(String recordId){
        Date fechaActual;
        Boolean boolCargaHistorica = false;
        List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
        boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
        if(boolCargaHistorica){
            fechaActual = CargaHistorica[0].TAM_InicioVigencia__c;
        } else {
            fechaActual= System.today();
        }
        
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'Enero');
        mapMes.put(2, 'Febrero');
        mapMes.put(3, 'Marzo');
        mapMes.put(4, 'Abril');
        mapMes.put(5, 'Mayo');
        mapMes.put(6, 'Junio');
        mapMes.put(7, 'Julio');
        mapMes.put(8, 'Agosto');
        mapMes.put(9, 'Septiembre');
        mapMes.put(10, 'Octubre');
        mapMes.put(11, 'Noviembre');
        mapMes.put(12, 'Diciembre');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;

        //Mapa de catálogo de lista de precios.
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                            Product2.IdExternoProducto__c,
                                                            TAM_MargenesGananciaTMEX_Pesos__c,
                                                            TAM_MargenesGananciaTMEX_Porc__c,
                                                            TAM_MSRP__c,
                                                   			UnitPrice,
                                                   			Pricebook2.Name
                                                    FROM    PricebookEntry
                                                    WHERE   IsActive = true 
                                                    AND     Pricebook2.Name =:strNombreCatalogoPrecios
                                                   	AND		Product2.IdExternoProducto__c IN: setCodigoModelo
        ];
        String strCodigoProducto;
        Double dblPrecioSinIVA;
        
        mapPriceBookEntry = new Map<String,PricebookEntry>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.Product2.IdExternoProducto__c;
            mapPriceBookEntry.put(strCodigoProducto, objListaPrecio);
            
        }
    }

    /* OBTENER DETALLE POLITICA INCENTIVOS ANTERIOR */
    public static void getDetalleAnterior (Date date_InicioVigencia, String recordTypeIdPolitica){

       List<TAM_DetallePolitica__c> lstDetallePoliticaAnterior = [SELECT   Id,
                                            Name,
                                            TAM_IncPropuesto_Cash__c,
                                            TAM_IncPropuesto_Porcentaje__c,
                                            TAM_PoliticaIncentivos__r.TAM_FinVigencia__c,
                                            TAM_Serie__c,
                                            TAM_AnioModelo__c,
                                            TAM_Clave__c
                                    FROM    TAM_DetallePolitica__c
                                    WHERE   TAM_PoliticaIncentivos__r.RecordTypeId =: recordTypeIdPolitica
                                    AND     TAM_PoliticaIncentivos__r.TAM_FinVigencia__c <=: date_InicioVigencia
                                    ORDER BY TAM_PoliticaIncentivos__r.TAM_FinVigencia__c DESC];
                                    

        mapIncentivoAnterior = new Map<String, TAM_DetallePolitica__c>();
        if(!lstDetallePoliticaAnterior.isEmpty()){
            for(TAM_DetallePolitica__c objDetallePolitica : lstDetallePoliticaAnterior){
                
                String idExterno = objDetallePolitica.TAM_Serie__c + objDetallePolitica.TAM_Clave__c +  objDetallePolitica.TAM_AnioModelo__c;
                idExterno = idExterno.toUpperCase();

                if(!mapIncentivoAnterior.containsKey(idExterno)){
                    mapIncentivoAnterior.put(idExterno, objDetallePolitica);
                } 
            }
        }
        
    }

    /* OBTENER LISTA DE PRECIOS STANDARD */
    public static void getStandardPrice(){

        //Mapa de catálogo de lista de precios.
        List<String> lstCodigoModeloStandard = new List<String>();
        for(String strCodigoModelo : setCodigoModelo){
            String strLlave = strCodigoModelo + '-StandardPriceBook';
            lstCodigoModeloStandard.add(strLlave);
        }
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                            ProductCode,
                                                            PrecioSinIva__c,
                                                            TAM_MargenesGananciaTMEX_Pesos__c,
                                                            TAM_MargenesGananciaTMEX_Porc__c,
                                                            TAM_MSRP__c,
                                                   			UnitPrice,
                                                            IdExterno__c,
                                                   			Pricebook2.Name
                                                    FROM    PricebookEntry
                                                    WHERE   IsActive = true 
                                                    AND     Pricebook2.Name = 'Standard Price Book'
                                                    AND     IdExterno__c IN: lstCodigoModeloStandard];

        String strCodigoProducto;
        
        mapPriceBookEntry_Standard = new Map<String,PricebookEntry>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.IdExterno__c.substringBefore('-');
            strCodigoProducto = strCodigoProducto.substring(0,strCodigoProducto.length());
            mapPriceBookEntry_Standard.put(strCodigoProducto, objListaPrecio);
        }
    }
}