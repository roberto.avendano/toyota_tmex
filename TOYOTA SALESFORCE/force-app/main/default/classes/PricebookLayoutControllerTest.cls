@isTest
private class PricebookLayoutControllerTest {
	
	public static Pricebook2 getPricebook(){
		RecordType rtParte = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Parte' and IsActive=true];		
		Id standardPricebookId = Test.getStandardPricebookId();
		List<Product2> newProds = new List<Product2>();

		for(Integer i=0; i<70; i++){
			String productName = 'PROD'+i;
			String description = 'Test product'+i;

			newProds.add(new Product2(
				Name= productName,
				IdExternoProducto__c=productName,
				ProductCode = productName,
				Description= description, 
				PartesRobadas__c = true,
				RecordTypeId = rtParte.Id,
				IsActive = true
			));
		}

		insert newProds;

        Product2 prodAFK= new Product2(
            Name='AFK001',
            IdExternoProducto__c='AFK001',
            ProductCode='AFK001',
            Description= 'Test productAFK',            
          	PartesRobadas__c = true,
          	RecordTypeId = rtParte.Id,
          	IsActive = true
        );            
        insert prodAFK;

		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id=standardPricebookId,
			UnitPrice=0.0,
			Product2Id=prodAFK.Id,			
			IsActive=true,
			IdExterno__c='Test1'
		);
		insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IdExternoListaPrecios__c='CustomPricebook', 
            isActive=true
        );

        insert customPB;

     	return customPB;
	}



	@isTest static void test_method() {
		ApexPages.StandardController stdController;
		stdController = new ApexPages.StandardController(PricebookLayoutControllerTest.getPricebook());

		PricebookLayoutController extController = new PricebookLayoutController(stdController);

		List<PricebookEntry> getcurrentPBE = extController.getLastPricebookEntries();
		extController.filter = 'PROD';
		extController.buscar();
		
		extController.next();
		extController.previous();
		extController.last();
		extController.first();
		extController.showHome();
		extController.showSearchProducts();
		extController.showSelectedEntries();

		extController.filter = 'AFK';
		extController.buscar();
		extController.savePricebookEntries();

		for(PricebookLayoutController.PBEWrapper pbeWrap: extController.newPBE){
			pbeWrap.selected = true;
		}
		
		extController.savePricebookEntries();

		PricebookEntry afkPBE = [SELECT Id FROM PricebookEntry LIMIT 1];
		extController.pricebookEntryID = afkPBE.Id;
		extController.deletePricebookEntry();

		extController.pricebookEntryID=null;
		extController.deletePricebookEntry();
	}
	
	
}