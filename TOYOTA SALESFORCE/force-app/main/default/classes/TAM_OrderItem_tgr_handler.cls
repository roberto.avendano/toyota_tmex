/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Trigger que sirve para actualizar el campo de TAM_MontoReembolsoAbierto__c
                        para los reportes de Tableou.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    12-OCTUBRE-2021   Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_OrderItem_tgr_handler extends TriggerHandler{

    private List<OrderItem> ordenListNew;
    private Map<Id,OrderItem> mapCtesServ;
    
    public TAM_OrderItem_tgr_handler() {
        this.ordenListNew = Trigger.new;
        this.mapCtesServ = (Map<Id,OrderItem>)Trigger.newMap;
    }

    public override void beforeInsert(){
        //actualizaMontoReembolso(ordenListNew);
    }

    public override void beforeUpdate(){
        actualizaMontoReembolso(ordenListNew);
    }

    public static void actualizaMontoReembolso(List<OrderItem> objOrdenPrm){
        System.debug('EN TAM_OrderItem_tgr_handler.actualizaMontoReembolso...');
        
        //Toma la lista de los reg que vienen en objOrdenPrm
        for (OrderItem objOrdenPaso : objOrdenPrm){
            //Actualiza el campo de TAM_MontoReembolsoAbierto__c con el de MontoReembolso__c
            objOrdenPaso.TAM_MontoReembolsoAbierto__c = objOrdenPaso.MontoReembolso__c;
            System.debug('EN TAM_OrderItem_tgr_handler.actualizaMontoReembolso setIdLead TAM_MontoReembolsoAbierto: ' + objOrdenPaso.TAM_MontoReembolsoAbierto__c);

        }

    }

    
}