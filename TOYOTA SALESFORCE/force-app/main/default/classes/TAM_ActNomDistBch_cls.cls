global without sharing class TAM_ActNomDistBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActNomDistBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActNomDistBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Lead> scope){
        System.debug('EN TAM_ActNomDistBch_cls.');
        Set<String> setCodDist = new Set<String>();
        Map<String, String> mapCodDist = new Map<String, String>();
        Map<String, Lead> mapIdLeadUps = new Map<String, Lead>();
        
        //Recorre la lista de Leads que viene en el scope
        for (Lead cand : scope){
            //System.debug('EN TAM_ActNomDistBch_cls cand:' + cand);
            if (cand.FWY_codigo_distribuidor__c != null)
                setCodDist.add(cand.FWY_codigo_distribuidor__c);
        }
        System.debug('EN TAM_ActNomDistBch_cls setCodDist:' + setCodDist);
         
        //Consulta los nombres de los distr asciados a  setCodDist
        for (Account objDist : [Select a.Name, a.Codigo_Distribuidor__c From Account a
                Where a.Codigo_Distribuidor__c IN :setCodDist]){
            mapCodDist.put(objDist.Codigo_Distribuidor__c, objDist.Name);
        }
        //System.debug('EN TAM_ActNomDistBch_cls mapCodDist:' + mapCodDist);

        //Recorre la lista de Leads que viene en el scope
        for (Lead cand : scope){
            //Ve si existe ese FWY_codigo_distribuidor__c en mapCodDist
            if (cand.FWY_codigo_distribuidor__c != null)
                if (mapCodDist.containsKey(cand.FWY_codigo_distribuidor__c))
                    mapIdLeadUps.put(cand.id, new Lead(id = cand.id, FWY_Nombre_distribuidor__c = mapCodDist.get(cand.FWY_codigo_distribuidor__c)));                    
        }
        System.debug('EN TAM_ActNomDistBch_cls mapIdLeadUps:' + mapIdLeadUps.KeySet());
        System.debug('EN TAM_ActNomDistBch_cls mapIdLeadUps:' + mapIdLeadUps.Values());

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!mapIdLeadUps.isEmpty()){
           //Recorre la lista de Opp y busca el VIN para asociar el id de la Cand en AccountId
           List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdLeadUps.values(), Lead.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
              if (!objDtbUpsRes.isSuccess())
                  System.debug('EN TAM_ActNomDistBch_cls Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());         
          }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
                
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActNomDistBch_cls.finish Hora: ' + DateTime.now());  
        
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_ActNombreDistProsMinutos);
        DateTime dtHoraActual = DateTime.now();
        DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //3
        String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
        System.debug('EN TAM_ActNomDistBch_cls.finish CRON_EXP: ' + CRON_EXP);

        //Manda a llamar el proceso de TAM_ActPropProsSch_cls para actuelizar el campo Prop
        TAM_ActNomDistSch_cls objActNomDistSch = new TAM_ActNomDistSch_cls();
        System.debug('EN TAM_ActNomDistSch_cls.finish objActNomDistSch: ' + objActNomDistSch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('TAM_ActNomDistSch_cls: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objActNomDistSch);
    } 
    
}