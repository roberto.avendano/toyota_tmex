/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_CheckOutDetalleSolicitudCompra__c
                        y cancelar las soicitudes que no tienen un mov asociado en DD.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCancelSolInvSch_cls implements Schedulable{

    global String sQuery {get;set;}
    global Boolean blnCanSolMesAntPrm {get;set;}
    global Date dtFechaIniConsPrm {get;set;}
    global Date dtFechaFinConsPrm {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActCancelSolInvSch_cls.execute...');

        String strTipoPedido = 'Inventario';
        String strVinPrueba = '5TDGZRAH0MS049120';
        String EstatusDOD = 'Cancelado';
        String EstDealerSol = 'Cancelada';
        String EstDealerSol2 = 'Rechazada';

        //Date dFechaConsulta = Date.newInstance(2021,07,01);//Date.today();
        Date dFechaConsulta = Date.today(); //Date.newInstance(2021,09,01);   
        Date dFechaFinCal = dFechaConsulta.addDays(-1);
        System.debug('EN TAM_ActCancelSolInvSch_cls.execute dFechaFinCal: ' + dFechaFinCal);
        
        this.blnCanSolMesAntPrm = false;
        this.dtFechaIniConsPrm = Date.today();
        this.dtFechaFinConsPrm = Date.today();
        
        //Consulta los datos del calendario donde le fecha de fin del calendario coincida con: 
        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
            From TAM_CalendarioToyota__c t Where TAM_FechaFin__c =:dFechaFinCal]){
            //Coincide la fecha del calendario Toyota
            this.blnCanSolMesAntPrm = true;
            this.dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
            this.dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
            System.debug('EN TAM_ActCancelSolInvSch_cls.execute this.blnCanSolMesAntPrm: ' + this.blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + this.dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + this.dtFechaFinConsPrm);
        }
        System.debug('EN TAM_ActCancelSolInvSch_cls.execute blnCanSolMesAntPrm: ' + blnCanSolMesAntPrm);
        
        //Si no es el fin del calendario del mes entonces toma la fecha de hoy y consulta
        if (!this.blnCanSolMesAntPrm){
            dFechaFinCal = Date.today();
	        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
	            From TAM_CalendarioToyota__c t Where TAM_FechaInicio__c <=:dFechaFinCal
	            And TAM_FechaFin__c >=:dFechaFinCal]){
	            //Coincide la fecha del calendario Toyota
	            this.dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
	            this.dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
	            System.debug('EN TAM_ActCancelSolInvSch_cls.execute this.blnCanSolMesAntPrm: ' + this.blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + this.dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + this.dtFechaFinConsPrm);
	        }//Fin del for para TAM_CalendarioToyota__c
        }//Fin si 
        
        //Una prueba
        if (Test.isRunningTest()){
             this.dtFechaIniConsPrm = Date.today();
             this.dtFechaFinConsPrm = Date.today();
             this.blnCanSolMesAntPrm = true;
             System.debug('EN TAM_ActCancelSolInvSch_cls.execute this.blnCanSolMesAntPrm2: ' + this.blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + this.dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + this.dtFechaFinConsPrm);
        }//Fin si Test.isRunningTest()
        
        this.sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
        this.sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_NombreDistribuidor__c ';
        this.sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
        this.sQuery += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
        //this.sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= ' + String.valueOf(this.dtFechaIniConsPrm);
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c <= ' + String.valueOf(this.dtFechaFinConsPrm);      
        this.sQuery += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';

        this.sQuery += ' And TAM_EstatusDOD__c != \'' + String.escapeSingleQuotes(EstatusDOD) + '\'';
        this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol) + '\'';
        this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol2) + '\'';

        this.sQuery += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';
        //this.sQuery += ' Limit 5';
 
        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActCancelSolInvSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  objActCancelSolInvBch      
        TAM_ActCancelSolInvBch_cls objActCancelSolInvBch = new TAM_ActCancelSolInvBch_cls(sQuery, this.blnCanSolMesAntPrm, this.dtFechaIniConsPrm, this.dtFechaFinConsPrm );
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest() && this.blnCanSolMesAntPrm)
            Id batchInstanceId = Database.executeBatch(objActCancelSolInvBch, 5);
                     
    }
    
}