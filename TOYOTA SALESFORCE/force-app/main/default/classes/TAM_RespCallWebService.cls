public with sharing class TAM_RespCallWebService {

    @AuraEnabled 
	public String NombreMetodo {get;set;}
    @AuraEnabled 
	public Boolean Error {get;set;}
    @AuraEnabled 
	public String Detalle {get;Set;}
    @AuraEnabled 
	public String JsonRes {get;set;}
    @AuraEnabled 
	public TAM_CallRestWebService.loginResponse loginResponse {get;set;}
	
	//Objeto para la respuesta del servicio 
	public TAM_RespCallWebService(){
		this.NombreMetodo = '';
		this.Error = false;
		this.Detalle = '';
		this.JsonRes = '';
		this.loginResponse = new TAM_CallRestWebService.loginResponse();
	}

	//Objeto para la respuesta del servicio 
	public TAM_RespCallWebService(String NombreMetodo, Boolean Error, String Detalle, String JsonRes,
		TAM_CallRestWebService.loginResponse loginResponse){
		this.NombreMetodo = NombreMetodo;
		this.Error = Error;
		this.Detalle = Detalle;
		this.JsonRes = JsonRes;
		this.loginResponse = loginResponse;
	}
    
}