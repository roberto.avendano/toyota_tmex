/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase InventarioTransitoTriggerHandler.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    04-Mayo-2020    		Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InventarioTransitoTriggerHandler_tst {

	@TestSetup static void loadData(){

		Inventario_en_Transito__c InventarioTransito = new Inventario_en_Transito__c(
			Name='MR2B29F36K1168300', 
			Dealer_Code__c='57002', 
			Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			Dummy_VIN__c='36K1168300', 
			Exterior_Color_Code__c='01G3', 
			Exterior_Color_Description__c='GRAY ME.', 
			Interior_Color_Code__c='FC20', 
			Interior_Color_Description__c='S/D BLACK', 
			Model_Number__c='2005', 
			Model_Year__c='2019', 
			Toms_Series_Name__c='YARIS SD'
			//Tipo_Inventario__c='Piso'
			//TAM_IdExterno__c='MR2B29F36K1168300-Piso'
	    );
	    insert InventarioTransito;  

		InventarioWholesale__c InventarioWholesale = new InventarioWholesale__c(
			Name='MR2B29F36K1168300', 
			Dealer_Code__c='57002', 
			Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			Dummy_VIN__c='36K1168300', 
			Exterior_Color_Code__c='01G3', 
			Exterior_Color_Description__c='GRAY ME.', 
			Interior_Color_Code__c='FC20', 
			Interior_Color_Description__c='S/D BLACK', 
			Model_Number__c='2005', 
			Model_Year__c='2019', 
			Toms_Series_Name__c='YARIS SD'
			//Tipo_Inventario__c='Piso'
			//TAM_IdExterno__c='MR2B29F36K1168300-Piso'
	    );
	    insert InventarioWholesale;  
				
		TAM_InventarioVehiculosToyota__c TAMInventarioVehiculosToyota = new TAM_InventarioVehiculosToyota__c(
			Name='MR2B29F36K1168300', 
			Dealer_Code__c='57002', 
			Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			Dummy_VIN__c='36K1168300', 
			Exterior_Color_Code__c='01G3', 
			Exterior_Color_Description__c='GRAY ME.', 
			Interior_Color_Code__c='FC20', 
			Interior_Color_Description__c='S/D BLACK', 
			Model_Number__c='2005', 
			Model_Year__c='2019', 
			Toms_Series_Name__c='YARIS SD'
			//Tipo_Inventario__c='Piso'
			//TAM_IdExterno__c='MR2B29F36K1168300-Piso'
	    );
	    insert TAMInventarioVehiculosToyota;
				             				
	}

    static testMethod void InventarioTransitoTriggerHandlerOK() {

		//Consulta los datos del cliente    	
    	for (Inventario_en_Transito__c objInvTransuto : [Select Id, Name From Inventario_en_Transito__c LIMIT 1]){
	   		System.debug('EN InventarioTransitoTriggerHandlerOK objInvTransuto: ' + objInvTransuto);		
    	}

    }

}