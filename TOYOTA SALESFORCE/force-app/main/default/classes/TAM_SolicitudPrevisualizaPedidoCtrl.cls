/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para guardar los datos de los modelos solicitados para Flotilla o Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    02-Enero-2020    	Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitudPrevisualizaPedidoCtrl {

	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();

	static String sRectorTypePasoDetSolPrograma = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoDetSolFlotilla = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();

	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();
		    
	//Un constructor por default
	public TAM_SolicitudPrevisualizaPedidoCtrl(){}
  
    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion saveDatosModelos(String sWrpDist, String recordId, String strSolRecortTypeId) {    	
    	System.debug('EN saveDatos sWrpDist: ' + sWrpDist );
    	System.debug('EN saveDatos recordId: ' + recordId);
    	System.debug('EN saveDatos strSolRecortTypeId: ' + strSolRecortTypeId);
    	String sMensaejeSalidaUpd = '';  
		
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		List<TAM_OrdenDeCompraWrapperClass> lObjWrpDist = new List<TAM_OrdenDeCompraWrapperClass>();		
		List<TAM_DetalleOrdenCompra__c>	ListDistProgUps = new List<TAM_DetalleOrdenCompra__c>();
		Set<String> setCaveDistr = new Set<String>();
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDistElim = new List<TAM_WrpDistSolFlotillaPrograma>();
		Set<String> setCaveDistrElim = new Set<String>();
		List<TAM_OrdenDeCompraWrapperClass>	ListDistProgDel = new List<TAM_OrdenDeCompraWrapperClass>();
		
		String sGetTipoRegSol = getTipoRegSol(recordId);
		System.debug('EN saveDatos sGetTipoRegSol: ' + sGetTipoRegSol);
		
		Savepoint sp = Database.setSavepoint();
		Boolean bolExito = true;
				
		//Ve si tiene algo la lista 
		lObjWrpDist = JSONParserSFDC(sWrpDist);
			
		//Recorre la lista de y obten el id del distribuidor
		for (TAM_OrdenDeCompraWrapperClass objWrpDist : lObjWrpDist){
			//Solo los que son mayor a 0
			System.debug('EN saveDatos objWrpDist: ' + objWrpDist);
			
			String sRecordName = objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strVersion + ' ' + objWrpDist.strModelo;
			//String sIdExterno = recordId + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strModelo + '-' + objWrpDist.strVersion + '-' + objWrpDist.strCodigoColorExterior + '-' + objWrpDist.strCodigoColorInterior + '-' + sRectorTypePasoPedidoEspecial;
			String sIdExterno = recordId + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strModelo + '-' + objWrpDist.strCodigoColorExterior + '-' + objWrpDist.strCodigoColorInterior + '-' + sGetTipoRegSol;
			System.debug('EN saveDatos sIdExterno: ' + sIdExterno);
			
			//Crea el registro y agregalo a la lista de ListDistProgUps
			ListDistProgUps.add(new TAM_DetalleOrdenCompra__c(
					Name = sRecordName,
					RecordTypeid = sRectorTypePasoPedidoEspecial,
					TAM_Cantidad__c = Integer.valueOf(objWrpDist.strCantidad),
					TAM_EstatusDealerSolicitud__c = 'Pendiente',
					TAM_FechaSolicitud__c = Date.today(),
					//TAM_SolicitudCompraPROGRAMA__c = recordId,
					TAM_SolicitudFlotillaPrograma__c = recordId,
					TAM_CatalogoCentralizadoModelos__c = objWrpDist.strIdCatModelos,
					TAM_IdExterno__c = sIdExterno,
					TAM_EntregarEnMiDistribuidora__c = objWrpDist.boolDistribuidoraActual
				)
			);
		}//Fin del for para lObjWrpDist
   		System.debug('EN saveDatos ListDistProgUps: ' + ListDistProgUps);
    	
    	Set<String> setIdNuevosReg = new Set<String>();
    	//Ve si tiene algo la lista de ListDistProgUps
    	if (!ListDistProgUps.isEmpty()){
	        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(ListDistProgUps, TAM_DetalleOrdenCompra__c.TAM_IdExterno__c, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess()){
					objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Hubo un error a la hora de crear/Actualizar los registros de los Modelos ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
					bolExito = false;
				}	
				if (objDtbUpsRes.isSuccess()){
					setIdNuevosReg.add(objDtbUpsRes.getId());
					objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos de los Modelos seleccionados se crear/Actualizar con exito');
				}	
			}//Fin del for para lDtbUpsRes
    	} //Fin si ListDistProgUps.isEmpty()
    	System.debug('EN saveDatos objRespoPaso.setIdNuevosReg: ' + setIdNuevosReg);
		System.debug('EN saveDatos objRespoPaso.strDetalle: ' + objRespoPaso.strDetalle);
		 
		Map<String, String> mapIdExtDet = new Map<String, String>();
		//Busca los reg asociados a setIdNuevosReg
		for (TAM_DetalleOrdenCompra__c objDetSol : [Select id, TAM_EntregarEnMiDistribuidora__c, TAM_IdExterno__c 
			From TAM_DetalleOrdenCompra__c Where id IN : setIdNuevosReg And TAM_EntregarEnMiDistribuidora__c = false]){
			String sTAMIdExterno = objDetSol.TAM_IdExterno__c.substring(0, objDetSol.TAM_IdExterno__c.LastIndexOf('-'));	
			mapIdExtDet.put(sTAMIdExterno, objDetSol.id);
		}//Fin del for para TAM_DetalleOrdenCompra__c
    	System.debug('EN saveDatos objRespoPaso.mapIdExtDet: ' + mapIdExtDet.KeySet());
		System.debug('EN saveDatos objRespoPaso.mapIdExtDet: ' + mapIdExtDet.Values());
		
		List<TAM_DistribuidoresFlotillaPrograma__c> lDistribFlotProgUps = new List<TAM_DistribuidoresFlotillaPrograma__c>();
		//Ya tienes los reg del detalle de la sol busca los reg relacionados en el objeto de TAM_DistribuidoresFlotillaPrograma__c
		for (TAM_DistribuidoresFlotillaPrograma__c objDistFlotProd : [Select id, TAM_IdExterno__c
			From TAM_DistribuidoresFlotillaPrograma__c
			Where TAM_SolicitudFlotillaPrograma__c =: recordId]){
			String sTAMIdExterno = objDistFlotProd.TAM_IdExterno__c.substring(0, objDistFlotProd.TAM_IdExterno__c.LastIndexOf('-'));
			String sTAMIdExternoFinal = sTAMIdExterno.substring(0, sTAMIdExterno.LastIndexOf('-'));
			//Ve si existe sTAMIdExternoFinal en mapIdExtDet y traere el id del reistro padre
			if (mapIdExtDet.containsKey(sTAMIdExternoFinal)){
				lDistribFlotProgUps.add(new TAM_DistribuidoresFlotillaPrograma__c (
						id = objDistFlotProd.id,
						TAM_DetalleSolicitudCompra_FLOTILLA__c = mapIdExtDet.get(sTAMIdExternoFinal)
					)
				);
			}//Fin si mapIdExtDet.containsKey(sTAMIdExternoFinal)
		}
    	System.debug('EN saveDatos objRespoPaso.lDistribFlotProgUps: ' + lDistribFlotProgUps);

    	Set<String> setIdUpdReg = new Set<String>();
    	//Ve si tiene algo la lista de lDistribFlotProgUps
    	if (!lDistribFlotProgUps.isEmpty()){
	        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lDistribFlotProgUps, TAM_DistribuidoresFlotillaPrograma__c.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess()){
					objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Hubo un error a la hora de crear/Actualizar los registros de los distribuidores ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
					bolExito = false;
				}
				if (objDtbUpsRes.isSuccess()){
					setIdUpdReg.add(objDtbUpsRes.getId());
					objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos de los Modelos seleccionados se crear/Actualizar con exito');
				}	
			}//Fin del for para lDtbUpsRes
    	} //Fin si lDistribFlotProgUps.isEmpty()
    	System.debug('EN saveDatos objRespoPaso.setIdUpdReg: ' + setIdUpdReg);
		System.debug('EN saveDatos objRespoPaso.strDetalle: ' + objRespoPaso.strDetalle);
		
		//Para prueba
		if (!bolExito)
			Database.rollback(sp);
		
        //serializa la lista y crea el JSON
		//sCadenaPaso = JSON.serialize(objPasoWrpDist);              
        System.debug('EN saveDatos objRespoPaso: ' + objRespoPaso);
		                 
        //Regresa la cadena
        return objRespoPaso;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion eliminaDatosModelos(String recordId, String sstrIdCatModelos, String sstrIdExterno) {    	
    	System.debug('EN eliminaDatosModelos recordId: ' + recordId );
    	System.debug('EN eliminaDatosModelos sstrIdCatModelos: ' + sstrIdCatModelos);
    	System.debug('EN eliminaDatosModelos sstrIdExterno: ' + sstrIdExterno);
    	String sMensaejeSalidaUpd = '';  
    	
    	String sTAMIdExternoPaso = sstrIdExterno + '%';
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
    	Set<String> setIdDetOrdCom = new Set<String>();
    	List<TAM_DetalleOrdenCompra__c> lTAMDetalleOrdenCompraDel = new List<TAM_DetalleOrdenCompra__c>();
    	List<TAM_DistribuidoresFlotillaPrograma__c> lTAMDistribuidoresFlotillaProgramaDel = new List<TAM_DistribuidoresFlotillaPrograma__c>();
    	
    	//Busca los regitros en el objeto de TAM_DetalleOrdenCompra__c
    	for (TAM_DetalleOrdenCompra__c objDetOrd : [Select Id, TAM_IdExterno__c From TAM_DetalleOrdenCompra__c
    		Where TAM_SolicitudFlotillaPrograma__c = :recordId And TAM_CatalogoCentralizadoModelos__c = :sstrIdCatModelos
    		And RecordTypeid =: sRectorTypePasoPedidoEspecial
    		And TAM_IdExterno__c LIKE :sTAMIdExternoPaso]){
    		setIdDetOrdCom.add(objDetOrd.id);
    		lTAMDetalleOrdenCompraDel.add(objDetOrd);
	    	System.debug('EN eliminaDatosModelos objDetOrd: ' + objDetOrd);
    	}
    	
    	//Consulta los datos en el objeto de TAM_DistribuidoresFlotillaPrograma__c
    	for (TAM_DistribuidoresFlotillaPrograma__c objDistFlotPro : [Select Id, TAM_IdExterno__c 
    		From TAM_DistribuidoresFlotillaPrograma__c Where TAM_SolicitudFlotillaPrograma__c =:recordId 
    		And TAM_DetalleSolicitudCompra_FLOTILLA__c IN :setIdDetOrdCom 
    		And TAM_IdExterno__c LIKE :sTAMIdExternoPaso]){
    		lTAMDistribuidoresFlotillaProgramaDel.add(objDistFlotPro);	
	    	System.debug('EN eliminaDatosModelos objDistFlotPro: ' + objDistFlotPro);    		
    	}
		
		//Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
    	
    	//Elimina los reg de lTAMDistribuidoresFlotillaProgramaDel
    	if (!lTAMDistribuidoresFlotillaProgramaDel.isEmpty())
    		delete lTAMDistribuidoresFlotillaProgramaDel;
    	//Elimina los reg de lTAMDetalleOrdenCompraDel
    	if (!lTAMDetalleOrdenCompraDel.isEmpty())
    		delete lTAMDetalleOrdenCompraDel;

		//*** NO SE TE OLVIDE QUITAR ESTE COMENTARIO ES PARA PRUREBAS ****///
        //Database.rollback(sp);		
		
		//Crea el menaje de regreso
		objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos del Modelo seleccionado fueron eliminados.');
		
        //Regresa la cadena
        return objRespoPaso;
    }

	//Regresa un objeto del tipo AddTelephoneDto si no hubo error a la hora de registrar el cliente		
	public static List<TAM_OrdenDeCompraWrapperClass> JSONParserSFDC(String sJsonResp){
		System.debug('EN JSONParserSFDC: sJsonResp: ' + sJsonResp);
		
		List<TAM_OrdenDeCompraWrapperClass> listObjWrp = new List<TAM_OrdenDeCompraWrapperClass>();
		try{
            JSONParser parser = JSON.createParser(sJsonResp);
            //Ve si tiene algo el objeto de parser  
            while (parser.nextToken() != null) {//[{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":10,"intInventario":8,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2201","strSerie":"AVANZA","strVersion":"LE MT","strYear_Serie":"2019-AVANZA"},'{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":2,"intInventario":1,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2202","strSerie":"AVANZA","strVersion":"LE AT","strYear_Serie":"2019-AVANZA"} '']'
				//Inicia el detalle del objeto: sNombreObj
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					//Toma el contenido del Json y conviertelo a SignInCls.class 
					TAM_OrdenDeCompraWrapperClass objTAM_DetalleOrdenCompra = (TAM_OrdenDeCompraWrapperClass)parser.readValueAs(TAM_OrdenDeCompraWrapperClass.class);
					listObjWrp.add(objTAM_DetalleOrdenCompra);
				}//Fin si parser.getCurrentToken() == JSONToken.START_OBJECT
            }//Fin mientras parser.nextToken() != null			
		}catch(Exception ex){
			System.debug('ERROR EN JSONParserSFDC: sJsonResp: ' + ex.getMessage());
	 	}
		System.debug('ANTES DE SALIR DE JSONParserSFDC: listObjWrp: ' + listObjWrp);
			 	
		//Regresa el objeto objSignInClsPaso
		return listObjWrp;
	}
   
    public static string getTipoRegSol(String sTipoRegSol) {
    	System.debug('EN getTipoRegSol sTipoRegSol: ' + sTipoRegSol);
    	
    	String sTipoRegSolPaso = '';
    	
    	sTipoRegSolPaso = sTipoRegSol == sRectorTypePasoSolPrograma ? sRectorTypePasoSolPrograma :
    		(sTipoRegSol == sRectorTypePasoSolProgramaCerrada ? sRectorTypePasoSolPrograma :
    			( sTipoRegSol == sRectorTypePasoSolVentaCorporativa ? sRectorTypePasoSolVentaCorporativa : 
    		 		( sTipoRegSol == sRectorTypePasoSolVentaCorporativaCerrada ? sRectorTypePasoSolVentaCorporativa : sRectorTypePasoSolPrograma)
    		 	) 
    		);

		System.debug('EN getTipoRegSol sTipoRegSolPaso: ' + sTipoRegSolPaso);
        return sTipoRegSolPaso;
    }    
   
}