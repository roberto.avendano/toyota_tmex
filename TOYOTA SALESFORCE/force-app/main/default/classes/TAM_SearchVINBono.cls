/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de las solicitudes de Bono
******************************************************************************* */
public without sharing  class TAM_SearchVINBono {
   
    @AuraEnabled
    public static List <wrapVIN > vinInventarioG(String searchKeyWord,String userId) {
        User[] usr;
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List <InventarioWholesale__c> lstOfVin = new List <InventarioWholesale__c>();
        Set<String> dealerCode = new Set<String>();
        List<Account> listaDealeres = new List<Account>();
        Date fechaHoy = system.today();
        Date fechaMenos2Meses = date.today().adddays(-100);
        String searchKey = searchKeyWord + '%';
        
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];   
        
        lstOfVin = [SELECT AnioFactura__c,ConnectionReceivedId,ConnectionSentId,CreatedById,
                    CreatedDate,Dealer_Code__c,Diadefactura__c,Distributor_Invoice_Date_MM_DD_YYYY__c,
                    Exterior_Color_Code__c,Exterior_Color_Description__c,FechaFactura__c,Id,
                    Interior_Color_Code__c,Interior_Color_Description__c,IsDeleted,LastModifiedById,
                    LastModifiedDate,LastReferencedDate,LastViewedDate,MesFactura__c,Model_Number__c,
                    Model_Year__c,Name,OwnerId,SystemModstamp,TAM_ExteriorColorCode__c,TAM_IdExterno__c,
                    TAM_InteriorColorCode__c,Toms_Series_Name__c
                    FROM InventarioWholesale__c WHERE Name LIKE: searchKey AND  Dealer_Code__c =: usr[0].CodigoDistribuidor__c];
        
        if(!lstOfVin.isEmpty()){
            for(InventarioWholesale__c i : lstOfVin){
                dealerCode.add(i.Dealer_Code__c);
            }    
        }
        
        
        if(!dealerCode.isEmpty()){
            listaDealeres = [Select id,name,Codigo_Distribuidor__c from Account where Codigo_Distribuidor__c IN : dealerCode];
            
            
        }
        
        
        if(!lstOfVin.isEmpty() && !listaDealeres.isEmpty()){
            for (InventarioWholesale__c  acc: lstOfVin) {
                for(Account dealers : listaDealeres){
                    if(acc.Dealer_Code__c == dealers.Codigo_Distribuidor__c){ 
                        wrapVIN newVIN = new wrapVIN();
                        newVIN.VIN = acc.name;
                        newVIN.nombreDealer  = dealers.name;
                        newVIN.inventarioG = true;
                        newVIN.inventarioF = false;
                        newVIN.Serie = acc.Toms_Series_Name__c;
                        newVIN.codigoModelo = acc.Model_Number__c;
                        newVIN.anioModelo = acc.Model_Year__c;
                        newVIN.codigoDealer = acc.Dealer_Code__c;
                        newVIN.vinBDDG = true;
                        newVIN.vinDealerDaily = false;
                        wrapListVIN.add(newVIN);   
                        
                    }
                }   
            }
        }
        
        if(!wrapListVIN.isEmpty()){
            return wrapListVIN; 
        }else{
            wrapListVIN = TAM_SearchVINBono.vinBDDealerDaily(searchKeyWord,userId);
            return wrapListVIN;
        }
        
        
    }
    
    @AuraEnabled
    public static List <wrapVIN > vinBDDealerDaily(String searchKeyWord,String userId) {
        User[] usr;
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List <Movimiento__C > returnList = new List <Movimiento__C > ();
        Date fechaHoy = system.today();
        Date fechaMenos2Meses = date.today().adddays(-100);
        String searchKey = searchKeyWord + '%';
        
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];     
        List <Movimiento__C > lstOfVin = [SELECT id,Sale_Code__c,Distribuidor__r.name,Distribuidor__r.Codigo_Distribuidor__c,Movimiento__c.Name,Distribuidor__c,VIN__r.name,Sale_Date__c,TAM_Serie__c,TAM_VIN__c,TAM_codigoModelo__c,TAM_AnioModelo__c from  Movimiento__C
                                          WHERE Sale_Date__c>=: fechaMenos2Meses AND Sale_Date__c <=: fechaHoy AND  VIN__r.name LIKE: searchKey  AND 
                                		 VIN__r.Ultimo_Movimiento__r.Trans_Type__c = 'RDR' AND Distribuidor__R.Codigo_Distribuidor__c =: usr[0].CodigoDistribuidor__c order by createdDate desc limit 1];
        
        if(!lstOfVin.isEmpty()){
            for (Movimiento__C  acc: lstOfVin) {
                wrapVIN newVIN = new wrapVIN();
                newVIN.VIN = acc.VIN__r.name;
                newVIN.inventarioG = true;
                newVIN.inventarioF = false;
                newVIN.Serie = acc.TAM_Serie__c;
                newVIN.codigoModelo = acc.TAM_codigoModelo__c;
                newVIN.anioModelo = acc.TAM_AnioModelo__c;
                newVIN.codigoDealer = acc.Distribuidor__r.Codigo_Distribuidor__c;
                newVIN.vinDealerDaily = true;
                newVIN.vinBDDG = false;
                newVIN.fechaVentaVIN = acc.Sale_Date__c;
                newVIN.codigoVenta	 = acc.Sale_Code__c;
                newVIN.nombreDealer  = acc.Distribuidor__r.name;
                wrapListVIN.add(newVIN);   
            }
        }
        
        return wrapListVIN;
        
    }
    
    
    public class wrapVIN {
        @AuraEnabled public boolean inventarioG {get; set;}
        @AuraEnabled public boolean inventarioF {get; set;}
        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String nombreDealer {get; set;}
        @AuraEnabled public String codigoVenta {get; set;} 
        @AuraEnabled public String VIN {get; set;} 
        @AuraEnabled public String Serie {get; set;}
        @AuraEnabled public String codigoModelo {get; set;}
        @AuraEnabled public String anioModelo {get; set;}
        @AuraEnabled public Boolean vinDealerDaily {get; set;}
        @AuraEnabled public Boolean vinBDDG {get; set;}
        @AuraEnabled public decimal precioPublicoVigente {get; set;} 
        @AuraEnabled public decimal precioPublicoDeVenta {get; set;}
        @AuraEnabled public Date fechaVentaVIN {get; set;}
        
        
    }
    
    
}