@isTest
private class DefaultValuesOrderEditControllerTest {
	@TestSetup static void loadData(){
	//Create portal account owner
	List<String> validAdmins= new List<String>{'Administrador del sistemaAD', 'Administrador del sistema', 'System Administrator'};
	UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
	Profile admin = [Select Id From Profile Where Name IN:validAdmins LIMIT 1];
	

	User portalAccountOwner1 = new User(
		UserRoleId = portalRole.Id,
		ProfileId = admin.Id,
		Username = 'ToyotaAdminPartner@test.com',
	   	Alias = 'batman',
		Email='bruce.wayne@wayneenterprises.com',
		EmailEncodingKey='UTF-8',
		Firstname='Bruce',
		Lastname='Wayne',
		LanguageLocaleKey='en_US',
		LocaleSidKey='en_US',
		TimeZoneSidKey='America/Chicago'
	);

	insert portalAccountOwner1;
	
	}

	@isTest static void one() {		
		User testAcc = [SELECT Id, Name, Alias FROM User WHERE Alias='batman' LIMIT 1];
		Profile p = [Select Id, Name From Profile Where Name='Gerente de Ventas Distribuidor' LIMIT 1];
		Profile admin = [Select Id, Name From Profile Where Name='Administrador del sistema' LIMIT 1];
		List<ParametrosConfiguracionToyota__c> parametrosConfig = new List<ParametrosConfiguracionToyota__c>();		
		
	    ParametrosConfiguracionToyota__c pcIFE = new ParametrosConfiguracionToyota__c(
        	Name= 'FormatoPRIFE',
        	DescripcionParametro__c = '',
        	Consecutivo__c = 101,
        	Valor__c= ''
    	);
    	parametrosConfig.add(pcIFE);


	    ParametrosConfiguracionToyota__c pcCirculacion  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRCirculacion',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 102,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcCirculacion);


	    ParametrosConfiguracionToyota__c pcFormato  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRSolicitud',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 103,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcFormato);

	    ParametrosConfiguracionToyota__c pcRazonSocial  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRRazonSocial',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 104,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcRazonSocial);

	    ParametrosConfiguracionToyota__c pcFactura  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRFactura',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 105,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcFactura);

	    ParametrosConfiguracionToyota__c pcFotoVIN  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRFotoVIN',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 106,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcFotoVIN);

	    ParametrosConfiguracionToyota__c pcFotoParte  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRFotoParte',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 107,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcFotoParte);

	    ParametrosConfiguracionToyota__c pcFotoPlaca  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRFotoPlaca',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 108,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcFotoPlaca);

	    ParametrosConfiguracionToyota__c pcCartaRobo  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPRCartaRobo',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 109,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcCartaRobo);

	    ParametrosConfiguracionToyota__c pcOtros  = new ParametrosConfiguracionToyota__c(
	        Name= 'FormatoPROtros',
	        DescripcionParametro__c = '',
	        Consecutivo__c = 110,
	        Valor__c= ''
	    );
	    parametrosConfig.add(pcOtros);

	    insert parametrosConfig;


			Account portalAccount1 = new Account(
				Name = 'TestAccount',
				OwnerId = testAcc.Id,
				TarjetaCirculacion__c = true,
				FormatoSolicitudPartesRobadas__c = false,
				CartaMembretadaRazonSocial__c = true,
				CopiaFacturaOriginal__c = false,
				FotoVehiculo__c = false,
				FotoParte__c = true,
				FotoPlacaVIN__c = true,
				CartaRobo__c = true,
				Otros__c = false
			);
			insert portalAccount1;

			Contact contact1 = new Contact(
				FirstName = 'Test',
	    		Lastname = 'McTesty',
				AccountId = portalAccount1.Id,
	    		Email = System.now().millisecond() + 'test@test.com'
			);
			insert contact1;

			User user1 = new User(
			    Username = 'toyotaPartnerU@test.com',
			    ContactId = contact1.Id,
			    ProfileId = p.Id,
			    Alias = 'test123',
			    Email = 'test12345@test.com',
			    EmailEncodingKey = 'UTF-8',
			    LastName = 'McTesty',
			    CommunityNickname = 'test12345',
			    TimeZoneSidKey = 'America/Los_Angeles',
			    LocaleSidKey = 'en_US',
			    LanguageLocaleKey = 'en_US'
			);
			insert user1;

			User user2 = new User(
			    Username = 'toyotaPartnerU2@test.com',
			    ProfileId = admin.Id,
			    Alias = 'tet1234',
			    Email = 'test12345@test.com',
			    EmailEncodingKey = 'UTF-8',
			    LastName = 'McTesty2',
			    CommunityNickname = 'test1256',
			    TimeZoneSidKey = 'America/Los_Angeles',
			    LocaleSidKey = 'en_US',
			    LanguageLocaleKey = 'en_US'
			);
			insert user2;

			System.runAs(user1){
				ApexPages.StandardController stdController = new ApexPages.StandardController(new Order());
				DefaultValuesOrderEditController extController = new DefaultValuesOrderEditController(stdController);			
				extController.goToEdit();			
			}

			System.runAs(user2){
				ApexPages.StandardController stdController = new ApexPages.StandardController(new Order());
				DefaultValuesOrderEditController extController = new DefaultValuesOrderEditController(stdController);			
				extController.goToEdit();	
			}

	}
	
}