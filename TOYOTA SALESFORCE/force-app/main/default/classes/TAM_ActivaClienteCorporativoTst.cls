/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de pruena para TAM_ActivaClienteCorporativoCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    24-Agosto-2019       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActivaClienteCorporativoTst {

	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
	static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	static String VaRtAccRegPM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String VaRtAccRegPF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String VaRtCteCorp = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static  TAM_ReactivarCliente__c objReactivarCliente = new TAM_ReactivarCliente__c();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();
	static	ContentVersion clienteContVerPrueba = new ContentVersion();
	static	ContentVersion candidatoContVerPrueba = new ContentVersion();

	@TestSetup static void loadData(){
		
		Account clienteMoral = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = VaRtAccRegPM
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'TestAccount',
			LastName = 'TestAccountLastName',
			Codigo_Distribuidor__c = '570551',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = VaRtAccRegPF
		);
		insert clienteFisico;
		
		TAM_ReactivarCliente__c objReactCte = new TAM_ReactivarCliente__c(
			Name = 'TestAccount',
			TAM_TipoCliente__c = 'Persona moral',
			TAM_ApellidoPaterno__c = 'TestAccount',
			TAM_ApellidoMaterno__c = 'TestAccount'
			/*UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = VaRtAccRegPF*/
		);
		insert objReactCte;

        ContentVersion clienteCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data2'),
                IsMajorVersion = true
        );
        insert clienteCv;
		clienteContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :clienteCv.Id LIMIT 1];
		
        ContentVersion candCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data1'),
                IsMajorVersion = true
        );
        insert candCv;
        candidatoContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :candCv.Id LIMIT 1];
				             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_ActivaClienteCorporativoOK() {
        Test.startTest();
			//Consulta los datos del cliente
			clientePruebaMoral = [Select Id, Name, RecordTypeId, RecordType.Name From Account Where RecordTypeId =:VaRtAccRegPM LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk clientePruebaMoral: ' + clientePruebaMoral);
	
			objReactivarCliente = [Select Id, Name, TAM_TipoCliente__c, TAM_ApellidoPaterno__c, TAM_ApellidoMaterno__c, TAM_ProgramaRango__c,
				TAM_GrupoCorporativo__c, TAM_NombreComercial__c, TAM_RFC__c, TAM_CorreoElectronico__c, TAM_Telefono__c, TAM_Calle__c,
				TAM_CodigoPostal__c, TAM_Colonia__c, TAM_Ciudad__c, TAM_Estado__c, TAM_NombreRL__c, TAM_ApellidoPaternoRL__c,
				TAM_ApellidoMaternoRL__c, TAM_RFCRL__c, TAM_CorreoElectronicoRL__c, TAM_TelefonoRL__c, TAM_Pais__c, TAM_SitioWeb__c 
				From TAM_ReactivarCliente__c LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk objReactivarCliente: ' + objReactivarCliente);
	        
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_ActivaClienteCorporativoCtrl.getDatosCliente(clientePruebaMoral.id);
		   
		   TAM_ActivaClienteCorporativoCtrl.wrpDatosClienteCorporativo objWrpDatCtes = 
		   	   new TAM_ActivaClienteCorporativoCtrl.wrpDatosClienteCorporativo();	
		   TAM_ActivaClienteCorporativoCtrl.wrpArchichivosRequeridos objWrpArchReq = 
		   	   new TAM_ActivaClienteCorporativoCtrl.wrpArchichivosRequeridos(false, false, false, false, false, false, false);	
	
			TAM_ActivaClienteCorporativoCtrl.wrpMapDatosCatalogos objWrpMapDatosCatalogos = 
				new TAM_ActivaClienteCorporativoCtrl.wrpMapDatosCatalogos();
			TAM_ActivaClienteCorporativoCtrl.wrpMapDatosCatalogos objWrpMapDatosCatalogos2 = 
				new TAM_ActivaClienteCorporativoCtrl.wrpMapDatosCatalogos('strCodigo', 'strDescripcion', true);
			
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getArchivosCargardos
	       TAM_ActivaClienteCorporativoCtrl.getArchivosCargardos(clientePruebaMoral.id);
	       
	       //Crea un objeto del tipo wrpDatosClienteCorporativo
	       TAM_ActivaClienteCorporativoCtrl.wrpDatosClienteCorporativo objWrpDatosClteCorp = 
	       	new TAM_ActivaClienteCorporativoCtrl.wrpDatosClienteCorporativo(objReactivarCliente, 'Persona Moral',
	       	clientePruebaMoral);
	       //Manda llamar elmetodo creaSolCteCorp
		   TAM_ActivaClienteCorporativoCtrl.creaSolCteCorp(clientePruebaMoral.id, objWrpDatosClteCorp, null, null);
			
	       //Manda llamar elmetodo upsDatosCliente
		   TAM_ActivaClienteCorporativoCtrl.upsDatosCliente(clientePruebaMoral.id, objWrpDatosClteCorp, null, null);
		   
	       //Manda llamar elmetodo upsDatosCliente
		   TAM_ActivaClienteCorporativoCtrl.getRangos(null, null, clientePruebaMoral.id, objWrpDatosClteCorp); //wrpDatosClienteCorporativo datosClienteCorp
		   
	       //Manda llamar elmetodo upsDatosCliente
		   TAM_ActivaClienteCorporativoCtrl.getGruposCorporativos(null, null);
	   Test.stopTest();  
    }
}