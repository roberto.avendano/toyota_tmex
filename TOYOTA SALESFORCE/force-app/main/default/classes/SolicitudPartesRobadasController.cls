public with sharing class SolicitudPartesRobadasController {
	public Order order{get;set;}
    public Document logo{
        get{
            return [SELECT Id FROM Document WHERE DeveloperName='Toyota_jpg'];
        }
        set;
    }
    public Document footer{
        get{
            return [SELECT Id FROM Document WHERE DeveloperName='ToyotaFooterPR'];
        }
        set;
    }

    public ParametrosConfiguracionToyota__c pc{
    	get{
    		if(pc==null){
    			pc = ParametrosConfiguracionToyota__c.getInstance('FormatoSolicitudesPRLeyenda');
    		}
    		return pc;
    	}
    	set;
    }

    public ParametrosConfiguracionToyota__c pcregExp{
    	get{
    		if(pcregExp == null){
    			pcregExp = ParametrosConfiguracionToyota__c.getInstance('FormatoSolicitudesPRLeyendaRegExp');
    		}
    		return pcregExp;
    	}
    	set;
    }

	public SolicitudPartesRobadasController() {		
		order =  getOrder(ApexPages.currentPage().getParameters().get('id'));
	}


	public String getLeyenda(){
        String leyenda = pc.DescripcionParametro__c != null ? String.valueOf(pc.DescripcionParametro__c):'Sin Leyenda';
		String regExp = pcregExp.Valor__c!=null ? String.valueOf(pcregExp.Valor__c) : 'sinExp';
        String razonSocial = order.Account.DenominacionSocial__c !=null ? String.valueOf(order.Account.DenominacionSocial__c): '';
        return leyenda.replaceAll(regExp, razonSocial);                
	}

	public Order getOrder(String pedido){
			return [SELECT 
					Name,
					Estatus__c,
					OrderNumber,
					Comentarios__c,
					Account.Name,
					Account.Codigo_Distribuidor__c,
					CreatedDate,
					SubtotalDistribuidor__c,
					TotalAmount,
					SubtotalCliente__c,
                	IVADistribuidor__c,
                	IVASugerido__c,
                	IVACliente__c,
                	TotalDistribuidor__c,
                	TotalSugerido__c,
                	TotalCliente__c,
                	Account.DenominacionSocial__c,
                	Account.RFC__c,
					Vehiculo__r.Ultimo_Movimiento__r.VIN__r.Modelo_b__r.Descr_Modelo__c,
					Vehiculo__r.Ultimo_Movimiento__r.First_Name__c,
					Vehiculo__r.Ultimo_Movimiento__r.Last_Name__c,
					Vehiculo__r.Name,
					(SELECT Quantity,PrecioDistribuidor__c, ListPrice, PrecioCliente__c,
					PricebookEntry.Product2.Name,
					PricebookEntry.Product2.Description,
					PricebookEntry.Product2.CantidadMaxima__c
					FROM OrderItems)
						FROM Order WHERE Id=:pedido];
	}

	
}