global class DeleteEntradasListaBatch implements Database.Batchable<sObject> {
    
    String query;
    
    global DeleteEntradasListaBatch(String q) {
        this.query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(scope.size() > 0){
            List<EntradaListaPrecios__c> entradas = (List<EntradaListaPrecios__c>) scope;
            System.debug(JSON.serialize(entradas));

            Database.DeleteResult[] result = Database.delete(entradas, false);
            System.debug(JSON.serialize(result));
            
            for(Integer i=0; i < result.size(); i++){
                if(!result[i].isSuccess()){                    
                    for(Database.Error err: result[i].getErrors()){
                        System.debug(entradas[i].Name +' '+ err.getStatusCode() + ' '+ err.getMessage());
                    }
                } else{
                    System.debug(entradas[i].Name +' Eliminado');
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('Eliminación exitosa');
    }
    
}