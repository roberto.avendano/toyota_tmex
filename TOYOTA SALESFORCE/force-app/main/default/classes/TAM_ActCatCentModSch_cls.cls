/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DetalleOrdenCompra__c, TAM_CheckOutDetalleSolicitudCompra__c,
                        TAM_DODSolicitudesPedidoEspecial__c, TAM_VinesFlotillaPrograma__c
                        y actuelizar el campo que se llama TAM_CatalogoCentralizadoModelos__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCatCentModSch_cls implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActCatCentModSch_cls.execute...');
        String strIdSolPrueba = 'a221Y000008j3M2QAI';

        this.sQuery = 'Select Id, TAM_CatalogoCentralizadoModelos__c, TAM_SolicitudFlotillaPrograma__c, ';
        this.sQuery += ' TAM_IdExterno__c From TAM_DetalleOrdenCompra__c ';
        this.sQuery += ' Where TAM_CatalogoCentralizadoModelos__c = null ';
        //this.sQuery += ' And Id = \'' + String.escapeSingleQuotes(strIdSolPrueba) + '\'';
        //this.sQuery += ' Limit 5';
        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActCatCentModSch_cls.execute sQuery: ' + sQuery);
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActCatCentModBch_cls objActCatCentModBch = new TAM_ActCatCentModBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActCatCentModBch, 15);
                     
    }

    
}