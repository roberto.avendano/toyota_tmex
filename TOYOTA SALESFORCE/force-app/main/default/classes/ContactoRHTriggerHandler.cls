/*******************************************************************************
Desarrollado por: Globant México
Autor: Roberto Carlos Avendaño Quintana
Proyecto: Assestment Center TMEX
*********************************************************************************/
public  class ContactoRHTriggerHandler extends TriggerHandler {
    
    private List<RH_Contacto__c> itemsNew;    
    
    public ContactoRHTriggerHandler(){
        this.itemsNew = (List<RH_Contacto__c>) Trigger.new;      
        
    }
    
    //Metodo que se ejecuta despues de insertar un nuevo registro en el objeto: RH_Contacto__c
    public override void afterInsert(){
        createEvalRH(itemsNew);
        
    }   
    
    //Metodo que se ejecuta despues de modificat un nuevo registro en el objeto: RH_Contacto__c
    public override void afterUpdate(){
        updateEvalRH(itemsNew);
        
    }
    
    //Metodo que se manda a ejecutar despues de crear un nuevo registro de tipo: RH_Contacto__c
    public static void createEvalRH(List<RH_Contacto__c> newItem){
        List<RH_Contacto__c> contactActivo = new List<RH_Contacto__c> ();
        List<RH_EmailCasoUso__c> inboxEval = new List<RH_EmailCasoUso__c> (); 
        List<RH_EvaluacionCandidato__c> evalCandidato = new List<RH_EvaluacionCandidato__c>();
        List<Contact> contactFlag = new List<Contact>();
        Set<id> idEvaluacion = new Set<id>();
        Set<Id> contactoLicencia = new Set<id>();
        Date fechaAssestment;
        Id EvaluacionRH; 
        Id ContactoRH;
        Id OwnerEvaluacion;
        Integer randomNumber = Integer.valueof((Math.random() * 1000));
        String password = '5Npx'+randomNumber+'mHrH';
        String casoNegocioEval,casoUso,recordIdEval,NombreCandidato,duracionEvaluacion;
        //Se recorren los nuevos registros insertados de tipo: RH_Contacto__c
        for(RH_Contacto__c contactoEval : newItem){
            //Se valida si exten nuevos registros con estatus = Activo y evaluacion no finalizada
            if(contactoEval.Estatus__c == 'Activo' && contactoEval.RH_evaluacionFinalizada__c == false ){
                //Se guardan la información en estructuras de datos para usarlos en el fluojo
                contactActivo.add(contactoEval);   
                idEvaluacion.add(contactoEval.RH_CasoNegocio__c);
                contactoLicencia.add(contactoEval.RH_Contacto__c);
                fechaAssestment = contactoEval.FechaEvaluacionRH__c;
                EvaluacionRH = contactoEval.RH_Evaluacion__c;
                NombreCandidato = (contactoEval.Name+' '+contactoEval.RH_ApellidoPaterno__c+' '+ contactoEval.RH_ApellidoMaterno__c);
                ContactoRH = contactoEval.Id;  
                duracionEvaluacion = contactoEval.Duracion_de_Evaluacion__c;
                
            }
            
        }
        
        //Se valida si exite un contacto activo y id de la evaluación sean diferentes de vacio
        if(!contactActivo.isEmpty() && !idEvaluacion.isEmpty()){
            //Un consulta SOQL para regresarnos todos los email que se la relacionaran a la evaluación utilizando como filtro el caso de negocio utilizado
            inboxEval = [Select id,name,RH_Fecha__c,RH_CC__c,RH_CCO__c,RH_Descripcion__c,RH_De__c,RH_Para__c,Correo_Adjunto__c,Prioridad__c,RH_CasoNegocio__r.name,Num_correo__c FROM RH_EmailCasoUso__c WHERE RH_CasoNegocio__c IN :idEvaluacion order by Num_correo__c asc];
        } 
        //Si nuestra lista contactoLicencia es diferente de vacio , se realiza una consulta SOQL, para obtener los datos relacionados al usuario
        if(!contactoLicencia.isEmpty()){
            OwnerEvaluacion = [SELECT ID, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID IN : contactoLicencia].Id;     
        }
        
        //Se valida que la lista inboxEval sea diferente de vacio
        if(!inboxEval.isEmpty()){
            //La lista inboxEval se recorre para por cada iteración crear un registro de tipo RH_EvaluacionCandidato__c , que son los correos que tendra la evaluación
            for(RH_EmailCasoUso__c inboxCandidato : inboxEval){
                RH_EvaluacionCandidato__c rhEvalCandidato = new RH_EvaluacionCandidato__c();
                rhEvalCandidato.RecordTypeId =  Schema.SObjectType.RH_EvaluacionCandidato__c.getRecordTypeInfosByName().get(inboxCandidato.RH_CasoNegocio__r.name).getRecordTypeId();
                rhEvalCandidato.Name = String.valueOf(inboxCandidato.Num_correo__c);
                rhEvalCandidato.RH_Fecha__c = inboxCandidato.RH_Fecha__c;
                rhEvalCandidato.RH_Asunto__c = inboxCandidato.Name;
                rhEvalCandidato.RH_CC__c = inboxCandidato.RH_CC__c;
                rhEvalCandidato.RH_CCO__c = inboxCandidato.RH_CCO__c;
                rhEvalCandidato.RH_Cuerpo__c = inboxCandidato.RH_Descripcion__c;
                rhEvalCandidato.RH_De__c = inboxCandidato.RH_De__c;
                rhEvalCandidato.RH_Para__c =  inboxCandidato.RH_Para__c;
                rhEvalCandidato.RH_Evaluacion__c =  EvaluacionRH;
                rhEvalCandidato.RH_NombreCandidato__c = NombreCandidato;
                rhEvalCandidato.RH_candidatoEvaluacion__c = ContactoRH;
                rhEvalCandidato.Correo_Adjunto__c = inboxCandidato.Correo_Adjunto__c;
                rhEvalCandidato.Prioridad__c = inboxCandidato.Prioridad__c;
                casoNegocioEval = inboxCandidato.RH_CasoNegocio__r.name;
                recordIdEval = Schema.SObjectType.RH_EvaluacionCandidato__c.getRecordTypeInfosByName().get(inboxCandidato.RH_CasoNegocio__r.name).getRecordTypeId(); 
                // Se agregan todos los registros de tipo: RH_EvaluacionCandidato__c a una lista de tipo : RH_EvaluacionCandidato__c
                evalCandidato.add(rhEvalCandidato);
                
            } 
            // Si la lista evalCandidato es diferente de vacio , se realiza una operación DML de tipo insert
            if(!evalCandidato.isEmpty()){
                try{
                    insert evalCandidato;  
                }catch(Exception e){
                    system.debug('Error al insertar los correos asociados con la solicitud'+e.getMessage());
                }
                
            }    
            //Despues de crear los correos relacionados a la evaluación se agregan los documentos a cada una de las evaluaciones
            agregarDocumentos(casoNegocioEval,evalCandidato);
            //Se manda a llamar el metodo que habilita los documentos a el candidato 
            habilitarDocumentosGrupo(casoNegocioEval,OwnerEvaluacion);
        }
        
        //Se llenan valores de control para el sistema de la evaluación
        List<RH_Contacto__c> rhContactoList = new List<RH_Contacto__c>();
        for(RH_Contacto__c contactoEvalRH : newItem){
            RH_Contacto__c contactoEval = new RH_Contacto__c();
            contactoEval.id = contactoEvalRH.id;
            contactoEval.RH_evaluacionGenerada__c = true;
            rhContactoList.add(contactoEval);
        }
        //Actualizar la bandera de evaluación creada
        if(!evalCandidato.isEmpty()){
            try{
                update rhContactoList;   
            }catch(Exception e){
                system.debug('Error al momento de actualizar el contacto RH');    
            }
            
        }
        
        //Se actualizan los datos en la licencia del contacto para que queden inhabilitada
        if(!contactoLicencia.isEmpty() && !contactActivo.isEmpty()){
            List<Contact> ctcLicencia = [Select id,RH_LicenciaDisponible__c,CasoUsoRH__c from Contact WHERE id IN : contactoLicencia];
            if(!ctcLicencia.isEmpty()){
                for(Contact ctcLicecnia : ctcLicencia){
                    ctcLicecnia.RH_LicenciaDisponible__c = false;
                    ctcLicecnia.FechaEvaluacionRH__c = fechaAssestment;
                    ctcLicecnia.Contrasena__c = password;
                    ctcLicecnia.RH_DuracionEvalucion__c = decimal.valueOf(duracionEvaluacion);
                    ctcLicencia[0].CasoUsoRH__c = recordIdEval;
                    contactFlag.add(ctcLicecnia);
                }
            }
            if(!contactFlag.isEmpty()){
                try{
                    update contactFlag; 
                    actualizarPasswUser(contactFlag[0].id,password);   
                }catch(Exception e){
                    system.debug('Error al actualizar el contacto relacionado a la evaluación');
                    
                } 
            }   
        }   
    } 
    
    //Evento de trigger que se dispara cuando un contacto RH , es actualizado a estatus = Activo
    public static void updateEvalRH(List<RH_Contacto__c> newItem){
        //Se declaran las estrcuturas de dato
        List<RH_Contacto__c> contactActivo = new List<RH_Contacto__c> ();
        List<RH_EmailCasoUso__c> inboxEval = new List<RH_EmailCasoUso__c> (); 
        List<RH_EvaluacionCandidato__c> evalCandidato = new List<RH_EvaluacionCandidato__c>();
        List<Contact> contactFlag = new List<Contact>();
        Set<id> idEvaluacion = new Set<id>();
        Set<Id> contactoLicencia = new Set<id>();
        Date fechaAssestment;
        Id EvaluacionRH,ContactoRH,OwnerEvaluacion;
        String NombreCandidato,duracionEvaluacion;
        Integer randomNumber = Integer.valueof((Math.random() * 1000));
        String password = '5Npx'+randomNumber+'mHrH';
        String casoNegocioEval,recordIdEval;
        //Con este for , se declaran los nuevos registros de tipo: RH_Contacto__c
        for(RH_Contacto__c contactoEval : newItem){
            //Se valida que un registro sea Estatus__c = 'Activo'
            if(contactoEval.Estatus__c == 'Activo' && contactoEval.RH_evaluacionFinalizada__c == false && contactoEval.RH_evaluacionGenerada__c == false){
                //Se llena con información las estructuras de datos declaradas
                contactActivo.add(contactoEval);   
                idEvaluacion.add(contactoEval.RH_CasoNegocio__c);
                contactoLicencia.add(contactoEval.RH_Contacto__c);
                fechaAssestment = contactoEval.FechaEvaluacionRH__c;
                EvaluacionRH = contactoEval.RH_Evaluacion__c;
                NombreCandidato = (contactoEval.Name+' '+ contactoEval.RH_ApellidoPaterno__c+' '+ contactoEval.RH_ApellidoMaterno__c);
                ContactoRH = contactoEval.Id;
                duracionEvaluacion = contactoEval.Duracion_de_Evaluacion__c;
            }
            
        }
        
        //Se valida que exista un contaco con estatus activo y que tenga asociado un id de la evaluación
        if(!contactActivo.isEmpty() && !idEvaluacion.isEmpty()){
            inboxEval = [Select id,name,RH_Fecha__c,RH_CC__c,RH_CCO__c,RH_Descripcion__c,RH_De__c,RH_Para__c,Prioridad__c,Correo_Adjunto__c,RH_CasoNegocio__r.name,Num_correo__c FROM RH_EmailCasoUso__c WHERE RH_CasoNegocio__c IN :idEvaluacion order by Num_correo__c asc];
        } 
        
        //SOQL para obtener ownwer de la evaluación apartir de contactoLicencia
        if(!contactoLicencia.isEmpty()){
            OwnerEvaluacion = [SELECT ID, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID IN : contactoLicencia].Id;     
        }
        
        //Se valida que inboxEval sea diferente de nulo para poder iterarlo
        if(!inboxEval.isEmpty()){
            //Por cada iteración se crea un nuevo correo que sera asociado a la bandeja de entrada del solicitante
            for(RH_EmailCasoUso__c inboxCandidato : inboxEval){
                RH_EvaluacionCandidato__c rhEvalCandidato = new RH_EvaluacionCandidato__c();
                rhEvalCandidato.Name = String.valueOf(inboxCandidato.Num_correo__c);
                rhEvalCandidato.RH_Fecha__c = inboxCandidato.RH_Fecha__c;
                rhEvalCandidato.RH_Asunto__c = inboxCandidato.Name;
                rhEvalCandidato.RH_CC__c = inboxCandidato.RH_CC__c;
                rhEvalCandidato.RH_CCO__c = inboxCandidato.RH_CCO__c;
                rhEvalCandidato.RH_Cuerpo__c = inboxCandidato.RH_Descripcion__c;
                rhEvalCandidato.RH_De__c = inboxCandidato.RH_De__c;
                rhEvalCandidato.RH_Para__c =  inboxCandidato.RH_Para__c;
                rhEvalCandidato.RH_Evaluacion__c =  EvaluacionRH;
                rhEvalCandidato.RH_NombreCandidato__c = NombreCandidato;
                rhEvalCandidato.RH_candidatoEvaluacion__c = ContactoRH;
                rhEvalCandidato.Prioridad__c = inboxCandidato.Prioridad__c;
                rhEvalCandidato.Correo_Adjunto__c = inboxCandidato.Correo_Adjunto__c;
                casoNegocioEval = inboxCandidato.RH_CasoNegocio__r.name;
                recordIdEval = Schema.SObjectType.RH_EvaluacionCandidato__c.getRecordTypeInfosByName().get(inboxCandidato.RH_CasoNegocio__r.name).getRecordTypeId(); 
                evalCandidato.add(rhEvalCandidato);
                
            } 
            //La lista evalCandidato contiene todos los correos asociados a la evaluacion del candidato y se valida que sea diferente de vacia
            if(!evalCandidato.isEmpty()){
                try{
                    insert evalCandidato;   
                }catch(Exception e){
                    system.debug('Error al insertar los correos asociados con la solicitud'+e.getMessage());
                    
                }
                
            } 
            
            //Evaluacion creada - se agregan los documentos que correspondan a la evaluación
            agregarDocumentos(casoNegocioEval,evalCandidato);
            habilitarDocumentosGrupo(casoNegocioEval,OwnerEvaluacion);
        }
        
        //Se llenan valores de control para el sistema de la evaluación
        List<RH_Contacto__c> rhContactoList = new List<RH_Contacto__c>();
        for(RH_Contacto__c contactoEvalRH : newItem){
            RH_Contacto__c contactoEval = new RH_Contacto__c();
            contactoEval.id = contactoEvalRH.id;
            contactoEval.RH_evaluacionGenerada__c = true;
            rhContactoList.add(contactoEval);
        }
        //Actualizar la bandera de evaliacion creada
        if(!evalCandidato.isEmpty()){
            try{
                update rhContactoList;    
            }catch(Exception e){
                system.debug('Error al insertar los correos asociados con la solicitud'+e.getMessage());
            }
            
        }
        
        
        if(!contactoLicencia.isEmpty() && !contactActivo.isEmpty()){
            List<Contact> ctcLicencia = [Select id,RH_LicenciaDisponible__c from Contact WHERE id IN : contactoLicencia];
            if(!ctcLicencia.isEmpty()){
                for(Contact ctcLicecnia : ctcLicencia){
                    ctcLicecnia.RH_LicenciaDisponible__c = false;  
                    ctcLicecnia.FechaEvaluacionRH__c = fechaAssestment;
                    ctcLicecnia.Contrasena__c = password;
                    ctcLicecnia.RH_DuracionEvalucion__c = decimal.valueOf(duracionEvaluacion);
                    ctcLicencia[0].CasoUsoRH__c = recordIdEval;
                    contactFlag.add(ctcLicecnia);
                }
            }
            if(!contactFlag.isEmpty()){
                try{
                    update contactFlag;
                    actualizarPasswUser(contactFlag[0].id,password);    
                }catch(Exception e){
                    system.debug('Error al actualizar el contacto relacionado a la evaluación');  
                }
            }   
        } 
    }    
    
    //Metodo que funciona para agregar los documentos a cada uno de los correos asociados con la bandeja de entrada del candidato
    public static void agregarDocumentos(String casoUso,List<RH_EvaluacionCandidato__c> evalCandidato){
        List<ContentDocumentLink> contenEvalLink = new List<ContentDocumentLink> ();
        List <ContentVersion> allDocumentsEval = new List <ContentVersion>(); 
        ContentWorkspace[] docsEval = [SELECT Id FROM ContentWorkspace WHERE Name =: casoUso];
        if(docsEval.size()> 0){
            allDocumentsEval = [SELECT Title,ContentDocumentId FROM ContentVersion WHERE ContentDocument.ParentId = :docsEval[0].id];     
            
        }
        if(!allDocumentsEval.isEmpty()){
            for(RH_EvaluacionCandidato__c evalInsert : evalCandidato){
                for(ContentVersion linkDocuments : allDocumentsEval){
                    ContentDocumentLink cDe = new ContentDocumentLink();
                    cDe.ContentDocumentId = linkDocuments.ContentDocumentId;
                    cDe.LinkedEntityId = evalInsert.id; 
                    cDe.ShareType = 'V'; 
                    cDe.Visibility = 'AllUsers';
                    contenEvalLink.add(cDe);
                }
                
            }  
        } 
        
        if(!contenEvalLink.isEmpty()){
            try{
                insert contenEvalLink;  
            }catch(Exception e){
                system.debug('Error al asociar los documentos con los correos'+e.getMessage());  
                
            }
            
        }
        
    }
    
    //Metodo que funciona para conceder privilegios al usuario de leer la biblioteca de doucumentos para su evaluación correspondiente
    @future 
    public static void habilitarDocumentosGrupo(String CasoUsoEval, id usurioLicencia){
        Group g = [SELECT Id, DeveloperName from Group Where name =: CasoUsoEval];
        List<GroupMember> listGroupMember =new List<GroupMember>();         
        if(g.id != null && usurioLicencia != null){
            GroupMember GM = new GroupMember();
            GM.GroupId = g.id;
            GM.UserOrGroupId = usurioLicencia;
            try{
                insert GM; 
            }catch(Exception e){
                system.debug('Error al cambiar insertar los permisos para acceder a los documentos'+e.getMessage());  
                
            }
            
        }
        
    }
    
    // Metodo que funciona para actalizar el password que necesita el candidato para el acceso a la evaluación
    public static void actualizarPasswUser(id ContactoRH, String newPassword){
        User userRh = [Select id FROM User WHERE contactId =: ContactoRH];
        if(userRH != null){
            try{
                System.setPassword(userRh.id,newPassword);   
            }catch(Exception e){
                system.debug('Error al cambiar al password'+e.getMessage());   
                
            }
        }
    }    
}