/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar crear la colaboración de las cuentas con el 
    					proceso de Merge Accounts

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    01-Septiembre-2020 	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeShareAccountsBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_MergeShareAccountsBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_MergeShareAccountsBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_MergeShareClientes__c> scope){
        System.debug('EN TAM_MergeShareAccountsBch.');
		List<AccountShare> lAccountShare = new List<AccountShare>();
		List<TAM_MergeShareClientes__c> lMergShareAcc = new List<TAM_MergeShareClientes__c>();
		
		//Crea un objeto del tipo 
		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_MergeShareClientes__c objMergAccountShare : scope){
			if (objMergAccountShare.TAM_TipoObjeto__c == 'Clientes'){
				//Crea el objeto del tipo AccountShare      
		    	AccountShare objCliente = new AccountShare(
					AccountId = objMergAccountShare.TAM_Cliente__c,
					AccountAccessLevel = 'Read',
					CaseAccessLevel = 'None',
					OpportunityAccessLevel = 'None',
					UserOrGroupId = objMergAccountShare.TAM_IdUsuarioShare__c
				); 
				//Crea la colaboración para las cuetas a partir del Dealer
				lAccountShare.add(objCliente);
			}//Fin si objMergAccountShare.TAM_TipoObjeto__c = 'Clientes'
			//Crea el objeto del tipo TAM_MergeShareClientes__c y metelo a la lista de lMergShareAcc
			lMergShareAcc.add(new TAM_MergeShareClientes__c(
					id = objMergAccountShare.id,
					TAM_Procesado__c = true 
				)
			);
        }
       	System.debug('EN TAM_MergeShareAccountsBch sIdExterno: ' + lAccountShare);
       	System.debug('EN TAM_MergeShareAccountsBch lMergShareAcc: ' + lMergShareAcc);

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lAccountShare.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Saveresult> lDtbUpsRes = Database.insert(lAccountShare, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareAccountsBch Hubo un error a la hora de crear/Actualizar los registros en AccountShare ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareAccountsBch Los datos de la AccountShare se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lMergShareAcc.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Saveresult> lDtbUpsRes = Database.update(lMergShareAcc, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareAccountsBch Hubo un error a la hora de crear/Actualizar los registros en MergShareAcc ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareAccountsBch Los datos del MergShareAcc se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
				
		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_MergeShareAccountsBch.finish Hora: ' + DateTime.now());     
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_MergeShareAccountsMinutos);
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //5
		String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
    	System.debug('EN TAM_MergeShareAccountsBch.finish CRON_EXP: ' + CRON_EXP);

		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
		TAM_MergeShareAccountsSch objMergeShareAccSch = new TAM_MergeShareAccountsSch();
    	System.debug('EN TAM_MergeShareAccountsBch.finish objMergeShareAccSch: ' + objMergeShareAccSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest())//MergShareCtes
			System.schedule('TAM_MergeShareAccountsSch: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objMergeShareAccSch);
         
    } 
        
}