/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_Integraciones__c y toma los reg del dist 57205

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    28-Jul-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_SirenaInteracWebServicesch_cls implements Schedulable{
 
    global String sQuery {get;set;}
    global String slstIdLead {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_SirenaInteracWebServiceBch_cls.execute...');
        
        //String strCodDist = '57205';
        String strTipoIntegracion = 'Sirena';
        String strCodDistSirenaIntegra = String.valueOf(System.Label.TAM_CodDistSirenaIntegra);
                
        this.sQuery = 'Select t.id, t.TAM_UrlConsultaCtesDealer__c, t.TAM_TipoIntegreacion__c, t.TAM_Distribuidor__c, ';
        this.sQuery += ' t.TAM_ApiKey__c, t.Name From TAM_Integraciones__c t  ';
        this.sQuery += ' where t.TAM_Distribuidor__r.Codigo_Distribuidor__c = \'' + String.escapeSingleQuotes(strCodDistSirenaIntegra) + '\'';
        this.sQuery += ' And t.TAM_TipoIntegreacion__c = \'' + String.escapeSingleQuotes(strTipoIntegracion) + '\'';
        this.sQuery += ' Order by Name';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_SirenaInteracWebServiceBch_cls.execute sQuery: ' + sQuery);

        System.debug('EN TAM_SirenaInteracWebServiceBch_cls.execute slstIdLead: ' + slstIdLead);        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_SirenaInteracWebServiceBch_cls objSirenaInteracWebServiceBch = new TAM_SirenaInteracWebServiceBch_cls(sQuery, slstIdLead);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objSirenaInteracWebServiceBch, 1);
                     
    }
    
}