@isTest(SeeAllData=false)
public class TAM_BonoLealtadTestClass {
	@testSetup static void setup() {
        
        //Serie
        Serie__c objSerie = new Serie__c();
        objSerie.Name = 'SUPRA';
        insert objSerie;
        
        //Producto 2020
        Product2 objProduct = new Product2();
        objProduct.Name = '2351';
        objProduct.NombreVersion__c = 'Supra Color Especial Gris Mate';
        objProduct.Anio__c = '2020';
        objProduct.Family = 'Toyota';
        objProduct.IdExternoProducto__c = '23512020';
        objProduct.Serie__c = objSerie.Id;
        objProduct.IsActive = true;
        objProduct.ProductCode='23512020';
        insert objProduct;
        
        //Producto 2021
        Product2 objProduct2 = new Product2();
        objProduct2.Name = '2350';
        objProduct2.NombreVersion__c = 'SUPRA CLASICO AVX';
        objProduct2.Anio__c = '2021';
        objProduct2.Family = 'Toyota';
        objProduct2.IdExternoProducto__c = '23502021';
        objProduct2.Serie__c = objSerie.Id;
        objProduct2.IsActive = true;
        objProduct.ProductCode='23502021';
        insert objProduct2; 
    }
    
    @isTest
    public static void testAplicaATodos_SI(){
        
        Serie__c objSerie = [SELECT Id FROM Serie__c WHERE Name = 'SUPRA'];
        List<Product2> lstProducts = [SELECT Id, Serie__c, Anio__c FROM Product2];
        
        Test.startTest();
        //Series Bono Lealtad_Todos
        Series_Bono_Lealtad__c objBonoAll = new Series_Bono_Lealtad__c();
        objBonoAll.Serie__c = objSerie.Id;
        objBonoAll.Anio_Modelo__c = '2020';
        objBonoAll.Fecha_Inicio__c = Date.today();
        objBonoAll.Fecha_Fin__c = Date.today() + 10;
        objBonoAll.Bono_Distribuidor__c = 5000;
        objBonoAll.Bono_TMEX__c = 5000;
        objBonoAll.Bono_TFS__c = 0;
        objBonoAll.Comentarios__c = 'Indispensable presentar Tarjeta de Circulación o Factura Toyota.';
        objBonoAll.Aplica_Todos_los_Modelos__c = 'SI';
        insert objBonoAll;
        
        List<Series_Bono_Lealtad__c> lstBonoAll = [SELECT Id FROM Series_Bono_Lealtad__c];
        System.assertEquals(1, lstBonoAll.size());
        Test.stopTest();
    }
    
    @isTest
    public static void testAplicaATodos_NO(){
        
        Serie__c objSerie = [SELECT Id FROM Serie__c WHERE Name = 'SUPRA'];
        List<Product2> lstProducts = [SELECT Id, Serie__c, Anio__c FROM Product2];
        
        Test.startTest();
		//Series Bono Lealtad_NO Todos
        Series_Bono_Lealtad__c objBono = new Series_Bono_Lealtad__c();
        objBono.Serie__c = objSerie.Id;
        objBono.Anio_Modelo__c = '2021';
        objBono.Fecha_Inicio__c = Date.today();
        objBono.Fecha_Fin__c = Date.today() + 10;
        objBono.Bono_Distribuidor__c = 5000;
        objBono.Bono_TMEX__c = 5000;
        objBono.Bono_TFS__c = 0;
        objBono.Comentarios__c = 'Indispensable presentar Tarjeta de Circulación o Factura Toyota.';
        objBono.Aplica_Todos_los_Modelos__c = 'NO';
        insert objBono;
        
        List<Series_Bono_Lealtad__c> lstBono = [SELECT Id FROM Series_Bono_Lealtad__c];
        System.assertEquals(1, lstBono.size());
        Test.stopTest();
    }
    
    
}