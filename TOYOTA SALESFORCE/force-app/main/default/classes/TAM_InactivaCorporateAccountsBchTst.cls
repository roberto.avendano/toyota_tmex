/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_InactivaCorporateAccountsSch y TAM_InactivaCorporateAccountsBch.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    20-Octubre-2020   	Héctor Figueroa             Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_InactivaCorporateAccountsBchTst {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
		
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();
    static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

    static String sListaPreciosCutom = getCustomPriceBookList();
		
	static	Account clienteDealer = new Account();
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Rangos__c rango = new Rangos__c();
	static  Opportunity objOpp = new Opportunity();

    static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
    static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
    static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
    static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
    static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();
	
	@TestSetup static void loadData(){

        Date dtToday = Date.today();
        Date dtAyer = dtToday.addDays(-5);

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 1,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla			 
		);
		insert rangoFlotilla;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor,
            TAM_FechaVigenciaFin__c = dtAyer
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaFin__c = dtAyer
		);
		insert clienteMoral;
				
		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica,
			TAM_TIPOPERSONAPROSPECTO__C = 'Person física',
			TAM_FechaVigenciaFin__c = dtAyer
		);
		insert clienteFisico;

        Vehiculo__c v01 = new Vehiculo__c(
            Name='XXXXXXXXXX1',
            Id_Externo__c='XXXXXXXXXX1'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;

        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = clienteDealer.Id,
            Submitted_Date__c = Datetime.now().addDays(-2),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA'                      
        );
        insert m01;        
        
        Vehiculo__c objPaso = [Select id From Vehiculo__c Where ID =:v01.ID];
        objPaso.Ultimo_Movimiento__c = m01.id;
        update objPaso;
        
        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod;  
        CatalogoCentMod = catCenMod; 
        
        CatalogoCentralizadoModelos__c catCenMod2 = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '11',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod2;  
        CatalogoCentMod2 = catCenMod2;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        Product2 ProducStdPriceBook = new Product2(
                Name = '22060',
                Anio__c = '2020', 
                NombreVersion__c = 'LE MT',
                IdExternoProducto__c = '220602020',
                ProductCode = '220602020',
                Description= 'AVANZA-22060-2020-B79-10-LE AT', 
                RecordTypeId = sRectorTypePasoProductoUnidad,
                Family = 'Toyota',
                IsActive = true
        );
        insert ProducStdPriceBook;
        
        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id = standardPricebookId,
            UnitPrice = 0.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true,
            IdExterno__c = '220602020'
        );
        insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
        
        PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
            Pricebook2Id = customPB.id,
            UnitPrice = 1.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true
        );
        insert pbeCustomProceBookEntry;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id
        );
        insert solVentaFlotillaPrograma;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma2 = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'Cerrada',
            RecordTypeId = sRectorTypePasoSolProgramaCerrada,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaCierreSolicitud__c = Date.today()                      
        );
        insert solVentaFlotillaPrograma2;
        
        Date dtFechaActual = Date.today();
        Date dtFechaFin = dtFechaActual.addDays(2);
        String strAnioActual = String.valueOf(dtFechaActual.year());
        //Un objeto para TAM_CalendarioToyota__c
        TAM_CalendarioToyota__c objCalendarioToyota = new TAM_CalendarioToyota__c(
            Name = 'Julio 2021'
            , TAM_FechaInicio__c = Date.today()
            , TAM_FechaFin__c = dtFechaFin
            , TAM_Anio__c = strAnioActual
        );
        System.debug('EN TAM_CheckOutDetalleSolComHandlerOK3 objCalendarioToyota: ' + objCalendarioToyota);        
        insert objCalendarioToyota;

        //Un vin para el Checkout        
        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Intercambio__c = false,
            TAM_VIN__c = 'XXXXXXXXXX1'
        );
        insert objCheckOutDetSolComp;                         

        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp2 = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma2.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Intercambio__c = false,
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_EstatusDealerSolicitud__c = 'Cerrada'
        );
        insert objCheckOutDetSolComp2;
				             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;        
    }

    static testMethod void TAM_InactivaCorporateAccountsBchOK() {

		//Consulta los datos del cliente
		clientePruebaMoral = [Select Id, Name From Account Where RecordTypeId = :sRectorTypePasoPersonaMoral LIMIT 1];        
   		System.debug('EN TAM_InactivaCorporateAccountsBchOK clientePruebaMoral: ' + clientePruebaMoral);

		//Inicia las pruebas    	
        Test.startTest();

			String sQuery = 'Select Id, Name, TAM_EstatusCliente__c, TAM_FechaVigenciaFin__c, RecordType.Name, TAM_TIPOPERSONAPROSPECTO__C ';
			sQuery += ' From Account ';
			sQuery += ' Where TAM_FechaVigenciaFin__c < TODAY';
			sQuery += ' Order by Name ';
			System.debug('EN TAM_InactivaCorporateAccountsBchOK sQuery: ' + sQuery);
			
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	    	    
			TAM_InactivaCorporateAccountsBch objDesaCtesCorpBch = new TAM_InactivaCorporateAccountsBch(sQuery);
    	    
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objDesaCtesCorpBch, 5);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_InactivaCorporateAccountsSch objTAMInactivaCorporateAccountsSch = new TAM_InactivaCorporateAccountsSch();       		
       		System.schedule('Ejecuta_TAM_InactivaCorporateAccountsSch:', sch, objTAMInactivaCorporateAccountsSch);

            //System.assertEquals(1,lSolFlotProgExcepVinesCmWrp.size(),'1 reistro debe ser regresado');
            System.assertEquals(strYear, '2050-2051');
       		
        Test.stopTest();
        
    }

}