/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Controlador para el LWC TAM_VariacionProvisionesLWC
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		09/Junio/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
public without sharing class TAM_VariacionProvisionesController {

    @AuraEnabled(cacheable=true)
    public static List<TAM_VariacionProvisiones__c> getVariaciones( String recordId, String searchKey, String sortBy, String sortDirection) {
         final String STRING_FINAL_RVRSL = 'RVRSL';
        
        String query = 'SELECT  Id, Name,TAM_DetalleEstadoCuenta__c,TAM_Provisionar__c,TAM_DetalleProvision__c,TAM_IncentivoProvisionadoSinIva__c,TAM_MontoCobradoSinIVA__c,TAM_Variacion__c,TAM_Tipo__c,TAM_NombreDealer__c, ' + 
        'TAM_CodigoDealer__c,TAM_CodigoVenta__c, TAM_NombrePropietario__c,TAM_ApelllidoPropietario__c,TAM_FechaEnvio__c,TAM_ClaveMovimiento__c,TAM_FechaPago__c,TAM_FacturaCobroTMEX__c  '+ 
        'FROM TAM_VariacionProvisiones__c WHERE TAM_DetalleProvision__r.TAM_ProvisionIncentivos__r.Id =:recordId AND TAM_Variacion__c != 0 AND TAM_ClaveMovimiento__c !=: STRING_FINAL_RVRSL ';
        
        if ( searchKey != null && searchKey != '' ) {
            String key = '%' + searchKey + '%';
            query += ' AND Name LIKE :key';
        }
        if ( sortBy != null && sortDirection != null ) {
            query += ' ORDER BY ' + sortBy + ' ' + sortDirection;
        }
        return Database.query( query );
    }

    @AuraEnabled
    public static string updateRecord( Object data ) {
        List < TAM_VariacionProvisiones__c > recordsForUpdate = ( List < TAM_VariacionProvisiones__c > ) JSON.deserialize(
            JSON.serialize( data ),
            List < TAM_VariacionProvisiones__c >.class
        );
        try {
            update recordsForUpdate;
            return 'Success:  updated successfully';
        }
        catch (Exception e) {
            return 'The following exception has occurred: ' + e.getMessage();
        }
    }
}