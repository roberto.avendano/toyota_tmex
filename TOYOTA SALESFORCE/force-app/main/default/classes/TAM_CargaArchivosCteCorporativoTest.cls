/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladora para el componete  de TAM_CargaArchivosCteCorporativoCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    24-Agosto-2019       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CargaArchivosCteCorporativoTest {

	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtLeadClienteCreado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cliente Creado').getRecordTypeId();    

	static String VaRtAccRegPM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String VaRtAccRegPF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String VaRtCteCorp = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();		
	static	Account clientePrueba = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();
	static	ContentVersion clienteContVerPrueba = new ContentVersion();
	static	ContentVersion candidatoContVerPrueba = new ContentVersion();

	@TestSetup static void loadData(){
		
		Account cliente = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert cliente;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = cliente.Id
	    );
	    insert cont;  
		
        Lead cand = new Lead(
	        RecordTypeId = VaRtLeadClienteCreado,
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '224',
	        Status='Nuevo Lead',
	        LastName = 'Test3',
	        TAM_EnviarAutorizacion__c = true,
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Programa'      
        );
	    insert cand;

		Account clienteFisico = new Account(
			FirstName = 'TestAccount',
			LastName = 'TestAccountLastName',
			Codigo_Distribuidor__c = '570551',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = VaRtAccRegPF
		);
		insert clienteFisico;

        ContentVersion clienteCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data2'),
                IsMajorVersion = true
        );
        insert clienteCv;
		clienteContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :clienteCv.Id LIMIT 1];
		
        ContentVersion candCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data1'),
                IsMajorVersion = true
        );
        insert candCv;
        candidatoContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :candCv.Id LIMIT 1];
             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_CargaArchivosCteCorporativoOK() {

		//Consulta los datos del cliente
		candidatoPrueba = [Select Id, Name, TAM_EnviarAutorizacion__c, RecordTypeId, FWY_Tipo_de_persona__c, 
		TAM_TipoCandidato__c From Lead LIMIT 1];        
   		System.debug('EN TAM_CargaArchivosCteCorporativoCtrlTstOk candidatoPrueba: ' + candidatoPrueba);

		//Consulta los datos del cliente
		clientePruebaMoral = [Select Id, Name, RecordTypeId, RecordType.Name From Account Where RecordTypeId =:VaRtAccRegPM LIMIT 1];        
   		System.debug('EN TAM_CargaArchivosCtrlTstOk clientePruebaMoral: ' + clientePruebaMoral);
				
		//Consulta los datos del documento			
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId, Description FROM ContentDocument];
        documents.get(0).Description = 'Acta Constitutiva';
        update documents.get(0);
        
        //Crea la liga hacia el documento
        ContentDocumentLink contentlink=new ContentDocumentLink(
        	 LinkedEntityId = clientePruebaMoral.id,
             ShareType= 'V',
             ContentDocumentId = documents.get(0).Id,
             Visibility = 'AllUsers'
        );
        insert contentlink;
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo getFiles
       TAM_CargaArchivosCteCorporativoCtrl.getFiles(clientePruebaMoral.id, 'Acta Constitutiva');
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Acta Constitutiva', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Cedula Fiscal', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'PDF datos adicionales', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Orden de compra', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Identificación oficial', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Comprobante de domicilio', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo updFileDesc
       TAM_CargaArchivosCteCorporativoCtrl.updFileDesc(clientePruebaMoral.id, 'Documentación de soporte', documents.get(0).id);
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand(clientePruebaMoral.id);
              
       /*
       clientePruebaMoral.TAM_EnviarAutorizacion__c = false;
	   clientePruebaMoral.FWY_Tipo_de_persona__c = 'Person física';
	   clientePruebaMoral.TAM_TipoCandidato__c = 'Programa'; 
       update clientePruebaMoral;
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand(clientePruebaMoral.id);       

       clientePruebaMoral.TAM_EnviarAutorizacion__c = false;
	   clientePruebaMoral.FWY_Tipo_de_persona__c = 'Persona moral';
	   clientePruebaMoral.TAM_TipoCandidato__c = 'Programa'; 
       update clientePruebaMoral;
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand(clientePruebaMoral.id);
       
       clientePruebaMoral.TAM_EnviarAutorizacion__c = false;
	   clientePruebaMoral.FWY_Tipo_de_persona__c = 'Person física';
	   clientePruebaMoral.TAM_TipoCandidato__c = 'Flotilla'; 
       update clientePruebaMoral;
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand(clientePruebaMoral.id);
       
       clientePruebaMoral.TAM_EnviarAutorizacion__c = false;
	   clientePruebaMoral.FWY_Tipo_de_persona__c = 'Persona moral';
	   clientePruebaMoral.TAM_TipoCandidato__c = 'Flotilla'; 
       update clientePruebaMoral;
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand(clientePruebaMoral.id);       
       */
       
       
       //Llama la clase de TAM_CargaArchivosCteCorporativoCtrl y metodo consultDatosCand
       TAM_CargaArchivosCteCorporativoCtrl.deleteFiles(documents.get(0).id);
        
    }
}