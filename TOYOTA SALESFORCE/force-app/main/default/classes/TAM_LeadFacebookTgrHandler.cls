/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Trigger que contiene la logica para procesar los registros 
                        de TAM_LeadFacebookTgrHandler
                        
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    24-Junio-2021     Héctor Figueroa             Modificación
******************************************************************************* */

public without sharing class TAM_LeadFacebookTgrHandler extends TriggerHandler{

    private List<TAM_LeadFacebook__c> CteServList;
    private Map<Id,TAM_LeadFacebook__c> mapCtesServ;
    
    public TAM_LeadFacebookTgrHandler() {
        this.CteServList = Trigger.new;
        this.mapCtesServ = (Map<Id,TAM_LeadFacebook__c>)Trigger.newMap;
    }

    public override void beforeUpdate(){
        actualizaRegLeadFace(CteServList);
    }

    public static void actualizaRegLeadFace(List<TAM_LeadFacebook__c> lLeadFacebook){
        System.debug('EN creaRegLead.creaRegLead lLeadFacebook: ' + lLeadFacebook);
        
        List<Lead> lLeadsNuevos = new List<Lead>();
        Map<String, String> mapIdExtLeadError = new Map<String, String>();
        Map<String, String> mapIdExtLeadSuccess = new Map<String, String>();
        Map<String, TAM_LeadFacebook__c> mapLeadFacebookUps = new Map<String, TAM_LeadFacebook__c>();
        
        //Recorre la lista de reg de  TAM_LeadFacebook__c y crealpos en lead
        for (TAM_LeadFacebook__c objLeadFacebook : lLeadFacebook){
            
            String sOrigenDist = '';
            //Viene de Lunave Facebook    
            if (objLeadFacebook.TAM_OrigenProspectoFacebook__c == 'TMEX - Facebook Lunave')
                sOrigenDist = 'TMEX Facebook';
            if (objLeadFacebook.TAM_OrigenProspectoFacebook__c != null)            
                if (objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DISTRIBUIDOR - Facebook')) //'DISTRIBUIDOR - Facebook Irapuato - 57045'
                    sOrigenDist = 'DISTRIBUIDOR - Facebook';
            if (objLeadFacebook.TAM_OrigenProspectoFacebook__c != null)         
                if (objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DTM Facebook')) //'DISTRIBUIDOR - Facebook Irapuato - 57045'
                    sOrigenDist = 'DTM Facebook';
            System.debug('EN creaRegLead.creaRegLead sOrigenDist: ' + sOrigenDist);

            String sCodigoDist = objLeadFacebook.TAM_CodigoDistribuidorWeb__c;            
            //Si objLeadFacebook.TAM_OrigenProspectoFacebook__c == 'TMEX - Facebook Lunave'
            if (objLeadFacebook.TAM_OrigenProspectoFacebook__c != null){
                if (objLeadFacebook.TAM_OrigenProspectoFacebook__c == 'TMEX - Facebook Lunave' || objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DISTRIBUIDOR - Facebook')
                   || objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DTM Facebook') ){ //'DISTRIBUIDOR - Facebook Irapuato - 57045'
                    String sCodigoDistPas;
                    if (objLeadFacebook.TAM_OrigenProspectoFacebook__c == 'TMEX - Facebook Lunave' || objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DTM Facebook'))
                        sCodigoDistPas = objLeadFacebook.TAM_CodigoDistribuidorWeb__c; //AGUASCALIENTES - 57019
                    if (objLeadFacebook.TAM_OrigenProspectoFacebook__c.contains('DISTRIBUIDOR - Facebook'))
                        sCodigoDistPas = objLeadFacebook.TAM_OrigenProspectoFacebook__c; //DISTRIBUIDOR - Facebook Irapuato - 57045
                    sCodigoDist = sCodigoDistPas.contains('-') ? sCodigoDistPas.right(5) : null;
                }//Fin si l.TAM_OrigenProspectoFacebook__c == 'TMEX - Facebook Lunave'
            }//Fin si l.TAM_OrigenProspectoFacebook__c != null
            System.debug('EN creaRegLead.creaRegLead sCodigoDist: ' + sCodigoDist);
           
            String sTelefonoPaso = objLeadFacebook.TAM_Telefono__c;
            if (sTelefonoPaso != null){                
                //Ve si contiene algun caracter especial
                String strPhonePaso = sTelefonoPaso.replace(')', '').replace('(','').replace('-','');
                if (strPhonePaso.length() > 10)
                    sTelefonoPaso = strPhonePaso.right(10);
            }//Fin si sTelefonoPaso != null
            System.debug('EN creaRegLead.creaRegLead sTelefonoPaso: ' + sTelefonoPaso);
            
            //El objeto de paso para Lead
            Lead objLeadPaso = new Lead(
                    TAM_IdExternoFacebook__c = objLeadFacebook.TAM_IdExterno__c,
                    FirstName = objLeadFacebook.Name,
                    LastName = objLeadFacebook.TAM_ApellidoPaterno__c,
                    FWY_ApellidoMaterno__c = objLeadFacebook.TAM_ApellidoMaterno__c,
                    Phone = sTelefonoPaso,
                    Email = objLeadFacebook.TAM_CorreoElectronico__c,
                    LeadSource = sOrigenDist,
                    FWY_codigo_distribuidor__c = sCodigoDist, 
                    TAM_FormaContactoPreferida__c = objLeadFacebook.TAM_FormaContactoPreferida__c,
                    FWY_Vehiculo__c = objLeadFacebook.TAM_VehIculo__c,   
                    TAM_TipoCompra__c = objLeadFacebook.TAM_TipoCompra__c,   
                    TAM_OtrosDatosFacebook__c = objLeadFacebook.TAM_OtrosDatosFacebook__c,   
                    TAM_ConfirmaDatos__c = true,      
                    TAM_DatosComodinFacebook__c = objLeadFacebook.TAM_DatosComodinFacebook__c,               
                    TAM_Estado__c = objLeadFacebook.TAM_Estado__c,
                    TAM_Campania__c = objLeadFacebook.TAM_Campania__c
                    //TAM_OrigenProspectoFacebook__c = objLeadFacebook.TAM_OrigenProspectoFacebook__c,   
             );
              
            //Agrega el registro a la lista de lLeadsNuevos
            if (objLeadFacebook.TAM_EstatusProceso__c != 'Procesado')
                lLeadsNuevos.add(objLeadPaso);
        }//Fin del for para lLeadFacebook
        System.debug('EN creaRegLead.creaRegLead lLeadsNuevos: ' + lLeadsNuevos);
        
        //Ya tienes la lista de y actualiza los datos
        if (!lLeadsNuevos.isEmpty()){
            Integer intCntCons = 0;
            List<Database.SaveResult> ldtSvr = Database.insert(lLeadsNuevos, false);
            for(Database.SaveResult objSvr : ldtSvr){
                if (!objSvr.isSuccess())
                    mapIdExtLeadError.put(lLeadsNuevos[intCntCons].TAM_IdExternoFacebook__c, objSvr.getErrors()[0].getMessage());
                if (objSvr.isSuccess())
                    mapIdExtLeadSuccess.put(lLeadsNuevos[intCntCons].TAM_IdExternoFacebook__c, lLeadsNuevos[intCntCons].TAM_IdExternoFacebook__c);
                intCntCons++;
            }//Fin del for para ldtSvr
        }//Fin si !lUsuariosSeguimientoLeadUps.isEmpty()        
        System.debug('EN creaRegLead.creaRegLead mapIdExtLeadError: ' + mapIdExtLeadError.KeySet());
        System.debug('EN creaRegLead.creaRegLead mapIdExtLeadError: ' + mapIdExtLeadError.Values());
        System.debug('EN creaRegLead.creaRegLead mapIdExtLeadSuccess: ' + mapIdExtLeadSuccess.KeySet());
        System.debug('EN creaRegLead.creaRegLead mapIdExtLeadSuccess: ' + mapIdExtLeadSuccess.Values());
        
        //Recorre la lista de reg de  TAM_LeadFacebook__c y crealpos en lead
        for (TAM_LeadFacebook__c objLeadFacebook : lLeadFacebook){
            //Crear mapLeadFacebookUps
            if (mapIdExtLeadError.containsKey(objLeadFacebook.TAM_IdExterno__c)){
                objLeadFacebook.TAM_EstatusProceso__c = 'Error';
                objLeadFacebook.TAM_DetalleError__c = mapIdExtLeadError.get(objLeadFacebook.TAM_IdExterno__c);
            }//Fin si mapIdExtLeadError.containsKey(objLeadFacebook.TAM_IdExterno__c)
            if (mapIdExtLeadSuccess.containsKey(objLeadFacebook.TAM_IdExterno__c))
                objLeadFacebook.TAM_EstatusProceso__c = 'Procesado';
            System.debug('EN creaRegLead.creaRegLead objLeadFacebook: ' + objLeadFacebook);
        }//Fin del for para TAM_LeadFacebook__c
        
    }//Fin si creaRegLead(List<TAM_LeadFacebook__c> lLeadFacebook)
    
}