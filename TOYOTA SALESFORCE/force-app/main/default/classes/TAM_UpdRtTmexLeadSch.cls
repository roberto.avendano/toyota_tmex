/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_UpdRtTmexLeadSch y TAM_UpdRtTmexLeadBch.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    03-Septiembre-2020   Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_UpdRtTmexLeadSch implements Schedulable{

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeShareAccountsSch.execute...');

        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_UpdLeadRecodTypeNoReg);		
		List<String> lOrigenLead = new List<String>{'TMEX Cotizador web', 'TMEX Micrositio', 'TMEX Facebook'};
		List<String> lRTOrigenLead = new List<String>{'Tráfico Piso', 'Retail Nuevos'};
		
		String sLOrigenLead = '(';
		//Recorre la lista de lOrigenLead
		for (String sOrigenLeadPaso : lOrigenLead){
			sLOrigenLead += '\'' + sOrigenLeadPaso + '\',';
		}
		sLOrigenLead = sLOrigenLead.substring(0, sLOrigenLead.lastIndexOf(','));
		sLOrigenLead += ')';
		System.debug('EN TAM_UpdRtTmexLeadSch.execute sLOrigenLead: ' + sLOrigenLead);
		
		String sLRTOrigenLead = '(';
		//Recorre la lista de lOrigenLead
		for (String sRTOrigenLeadPaso : lRTOrigenLead){
			sLRTOrigenLead += '\'' + sRTOrigenLeadPaso + '\',';
		}
		sLRTOrigenLead = sLRTOrigenLead.substring(0, sLRTOrigenLead.lastIndexOf(','));
		sLRTOrigenLead += ')';
		System.debug('EN TAM_UpdRtTmexLeadSch.execute sLRTOrigenLead: ' + sLRTOrigenLead);
		
		//Crea la consulta para los Leads
		this.sQuery = 'Select Id, LeadSource, RecordTypeId ';
		this.sQuery += ' From Lead ';
		this.sQuery += ' Where LeadSource IN ' + sLOrigenLead;
		this.sQuery += ' And RecordType.Name IN ' + sLRTOrigenLead;
		this.sQuery += ' Order by LeadSource, CreatedDate DESC ';
		if (Test.isRunningTest())
			this.sQuery += ' Limit ' + (intMergeShareAccountsMinutos);
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeShareAccountsSch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_UpdRtTmexLeadBch objUpdRtTmexLeadBch = new TAM_UpdRtTmexLeadBch(sQuery);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objUpdRtTmexLeadBch, 25);
			    	 
    }

    
}