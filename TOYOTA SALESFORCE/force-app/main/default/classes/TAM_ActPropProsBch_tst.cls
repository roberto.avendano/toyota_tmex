/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto Lead
                        y actueliza el nombre del propietario TAM_ActPropProsSch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    19-Feb-2021          Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActPropProsBch_tst {

    static testMethod void TAM_ActPropProsBchOK() {
        ParametrosConfiguracionToyota__c config = new ParametrosConfiguracionToyota__c();
        config.name = 'PlantillaEmailNotificaOwner';
        config.Valor__c = 'Plantilla de candidato nuevo';
        config.consecutivo__c = 754;
        config.DescripcionParametro__c = 'Especifica los parámetros para la búsqueda de la plantilla de email '
            + 'que será utilizada para notificar a los usuarios cuando se les ha sido asignado un lead';
        insert config;
        
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '570301');
            system.debug('distribuidora metodo 1'+ac);
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id);
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                firstname='Testingproductfs', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', 
                username='testOwnertfsproduc@noemail.com.test'
            );
            
            insert user;
            
            RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_RetailNuevos' and SobjectType = 'Lead'];      
            Lead l = new Lead();
            l.RecordTypeId = rectype.id;
            l.FirstName = 'Test';
            l.FWY_Intencion_de_compra__c = 'Este mes';  
            l.Email = 'as@a.com';
            l.phone = '225';
            l.Status='Nuevo Lead';
            l.LastName = 'Test1';
            l.FWY_codigo_distribuidor__c = '57031';
            l.TAM_PropietarioAnterior__c = user.id;
            l.TAM_NuevoPropietario__c = 'Testingproductfs Testingproductfs';
            insert l;
            
            String sQuery = 'Select Id, Owner.Name, TAM_PropietarioAnterior__c, TAM_PropietarioAnterior__r.Name, ';
            sQuery += ' TAM_NuevoPropietario__c, FWY_codigo_distribuidor__c From Lead ';
            sQuery += ' Where TAM_NomPropKban__c = null OR TAM_CambioPropietario__c = true';
            sQuery += ' Limit 1';           
            System.debug('EN TAM_ActPropProsBch_tst.execute sQuery: ' + sQuery);
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActPropProsBch_cls objActPropProsBch = new TAM_ActPropProsBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActPropProsBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_ActPropProsSch_cls ActFechaDDChkOutSch_Jobh = new TAM_ActPropProsSch_cls();
            System.schedule('TAM_ActPropProsSch_tst', sch, ActFechaDDChkOutSch_Jobh);
            
        Test.stopTest();
        
    }
}