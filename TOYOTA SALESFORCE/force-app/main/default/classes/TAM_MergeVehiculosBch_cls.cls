/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_MergeVehiculosBch_cls y
    					actualizar el cliente en TAM_ClienteVehiculo__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Agosto-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_MergeVehiculosBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sClienteFinal;
    
    //Un constructor por default
    global TAM_MergeVehiculosBch_cls(string query, String sClienteFinal){
        this.query = query;
        this.sClienteFinal = sClienteFinal;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_MergeVehiculosBch_cls.start query: ' + this.query + ' sClienteFinal: ' + this.sClienteFinal );
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_ClienteVehiculo__c> scope){
        System.debug('EN TAM_MergeVehiculosBch_cls.');
		List<TAM_ClienteVehiculo__c> lOppUps = new List<TAM_ClienteVehiculo__c>();

		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_ClienteVehiculo__c objOpp : scope){
        	TAM_ClienteVehiculo__c OppPaso = new TAM_ClienteVehiculo__c(id = objOpp.id, TAM_Cliente__c = this.sClienteFinal,
        		TAM_MergeCuentaAnterior__c = objOpp.TAM_Cliente__c); 
        	lOppUps.add(OppPaso);
        	System.debug('EN TAM_MergeVehiculosBch_cls OppPaso: ' + objOpp);
        }

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lOppUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lOppUps, TAM_ClienteVehiculo__c.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeVehiculosBch_cls Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeVehiculosBch_cls Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
				
		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_MergeVehiculosBch_cls.finish Hora: ' + DateTime.now());      
    } 

}