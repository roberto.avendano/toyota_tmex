@isTest
private class SolicitudPartesRobadasControllerTest {
    
    @testSetup
    static void getSolicitud(){
        //Creacion de objetos para Test

         Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
         Id standardPricebookId = Test.getStandardPricebookId();
         //Creacion de un usuario
         User u = new User(
                Alias = 'ConsTSM', 
                Email='ConsultorTSM@testorgtoyota.com',
                LastName='TSM',
                ProfileId = p.Id,
                UserName='standarduser@testorgtoyota.com',
                EmailEncodingKey='UTF-8',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Los_Angeles'
                );
            insert u; 
        
        //Creacion del Document (Imagen de toyota)
         Document testDoc = new Document(
                Name='Toyota.jpg',
                DeveloperName='Toyota_jpg',
                FolderId= u.Id      
                );
            insert testDoc;

                //Creacion del Document (Imagen de toyota)
         Document testDoc2 = new Document(
                Name='ToyotaFooterPR.jpg',
                DeveloperName='ToyotaFooterPR',
                FolderId= u.Id      
                );
        insert testDoc2;

        //Creación de los parametros de configuracion.
        ParametrosConfiguracionToyota__c pc = new ParametrosConfiguracionToyota__c(
            Name= 'FormatoSolicitudesPRLeyenda',
            //DescripcionParametro__c = 'Leyenda formato',
            Consecutivo__c = 101,
            Valor__c= 'He leido el acuerdo y acepto de conformidad'
        );

        insert pc;

        ParametrosConfiguracionToyota__c pcRex = new ParametrosConfiguracionToyota__c(
            Name= 'FormatoSolicitudesPRLeyendaRegExp',
            DescripcionParametro__c = '',
            Consecutivo__c = 102
            //Valor__c= 'NombreDealer'
        );

        insert pcRex;
            
            //Creacion de Account para el pedido
        Account acct = new Account(
                Name = 'Toyota Aeropuerto',
                DenominacionSocial__c = 'Toyota Aeropuerto S.A de C.V'
                );
            insert acct;

            //System.debug(acct.Id);

            //Declaracion del  date para pedido
        Date myDate = Date.newInstance(2016, 12, 21);

        
        String idExterno='12345678910';
            //Creacion del vehiculo para el numero de serie en pedido
        Vehiculo__c vhl = new Vehiculo__c(
                Name=idExterno,
                Id_Externo__c =idExterno 
                );
                insert vhl;


        Pricebook2 pb2Custom = new Pricebook2(
                IdExternoListaPrecios__c=idExterno,
                Name='Test',
                IsActive=true
                );
            insert pb2Custom;

        
                //Relaciones
        
        Product2 producto = new Product2(
                IdExternoProducto__c=idExterno,
                Name='Producto Prueba',
                Description= 'Test product2',
                CantidadMaxima__c=5,
                PartesRobadas__c= false
                );
            insert producto;

        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id=standardPricebookId,
            UnitPrice=0.0,
            Product2Id=producto.Id,            
            IsActive=true,
            IdExterno__c='Test1'
            );
        insert pbeStandard;

        PricebookEntry pbeCustom = new PricebookEntry(
            Pricebook2Id=pb2Custom.Id,
            UnitPrice=100,
            Product2Id=producto.Id,
            UseStandardPrice=false,
            IsActive=true,
            IdExterno__c='Test2'
            );
        insert pbeCustom;


            //Creacion del pedido
        Order pedido = new Order(
                Status='En proceso',
                EffectiveDate= Date.today(),
                AccountId = acct.Id,
                Vehiculo__c=vhl.Id,
                IFELicenciaDealer__c=true,
                TarjetaCirculacionDealer__c=true,
                FormatoSolicitudPartesDealer__c=true,
                CopiaFacturaOriginalDealer__c=true,
                Pricebook2Id=pb2Custom.Id
                );
            insert pedido;
    
        
        OrderItem ordItem = new OrderItem(
                PricebookEntryId=pbeCustom.Id,
                OrderId=pedido.Id,
                UnitPrice=pbeCustom.UnitPrice,
                Quantity=3
            ); 
            insert ordItem;
    }

    
    @isTest static void SolicitudPartesRobadasControllerTest() {
        Order orn=[SELECT Id FROM Order LIMIT 1];
        Test.startTest();
        //String pedido='801q0000000EU7W';
        System.currentPageReference().getParameters().put('id', orn.Id);
        SolicitudPartesRobadasController sprc= new SolicitudPartesRobadasController(); 
        //sprc.getOrder(pedido);
        Order orderO = sprc.order;  
        Document docu=sprc.logo;
        Document foot=sprc.footer;
        ParametrosConfiguracionToyota__c pc = sprc.pc;
        ParametrosConfiguracionToyota__c pcregExp = sprc.pcregExp;

        System.debug(docu.Id);
        System.debug(orderO.Vehiculo__r.Ultimo_Movimiento__r.VIN__r.Modelo_b__r.Descr_Modelo__c);
        sprc.getLeyenda();
        Test.stopTest();
    }
    
}