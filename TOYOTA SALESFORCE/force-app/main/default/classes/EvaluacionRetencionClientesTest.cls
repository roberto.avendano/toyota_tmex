@isTest
private class EvaluacionRetencionClientesTest {

	    private static String tipoEvaluacion = 'RetencionClientes';

        public static Evaluaciones_Dealer__c getTestDealer(){
             
            String tipoEvaluacionED = 'Evaluacion'+tipoEvaluacion;
            String tipoEvaluacionSecc = 'Secciones'+tipoEvaluacion;
            
            RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
            RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
            RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
            Profile p = [SELECT Id FROM Profile WHERE Name='Consultor Retención Clientes'];
                
                User u = new User(
                        Alias = 'ConsRC', 
                        Email='ConsultorRC@testorgtoyota.com',
                        LastName='RetencionC',
                        ProfileId = p.Id,
                        UserName='standarduser@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert u;
                User u2 = new User(
                        Alias = 'ConsRC2', 
                        Email='ConsultorRC2@testorgtoyota.com',
                        LastName='RetencionC2',
                        ProfileId = p.Id,
                        UserName='standarduser2@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert u2;

/*                User userTSM = new User(
                        Alias = 'ConsTSM', 
                        Email='ConsultorTSM@testorgtoyota.com',
                        LastName='TSM2',
                        ProfileId = '00ei0000001RXinAAG',
                        UserName='TSMuser2@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert userTSM;*/

                Seccion__c seccion01 = new Seccion__c(
                        Name = 'seccion01',
                        RecordTypeId = recordTypeSec.Id,
                        Activo__c = true
                );
                insert seccion01;
                Seccion__c seccion02 = new Seccion__c(
                        Name = 'Secccion02',
                        RecordTypeId = recordTypeSec.Id,
                        Activo__c = true
                );
                insert seccion02;

                Pregunta__c pregunta01 = new Pregunta__c(
                        Name = 'KP-5',
                        Reactivo__c = 'El concesionario mide de manera mensual la tasa de los clientes contactados para la bienvenida a servicio?',
                        Seccion_Toyota_Mexico__c = seccion01.Id,
                        Activo__c = true
                );
                insert pregunta01;

                Pregunta__c pregunta02 = new Pregunta__c(
                        Name = 'KP-1',
                        Reactivo__c = '¿El concesionario mide de manera mensual la tasa de Retención de Clientes de los autos propios?',
                        Seccion_Toyota_Mexico__c = seccion02.Id,
                        Activo__c = true
                );
                insert pregunta02;

                Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
                        Name = 'OE',
                        Orden__c = '1',
                        Clave_Preg__c = pregunta01.Id
                );
                insert oe01;

                Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
                        Name = 'OE',
                        Orden__c = '1',
                        Clave_Preg__c = pregunta02.Id
                );
                insert oe02;

                Account dealer = new Account(
                        RecordTypeId = recordTypeDealer.Id,
                        Name = 'Test Record'
                );
                insert dealer;

                Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
                        RecordTypeId = recordTypeED.Id,
                        Consultor_TSM_Titular__c = u2.Id,
                        Nombre_Dealer__c = dealer.Id                    
                );
                insert evalDealer;

                Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
                    Evaluacion_Dealer__c = evalDealer.Id,
                    Usuario__c = u.Id,
                    Seccion_Toyota_Mexico__c = seccion01.Id
                );
                insert rsc01;
                    Relacion_Seccion_Consultor__c rsc02 = new Relacion_Seccion_Consultor__c(
                    Evaluacion_Dealer__c = evalDealer.Id,
                    Usuario__c = u2.Id,
                    Seccion_Toyota_Mexico__c = seccion02.Id
                );
                insert rsc02;
                update evalDealer;

              Respuestas_Preguntas_TSM__c respPreg = [SELECT Id, Evaluacion_Dealer__c, Pregunta_Relacionada__c, (SELECT Id FROM Respuestas_ObjetosTSM_del__r) FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c =:evalDealer.Id limit 1];
                Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
                    Celula_Area__c='DO',
                    Observaciones__c='Observaciones__c',
                    Condicion_Observada__c='Condicion_Observada__c',
                    Referencia__c='TSM',
                    Cuenta__c=evalDealer.Nombre_Dealer__c,
                    Respuestas_Preguntas_TSM__c=respPreg.Id
                );
                insert api01;
                
                Respuestas_ObjetosTSM__c ro01 = new Respuestas_ObjetosTSM__c(
                                Objeto_Evaluacion_Relacionado__c=oe01.Id, 
                                Pregunta_Relacionada_del__c=respPreg.Pregunta_Relacionada__c, 
                                Respuestas_Preguntas_TSM_del__c=respPreg.Id
                );
                insert ro01;

                Actividad_Plan_Integral__c api02 = new Actividad_Plan_Integral__c(
                    Celula_Area__c='DO',
                    Observaciones__c='Observaciones__c',
                    Condicion_Observada__c='Condicion_Observada__c',
                    Referencia__c='TSM',
                    Cuenta__c=evalDealer.Nombre_Dealer__c,
                    Respuestas_Preguntas_TSM__c=respPreg.Id,
                    RespuestasObjetosTSM__c=ro01.Id
                );
                insert api02;
                
            	String pdfContent = 'This is a test string';
            	Attachment adjunto= new Attachment(
                	Name='Adjunto1',
                   	Body=blob.toPDF(pdfContent),
                    ParentId= api02.Id
                );
            	insert adjunto;

                return evalDealer;
        }

@isTest
	static void itShould(){
		Evaluaciones_Dealer__c evalDealer = EvaluacionRetencionClientesTest.getTestDealer();
		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(evalDealer);
			EvaluacionRetencionClientesController evRetencion = new EvaluacionRetencionClientesController(stdController);
			evRetencion.Guardar();
			evRetencion.Previous();
			evRetencion.Next();
			evRetencion.getItems();
			evRetencion.getPrioridades();
			evRetencion.getAreas();
			evRetencion.getReferencia();
			evRetencion.getPerfilTSMId();
			evRetencion.getValidateAtt();

            evalDealer.Nombre_Dealer__c=null;
            evalDealer.Consultor_TSM_Titular__c=null;
            evalDealer.Id=null;

            evRetencion.Guardar();

            User userRetencion=[SELECT Id, Name, LastName, ProfileId from User WHERE LastName='RetencionC'];
            //userRetencion.ProfileId='00ei0000001RXinAAG';
            //update userRetencion;
            System.runAs(userRetencion){                
                evRetencion = new EvaluacionRetencionClientesController(stdController);
                evRetencion.Guardar();
            }

/*            User tsm2 = [SELECT Id, Name, LastName,  ProfileId from User WHERE LastName='TSM2'];            
            System.runAs(tsm2){
                 evRetencion = new EvaluacionRetencionClientesController(stdController);
            }*/

		Test.stopTest();
	}	




}