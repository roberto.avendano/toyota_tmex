@isTest
public class TAM_SolicitudInternaTriggerTest {
    
    @testSetup static void setupData() {
        
        Date inicio = date.parse('01/04/2018');
        Date fin = date.parse('30/04/2018');
        
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND Name='Distribuidor'];
        
        Account acc = new Account(Name='TMEX ACCOUNT TEST',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        //Se crea el grupo con el nombre del dealer 
        Group grp = new Group ();
        grp.name = 'TMEX ACCOUNT TEST';
        grp.DeveloperName = 'TMEX_Account_Test';
        insert grp;
        
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='TMEX'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Estatus__c = 'Activo',
                                Departamento__c = 'Human Resources',
                                Codigo__c = '456RTY',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                //Permiso_de_Privacidad__c = false,
                                RecordTypeId = conRT.Id);
        insert c;
        
        
        ColorExterno__c ce = new ColorExterno__c(Name = 'MAGNETIC GRAY MET',
                                                 CodigoColor__c='1G3',descripcion__c = '1G3');
        insert ce;
        
        ColorInterno__c ci = new ColorInterno__c(Name ='MID BLUE BLACK',
                                                 CodigoColor__c	='70',descripcion__c = '70');
        insert ci;
        //CREACION DE SERIES
        Serie__c s = new Serie__c(
            Name = 'C-HR',
            Marca__c='Toyota',
            CdigoModeloFinanzas__c='230418',
            Id_Externo_Serie__c='2304'
        );
        insert s;
        
        //CREACION DE PRODUCTOS          
        Id stdPBId = Test.getStandardPricebookId();
        Product2 p1 = new Product2(
            Serie__c = s.Id,
            NombreVersion__c = '2304 C-HR A',
            Name = '2304 C-HR A',
            IsActive = true,
            IdExternoProducto__c= '2304C-HRA'
        );
        insert p1;
        
        ColorInternoModelo__c cim = new ColorInternoModelo__c(
            Name = ci.Name, 
            Modelo__c = p1.Id, 
            IDExterno__c = 'TESTCOLORINTERNOMODELO01',
            CodigoColorInterno__c = ci.CodigoColor__c, 
            AgnioModelo__c = '2018'
        );
        insert cim;
        
        ColorExternoModelo__c cem = new ColorExternoModelo__c(
            Name = ce.Name, 
            ColorInternoModelo__c = cim.Id,
            Modelo__c = cim.Modelo__c, 
            ID_Externo__c = 'TESTCOLOREXTERNOMODELO01' ,
            CodigoColorExterno__c = ce.CodigoColor__c ,
            AnioModelo__c = '2018',
            Tipo__c = 'Estandar'
        );
        insert cem;
        
        RecordType sivRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='SolicitudInternaVehiculos__c' 
                            AND DeveloperName='PoolAsignacion'];
        
        List<SolicitudInternaVehiculos__c> sivs = new List<SolicitudInternaVehiculos__c>();
        
        SolicitudInternaVehiculos__c siv1 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             Distribuidor_recepcion__c = acc.id,
                                                                             ColorInternoOpcion1__c = cim.id,
                                                                             ColorExternoOpcion1__c = cem.id,
                                                                             RecordTypeId = sivRT.Id,
                                                                             TipoSolicitud__c = 'Asignación',
                                                                             Estatus__c='Nuevo',
                                                                             AnioModelo__c = '2018',
                                                                             Modelo__c = p1.Id);
        insert siv1;
        
        
    }
    
    @isTest
    static void test_one(){
        Id recordTypeDistribuidor =  Schema.SObjectType.SolicitudInternaVehiculos__c.getRecordTypeInfosByDeveloperName().get('PoolEntrega').getRecordTypeId();
        
        SolicitudInternaVehiculos__c solInternaVehiculo = [Select id,RecordTypeId,TipoSolicitud__c,Estatus__c
                                                           From SolicitudInternaVehiculos__c WHERE  TipoSolicitud__c = 'Asignación'];
        
        
        SolInternaVehiculo.Estatus__c =  'En Proceso de Entrega';
        SolInternaVehiculo.RecordTypeId = recordTypeDistribuidor;
        update solInternaVehiculo;
        
    }
    
}