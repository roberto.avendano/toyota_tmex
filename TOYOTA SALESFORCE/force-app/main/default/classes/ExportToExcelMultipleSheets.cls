public with sharing class ExportToExcelMultipleSheets {
    public List<EstadoCuentaController.wrapperEstadoCta> detalleEdoCtaPorCobrar{get;set;}
    public List<EstadoCuentaController.wrapperEstadoCta> detalleEdoCtaFacturadosTMEX{get;set;}
    public List<EstadoCuentaController.wrapperEstadoCta> detalleEdoCtaPendientesCobro{get;set;}
    public TAM_EstadoCuenta__c estadoCuentaGralInfo{get;set;}
    
    public ExportToExcelMultipleSheets(){
        String recordId  = (String)ApexPages.currentPage().getParameters().get('id');
        String tipoProvision = (String)ApexPages.currentPage().getParameters().get('provision'); 
        String UserId = UserInfo.getUserId();
        system.debug('Provision'+recordId);
        if(tipoProvision == 'Retail'){
            detalleEdoCtaPorCobrar = EstadoCuentaController.getProvisionByDealer(recordId,'porCobrar','Retail',UserId);
            detalleEdoCtaFacturadosTMEX = EstadoCuentaController.getProvisionByDealer(recordId,'enviadosFacturar','Retail',UserId);
            detalleEdoCtaPendientesCobro = EstadoCuentaController.getProvisionByDealer(recordId,'noCobrados','Retail',UserId);
            estadoCuentaGralInfo  = EstadoCuentaController.getInfoGralEdoCta(recordId);
        }
        
        if(tipoProvision == 'VentaCorporativa'){
            detalleEdoCtaPorCobrar = EstadoCuentaController.getProvisionByDealer(recordId,'porCobrar','VentaCorporativa',UserId);
            detalleEdoCtaFacturadosTMEX = EstadoCuentaController.getProvisionByDealer(recordId,'enviadosFacturar','VentaCorporativa',UserId);
            detalleEdoCtaPendientesCobro = EstadoCuentaController.getProvisionByDealer(recordId,'noCobrados','VentaCorporativa',UserId);
            estadoCuentaGralInfo  = EstadoCuentaController.getInfoGralEdoCta(recordId);
        }
        
        if(tipoProvision == 'Bono'){
            detalleEdoCtaPorCobrar = EstadoCuentaController.getProvisionByDealer(recordId,'porCobrar','Bono',UserId);
            detalleEdoCtaFacturadosTMEX = EstadoCuentaController.getProvisionByDealer(recordId,'enviadosFacturar','Bono',UserId);
            detalleEdoCtaPendientesCobro = EstadoCuentaController.getProvisionByDealer(recordId,'noCobrados','Bono',UserId);
            estadoCuentaGralInfo  = EstadoCuentaController.getInfoGralEdoCta(recordId);
        }
        
        if(tipoProvision == 'AutoDemo'){
            detalleEdoCtaPorCobrar = EstadoCuentaController.getProvisionByDealer(recordId,'porCobrar','AutoDemo',UserId);
            detalleEdoCtaFacturadosTMEX = EstadoCuentaController.getProvisionByDealer(recordId,'enviadosFacturar','AutoDemo',UserId);
            detalleEdoCtaPendientesCobro = EstadoCuentaController.getProvisionByDealer(recordId,'noCobrados','AutoDemo',UserId);
            estadoCuentaGralInfo  = EstadoCuentaController.getInfoGralEdoCta(recordId);
        }
        
    }
       
    /*
    public class wrapperEstadoCta {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String nombrePropietario {get; set;}
        @AuraEnabled public String apellidoPropietario {get; set;}
        @AuraEnabled public String IdDetalleProvision {get; set;}
        @AuraEnabled public String IdDetalleEdoCta {get; set;}
        @AuraEnabled public String IdEstadoCuenta {get; set;}
        @AuraEnabled public String VIN {get; set;}
        @AuraEnabled public String serie {get; set;}
        @AuraEnabled public String codigoModelo {get; set;}
        @AuraEnabled public String anioModelo {get; set;}
        @AuraEnabled public decimal incentivoProvisionadoDealer {get; set;}
        @AuraEnabled public decimal incentivoSolicitadoDealer {get; set;}
        @AuraEnabled public decimal incentivoProvisionadoTMEX {get; set;}
        @AuraEnabled public decimal incentivorEfectivoTotal {get; set;}
        @AuraEnabled public decimal incentivoParteTMEX {get; set;}
        @AuraEnabled public String nombreProvision {get; set;}
        @AuraEnabled public String idProvision {get; set;}
        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String nombreDealer {get; set;}        
        @AuraEnabled public String facturaVenta {get; set;}
        @AuraEnabled public String IdfacturaVenta {get; set;}
        @AuraEnabled public String comentarioDealer {get; set;}
        @AuraEnabled public Boolean VINEntregado {get; set;} 
        @AuraEnabled public Boolean EdoCtaCerrado {get; set;} 
        @AuraEnabled public String estatusVINEdoCta {get; set;}
        @AuraEnabled public String tipoTransaction {get; set;}
        @AuraEnabled public String fechaVenta {get; set;}
        @AuraEnabled public String mesPeriodoVenta {get; set;}
        @AuraEnabled public String anioPeriodoVenta {get; set;}
        @AuraEnabled public String facturaCobroTMEX {get; set;}
        @AuraEnabled public String IdfacturaCobroTMEX {get; set;}
        @AuraEnabled public String recordIdFacturaCobroTMEX {get; set;}
        @AuraEnabled public String receptorFacturaCargada {get; set;}
        @AuraEnabled public String idPoliticaIncentivo {get; set;}
        @AuraEnabled public String rangoPrograma {get; set;}
        @AuraEnabled public decimal porcentajeParticipacionTMEX {get; set;} 
        @AuraEnabled public decimal porcentajeDescuentoTotal {get; set;} 
        @AuraEnabled public decimal subTotalFacturacion {get; set;} 
        @AuraEnabled public Boolean ventaTFS {get; set;}
        @AuraEnabled public Boolean enviarFacturar {get; set;}
        @AuraEnabled public Boolean transferirRetail {get; set;}
        @AuraEnabled public Boolean validarTransferencia {get; set;}
        @AuraEnabled public String politicaIntercambioRetail {get; set;}
        @AuraEnabled public String estatusRegistroFinanzas {get; set;}
        @AuraEnabled public String TAMDocumentoId {get; set;}
        @AuraEnabled public String TAMNombreDocumento {get; set;}
        @AuraEnabled public Date fechaTentativaPago {get; set;}
        @AuraEnabled public String comentarioFinanzasTMEX {get; set;}
        @AuraEnabled public String estatusFinanzasTMEX {get; set;}
        
    }*/
}