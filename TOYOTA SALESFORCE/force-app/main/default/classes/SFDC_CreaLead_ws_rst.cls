/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Publicación del WS con tecnologia REST para la creación de Prospectos

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    10-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

@RestResource(urlMapping='/SFDC_CreaLead_ws_rst/*')
global with sharing class SFDC_CreaLead_ws_rst {

    @HttpPost  
    global static void SFDC_CreaLeadRst(){
        RestRequest req = RestContext.request;
        Blob bBody = req.requestBody;
        String sBody =  bBody.toString();

        System.debug('EN SFDC_CreaLead_ws_rst sBody: ' + sBody);        
        //Manda llamar la clase que se llama SFDC_UpdDatosInvestigacion_ws_rst y el metodo updInvestigaciones
        String sResUpdInves = SFDC_CreaLead_ctrl_rst.getCreaLead(sBody);
        System.debug('EN SFDC_CreaLead_ws_rst sResUpdInves: ' + sResUpdInves);

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(sResUpdInves);        
        
    }
    
}