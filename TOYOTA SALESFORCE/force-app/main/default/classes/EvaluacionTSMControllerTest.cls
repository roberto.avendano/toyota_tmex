@isTest
private class EvaluacionTSMControllerTest{

        private static String tipoEvaluacion = 'Kodawari';

        public static Evaluaciones_Dealer__c getTestDealer(){
                //Registro
            String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
            String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
            RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
            RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
            RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
            Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
                
                User u = new User(
                        Alias = 'ConsTSM', 
                        Email='ConsultorTSM@testorgtoyota.com',
                        LastName='TSM',
                        ProfileId = p.Id,
                        UserName='standarduser@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert u;
                User u2 = new User(
                        Alias = 'ConsTSM', 
                        Email='ConsultorTSM2@testorgtoyota.com',
                        LastName='TSM',
                        ProfileId = p.Id,
                        UserName='standarduser2@testorgtoyota.com',
                        EmailEncodingKey='UTF-8',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',
                        TimeZoneSidKey='America/Los_Angeles'
                );
                insert u2;

                Seccion__c seccion01 = new Seccion__c(
                        Name = 'Secccion01',
                        RecordTypeId = recordTypeSec.Id,
                        Activo__c = true
                );
                insert seccion01;
                Seccion__c seccion02 = new Seccion__c(
                        Name = 'Secccion02',
                        RecordTypeId = recordTypeSec.Id,
                        Activo__c = true
                );
                insert seccion02;

                Pregunta__c pregunta01 = new Pregunta__c(
                        Name = 'AC-01',
                        Reactivo__c = 'Reactivo__c',
                        Seccion_Toyota_Mexico__c = seccion01.Id,
                        Activo__c = true
                );
                insert pregunta01;

                Pregunta__c pregunta02 = new Pregunta__c(
                        Name = 'AC-02',
                        Reactivo__c = 'Reactivo__c',
                        Seccion_Toyota_Mexico__c = seccion02.Id,
                        Activo__c = true
                );
                insert pregunta02;

                Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
                        Name = 'OE',
                        Orden__c = '1',
                        Clave_Preg__c = pregunta01.Id
                );
                insert oe01;

                Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
                        Name = 'OE',
                        Orden__c = '1',
                        Clave_Preg__c = pregunta02.Id
                );
                insert oe02;

                Account dealer = new Account(
                        RecordTypeId = recordTypeDealer.Id,
                        Name = 'Test Record'
                );
                insert dealer;

                Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
                        RecordTypeId = recordTypeED.Id,
                        Consultor_TSM_Titular__c = u2.Id,
                        Nombre_Dealer__c = dealer.Id
                );
                insert evalDealer;

                Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
                    Evaluacion_Dealer__c = evalDealer.Id,
                    Usuario__c = u.Id,
                    Seccion_Toyota_Mexico__c = seccion01.Id
                );
                insert rsc01;
                        Relacion_Seccion_Consultor__c rsc02 = new Relacion_Seccion_Consultor__c(
                    Evaluacion_Dealer__c = evalDealer.Id,
                    Usuario__c = u2.Id,
                    Seccion_Toyota_Mexico__c = seccion02.Id
                );
                insert rsc02;
                update evalDealer;

                Respuestas_Preguntas_TSM__c respPreg = [SELECT Id, Evaluacion_Dealer__c, Pregunta_Relacionada__c, (SELECT Id FROM Respuestas_ObjetosTSM_del__r) FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c =:evalDealer.Id limit 1];
                Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
                    Celula_Area__c='DO',
                    Observaciones__c='Observaciones__c',
                    Condicion_Observada__c='Condicion_Observada__c',
                    Referencia__c='TSM',
                    Cuenta__c=evalDealer.Nombre_Dealer__c,
                    Respuestas_Preguntas_TSM__c=respPreg.Id
                );
                insert api01;
                
                Respuestas_ObjetosTSM__c ro01 = new Respuestas_ObjetosTSM__c(
                                Objeto_Evaluacion_Relacionado__c=oe01.Id, 
                                Pregunta_Relacionada_del__c=respPreg.Pregunta_Relacionada__c, 
                                Respuestas_Preguntas_TSM_del__c=respPreg.Id
                );
                insert ro01;

                Actividad_Plan_Integral__c api02 = new Actividad_Plan_Integral__c(
                    Celula_Area__c='DO',
                    Observaciones__c='Observaciones__c',
                    Condicion_Observada__c='Condicion_Observada__c',
                    Referencia__c='TSM',
                    Cuenta__c=evalDealer.Nombre_Dealer__c,
                    Respuestas_Preguntas_TSM__c=respPreg.Id,
                    RespuestasObjetosTSM__c=ro01.Id
                );
                insert api02;
                

                return evalDealer;
        }

	@isTest
	static void itShould(){

		Evaluaciones_Dealer__c evalDealer = EvaluacionTSMControllerTest.getTestDealer();
                

		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(evalDealer);
                	EvaluacionTSMController evc = new EvaluacionTSMController(stdController);
                	evc.getItems();
                        evc.getPrioridades();
                        evc.getAreas();
                        evc.getReferencia();
                	evc.Next();
                	evc.Next();
                	evc.getValidateAtt();
                	evc.Previous();
                	evc.Previous();
                	evc.Buscar();
                	evc.Guardar();

                	AttachmentController.uploadAttachment(evalDealer.Id, 'String attachmentBody', 'String attachmentName.txt', null);
                	evc = new EvaluacionTSMController(stdController);
                	try{
                		evc.preguntas = null;
                		evc.Guardar();
                	}catch(Exception e){
                		System.debug('Validacion de exception al guardar=' + evc.saveResult);
                	}

                        try{
                                evc.preguntaGuardada = null;
                                evc.Guardar();
                        }catch(Exception e){
                                System.debug('Validacion de exception al guardar=' + evc.saveResult);
                        }  


		Test.stopTest();
	}
}