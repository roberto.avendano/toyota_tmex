/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para el componente TAM_ReporteLeadsDTCtrl y la
                        clase TAM_ReporteLeadsDTCtrl

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Junio-2021        Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ReporteLeadsDTCtrl_tst {

    static  Account Distrib = new Account();
    
    @TestSetup static void loadData(){
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '57000',
                TAM_GestorLeads__c = UserInfo.getUserId()
            );              
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id
            );
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive = true,
                ContactId = con.Id,
                timezonesidkey ='America/Los_Angeles', 
                username ='testOwnertfsproduc@noemail.com.test',
                TAM_ReasignacionMasivaLeads__c = true
            );
            insert user;

            RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_NuevoCandidato' and SobjectType = 'Lead'];    
                        
            Lead l2vin = new Lead();
            l2vin.RecordTypeId = rectype.id;
            l2vin.LeadSource = 'DISTRIBUIDOR Tráfico de piso'; //'Landing Page';
            l2vin.FirstName = 'Hector';
            l2vin.Email = 'hectfa@test.com';
            l2vin.phone = '5571123922';
            l2vin.Status='Nuevo Prospecto';
            l2vin.LastName = 'Hector';
            l2vin.TAM_NomPropKban__c = 'Héctor Figueroa';
            l2vin.TAM_Inventario__c  = null;
            l2vin.FWY_codigo_distribuidor__c = '57000';
            l2vin.TAM_EnviarAutorizacion__c = false;
            insert l2vin;

            TAM_UsuariosSeguimientoLead__c objUsrSeg = new TAM_UsuariosSeguimientoLead__c(
                Name = 'Hector Hector',
                TAM_IdExterno__c = 'Hector Hector-57000',
                TAM_NoDistribuidor__c = '57000',
                TAM_NombreCompleto__c = 'Hector Hector',
                TAM_NombreDistribuidor__c = 'TOYOTA POLANCO',
                TAM_ActivoSFDC__c = true,
                TAM_IdUsuarioPartner__c = user.id,
                TAM_Activo__c = true
            );
            insert objUsrSeg;
            
            TAM_MaximoNumeroLeads__c testCustum = new TAM_MaximoNumeroLeads__c();
            testCustum.TAM_CodigoDealer__c = '57000';
            testCustum.Id_Externo__c = '57000';
            testCustum.TAM_MaximoLeads__c = '20';
            insert testCustum;
            
        Test.stopTest();

    }

    static testMethod void TAM_ReporteLeadsDTCtrlOK() {
        //Inicia las pruebas        
        Test.startTest();
            Lead candPaso = [Select id, Name, FirstName, LastName, LeadSource, Email, phone, Status, FWY_codigo_distribuidor__c
                From Lead LIMIT 1];

            TAM_UsuariosSeguimientoLead__c objUsrSeg = [Select ID, Name, TAM_IdExterno__c, TAM_NoDistribuidor__c, 
                TAM_NombreCompleto__c, TAM_NombreDistribuidor__c, TAM_ActivoSFDC__c, TAM_IdUsuarioPartner__c, TAM_Activo__c 
                From TAM_UsuariosSeguimientoLead__c LIMIT 1];
            
            String IdClienteActual = '';
            String sPropietario = 'test123p';
            String sDistribuidor = '57000';
            String strCadenaDeBusqueda = '57000'; 
            String defaultPrm = 'true';
            
            //La lista del tipo wrpUsuarioDealer
            List<TAM_ReporteLeadsDTCtrl.wrpUsuarioDealer> lUsrDealers = new List<TAM_ReporteLeadsDTCtrl.wrpUsuarioDealer>();            
            lUsrDealers.add(new TAM_ReporteLeadsDTCtrl.wrpUsuarioDealer(objUsrSeg.TAM_IdUsuarioPartner__c, sDistribuidor, objUsrSeg.TAM_NombreCompleto__c));

            List<TAM_ReporteLeadsDTCtrl.wrpDelaer> lDealers = new List<TAM_ReporteLeadsDTCtrl.wrpDelaer>();
            //Llena la lista de lDealers  
            lDealers.add(new TAM_ReporteLeadsDTCtrl.wrpDelaer(sDistribuidor, 'TOYOTA POLANCO', lUsrDealers));
            
	        System.debug('En clsGetDatosUsuariosActual  lDealers: ' + lDealers);
	        //Inicializa el objeto de objwrpUsuario        
	        TAM_ReporteLeadsDTCtrl.wrpUsuario objWrpUsuarioPaso = new TAM_ReporteLeadsDTCtrl.wrpUsuario(sDistribuidor, 'Testingproductfs', 'Manager');
	        objWrpUsuarioPaso.idUsuario = objUsrSeg.TAM_IdUsuarioPartner__c;
	        objWrpUsuarioPaso.nombreDist = 'TOYOTA POLANCO';                
	        objWrpUsuarioPaso.blnMuestraLD = false;                
	        objWrpUsuarioPaso.blnMuestraLUD = false;
	        objWrpUsuarioPaso.lDealers = lDealers;        
            
            //Un objeto sin parametros
            TAM_ReporteLeadsDTCtrl.wrpUsuario objWrpUsuarioSinPrm = new TAM_ReporteLeadsDTCtrl.wrpUsuario();
            //Un objeto sin parametros
            TAM_ReporteLeadsDTCtrl.wrpUsuario objWrpUsuarioConPrm = new TAM_ReporteLeadsDTCtrl.wrpUsuario(
                sDistribuidor, 'Testingproductfs', 'Manager'
            );
            
            //Llama a la funcion de TAM_ReporteLeadsDTCtrl.clsGetPerfilUserActual
            TAM_ReporteLeadsDTCtrl.clsGetPerfilUserActual();            
            //Llama a la funcion de TAM_ReporteLeadsDTCtrl.clsGetDatosUsuariosActual
            TAM_ReporteLeadsDTCtrl.clsGetDatosUsuariosActual(objUsrSeg.TAM_IdUsuarioPartner__c);
            //Llama a la funcion de TAM_ReporteLeadsDTCtrl.clsGetDatosUsuariosActual
            TAM_ReporteLeadsDTCtrl.clsGetListaUsr(sDistribuidor, objWrpUsuarioPaso);
            //Llama a la funcion de TAM_ReporteLeadsDTCtrl.clsGetDatosUsuariosActual
            TAM_ReporteLeadsDTCtrl.clsBuscaTotPorUsuario(objUsrSeg.TAM_IdUsuarioPartner__c
                , objWrpUsuarioPaso, '57999', '57999', '2021-06-01', '2021-06-30', 'Mes actual');
            //Llama a la funcion de TAM_ReporteLeadsDTCtrl.clsGetDatosUsuariosActual
            TAM_ReporteLeadsDTCtrl.clsBuscaTotPorUsuario(objUsrSeg.TAM_IdUsuarioPartner__c
                , objWrpUsuarioPaso, '57000', '57000', '2021-06-01', '2021-06-30', 'Mes anterior');
            
        
        //Terminan las pruebas
        Test.stopTest();        
    }
}