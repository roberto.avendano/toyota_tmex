/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto Lead
                        y actuelizar los campos de FWY_codigo_distribuidor__c y TAM_NomPropKban__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    17-Sep-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActNomKbanLeadSch_cls implements Schedulable{

    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActNomKbanLeadSch_cls.execute...');
        
        String sOwnerIdField = 'Owner';
        String sCodDistField = 'FWY_codigo_distribuidor__c';
        
        String sHoraZonaHoraria = TAM_ActCteVehicSch_cls.getHoraZonaHoraria();
        System.debug('EN TAM_ActNomKbanLeadSch_cls.execute sHoraZonaHoraria: ' + sHoraZonaHoraria);

        //Para la fecha Ini        
        DateTime dtFechaCreacIni = DateTime.now().addDays(-30);
        DateTime dtFechaCreacIni2 = dtFechaCreacIni.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacIni;
        String sdtFechaCreacIni = String.valueOf(dtFechaCreacIni2);
        String sdtFechaCreacIniFinal = sdtFechaCreacIni.replace(' ', 'T') + 'Z';
        //Para la fecha Fin
        DateTime dtFechaCreacFin = DateTime.now();
        DateTime dtFechaCreacFin2 = dtFechaCreacFin.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacFin;
        String sdtFechaCreacFin = String.valueOf(dtFechaCreacFin2);
        String sdtFechaCreacFinFinal = sdtFechaCreacFin.replace(' ', 'T') + 'Z';
        System.debug('EN TAM_ActNomKbanLeadSch_cls.execute sdtFechaCreacIniFinal: ' + sdtFechaCreacIniFinal + ' sdtFechaCreacFinFinal: ' + sdtFechaCreacFinFinal);
            
        String sQuery = 'Select Lead.TAM_CodDistribuidorUsr__c, Lead.FWY_codigo_distribuidor__c,';
        sQuery += ' Lead.TAM_NomPropUsr__c, Lead.TAM_NomPropKban__c, LeadId, Lead.FWY_Distribuidor_f__c,';
        sQuery += ' Lead.Distribuidor_para_cuenta__c';
        sQuery += ' From LeadHistory l';
        //Si es una prueba
        if (!Test.isRunningTest()){        
	        sQuery += ' Where CreatedDate >= ' + String.valueOf(sdtFechaCreacIniFinal);
	        sQuery += ' And CreatedDate <= ' + String.valueOf(sdtFechaCreacFinFinal);
            sQuery += ' And (l.Field = \'' + String.escapeSingleQuotes(sOwnerIdField) + '\'';
            sQuery += ' Or l.Field = \'' + String.escapeSingleQuotes(sCodDistField) + '\')';	        
	        sQuery += ' Order by LeadId, CreatedDate ASC';
        }
        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 1';
        System.debug('EN TAM_ActNomKbanLeadSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  objActCancelSolInvBch      
        TAM_ActNomKbanLeadBch_cls objActNomKbanLeadBch = new TAM_ActNomKbanLeadBch_cls(sQuery);
        //No es una prueba entonces procesa de 20 en 20
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActNomKbanLeadBch, 25);

    }
    
    /*public static string getHoraZonaHoraria(){
        System.debug('EN TAM_ActNomKbanLeadSch_cls.execute getHoraZonaHoraria...');
        String sHoraTimeZone = '';    
        for (User objUsr : [Select u.id, u.TimeZoneSidKey From User u Where id =:UserInfo.getUserId()]){ //UserInfo.getUserId()
            System.debug('EN TAM_ActNomKbanLeadSch_cls.execute objUsr.TimeZoneSidKey: ' + objUsr.TimeZoneSidKey);
            Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                if (pickListVal.getValue() == objUsr.TimeZoneSidKey){
                    System.debug('EN TAM_ActNomKbanLeadSch_cls.execute getLabel(): ' + pickListVal.getLabel() + ' getValue(): ' + pickListVal.getValue());  
                    //Toma los primeros caracteres de la pickListVal.getLabel()
                    String strTimeZonePaso = pickListVal.getLabel().left(11);                                 
                    System.debug('EN TAM_ActNomKbanLeadSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso);
                    String[] arrFechaGmt = strTimeZonePaso.split('-');
                    String[] arrHoraTimeZoneGmtPaso = arrFechaGmt[1].split(':');
                    sHoraTimeZone = arrHoraTimeZoneGmtPaso[0]; 
                    System.debug('EN TAM_ActNomKbanLeadSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso + ' arrFechaGmt: ' + arrFechaGmt + ' arrHoraTimeZoneGmtPaso: ' + arrHoraTimeZoneGmtPaso + ' sHoraTimeZone: ' + sHoraTimeZone);
                }
            }
        }        
        System.debug('sHoraTimeZone : ' + sHoraTimeZone);
        return sHoraTimeZone;     
    }*/
    
}