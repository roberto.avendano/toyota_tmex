global class BatchRepo6m_Schedulable implements Schedulable {
    global static String scheduleit(){
        string strSeconds = '0';
        string strMinutes = '0';
        string strHours = '1';
        string strDay_of_month = 'L';
        string strMonth = '6,12';
        string strDay_of_week = '?';
        string strYear = '2019-2200';
        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
        
        BatchRepo6m_Schedulable job = new BatchRepo6m_Schedulable();
        return System.schedule('Ejecuta_BatchRepo6m',sch,job);
    }
    
    global void execute(SchedulableContext sc) {
        BatchRepo6m uc = new BatchRepo6m();
        Database.executeBatch(uc);
    }
}