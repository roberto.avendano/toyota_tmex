public without sharing  class TAM_SearchAutoDemoExcepcion {
    
    //Variable publica
    public static Map<String, Movimiento__c> mapMovimientos {get; set;}
    
    @AuraEnabled
    public static List <wrapVIN > vinBDDealerDaily(String searchKeyWord,String userId) {
        User[] usr;
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List <Movimiento__C > returnList = new List <Movimiento__C > ();
        Set<String> lstMensajesReglas = new Set<String>();
        Date fechaHoy = system.today();
        Date fechaMenos225Dias = date.today().addDays(-225);
        String searchKey = searchKeyWord + '%';
        Boolean boolHistoricoCodigo_06 = false;
        TAM_DetallePoliticaJunction__c IncentivosRetail = new TAM_DetallePoliticaJunction__c();
        Boolean bool90DiasIncentivo = true;
        Boolean bool60DiasBaja= true;
        
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];

        //Consultar en Solicitudes de Auto Demo
        List<SolicitudAutoDemo__c> lstAutosDemo = [ SELECT 	Id, VIN__r.Id, VIN__r.Name, Estatus__c,FechaBajaPagoCompletado__c,FechaBajaAutoDemoSolicitud__c
                                                    FROM 	SolicitudAutoDemo__c 
                                                    WHERE 	FechaAltaAutoDemo__c >=: fechaMenos225Dias
                                                    AND     FechaAltaAutoDemo__c <=: fechaHoy 
                                                    AND     VIN__r.name LIKE:searchKey  
                                                    AND     Distribuidor__r.Codigo_Distribuidor__c =: usr[0].CodigoDistribuidor__c
                                                   LIMIT 5];

        Map<String, SolicitudAutoDemo__c> mapAutoDemo = new Map<String, SolicitudAutoDemo__c>();
        for (SolicitudAutoDemo__c  objDemo: lstAutosDemo) {
            mapAutoDemo.put(objDemo.VIN__r.Name, objDemo);
        }
        
        //Consultar movimientos de venta en DD
        List <Movimiento__c> lstOfVin = [SELECT id,Sale_Code__c,Distribuidor__r.name,Distribuidor__r.Codigo_Distribuidor__c,Movimiento__c.Name,
                                                Distribuidor__c,VIN__r.name,Sale_Date__c,TAM_Serie__c,TAM_VIN__c,TAM_codigoModelo__c,TAM_AnioModelo__c,
                                                Fleet__c,Month__c,Year__c
                                        FROM    Movimiento__c
                                        WHERE   VIN__r.Name IN: mapAutoDemo.KeySet()];
       
        Set<String> setVIN = new Set<String>();
        mapMovimientos = new Map<String, Movimiento__c>();
        for (Movimiento__C  objMov: lstOfVin) {
            setVIN.add(objMov.VIN__r.name);
            mapMovimientos.put(objMov.VIN__r.name, objMov);
        }
        
        //Consultar en Facturas XML
        List<Factura__c> lstFacturasXML = [SELECT Id, VIN__r.Name,TAM_FechaTimbrado__c,TAM_Total__c 
                                                    FROM Factura__c WHERE VIN__r.Name IN: setVIN];
        Map<String, Factura__c> mapFacturasXML = new Map<String, Factura__c>();
        for (Factura__c  objXML: lstFacturasXML) {
            mapFacturasXML.put(objXML.VIN__r.Name, objXML);
        }
        
        //Consultar si existe en un estado de cuenta 
        String recordIdRetail  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('AutoDemo').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c[] detalleEdoCta = [Select Id,TAM_VIN__c From TAM_DetalleEstadoCuenta__c Where recordTypeId =: recordIdRetail AND
                                                   TAM_VIN__c =: setVIN];
        
		//Consultar si existe en una Solicitud con estatus Pendiente 
		String recordIdExcepcionDemo = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Auto_Demo').getRecordTypeId(); 	
        TAM_SolicitudExpecionIncentivo__c [] solicitudExepcion = [Select id,TAMVIN__c From TAM_SolicitudExpecionIncentivo__c
                                                                 Where recordTypeId =: recordIdExcepcionDemo AND TAMVIN__c =: setVIN AND (TAM_AprobacionDM__c = 'Pendiente' or TAM_AprobacionFinanzas__c = 'Aprobado')];
       

        if(!lstOfVin.isEmpty()){                                           
            for (Movimiento__C  acc: lstOfVin) {

                
                //Validar en DD
                if(acc.Fleet__c == 'C'){
                    lstMensajesReglas.add('VIN registrado como Venta Corporativa en Dealer Daily.');
                }

                if(acc.Sale_Code__c == '06' || acc.Sale_Code__c == '6'){
                    boolHistoricoCodigo_06 = true;
                    
                    //Validar en Facturas
                    if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name)){

                        //Obtener Incentivos Retail
                        String strMesFactura = String.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c.month());
                        String strAnioFactura = String.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c.year());
                        IncentivosRetail= getIncentivosRetail(strAnioFactura, strMesFactura,acc.VIN__r.name);
                        
                        if(IncentivosRetail != null){
                            Integer intDiasVigencia = (system.today()).daysBetween(Date.valueOf(IncentivosRetail.TAM_PoliticaIncentivos__r.TAM_FinVigencia__c));
                            if(intDiasVigencia < -90){
                                lstMensajesReglas.add('Se ha superado los 90 días para el cobro del incentivo.');
                                bool90DiasIncentivo = false;
                            }
                        } else {
                            lstMensajesReglas.add('VIN no cuenta con incentivo vigente.');

                        }
                        //Validar en Auto Demo
                        if(mapAutoDemo != null && mapAutoDemo.containsKey(acc.VIN__r.name)){
                                
                            //Fecha de baja con 60 días
                            Date datFechaBaja;
                            if(mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c <= mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c){
                                datFechaBaja = mapAutoDemo.get(acc.VIN__r.name).FechaBajaPagoCompletado__c;
                            }else {
                                datFechaBaja = mapAutoDemo.get(acc.VIN__r.name).FechaBajaAutoDemoSolicitud__c;
                            }
                            Integer intDiasBaja = (date.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c)).daysBetween(datFechaBaja);
                            //Excepción de 60 días de baja queda excenta
                          
                            /*if(intDiasBaja < -60){
                                lstMensajesReglas.add('La fecha de baja o pago completado excede 60 días.');
                                bool60DiasBaja = false;
                            }*/

                            //Baja del vehiculo
                            if(mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja por pago completado sin solicitud' && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Pago completado sin solicitud'
                            && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja anticipada' && mapAutoDemo.get(acc.VIN__r.name).Estatus__c != 'Baja por vigencia'){
                                lstMensajesReglas.add('VIN aún se encuentra activo en el módulo de Auto-Demo.');
                            }
                        } else {
                            lstMensajesReglas.add('VIN no registrado en el módulo de Auto-Demo.');
                        }

                    }else{
                        lstMensajesReglas.add('VIN no presenta factura generada.');
                    }
                } else {
                    lstMensajesReglas.add('VIN no registrado con código de venta 06 en Dealer Daily.');
                }
                
                //Llenado Wrapper
                wrapVIN newVIN = new wrapVIN();
                newVIN.VIN = acc.VIN__r.name;
                newVIN.VINID = acc.VIN__r.Id;
                newVIN.idMovimiento = acc.Id;
                newVIN.Serie = acc.TAM_Serie__c;
                newVIN.codigoModelo = acc.TAM_codigoModelo__c;
                newVIN.anioModelo = acc.TAM_AnioModelo__c;
                newVIN.codigoDealer = acc.Distribuidor__r.Codigo_Distribuidor__c;
                //Existe en un estado de cuenta
                if(detalleEdoCta.size () > 0){ 
                	newVIN.existeEstadoCuenta = true;   
                    newVIN.mensajeExisteEstadoCuenta = 'El VIN ingresado ya se encuentra en un estado de cuenta (Auto Demo)';
                }else{
                    newVIN.existeEstadoCuenta = false;  
                }
                //Existe en una solicitud de excepción de auto demo con estatus pendiente
                if(solicitudExepcion.size() > 0){
                    newVIN.existeSolicitudPendiente = true;   
                    newVIN.mensajeExisteSolPendiente = 'El VIN ingresado ya se encuentra en una solicitud de excepción';
                }else{
                    newVIN.existeSolicitudPendiente = false;  
                }
                newVIN.fechaVentaVIN = acc.Sale_Date__c;
                if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name)){
                	newVIN.fechaTimbradoXML = Date.valueOf(mapFacturasXML.get(acc.VIN__r.name).TAM_FechaTimbrado__c);
                }
                newVIN.codigoVenta	 = acc.Sale_Code__c;
                newVIN.nombreDealer  = acc.Distribuidor__r.name;
                newVIN.lstMensajes = lstMensajesReglas;
                newVIN.boolCode_06 = boolHistoricoCodigo_06;
                if(mapFacturasXML != null && mapFacturasXML.containsKey(acc.VIN__r.name) && IncentivosRetail != null && bool90DiasIncentivo && bool60DiasBaja && mapAutoDemo != null && mapAutoDemo.containsKey(acc.VIN__r.name) 
                    && lstMensajesReglas.isEmpty()){
                    
                    newVIN.idAutoDemo = mapAutoDemo.get(acc.VIN__r.name).Id;
                    newVIN.idFacturaCarga =  mapFacturasXML.get(acc.VIN__r.name).Id;
                    newVIN.dblValorFactura = mapFacturasXML.get(acc.VIN__r.name).TAM_Total__c;
                    newVIN.idDetallePolitica = IncentivosRetail.Id;
                    newVIN.dblPrecioLista = IncentivosRetail.TAM_MSRP__c;
                    newVIN.dblPrecioMenosIncentivo = IncentivosRetail.TAM_MSRP__c - IncentivosRetail.TAM_IncPropuesto_Cash__c;
                    
                    newVIN.dblIncentivoPropuesto = IncentivosRetail.TAM_IncPropuesto_Cash__c;
                    newVIN.dblIncentivoTMEX = IncentivosRetail.TAM_IncentivoTMEX_Cash__c;
                    newVIN.dblIncentivoDealer = IncentivosRetail.TAM_IncDealer_Cash__c;
                    newVIN.intPorcentajeParticipacionTMEX = Integer.valueOf((IncentivosRetail.TAM_IncentivoTMEX_Cash__c / IncentivosRetail.TAM_IncPropuesto_Cash__c)*100);
                    newVIN.dblIncentivoOtorgado =  IncentivosRetail.TAM_MSRP__c - mapFacturasXML.get(acc.VIN__r.name).TAM_Total__c;
                    
                    //Escenario  1 "Incentivo Otorgado es mayor al definido en politicas- TMEX solo pagara su limite establecido en el Grid."
                    if(newVIN.dblIncentivoOtorgado >= newVIN.dblIncentivoPropuesto){
                        newVIN.dblIncentivoProporcionalTMEXSinIVA = (IncentivosRetail.TAM_IncentivoTMEX_Cash__c/1.16 );
                    }
                    
                    //Escenario  2 "Incentivo Otorgado es menor al de las politicas"
                    if(newVIN.dblIncentivoOtorgado < newVIN.dblIncentivoPropuesto){
                        Double participacionTMEX = Double.valueOf(IncentivosRetail.TAM_IncentivoTMEX_Cash__c / IncentivosRetail.TAM_IncPropuesto_Cash__c);
                        newVIN.dblIncentivoProporcionalTMEXSinIVA = (newVIN.dblIncentivoOtorgado * participacionTMEX)/1.16;
                    }

                    // Escenario  3 "No se dio incentivo"
                    if(newVIN.dblIncentivoOtorgado == 0){
                        newVIN.dblIncentivoProporcionalTMEXSinIVA = 0;
                    }
                    newVIN.solicitudAceptada = true; 
                } else {
                    
                    newVIN.dblValorFactura = null;
                    newVIN.dblPrecioLista = null;
                    newVIN.dblPrecioMenosIncentivo = null;
                    newVIN.dblIncentivoPropuesto = null;
                    newVIN.dblIncentivoTMEX = null;
                    newVIN.dblIncentivoDealer = null;
                    newVIN.intPorcentajeParticipacionTMEX = null;
                    newVIN.dblIncentivoOtorgado =  null;
                    newVIN.dblIncentivoProporcionalTMEXSinIVA = null;
                    newVIN.solicitudAceptada = false;
                }
                wrapListVIN.add(newVIN); 
            }
        }
        
        system.debug('return'+wrapListVIN);
        return wrapListVIN;
        
    }
    
    //Wrapper
    public class wrapVIN {

        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String nombreDealer {get; set;}
        @AuraEnabled public String codigoVenta {get; set;} 
        @AuraEnabled public String VIN {get; set;} 
        @AuraEnabled public String VINID {get; set;} 
        @AuraEnabled public String Serie {get; set;}
        @AuraEnabled public String codigoModelo {get; set;}
        @AuraEnabled public String anioModelo {get; set;}
        @AuraEnabled public String idFacturaCarga {get; set;}
        @AuraEnabled public String idMovimiento{get; set;}


        @AuraEnabled public decimal precioPublicoVigente {get; set;} 
        @AuraEnabled public decimal precioPublicoDeVenta {get; set;}
        @AuraEnabled public Date fechaVentaVIN {get; set;}
        @AuraEnabled public Date fechaTimbradoXML {get; set;}

        @AuraEnabled public Set<String> lstMensajes {get; set;}
        @AuraEnabled public Boolean boolCode_06 {get;set;}
        @AuraEnabled public Double dblValorFactura {get;set;}
        @AuraEnabled public Double dblPrecioLista {get;set;}
        @AuraEnabled public Double dblPrecioMenosIncentivo {get;set;}
        @AuraEnabled public Double dblIncentivoPropuesto {get;set;}
        @AuraEnabled public Double dblIncentivoTMEX {get;set;}
        @AuraEnabled public Double dblIncentivoDealer {get;set;}
        @AuraEnabled public Integer intPorcentajeParticipacionTMEX {get;set;}
        @AuraEnabled public Double dblIncentivoOtorgado{get;set;}
        @AuraEnabled public Double dblIncentivoProporcionalTMEXSinIVA{get;set;}
        @AuraEnabled public Boolean solicitudAceptada {get;set;}
        @AuraEnabled public String idDetallePolitica {get;set;}
        @AuraEnabled public String idAutoDemo {get;set;} 
        
        @AuraEnabled public Boolean existeEstadoCuenta {get;set;}
        @AuraEnabled public String  mensajeExisteEstadoCuenta {get;set;} 
        @AuraEnabled public Boolean existeSolicitudPendiente {get;set;}
        @AuraEnabled public String  mensajeExisteSolPendiente {get;set;} 
    }
    
    //Obtener Mapa de Incentivos Retail
    public static TAM_DetallePoliticaJunction__c getIncentivosRetail(String strAnio, String strMes, String strVIN){
        Integer intMes = Integer.valueOf(strMes);
        Integer intAnio = Integer.valueOf(strAnio);
       	String strAnioModelo = mapMovimientos.get(strVIN).TAM_AnioModelo__c;
        String strSerie = mapMovimientos.get(strVIN).TAM_Serie__c;
        String strModelo = mapMovimientos.get(strVIN).TAM_codigoModelo__c;

        TAM_DetallePoliticaJunction__c objIncentivos;
        for(TAM_DetallePoliticaJunction__c objAUX:  [SELECT 	Id,
                                                        		TAM_AnioModelo__c,
                                                                TAM_Serie__c,
                                                                TAM_Clave__c,
                                                                Name,
                                                                TAM_MSRP__c,
                                                        		TAM_IncPropuesto_Cash__c,
                                                                TAM_IncentivoTMEX_Cash__c,
                                                        		TAM_IncDealer_Cash__c,
                                                                TAM_PoliticaIncentivos__r.Id,
                                                                TAM_PoliticaIncentivos__r.Name,
                                                                TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c,
                                                                TAM_PoliticaIncentivos__r.TAM_FinVigencia__c
                                                        FROM	  TAM_DetallePoliticaJunction__c
                                                        WHERE	  TAM_PoliticaIncentivos__r.RecordType.Name = 'Retail'
                                                        AND	  (TAM_PoliticaIncentivos__r.TAM_EstatusIncentivos__c = 'Vigente'
                                                               OR TAM_PoliticaIncentivos__r.TAM_EstatusIncentivos__c = 'Vencida')
                                                        AND     CALENDAR_MONTH(TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c) <=: intMes
                                                        AND     CALENDAR_YEAR(TAM_PoliticaIncentivos__r.TAM_InicioVigencia__c) <=: intAnio
                                                        AND     CALENDAR_MONTH(TAM_PoliticaIncentivos__r.TAM_FinVigencia__c) >=: intMes
                                                        AND     CALENDAR_YEAR(TAM_PoliticaIncentivos__r.TAM_FinVigencia__c) >=: intAnio
                                                        AND	  TAM_IncentivoTMEX_Cash__c != null
                                                        AND	  TAM_AnioModelo__c =: strAnioModelo
                                                        AND	  TAM_Serie__c =: strSerie
                                                        AND	  TAM_Clave__c =: strModelo
                                                        ORDER BY TAM_IncPropuesto_Cash__c DESC LIMIT 1]){
            objIncentivos = objAUX;
        }
        return objIncentivos;
    } 
}