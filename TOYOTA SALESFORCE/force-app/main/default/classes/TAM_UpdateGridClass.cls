/**
    Descripción General: Actualizar Modelos faltantes desde Catálogo centralizado de modelos para el componente "TAM_Grid"
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        23/Julio/2020          Versión Inicial
    ________________________________________________________________
**/
public without sharing class TAM_UpdateGridClass {
    
    //Variables Públicas
    public static Set<String> setIdExternoModelos {get;set;}
    public static Set<String> setCodigoModelo {get;set;}
    public static Map<String, CatalogoCentralizadoModelos__c> mapCatalogoCentralizado {get;set;}
    public static Map<String, InventarioPiso__c> mapInventarioPiso {get;set;}
    public static Map<String,PricebookEntry> mapPriceBookEntry {get;set;}
    public static Map<String,PricebookEntry> mapPriceBookEntry_Standard {get;set;}
    public static Set<String> setClaveModelosClon{get;set;}
    public static Map<String, List<TAM_DetallePoliticaJunction__c>> mapDetalleInsertado {get;set;}
    

    
    /*GET DETALLE JUNCTION*/
    public static void getDetalleJunction(String recordId){
        List<TAM_DetallePoliticaJunction__c> lstDetalleJunction = [SELECT 	Name,
                                                                   		 	TAM_AnioModelo__c,
                                                                   			TAM_Clave__c,
                                                                   			TAM_Serie__c,
                                                                   			TAM_PoliticaIncentivos__c
                                                                   FROM	  	TAM_DetallePoliticaJunction__c
                                                                   WHERE	TAM_PoliticaIncentivos__c =:recordId];
        for(TAM_DetallePoliticaJunction__c objDetalle : lstDetalleJunction){
            String strCodigo = objDetalle.TAM_Serie__c + objDetalle.TAM_Clave__c + objDetalle.TAM_AnioModelo__c;
            if(setIdExternoModelos.contains(strCodigo)){
                setIdExternoModelos.remove(strCodigo);
            }
        }
    }
    
    /*SET DETALLE POLITICA DE INCENTIVOS JUNCTION*/
    public static void setDetalleJunction(String recordId){
        TAM_PoliticasIncentivos__c objPolitica = [SELECT TAM_Marca__c FROM TAM_PoliticasIncentivos__c WHERE Id=: recordId];
        
        //Llamado de metodos de consulta
        getCatalogoCentralizado(objPolitica.TAM_Marca__c);
        getInventario(recordId);
        getPrecios(recordId);
        getStandardPrice();
       	getDetalleJunction(recordId);
        getIncentivoAnterior(recordId);
        /*******************************/
        
        String strCodigoModelo = '';
        Double intMargenTMEX_Porcentaje = 0.0;
        Integer intMargenTMEX_Pesos = 0;
        Integer intMSRP = 0;
        Integer intDiasInventario;
        Integer intCantidadInventario;
        Date datFechaInventario;
        String incentivoAnterior;
        String strNombreListaPrecios;
        
        List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaJunction = new List<TAM_DetallePoliticaJunction__c>();
        
        if(!setIdExternoModelos.isEmpty()){
            for(String strIdModelos : setIdExternoModelos){
                strIdModelos= strIdModelos.toUpperCase();
                TAM_DetallePoliticaJunction__c objDetalleJunction = new TAM_DetallePoliticaJunction__c();
                
                //Validación de mapa de pricebookEntry
                strCodigoModelo = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) +  String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
                strCodigoModelo = strCodigoModelo.toUpperCase();
    
                Integer intanterior = Integer.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) -1;
                String strCodigoModeloAnterior = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) +  String.valueOf(intanterior);
                
                if(mapPriceBookEntry.containsKey(strCodigoModelo)){
                    //Margen de ganancia TMEX %
                    if(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c != null){
                        intMargenTMEX_Porcentaje = Double.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c);
                    }
                    //Margen de Ganancia TMEX $
                    if(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c != null){
                        intMargenTMEX_Pesos = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c);
                    }
                    //MSRP
                    if(mapPriceBookEntry.get(strCodigoModelo).TAM_MSRP__c != null){
                        intMSRP = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MSRP__c);
                    }
                    //Nombre lista de precios
                    strNombreListaPrecios = mapPriceBookEntry.get(strCodigoModelo).Pricebook2.Name;
                    
                } else if(mapPriceBookEntry_Standard.containsKey(strCodigoModelo)) {
                    //Margen de ganancia TMEX %
                    if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c != null){
                        intMargenTMEX_Porcentaje = Double.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c);
                    }
                    //Margen de ganancia TMEX $
                    if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c != null){
                        intMargenTMEX_Pesos = Integer.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c);
                    }
                    //MSRP
                    if(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MSRP__c != null){
                        intMSRP = Integer.valueOf(mapPriceBookEntry_Standard.get(strCodigoModelo).TAM_MSRP__c);
                    }
                    //Nombre lista de precios
                    strNombreListaPrecios = mapPriceBookEntry_Standard.get(strCodigoModelo).Pricebook2.Name;
                }
                
                //Validación de mapa de inventarios
                if(mapInventarioPiso.containsKey(strIdModelos)){
                    intCantidadInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).Cantidad__c);
                    intDiasInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).DiasVenta__c);
                    datFechaInventario = Date.valueOf(mapInventarioPiso.get(strIdModelos).FechaInventario__c);
                } else {
                    intCantidadInventario = 0;
                    intDiasInventario = 0;
                    datFechaInventario = null;
                }
                
    
                
                
                //Llenado de lista.
                if(mapDetalleInsertado.containsKey(strCodigoModeloAnterior)){
                    System.debug('ENTRO 1 strCodigoModelo: ' + strCodigoModeloAnterior);
                    List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaClon = new List<TAM_DetallePoliticaJunction__c>();
                    for(TAM_DetallePoliticaJunction__c objdet : mapDetalleInsertado.get(strCodigoModeloAnterior)){
                        lstDetallePoliticaClon.add(objdet);
                    }
    
                    for(TAM_DetallePoliticaJunction__c objDetalleClon : lstDetallePoliticaClon){
                            TAM_DetallePoliticaJunction__c objClon = objDetalleClon.clone(false,true,false,false);
                            
    
                            /** Nombre     */	objClon.Name = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + objClon.TAM_Consecutivo__c;
                            /** Politica   */	objClon.TAM_PoliticaIncentivos__c = recordId;
                            /** Año Modelo */   objClon.TAM_AnioModelo__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
                            /** Modelo     */   objClon.TAM_Clave__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c);
                            /** Versión    */   objClon.TAM_Version__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Version__c);
                            /** Inventario */   objClon.TAM_Inventario_Dealer__c = intCantidadInventario;
                            /** Dias Venta */   objClon.TAM_Dias_de_Inventario__c = intDiasInventario;
                            /**	MSRP       */   objClon.TAM_MSRP__c = intMSRP;
                            /**	MargenTMEX $*/  objClon.TAM_TMEX_Margen_Pesos__c = intMargenTMEX_Pesos;
                            /**	MargenTMEX %*/  objClon.TAM_TMEX_Margen_Porcentaje__c = intMargenTMEX_Porcentaje;
                            /**	Serie      */   objClon.TAM_Serie__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c);
                            /**	Id Externo */   objClon.TAM_IdExterno__c = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + objClon.TAM_Consecutivo__c;
                            /** Fecha Inve */   objClon.TAM_FechaInventario__c = datFechaInventario;
                            /**Marca */         objDetalleJunction.TAM_Marca__c = objPolitica.TAM_Marca__c;
                            
                            /**ListaPrecios*/	objClon.TAM_NomListaPreciosAux__c = strNombreListaPrecios;
                            System.debug('objClon: ' + objClon);
                        lstDetallePoliticaJunction.add(objClon);
                    }
    
                } else {
                    System.debug('ENTRO 2 strCodigoModelo: ' + strCodigoModelo);
                    /** Nombre     */	objDetalleJunction.Name = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + '0';
                    /** Politica   */	objDetalleJunction.TAM_PoliticaIncentivos__c = recordId;
                    /** Año Modelo */   objDetalleJunction.TAM_AnioModelo__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
                    /** Modelo     */   objDetalleJunction.TAM_Clave__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c);
                    /** Versión    */   objDetalleJunction.TAM_Version__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Version__c);
                    /** Inventario */   objDetalleJunction.TAM_Inventario_Dealer__c = intCantidadInventario;
                    /** Dias Venta */   objDetalleJunction.TAM_Dias_de_Inventario__c = intDiasInventario;
                    /**	MSRP       */   objDetalleJunction.TAM_MSRP__c = intMSRP;
                    /**	MargenTMEX $*/  objDetalleJunction.TAM_TMEX_Margen_Pesos__c = intMargenTMEX_Pesos;
                    /**	MargenTMEX %*/  objDetalleJunction.TAM_TMEX_Margen_Porcentaje__c = intMargenTMEX_Porcentaje;
                    /**	Serie      */   objDetalleJunction.TAM_Serie__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c);
                    /**	Consecutivo*/   objDetalleJunction.TAM_Consecutivo__c = 0;
                    /**	Id Externo */   objDetalleJunction.TAM_IdExterno__c = 	recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + '0';
                    /** Fecha Inve */   objDetalleJunction.TAM_FechaInventario__c = datFechaInventario;
                    
                    /**ListaPrecios*/	objDetalleJunction.TAM_NomListaPreciosAux__c = strNombreListaPrecios;
                    /**Marca */         objDetalleJunction.TAM_Marca__c = objPolitica.TAM_Marca__c;
                    
                    lstDetallePoliticaJunction.add(objDetalleJunction);
                }
            }
            
            try {
                Database.insert(lstDetallePoliticaJunction);
                System.debug('ID Insertados: ' + lstDetallePoliticaJunction[0].Id);
            } catch (DmlException e) {
                System.debug(e.getMessage());
            }
        }
    }
    
    
    /*OBTENER CATÁLOGO CENTRALIZADO*/
    public static void getCatalogoCentralizado(String strMarca){
        String idExterno = '';
        String codigoModelo = '';
        setIdExternoModelos = new Set<String>();
        setCodigoModelo = new Set<String>();
        mapCatalogoCentralizado = new Map<String, CatalogoCentralizadoModelos__c>();
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT  Modelo__c,
                                                                                AnioModelo__c,
                                                                                Serie__c,
                                                                                Disponible__c,
                                                                                Version__c,
                                                                                Serie_AnioModelo__c
                                                                        FROM    CatalogoCentralizadoModelos__c
                                                                        WHERE   Marca__c =: strMarca];
        
        for(CatalogoCentralizadoModelos__c objCatalogo : lstCatalogoCentralizado){
            idExterno = objCatalogo.Serie__c + objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
            idExterno = idExterno.toUpperCase();
            setIdExternoModelos.add(idExterno);
            codigoModelo = objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
            setCodigoModelo.add(codigoModelo);
            mapCatalogoCentralizado.put(idExterno,objCatalogo);
        }        
    }
    
    /*OBTENER INVENTARIO*/
    public static void getInventario(String recordId){
        
        Date dateFechaInventario;
        Boolean boolCargaHistorica = false;
        String idExternoInventario ='';
        String idExternoInventario_CatalogoModelos ='';
        Decimal intCantidadTotal;
        Decimal intDiasInventarioAux;
        Integer intDiasInventarioTotal;
        mapInventarioPiso = new Map<String, InventarioPiso__c>();
        
        List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
        boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
        if(boolCargaHistorica){
            dateFechaInventario = CargaHistorica[0].TAM_InicioVigencia__c - 1;
        } else {
            dateFechaInventario= System.today() - 1;
        }
        
        List<InventarioPiso__c> lstInventarioPiso = [SELECT	 AnioModelo__c,
                                                     Modelo__c,
                                                     Serie__c,
                                                     Version__c,
                                                     Cantidad__c,
                                                     DiasVenta__c,
                                                     Serie_AnioModelo__c,
                                                     FechaInventario__c
                                                     FROM    InventarioPiso__c
                                                     WHERE   FechaInventario__c =:dateFechaInventario];
        
        
        if(!lstInventarioPiso.isEmpty()){
            for(InventarioPiso__c objInventario : lstInventarioPiso){
                
                //Llenado de Inventario
                idExternoInventario = objInventario.Serie__c + objInventario.Modelo__c +  objInventario.AnioModelo__c;
                idExternoInventario = idExternoInventario.toUpperCase();
                
                
                if(mapInventarioPiso.containsKey(idExternoInventario)){
                    intCantidadTotal =  mapInventarioPiso.get(idExternoInventario).Cantidad__c + objInventario.Cantidad__c;
                    objInventario.Cantidad__c = intCantidadTotal;
                    
                    intDiasInventarioAux = mapInventarioPiso.get(idExternoInventario).DiasVenta__c + objInventario.DiasVenta__c;
                    intDiasInventarioTotal = Integer.valueOf(intDiasInventarioAux.round(System.RoundingMode.HALF_UP));
                    objInventario.DiasVenta__c = intDiasInventarioTotal;
                    
                    
                }
                mapInventarioPiso.put(idExternoInventario,objInventario);
            }
        }
    }
    
    /*OBTENER LISTA DE PRECIOS*/
    public static void getPrecios(String recordId){
        Date fechaActual;
        Boolean boolCargaHistorica = false;
        List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
        boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
        if(boolCargaHistorica){
            fechaActual = CargaHistorica[0].TAM_InicioVigencia__c;
        } else {
            fechaActual= System.today();
        }
        
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'Enero');
        mapMes.put(2, 'Febrero');
        mapMes.put(3, 'Marzo');
        mapMes.put(4, 'Abril');
        mapMes.put(5, 'Mayo');
        mapMes.put(6, 'Junio');
        mapMes.put(7, 'Julio');
        mapMes.put(8, 'Agosto');
        mapMes.put(9, 'Septiembre');
        mapMes.put(10, 'Octubre');
        mapMes.put(11, 'Noviembre');
        mapMes.put(12, 'Diciembre');
        
        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        //Mapa de catálogo de lista de precios.
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                   Product2.IdExternoProducto__c,
                                                   TAM_MargenesGananciaTMEX_Pesos__c,
                                                   TAM_MargenesGananciaTMEX_Porc__c,
                                                   TAM_MSRP__c,
                                                   Pricebook2.Name
                                                   FROM    PricebookEntry
                                                   WHERE   IsActive = true 
                                                   AND     Pricebook2.Name =:strNombreCatalogoPrecios
                                                   AND		Product2.IdExternoProducto__c IN: setCodigoModelo
                                                  ];
        String strCodigoProducto;
        Double dblPrecioSinIVA;
        
        mapPriceBookEntry = new Map<String,PricebookEntry>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.Product2.IdExternoProducto__c;
            mapPriceBookEntry.put(strCodigoProducto, objListaPrecio);
            
        }
    }
    
    
    /* OBTENER LISTA DE PRECIOS STANDARD */
    public static void getStandardPrice(){
        
        //Mapa de catálogo de lista de precios.
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                   ProductCode,
                                                   PrecioSinIva__c,
                                                   TAM_MargenesGananciaTMEX_Pesos__c,
                                                   TAM_MargenesGananciaTMEX_Porc__c,
                                                   TAM_MSRP__c,
                                                   IdExterno__c
                                                   FROM    PricebookEntry
                                                   WHERE   IsActive = true 
                                                   AND     Pricebook2.Name = 'Standard Price Book'
                                                   AND     IdExterno__c IN: setCodigoModelo];
        
        String strCodigoProducto;
        
        mapPriceBookEntry_Standard = new Map<String,PricebookEntry>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.IdExterno__c.substringBefore('-');
            strCodigoProducto = strCodigoProducto.substring(0,strCodigoProducto.length());
            mapPriceBookEntry_Standard.put(strCodigoProducto, objListaPrecio);
        }
    }

    /* OBTENER INCENTIVO DE AÑO MODELO ANTERIOR */
    public static void getIncentivoAnterior(String recordId){

        //obtener informacion inicial
        List<TAM_DetallePoliticaJunction__c> lstDetalleInsertado = [SELECT  TAM_AnioModelo__c,
                                                                            TAM_Serie__c,
                                                                            TAM_Version__c,
                                                                            TAM_Clave__c,
                                                                            TAM_Inventario_Dealer__c,
                                                                            TAM_Dias_de_Inventario__c,
                                                                            TAM_Venta_MY__c,
                                                                            TAM_BalanceOut__c,
                                                                            TAM_Pago_TFS__c,
                                                                            TAM_IncPropuesto_Cash__c,
                                                                            TAM_IncPropuesto_Porcentaje__c,
                                                                            TAM_IncentivoTMEX_Cash__c,
                                                                            TAM_IncentivoTMEX_Porcentaje__c,
                                                                            TAM_IncDealer_Cash__c,
                                                                            TAM_IncDealer_Porcentaje__c,
                                                                            TAM_Tripleplay__c,
                                                                            TAM_ProductoFinanciero__c,
                                                                            TAM_Descripcion__c,
                                                                            TAM_TotalEstimado__c,
                                                                            TAM_TP_IncTMEX_Cash__c,
                                                                            TAM_TP_IncTMEX_Porcentaje__c,
                                                                            TAM_TP_IncDealer_Porcentaje__c,
                                                                            TAM_TP_IncDealer_Cash__c,
                                                                            TAM_IncTFS_Cash__c,
                                                                            TAM_IncTFS_Porcentaje__c,
                                                                            TAM_MSRP__c,
                                                                            TAM_Duplicar__c,
                                                                            TAM_TMEX_Margen_Pesos__c,
                                                                            TAM_TMEX_Margen_Porcentaje__c,
                                                                            TAM_Consecutivo__c,
                                                                            TAM_Rango__c,
                                                                            TAM_Rango__r.Name,
                                                                            TAM_FechaInventario__c,
                                                                            TAM_Lanzamiento__c,
                                                                            TAM_MotivoIncentivo__c,
                                                                            TAM_Incentivo_actual__c,
                                                                            TAM_AplicanAmbosIncentivos__c,
                                                                            TAM_IdExterno__c
                                                                   FROM		TAM_DetallePoliticaJunction__c
                                                                   WHERE 	TAM_PoliticaIncentivos__c =:recordId
                                                                   AND	 (TAM_IncPropuesto_Cash__c != null OR TAM_IncPropuesto_Porcentaje__c != null)
                                                                    ORDER BY  TAM_Serie__c ASC,TAM_Clave__c ASC, TAM_AnioModelo__c DESC];

        Set<String> setAniosModelo = new Set<String>();
        mapDetalleInsertado = new Map<String, List<TAM_DetallePoliticaJunction__c>>();
        List<TAM_DetallePoliticaJunction__c> lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
        for(TAM_DetallePoliticaJunction__c objDetalleJunction: lstDetalleInsertado){
            
            //Set de años modelo
            setAniosModelo.add(objDetalleJunction.TAM_AnioModelo__c);

            //Llenado de mapa detalle insertado
            String strClaveAnio = objDetalleJunction.TAM_Clave__c + objDetalleJunction.TAM_AnioModelo__c;
            strClaveAnio = strClaveAnio.toUpperCase();
            
            lstDetalleAux = mapDetalleInsertado.get(strClaveAnio);
            
            if(mapDetalleInsertado.containsKey(strClaveAnio)){
                
                if(!lstDetalleAux.contains(objDetalleJunction)){
                    
                    lstDetalleAux.add(objDetalleJunction);
                    mapDetalleInsertado.put(strClaveAnio, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
                lstDetalleAux.add(objDetalleJunction);
                mapDetalleInsertado.put(strClaveAnio, lstDetalleAux);
            }
        }
    }

    
}