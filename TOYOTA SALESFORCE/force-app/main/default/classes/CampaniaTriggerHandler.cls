public class CampaniaTriggerHandler extends TriggerHandler {
    
    private List<Campania_Entrada__c> itemsNew;    
    
    public CampaniaTriggerHandler(){
        this.itemsNew = (List<Campania_Entrada__c>) Trigger.new;      
        
    }
    
    //Metodo que se ejecuta despues de insertar un nuevo registro en el objeto: Campania_Entrada__c
    public override void afterInsert(){
        relatedRecordCampaing(itemsNew);
        
    } 
    
    public static void relatedRecordCampaing(List<Campania_Entrada__c> itemsNew){
        List<String> viniN                              = new List<String>();
        List<Contactos_Campanas__c> campaniaOwner       = new List<Contactos_Campanas__c>();
        List<AccountContactRelation> accCtc             = new List<AccountContactRelation>();
        List<Revision_Campana__c> relacionVinCampania   = new List<Revision_Campana__c>();
        List<Catalogo_Campania__c> catalogoCamapania    = new List<Catalogo_Campania__c>();                                                                         
        Map<String,Catalogo_Campania__c> mapCampanias = new  Map<String,Catalogo_Campania__c>();
        
        //Logica para crear  campañas en el cataologo y/o actualizarlas en el caso de que existan
        for(Campania_Entrada__c campaniaEntrada : itemsNew){
            system.debug('vin entrante'+campaniaEntrada.VIN__c);
            //Se crean los registros de tipo Catalogo_Campania__c por cada iteracion
            Catalogo_Campania__c registroCampania   = new Catalogo_Campania__c();
            registroCampania.name                   = campaniaEntrada.name;
            registroCampania.Descripcion_Campana__c = campaniaEntrada.Descripcion_Campania__c;
            registroCampania.Estatus_Campana__c     = (campaniaEntrada.Estatus__c == 'true'? true : false);
            registroCampania.Id_Externo__c          = campaniaEntrada.Name;
            registroCampania.Fecha_Inicio__c        = campaniaEntrada.Fecha_Inicio__c;
            registroCampania.Fecha_Fin__c           = campaniaEntrada.Fecha_Final__c;
            mapCampanias.put(campaniaEntrada.Name,registroCampania);
            
            //Se crean los registros de tipo Revision_Campana__c por cada iteracion
            Revision_Campana__c registroRelacionVinCampania = new Revision_Campana__c();
            registroRelacionVinCampania.name                = campaniaEntrada.VIN__c+'_'+campaniaEntrada.name;
            registroRelacionVinCampania.VIN__c              = campaniaEntrada.VIN__c;
            registroRelacionVinCampania.Id_Externo__c       =  campaniaEntrada.VIN__c+'_'+campaniaEntrada.name;  
            registroRelacionVinCampania.putSObject('VIN_Campana__r', new Vehiculo__c(Id_Externo__c = campaniaEntrada.VIN__c));
            registroRelacionVinCampania.putSObject('CampanaId__r', new Catalogo_Campania__c(Id_Externo__c = campaniaEntrada.Name));
            registroRelacionVinCampania.Estado_campania__c  = campaniaEntrada.Tipo__c;
            relacionVinCampania.add(registroRelacionVinCampania);
            vinIn.add(campaniaEntrada.VIN__c);
            
        }
        
        //Se insertan se valida si existe las campañas en el catalogo para actualizarlas o insertar
        if(!mapCampanias.isEmpty()){
            try{
                system.debug('Lista para upsert catalogoCamapania'+mapCampanias);
                upsert mapCampanias.values() Id_Externo__c;
            }catch(Exception e){
                system.debug('Error al realizar upsert en catalogo de campañas'+e.getMessage());
            }
            
        }
        
        //Se realiza un upsert en la lista relacionVinCampania para relacionar el objeto Revision_Campana__c con los vehiculos y las campañas  
        if(!relacionVinCampania.isEmpty()){
            try{
                system.debug('Lista para upsert relacionVinCampania'+relacionVinCampania);
                upsert relacionVinCampania Id_Externo__c;    
            }catch(Exception e){
                system.debug('Error al realizar upsert en Revision_Campana__c'+e.getMessage());   
            }
            
        }
        
        //Query para consultar AccountContactRelation
        if(!vinIn.isEmpty()){
            system.debug('lista de vin antes de query'+vinIn);
            accCtc = [Select id,ContactId,AccountId,TAM_VIN__c,contact.lastName From AccountContactRelation where TAM_VIN__c IN : vinIn];   
            system.debug('resultado de la query'+accCtc);       
        }
        
        //Logica para relacionar el objeto master Revision_Campana__c con el objeto detail (Contactos)
        if(!accCtc.isEmpty() && !relacionVinCampania.isEmpty()){
            for(AccountContactRelation acCt : accCtc){
                for(Revision_Campana__c rvc : relacionVinCampania){
                    if(acct.TAM_VIN__c == rvc.VIN__c){
                        Contactos_Campanas__c ctcNew = new Contactos_Campanas__c();
                        ctcNew.name                  = acCt.contact.lastName.abbreviate(60)+'-'+acCt.TAM_VIN__c;
                        ctcNew.Revision_Campanas__c  = rvc.id;
                        ctcNew.Contacto_Campana__c   = acCt.ContactId;
                        ctcNew.Id_Externo__c         = acct.TAM_VIN__c+'-'+acct.ContactId+'-'+rvc.Id;
                        campaniaOwner.add(ctcNew);
                        
                    }           
                    
                }
                
            }   
        }
        
        if(!campaniaOwner.isEmpty()){
            try{
                system.debug('Lista para campaniaOwner' + campaniaOwner);
                upsert campaniaOwner Id_Externo__c;   
            }catch(Exception e){
                system.debug('Error en el upsert del objeto Contactos_Campanas__c'+e.getMessage());    
                
            }
            
        }
        
        try{
            delete itemsNew;
            
        }catch(Exception e){
            system.debug('Error al eliminar los registros de Entarada de camapañas'+e.getMessage());
        }
        
    }
    
}