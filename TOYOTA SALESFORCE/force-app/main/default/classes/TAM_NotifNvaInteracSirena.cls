/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de las 
                        Interacciones_Lead__c que se crean o actualizan.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    26-Septiembre-2021   Héctor Figueroa             Modificación
******************************************************************************* */

public with sharing class TAM_NotifNvaInteracSirena {
    
    public class datosInteaccionSirena{

        @AuraEnabled public String sIdLead {get;set;}
        @AuraEnabled public String sNomLead {get;set;}
        @AuraEnabled public String sEstatusLead {get;set;}
        @AuraEnabled public String sFechaCreacCand {get;set;}
        @AuraEnabled public String sFechaCreacMsg {get;set;}
        @AuraEnabled public String sMensaje {get;set;}
        
        public datosInteaccionSirena(){
            this.sIdLead = '';
            this.sNomLead = '';
            this.sEstatusLead = '';
            this.sFechaCreacCand = '';
            this.sFechaCreacMsg = '';
            this.sMensaje = '';
        }
          
        public datosInteaccionSirena(String sIdLead, String sNomLead, String sEstatusLead, 
            String sFechaCreacCand, String sFechaCreacMsg, String sMensaje){
            this.sIdLead = sIdLead;
            this.sNomLead = sNomLead;
            this.sEstatusLead = sEstatusLead;
            this.sFechaCreacCand = sFechaCreacCand;
            this.sFechaCreacMsg = sFechaCreacMsg;
            this.sMensaje = sMensaje;
        }
          
    }

    @AuraEnabled
    public static List<datosInteaccionSirena> getUltimasInteracSirena(){
        System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena...');
        List<datosInteaccionSirena> lstDatosInteaccionSirena = 
            new List<datosInteaccionSirena>();
        
        String sURL = System.Label.TAM_DatosLeadUrl;
        System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena sURL: ' + sURL);
                 
        //Consulta los ultomos 10 reg de Interacciones para el usuario seleccionado
        for (Interacciones_Lead__c objInteracc : [Select i.id, i.TAM_Detalle__c, i.TAM_Candidato__r.Name, 
            i.TAM_Candidato__r.TAM_FechaCreacionSirena__c, i.TAM_Candidato__c, i.TAM_Candidato__r.Status, 
            i.TAM_FechaContacto__c From Interacciones_Lead__c i Where 
            //TAM_Candidato__c = '00Q6w000002ZPVpEAO' And Name IN ('IL-0000060', 'IL-0000065', 'IL-0002487')
            i.TAM_Candidato__r.OwnerId = :UserInfo.getUserId()
            And TAM_Notificar__c = true And TAM_IntegracionSirena__c = true 
            Order by i.TAM_FechaContacto__c DESC LIMIT 15]){
            Date dtFechaCreaCandSirena = objInteracc.TAM_Candidato__r.TAM_FechaCreacionSirena__c.Date();
            Date dtFechaContacto = objInteracc.TAM_FechaContacto__c.Date();
            String sMensajeFinal = objInteracc.TAM_Detalle__c.length() > 100 ? objInteracc.TAM_Detalle__c.substring(0,100) : objInteracc.TAM_Detalle__c;
            System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena Name: ' + objInteracc.TAM_Candidato__r.Name);
            System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena Status: ' + objInteracc.TAM_Candidato__r.Status);
            System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena sMensajeFinal: ' + sMensajeFinal);
            System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena String.valueOf(dtFechaCreaCandSirena): ' + String.valueOf(dtFechaCreaCandSirena));
            //String sNomLead, String sEstatusLead, String sFechaCreacCand, String sFechaCreacMsg, String sMensaje
            //Agrega el registro a la lista de lstDatosInteaccionSirena
            lstDatosInteaccionSirena.add(new datosInteaccionSirena(sURL + '' + objInteracc.TAM_Candidato__c, 
                    objInteracc.TAM_Candidato__r.Name,
                    objInteracc.TAM_Candidato__r.Status, String.valueOf(dtFechaCreaCandSirena),
                    String.valueOf(dtFechaContacto), objInteracc.TAM_Detalle__c 
                )
            );
        }
        System.debug('EN TAM_NotifNvaInteracSirena.getUltimasInteracSirena lstDatosInteaccionSirena: ' + lstDatosInteaccionSirena);
        
        //Regresa la lista de lstDatosInteaccionSirena
        return lstDatosInteaccionSirena;
    }
    
}