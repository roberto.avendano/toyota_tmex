/*******************************************************************************
Desarrollado por: Globant México
Autor: Roberto Carlos Avendaño Quintana
Proyecto: Assestment Center TMEX
*********************************************************************************/
public class ControllerCancelarEval {
    
    @AuraEnabled 
    public static RH_Contacto__c  getInfoRecord(Id recordIdEval){
        RH_Contacto__c infoRecord = new RH_Contacto__c();
        if(recordIdEval != null){
            infoRecord = [Select id,Estatus__c from RH_Contacto__c WHERE id =: recordIdEval];    
        } 
        
        return infoRecord;
        
    }
    
    @AuraEnabled
    public static boolean cancela(String recordIdEval){
        String contactId,usrId;
        RH_Contacto__c[] contactRH = [select RH_Contacto__c,Estatus__c  from RH_Contacto__c WHERE id =: recordIdEval];
        
        if(contactRH.size() > 0 && contactRH[0].Estatus__c == 'Pendiente'){
            contactRH[0].Estatus__c = 'Evaluación cancelada';
            update contactRH;
        }
        
        
        if(contactRH.size() > 0 && contactRH[0].Estatus__c == 'Activo'){
            contactId  = contactRH[0].RH_Contacto__c;
            
        }
        
        if(contactId != null){
            User[] userId = [Select id From User where contactId =: contactId];
            if(userId.size() > 0){
                usrId = userId[0].id;
            }  
            
        }
        
        
        if(usrId != null){
            system.debug('Cancelar evaluación');
            changeOwnerEvalAdmin(usrId);
            
        }
        return true;
        
    }
    
    //Metodo utilizado para cambiar el owner de la evaluaciónes a usuario ADMIN despues de el tiempo de la evaluación finalizo
    @AuraEnabled
    public static Boolean changeOwnerEvalAdmin(String UserId){
        //****Definir cual es el usuario admin al que se le asignaran las evaluaciones ********
        Boolean succesOwnerEval;
        String userAdminRH = '005i0000000HOf6';
        boolean activarLic;
        id ContactUser;
        User usrCtc = new User();
        String idUserSFDC = UserId.subString(0,15); 
        List<RH_EvaluacionCandidato__c> evalRhUpd = new List<RH_EvaluacionCandidato__c>(); 
        if(UserId != null){
            usrCtc = [Select id,contactId from User Where id =: idUserSFDC];
        }
        if(usrCtc != null){
            ContactUser  = usrCtc.contactId;
            activarLic = activarLicencia(ContactUser);
            inhabilitarDocumentosGrupo(idUserSFDC);	
        }
        List<RH_EvaluacionCandidato__c> inboxEvaluacion = [Select ownerId,id from RH_EvaluacionCandidato__c WHERE  RH_evalFinalizada__c = true AND RH_candidatoEvaluacion__R.RH_Contacto__c =: ContactUser];
        if(!inboxEvaluacion.isEmpty()){
            for(RH_EvaluacionCandidato__c  inboxEval : inboxEvaluacion){
                RH_EvaluacionCandidato__c updOwner = new RH_EvaluacionCandidato__c();
                updOwner.id =  inboxEval.id;
                inboxEval.OwnerId = userAdminRH;
                evalRhUpd.add(inboxEval);
            }
            
        }
        if(!evalRhUpd.isEmpty()){
            Database.SaveResult[] srList = Database.update(inboxEvaluacion, false);
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    succesOwnerEval = true;
                }
                else {              
                    for(Database.Error err : sr.getErrors()) {
                        succesOwnerEval = false;
                    }
                }
            }
        }
        
        if(activarLic == true && succesOwnerEval == true){
            return true;
            
        }else{
            return false;
            
        }
        
    } 
    
    //Metodo que funciona para activar la licencia utilizada , resetear los campos del contacto y ademas para marcar la evaluación como finalizada a nivel de contacto RH
    @auraEnabled
    public static boolean activarLicencia(id ContactoLicencia){
        boolean resultActivarLic;
        RH_Contacto__c rhContacto = new RH_Contacto__c();
        List<Contact> contactLicenciaList = new List<Contact>();
        Contact contactLicencia = new Contact();
        if(ContactoLicencia != null){
            contactLicencia = [Select id,RH_LicenciaDisponible__c From Contact Where id =: ContactoLicencia limit 1];
            contactLicencia.RH_LicenciaDisponible__c = true;
            contactLicencia.RH_FechaAssestment__c = null;
            contactLicencia.RH_DuracionEvalucion__c = null;
            contactLicencia.Contrasena__c = null;
            contactLicencia.Inicio_Evaluacion__c = false;
            contactLicencia.FechaActivacionEval__c = null;
            contactLicencia.FechaEvaluacionRH__c = null;
            contactLicencia.CasoUsoRH__c = null;
            contactLicenciaList.add(contactLicencia);
            try{
                Database.SaveResult[] srList = Database.update(contactLicenciaList, false);
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        resultActivarLic = true;
                    }
                    else {                
                        for(Database.Error err : sr.getErrors()) {
                            resultActivarLic = false;
                        }
                    }
                }
                
            }catch(Exception e){
                system.debug('error general'+e.getMessage());
                system.debug('error en la linea'+e.getLineNumber());
                
            }
        }
        
        if(ContactoLicencia != null){
            rhContacto = [Select id,RH_evaluacionFinalizada__c  From RH_Contacto__c Where RH_Contacto__r.id =: ContactoLicencia AND RH_evaluacionFinalizada__c = false limit 1];
            if(rhContacto != null){
                rhContacto.RH_evaluacionFinalizada__c = true;
                rhContacto.Estatus__c = 'Evaluación cancelada';
                update rhContacto;
                
            }
            
        }
        
        return resultActivarLic;
        
    }
    
    //Metodo que funciona para inhabilitar a el usuario de los grupos publicos y no tener acceso a los docuementos despues de que termine la evaluación
    @future @auraEnabled 
    public static void inhabilitarDocumentosGrupo(id usuarioLicencia){
        List<GroupMember> memberDel = new List <GroupMember>(); 
        if(usuarioLicencia != null){
            memberDel = [Select Id from GroupMember where UserOrGroupID =: usuarioLicencia];
        }
        if(!memberDel.isEmpty()){
            delete memberDel;       
        }
        
    }
    
    
    //Metodo APEX , que funciona para actualizar el password del usuario de Salesforce. (Se cambia el password por uno administrativo);
    @auraEnabled
    public static void changePasswUsr(String UserId){
        String idUserSFDC;
        if(UserId != null){
            idUserSFDC = UserId.subString(0,15); 
        }      
        String passwordAdmin = [Select id,PasswordAdmin__c From adminRHAssestment__c].PasswordAdmin__c;
        System.setPassword(idUserSFDC, passwordAdmin);
        
    }
    
    
}