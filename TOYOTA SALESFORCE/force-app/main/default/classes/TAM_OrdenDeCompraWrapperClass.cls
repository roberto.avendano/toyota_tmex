/**
    Descripción General: Clase Wrapper para Ordenes de Compra.
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        24/Oct/2019         Versión Inicial
    ________________________________________________________________
**/
public class TAM_OrdenDeCompraWrapperClass {
    public class wrpVinesFlotilla{
        @AuraEnabled 
        public String strIdVinFlotilla{get;set;}
        @AuraEnabled 
        public String strIdExterno {get;set;}
        @AuraEnabled 
        public String filtro {get;set;}
    }
    @AuraEnabled 
    public String strMarca {get;set;}
    @AuraEnabled 
    public String strSerie {get;set;}
    @AuraEnabled
    public String strModelo {get;set;}
    @AuraEnabled
    public String strAnioModelo {get;set;}
    @AuraEnabled
    public String strVersion {get;set;}
    @AuraEnabled
    public String strCodigoColorExterior {get;set;}
    @AuraEnabled
    public String strCodigoColorInterior {get;set;}
    @AuraEnabled
    public String strDescripcionColorExterior {get;set;}
    @AuraEnabled
    public String strDescripcionColorInterior {get;set;}
    @AuraEnabled
    public String strCantidadInventario {get;set;}    
    @AuraEnabled
    public String strCantidad {get;set;}
    @AuraEnabled
    public String strDescuentoDealer {get;set;}
    @AuraEnabled
    public String strPrecioSinIVA {get;set;}
    @AuraEnabled
    public String strSubtotal {get;set;} 
    @AuraEnabled
    public String strName {get;set;}
    @AuraEnabled
    public String strNameCustom {get; set;}
    @AuraEnabled
    public String strColorExteriorCustom {get; set;}
    @AuraEnabled
    public String strColorInteriorCustom {get; set;}
    @AuraEnabled
    public Boolean boolDistribuidoraActual {get;set;}
    @AuraEnabled
    public list<TAM_DistribuidoresFlotillaPrograma__c> ListaDetalleDistribuidores {get;set;}
    @AuraEnabled
    public String strIdExterno {get; set;}
    @AuraEnabled
    public String strIdCatModelos {get; set;}  
    @AuraEnabled
    public String strIdAccount {get; set;}
    @AuraEnabled
    public String strVin {get; set;}   
    @AuraEnabled
    public String strIdRegistro {get; set;}
    @AuraEnabled
    public list<wrpVinesFlotilla> ListaVines {get;set;}     
    @AuraEnabled
    public String strNoDealer {get; set;}
    @AuraEnabled
    public Boolean blnIntercambio {get; set;}

    @AuraEnabled
    public Boolean blnDd {get; set;}
    @AuraEnabled
    public Boolean blnCancel {get; set;}
    @AuraEnabled
    public String strSolName {get; set;}

    @AuraEnabled
    public Boolean blnExisteInvGDealer {get; set;}
    @AuraEnabled
    public String strInvGDealerMsg {get; set;}

	//Un constructor por default
	public TAM_OrdenDeCompraWrapperClass(){
	    this.strMarca = '';
	    this.strSerie = '';
	    this.strModelo = '';
	    this.strAnioModelo = '';
	    this.strVersion = '';
	    this.strCodigoColorExterior = '';
	    this.strCodigoColorInterior = '';
	    this.strDescripcionColorExterior = '';
	    this.strDescripcionColorInterior = '';
	    this.strCantidad = '';
	    this.strCantidadInventario = '';
	    this.strDescuentoDealer = '';
	    this.strPrecioSinIVA = '';
	    this.strSubtotal = '';
	    this.strName = '';
	    this.strNameCustom = '';
	    this.strColorExteriorCustom = '';
	    this.strColorInteriorCustom = '';
	    this.boolDistribuidoraActual = false;
	    this.ListaDetalleDistribuidores = new List<TAM_DistribuidoresFlotillaPrograma__c>();
	    this.strIdExterno = '';
	    this.strIdCatModelos = '';
	    this.strVin = '';
	    this.strIdRegistro = '';
	    this.strNoDealer = '';
	    this.blnIntercambio = false;
	    this.blnDd = false;
	    this.blnCancel = false;
	    this.strSolName = '';
	    this.blnExisteInvGDealer = false;
	    this.strInvGDealerMsg = '';
	}
	
	//Un constructor con parametros
    public TAM_OrdenDeCompraWrapperClass(String strMarca, String strSerie, String strModelo, String strAnioModelo, String strVersion, String strCodigoColorExterior,
                                         String strCodigoColorInterior, String strDescripcionColorExterior, String strDescripcionColorInterior, String strCantidad, 
                                         String strDescuentoDealer, String strPrecioSinIVA, String strSubtotal, String strName, String strNameCustom, String strColorExteriorCustom,
                                         String strColorInteriorCustom, Boolean boolDistribuidoraActual) {
        this.strMarca = strMarca;
        this.strSerie = strSerie;
        this.strModelo = strModelo;
        this.strAnioModelo = strAnioModelo;
        this.strVersion = strVersion;
        this.strCodigoColorExterior = strCodigoColorExterior;
        this.strCodigoColorInterior = strCodigoColorInterior;
        this.strDescripcionColorExterior = strDescripcionColorExterior;
        this.strDescripcionColorInterior = strDescripcionColorInterior;
        this.strCantidad = strCantidad;
        this.strDescuentoDealer = strDescuentoDealer;
        this.strPrecioSinIVA = strPrecioSinIVA;
        this.strSubtotal = strSubtotal;
        this.strName = strName;
        this.strNameCustom = strNameCustom;
        this.strColorExteriorCustom = strColorExteriorCustom;
        this.strColorInteriorCustom = strColorInteriorCustom;
        this.boolDistribuidoraActual = boolDistribuidoraActual;
        this.ListaDetalleDistribuidores = new list<TAM_DistribuidoresFlotillaPrograma__c>();
    }

}