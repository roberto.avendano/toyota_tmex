@isTest
public class SolicitudSiteRegisterControllerTest {
    
    @isTest
    static void test_one(){
        Date inicio = date.parse('01/03/2018');
        Date fin = date.parse('31/03/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='ToyotaGroup'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        PoliticaAutosPoolAsignados__c paa2 = new PoliticaAutosPoolAsignados__c(Name='Director comercial',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa2;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='ToyotaGroup'];
        
        Contact c1 = new Contact(AccountId= acc.Id, 
                                 LastName= 'Contact1 TMEX Test', 
                                 Departamento__c = 'Human Resources',
                                 Codigo__c = '456RTY',
                                 Puesto__c	= 'Chief Coordinating Officer',
                                 PuestoEmpleado__c = paa.Id,
                                 Email ='aramos@grupoassa.com', 
                                 RecordTypeId = conRT.Id);
        
        Contact c2 = new Contact(AccountId= acc.Id, 
                                 LastName= 'Contact2 TMEX Test', 
                                 Departamento__c = 'Human Resources',
                                 Codigo__c = '456RZY',
                                 Puesto__c	= 'Chief Coordinating Officer',
                                 PuestoEmpleado__c = paa.Id,
                                 Email ='aramo2s@grupoassa.com', 
                                 RecordTypeId = conRT.Id);
        List<Contact> contacts = new List<Contact>{c1,c2};
            insert contacts;
        
        
        ContactoSite__c cs = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                              FROM ContactoSite__c WHERE Contacto__c =:contacts[0].Id];

        ContactoSite__c cs2 = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                              FROM ContactoSite__c WHERE Contacto__c =:contacts[1].Id];
        cs2.UsuarioRegistrado__c = true;
        try{
            update cs2;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        test.startTest();
        
        PageReference page = System.Page.SolicitudSiteRegister;
        Test.setCurrentPage(page);
        SolicitudSiteRegisterController ssrc = new SolicitudSiteRegisterController(new ApexPages.standardController(cs));
        ssrc.email = 'aramos@grupoassa.com';
        ssrc.password = '12345';
        ssrc.confirmPassword='12345';
        ssrc.registerUser();
        ssrc.login();
        SolicitudSiteRegisterController ssrc2 = new SolicitudSiteRegisterController(new ApexPages.standardController(cs));
        ssrc2.email = 'aramos@grupoassa.com';
        ssrc2.password = '12345';
        ssrc2.confirmPassword='12';
        ssrc2.registerUser();
        ssrc2.login();
        SolicitudSiteRegisterController ssrc3 = new SolicitudSiteRegisterController(new ApexPages.standardController(cs2));
        ssrc3.email = 'aramos23@grupoassa.com';
        ssrc3.password = '12345';
        ssrc3.confirmPassword='12';
        ssrc3.registerUser();
        ssrc3.login();
        
        test.stopTest();
    }
}