global with sharing class EvaluacionKodawariWizardController {

    public Evaluaciones_Dealer__c evalDealer{get; set;}
    public String tipoCertificacion{get; set;}

    public List<User> consultores{get; set;}
    public List<Seccion__c> secciones{get; set;}
   
    //public Evaluaciones_Dealer__c oEvDealer {get;set;}
    public String titular{get;set;}
    //public Id idedealer {get;set;}
    

    public Map<Id,List<Seccion__c>> userSeccion{get; set;}
    public Map<Id, Evaluaciones_Dealer__c> edealerMap = new Map<Id, Evaluaciones_Dealer__c>();

    //public String seccionesUser{get; set;}
    
    
    // BEGIN Guardar Wizard //
    
    public String titularElegido {get;set;}
    public String adjuntosElegidos {get;set;}
    public String seccionesConsultor {get;set;}
    public List<String> adjuntos {get;set;}

    public String dealerSelected {get;set;}
    public Id dealerSelectedId {get;set;}
    
    public List<WrapperRSC> wrscList {get;set;}
    public Boolean saveDone {get;set;}
    public String saveResult {get;set;}
    //public Id certType {get;set;}
    
    // Id de Autoevaluación
    public String rtAutoEval{get; set;}
    //  END  Guardar Wizard //


    Map<String,Map<String,RecordType>> tiposRegistro;
    
    public EvaluacionKodawariWizardController(ApexPages.StandardController controller) {
        evalDealer = (Evaluaciones_Dealer__c)controller.getRecord();
        
        // BEGIN //
        rtAutoEval = Constantes.EVALUACIONES_KODAWARI_DEALER_TIPOS_AUTOEVALUACION;
        wrscList = new List<WrapperRSC>();
        saveDone = false;
        saveResult = '';
        //  END  //

        tiposRegistro = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        
        System.debug(evalDealer);
        if(evalDealer!=null && evalDealer.Id!=null){
            evalDealer = [SELECT Evaluacion_Cerrada__c,
                Consultor_TSM_Titular__c, Id, Name, Nombre_Dealer__c, RecordTypeId,
                RecordType.Name, RecordType.DeveloperName, Tipo_de_Evaluacion_Kodawari__c,
                Nombre_Dealer__r.Id, Nombre_Dealer__r.Name,
                Consultor_TSM_Titular__r.Id, Consultor_TSM_Titular__r.Name,Perfil__c,
                (select Id, Name from Respuestas_Preguntas_TSM__r),
                (select Id, Name, 
                        Usuario__r.Id, Usuario__r.Name, 
                        Seccion_Toyota_Mexico__r.Id, Seccion_Toyota_Mexico__r.Name 
                    from Relaciones_Seccion_Consultor__r)
                FROM Evaluaciones_Dealer__c WHERE Id =: evalDealer.Id];
        }else{
            String clientName = (String) ApexPages.currentPage().getParameters().get(Constantes.EVALUACIONES_DEALER_FIELD_DEALER_ID);
            String clientId = (String) ApexPages.currentPage().getParameters().get(Constantes.EVALUACIONES_DEALER_FIELD_DEALER_ID + '_lkid');
            if(clientId!=null){
                evalDealer.Nombre_Dealer__c = clientId;
                evalDealer.Nombre_Dealer__r = new Account(Id = clientId, Name = clientName);
            }
        }
        try{
            Profile profile = Utility.getProfileById(UserInfo.getProfileId());
            evalDealer.Perfil__c = profile.Name;
        }catch(Exception e){
            System.debug('No existe el perfil ' + profile.Name + ' en la evaluacion');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No existe el perfil ' + profile.Name + ' en la evaluacion'));
        }
        

        System.debug(evalDealer);

        if(evalDealer.RecordTypeId==null){
            evalDealer.RecordTypeId = tiposRegistro.get('Evaluaciones_Dealer__c').get('Evaluacion_Kodawari').Id;
            evalDealer.RecordType = tiposRegistro.get('Evaluaciones_Dealer__c').get('Evaluacion_Kodawari');
        }
		
        Map<Id, User> valconsultores = EvaluacionKodawariWizardController.getConsultoresRA();

        Map<Id,Seccion__c> valsecciones;
        if(evalDealer.RecordTypeId == tiposRegistro.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id){
            evalDealer.RecordType = tiposRegistro.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes');
            valsecciones = EvaluacionKodawariWizardController.getSeccionesRA('SeccionesRetencionClientes');
        }else{
            valsecciones = EvaluacionKodawariWizardController.getSeccionesRA('Seccion_Kodawari');
        }

        userSeccion = new Map<Id,List<Seccion__c>>();
        for(Relacion_Seccion_Consultor__c rsc : evalDealer.Relaciones_Seccion_Consultor__r){
            if(!userSeccion.containsKey(rsc.Usuario__r.Id)){
                userSeccion.put(rsc.Usuario__r.Id, new List<Seccion__c>());
            }
            userSeccion.get(rsc.Usuario__r.Id).add(rsc.Seccion_Toyota_Mexico__r);

            if(valconsultores.containsKey(rsc.Usuario__r.Id)){
                valconsultores.remove(rsc.Usuario__r.Id);
            }
            if(valsecciones.containsKey(rsc.Seccion_Toyota_Mexico__r.Id)){
                valsecciones.remove(rsc.Seccion_Toyota_Mexico__r.Id);
            }
        }
        if(valconsultores.containsKey(evalDealer.Consultor_TSM_Titular__r.Id)){
            valconsultores.remove(evalDealer.Consultor_TSM_Titular__r.Id);
        }
        consultores = valconsultores.values();
        secciones = valsecciones.values();
        
        // BEGIN //
        Set<Id> sUsers = new Set<Id>();
        for (Relacion_Seccion_Consultor__c r : evalDealer.Relaciones_Seccion_Consultor__r) {
            if (!sUsers.contains(r.Usuario__c)) {
                sUsers.add(r.Usuario__c);
            }
        }
        
        for (Id u : sUsers) {
            WrapperRSC wRSC = new WrapperRSC();
            wRSC.usuario = [SELECT Id, Name FROM User Where Id = :u LIMIT 1];
            for (Relacion_Seccion_Consultor__c r : evalDealer.Relaciones_Seccion_Consultor__r) {
                if (r.Usuario__r.Id == u) {
                    wRSC.sList.add(r.Seccion_Toyota_Mexico__r);
                }
            }
            wrscList.add(wRSC);
        }
        //  END  //
        
    }

    public List<SelectOption> getTipoCetificaciones() {
        return EvaluacionKodawariWizardController.getTipoCetificacionesRA(evalDealer.RecordTypeId);
    }
    
    // BEGIN //
    public class WrapperRSC {
        public User usuario {get;set;}
        public List<Seccion__c> sList {get;set;}
        
        public WrapperRSC() {
            usuario = new User();
            sList = new List<Seccion__c>();
        }
    }
    //  END  //

    @RemoteAction
    global static List<SelectOption> getTipoCetificacionesRA(Id idRTEval) {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Evaluaciones_Dealer__c.Tipo_de_Evaluacion_Kodawari__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        if(Constantes.PROFILES_COMMUNITY_AUTOEVAL_ONLY.contains(Userinfo.getProfileid())){
            options.add(new SelectOption(Constantes.EVALUACIONES_KODAWARI_DEALER_TIPOS_AUTOEVALUACION, Constantes.EVALUACIONES_KODAWARI_DEALER_TIPOS_AUTOEVALUACION));
        }else if(Constantes.TIPOS_REGISTRO_DEVNAME_ID.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id==idRTEval){
            options = Constantes.EVALUACIONES_RETENCION_TIPOS;
        }else{
            for( Schema.PicklistEntry f : ple){
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }
        return options;

    }

    @RemoteAction
    global static List<Account> getAccountACRA(String txtName){
        try {
            String soql = 'SELECT Id, Name From Account WHERE RecordType.DeveloperName=\'Dealer\' and Name LIKE \'%' + String.escapeSingleQuotes(txtName) + '%\' order by Name limit 20';
            return Database.query(soql);
        }
        catch (Exception e) {
            System.debug(e.getMessage());
        }
        return new List<Account>();
    }

    @RemoteAction
    global static Map<Id,User> getConsultoresRA(){
        return new Map<Id,User>([SELECT Id, Name, Profile.Name FROM User WHERE ProfileId =: Constantes.PROFILE_CONSULTOR_TSM and IsActive=true]);
    }

    @RemoteAction
    global static Map<Id,Seccion__c> getSeccionesRA(String tipo){
        return new Map<Id,Seccion__c>([SELECT Id, Name FROM Seccion__c WHERE RecordType.DeveloperName=:tipo and Activo__c=true]);
    }

	//FIXME Quitar
    /*@RemoteAction
    global static Map<Id,Seccion__c> getSeccionesRA(){
    	String tipo = 'Seccion_Kodawari';
        return new Map<Id,Seccion__c>([SELECT Id, Name FROM Seccion__c WHERE RecordType.DeveloperName=:tipo and Activo__c=true]);
    }*/
    
    // BEGIN Guardar Wizard //
    
    // Save Wizard //
    public void saveWizard() {
        
        Savepoint sp = Database.setSavepoint();
        try{
            integer start;
            start=1;
            Integer max;
            max=2;
            
            // Dealer //
            // Id dealerSelected
            
            // Consultor Titular //
            // Se considera la posibilidad de que los titulares puedan ser más de uno.
            List<String> titularElegidoSplit = new List<String>();
            titularElegidoSplit = titularElegido.split('\\,');
            if(titularElegidoSplit.size()>0){
                titular = titularElegidoSplit[0];
                evalDealer.Consultor_TSM_Titular__c = titular;
            }

            // Secciones-Consultor //
            List<String> seccionesConsultorSplit = new List<String>();
            System.debug('Secciones consultor: ' + seccionesConsultor);
            seccionesConsultorSplit = seccionesConsultor.split('\\,');
            System.debug('seccionesConsultorSplit: ' + seccionesConsultorSplit);
            List<Id> consultorSeccion = new List<Id>();
            
            //
            dealerSelectedId = Id.valueOf(dealerSelected);
            
            
            evalDealer.Nombre_Dealer__c = dealerSelected;
            
            System.debug(evalDealer);
            if(evalDealer.Id==null){
                insert evalDealer;
            }
            System.debug(evalDealer);

            //fin guardar objeto evaluaciones dealers

            Set<Id> idsSecciones = new Set<Id>();
            for (String sc : seccionesConsultorSplit) {
                consultorSeccion.clear();
                consultorSeccion = sc.split('\\-');
                idsSecciones.add(consultorSeccion[1]);
            }

            Map<String,Relacion_Seccion_Consultor__c> mapRelacionSecciones = new Map<String,Relacion_Seccion_Consultor__c>();
            //for(Relacion_Seccion_Consultor__c rsc : [SELECT Id, Evaluacion_Dealer__c, Usuario__c, Seccion_Toyota_Mexico__c FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c = :evalDealer.Id AND Seccion_Toyota_Mexico__c IN :idsSecciones]){
            //    mapRelacionSecciones.put(rsc.Seccion_Toyota_Mexico__c, rsc);
            //}
            List<Relacion_Seccion_Consultor__c> relUsrSecc = [SELECT Id, Evaluacion_Dealer__c, Usuario__c, Seccion_Toyota_Mexico__c FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c = :evalDealer.Id];
            if(relUsrSecc.size()>0){
            	delete relUsrSecc;
            }
            System.debug(mapRelacionSecciones);

            for (String sc : seccionesConsultorSplit) {
                consultorSeccion.clear();
                consultorSeccion = sc.split('\\-');
                System.debug('consultorSeccion: ' + consultorSeccion);
                String seccionItem = consultorSeccion[1];
                String userItem = consultorSeccion[0];

                if (mapRelacionSecciones.containsKey(seccionItem)) {
                    mapRelacionSecciones.get(seccionItem).Usuario__c = userItem;
                } else {
                    Relacion_Seccion_Consultor__c rsc = new Relacion_Seccion_Consultor__c(
                        Evaluacion_Dealer__c = evalDealer.Id,
                        Usuario__c = userItem,
                        Seccion_Toyota_Mexico__c = seccionItem
                    );
                    System.debug(rsc);
                    insert rsc;
                    System.debug(rsc);
                    mapRelacionSecciones.put(seccionItem, rsc);
                }
                
            }
            System.debug(mapRelacionSecciones);
            
            //upsert mapRelacionSecciones.values();
            update evalDealer;
            System.debug(evalDealer);

            saveDone = true;
            saveResult = 'Guardado correctamente';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, saveResult));
        }catch(DMLException  dmle){
            Database.rollback(sp);
            saveDone = false;
            saveResult = 'Error al guardar. \n'+dmle.getdmlMessage(0);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, saveResult));
            System.debug(dmle.getStackTraceString());
        }catch(Exception e){
            Database.rollback(sp);
            saveDone = false;
            saveResult = 'Error al guardar. \n'+e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, saveResult));
            System.debug(e.getMessage());
        }
    }
    
    //  END  Guardar Wizard //

}