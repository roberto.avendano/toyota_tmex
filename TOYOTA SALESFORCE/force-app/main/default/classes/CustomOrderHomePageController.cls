public with sharing class CustomOrderHomePageController {
	public Order currentOrder{get;set;}
	public List<PBEWrapper> returnProds{get;set;}
	public List<PBEWrapper> prodSelected{get;set;}
	public String filter{get;Set;}
	public String indexUnSelect{get; set;}
	public String orderItemID{get;set;}
	private List<OrderItem> orderItms;

	private static String CAMPOS_BUSQUEDA_PRODUCTOS = 'Id, Product2Id, Product2.RecordType.DeveloperName, Product2.Name, Product2.ProductCode, Product2.Description, Product2.CantidadMaxima__c, Pricebook2Id, UnitPrice';
   	public Set<String> prodsRepetidos{get;set;}
   	private Set<Id> currentItems;
   	//private Map<Id, PricebookEntry> pricesEntries;

    public CustomOrderHomePageController(ApexPages.StandardController controller) {
        returnProds = new List<PBEWrapper>();
        prodSelected = new List<PBEWrapper>();
        prodsRepetidos = new Set<String>();
        //pricesEntries = new Map<Id, PricebookEntry>();

		currentOrder = [SELECT Id, Name, Pricebook2Id FROM Order WHERE Id=:controller.getId()];

		if(currentOrder.Pricebook2Id==null){
			ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosDestino');
			List<PriceBook2> lpbPR = [SELECT Id From PriceBook2 WHERE Name=:pc.Valor__c and IsActive=true];
			if(lpbPR!=null && lpbPR.size()==1){
				PriceBook2 pbPR = lpbPR.get(0);
				currentOrder.Pricebook2Id = pbPR.Id;
			}else{
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No se encontro la lista de precios "' + pc.Valor__c + '"'));
			}
		}
		indexUnSelect = '';
	}


	public Set<Id> currentOrderItems(){
		Set<Id> pricebookID = new Set<Id>();
		for(OrderItem item: [SELECT Id, PricebookEntryId FROM OrderItem WHERE OrderId =: currentOrder.Id]){
			pricebookID.add(item.PricebookEntryId);
		}
		return pricebookID;
	}

	public PageReference iniciarVariables(){
		returnProds.clear();
		prodSelected.clear();
		prodsRepetidos.clear();
		filter = '';
		return null;
	}

	public PageReference agregarRegresar(){
		returnProds.clear();
		prodsRepetidos.clear();
		filter = '';
		return null;
	}

	public Integer getProdRepetidosSize(){
		return prodsRepetidos.size();
	}

	public PageReference buscar(){
        returnProds.clear();
        prodsRepetidos.clear();

		if(filter.length()>=3){
			String querySelect = 'SELECT ' + CAMPOS_BUSQUEDA_PRODUCTOS + ' FROM PricebookEntry ';
            currentItems = currentOrderItems();
            String query = querySelect + 
                		 'WHERE Product2.IsActive=true and Product2.PartesRobadas__c=true and (Product2.Name like \'%'+filter+'%\' OR Product2.Description like \'%'+filter+'%\' OR Product2.PNC__r.Name like \'%'+filter+'%\')'+ 
                		 'AND Pricebook2Id=\'' + String.valueOf(currentOrder.Pricebook2Id) + '\' AND Id NOT IN:currentItems LIMIT 100';                    
            List<SObject> misProds = Database.query(query);

            Map<Id,List<Id>> mapProductosFinales = new Map<Id,List<Id>>();
            Set<Id> setProductosFinales = new Set<Id>();
            
            for(PricebookEntry pbe : (List<PricebookEntry>)misProds){
            	if(pbe.Product2.RecordType.DeveloperName == 'ProductoFinal'){
            		setProductosFinales.add(pbe.Product2Id);
            		returnProds.add(new PBEWrapper(pbe,true));
            	}else{
            		returnProds.add(new PBEWrapper(pbe,false));
            	}
            }

            if(setProductosFinales.size() > 0){
            	for(ProductoFinalComponente__c pfc : [SELECT Producto__c, ProductoFinal__c FROM ProductoFinalComponente__c WHERE ProductoFinal__c IN :setProductosFinales]){
            		if(!mapProductosFinales.containsKey(pfc.ProductoFinal__c)){
            			mapProductosFinales.put(pfc.ProductoFinal__c, new List<Id>());
            		}
            		mapProductosFinales.get(pfc.ProductoFinal__c).add(pfc.Producto__c);
            	}

            	if(mapProductosFinales.size()>0){
            		for(PBEWrapper item : returnProds){
            			if(item.pfinal){
            				if(mapProductosFinales.containsKey(item.idProducto)){
            					item.elementosFinal = mapProductosFinales.get(item.idProducto);
            				}
            			}
            		}
            	}
            }
        }
        System.debug(JSON.serialize(returnProds));
		return null;
	}

	private List<PBEWrapper> elementosProductofinal(List<Id> listaproductos, Id idProductoFinal, String productoFinalName){
		System.debug(listaproductos);
		List<PBEWrapper> returnsubProds = new List<PBEWrapper>();
		String querySelect = 'SELECT ' + CAMPOS_BUSQUEDA_PRODUCTOS + ' FROM PricebookEntry ';
        String query = querySelect + 
    		 'WHERE Product2.IsActive=true and Product2.PartesRobadas__c=true and Product2Id IN :listaproductos '+ 
    		 'AND Pricebook2Id=\'' + String.valueOf(currentOrder.Pricebook2Id) + '\' ';                    
        List<SObject> misProds = Database.query(query);
        System.debug(misProds);
        for(PricebookEntry pbe : (List<PricebookEntry>)misProds){
	    	if(pbe.Product2.RecordType.DeveloperName != 'ProductoFinal'){
	    		returnsubProds.add(new PBEWrapper(pbe,true,idProductoFinal, productoFinalName));
	    	}
	    }
	    System.debug(returnsubProds);
		return returnsubProds;
	}

	public PageReference seleccionar(){
		//prodsRepetidos.clear();
		Map<Id, PBEWrapper> uniqueProds = new Map<Id, PBEWrapper>();		
		//Map<Id, PBEWrapper> prodSelectedMap = new Map<Id, PBEWrapper>();

		/*
		if(prodSelected.size() > 0){
			for(PBEWrapper item : prodSelected){
				prodSelectedMap.put(item.priceEntry.Id, item);
			}
		}*/ 
		
		for(PBEWrapper item : returnProds){
			if(item.selected){
				if(item.pfinal && item.elementosFinal.size()>0){
					for(PBEWrapper sub : elementosProductofinal(item.elementosFinal, item.idProducto, item.productoFinalName)){
						/*if(!prodSelectedMap.containsKey(sub.priceEntry.Id)){
							uniqueProds.put(sub.priceEntry.Id, sub);
						} else{
							prodsRepetidos.add(sub.priceEntry.Product2.Name);
						}*/
						//prodSelected.add(sub);
						uniqueProds.put(sub.priceEntry.Id, sub);
					}
				}
				else{
					/*if(!prodSelectedMap.containsKey(item.priceEntry.Id)){
						uniqueProds.put(item.priceEntry.Id, item);
					} else{
						prodsRepetidos.add(item.priceEntry.Product2.Name);
					}*/
					uniqueProds.put(item.priceEntry.Id, item);
					//prodSelected.add(item);
				}				
			}
		}



		prodSelected.addAll(uniqueProds.values());
		//System.debug(JSON.serialize(prodSelected));
		return null;
	}


	public PageReference validaProdRepetido(){
		prodsRepetidos.clear();	
		Map<Id, PBEWrapper> prodSelectedMap = new Map<Id, PBEWrapper>();

		if(prodSelected.size() > 0){
			for(PBEWrapper item : prodSelected){
				prodSelectedMap.put(item.priceEntry.Id, item);
			}
		} 
		
		for(PBEWrapper item : returnProds){
			if(item.selected){
				if(item.pfinal && item.elementosFinal.size()>0){
					for(PBEWrapper sub : elementosProductofinal(item.elementosFinal, item.idProducto, item.productoFinalName)){
						if(prodSelectedMap.containsKey(sub.priceEntry.Id) || currentItems.contains(sub.priceEntry.Id)){
							prodsRepetidos.add(sub.priceEntry.Product2.Name);
						} 
					}
				}
				else{
					if(prodSelectedMap.containsKey(item.priceEntry.Id) || currentItems.contains(item.priceEntry.Id)){
						prodsRepetidos.add(item.priceEntry.Product2.Name);
					} 
				}				
			}
		}

		return null;

	}

	public PageReference guardar(){
		orderItms = new List<OrderItem>();
		try{
			System.debug(prodSelected);
			for(PBEWrapper sp : prodSelected){
				if(!sp.excedeCantMax()){					
					if(!(sp.cantidad=='0')){
						orderItms.add(mapeaOrderItem(sp));
					}
				}
				/*if(sp.pfinal && sp.elementosFinal.size()>0){
					for(PBEWrapper sub : elementosProductofinal(sp.elementosFinal,sp.idProducto, '')){
						orderItms.add(mapeaOrderItem(sub));
					}
				}else{
					orderItms.add(mapeaOrderItem(sp));
				}*/
			}
			if(orderItms.size()>0){
				update currentOrder;
				insert orderItms;
			}

			iniciarVariables();
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Se agregaron ' + orderItms.size() + ' productos'));
			PageReference redir = new PageReference('/'+currentOrder.Id);
			redir.setRedirect(true);
			return redir;
		}catch(DmlException e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDmlMessage(0)));
			System.debug(e.getDmlMessage(0));
		}
		
		return null;
	}

	private OrderItem mapeaOrderItem(PBEWrapper sp){
		OrderItem obj = new OrderItem();
		obj.OrderId = currentOrder.Id;
		obj.PricebookEntryId = sp.priceEntry.Id;
		obj.Quantity = Decimal.valueOf(sp.cantidad);
		//obj.Quantity = sp.priceEntry.Product2.CantidadMaxima__c;
		obj.UnitPrice = sp.priceEntry.UnitPrice;
		obj.ProductoFinal__c = sp.idProductoFinal;

		return obj;
	}

	public PageReference unSelect(){		
		if(indexUnSelect!=''){
			for(PBEWrapper item : returnProds){
				if(item.priceEntry.Id==indexUnSelect){
					item.selected = false;
				}
			}
			for(Integer i=0 ; i < prodSelected.size() ; i++){
				if(prodSelected.get(i).priceEntry.Id==indexUnSelect){
					prodSelected.remove(i);
				}
			}
			indexUnSelect = '';
		}
		return null;
	}

	public PageReference deleteOrderItem(){
		PageReference pr;		
		OrderItem orderItem= [SELECT Id FROM OrderItem WHERE Id =: orderItemID];					
		if(orderItem!=null){
			Database.DeleteResult drList = Database.delete(orderItem, false);			
			if(!drList.isSuccess()){
				for(Database.Error err: drList.getErrors()){									
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'El siguiente error ha ocurrido: '+err.getMessage()));
				}
			} else{
				pr = new PageReference('/apex/CustomOrderHomePage?id='+ApexPages.currentPage().getParameters().get('id'));
				pr.setRedirect(true);
			}			
		}
				
		return pr;		
	}

	public class PBEWrapper{	
		public PricebookEntry priceEntry{get;set;}
        public Boolean selected{get;set;}
        public Boolean pfinal{get;set;}
        public List<Id> elementosFinal{get;set;}

        public Id idProducto{get;set;}
        public Id idProductoFinal{get;set;}
        public String productoFinalName{get;Set;}
        public String cantidad{get;Set;}

        public PBEWrapper(){
        	this.selected = false;
        	this.elementosFinal = new List<Id>();
        	this.pfinal = false;
        }
        
        public PBEWrapper(PricebookEntry priceEntry){
        	this();
        	this.priceEntry = priceEntry;
        	this.idProducto = priceEntry.Product2Id;
        	this.cantidad = String.valueOf(priceEntry.Product2.CantidadMaxima__c) != null ? String.valueOf(priceEntry.Product2.CantidadMaxima__c): '0';        	
        }
        
        public PBEWrapper(PricebookEntry priceEntry, Boolean pfinal){
        	this(priceEntry);
        	this.pfinal = pfinal;
        	this.productoFinalName = pfinal? priceEntry.Product2.Name : '';
        }
        
        public PBEWrapper(PricebookEntry priceEntry, Boolean pfinal, Id idProductoFinal, String productoFinalName){
        	this(priceEntry,pfinal);
        	this.idProductoFinal = idProductoFinal;
        	this.selected = true;
        	this.productoFinalName = productoFinalName;
        }
        
        public Boolean excedeCantMax(){
        	this.cantidad = this.cantidad!='' ? this.cantidad:'0';
        	return Decimal.valueOf(this.cantidad) > getCantidadMax(this.priceEntry.Product2.CantidadMaxima__c);
        }

        private Decimal getCantidadMax(Decimal maxQuantity){
        	return maxQuantity!= null ? maxQuantity: 0.0;
        }
	}    
    
}