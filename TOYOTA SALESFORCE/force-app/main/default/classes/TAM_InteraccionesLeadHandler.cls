/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de las 
                        Interacciones_Lead__c que se crean o actualizan.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    26-Septiembre-2021   Héctor Figueroa             Modificación
******************************************************************************* */

public with sharing class TAM_InteraccionesLeadHandler extends TriggerHandler{

    private List<Interacciones_Lead__c> itemsNew;
    private Map<Id, Interacciones_Lead__c> mapItemsOld;    

    public TAM_InteraccionesLeadHandler() {
        this.itemsNew = (List<Interacciones_Lead__c>) Trigger.new;
        this.mapItemsOld = (Map<Id, Interacciones_Lead__c>) Trigger.oldMap;
    }

    //Sobreescribe el metodo de afterInsert
    public override void beforeInsert() {
        System.debug('EN TAM_InteraccionesLeadHandler.beforeInsert....');
        actualizaDatosLeadIntec(this.itemsNew, this.mapItemsOld);
    }

    /*//Sobreescribe el metodo de afterInsert
    public override void beforeUpdate() {
        System.debug('EN TAM_InteraccionesLeadHandler.afterInsert....');
        //actualizaDatosLeadIntec(this.itemsNew, this.mapItemsOld);
    }*/

    //Funcion que sirve para actualizar las listas de precio que se estan crgando en TAM_CatalogoProductos__c
    private void actualizaDatosLeadIntec(List<Interacciones_Lead__c> itemsNew, Map<Id, Interacciones_Lead__c> mapItemsOld){
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec itemsNew: ' + itemsNew);
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapItemsOld: ' + mapItemsOld);      

        Set<String> serIdLead = new Set<String>();
        Map<String, Date> mapIdLead = new Map<String, Date>();
        Map<String, Set<String>> mapIdLeadIdExtInterc = new Map<String, Set<String>>();
        Map<String, Map<String, Boolean>> mapIdLeadIdIntecBool = new Map<String, Map<String, Boolean>>();
        Set<String> serIdExtIntec = new Set<String>();
        
        //Recorre la lista de Interacciones_Lead__c y actueliza los campos que se necesitan para el PushTopic
        for (Interacciones_Lead__c objIntecLead : itemsNew){
            //Toma el id del candidato
            if (objIntecLead.TAM_Candidato__c != null && objIntecLead.TAM_IntegracionSirena__c )
                serIdLead.add(objIntecLead.TAM_Candidato__c);
        }//Fin del for para itemsNew
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec serIdLead: ' + serIdLead);
        
        //Cosulta los datos del prospecto
        for (Lead objCand : [Select id, TAM_FechaCreacionSirena__c From Lead 
            Where id IN:serIdLead ]){
            Date dtFechaCreaCandSirena = !Test.isRunningTest() ? (objCand.TAM_FechaCreacionSirena__c != null ? objCand.TAM_FechaCreacionSirena__c.Date() : Date.today()) : Date.today();
            //Metelo al mapa de mapIdLead
            mapIdLead.put(objCand.id, dtFechaCreaCandSirena);
        }
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLead: ' + mapIdLead.keySet());
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLead: ' + mapIdLead.values());
        
        //Recorre la lista de reg de las Interacciones_Lead__c y ve si son de diferentes fechas
        for (Interacciones_Lead__c objIntecLead : itemsNew){
            //Toma el id del candidato
            if (objIntecLead.TAM_Candidato__c != null){
                if (mapIdLead.containsKey(objIntecLead.TAM_Candidato__c)){                
	                Date dtFechaCreaCandSirena = !Test.isRunningTest() ? mapIdLead.get(objIntecLead.TAM_Candidato__c) : Date.today();
	                Date dtFechaContacto = !Test.isRunningTest() ? objIntecLead.TAM_FechaContacto__c.Date() : Date.today();
	                //Obten la diferencia entre las dos fechas
	                Integer numberDaysDue = dtFechaCreaCandSirena.daysBetween(dtFechaContacto); 
                    System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec numberDaysDue: ' + numberDaysDue);
					//Si es mayor o igual a 1 entonces prende la bandera de TAM_Notificar__c
					if (numberDaysDue >= 1 || Test.isRunningTest()){
					    //Metelo al mapa de mapIdLeadIdExtInterc
					    if (mapIdLeadIdExtInterc.containsKey(objIntecLead.TAM_Candidato__c))
					       mapIdLeadIdExtInterc.get(objIntecLead.TAM_Candidato__c).add(objIntecLead.TAM_IdExternoSirena__c);
                        if (!mapIdLeadIdExtInterc.containsKey(objIntecLead.TAM_Candidato__c))
                           mapIdLeadIdExtInterc.put(objIntecLead.TAM_Candidato__c, new Set<String>{objIntecLead.TAM_IdExternoSirena__c});
					    //Metelo al mapa de serIdExtIntec
					    serIdExtIntec.add(objIntecLead.TAM_IdExternoSirena__c);
					    //objIntecLead.TAM_Notificar__c = true;
	                    //System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec TAM_Notificar__c: ' + objIntecLead.TAM_Notificar__c );
					}//Fin si numberDaysDue >= 1
                }//Fin si mapIdLead.containsKey(objIntecLead.TAM_Candidato__c
            }//Fin si objIntecLead.TAM_Candidato__c != null
        }//Fin del for para itemsNew
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLeadIdExtInterc: ' + mapIdLeadIdExtInterc.keySet());
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLeadIdExtInterc: ' + mapIdLeadIdExtInterc.values());
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec serIdExtIntec: ' + serIdExtIntec);
    
        //Ya tienes las interaciones que vas a buscar consulta y toma el ultimo mov
        for (Interacciones_Lead__c objInteracCons : [Select id, TAM_Candidato__c, TAM_IdExternoSirena__c From Interacciones_Lead__c
            Where TAM_IdExternoSirena__c IN :serIdExtIntec And TAM_Candidato__c IN :mapIdLeadIdExtInterc.keyset() 
            And TAM_Notificar__c = false Order by CreatedDate DESC]){
            //Metelo al mapa de mapIdLeadIdIntecBool
            if (mapIdLeadIdIntecBool.containsKey(objInteracCons.TAM_Candidato__c))
                if (!mapIdLeadIdIntecBool.get(objInteracCons.TAM_Candidato__c).containsKey(objInteracCons.TAM_IdExternoSirena__c))
                    mapIdLeadIdIntecBool.get(objInteracCons.TAM_Candidato__c).put(objInteracCons.TAM_IdExternoSirena__c,true);
            if (!mapIdLeadIdIntecBool.containsKey(objInteracCons.TAM_Candidato__c))
                mapIdLeadIdIntecBool.put(objInteracCons.TAM_Candidato__c, new Map<String, boolean>{objInteracCons.TAM_IdExternoSirena__c => true});
        }//Fin del for para Interacciones_Lead__c
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLeadIdIntecBool: ' + mapIdLeadIdIntecBool.keySet());
        System.debug('EN TAM_InteraccionesLeadHandler.actualizaDatosLeadIntec mapIdLeadIdIntecBool: ' + mapIdLeadIdIntecBool.values());
              
        //Recorre la lista de Interacciones_Lead__c y actueliza los campos que se necesitan para el PushTopic
        for (Interacciones_Lead__c objIntecLead : itemsNew){
            //Toma el id del candidato
            if (objIntecLead.TAM_Candidato__c != null){
                //Buscalo en mapIdLeadIdIntecBool
                if (mapIdLeadIdIntecBool.containsKey(objIntecLead.TAM_Candidato__c)){
                    if (mapIdLeadIdIntecBool.get(objIntecLead.TAM_Candidato__c).containsKey(objIntecLead.TAM_IdExternoSirena__c)){
                        objIntecLead.TAM_Notificar__c = true;
                        //objIntecLead.TAM_UrlCandidato__c = 'https://qa-toyotadealerconnect.cs169.force.com/ToyotaDigital/s/lead/';
                    }
                }
            }
        }//Fin del for para itemsNew
        
    }
        
}