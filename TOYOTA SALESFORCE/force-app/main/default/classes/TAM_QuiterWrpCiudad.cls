/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de Ciudad para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_QuiterWrpCiudad {

    //Ahora Crea el llamado para la serializacion JSON.deserialize
    public List<QuiterCities> cities;    

    public class QuiterCities{
        @AuraEnabled 
        public String id {get;set;}    
        @AuraEnabled 
        public String stateId {get;set;}    
        @AuraEnabled 
        public String description {get;set;}    
                        
        //Un contructor por default
        public QuiterCities(){
            this.description = '';
            this.id = ''; 
            this.stateId = '';
        }
        
        //Un contructor por default
        public QuiterCities(String id, String stateId, String description){
            this.id = id;
            this.stateId = stateId;   
            this.description = description;
        }
    }    
    
}