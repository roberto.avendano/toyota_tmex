global class BatchRepo6m implements Database.Batchable<sObject>,Database.Stateful{
	
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
        	'SELECT Id,Name,fwy_Segmento__c,fwy_Subsegmento__c FROM fwy_ReporteComunidades__c WHERE (fwy_Segmento__c = \'Seguros\' AND fwy_Subsegmento__c=\'Renovación de seguros\')'
        );
    }
    global void execute(Database.BatchableContext bc,List<fwy_ReporteComunidades__c>ReporteComunidad6m){
        Database.delete(ReporteComunidad6m,false);
    }
    global void finish(Database.BatchableContext bc){
        
    }
}