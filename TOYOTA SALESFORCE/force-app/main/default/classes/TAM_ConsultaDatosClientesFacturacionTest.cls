/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS TAM_ConsultaDatosClientesFacturacionCtrl

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    27-Junio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ConsultaDatosClientesFacturacionTest {

    static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
    static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
    static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
    static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
    static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  Lead candidatoPrueba = new Lead();
    static  TAM_LeadInventarios__c leadInventa = new TAM_LeadInventarios__c();
    static  TAM_InventarioVehiculosToyota__c objInvVehToy = new TAM_InventarioVehiculosToyota__c();
    static  TAM_CatalogoCodigosModelo__c objCatCodMod = new TAM_CatalogoCodigosModelo__c();

    @TestSetup static void loadData(){
        
        Account clienteMoral = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización'
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'TestAccount',
            LastName = 'TestAccountLastName',
            Codigo_Distribuidor__c = '570551',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización'
        );
        insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  
        
        Lead cand = new Lead(
            RecordTypeId = VaRtLeadFlotilla,
            FirstName = 'Test12',
            FWY_Intencion_de_compra__c = 'Este mes',    
            Email = 'aw@a.com',
            phone = '224',
            Status='Pedido en Proceso',
            FWY_Estado_del_candidatoWEB__c='Pedido en Proceso',
            LastName = 'Test3',
            FWY_Tipo_de_persona__c = 'Person física',
            TAM_TipoCandidato__c = 'Programa',
            FWY_codigo_distribuidor__c = '570550'
        );
        insert cand;
        System.debug('EN TAM_ConsultaDatosClientesFacturacionOK cand: ' + cand);
        
        TAM_Integraciones__c objTAMIntegraciones = new TAM_Integraciones__c(
          Name = 'Intelisis',
          TAM_username__c = 'pruebaUser@hotmail.com', 
          TAM_password__c = 'X49595KFKF/GKG', 
          TAM_UrlConsultaCtesDealer__c = 'https://apitoy.intelisis-solutions.com:8443/api/clientes', 
          TAM_UrlAutenticacion__c = 'https://apitoy.intelisis-solutions.com:8443/recuperartoken', 
          TAM_GrantType__c = '', 
          TAM_Distribuidor__c = clienteMoral.id, 
          TAM_ApiSecret__c = '', 
          TAM_ApiKey__c = '', 
          TAM_TokenSFDC__c = '', 
          TAM_TipoIntegreacion__c = 'Intelisis'
        );
        insert objTAMIntegraciones;
                
    } 

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }
    
    static testMethod void TAM_ConsultaDatosClientesFacturacionOK() {

    Test.startTest();
        Account clientePaso = [Select id, Name, Codigo_Distribuidor__c, UnidadesAutosDemoAutorizadas__c, TAM_EstatusCliente__c From Account Limit 1];
                
        //Consulta los datos del cliente
        candidatoPrueba = [Select Id, Name, LastName, FirstName, TAM_NombreRL__c, TAM_ApellidoPaternoRL__c, TAM_ApellidoMaternoRL__c,
        FWY_Tipo_de_persona__c, TAM_RFCRL__c, TAM_TelefonoRL__c, TAM_TelefonoCelularRL__c, TAM_CorreoElectronicoRL__c,
        TAM_Calle__c, TAM_CodigoPostal__c, TAM_Colonia__c, TAM_DelegacionMunicipio__c, TAM_Ciudad__c, TAM_Estado__c,
        TAM_Pais__c, Status, FWY_codigo_distribuidor__c From Lead LIMIT 1];        
        System.debug('EN TAM_ConsultaDatosClientesFacturacionOK candidatoPrueba: ' + candidatoPrueba);
        
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getDatosCandidato
        TAM_ConsultaDatosClientesFacturacionCtrl.getDatosCandidato(candidatoPrueba.id);         
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getRetauraDatosCandidato
        TAM_ConsultaDatosClientesFacturacionCtrl.getRetauraDatosCandidato(candidatoPrueba.id);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getRetauraDatosCandidato
        TAM_ConsultaDatosClientesFacturacionCtrl.validaCamposCandidatos(candidatoPrueba.id, candidatoPrueba, 
            new List<TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG>());
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo actualizarDatos
        TAM_ConsultaDatosClientesFacturacionCtrl.actualizarDatos(candidatoPrueba.id, candidatoPrueba, 
        new List<TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG>(), 'sTOtaFac', 
        'sAseVenFac', 'sOrigemFac', 'sAgnFac', 'sEdoPedFac', 'sTVentaFac', 'sTtaDesFac', 
        true, false, false, '234', New Account());
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo confirmaDatosCandidato
        TAM_ConsultaDatosClientesFacturacionCtrl.confirmaDatosCandidato(candidatoPrueba.id, candidatoPrueba, 
        new List<TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG>(), 'sTOtaFac', 
        'sAseVenFac', 'sOrigemFac', 'sAgnFac', 'sEdoPedFac', 'sTVentaFac', 'sTtaDesFac', 
        true, false, false, '345', New TAM_Integraciones__c());
        
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getPermiteConsultarDatosCteDealer
        TAM_ConsultaDatosClientesFacturacionCtrl.getPermiteConsultarDatosCteDealer(candidatoPrueba.id, candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getTipoOfertaFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getTipoOfertaFact(candidatoPrueba);

        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getAsesorVentasFactAuto
        TAM_ConsultaDatosClientesFacturacionCtrl.getAsesorVentasFactAuto(candidatoPrueba.id, candidatoPrueba, clientePaso);
        
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getAsesorVentasFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getAsesorVentasFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getOrigenFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getOrigenFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getAgenciaFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getAgenciaFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getEdoPedInicialFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getEdoPedInicialFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getTipoVentaFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getTipoVentaFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getTipoVentaDestinoFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getTipoVentaDestinoFact(candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getVinesSelecProspectoFact
        TAM_ConsultaDatosClientesFacturacionCtrl.getVinesSelecProspectoFact(candidatoPrueba.id, candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo loginClienteDealer
        TAM_ConsultaDatosClientesFacturacionCtrl.loginClienteDealer(candidatoPrueba.id, candidatoPrueba);
        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getClientesDealer
        TAM_ConsultaDatosClientesFacturacionCtrl.getClientesDealer(candidatoPrueba.id, candidatoPrueba, 'strToken',
            'strAccessToken', new TAM_Integraciones__c());

        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo getEstatusLead
        TAM_ConsultaDatosClientesFacturacionCtrl.getEstatusLead(candidatoPrueba.id);

        //Llama la clase de TAM_ConsultaDatosClientesFacturacionCtrl y metodo verificaEstadoInventario
        TAM_ConsultaDatosClientesFacturacionCtrl.verificaEstadoInventario(candidatoPrueba.id, candidatoPrueba);
        
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos objWrpMapDatosCatalogos = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos();
        objWrpMapDatosCatalogos.strCodigo = '';
        objWrpMapDatosCatalogos.strDescripcion = '';            
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos objWrpMapDatosCatalogos2 = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos('001', 'Descrip');
        
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG objWrpInventarioFyG = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG();
        objWrpInventarioFyG.inventario = new TAM_InventarioVehiculosToyota__c();
        objWrpInventarioFyG.leadInventario = new TAM_LeadInventarios__c();
        objWrpInventarioFyG.sVersion = '';
        objWrpInventarioFyG.Description = '';
        objWrpInventarioFyG.Catalogo = '';
        objWrpInventarioFyG.Carline = '';
        objWrpInventarioFyG.PrecioVenta = 0.00;
        objWrpInventarioFyG.MonedaVenta = '';
        objWrpInventarioFyG.TipoCambioPactado = 0.00;
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG objWrpInventarioFyG2 = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpInventarioFyG(new TAM_InventarioVehiculosToyota__c(), 
                new TAM_LeadInventarios__c());
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos         
        TAM_ConsultaDatosClientesFacturacionCtrl.DatosCliente objDatosCliente = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.DatosCliente();
            objDatosCliente.nombreClienteSFDC = '';
            objDatosCliente.apellidoPaterno = '';
            objDatosCliente.apellidoMaterno = '';
            objDatosCliente.rfc = '';
            objDatosCliente.noCliente = '';
            objDatosCliente.telefono = '';
            objDatosCliente.celular = '';
            objDatosCliente.direccion = '';
            objDatosCliente.cp = '';
            objDatosCliente.localidad = '';
            objDatosCliente.colonia = '';
            objDatosCliente.delegacionMunicipio = '';
            objDatosCliente.estado = '';
            objDatosCliente.pais = '';
            objDatosCliente.Email = '';
            
            //TODOS ESTOS DATOS PARA QUITER
            objDatosCliente.authCommercialCommunications = false;
            objDatosCliente.country = '';
            objDatosCliente.gender = '';
            objDatosCliente.colony = '';
            objDatosCliente.city = '';
            objDatosCliente.document = '';
            objDatosCliente.postalCode = '';
            objDatosCliente.dataProtection2 = false;
            objDatosCliente.id = '';
            objDatosCliente.state = '';
            objDatosCliente.address = '';
            objDatosCliente.fiscalAddress = '';
            objDatosCliente.documentId = '';  
            objDatosCliente.category = '';          
            objDatosCliente.delegation = '';          
    
            objDatosCliente.name = '';
            objDatosCliente.surname = '';
            objDatosCliente.surname2 = '';
            objDatosCliente.mobilePhoneNumber = new List<String>();
            objDatosCliente.phoneNumber = new List<String>();
            
        TAM_ConsultaDatosClientesFacturacionCtrl.DatosCliente objDatosCliente2 = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.DatosCliente('nombreClienteSFDC', 'apellidoPaterno', 'apellidoMaterno', 
            'rfc', 'noCliente', 'telefono', 'celular', 'direccion', 'cp', 'localidad', 'colonia', 'delegacionMunicipio', 
            'estado', 'pais', 'Email');
        
        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos         
        TAM_ConsultaDatosClientesFacturacionCtrl.RequestDatosCliente objRequestDatosCliente = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.RequestDatosCliente();
        objRequestDatosCliente.nombreClienteSFDC = '';
        objRequestDatosCliente.apellidoPaterno = '';
        objRequestDatosCliente.apellidoMaterno = '';
        objRequestDatosCliente.rfc = '';
        objRequestDatosCliente.celular = '';
        objRequestDatosCliente.IdCliente = '';
        objRequestDatosCliente.Email = '';

        TAM_ConsultaDatosClientesFacturacionCtrl.RequestDatosCliente objRequestDatosCliente2 = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.RequestDatosCliente('nombreClienteSFDC', 'apellidoPaterno', 'apellidoMaterno', 
            'rfc', 'IdCliente', '5534567654', '5534567654', 'Email');

        //Crea un objeto del tipo TAM_ConsultaDatosClientesFacturacionCtrl.wrpMapDatosCatalogos         
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpDatosLogin objWrpDatosLogin = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpDatosLogin();
        objWrpDatosLogin.dealerId = '';
        objWrpDatosLogin.apiKey = '';   
        objWrpDatosLogin.apiSecret = '';
        TAM_ConsultaDatosClientesFacturacionCtrl.wrpDatosLogin objWrpDatosLogin2 = 
            new TAM_ConsultaDatosClientesFacturacionCtrl.wrpDatosLogin('dealerId', 'apiKey', 'apiSecret');
    Test.stopTest();    
    }
}