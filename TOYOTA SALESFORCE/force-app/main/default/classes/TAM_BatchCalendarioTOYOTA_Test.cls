@isTest
public class TAM_BatchCalendarioTOYOTA_Test {
    
    static testMethod void testMethod1() 
    {
        List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
        
        //Configuración Personalizada
        ConfigCheckOutDD__c config = new ConfigCheckOutDD__c();
        config.TAM_AnioCalToyota__c = '2021';
        config.TAM_Fleet__c = 'N';
        config.TAM_MesCalToyota__c = '1';
        config.TAM_TipoMovimiento__c = 'RDR';
        config.TAM_TipoRegistro__c = 'Inventario';
        insert config;
        
		//Calendario TOYOTA
		TAM_CalendarioToyota__c calToyota = new TAM_CalendarioToyota__c();
        calToyota.Name = 'Febrero';
        calToyota.TAM_Anio__c = '2021';
        calToyota.TAM_FechaInicio__c = date.today().addDays(-1);
        calToyota.TAM_FechaFin__c = date.today().addMonths(1);
        insert calToyota;
        
        
        Test.startTest();
        
        TAM_BatchCalendarioTOYOTA obj = new TAM_BatchCalendarioTOYOTA();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
        
    }
}