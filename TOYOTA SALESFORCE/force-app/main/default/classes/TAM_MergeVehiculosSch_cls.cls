/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_MergeVehiculosSch_cls y
    					actualizar el cliente en AccountContactRelation

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Agosto-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_MergeVehiculosSch_cls implements Schedulable{

	global String sQuery {get;set;}
	global List<String> lClientes {get;set;}
	global String strClienteFinal {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeVehiculosSch_cls.execute...');
		String sLClientes = '(';
		
		//Recorre la lista de lClientes
		for (String sIdCliente : lClientes){
			sLClientes += '\'' + sIdCliente + '\',';
		}
		sLClientes = sLClientes.substring(0, sLClientes.lastIndexOf(','));
		sLClientes += ')';
		System.debug('EN TAM_MergeVehiculosSch_cls.execute sLClientes: ' + sLClientes);
		
		this.sQuery = 'Select Id, TAM_Cliente__c ';
		this.sQuery += ' From TAM_ClienteVehiculo__c ';
		this.sQuery += ' Where TAM_Cliente__c IN ' + sLClientes;
		this.sQuery += ' Order by TAM_Cliente__c ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeVehiculosSch_cls.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_MergeVehiculosBch_cls objUpdContactsCls = new TAM_MergeVehiculosBch_cls(sQuery, strClienteFinal);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objUpdContactsCls, 100);
			    	 
    }

}