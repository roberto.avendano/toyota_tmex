@isTest
private class CampaniaHandlerTest {

    @TestSetup
    static void makeData(){ 
        //Se obtiene el tipo de registro 'Cliente-Persona Moral' para crear la cuenta
        String tipoRegistroAcc = [Select id from RecordType where sObjectType = 'Account' and DeveloperName ='ClientePersonaMoral'].id;

        //Se crea el registro de  la cuenta que contendra todos los contactos del CRM
        Account accContactos = new Account ();
        accContactos.recordtypeId = tipoRegistroAcc;
        accContactos.Name = 'Cuenta de CRM';
        insert accContactos;
        
        //Obtenemos el tipo de registro para Contacto 'Contactos-Clientes'
        String RecordTypeContacto = [Select id from RecordType where sObjectType = 'Contact' and DeveloperName ='Contactos_Clientes'].id;

        //Se crea un registro de tipo Contact
        Contact clienteContacto = new Contact();
        clienteContacto.recordtypeId = RecordTypeContacto;
        clienteContacto.firstname = 'Hector Figueroa Anguiano';
        clienteContacto.lastName = 'Anguiano';
        clienteContacto.AccountId = accContactos.Id;
        clienteContacto.Email = 'Test@avxtest.com';
        clienteContacto.Phone = '5571123910';
        insert clienteContacto;        


    }


    @isTest
    static void metodoPrueba1(){

        //Obtenemos el contacto que se creo en la data de prueba
        Contact ctcId = [Select id from Contact WHERE lastname  = 'Anguiano'];

        //Obtenemos el registro de las relacion entre cuenta y contacto que se creo en automatico
        AccountContactRelation accCtc = [select id,TAM_VIN__c from AccountContactRelation WHERE ContactId =: ctcId.id];

        //Se modifica el númerpo de VIN para que corresponda al VIN de la camapaña
        accCtc.TAM_VIN__c = 'qwertyui1234567';
        update accCtc;

        //Se crea un registro de campaña de entrada que simulara la carga de los datos
            Campania_Entrada__c registroCampania    = new Campania_Entrada__c();
            registroCampania.name                   = 'AG01';
            registroCampania.Campania__c            =  'AG01';
            registroCampania.Descripcion_Campania__c = 'Registro para clase de prueba';
            registroCampania.Estatus__c = 'ASIGNADO';
            registroCampania.Fecha_Final__c = Date.newInstance(2016, 12, 9);
            registroCampania.Fecha_Inicio__c = Date.newInstance(2016, 12, 9);
            registroCampania.vin__c = 'qwertyui1234567';

            //Se inserta el registro para que se active el trigger
            insert registroCampania;
            system.debug('id del registro de entrada'+registroCampania.id);


    }

}