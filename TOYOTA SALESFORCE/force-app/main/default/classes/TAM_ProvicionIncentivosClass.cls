/** 
    Descripción General: Pantalla de provición de incetivos a distribuidores.
    ________________________________________________________________
        Autor                   Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz            06/Ene/2020         Versión Inicial
    ________________________________________________________________
**/
public without sharing class TAM_ProvicionIncentivosClass {
   
    @AuraEnabled
    public static TAM_ProvisionIncentivos__c getProvisionInfo(String recordId){
        TAM_ProvisionIncentivos__c objProvision = [ SELECT  TAM_MesDeProvision__c,TAM_AnioDeProvision__c 
                                                    FROM    TAM_ProvisionIncentivos__c
                                                    WHERE   Id =:recordId
                                                    LIMIT   1];
        return objProvision;
    } 
}