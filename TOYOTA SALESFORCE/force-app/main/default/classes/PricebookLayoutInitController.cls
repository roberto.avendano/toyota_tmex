public with sharing class PricebookLayoutInitController {
	
	private Pricebook2 currentPricebok;
    private String listViewURL;
    private String listViewName;
    private static String SUFIX_LIST_VIEW = 'C';

	public PricebookLayoutInitController(ApexPages.StandardController controller) {		
        this.currentPricebok = getPricebook2(controller.getId());
        this.listViewName = currentPricebok.Name+ SUFIX_LIST_VIEW;
        String parameters = getListViewID(listViewName)!=null?'&haveList=1&j_id0:j_id2:fcf='+ getListViewID(listViewName): '&haveList=0';
        this.listViewURL = '/apex/PricebookLayoutHome?id='+currentPricebok.Id+ parameters;
	}


    public Pricebook2 getPricebook2(String pricebookID){
        return [SELECT Id, Name FROM Pricebook2 WHERE Id=:pricebookID];
    }

    //Error: List has no rows for assignment to SObject
    public String getListViewID(String listViewName){
        String listViewID;
        try {
            
            listViewID = [SELECT Id, Name FROM ListView WHERE SObjectType='PricebookEntry' AND Name=:listViewName].Id;
            listViewID = listViewID.left(15);
        
        } catch(Exception ex){

        }

        return listViewID;
    }

    public PageReference redirectToHome(){
        PageReference goToHome = new PageReference(listViewURL); //Posible error: Argument 1 cannot be null
        goToHome.setRedirect(true);
        return goToHome;
    }
}