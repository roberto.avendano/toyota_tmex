/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar los registros de la Opp a otra diferente

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    05-Octubre-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeShareOppBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_MergeShareOppBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_MergeShareOppBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        System.debug('EN TAM_MergeShareOppBch.');

		List<Opportunity> lOppUps = new List<Opportunity>();
		Set<String> setNomDistri = new Set<String>();
		
		//Un objeto del tipo Savepoint
		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (Opportunity objOpp : scope){
        	//Remplaza el id de la cuenta en sIdExterno por sClienteFinal
        	Opportunity OppPaso = new Opportunity(id = objOpp.id, AccountId = objOpp.Tam_MergeCuentaFinal__c, TAM_Merge__c = true); 
        	lOppUps.add(OppPaso);
        	System.debug('EN TAM_MergeShareOppBch OppPaso: ' + OppPaso);
        }
       	System.debug('EN TAM_MergeShareOppBch lOppUps: ' + lOppUps);

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lOppUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lOppUps, Opportunity.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareOppBch Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeShareOppBch Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
				
		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_MergeShareOppBch.finish Hora: ' + DateTime.now());    

        Integer intUpdLCteIdAppMinutos = Integer.valueOf(System.Label.TAM_UpdCteIdAppMinutos);
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraFinal = dtHoraActual.addMinutes(intUpdLCteIdAppMinutos); //5
		String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
    	System.debug('EN TAM_MergeShareOppBch.finish CRON_EXP: ' + CRON_EXP);

		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
		TAM_MergeShareOppSch objUpdRtTmexLeadSch = new TAM_MergeShareOppSch();
    	System.debug('EN TAM_MergeShareOppBch.finish objUpdRtTmexLeadSch: ' + objUpdRtTmexLeadSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest())
			System.schedule('TAM_MergeShareOppSch: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objUpdRtTmexLeadSch);          
    } 
        
}