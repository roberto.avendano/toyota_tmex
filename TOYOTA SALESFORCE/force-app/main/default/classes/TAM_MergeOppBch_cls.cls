/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar los registros de la Opp a otra diferente

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    18-Agosto-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeOppBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sClienteFinal;
    
    //Un constructor por default
    global TAM_MergeOppBch_cls(string query, String sClienteFinal){
        this.query = query;
        this.sClienteFinal = sClienteFinal;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_MergeOppBch_cls.start query: ' + this.query + ' sClienteFinal: ' + this.sClienteFinal );
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Opportunity> scope){
        System.debug('EN TAM_MergeOppBch_cls.');

		List<Opportunity> lOppUps = new List<Opportunity>();
		Set<String> setNomDistri = new Set<String>();
		Map<String, String> mapIdGrupoNombre = new Map<String, String>();
		List<TAM_MergeShareClientes__c> lMergeShareClientesShare = new List<TAM_MergeShareClientes__c>();	
		Map<String, TAM_MergeShareClientes__c> mapMergeShareClientesShare = new Map<String, TAM_MergeShareClientes__c>();	
		Set<String> setIdCteFinNomDistri = new Set<String>();
		Set<String> setIdCteFinNomDistriExist = new Set<String>();
		
		//Un objeto del tipo Savepoint
		Savepoint sp = Database.setSavepoint();        

        //Recorre la lista de Casos para cerrarlos 
        for (Opportunity objOpp : scope){
        	if (objOpp.TAM_Distribuidor__c != null){
        		setNomDistri.add(objOpp.TAM_Distribuidor__r.Name);
        		//Mete los datos del cliente final y el nombre del dist
        		setIdCteFinNomDistri.add(this.sClienteFinal + ' - ' + objOpp.TAM_Distribuidor__r.Name);	 
        	}//Fin si objOpp.TAM_Distribuidor__c != null
        }
        System.debug('EN TAM_MergeOppBch_cls setNomDistri: ' + setNomDistri);
        System.debug('EN TAM_MergeOppBch_cls setIdCteFinNomDistri: ' + setIdCteFinNomDistri);

    	//Consulta los grupos asociados a setNomDist
    	if (!setNomDistri.isEmpty()){
	    	for (Group grupoPublico : [Select g.Name, g.Id From Group g
				where Name =: setNomDistri]){
	    		mapIdGrupoNombre.put(grupoPublico.Name, grupoPublico.id);
	    	}
    	}
        System.debug('EN TAM_MergeOppBch_cls mapIdGrupoNombre: ' + mapIdGrupoNombre);
		
		for (TAM_MergeShareClientes__c objMerCtesPaso : [SELECT ID, TAM_IdExterno__c From TAM_MergeShareClientes__c
			Where TAM_IdExterno__c IN :setIdCteFinNomDistri]){
			setIdCteFinNomDistriExist.add(objMerCtesPaso.TAM_IdExterno__c);
		}
        System.debug('EN TAM_MergeOppBch_cls setIdCteFinNomDistriExist: ' + setIdCteFinNomDistriExist);
				
        //Recorre la lista de Casos para cerrarlos 
        for (Opportunity objOpp : scope){
			//Agrega el objeto a la lista de lMergeShareClientesShare
			TAM_MergeShareClientes__c objPasoTAMMergeShareClientes = new TAM_MergeShareClientes__c(
					Name = this.sClienteFinal + ' - ' + objOpp.TAM_Distribuidor__r.Name,
					TAM_Cliente__c = this.sClienteFinal,
					TAM_IdUsuarioShare__c =  mapIdGrupoNombre.get(objOpp.TAM_Distribuidor__r.Name),
					TAM_Procesado__c = false,
					TAM_ActivarProceso__c = false,
					TAM_IdExterno__c = this.sClienteFinal + ' - ' + objOpp.TAM_Distribuidor__r.Name,
					TAM_TipoObjeto__c = 'Clientes'
			);	
			//Ve si el ID no exixte en setIdCteFinNomDistriExist y metelo a la lista de lMergeShareClientesShare
			if (!setIdCteFinNomDistriExist.contains(this.sClienteFinal + ' - ' + objOpp.TAM_Distribuidor__r.Name))
				mapMergeShareClientesShare.put(this.sClienteFinal + ' - ' + objOpp.TAM_Distribuidor__r.Name, objPasoTAMMergeShareClientes);
			//Continua con el proceso
        	String sIdExterno = objOpp.TAM_IdExterno__c; 
        	System.debug('EN TAM_MergeOppBch_cls sIdExterno: ' + sIdExterno);        	
        	//Remplaza el id de la cuenta en sIdExterno por sClienteFinal
        	String sIdExternoFinal = sIdExterno != null ? sIdExterno.replace(objOpp.AccountId, this.sClienteFinal) : '';
        	System.debug('EN TAM_MergeOppBch_cls sIdExternoFinal: ' + sIdExternoFinal);        	
        	Opportunity OppPaso = new Opportunity(id = objOpp.id,
        		TAM_IdExterno__c = sIdExternoFinal, TAM_MergeCuentaAnterior__c = objOpp.AccountId,
        		Tam_MergeCuentaFinal__c = this.sClienteFinal); 
        	lOppUps.add(OppPaso);
        	System.debug('EN TAM_MergeOppBch_cls OppPaso: ' + OppPaso);
        }
       	System.debug('EN TAM_MergeOppBch_cls mapMergeShareClientesShare: ' + mapMergeShareClientesShare);

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
		List<Database.UpsertResult> lDtbUpsResMerge = Database.upsert(mapMergeShareClientesShare.values(), TAM_MergeShareClientes__c.TAM_IdExterno__c,  false);
		//Ve si hubo error
		for (Database.UpsertResult objDtbUpsRes : lDtbUpsResMerge){
			if (!objDtbUpsRes.isSuccess())
				System.debug('EN TAM_MergeAccountCRMCtrl Hubo un error a la hora de crear/Actualizar los registros en TAM_MergeOppBch_cls ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());				
			if (objDtbUpsRes.isSuccess())
				System.debug('EN TAM_MergeAccountCRMCtrl Los datos del TAM_MergeOppBch_cls se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
		}//Fin del for para lDtbUpsResMerge

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lOppUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lOppUps, Opportunity.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeOppBch_cls Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeOppBch_cls Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
				
		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_MergeOppBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}