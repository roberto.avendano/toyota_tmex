/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para obtener el ultimo mov de un vin apartir de una fecha

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    07-Dic-2021          Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_getUltimoMovVin {
 
    //La funcion para poder converttir la hora de envio a un objeto del tipo Time
    public static Movimiento__c getUltimoMovVin(String strVin, Date dtFechaMov, Date dtFechaVenta, Boolean UltMov){
        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin strVin: ' + strVin + ' dtFechaMov: ' + dtFechaMov + ' dtFechaVenta: ' + dtFechaVenta + ' UltMov: ' + UltMov);      
        Movimiento__c objUltMov = new Movimiento__c();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        Boolean TieneSoloUnMov = false;
        Integer intContadorVenta = 0;        
        Date dFechaIniConsultaMov;
        Date dFechaFinConsultaMov = Test.isRunningTest() ? Date.today() : dtFechaMov.addDays(1);
        Boolean blnTieneFechaCalToy = false;
        if (Test.isRunningTest()){
            dtFechaVenta = Date.today();
            dFechaIniConsultaMov = dtFechaVenta;
            dFechaFinConsultaMov = dtFechaVenta.addDays(1);
        }//Fin si Test.isRunningTest()
        //Inicializa la fecha de fin
        Date dtFechaEnvioFinal = Date.newInstance(dtFechaMov.year(), dtFechaMov.month(), dtFechaMov.day());
        if (UltMov)
            dtFechaEnvioFinal = Date.today();                  
        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin dtFechaEnvioFinal: ' + dtFechaEnvioFinal);      
        
        //Busca en el objeto de TAM_CalendarioToyota__c el rango al que le corresponda
        for (TAM_CalendarioToyota__c objCalToy : [Select id, TAM_FechaInicio__c, TAM_FechaFin__c 
            From TAM_CalendarioToyota__c Where TAM_FechaInicio__c <= :dtFechaEnvioFinal 
            And TAM_FechaFin__c >= :dtFechaEnvioFinal]){
            dFechaIniConsultaMov = objCalToy.TAM_FechaInicio__c;
            if (UltMov)
                dFechaFinConsultaMov = objCalToy.TAM_FechaFin__c;
            blnTieneFechaCalToy = true;
        }
        if (UltMov)
            dFechaIniConsultaMov = Date.newInstance(1980,01,01);
        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin: dFechaIniConsultaMov ' + dFechaIniConsultaMov + ' dFechaFinConsultaMov: ' + dFechaFinConsultaMov + ' blnTieneFechaCalToy: ' + blnTieneFechaCalToy);      
        
        //Recorre la lista de Casos para cerrarlos 
        for (Vehiculo__c objVehic : [Select v.Id, v.CreatedDate, 
                (Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, 
                Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, 
                Qty__c, TAM_FechaEnvForm__c, Fleet__c
                From Movimientos__r Where RecordTypeId =: strRecordTypeId 
                and TAM_FechaEnvForm__c >=: dFechaIniConsultaMov 
                and TAM_FechaEnvForm__c <:dFechaFinConsultaMov
                Order by Submitted_Date__c DESC) 
            From Vehiculo__c v Where Name =:strVin ]){  //dtFechaMov
            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin objVehic.Movimientos__r.size(): ' + objVehic.Movimientos__r.size());
            if (!objVehic.Movimientos__r.isEmpty()){
                for (Movimiento__c movPaso : objVehic.Movimientos__r){
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin movPaso: ' + movPaso);            
                }                
            }//Fin si !objVehic.Movimientos__r.isEmpty()
            //Ve si la lista de mov es mayor a 1, toma elID del Vehiculo
            if (objVehic.Movimientos__r.size() > 1 || Test.isRunningTest()){
                Boolean TieneMovMismaFecha = false;
                Map<String, Map<String, Time>> mapDateSubmMapIdMovTime = new Map<String, Map<String, Time>>();
                Movimiento__c objMovPaso = objVehic.Movimientos__r.get(0);
                //Ve que tipo de mov es 
                if (objMovPaso.Trans_Type__c == 'RDR')
                    intContadorVenta += 1;
                if (objMovPaso.Trans_Type__c == 'RVRSL')
                    intContadorVenta += -1;                                        
                Time HoraEnvio = getHoraEnvio(objMovPaso.Submitted_Time__c);
                Date dtFechaEnvio = objMovPaso.Submitted_Date__c.date();
                System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin dtFechaEnvio0: ' + dtFechaEnvio + ' HoraEnvio: ' + HoraEnvio + ' intContadorVenta: ' + intContadorVenta);
                //Toma el primero mov y metelo a al mapa de mapDateSubmMapIdMovTime
                mapDateSubmMapIdMovTime.put(String.valueOf(dtFechaEnvio), new Map<String, Time>{objMovPaso.id => HoraEnvio});
                //Recorre la lista de mov y toma a partir del segundo mof
                for (Integer iCnt = 1; iCnt < objVehic.Movimientos__r.size(); iCnt++){
                   //Toma el mov en la posicion iCnt
                    Movimiento__c objMovPaso2 = objVehic.Movimientos__r.get(iCnt);
	                //Ve que tipo de mov es 
	                if (objMovPaso2.Trans_Type__c == 'RDR')
	                    intContadorVenta += 1;
	                if (objMovPaso2.Trans_Type__c == 'RVRSL')
	                    intContadorVenta += -1;                                        
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta1: ' + intContadorVenta);
                    //Inicializa la fecha del mov
                    Date dtFechaEnvio2 = objMovPaso2.Submitted_Date__c.date();                
                    Time HoraEnvio2 = getHoraEnvio(objMovPaso2.Submitted_Time__c);
                    //Ve si existe la fecha  Submitted_Date__c en el mapa de mapDateSubmMapIdMovTime
                    if (mapDateSubmMapIdMovTime.containsKey( String.valueOf(dtFechaEnvio2) )){
                       mapDateSubmMapIdMovTime.get( String.valueOf(dtFechaEnvio2) ).put(objMovPaso2.id, HoraEnvio2);
                       TieneMovMismaFecha = true;
                    }//Fin si mapDateSubmMapIdMovTime.containsKey(dtFechaEnvio2) 
                }//Fin del for para los mov
                
                //Mas de un Mov con la misma fecha
                if (TieneMovMismaFecha){
                    List<Time> lHoraEnvio = new List<Time>();
                    for (Map<String, Time> MapPaso : mapDateSubmMapIdMovTime.Values()){
                        for (String sIdMov : MapPaso.keySet()){
                            lHoraEnvio.add(MapPaso.get(sIdMov));                            
                        }
                    }//Fin del for para mapDateSubmMapIdMovTime.Values()
                    //Ordena la lista lHoraEnvio
                    lHoraEnvio.sort();
                    Time sFechaEnv = lHoraEnvio.get(0);
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin lHoraEnvio: ' + lHoraEnvio + ' sFechaEnv: ' + sFechaEnv);

                    List<Time> lHoraEnvioPaso = new List<Time>();
                    List<String> lHoraMill = new List<String>();
                    List<String> lHoraEnvioFinal = new List<String>();

                    //Recorre la lista y ver si hay dos registros exactamente iguales en la Hora
                    for (Integer inCntFec = 1; inCntFec < lHoraEnvio.size(); inCntFec++){
                        if ( lHoraEnvio[inCntFec] == sFechaEnv)
                            lHoraEnvioPaso.add(sFechaEnv);
                    }
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin lHoraEnvioPaso: ' + lHoraEnvioPaso);
                    
                    //Tinene algo la lista de lHoraEnvioPaso
                    if (!lHoraEnvioPaso.isEmpty()){
                        //Ve si tiene algo la lista de lHoraEnvioPaso
                        for (Integer iCnt = 1; iCnt < objVehic.Movimientos__r.size(); iCnt++){
                           Movimiento__c objMovPaso3 = objVehic.Movimientos__r.get(iCnt);
	                       //Inicializa la fecha del mov
                           Time HoraEnvio2 = getHoraEnvio(objMovPaso3.Submitted_Time__c);
                           if (HoraEnvio2 == sFechaEnv){
                               String sSubmittedTime = String.valueOf(objMovPaso3.Submitted_Time__c);
                               System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin Submitted_Time__c: ' + sSubmittedTime);
                               List<String> strHoraLst = new List<String>();
                               Integer iPosIni = 0;
				               for (Integer iCntCarc = 0; iCntCarc < sSubmittedTime.length() -1 ; iCntCarc++){
                                   //System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin sSubmittedTime.charAt(iCntCarc): ' + sSubmittedTime.charAt(iCntCarc));
				                   if (sSubmittedTime.charAt(iCntCarc) == 46){
				                      strHoraLst.add(sSubmittedTime.substring(iPosIni,iCntCarc));
                                      //System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin strHoraEnvio: ' + sSubmittedTime.substring(iPosIni,iCntCarc));				                      
				                      iPosIni = iCntCarc + 1;
				                   }//Fin si strHoraEnvio.charAt(iCnt) == 46
				               }//Fin del for para strHoraEnvio
                               String sHoraMill = sSubmittedTime.substring(iPosIni, sSubmittedTime.length());
                               System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin arrSdtFechaEnvio2: ' + sHoraMill + ' HoraEnvio2: ' + HoraEnvio2 + ' sFechaEnv: ' + sFechaEnv);
                               lHoraMill.add(sHoraMill);
                           }//Fin si sdtFechaEnvio2.contains(sFechaEnv)
                        }//Fin del for para los mov
                        //Ordena la lista
                        lHoraMill.sort();
                        
                        String sFechaEnvPaso = String.valueOf(sFechaEnv);
                        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin sFechaEnvPaso: ' + sFechaEnvPaso);
                        String sHoraFinal = sFechaEnvPaso + '.' + lHoraMill.get(lHoraMill.size()-1);
                        //Agrega el ultimo elemento a la lista de lHoraEnvio
                        lHoraEnvioFinal.add( sHoraFinal );
                    }//Fin si !lHoraEnvioPaso.isEmpty()
                    
                    //No tiene nada la lista de lHoraEnvioPaso
                    if (lHoraEnvioPaso.isEmpty())
                        lHoraEnvioFinal.add( String.valueOf(lHoraEnvio.get(lHoraEnvio.size()-1)) );                        
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin lHoraEnvioFinal: ' + lHoraEnvioFinal);
                    
                    //Toma el 1er reg de la lista ordenada
                    Time horaFinal = lHoraEnvio.get(lHoraEnvio.size()-1);
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin lHoraEnvio: ' + lHoraEnvio + ' horaFinal: ' + horaFinal);
                    Boolean blnExiste = false;
                    String sIdMovFinal;
                    //Recorre el mapa de mapDateSubmMapIdMovTime
                    for (Map<String, Time> MapPaso : mapDateSubmMapIdMovTime.Values()){
                        for (String sIdMov : MapPaso.keySet()){
                            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin lHoraEnvio.get(lHoraEnvio.size()-1): ' + lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1) + ' MapPaso.get(sIdMov): ' + String.valueOf(MapPaso.get(sIdMov)) );                    
                            //Toma la hora y ve si se trata de la misma
                            if (lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1) == String.valueOf(MapPaso.get(sIdMov)) ){
                               sIdMovFinal = sIdMov;
                               blnExiste = true; 
                               break;
                            }//Fin si lHoraEnvio.get(0) == MapPaso.get(sIdMov)
                        }//Fin del for para MapPaso.keySet()
                        //Salte de este ciclo si ya lo encontro
                        if (blnExiste)
                            break;
                    }//Fin del for para mapDateSubmMapIdMovTime.Values() 
                    //Ve si blnExiste y crea el objeto del auto para poder actuliza el mov
                    if (blnExiste){
                        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin blnExiste: ' + blnExiste + ' sIdMovFinal: ' + sIdMovFinal);                        
                        List<String> lTimeFinal = new List<String>();
                        for (Movimiento__c objMovFinal : objVehic.Movimientos__r){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sUltHoraMovFinal = sUltHoraMov.left(9); 
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            String sHoraMovFinal = sHoraMov.left(9);
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            if (sUltHoraMovFinal == sHoraMovFinal){
                                lTimeFinal.add(sHoraMov.right(6));
                                //objUltMov = objMovFinal;
                                //break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov EXISTE lTimeFinal: ' + lTimeFinal);
                        lTimeFinal.sort();                        
                        //Toma el ultimo
                        String sHoraMovPaso = lTimeFinal.size() == 1 ? lTimeFinal.get(0) : lTimeFinal.get(lTimeFinal.size()-1);
                        for (Movimiento__c objMovFinal : objVehic.Movimientos__r){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov EXISTE sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            String sHoraMovPasoFinal = sHoraMov.right(6);
                            if (sHoraMovPaso == sHoraMovPasoFinal){
                                objUltMov = objMovFinal;
                                break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos                        
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov objUltMov: ' + objUltMov);
                        
                        /*for (Movimiento__c objMov : [Select m.Id, m.VIN__c, m.Trans_Type__c, Id_Modelo__c, Fleet__c,
                            Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, Qty__c, TAM_FechaEnvForm__c 
                            From Movimiento__c m Where id = :sIdMovFinal And VIN__c != null]){
                            objUltMov = objMov;
                            //Inicializa el campo de Qty__c 
                            objUltMov.Qty__c = String.valueOf(intContadorVenta);
                            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta2: ' + intContadorVenta);
                        }//fin del for para select de Movimiento__c */
                    }//Fin si blnExiste
                    
                    //No existe porque es la mia fecha y hora solo cambian los milisegundos
                    if (!blnExiste){ 
                        List<String> lTimeFinal = new List<String>();
                        for (Movimiento__c objMovFinal : objVehic.Movimientos__r){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sUltHoraMovFinal = sUltHoraMov.left(9); 
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            String sHoraMovFinal = sHoraMov.left(9);
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            if (sUltHoraMovFinal == sHoraMovFinal){
                                lTimeFinal.add(sHoraMov.right(6));
                                //objUltMov = objMovFinal;
                                //break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lTimeFinal: ' + lTimeFinal);
                        lTimeFinal.sort();                        
                        //Toma el ultimo
                        String sHoraMovPaso = lTimeFinal.size() == 1 ? lTimeFinal.get(0) : lTimeFinal.get(lTimeFinal.size()-1);
                        for (Movimiento__c objMovFinal : objVehic.Movimientos__r){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            String sHoraMovPasoFinal = sHoraMov.right(6);
                            if (sHoraMovPaso == sHoraMovPasoFinal){
                                objUltMov = objMovFinal;
                                break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos                        
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov objUltMov: ' + objUltMov);
                    }//Fin si no existe debe ser la misma fecha y hora                    
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin TieneMovMismaFecha: ' + TieneMovMismaFecha + ' objUltMov: ' + objUltMov);
                }//Fin si TieneMovMismaFecha
                
                //Mas de un Mov con la misma fecha
                if (!TieneMovMismaFecha || Test.isRunningTest()){
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin !TieneMovMismaFecha: ' + !TieneMovMismaFecha);                    
                    String sIdMovFinal;
                    //Toma la info del 1er mov ordenado por fecha dtFechaEnvio
                    Map<String, Time> MapPaso = mapDateSubmMapIdMovTime.get(String.valueOf(dtFechaEnvio));
                    for (String sIdMov : MapPaso.keySet()){
                        sIdMovFinal = sIdMov;
                    }//Fin del for para MapPaso.keySet()
                    for (Movimiento__c objMov : [Select m.id, m.VIN__c, m.Trans_Type__c, Id_Modelo__c, Fleet__c,
                         Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, Qty__c, TAM_FechaEnvForm__c, Submitted_Date__c 
                         From Movimiento__c m Where id = :sIdMovFinal And VIN__c != null]){
                         objUltMov = objMov;
                         //Inicializa el campo de Qty__c                         
                         objUltMov.Qty__c = String.valueOf(intContadorVenta);                         
                         System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta2: ' + intContadorVenta);
                    }//fin del for para select de Movimiento__c                     
                }//No tiene movimientos con la misma Fecha
            }//Fin si objVehic.Movimientos__r.size() > 1
	        //Solo tiene un mov
	        if (objVehic.Movimientos__r.size() == 1 || Test.isRunningTest()){
                TieneSoloUnMov = true;
                System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin objVehic.Movimientos__r.size()1: ' + objVehic.Movimientos__r.size());
                Movimiento__c objMov = objVehic.Movimientos__r.get(0);
                //Ve que tipo de mov es 
                if (objMov.Trans_Type__c == 'RDR')
                   intContadorVenta = 1;
                if (objMov.Trans_Type__c == 'RVRSL')
                   intContadorVenta = -1;
                System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta3: ' + intContadorVenta);                   
                //Inicializa el campo de Qty__c                         
	           objUltMov = objMov;
	           objUltMov.Qty__c = String.valueOf(intContadorVenta);
	        }//Fin si objVehic.Movimientos__r.size() == 1
        }//Fin del for para la consulta del Vehiculo y sus mov
        
        //Ve si no tiene movimientos menores a la fecha de 
        if ((TieneSoloUnMov && UltMov) || Test.isRunningTest()){
            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin consulta el mov que tiene...');
	        for (Vehiculo__c objVehic : [Select v.Id, v.Ultimo_Movimiento__c 
	            From Vehiculo__c v Where Name =:strVin ]){
	            //Consulta los datos del mov Ultimo_Movimiento__c
	            for (Movimiento__c objMov : [Select Sale_Date__c, Submitted_Date__c, Submitted_Time__c, 
	               VIN__c, Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, 
	               Year__c, Qty__c, TAM_FechaEnvForm__c, Fleet__c 
	               From Movimiento__c Where id = :objVehic.Ultimo_Movimiento__c]){
                   objUltMov = objMov;     
	                //Ve que tipo de mov es 
	                if (objMov.Trans_Type__c == 'RDR')
	                   intContadorVenta = 1;
	                if (objMov.Trans_Type__c == 'RVRSL')
	                   intContadorVenta = -1;
                    System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta4: ' + intContadorVenta);	                    
	                //Inicializa el campo de Qty__c
                   objUltMov.Qty__c = String.valueOf(intContadorVenta);                                
	            }
	        }
        }//Fin si !TieneSoloUnMov

        if ( (!blnTieneFechaCalToy && !UltMov) || Test.isRunningTest()){
            Movimiento__c objUltMovPaso = new Movimiento__c();
            objUltMov = objUltMovPaso;
            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin intContadorVenta5: ' + intContadorVenta);
            objUltMov.Qty__c = String.valueOf(intContadorVenta);
            objUltMov.VIN__c = null;
            System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin objUltMov0: ' + objUltMov);    
        }
        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin objUltMov1: ' + objUltMov);
        return objUltMov;
    }
 
    //La funcion para poder converttir la hora de envio a un objeto del tipo Time
    public static Time getHoraEnvio(String strHoraEnvio){
        System.debug('EN TAM_getUltimoMovVin.getHoraEnvio strHoraEnvio: ' + strHoraEnvio);      

        Time HoraEnvioPaso;
        List<String> strHoraLst = new List<String>();
        
        //'15.42.11.872359' 15.42.11.872359
        if (strHoraEnvio != null){
            String[] lstValores = strHoraEnvio.split('.');
            //Ve si la hora contiene un punto
            if (strHoraEnvio.contains('.')){
                Integer iPosIni = 0;
                for (Integer iCnt = 0; iCnt < strHoraEnvio.length() -1 ; iCnt++){
                    if (strHoraEnvio.charAt(iCnt) == 46){
                       System.debug('EN TAM_getUltimoMovVin.getHoraEnvio strHoraEnvio: ' + strHoraEnvio.substring(iPosIni,iCnt));                            
                       strHoraLst.add(strHoraEnvio.substring(iPosIni,iCnt));
                       iPosIni = iCnt + 1;
                    }//Fin si strHoraEnvio.charAt(iCnt) == 46
                }//Fin del for para strHoraEnvio
                String sHoraMill = strHoraEnvio.substring(iPosIni, strHoraEnvio.length());
                String sHoraMillFinal = sHoraMill.left(5);
                System.debug('EN TAM_getUltimoMovVin.getHoraEnvio sHoraMillFinal: ' + sHoraMillFinal);
                strHoraLst.add(sHoraMillFinal);
                //Inicializa la hora finalmente
                //HoraEnvioPaso = Time.newInstance(Integer.valueOf(strHoraLst[0]), Integer.valueOf(strHoraLst[1]), Integer.valueOf(strHoraLst[2]), Integer.valueOf(strHoraLst[3]));
                HoraEnvioPaso = Time.newInstance(Integer.valueOf(strHoraLst[0]), Integer.valueOf(strHoraLst[1]), Integer.valueOf(strHoraLst[2]), 00);
            }//Fin si strHoraEnvio.contains('.')
        }//Fin si strHoraEnvio != null

        System.debug('EN TAM_getUltimoMovVin.getHoraEnvio HoraEnvioPaso: ' + HoraEnvioPaso);        
        //Regresa la hora 
        return HoraEnvioPaso;
    }
 
    
}