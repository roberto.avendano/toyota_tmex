/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_CheckOutDetalleSolicitudCompra__c
                        y cancelar las soicitudes que no tienen un mov asociado en DD.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCancelSolInvBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global Boolean blnCanSolMesAntPrm;
    global Date dtFechaIniConsPrm;
    global Date dtFechaFinConsPrm;
    
    //Un constructor por default
    global TAM_ActCancelSolInvBch_cls(string query, Boolean blnCanSolMesAntPrm, 
        Date dtFechaIniConsPrm, Date dtFechaFinConsPrm){
        this.query = query;
        this.blnCanSolMesAntPrm = blnCanSolMesAntPrm;
        this.dtFechaIniConsPrm = dtFechaIniConsPrm;
        this.dtFechaFinConsPrm = dtFechaFinConsPrm;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActCancelSolInvBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_CheckOutDetalleSolicitudCompra__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_ActCancelSolInvBch_cls.');

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
                            
        //Un Objero para el error en caso de exista
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        String sHabilitaMergeAccounts = System.Label.TAM_FechaMesToyota;                
        Set<String> setVinMov = new Set<String>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_MovimientosSolicitudes__c> mapIdExtObjMovSolUps = new Map<String, TAM_MovimientosSolicitudes__c>();

        Date dFechaCancelaPaso = Date.today(); //Date.newInstance(2021,08,03); 
        Date dFechaCancelaFinal = dFechaCancelaPaso.addDays(-1);

        Set<String> setVinConMov = new Set<String>();
        Set<String> setVinCheckOutPedInv = new Set<String>();
        Set<String> setIdSolPedInv = new Set<String>();

        Map<String, TAM_VinesFlotillaPrograma__c> mapVinObjVineFlotProg = new Map<String, TAM_VinesFlotillaPrograma__c>();
        Map<String, TAM_VinesFlotillaPrograma__c> mapVinObjVineFlotProgUps = new Map<String, TAM_VinesFlotillaPrograma__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdCheckoutObjUps = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();                

        //Recorre el mapa de mapIdVinReg 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutConsFin : scope){
        
            Movimiento__c objMovInv = new Movimiento__c();
            List<Movimiento__c> lMonvPasoCons = new List<Movimiento__c>();        
            Map<String, Map<Date, List<Movimiento__c>>> MapVinFechaMovObjMov = new Map<String, Map<Date, List<Movimiento__c>>>();
            Set<Date> setFechaSubmite = new Set<Date>();
            Map<String, Map<Date, TAM_MovimientosSolicitudes__c>> MapVinFechaMovObjMovSol = new Map<String, Map<Date, TAM_MovimientosSolicitudes__c>>();
            
            //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
            String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                //' And TAM_FechaEnvForm__c >= ' + String.valueOf(this.dtFechaIniConsPrm) +
                ' And TAM_FechaEnvForm__c <= ' + String.valueOf(this.dtFechaFinConsPrm) +
                ' And VIN__r.Name = \'' + String.escapeSingleQuotes(objCheckOutConsFin.TAM_VIN__c) + '\'' +
                ' Order by Submitted_Date__c DESC';
            //Ees una prueba
            if (Test.isRunningTest())    
                queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' Order by Submitted_Date__c DESC LIMIT 2';
            System.debug('EN TAM_ActCancelSolInvBch_cls queryMovimiento: ' + queryMovimiento);
            
            //Busca los movimientos    
            List<sObject> lstObjLstMov = new List<sObject>();
            if (this.blnCanSolMesAntPrm)
                lstObjLstMov = Database.query(queryMovimiento);

            //Recorre la lista de los mov por vin y metelos al mapa de
            for (sObject objsObjectPaso : lstObjLstMov){
                Movimiento__c objMovPaso = (Movimiento__c) objsObjectPaso;
                //Metelo a la lista de lMonvPasoCons
                lMonvPasoCons.add(objMovPaso);
            }//Fin del for para lstObjLstMov

            //Ve si la lista lMonvPasoCons tiene algo
            if (!lMonvPasoCons.isEmpty()){
                //Tiene un solo mov
                if (lMonvPasoCons.size() == 1)
                   objMovInv = lMonvPasoCons.get(0);
                //Tiene mas de un mov    
                if (lMonvPasoCons.size() > 1)
                   objMovInv = TAM_ActTotVtaInvBch_cls.getUltMov(lMonvPasoCons);            
                System.debug('EN TAM_ActCancelSolInvBch_cls objMovInv: ' + objMovInv);
                System.debug('EN TAM_ActCancelSolInvBch_cls TAM_NombreDistribuidor__c: ' + objCheckOutConsFin.TAM_NombreDistribuidor__c + ' TAM_CodigoDistribuidorFrm__c: ' + objMovInv.TAM_CodigoDistribuidorFrm__c);
                //Ve si el ultimo mov es una venta
                if (objMovInv.Trans_Type__c == 'RDR'){
                    //Ve si el codigo del deler del mov es diferente de la solicitud
                    if (objCheckOutConsFin.TAM_NombreDistribuidor__c == objMovInv.TAM_CodigoDistribuidorFrm__c)
                        if (!Test.isRunningTest())
                            setVinConMov.add(objCheckOutConsFin.TAM_VIN__c);
                }//Fin si objMovInv.Trans_Type__c == 'RDR'
            }//Fin si !lMonvPasoCons.isEmpty()
                            
        }//Fin del for para la lista de TAM_CheckOutDetalleSolicitudCompra__c        
        System.debug('EN TAM_ActCancelSolInvBch_cls setVinConMov: ' + setVinConMov);

        //Recorre de nuevo la lista de TAM_CheckOutDetalleSolicitudCompra__c
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutConsFin : scope){
            //Toma el vin y buscalos en el set de setVinConMov
            if (!setVinConMov.contains(objCheckOutConsFin.TAM_VIN__c)) {
                setIdSolPedInv.add(objCheckOutConsFin.TAM_SolicitudFlotillaPrograma__c);
                setVinCheckOutPedInv.add(objCheckOutConsFin.TAM_VIN__c);
            }//Fin si !setVinConMov.contains(objCheckOutConsFin.TAM_VIN__c)
        }//Fin del for para scope
        System.debug('EN TAM_ActCancelSolInvBch_cls setIdSolPedInv: ' + setIdSolPedInv);
        System.debug('EN TAM_ActCancelSolInvBch_cls setVinCheckOutPedInv: ' + setVinCheckOutPedInv);
        
        //Consulta los vines asociados a los reg de VinesSolProg
        for (TAM_VinesFlotillaPrograma__c objVinFlotProg : [Select t.Id, t.Name, t.TAM_SolicitudFlotillaPrograma__c, t.TAM_Estatus__c 
            From TAM_VinesFlotillaPrograma__c t Where t.TAM_SolicitudFlotillaPrograma__c 
            IN :setIdSolPedInv And Name  IN:setVinCheckOutPedInv ]){
            //Metelo al mapa de mapVinObjVineFlotProg
            mapVinObjVineFlotProg.put(objVinFlotProg.Name, objVinFlotProg);
        }
        System.debug('EN TAM_ActCancelSolInvBch_cls mapVinObjVineFlotProg: ' + mapVinObjVineFlotProg.keyset());
        System.debug('EN TAM_ActCancelSolInvBch_cls mapVinObjVineFlotProg: ' + mapVinObjVineFlotProg.values());
                
        //Recorre de nuevo la lista de TAM_CheckOutDetalleSolicitudCompra__c
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutConsFin : scope){
            
            //Busca el Vin en el mapa de mapVinObjVineFlotProg
            if (mapVinObjVineFlotProg.containsKey(objCheckOutConsFin.TAM_VIN__c)){
                TAM_VinesFlotillaPrograma__c objVinFlotProgPaso = mapVinObjVineFlotProg.get(objCheckOutConsFin.TAM_VIN__c);
                System.debug('EN TAM_ActCancelSolInvBch_cls objVinFlotProgPaso: ' + objVinFlotProgPaso);

                //Crea el reg en el objeto de 
                TAM_VinesFlotillaPrograma__c objVinFlotProgUps = new TAM_VinesFlotillaPrograma__c(
                        Id = objVinFlotProgPaso.id, TAM_ListaParaCancelar__c = true,
                        TAM_Estatus__c = 'Cancelada',
                        TAM_FechaCancelacion__c = dFechaCancelaFinal,
                        Name = objCheckOutConsFin.TAM_VIN__c
                );
                //Agregar el objeto al mapa de mapIdCheckoutObjUps
                mapVinObjVineFlotProgUps.put(objVinFlotProgPaso.id, objVinFlotProgUps);
                //Crea el registro en el log de errores                
                lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objVinFlotProgPaso.id + '-' + objCheckOutConsFin.TAM_VIN__c, TAM_VIN__c = objCheckOutConsFin.TAM_VIN__c, TAM_Proceso__c = 'Cancelada Vin Flot Prog Inventario Sin Mov en el Mes', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'objVinFlotProgUps' + objVinFlotProgUps) );                            
            }//Fin si mapVinObjVineFlotProg.containsKey(objCheckOutConsFin.TAM_VIN__c)
            
            //Toma el vin y buscalos en el set de setVinConMov
            if (!setVinConMov.contains(objCheckOutConsFin.TAM_VIN__c)) {
                
                //Agregalo al mapa de mapIdCheckoutObjUps 
                TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = new TAM_CheckOutDetalleSolicitudCompra__c(
                        Id = objCheckOutConsFin.id, TAM_ListaParaCancelar__c = true,
                        TAM_EstatusDealerSolicitud__c = 'Cancelada',
                        TAM_FechaCancelacion__c = dFechaCancelaFinal,
                        TAM_VIN__c = objCheckOutConsFin.TAM_VIN__c
                );
                
                //Agregar el objeto al mapa de mapIdCheckoutObjUps
                mapIdCheckoutObjUps.put(objCheckOutConsFin.id, objCheckOutPaso);
                //Crea el registro en el log de errores                
                lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutConsFin.id + '-' + objCheckOutConsFin.TAM_VIN__c, TAM_VIN__c = objCheckOutConsFin.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Inventario Sin Mov en el Mes', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutPaso) );            
                
            }//Fin si !setVinConMov.contains(objCheckOutConsFin.TAM_VIN__c)
        }//Fin del for para scope
        System.debug('EN TAM_ActCancelSolInvBch_cls mapIdExtObjMovSolUps: ' + mapIdExtObjMovSolUps.KeySet());
        System.debug('EN TAM_ActCancelSolInvBch_cls mapIdExtObjMovSolUps: ' + mapIdExtObjMovSolUps.Values());

        System.debug('EN TAM_ActCancelSolInvBch_cls mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.KeySet());
        System.debug('EN TAM_ActCancelSolInvBch_cls mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.Values());

        System.debug('EN TAM_ActCancelSolInvBch_cls mapVinObjVineFlotProgUps: ' + mapVinObjVineFlotProgUps.KeySet());
        System.debug('EN TAM_ActCancelSolInvBch_cls mapVinObjVineFlotProgUps: ' + mapVinObjVineFlotProgUps.Values());

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        

        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdCheckoutObjUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdCheckoutObjUps.values(), TAM_CheckOutDetalleSolicitudCompra__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActCancelSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Checkout ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Ve si tiene algo la lista de mapVinObjVineFlotProgUps
        if (!mapVinObjVineFlotProgUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapVinObjVineFlotProgUps.values(), TAM_VinesFlotillaPrograma__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActCancelSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Vines Flot Prog ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdExtObjMovSolUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdExtObjMovSolUps.values(), TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActCancelSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Roleback a todo
        //Database.rollback(sp);

        //Crea los reg en el objeto de Log
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);
   
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActCancelSolInvBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}