public class Evaluaciones_DealerTriggerHandler extends TriggerHandler {
	
	private Map<Id, Evaluaciones_Dealer__c> newItemMap;
	private List<Evaluaciones_Dealer__c> newItems;

	public Evaluaciones_DealerTriggerHandler() {
		this.newItemMap = (Map<Id, Evaluaciones_Dealer__c>) Trigger.newMap;	
		this.newItems = (List<Evaluaciones_Dealer__c>) Trigger.new;	
	}

	public override void beforeInsert(){
		validaCampos(newItems);
	}

	public override void beforeUpdate(){
		validaCampos(newItems);
	}

	public override void afterInsert() {
		evaluacionesAfter(newItemMap);
	}

	public override void afterUpdate() {
		evaluacionesAfter(newItemMap);
	}

    public static void  evaluacionesAfter(Map<Id,Evaluaciones_Dealer__c> items){
		
		Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
		
		//Selecciona solo las evaluaciones de tipo kodawari
		Id idRTKodawary = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Kodawari').Id;
		Id idRTIntegral = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral').Id;
		Id idRTComoNuevos = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Como_Nuevos').Id;
		Id idRTIntegralCN = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral_Como_Nuevos').Id;
		Id idRTRetencionClientes = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id;


		Id idRTEDER = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_EDER').Id;
		Id idRTSSC = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_SSC').Id;
		Id idRTOS = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_OperacionServicio').Id;
		Id idRTBP = tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_BodyPaint').Id;


		Set<Id> idsKodawaris = new Set<Id>();
		Set<Id> idsIntegrales = new Set<Id>();
		Set<Id> idsIntegralesCN = new Set<Id>();
		Set<Id> idsComoNuevos = new Set<Id>();
		Set<Id> idsRetencionClientes = new Set<Id>();


		Set<Id> idsEDER = new Set<Id>();
		Set<Id> idsSSC = new Set<Id>();
		Set<Id> idsOS = new Set<Id>();
		Set<Id> idsBP = new Set<Id>();


		for(Evaluaciones_Dealer__c ed : items.values()){
			// Agrega evaluaciones para validar la creacion y eliminacion de preguntas relacionadas
			if(ed.RecordTypeId == idRTKodawary && (ed.ResultadoEvaluacion__c==null || ed.ResultadoEvaluacion__c=='')){
				idsKodawaris.add(ed.Id);
			}else if(ed.RecordTypeId == idRTIntegral){
				idsIntegrales.add(ed.Id);
			}else if(ed.RecordTypeId == idRTComoNuevos){
				idsComoNuevos.add(ed.Id);
			}else if(ed.RecordTypeId == idRTIntegralCN){
				idsIntegralesCN.add(ed.Id);
			}else if(ed.RecordTypeId == idRTRetencionClientes){
				idsRetencionClientes.add(ed.Id);
			}else if(ed.RecordTypeId == idRTEDER){
				idsEDER.add(ed.Id);
			}else if(ed.RecordTypeId == idRTSSC){
				idsSSC.add(ed.Id);
			}else if(ed.RecordTypeId == idRTOS){
				idsOS.add(ed.Id);
			}else if(ed.RecordTypeId == idRTBP){
				idsBP.add(ed.Id);
			}
		}
		System.debug('idsKodawaris'+idsKodawaris.size());
		System.debug('idsIntegrales'+idsIntegrales.size());
		System.debug('idsComoNuevos'+idsComoNuevos.size());
		System.debug('idsIntegralesCN'+idsIntegralesCN.size());
		System.debug('idsRetencionClientes'+idsRetencionClientes.size());

		System.debug('idsEDER'+idsEDER.size());
		System.debug('idsSSC'+idsSSC.size());
		System.debug('idsOS'+idsOS.size());
		System.debug('idsBP'+idsBP.size());

		// Manejo de respuestas de evaluaciones integrales
		if(idsComoNuevos.size()>0){
			// Busca el catalogo de preguntas de tipo 
			Map<Id,Pregunta__c> mapPreguntas = new Map<Id,Pregunta__c>([SELECT Id FROM Pregunta__c WHERE RecordType.DeveloperName='Preguntas_Como_Nuevos' and Activo__c=true]);
			Map<Id,Set<Id>> mapEvalPreguntas = new Map<Id,Set<Id>>();
			List<Respuestas_Preguntas_TSM__c> nuevasRespEvaluacion = new List<Respuestas_Preguntas_TSM__c>();
			// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
			for(Respuestas_Preguntas_TSM__c rp : [SELECT Id, Pregunta_Relacionada__c, Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN: idsComoNuevos]){
				if(!mapEvalPreguntas.containsKey(rp.Evaluacion_Dealer__c)){
					mapEvalPreguntas.put(rp.Evaluacion_Dealer__c,new Set<Id>());
				}
				mapEvalPreguntas.get(rp.Evaluacion_Dealer__c).add(rp.Pregunta_Relacionada__c);
			}

			for(Id idEval : idsComoNuevos){
				for(Pregunta__c p : mapPreguntas.values()){
					if(!mapEvalPreguntas.containsKey(idEval) || !mapEvalPreguntas.get(idEval).contains(p.Id)){
						nuevasRespEvaluacion.add(new Respuestas_Preguntas_TSM__c(
							RecordTypeId = tipoRegEvalDealer.get('Respuestas_Preguntas_TSM__c').get('Respuestas_Como_Nuevos').Id,
							Pregunta_Relacionada__c = p.Id,
							Evaluacion_Dealer__c = idEval
						));
					}
				}
			}

			if(nuevasRespEvaluacion.size()>0){
				insert nuevasRespEvaluacion;
			}
		}

		// Manejo de respuestas de evaluaciones integrales Como Nuevos
		if(idsIntegralesCN.size()>0){
			// Busca el catalogo de preguntas de tipo 
			Map<Id,Pregunta__c> mapPreguntas = new Map<Id,Pregunta__c>([SELECT Id FROM Pregunta__c WHERE RecordType.DeveloperName='Preguntas_Evaluacion_Integral_Como_Nuevos' and Activo__c=true]);
			Map<Id,Set<Id>> mapEvalPreguntas = new Map<Id,Set<Id>>();
			List<Respuestas_Preguntas_TSM__c> nuevasRespEvaluacion = new List<Respuestas_Preguntas_TSM__c>();
			// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
			for(Respuestas_Preguntas_TSM__c rp : [SELECT Id, Pregunta_Relacionada__c, Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN: idsIntegralesCN]){
				if(!mapEvalPreguntas.containsKey(rp.Evaluacion_Dealer__c)){
					mapEvalPreguntas.put(rp.Evaluacion_Dealer__c,new Set<Id>());
				}
				mapEvalPreguntas.get(rp.Evaluacion_Dealer__c).add(rp.Pregunta_Relacionada__c);
			}

			for(Id idEval : idsIntegralesCN){
				for(Pregunta__c p : mapPreguntas.values()){
					if(!mapEvalPreguntas.containsKey(idEval) || !mapEvalPreguntas.get(idEval).contains(p.Id)){
						nuevasRespEvaluacion.add(new Respuestas_Preguntas_TSM__c(
							RecordTypeId = tipoRegEvalDealer.get('Respuestas_Preguntas_TSM__c').get('Respuestas_Evaluacion_Integral_Como_Nuevos').Id,
							Pregunta_Relacionada__c = p.Id,
							Evaluacion_Dealer__c = idEval
						));
					}
				}
			}

			if(nuevasRespEvaluacion.size()>0){
				insert nuevasRespEvaluacion;
			}
		}

		// Manejo de respuestas de evaluaciones integrales
		if(idsIntegrales.size()>0){
			// Busca el catalogo de preguntas de tipo 
			Map<Id,Pregunta__c> mapPreguntas = new Map<Id,Pregunta__c>([SELECT Id FROM Pregunta__c WHERE RecordType.DeveloperName='Preguntas_Evaluacion_Integral' and Activo__c=true]);
			Map<Id,Set<Id>> mapEvalPreguntas = new Map<Id,Set<Id>>();
			List<Respuestas_Preguntas_TSM__c> nuevasRespEvaluacion = new List<Respuestas_Preguntas_TSM__c>();
			// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
			for(Respuestas_Preguntas_TSM__c rp : [SELECT Id, Pregunta_Relacionada__c, Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN: idsIntegrales]){
				if(!mapEvalPreguntas.containsKey(rp.Evaluacion_Dealer__c)){
					mapEvalPreguntas.put(rp.Evaluacion_Dealer__c,new Set<Id>());
				}
				mapEvalPreguntas.get(rp.Evaluacion_Dealer__c).add(rp.Pregunta_Relacionada__c);
			}

			for(Id idEval : idsIntegrales){
				for(Pregunta__c p : mapPreguntas.values()){
					if(!mapEvalPreguntas.containsKey(idEval) || !mapEvalPreguntas.get(idEval).contains(p.Id)){
						nuevasRespEvaluacion.add(new Respuestas_Preguntas_TSM__c(
							RecordTypeId = tipoRegEvalDealer.get('Respuestas_Preguntas_TSM__c').get('Respuestas_Evaluacion_Integral').Id,
							Pregunta_Relacionada__c = p.Id,
							Evaluacion_Dealer__c = idEval
						));
					}
				}
			}

			if(nuevasRespEvaluacion.size()>0){
				insert nuevasRespEvaluacion;
			}
		}
		
		// Manejo de respuestas de evaluaciones Kodawari
		if(idsKodawaris.size()>0){
			Set<Id> idsSecciones = new Set<Id>();
			Map<Id, Set<Id>> mapEDSecciones = new Map<Id, Set<Id>>();
		
			// Busca las Relacion_Seccion_Consultor__c que tenga guarda la Evaluacion
			for(Relacion_Seccion_Consultor__c rsc : [SELECT Id, Evaluacion_Dealer__c, Seccion_Toyota_Mexico__c 
				FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c IN :idsKodawaris]){
				if(!mapEDSecciones.containsKey(rsc.Evaluacion_Dealer__c)){
					mapEDSecciones.put(rsc.Evaluacion_Dealer__c, new Set<Id>());
				}
				mapEDSecciones.get(rsc.Evaluacion_Dealer__c).add(rsc.Seccion_Toyota_Mexico__c);
				idsSecciones.add(rsc.Seccion_Toyota_Mexico__c);
			}
	
			Map<Id,List<Pregunta__c>> mapSeccionesPreguntas = new Map<Id,List<Pregunta__c>>();
			Map<Id, Set<Id>> mapPregustaRespuetas = new Map<Id, Set<Id>>();
	
			// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
			for(Pregunta__c p : [SELECT Id,Seccion_Toyota_Mexico__c, (select Id from Objetos_de_Evaluacion_TSM__r) 
				from Pregunta__c Where Seccion_Toyota_Mexico__c IN :idsSecciones and Activo__c=true]){
				if(!mapSeccionesPreguntas.containsKey(p.Seccion_Toyota_Mexico__c)){
					mapSeccionesPreguntas.put(p.Seccion_Toyota_Mexico__c, new List<Pregunta__c>());
				}
				mapSeccionesPreguntas.get(p.Seccion_Toyota_Mexico__c).add(p);
				if(!mapPregustaRespuetas.containsKey(p.Id)){
					mapPregustaRespuetas.put(p.Id, new Set<Id>());
				}
	
				if(p.Objetos_de_Evaluacion_TSM__r!=null && p.Objetos_de_Evaluacion_TSM__r.size()>0){
					for(Objeto_de_Evaluacion_TSM__c ro : p.Objetos_de_Evaluacion_TSM__r){
						mapPregustaRespuetas.get(p.Id).add(ro.Id);
					}
				}
	
			}
	
			Map<String,Respuestas_Preguntas_TSM__c> oldPreguntas = new Map<String,Respuestas_Preguntas_TSM__c>();
	
			// Busca las respuestas de la evaluacion basadas en los ids de items
	        for(Respuestas_Preguntas_TSM__c item : [SELECT Id, Pregunta_Relacionada__c,Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN :idsKodawaris]){
	            oldPreguntas.put(item.Evaluacion_Dealer__c + '-' + item.Pregunta_Relacionada__c, item);
	        }
	
			List<Respuestas_Preguntas_TSM__c> newPreguntas = new List<Respuestas_Preguntas_TSM__c>();
			List<Respuestas_ObjetosTSM__c> newRespuestas = new List<Respuestas_ObjetosTSM__c>();
			// Quita las preguntas existentes de los elementos que cambiaron(antiguos) con la nueva actualización
			for(Id evalDealer : mapEDSecciones.keySet()){

				if(mapSeccionesPreguntas.size()==0){
					items.get(evalDealer).addError('El contenido de esta evaluación ya no esta disponible. Ya no es posible modificar la evaluación.');
				}else{
					for(Id seccion : mapEDSecciones.get(evalDealer)){
						if(mapSeccionesPreguntas.size()>0){
							for(Pregunta__c p : mapSeccionesPreguntas.get(seccion)){
								if(oldPreguntas.containsKey(evalDealer + '-' + p.Id)){
		                            oldPreguntas.remove(evalDealer + '-' + p.Id);
		                        }else{
		                            Respuestas_Preguntas_TSM__c np = new Respuestas_Preguntas_TSM__c(
			    						Evaluacion_Dealer__c = evalDealer, 
			    						Pregunta_Relacionada__c = p.Id
			    					);
			    					newPreguntas.add(np);
		                        }
							}
						}
					}
				}
			}
			
			// Inserta las nuevas preguntas y los registros para guardar las respuestas
			if(newPreguntas.size()>0){
				insert newPreguntas;
				for(Respuestas_Preguntas_TSM__c np : newPreguntas){
					if(mapPregustaRespuetas!=null && mapPregustaRespuetas.containsKey(np.Pregunta_Relacionada__c)){
						for(Id idResp : mapPregustaRespuetas.get(np.Pregunta_Relacionada__c)){
							newRespuestas.add(new Respuestas_ObjetosTSM__c(
								Objeto_Evaluacion_Relacionado__c = idResp, 
								Pregunta_Relacionada_del__c = np.Pregunta_Relacionada__c, 
								Respuestas_Preguntas_TSM_del__c = np.Id
							));
						}
					}
				}
				if(newRespuestas.size()>0){
					insert newRespuestas;
				}
			}
			
			// Elimina lasPreguntas que ya no esten ligadas a la evaluación
	        if(oldPreguntas.size()>0){
	            delete oldPreguntas.values();
	        }
    	}
		
		// Manejo de respuestas de evaluaciones RetencionClientes
		if(idsRetencionClientes.size()>0){
			Set<Id> idsSecciones = new Set<Id>();
			Map<Id, Set<Id>> mapEDSecciones = new Map<Id, Set<Id>>();
		
			// Busca las Relacion_Seccion_Consultor__c que tenga guarda la Evaluacion
			for(Relacion_Seccion_Consultor__c rsc : [SELECT Id, Evaluacion_Dealer__c, Seccion_Toyota_Mexico__c 
				FROM Relacion_Seccion_Consultor__c WHERE Evaluacion_Dealer__c IN :idsRetencionClientes]){
				if(!mapEDSecciones.containsKey(rsc.Evaluacion_Dealer__c)){
					mapEDSecciones.put(rsc.Evaluacion_Dealer__c, new Set<Id>());
				}
				mapEDSecciones.get(rsc.Evaluacion_Dealer__c).add(rsc.Seccion_Toyota_Mexico__c);
				idsSecciones.add(rsc.Seccion_Toyota_Mexico__c);
			}
	
			Map<Id,List<Pregunta__c>> mapSeccionesPreguntas = new Map<Id,List<Pregunta__c>>();
			Map<Id, Set<Id>> mapPregustaRespuetas = new Map<Id, Set<Id>>();
	
			// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
			for(Pregunta__c p : [SELECT Id,Seccion_Toyota_Mexico__c, (select Id from Objetos_de_Evaluacion_TSM__r) 
				from Pregunta__c Where Seccion_Toyota_Mexico__c IN :idsSecciones and Activo__c=true]){
				if(!mapSeccionesPreguntas.containsKey(p.Seccion_Toyota_Mexico__c)){
					mapSeccionesPreguntas.put(p.Seccion_Toyota_Mexico__c, new List<Pregunta__c>());
				}
				mapSeccionesPreguntas.get(p.Seccion_Toyota_Mexico__c).add(p);
				if(!mapPregustaRespuetas.containsKey(p.Id)){
					mapPregustaRespuetas.put(p.Id, new Set<Id>());
				}
	
				if(p.Objetos_de_Evaluacion_TSM__r!=null && p.Objetos_de_Evaluacion_TSM__r.size()>0){
					for(Objeto_de_Evaluacion_TSM__c ro : p.Objetos_de_Evaluacion_TSM__r){
						mapPregustaRespuetas.get(p.Id).add(ro.Id);
					}
				}
	
			}
	
			Map<String,Respuestas_Preguntas_TSM__c> oldPreguntas = new Map<String,Respuestas_Preguntas_TSM__c>();
	
			// Busca las respuestas de la evaluacion basadas en los ids de items
	        for(Respuestas_Preguntas_TSM__c item : [SELECT Id, Pregunta_Relacionada__c,Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN :idsRetencionClientes]){
	            oldPreguntas.put(item.Evaluacion_Dealer__c + '-' + item.Pregunta_Relacionada__c, item);
	        }
	
			List<Respuestas_Preguntas_TSM__c> newPreguntas = new List<Respuestas_Preguntas_TSM__c>();
			List<Respuestas_ObjetosTSM__c> newRespuestas = new List<Respuestas_ObjetosTSM__c>();
			// Quita las preguntas existentes de los elementos que cambiaron(antiguos) con la nueva actualización
			for(Id evalDealer : mapEDSecciones.keySet()){
				for(Id seccion : mapEDSecciones.get(evalDealer)){
					for(Pregunta__c p : mapSeccionesPreguntas.get(seccion)){
						if(oldPreguntas.containsKey(evalDealer + '-' + p.Id)){
                            oldPreguntas.remove(evalDealer + '-' + p.Id);
                        }else{
                            Respuestas_Preguntas_TSM__c np = new Respuestas_Preguntas_TSM__c(
    						Evaluacion_Dealer__c = evalDealer, 
    						Pregunta_Relacionada__c = p.Id
    					);
    					newPreguntas.add(np);
                        }
					}
				}
			}
			
			// Inserta las nuevas preguntas y los registros para guardar las respuestas
			if(newPreguntas.size()>0){
				insert newPreguntas;
				for(Respuestas_Preguntas_TSM__c np : newPreguntas){
					if(mapPregustaRespuetas.containsKey(np.Pregunta_Relacionada__c)){
						for(Id idResp : mapPregustaRespuetas.get(np.Pregunta_Relacionada__c)){
							newRespuestas.add(new Respuestas_ObjetosTSM__c(
								Objeto_Evaluacion_Relacionado__c = idResp, 
								Pregunta_Relacionada_del__c = np.Pregunta_Relacionada__c, 
								Respuestas_Preguntas_TSM_del__c = np.Id
							));
						}
					}
				}
				if(newRespuestas.size()>0){
					insert newRespuestas;
				}
			}
			
			// Elimina lasPreguntas que ya no esten ligadas a la evaluación
	        if(oldPreguntas.size()>0){
	            delete oldPreguntas.values();
	        }
		}

		if(idsEDER.size()>0){
			generaPreguntasObjetos('EDER',idsEDER);
		}

		if(idsSSC.size()>0){
			generaPreguntasObjetos('SSC',idsSSC);
		}

		if(idsOS.size()>0){
			generaPreguntasObjetos('OperacionServicio',idsOS);
		}

		if(idsBP.size()>0){
			generaPreguntasObjetos('BodyPaint',idsBP);
		}
	}

	public static void generaPreguntasObjetos(String sufEvaluacion, Set<Id> idsEval){
		Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;

		System.debug(sufEvaluacion);
		System.debug(idsEval);
		// Busca el catalogo de preguntas de tipo 
		String pregustaEval = 'Preguntas_' + sufEvaluacion;
		Map<Id,Pregunta__c> mapPreguntas = new Map<Id,Pregunta__c>([SELECT Id, (select Id from Objetos_de_Evaluacion_TSM__r) FROM Pregunta__c WHERE RecordType.DeveloperName=:pregustaEval and Activo__c=true and Seccion_Toyota_Mexico__r.Activo__c=true]);
		Map<Id,Set<Id>> mapEvalPreguntas = new Map<Id,Set<Id>>();
		List<Respuestas_Preguntas_TSM__c> nuevasRespEvaluacion = new List<Respuestas_Preguntas_TSM__c>();
		Map<String,Respuestas_Preguntas_TSM__c> oldPreguntas = new Map<String,Respuestas_Preguntas_TSM__c>();
		Map<Id, Set<Id>> mapPregustaRespuetas = new Map<Id, Set<Id>>();
		List<Respuestas_ObjetosTSM__c> newRespuestasObjetos = new List<Respuestas_ObjetosTSM__c>();

		// Busca las Preguntas de las secciones existentes ligadas a las evaluaciones
		for(Respuestas_Preguntas_TSM__c rp : [SELECT Id, Pregunta_Relacionada__c, Evaluacion_Dealer__c FROM Respuestas_Preguntas_TSM__c WHERE Evaluacion_Dealer__c IN:idsEval]){
			if(!mapEvalPreguntas.containsKey(rp.Evaluacion_Dealer__c)){
				mapEvalPreguntas.put(rp.Evaluacion_Dealer__c,new Set<Id>());
			}
			mapEvalPreguntas.get(rp.Evaluacion_Dealer__c).add(rp.Pregunta_Relacionada__c);
			oldPreguntas.put(rp.Evaluacion_Dealer__c + '-' + rp.Pregunta_Relacionada__c, rp);
		}

		Id rtRespEval = tipoRegEvalDealer.get('Respuestas_Preguntas_TSM__c').get('Respuestas_'+sufEvaluacion).Id;
		for(Id idEval : idsEval){
			for(Pregunta__c p : mapPreguntas.values()){
				if(!mapEvalPreguntas.containsKey(idEval) || !mapEvalPreguntas.get(idEval).contains(p.Id)){
						nuevasRespEvaluacion.add(new Respuestas_Preguntas_TSM__c(
							RecordTypeId = rtRespEval,
							Pregunta_Relacionada__c = p.Id,
							Evaluacion_Dealer__c = idEval
						));
					
				}else{
					oldPreguntas.remove(idEval + '-' + p.Id);
				}

				//mapEvalPreguntas.get(idEval).remove(p.Id);
				

				if(!mapPregustaRespuetas.containsKey(p.Id)){
					mapPregustaRespuetas.put(p.Id, new Set<Id>());
				}
	
				if(p.Objetos_de_Evaluacion_TSM__r!=null && p.Objetos_de_Evaluacion_TSM__r.size()>0){
					for(Objeto_de_Evaluacion_TSM__c ro : p.Objetos_de_Evaluacion_TSM__r){
						mapPregustaRespuetas.get(p.Id).add(ro.Id);
					}
				}
			}

		}

		if(nuevasRespEvaluacion.size()>0){
			insert nuevasRespEvaluacion;
				for(Respuestas_Preguntas_TSM__c np : nuevasRespEvaluacion){
					if(mapPregustaRespuetas!=null && mapPregustaRespuetas.containsKey(np.Pregunta_Relacionada__c)){
						for(Id idResp : mapPregustaRespuetas.get(np.Pregunta_Relacionada__c)){
							newRespuestasObjetos.add(new Respuestas_ObjetosTSM__c(
								Objeto_Evaluacion_Relacionado__c = idResp, 
								Pregunta_Relacionada_del__c = np.Pregunta_Relacionada__c, 
								Respuestas_Preguntas_TSM_del__c = np.Id,
								Respuesta__c='No'
							));
						}
					}
				}
				if(newRespuestasObjetos.size()>0){
					insert newRespuestasObjetos;
				}
		}

		// Elimina lasPreguntas que ya no esten ligadas a la evaluación
		if(oldPreguntas.size()>0){
			delete oldPreguntas.values();
		}
	}

	public static void validaCampos(List<Evaluaciones_Dealer__c> items){
		for(Evaluaciones_Dealer__c ed : items){
			if(ed.Consultor_TSM_Titular__c==null){
				ed.Consultor_TSM_Titular__c = ed.OwnerId;
			}
		}
	}
}