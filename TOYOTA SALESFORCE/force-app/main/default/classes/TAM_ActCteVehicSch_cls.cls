/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        Vehiculo__c y verificar que exista el cliente y su relacion con
                        Opp, Opp-Factura, Opp-Vehic, Cte-Vehiv.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCteVehicSch_cls implements Schedulable{
    
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActCteVehicSch_cls.execute...');
        
        String sHoraZonaHoraria = getHoraZonaHoraria();
        System.debug('EN TAM_ActCteVehicSch_cls.execute sHoraZonaHoraria: ' + sHoraZonaHoraria);

        //Para la fecha Ini        
        DateTime dtFechaCreacIni = DateTime.now().addDays(-2);
        DateTime dtFechaCreacIni2 = dtFechaCreacIni.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacIni;
        String sdtFechaCreacIni = String.valueOf(dtFechaCreacIni2);
        String sdtFechaCreacIniFinal = sdtFechaCreacIni.replace(' ', 'T') + 'Z';
        //Para la fecha Fin
        DateTime dtFechaCreacFin = DateTime.now();
        DateTime dtFechaCreacFin2 = dtFechaCreacFin.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacFin;
        String sdtFechaCreacFin = String.valueOf(dtFechaCreacFin2);
        String sdtFechaCreacFinFinal = sdtFechaCreacFin.replace(' ', 'T') + 'Z';
        System.debug('EN TAM_ActCteVehicSch_cls.execute sdtFechaCreacIniFinal: ' + sdtFechaCreacIniFinal + ' sdtFechaCreacFinFinal: ' + sdtFechaCreacFinFinal);
            
        String sQuery = 'Select v.Name, v.Id From Vehiculo__c v';
        sQuery += ' Where CreatedDate >= ' + String.valueOf(sdtFechaCreacIniFinal);
        sQuery += ' And CreatedDate <= ' + String.valueOf(sdtFechaCreacFinFinal);
        sQuery += ' Order by v.Name';

        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 1';
        System.debug('EN TAM_ActCteVehicSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  objActCancelSolInvBch      
        TAM_ActCteVehicBch_cls objActCteVehicBch = new TAM_ActCteVehicBch_cls(sQuery);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActCteVehicBch, 1);

    }
    
    public static string getHoraZonaHoraria(){
        System.debug('EN TAM_ActCteVehicSch_cls.execute getHoraZonaHoraria...');
        String sHoraTimeZone = '';    
        for (User objUsr : [Select u.id, u.TimeZoneSidKey From User u Where id =:UserInfo.getUserId()]){ //UserInfo.getUserId()
            System.debug('EN TAM_ActCteVehicSch_cls.execute objUsr.TimeZoneSidKey: ' + objUsr.TimeZoneSidKey);
            Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                if (pickListVal.getValue() == objUsr.TimeZoneSidKey){
                    System.debug('EN TAM_ActCteVehicSch_cls.execute getLabel(): ' + pickListVal.getLabel() + ' getValue(): ' + pickListVal.getValue());  
                    //Toma los primeros caracteres de la pickListVal.getLabel()
                    String strTimeZonePaso = pickListVal.getLabel().left(11);                                 
                    System.debug('EN TAM_ActCteVehicSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso);
                    String[] arrFechaGmt = strTimeZonePaso.split('-');
                    String[] arrHoraTimeZoneGmtPaso = arrFechaGmt[1].split(':');
                    sHoraTimeZone = arrHoraTimeZoneGmtPaso[0]; 
                    System.debug('EN TAM_ActCteVehicSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso + ' arrFechaGmt: ' + arrFechaGmt + ' arrHoraTimeZoneGmtPaso: ' + arrHoraTimeZoneGmtPaso + ' sHoraTimeZone: ' + sHoraTimeZone);
                }
            }
        }        
        System.debug('sHoraTimeZone : ' + sHoraTimeZone);
        return sHoraTimeZone;     
    }
    
}