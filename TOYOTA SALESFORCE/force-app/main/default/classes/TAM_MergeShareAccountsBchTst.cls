/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_MergeShareAccountsSch y TAM_MergeShareAccountsBch.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    01-Septiembre-2020   Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_MergeShareAccountsBchTst {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
		
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();
		
	static	Account clienteDealer = new Account();
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Rangos__c rango = new Rangos__c();
	static  Opportunity objOpp = new Opportunity();
	
	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;

		TAM_MergeShareClientes__c objMergeShareClientes = new TAM_MergeShareClientes__c(
			Name = clienteDealer.id + ' - ' + clienteDealer.Name,
			TAM_Cliente__c = clienteMoral.id,
			TAM_IdUsuarioShare__c =  UserInfo.getUserId(),
			TAM_Procesado__c = false,
			TAM_ActivarProceso__c = false,
			TAM_IdExterno__c = clienteDealer.id + ' - ' + clienteDealer.Name,
			TAM_TipoObjeto__c = 'Clientes'
		);
		insert objMergeShareClientes;
				             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_MergeShareAccountsBchOK() {

		//Consulta los datos del cliente
		clientePruebaMoral = [Select Id, Name From Account Where RecordTypeId = :sRectorTypePasoPersonaMoral LIMIT 1];        
   		System.debug('EN TAM_MergeShareAccountsBchOK clientePruebaMoral: ' + clientePruebaMoral);

		//Inicia las pruebas    	
        Test.startTest();

			String sQuery = 'Select Id, TAM_Cliente__c, TAM_IdUsuarioShare__c, TAM_Procesado__c, TAM_TipoObjeto__c ';
			sQuery += ' From TAM_MergeShareClientes__c ';
			sQuery += ' Where TAM_Procesado__c != true';
			sQuery += ' Order by TAM_Cliente__c ';
			sQuery += ' Limit 1';
			System.debug('EN TAM_MergeShareAccountsBchOK sQuery: ' + sQuery);
			
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	    	    
			TAM_MergeShareAccountsBch objMergeShareAccBch = new TAM_MergeShareAccountsBch(sQuery);
    	    
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objMergeShareAccBch, 5);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_MergeShareAccountsSch objTAMMergeOppSch = new TAM_MergeShareAccountsSch();       		
       		System.schedule('Ejecuta_TAM_MergeShareAccountsSch:', sch, objTAMMergeOppSch);
       		
        Test.stopTest();
        
    }
}