/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladora para el componete  de TAM_CargaArchivosCteCorporativo.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    23-Agosto-2019       Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_CargaArchivosCteCorporativoCtrl {
 
	public static String VaRtLeadlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	public static String VaRtLeadPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	public static String VaRtLeadClienteCreado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cliente Creado').getRecordTypeId();    
    
    
   @AuraEnabled  
   public static List<ContentDocument> getFiles(string recordId, String nombreArchivo){ 
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.getFiles recordId:  ' + recordId + ' nombreArchivo: ' + nombreArchivo);
        // TO avoid following exception 
        // System.QueryException: Implementation restriction: ContentDocumentLink requires
        // a filter by a single Id on ContentDocumentId or LinkedEntityId using the equals operator or 
        // multiple Id's using the IN operator.
        // We have to add sigle record id into set or list to make SOQL query call
        Set<Id> recordIds=new Set<Id>{recordId};
        Set<Id> documentIds = new Set<Id>(); 
        List<ContentDocumentLink> cdl=[SELECT id,LinkedEntityId,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : recordIds
        And ContentDocument.Description = :nombreArchivo];  //'Acta Constitutiva' 
        for(ContentDocumentLink cdLink:cdl){  
            documentIds.add(cdLink.ContentDocumentId);  
        }      
        return [SELECT Id,Title,FileType FROM ContentDocument WHERE id IN: documentIds];  
    } 
    
   @AuraEnabled  
    public static String updFileDesc(string recordId, String nombreArchivo, String documentId){ 
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.updFileDesc recordId:  ' + recordId + ' nombreArchivo: ' + nombreArchivo + ' documentId: ' + documentId);
        String sFileIdUpd = '';
        Set<Id> recordIds=new Set<Id>{recordId};
        Set<Id> documentIds = new Set<Id>();
        List<ContentDocument> lContentDocumentUpd = new List<ContentDocument>();
        List<Account> lAccountUpd = new List<Account>();
         
        List<ContentDocumentLink> cdl = [SELECT id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : recordIds 
        And ContentDocumentId =: documentId];  //'Acta Constitutiva' 
        
        //Recorre la lista y crea el objeto del tipo : 
        for(ContentDocumentLink cdLink : cdl){
        	lContentDocumentUpd.add(new ContentDocument(id = cdLink.ContentDocumentId, Description = nombreArchivo));
        	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.ContentDocumentLink cdLink.LinkedEntityId:  ' + cdLink.LinkedEntityId + ' nombreArchivo: ' + nombreArchivo);
        	Account objCliente;
        	
        	String sIdArchivo = cdLink.LinkedEntityId;
        	if (sIdArchivo.startsWith('001'))  //0013F00000URlAHQA1
        		objCliente = new Account(id = cdLink.LinkedEntityId);
        	
        	//Cuando se trata de los archivos para el cliente corporativo
        	if (sIdArchivo.startsWith('001')){
	        	if (nombreArchivo == 'Acta Constitutiva') 
					objCliente.TAM_ActaConstitutiva__c = true;        		  
	        	if (nombreArchivo == 'Cedula Fiscal') 
					objCliente.TAM_CedulaFiscal__c = true;
	        	if (nombreArchivo == 'PDF datos adicionales') 
					objCliente.TAM_PdfDatosAdicionalesRepLegal__c = true;  
        		if (nombreArchivo == 'Orden de compra') 
					objCliente.TAM_OrdenCompraCartaCompromiso__c = true; 					      		  
        	}//Fin si sIdArchivo.startsWith('001')
        		       		    
        	//Cuando se trata de los archivos para el cliente de Programa
        	if (sIdArchivo.startsWith('001')){
	        	if (nombreArchivo == 'Identificación oficial') 
					objCliente.TAM_IdentificacionOficial__c = true;        		  
	        	if (nombreArchivo == 'Comprobante de domicilio') 
					objCliente.TAM_ComprobanteDomicilio__c = true;        		  
	        	if (nombreArchivo == 'Documentación de soporte') 
					objCliente.TAM_DocumentacionSoporte__c = true;     
        	}//Fin si sIdArchivo.startsWith('001')
        	
			/*//Cuando se trata de cliente
			if (sIdArchivo.startsWith('001')){
        		if (nombreArchivo == 'Carta Compromiso') 
					objCliente.TAM_CartaCompromiso__c = true;
        		if (nombreArchivo == 'Orden de compra') 
					objCliente.TAM_OrdenCompra__c = true; 					     
			}//Fin si sIdArchivo.startsWith('001')*/
				
			//Metelo a la lista de lLeadUpd
			if (sIdArchivo.startsWith('001'))
        		lAccountUpd.add(objCliente);
        		
        }
		
		//Ve si tiene algo la lista de lContentDocumentUpd y actualiza la Descripción del archivo
        if (!lContentDocumentUpd.isEmpty())
        	update lContentDocumentUpd;
        System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.updFileDesc lContentDocumentUpd:  ' + lContentDocumentUpd);
        
		//Ve si tiene algo la lista de lContentDocumentUpd y actualiza la Descripción del archivo
        if (!lAccountUpd.isEmpty())
        	if (!Test.isRunningTest())
        		update lAccountUpd;
        System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.updFileDesc lAccountUpd:  ' + lAccountUpd);
        
        //Regresa el ID del Reg actualizado 
        return sFileIdUpd;  
    } 
    
        
    @AuraEnabled  
    public static void deleteFiles(string sdocumentId){ 
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.deleteFiles sdocumentId:  ' + sdocumentId);
    	String sIdLead = '';
    	String nombreArchivo = '';
    	//Busca el id del objeto relacionado en ContentDocumentLink
    	for (ContentDocumentLink objContentDocumentLink : [Select c.LinkedEntityId, c.ContentDocumentId, ContentDocument.Description
    		From ContentDocumentLink c
    		Where ContentDocumentId = :sdocumentId ] ){
    		//Ve si el campo de LinkedEntityId empieza por 00Q que se refiere al Lead
    		String sIdLeadPaso = objContentDocumentLink.LinkedEntityId;
    		if (sIdLeadPaso.startsWith('001')){
    			sIdLead = objContentDocumentLink.LinkedEntityId;
    			nombreArchivo = objContentDocumentLink.ContentDocument.Description;
    			break;
    		}//Fin si sIdLeadPaso.startsWith('001')
    	}
    	
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.deleteFiles sIdLead:  ' + sIdLead + ' nombreArchivo: ' + nombreArchivo);
    	//Ya tienes el id el lead y sudescrepcion ajora apaga la bandera del archivo que correspode
    	if (sIdLead != '' && nombreArchivo != ''){
        	Account objCliente = new Account(id = sIdLead);
        	//Cuando se trata de los archivos para el cliente corporativo
        	if (nombreArchivo == 'Acta Constitutiva') 
				objCliente.TAM_ActaConstitutiva__c = false;        		  
        	if (nombreArchivo == 'Cedula Fiscal') 
				objCliente.TAM_CedulaFiscal__c = false;        		  
        	if (nombreArchivo == 'Orden de compra') 
				objCliente.TAM_OrdenCompraCartaCompromiso__c = false;        		  
        	if (nombreArchivo == 'PDF datos adicionales') 
				objCliente.TAM_PdfDatosAdicionalesRepLegal__c = false;        		  
        	//Cuando se trata de los archivos para el cliente de Programa
        	if (nombreArchivo == 'Identificación oficial') 
				objCliente.TAM_IdentificacionOficial__c = false;        		  
        	if (nombreArchivo == 'Comprobante de domicilio') 
				objCliente.TAM_ComprobanteDomicilio__c = false;        		  
        	if (nombreArchivo == 'Documentación de soporte') 
				objCliente.TAM_DocumentacionSoporte__c = false; 
   			System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.deleteFiles objCliente:  ' + objCliente);
			//Metelo a la lista de lLeadUpd
        	if (!Test.isRunningTest())			
        		update objCliente;
    	}//fin si sIdAccount != '' && sFileDescripcion != ''
    	
    	//Finalmente elimina el reg del aechivo
        delete [SELECT Id,Title,FileType from ContentDocument WHERE id=:sdocumentId];       
    }      
    

   @AuraEnabled  
   public static String consultDatosCand(string recordId){ 
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand recordId:  ' + recordId);
    	String sTipoPersona = '';
    	    	
   		for (Account objCliente : [Select id, RecordType.Name From Account Where ID =: recordId]){
   			if (objCliente.RecordType.Name == 'Cliente - Persona Moral' || objCliente.RecordType.Name == 'Cliente - Persona Moral No Vigente')
   				sTipoPersona = 'ClienteCorpPM';	
   			if (objCliente.RecordType.Name == 'Cliente - Persona Fisica' || objCliente.RecordType.Name == 'Cliente - Persona Fisica No Vigente')
   				sTipoPersona = 'ClienteCorpPF';	
   		}
    	
    	System.debug('EN TAM_CargaArchivosCteCorporativoCtrl.consultDatosCand sTipoPersona:  ' + sTipoPersona);
        return sTipoPersona;  
    } 
   
}