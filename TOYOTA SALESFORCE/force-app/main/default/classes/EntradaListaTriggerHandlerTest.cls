@isTest
private class EntradaListaTriggerHandlerTest {
    @testSetup static void loadData(){
        List<Product2> nuevosProductos = new List<Product2>();
        Id standardPricebookId = Test.getStandardPricebookId();

        Pricebook2 pbPartes = new Pricebook2(
            IdExternoListaPrecios__c='LPRPartesRobadas',
            Name='Lista de precios partes robadas',
            IsActive=true
        );
        insert pbPartes;

        Pricebook2 pbStandard = new Pricebook2(
            IdExternoListaPrecios__c='StandardPriceBook',
            Name='Standard Price Book',
            IsActive=true
        );
        upsert pbStandard IdExternoListaPrecios__c;


        for(Integer i=1; i<=10; i++){
            nuevosProductos.add(
            new Product2(
                IdExternoProducto__c='TESTPROD'+i,
                Name='TEST PROD'+i,
                Description= 'TEST PRODUCT DESCRIPTION '+i,             
                IsActive = true
            ));         
        }

        insert nuevosProductos;

        PricebookEntry pbeStandardProd1= new PricebookEntry(
            Pricebook2Id= standardPricebookId,
            Product2Id= nuevosProductos[0].Id,
            UnitPrice= 0.0,
            IdExterno__c= nuevosProductos[0].IdExternoProducto__c+'-StandardPriceBook',
            IsActive = true
        );
        insert pbeStandardProd1;

        PricebookEntry pbeCustom = new PricebookEntry(
            Pricebook2Id=pbPartes.Id,
            UnitPrice=100,
            Product2Id=nuevosProductos[0].Id,           
            IsActive=true,
            IdExterno__c= nuevosProductos[0].IdExternoProducto__c+'-'+pbPartes.IdExternoListaPrecios__c
        );
        insert pbeCustom;

        
    }


    @isTest static void test_one() {
        List<EntradaListaPrecios__c> entradas = new List<EntradaListaPrecios__c>();
        for(Integer i=1; i<=20; i++){
            String parte = '2TESTPROD'+i+'                ';
            entradas.add(new EntradaListaPrecios__c(                
                Parte__c = parte.left(16)+'               6DJSTMNT WARR. CLAIMA1A 0000000001328 0000000001328 000000000132800001 0000000'
            )); 
        }

        insert entradas;

        //System.debug(JSON.serialize([SELECT IdExterno__c FROM PricebookEntry]));

    }  
    
}