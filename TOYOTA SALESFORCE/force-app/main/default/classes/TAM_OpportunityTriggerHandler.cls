/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de las Opp TAM_OpportunityTriggerHandler
                        
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    07-Octubre-2020   Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_OpportunityTriggerHandler extends TriggerHandler{

    private List<Opportunity> itemsNew;
    private Map<ID, Opportunity> mapItemsOld;        
        
    //Un constructor por default
    public TAM_OpportunityTriggerHandler() {
        this.itemsNew = (List<Opportunity>) Trigger.new;
        this.mapItemsOld = (Map<Id, Opportunity>) Trigger.oldMap;
    }

    //Sobreescribe el metodo de beforeUpdate
    public override void afterInsert() {
        System.debug('EN afterInsert....');
        creaColaboracionDistribuidor(itemsNew);     
    }

    //Sobreescribe el metodo de beforeUpdate
    public override void afterUpdate() {
        System.debug('EN afterUpdate....');
        creaColaboracionDistribuidor(itemsNew);
    }

    public static void creaColaboracionDistribuidor(List<Opportunity> lstNewClienteVehiculo){
        Set<String> setIdOwner = new Set<String>();
        Map<String, String> mapIdUsrNomDis = new Map<String, String>();
        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        List<OpportunityShare>  lClienteVehiculoShare = new List<OpportunityShare>();
        Set<String> setIdOpp = new Set<String>();
        Map<String, Opportunity> mapIdOppNoDist = new Map<String, Opportunity>();
        
        System.debug('ENTRO TAM_OpportunityTriggerHandler ...');           
        for(Opportunity objProvision : lstNewClienteVehiculo){
            //Ve si tiene algo el cammpo de OwnerId
            setIdOpp.add(objProvision.id);             
        }
        System.debug('ENTRO TAM_OpportunityTriggerHandler setIdOpp: ' + setIdOpp);           

        for(Opportunity objProvision : [Select id, TAM_Distribuidor__r.Name From Opportunity
            Where id IN :setIdOpp]){
            //Ve si tiene algo el cammpo de OwnerId
            if (objProvision.TAM_Distribuidor__r.Name != null)
                mapIdOppNoDist.put(objProvision.id, objProvision);
        }
        System.debug('ENTRO TAM_OpportunityTriggerHandler mapIdOppNoDist: ' + mapIdOppNoDist.keySet());           
        System.debug('ENTRO TAM_OpportunityTriggerHandler mapIdOppNoDist: ' + mapIdOppNoDist.Values());           
                
        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group WHERE NAME LIKE '%TOYOTA%' OR NAME LIKE '%TMEX%' 
            ORDER BY NAME]){
            mapNomGrupoIdGrupo.put(Gropo.Name, Gropo.id);
        }
        System.debug('ENTRO TAM_OpportunityTriggerHandler mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.keySet());           
        System.debug('ENTRO TAM_OpportunityTriggerHandler mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           
        
        //Crea la colaboracion para el reg 
        for(Opportunity objOpp : mapIdOppNoDist.Values()){
            String IdNomdis = mapIdOppNoDist.containsKey(objOpp.id) ? mapIdOppNoDist.get(objOpp.id).TAM_Distribuidor__r.Name.toUpperCase() : ' ';
            System.debug('ENTRO TAM_OpportunityTriggerHandler IdNomdis: ' + IdNomdis); 
            if (Test.isRunningTest())
                IdNomdis = 'TOYOTA INNOVA FLETEROS';
            OpportunityShare objPasoShare = new OpportunityShare(
                    OpportunityId = objOpp.id,
                    UserOrGroupId = mapNomGrupoIdGrupo.get(IdNomdis),
                    OpportunityAccessLevel = 'Read'
            );
            //Agregalo a la lista lClienteVehiculoShare     
            lClienteVehiculoShare.add(objPasoShare);
            System.debug('ENTRO TAM_OpportunityTriggerHandler objOpp.ID: ' + objPasoShare); 
            if (!mapNomGrupoIdGrupo.containsKey(IdNomdis))
                System.debug('ENTRO TAM_OpportunityTriggerHandler NO EXISTE IdNomdis: ' + IdNomdis);         
        }
        //System.debug('ENTRO TAM_OpportunityTriggerHandler lClienteVehiculoShare IdNomdis: ' + lClienteVehiculoShare);           

        if (!lClienteVehiculoShare.isEmpty()){
            //Actualiza las Opp 
            List<Database.Saveresult> lDtbUpsRes = Database.insert(lClienteVehiculoShare, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el precio de lista de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !lClienteVehiculoShare.isEmpty()
            
    }
    
}