/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de las solicitudes de excepciónes
******************************************************************************* */

@isTest
public class SolicitudExcepcionIncentivos_Test {
    
    @testSetup static void setup() {
        
        //Se declara una fehca
        Date fecha = date.today(); 
        
        //Se inserta el documento adjunto
        document doc = new Document();
        doc.Body = Blob.valueOf('Some Text');
        doc.ContentType = 'application/pdf';
        doc.DeveloperName = 'mydocument';
        doc.IsPublic = true;
        doc.FolderId = UserInfo.getUserId();
        doc.Name = 'My Document';
        insert doc;
        
        //Politica de Incentivos   
        TAM_PoliticasIncentivos__c politicaRetail = new TAM_PoliticasIncentivos__c();
        politicaRetail.Name = 'Politica Test';
        politicaRetail.TAM_InicioVigencia__c = Date.today()-210;
        politicaRetail.TAM_FinVigencia__c = Date.today() + 230;
        politicaRetail.TAM_PeriodoDeGracia__c = 5;
        politicaRetail.TAM_EstatusIncentivos__c = 'Vigente';
        politicaRetail.TAM_CargaHistorica__c = true;
        politicaRetail.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        system.debug('fecha inicio'+politicaRetail.TAM_InicioVigencia__c);
        insert  politicaRetail;
        
        //Detalle de la politica
        TAM_DetallePoliticaJunction__c detallePolitica = new TAM_DetallePoliticaJunction__c();
        detallePolitica.TAM_AnioModelo__c = '2020';
        detallePolitica.TAM_BonoLealtad__c = 5000;
        detallePolitica.TAM_BonoLealtadDealer__c = 2500;
        detallePolitica.TAM_BonoLealtadTMEX__c = 2500;
        detallePolitica.TAM_Descripcion__c = 'Test';
        detallePolitica.Name = 'Detalle de prueba';
        detallePolitica.TAM_IncPropuesto_Cash__c = 10000;
        detallePolitica.TAM_IncDealer_Cash__c = 5000;
        detallePolitica.TAM_IncentivoTMEX_Cash__c = 5000;
        detallePolitica.TAM_Lealtad__c  = true;
        detallePolitica.TAM_PoliticaIncentivos__c = politicaRetail.id;
        detallePolitica.TAM_Serie__c = 'Corolla';
        detallePolitica.TAM_Clave__c = '1780';
        detallePolitica.TAM_Pago_TFS__c = true;
        insert detallePolitica;
        
        //Se inserta un producto 
        Product2 productVIN = new Product2();
        productVIN.Name='1780';
        productVIN.Anio__c = '2020';
        productVIN.IdExternoProducto__c='17802020';
        productVIN.ProductCode='17802020';
        productVIN.CantidadMaxima__c = 5;
        productVIN.IsActive = true;
        insert productVIN;
        
        //Se inserta una lista de precio
        Pricebook2 listaPrecio = new Pricebook2();
        listaPrecio.IsActive = true;
        listaPrecio.Name = 'VH-JUNIO-2020';
        listaPrecio.IdExternoListaPrecios__c = 'VH-JUNIO-2020';
        listaPrecio.InicioVigencia__c = fecha;
        listaPrecio.FinVigencia__c = fecha.addDays(10);
        insert listaPrecio;
        
        //Se inseta una lista de precios
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry precioStandard = new PricebookEntry();
        precioStandard.Pricebook2Id= standardPricebookId;
        precioStandard.Product2Id= productVIN.Id;
        precioStandard.UnitPrice= 0.0;
        precioStandard.IsActive = true;
        precioStandard.PrecioSinIva__c = 211176.00;
        precioStandard.PrecioDealer__c = 67829;
        precioStandard.PrecioPublico__c = 134324;
        precioStandard.IsActive = true;
        insert precioStandard;
        
        //Se inserta una entrada de precios
        PricebookEntry precioEntrada = new PricebookEntry();
        precioEntrada.Pricebook2Id= listaPrecio.id;
        precioEntrada.Product2Id= productVIN.Id;
        precioEntrada.UnitPrice= 0.0;
        precioEntrada.IsActive = true;
        precioEntrada.PrecioSinIva__c = 211176.00;
        precioEntrada.PrecioDealer__c = 67829;
        precioEntrada.PrecioPublico__c = 134324;
        precioEntrada.IsActive = true;
        insert precioEntrada;
        
        //Se inseta una cuneta de distribuidor
        Account acct = new Account();
        acct.name = 'DEALER DE PRUEBA';
        acct.Codigo_Distribuidor__c = '570002';
        insert acct;
                
        //RangoPrograma
        Rangos__c rangoPrograma = new Rangos__c();
        rangoPrograma.name  = 'UBER';
        rangoPrograma.Activo__c = true;
        rangoPrograma.NumUnidadesMinimas__c = 5;
        rangoPrograma.NumUnidadesMaximas__c = 10;
        insert rangoPrograma;
        
        //Politica de Incentivos   
        TAM_PoliticasIncentivos__c politicaVC = new TAM_PoliticasIncentivos__c();
        politicaVC.Name = 'Politica Test';
        politicaVC.TAM_InicioVigencia__c = Date.today()-1;
        politicaVC.TAM_FinVigencia__c = Date.today() + 1;
        politicaVC.TAM_PeriodoDeGracia__c = 5;
        politicaVC.TAM_EstatusIncentivos__c = 'Vigente';
        politicaVC.TAM_CargaHistorica__c = true;
        politicaVC.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
        insert  politicaVC;
        
        //Detalle de la politica 1
        TAM_DetallePoliticaJunction__c detallePoliticavc = new TAM_DetallePoliticaJunction__c();
        detallePoliticavc.TAM_AnioModelo__c = '2020';
        detallePoliticavc.TAM_BonoLealtad__c = 5000;
        detallePoliticavc.TAM_BonoLealtadDealer__c = 2500;
        detallePoliticavc.TAM_BonoLealtadTMEX__c = 2500;
        detallePoliticavc.TAM_Descripcion__c = 'Test';
        detallePoliticavc.Name = 'Detalle de prueba';
        detallePoliticavc.TAM_IncPropuesto_Cash__c = 10000;
        detallePoliticavc.TAM_IncDealer_Cash__c = 5000;
        detallePoliticavc.TAM_IncentivoTMEX_Cash__c = 5000;
        detallePoliticavc.TAM_Lealtad__c  = true;
        detallePoliticavc.TAM_PoliticaIncentivos__c = politicaRetail.id;
        detallePoliticavc.TAM_Serie__c = 'Corolla';
        detallePoliticavc.TAM_Clave__c = '1780';
        detallePoliticavc.TAM_Pago_TFS__c = true;
        detallePoliticavc.TAM_Rango__c = rangoPrograma.id;
        detallePoliticavc.TAM_MSRP__c = 350000;
        insert detallePoliticavc;
        
        //Detalle de la politica 2
        TAM_DetallePoliticaJunction__c detallePoliticaDos = new TAM_DetallePoliticaJunction__c();
        detallePoliticaDos.TAM_AnioModelo__c = '2020';
        detallePoliticaDos.TAM_BonoLealtad__c = 5000;
        detallePoliticaDos.TAM_BonoLealtadDealer__c = 2500;
        detallePoliticaDos.TAM_BonoLealtadTMEX__c = 2500;
        detallePoliticaDos.TAM_Descripcion__c = 'Test';
        detallePoliticaDos.Name = 'Detalle de prueba';
        detallePoliticaDos.TAM_IncPropuesto_Cash__c = 10000;
        detallePoliticaDos.TAM_IncDealer_Cash__c = 5000;
        detallePoliticaDos.TAM_IncentivoTMEX_Cash__c = 5000;
        detallePoliticaDos.TAM_Lealtad__c  = true;
        detallePoliticaDos.TAM_PoliticaIncentivos__c = politicaRetail.id;
        detallePoliticaDos.TAM_Serie__c = 'Corolla';
        detallePoliticaDos.TAM_Clave__c = '1780';
        detallePoliticaDos.TAM_Pago_TFS__c = true;
        detallePoliticaDos.TAM_Rango__c = rangoPrograma.id;
        detallePoliticaDos.TAM_MSRP__c = 350000;
        detallePoliticaDos.TAM_IncPropuesto_Porcentaje__c = 20;
        insert detallePoliticaDos;
        
        //Productos
        Product2 productVINVC = new Product2();
        productVINVC.Name='1781';
        productVINVC.Anio__c = '2021';
        productVINVC.IdExternoProducto__c='17802021';
        productVINVC.ProductCode='17802021';
        productVINVC.CantidadMaxima__c = 5;
        productVINVC.IsActive = true;
        insert productVINVC;
        
        //Lista de precios
        Pricebook2 listaPrecioVC = new Pricebook2();
        listaPrecioVC.IsActive = true;
        listaPrecioVC.Name = 'VH-MAYO-2020';
        listaPrecioVC.IdExternoListaPrecios__c = 'VH-ABRIL-2020';
        listaPrecioVC.InicioVigencia__c = fecha;
        listaPrecioVC.FinVigencia__c = fecha.addDays(10);
        insert listaPrecioVC;
        
        //Precio 
        Id standardPricebookId2 = Test.getStandardPricebookId();
        PricebookEntry precioStandardVC = new PricebookEntry();
        precioStandardVC.Pricebook2Id= standardPricebookId2;
        precioStandardVC.Product2Id= productVINVC.Id;
        precioStandardVC.UnitPrice= 0.0;
        precioStandardVC.IsActive = true;
        precioStandardVC.PrecioSinIva__c = 211176.00;
        precioStandardVC.PrecioDealer__c = 67829;
        precioStandardVC.PrecioPublico__c = 134324;
        precioStandardVC.IsActive = true;
        insert precioStandardVC;
        
        //Entrada de precio
        PricebookEntry precioEntradaVC = new PricebookEntry();
        precioEntradaVC.Pricebook2Id= listaPrecioVC.id;
        precioEntradaVC.Product2Id= productVINVC.Id;
        precioEntradaVC.UnitPrice= 0.0;
        precioEntradaVC.IsActive = true;
        precioEntradaVC.PrecioSinIva__c = 211176.00;
        precioEntradaVC.PrecioDealer__c = 67829;
        precioEntradaVC.PrecioPublico__c = 134324;
        precioEntradaVC.IsActive = true;
        insert precioEntradaVC;
        
        //Cuenta de tipo distribuidor
        Account acctVC = new Account();
        acctVC.name = 'DEALER DE PRUEBA 5';
        acctVC.Codigo_Distribuidor__c = '5700025';
        acctVC.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        insert acctVC;
        
        //Se crear un registro de tipo excepción
        TAM_SolicitudExpecionIncentivo__c solExcp = new TAM_SolicitudExpecionIncentivo__c();
        solExcp.TAMVIN__c = 'WERT345678QWERTYU';
        solExcp.TAM_AnioModelo__c = '2021';
        solExcp.TAM_Distribuidor__c = acct.id;
        insert solExcp;
         
        
    }

    @isTest static void testMethod1(){
        Date fecha = date.today();
        
        List<SolicitudExcepcionIncentivos.wrapResumenPrecio> wrapperPoliticasVigentes = new List<SolicitudExcepcionIncentivos.wrapResumenPrecio>();
        wrapperPoliticasVigentes = SolicitudExcepcionIncentivos.getPoliticasVigentes('1780','2020',fecha,'SinBOno','TFS');
        system.assertEquals(true,!wrapperPoliticasVigentes.isEmpty());
        
        TAM_DetallePolitica__c precioVigente = new TAM_DetallePolitica__c();
        precioVigente = SolicitudExcepcionPrecios.getPrecioVigente('1780','2020');
        system.assertEquals(true, precioVigente != null);

        List<PricebookEntry> listaPrecios = new List<PricebookEntry>();
        listaPrecios = SolicitudExcepcionPrecios.getPreciosVIN('1780','2020');
        system.assertEquals(true, !listaPrecios.isEmpty());
        
        String idSolicitudCreada;
        idSolicitudCreada = SolicitudExcepcionPrecios.registraSolicitudExcepcion('1234qwer123tty',450000,430000,'Comentario',null,'570002');
        system.assertEquals(true,idSolicitudCreada != null);
        
    }
    
    @isTest static void testMethod2(){
        Date fecha = date.today(); 
        
        List<SolicitudExcepcionIncentivos.wrapResumenPrecio> wrapperPoliticasVigentes = new List<SolicitudExcepcionIncentivos.wrapResumenPrecio>();
        wrapperPoliticasVigentes = SolicitudExcepcionIncentivos.getPoliticasVigentes('1780','2020',fecha,'ConBono','TFS');
        system.assertEquals(true,!wrapperPoliticasVigentes.isEmpty());
        
        SolicitudExcepcionPrecios.wrapResumenPrecio wrapperPrecio1 = new SolicitudExcepcionPrecios.wrapResumenPrecio();
        wrapperPrecio1 = SolicitudExcepcionPrecios.calculoPrecioVigente('1780','2020',fecha);
        system.assertEquals(true,wrapperPrecio1 != null);
        
        SolicitudExcepcionPrecios.wrapResumenPrecio wrapperPrecio2 = new SolicitudExcepcionPrecios.wrapResumenPrecio();
        wrapperPrecio2 = SolicitudExcepcionPrecios.calculoPrecioVigente('1780','2020',null);
        system.assertEquals(true,wrapperPrecio2 != null);
        
        List<String> listaMotivos = new List<String>();
        listaMotivos = SolicitudExcepcionPrecios.obtieneMotivosSolicitud();
        system.assertEquals(true,!listaMotivos.isEmpty());
        
        List<TAM_FacturaCarga__c> facturaCarga = new List<TAM_FacturaCarga__c>();
        facturaCarga = SolicitudExcepcionPrecios.buscaFacturasVIN('1234qwer123tty');
        system.assertEquals(false,!facturaCarga.isEmpty());
        
    }
    
    @isTest static void testMethod3(){
		List<TAM_DetallePoliticaJunction__c> detalleIncentivos = new List<TAM_DetallePoliticaJunction__c> ();
        detalleIncentivos = SolicitudExcepcionIncentivos.getPoliticasVIN('1780','2020');
        system.assertEquals(true,!detalleIncentivos.isEmpty());

    }
    
    @isTest static void testMethod4(){
        TAM_DetallePoliticaJunction__c politicaSelect = [Select id from TAM_DetallePoliticaJunction__c WHERE TAM_Serie__c = 'COROLLA' LIMIT 1];           
        Account dealer = [Select id from Account WHERE name = 'DEALER DE PRUEBA'];
        List<wrapResumenPrecio> wrpR = new List<wrapResumenPrecio>();
        List<SolicitudExcepcionIncentivos.wrapResumenPrecio> politicas =new List <SolicitudExcepcionIncentivos.wrapResumenPrecio>();
        DateTime fechaPrueba = date.today();
        
        String idSolicitudCreada;
        idSolicitudCreada = SolicitudExcepcionIncentivos.registraSolicitudExcepcion('1234qwer123tty','Corolla',571839,92819,'de prueba',null,politicaSelect.id,dealer.id,politicas,'TFS','Retail',fechaPrueba,'2021','RAV4','SPORT',false);
        system.assertEquals(true, idSolicitudCreada != null);
        
        
    }
    
    @isTest static void testMethod5(){
        TAM_DetallePoliticaJunction__c politicaSelect = [Select id from TAM_DetallePoliticaJunction__c WHERE TAM_Serie__c = 'COROLLA' LIMIT 1];        
        Account dealer = [Select id from Account WHERE name = 'DEALER DE PRUEBA'];
        
        SolicitudExcepcionIncentivos.wrapResumenPrecio calculoIncentivo1 = new SolicitudExcepcionIncentivos.wrapResumenPrecio();
        calculoIncentivo1 = SolicitudExcepcionIncentivos.calculoPrecioIncentico(politicaSelect.id,'1780','2020','SinBono');
        system.assertEquals(true, calculoIncentivo1 != null);
        
        SolicitudExcepcionIncentivos.wrapResumenPrecio calculoIncentivo2 = new SolicitudExcepcionIncentivos.wrapResumenPrecio();
        calculoIncentivo2 = SolicitudExcepcionIncentivos.calculoPrecioIncentico(politicaSelect.id,'1780','2020','ConBono');
        system.assertEquals(true, calculoIncentivo2 != null);
        
    }
    
    @isTest static void testMethod6(){
        
        List<String> motivosSolicitud = new List<String>();
        motivosSolicitud = SolicitudExcepcionIncentivos.obtieneMotivosSolicitud();
        system.assertEquals(true,!motivosSolicitud.isEmpty());
        
    }
    
    @isTest static void testMethod7(){
        
        List<TAM_DetalleEstadoCuenta__c> facturaCarga = new List<TAM_DetalleEstadoCuenta__c>();
        facturaCarga = SolicitudExcepcionIncentivos.buscaFacturasVIN('1234567qwerty');
        system.assertEquals(true,facturaCarga != null);
        
    }    
    
    @isTest static void testMethod8(){
        String fechaVenta = '2020-05-02 19:00:00';
        TAM_DetallePoliticaJunction__c politica = [Select id from TAM_DetallePoliticaJunction__c where name = 'Detalle de prueba' limit 1];
        
        TAM_DetallePoliticaJunction__c politicasFechaCierre = [Select id from TAM_DetallePoliticaJunction__c limit 1];
        SolicitudExcepcionIncentivosFlotilla.getPoliticaFechaCierre('1780','2020','UBER','TFS',fechaVenta);
        system.assertEquals(true,politicasFechaCierre != null);
        
        List<PricebookEntry> listaPrecios = new List<PricebookEntry>();
        listaPrecios = SolicitudExcepcionIncentivosFlotilla.getPreciosVIN('1780','2020');
        system.assertEquals(true,!listaPrecios.isEmpty());
        
        List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio> listaWrapperPorcentaje = new List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio>();
        listaWrapperPorcentaje = SolicitudExcepcionIncentivosFlotilla.getPoliticasVigentesPorcentaje('1780','2020','UBER','TFS');
        system.assertEquals(false,!listaWrapperPorcentaje.isEmpty());
       
        List<String> listaMotivos = new List<String>();
        listaMotivos = SolicitudExcepcionIncentivosFlotilla.obtieneMotivosSolicitud();
        system.assertEquals(true,!listaMotivos.isEmpty());
        
        SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio calculoManual = new SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio();
        calculoManual = SolicitudExcepcionIncentivosFlotilla.getCalculoManual(350000,10,10);
        system.assertEquals(true,calculoManual != null);
        
        TAM_WrpSolicitudFlotillaProgramaModelos wrapSolicitud = new TAM_WrpSolicitudFlotillaProgramaModelos();
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion itemVIN = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion();
        itemVIN.strVin = 'QWERTY45678';
        List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion> lVinesSeleccionados = new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion>();
        lVinesSeleccionados.adD(itemVIN);
        wrapSolicitud.lVinesSeleccionados = lVinesSeleccionados;
        wrapSolicitud.strFolioSolicitd = '1238209101AQ';
        wrapSolicitud.strNombreCliente = 'Hector Figueroa';
        wrapSolicitud.strRangoPrograma = 'UBER';
        wrapSolicitud.strFechaVigencia   = '2020-05-02';
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados modeloSelect = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
        modeloSelect.strIdSolicitud = '00120101';
        modeloSelect.strIdExterno = '2345678JAJSHY';
        modeloSelect.strNombre = 'Avanza-2205-2020-X12-10';
        modeloSelect.strVersion = 'LE';
        modeloSelect.strDescripcionColorExterior = '0101';
        modeloSelect.strDescripcionColorInterior  = 'E427';
        modeloSelect.strCantidadSolicitada  = '5';
        modeloSelect.strIdCatCentrModelos = 'A001';
        modeloSelect.bolMismoDistribuidor = true;
        modeloSelect.bolCreaExcepcion = true;
        list<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> listmodeloSelect =  new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>();
        listmodeloSelect.adD(modeloSelect);
        wrapSolicitud.lModelosSeleccionados = listmodeloSelect;
        List<String> listaVINES = new  List<String> ();
        listaVINES.add('QWERTYUIO4567890:57011');
        listaVINES.add('QWERTYUIO4567891:57011');
        String listaVinesST = listaVINES.toString();
        String fechaVentaVIN = '2020-08-03 19:00:00';
        
        Document doc = [Select id from Document where DeveloperName = 'mydocument'];

        SolicitudExcepcionIncentivos.guardaSolicitudExcepcion(wrapSolicitud,'Otro motivo.','comentario',370000,true,politica.id,10,10,10,politica.id,'5700025',listaVinesST,fechaVentaVIN,true,doc.Id);
        List<TAM_SolicitudExpecionIncentivo__c> solExcepcion = [Select id from TAM_SolicitudExpecionIncentivo__c limit 1];
		system.assertEquals(true,!solExcepcion.isEmpty());        
             
        SolicitudExcepcionIncentivos.guardaSolicitudExcepcion(wrapSolicitud,'Otro motivo.','comentario',370000,false,politica.id,10,10,10,politica.id,'5700025',listaVinesST,fechaVentaVIN,true,doc.Id);
        List<TAM_SolicitudExpecionIncentivo__c> solExcepcionRetail = [Select id from TAM_SolicitudExpecionIncentivo__c limit 1];
        system.assertEquals(true,!solExcepcionRetail.isEmpty());
        
        
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion test = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion ('test');
        TAM_WrpSolicitudFlotillaProgramaModelos constructorVIN = new  TAM_WrpSolicitudFlotillaProgramaModelos('Carlos','Test','A001','2020-05-02','Carlos','TFS','UBER','Roberto','2020-05-02',null);
        SolicitudExcepcionIncentivos.getPoliticaFechaCierre('1780','2020','UBER','TFS',fechaVenta);
        TAM_DetallePoliticaJunction__c politicasFechaCierreDetalle = [Select id from TAM_DetallePoliticaJunction__c limit 1];
        system.assertEquals(true, politicasFechaCierreDetalle != null);
        
    }   
    
    @isTest static void testMethod10(){
    	Document doc = [Select id from Document where DeveloperName = 'mydocument'];
        SolicitudExcepcionIncentivos.deleteDocument(doc.Id);
        List<ContentDocument> documentRecord = new List<ContentDocument>();
        documentRecord = [Select id from ContentDocument WHERE id =: doc.Id];
        system.assertEquals(true,documentRecord.isEmpty());
        String fechaVenta = '2021-03-05 19:00:00';
        
        
        SolicitudExcepcionIncentivos.RetailFlot('1780','2020',fechaVenta,'SinBOno','TFS');
        List<TAM_SolicitudExpecionIncentivo__c> solExcepcionRF = [Select id from TAM_SolicitudExpecionIncentivo__c limit 1];
		system.assertEquals(true,!solExcepcionRF.isEmpty());   
    
        SolicitudExcepcionIncentivos.RetailFlot('1780','2020',fechaVenta,'ConBOno','TFS');
        List<TAM_SolicitudExpecionIncentivo__c> solExcepcionRF2 = [Select id from TAM_SolicitudExpecionIncentivo__c limit 1];
		system.assertEquals(true,!solExcepcionRF2.isEmpty());   

    }
    
    @isTest static void testMethod11(){
        SolicitudExcepcionIncentivos.buscaExcepcionesSFDC('QWERTYUIO4567891');
        
        Document doc = [Select id from Document where DeveloperName = 'mydocument'];
        List<TAM_SolicitudExpecionIncentivo__c> solExcepcionRF = [Select id from TAM_SolicitudExpecionIncentivo__c limit 1];
        SolicitudExcepcionIncentivos.relatedDocumentWithRequestException(solExcepcionRF[0].Id,doc.Id);
    }
    
    public class wrapResumenPrecio {
        public decimal precioPublico {get; set;}
        public decimal precioDealer {get; set;}
        public decimal incentivoSeleccionado {get; set;}
        public decimal precioFinal {get; set;}
        public decimal incentivoPropuesto {get; set;}
        public decimal incentivoDealer {get; set;}
        public decimal incentivoTMEX {get; set;}
        public decimal bonoLealtad {get; set;}
        public decimal bonoLealtadTMEX {get; set;}
        public decimal bonoLealtadDealer {get; set;}
        public String nombrePolitica {get; set;}
        public String periodoPolitica {get; set;}
        public String listaPrecioVigente {get; set;}
        public Boolean pagoTFS {get; set;}
        public String idPolitica {get; set;}
    }
    
}