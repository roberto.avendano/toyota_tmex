public class BuscadorArticulo {
    
    public Knowledge__kav boletin{get;set;}
    public string renderAsdoc{get;set;}
    public String application{get;set;}
    public boolean Visual{get;set;}
    
    public BuscadorArticulo(){
        system.debug('stdController ' + boletin); 
        System.debug(LoggingLevel.INFO,'Inicia BuscadorArticulo');
        id idK = ApexPages.currentPage().getParameters().get('id');
        system.debug('stdController ' + idK);    
        this.boletin = [SELECT Id, Title, UrlName, VersionNumber, Language, Publishstatus, Cuerpo__c, 
                        ArticleNumber,numero_boletin__c, LastPublishedDate FROM Knowledge__kav  where id = :idK 
                        and Language = 'es_MX' and Publishstatus = 'online'];
        Visual = false;
        System.debug(LoggingLevel.INFO,'Termina BuscadorArticulo');
    }
    public String getReferer(){
        	return ApexPages.currentPage().getHeaders().get('referer');    
    }
    public PageReference SaveAspdf() {
        System.debug(LoggingLevel.INFO,'Inicia SaveAspdf');
        Visual = true;
        renderAsdoc='pdf';
        application='application/pdf';
        string fileName = this.boletin.Title + '.pdf';
        System.debug('fileName ' + fileName);
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachemnt; filename=' + fileName);
        System.debug(LoggingLevel.INFO,'Termina SaveAspdf');
        return null;
    }
    
    
}