public without sharing class TAM_ValidateProfileSolicitudExc {
    
    @AuraEnabled
    public static User getProfileByUser (String userId){
        
        User usr = [Select id,Profile.Name,Name From User where id =: userId];   
        
        return usr;   
        
    }
    
    
}