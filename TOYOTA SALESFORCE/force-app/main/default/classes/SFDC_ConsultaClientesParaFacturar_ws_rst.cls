/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Publicaciín del WS con tecnologia REST para la consulta de Propesctos en Etapa de facturación

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Junio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

@RestResource(urlMapping='/SFDC_ConsultaClientesParaFacturar_ws_rst/*')
global with sharing class SFDC_ConsultaClientesParaFacturar_ws_rst {

	@HttpPost  
	global static void ConsultaClientesParaFacturarRst(){
		RestRequest req = RestContext.request;
		Blob bBody = req.requestBody;
		String sBody =  bBody.toString();

		System.debug('EN SFDC_ConsultaClientesParaFacturar_ws_rst sBody: ' + sBody);		
		//Manda llamar la clase que se llama SFDC_UpdDatosInvestigacion_ws_rst y el metodo updInvestigaciones
		String sResUpdInves = SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar(sBody);
		System.debug('EN SFDC_ConsultaClientesParaFacturar_ws_rst sResUpdInves: ' + sResUpdInves);

		RestContext.response.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueOf(sResUpdInves);
		
		//Ya respondiste entonces actualiza el estatus de cada prospecto
		SFDC_ConsultaCtesParaFacturar_ctrl_rst.updProspCons(sResUpdInves);
		
	}
    
}