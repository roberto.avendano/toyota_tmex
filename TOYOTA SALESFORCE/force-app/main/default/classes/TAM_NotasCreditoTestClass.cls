/******************************************************************************** 
    Desarrollado por:   Globant México
    Autor:              Cecilia Cruz Moran
    Proyecto:           TOYOTA TAM
    Descripción:        Trigger Handler que procesa la carga de la notas de crédito

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                 Autor                       Descripcion
    1.0    15-Junio-2021        Cecilia Cruz Moran           Creación
******************************************************************************* */
@isTest(SeeAllData=false)
public class TAM_NotasCreditoTestClass {

    static final String STRING_UUID_RELACIONADO = 'UUID_Relacionado-';
    static final String STRING_UUID_NOTA_CREDITO = 'UUID_NotaCredito-';
    static final String STRING_CODIGO_DEALER = '57000';
    static final String STRING_FOLIO = 'F-000';
    static final String STRING_METODO_PAGO = 'PUE';
    
    @testSetup static void setup() {
        
        //Facturas
        Account objDealer = new Account();
        objDealer.Codigo_Distribuidor__c = STRING_CODIGO_DEALER;
		objDealer.Name = 'Toyota Prueba';  
        Database.insert(objDealer);
        
        Integer intCantidadFacturas = 10;
        List<Factura__c> lstFactura = new List<Factura__c>();
        for(Integer i=0; i<=intCantidadFacturas; i++){
            Factura__c objFactura = new Factura__c();
            objFactura.TAM_UUID__c = STRING_UUID_RELACIONADO +i;
            objFactura.TAM_Folio__c = STRING_FOLIO+i;
            objFactura.Distribuidor__c = objDealer.Id;
            lstFactura.add(objFactura);
        }
        Database.insert(lstFactura);
        
        //Notas de Credito
        Integer intCantidadRegistros = 20;
        List<TAM_Notas_Credito_Carga__c> lstNotaCreditoCarga = new List<TAM_Notas_Credito_Carga__c>();
        for(Integer i=0; i<=intCantidadRegistros; i++){
            TAM_Notas_Credito_Carga__c objNotaCredito = new TAM_Notas_Credito_Carga__c();
            objNotaCredito.TAM_Codigo_Distribuidor__c = STRING_CODIGO_DEALER;
            objNotaCredito.TAM_FechaOperacion__c = DateTime.now();
            objNotaCredito.TAM_Fecha_Timbrado__c = DateTime.now();
            objNotaCredito.TAM_Folio__c = STRING_FOLIO + i;
            objNotaCredito.TAM_UUID_Nota_Credito__c = STRING_UUID_NOTA_CREDITO +i;
            objNotaCredito.TAM_UUIDRelacionado__c = STRING_UUID_RELACIONADO +i;
            objNotaCredito.TAM_MetodoPago__c = STRING_METODO_PAGO;
     		lstNotaCreditoCarga.add(objNotaCredito);
        }
        Database.upsert(lstNotaCreditoCarga, TAM_Notas_Credito_Carga__c.TAM_UUID_Nota_Credito__c); 
    }
    
    @isTest
    public static void TAM_NotasCreditoCarga_Test(){
        Test.startTest();
        List<TAM_Notas_Credito__c> lstNotaCredito = [SELECT Id FROM TAM_Notas_Credito__c];
        Test.stopTest();
        System.assertNotEquals(0, lstNotaCredito.size(), 'No se inserto ningun registro de Nota de Crédito');
    }
}