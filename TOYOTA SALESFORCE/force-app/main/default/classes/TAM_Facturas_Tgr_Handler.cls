/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del archivo de facturas

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    17-Febrero-2021     Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_Facturas_Tgr_Handler extends TriggerHandler{

    private List<Factura__c> itemsNew;
    private Map<Id, Factura__c> mapItemsOld;    

	//Un constructor por default
	public TAM_Facturas_Tgr_Handler() {
        this.itemsNew = (List<Factura__c>) Trigger.new;
        this.mapItemsOld = (Map<Id, Factura__c>) Trigger.oldMap;		
	}

	//Sobreescribe el metodo de afterInsert
    public override void afterInsert() {
		System.debug('EN beforeInsert....');
		actualizaDatosOportunidad();
        actualizaDatosLead(this.itemsNew);
    }

    //Sobreescribe el metodo de afterInsert
    public override void afterUpdate() {
        System.debug('EN beforeInsert....');
        actualizaDatosLead(this.itemsNew);
    }

    //Funcion que sirve para actualizar las listas de precio que se estan crgando en TAM_CatalogoProductos__c
    private void actualizaDatosOportunidad(){
		System.debug('EN creaMapasDatos....');
    }

    private void actualizaDatosLead(List<Factura__c> itemsNew){
    
        Set<String> setIdVinActCand = new Set<String>();
        map<String, Lead> mapLeadUpsUps = new map<String, Lead>();
        Set<String> setErroresLeadErr = new Set<String>();
        Set<String> setIdLeadSucc = new Set<String>();              
        Set<String> setDdVehic = new Set<String>();              

        map<String, TAM_LeadInventarios__c> mapLeadInvComs = new map<String, TAM_LeadInventarios__c>();
        map<String, Factura__c> mapVinNomCte = new map<String, Factura__c>();
        map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new map<String, TAM_LeadInventarios__c>();
        map<String, TAM_LeadInventarios__c> mapLeadInvCons = new map<String, TAM_LeadInventarios__c>();
        
        //Recorre la lista de reg que se estan nsertando
        for(Factura__c v: itemsNew){                
            //Toma el Vin para la busqueda del candidato asociado a ese vin
            if(v.Name != null){
                String sVinName = v.Name;
                String sVinPaso = sVinName.contains('-') ? sVinName.substring(0, sVinName.indexOf('-')) : sVinName;
                //Agregalo a la mapa de mapVinNomCte
                mapVinNomCte.put(sVinPaso, v); //v.TAM_NombreReceptor__c != null ? v.TAM_NombreReceptor__c : ''
            }//Fin si v.Name != null
        }        
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.KeySet());         
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.Values()); 
        
        //Consulta el VIN en Vehiculos para ver si ya se cargo en DD
        for (Vehiculo__c DD : [Select Name From Vehiculo__c Where Name IN :mapVinNomCte.KeySet()]){
            setDdVehic.add(DD.Name);
        }
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + setDdVehic);        
        
        //Consulta los datos de los vines que ya estan cargados
        for (TAM_LeadInventarios__c objLeadInventarios : [Select t.id, t.TAM_Prospecto__c, t.Name, 
            TAM_Idexterno__c, TAM_uIDFactura__c, TAM_VINFacturacion__c, Factura__c, TAM_FechaFacturacion__c
            From TAM_LeadInventarios__c t Where (Name IN :mapVinNomCte.KeySet() OR TAM_VINFacturacion__c IN :mapVinNomCte.KeySet())
            And TAM_Prospecto__c != null]){
            String sEstatusLead = setDdVehic.contains(objLeadInventarios.Name) ? 'Entregado' : 'Facturado';  
            String sPorcentajeAvance = setDdVehic.contains(objLeadInventarios.Name) ? '100%' : '90%';  
            Integer intFWYEnganchePorcentaje = setDdVehic.contains(objLeadInventarios.Name) ? 100 : 90;
            //Crea el objeto de Lead para actualizar los datos    
            Lead CandUpd = new Lead(
                id = objLeadInventarios.TAM_Prospecto__c, TAM_PedidoFacturado__c = true, 
                Status = sEstatusLead, FWY_Estado_del_candidatoWEB__c = sEstatusLead, 
                TAM_PorcentajeAvance__c = sPorcentajeAvance, 
                //FWY_Enganche_porcentaje__c = intFWYEnganchePorcentaje,
                FWY_Porcentaje_de_avance__c = intFWYEnganchePorcentaje,
                TAM_FechaFacturacionDealer__c = mapVinNomCte.containsKey(objLeadInventarios.Name) ? Date.valueOf(mapVinNomCte.get(objLeadInventarios.Name).TAM_FechaTimbrado__c) : null
            );
            //mete los datos en el mapa de mapIdInveCand para mandar llamar el proceso ActualizaVinCandito
            mapLeadUpsUps.put(objLeadInventarios.TAM_Prospecto__c, CandUpd);     
            mapLeadInvComs.put(objLeadInventarios.Name, objLeadInventarios);
            if (objLeadInventarios.TAM_VINFacturacion__c != null)
                mapLeadInvComs.put(objLeadInventarios.TAM_VINFacturacion__c, objLeadInventarios);
        }    
        System.debug('EN actualizaDatosLead mapLeadUpsUps: '+ mapLeadUpsUps);
        System.debug('EN actualizaDatosLead mapLeadInvComs: '+ mapLeadInvComs);

        //Ve si tiene algo el mapa de  mapLeadUpsUps             
        //mapLeadInvComs.put(objLeadInventarios.id, objLeadInventarios);
        if (!mapLeadUpsUps.isEmpty()){
                                                
           List<Lead> lLeadUpd = mapLeadUpsUps.values();
           //Recorre la lista de lLeadUpd y actualiza el campo de TAM_NombreClienteFacturacion__c
           for (Lead CandUpd : lLeadUpd){
                //Busca el lead en el mapa de mapLeadInvComs
                if (mapLeadUpsUps.containsKey(CandUpd.id)){
                   String sVinLead = '';
                   String sVinLeadFact = '';
                   //Toma el nombre del objeto  objLeadInventarios
                   for (TAM_LeadInventarios__c objLeadInv : mapLeadInvComs.Values()){
                        if (objLeadInv.TAM_Prospecto__c == CandUpd.id){
                            sVinLead = objLeadInv.Name;
                            sVinLeadFact = objLeadInv.TAM_VINFacturacion__c != null ? objLeadInv.TAM_VINFacturacion__c : objLeadInv.Name;
                            break;
                        }//Fin si objLeadInv.TAM_Prospecto__c == CandUpd.id
                   }//Fin si 
                   TAM_LeadInventarios__c objLeadInvUpd;
                   //Busca el cliente asociado a sVinLead o sVinLeadFact
                   if (mapVinNomCte.containsKey(sVinLead)){ 
                        objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLead).id);
                        objLeadInvUpd.TAM_NombreClienteFacturacion__c = mapVinNomCte.get(sVinLead).TAM_NombreReceptor__c;
                        //Ve si el campo de TAM_uIDFactura__c de mapLeadInvComs.get(sVinLead) no tiene naada entonces actualizalo
                        if (mapLeadInvComs.get(sVinLead).TAM_uIDFactura__c == null){
                            //Actualiza el resto de los campos 
                            objLeadInvUpd.TAM_uIDFactura__c = mapVinNomCte.get(sVinLead).TAM_UUID__c;
                            objLeadInvUpd.TAM_VINFacturacion__c = sVinLead;
                            objLeadInvUpd.Factura__c = mapVinNomCte.get(sVinLead).id;
                            objLeadInvUpd.TAM_FechaFacturacion__c = mapVinNomCte.get(sVinLead).TAM_FechaTimbrado__c.Date();
                            
                        }//Fin si mapLeadInvComs.get(sVinLead).TAM_uIDFactura__c == null
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   if (!mapVinNomCte.containsKey(sVinLead)) {
                      if (mapVinNomCte.containsKey(sVinLeadFact)){ 
                          objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLeadFact).id);
                          objLeadInvUpd.TAM_NombreClienteFacturacion__c = mapVinNomCte.get(sVinLeadFact).TAM_NombreReceptor__c;
                          //Ve si el campo de TAM_uIDFactura__c de mapLeadInvComs.get(sVinLead) no tiene naada entonces actualizalo
                          if (mapLeadInvComs.get(sVinLeadFact).TAM_uIDFactura__c == null){
                             //Actualiza el resto de los campos 
                             objLeadInvUpd.TAM_uIDFactura__c = mapVinNomCte.get(sVinLeadFact).TAM_UUID__c;
                             objLeadInvUpd.TAM_VINFacturacion__c = sVinLeadFact;
                             objLeadInvUpd.Factura__c = mapVinNomCte.get(sVinLeadFact).id;
                             objLeadInvUpd.TAM_FechaFacturacion__c = mapVinNomCte.get(sVinLeadFact).TAM_FechaTimbrado__c.Date();
                          }//Fin si mapLeadInvComs.get(sVinLead).TAM_uIDFactura__c == null
                      }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   System.debug('EN actualizaDatosLead objLeadInvUpd: ' + objLeadInvUpd);
                   //Ve si tiene algo el obj de objLeadInvUpd
                   if (objLeadInvUpd != null)
                      mapLeadInvUpd.put(objLeadInvUpd.id, objLeadInvUpd);  
                }//Fin si mapLeadInvComs.containsKey(CandUpd.id)
           }//Fin del for para lLeadUpd

           System.debug('EN actualizaDatosLead lLeadUpd: ' + lLeadUpd);         
           Integer icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lLeadUpd, Lead.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
               if (!objDtbUpsRes.isSuccess())
                   setErroresLeadErr.add('En Lead : ' + lLeadUpd.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbUpsRes              

           System.debug('EN actualizaDatosLead mapLeadInvUpd: ' + mapLeadInvUpd.KeySet());         
           System.debug('EN actualizaDatosLead mapLeadInvUpd: ' + mapLeadInvUpd.Values());
           icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbLiUpsRes = Database.upsert(mapLeadInvUpd.values(), TAM_LeadInventarios__c.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbLiUpsRes){
               if (!objDtbUpsRes.isSuccess())
                  setErroresLeadErr.add('En Lead : ' + mapLeadInvUpd.values().get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbLiUpsRes              

        }//Fin si !mapLeadUpsUps.isEmpty()
        System.debug('EN actualizaDatosLead setErroresLeadErr: ' + setErroresLeadErr);         
        System.debug('EN actualizaDatosLead setIdLeadSucc: ' + setIdLeadSucc);
    
    }
    
}