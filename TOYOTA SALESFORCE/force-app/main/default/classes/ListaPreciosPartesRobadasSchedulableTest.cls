@isTest
private class ListaPreciosPartesRobadasSchedulableTest{

	@testSetup
    static void setDatos(){

        RecordType rtParte = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Parte' and IsActive=true];
        RecordType rtProductoFinal = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='ProductoFinal' and IsActive=true];    	
        Id standardPricebookId = Test.getStandardPricebookId();
        
        ParametrosConfiguracionToyota__c pc1 = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosOrigen',
            Valor__c='PRListaPreciosOrigen',
            Consecutivo__c = 1
        );
        insert pc1;
        
        ParametrosConfiguracionToyota__c pc2 = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosDestino',
            Valor__c='PRListaPreciosDestino',
            Consecutivo__c = 2
        );
        insert pc2;
        
        Date ahora = Date.today();
        ParametrosConfiguracionToyota__c pc3 = new  ParametrosConfiguracionToyota__c(
            Name='PRCambioPreciosDealer',
            Valor__c=String.valueOf(ahora.day()),
            Consecutivo__c = 3
        );
        insert pc3;

        ParametrosConfiguracionToyota__c pc4 = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosOrigenName',
            Valor__c='PRListaPreciosOrigenName',
            Consecutivo__c = 4
        );
        insert pc4;

        ParametrosConfiguracionToyota__c pc5 = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosDestinoName',
            Valor__c='PRListaPreciosDestinoName',
            Consecutivo__c = 5
        );
        insert pc5;

        ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('PRCambioPreciosDealer');
        
        
        Pricebook2 customPriceBook1 = new Pricebook2(
            Name = pc1.Valor__c,
            Description = pc1.Valor__c,
            IdExternoListaPrecios__c = pc1.Valor__c,
            isActive = true
        );
        insert customPriceBook1;

        
        Pricebook2 customPriceBook2 = new Pricebook2(
            Name = pc2.Valor__c,
            Description = pc2.Valor__c,
            IdExternoListaPrecios__c = pc2.Valor__c,
            isActive = true
        );
        insert customPriceBook2;
        
        
        Pricebook2 customPriceBook3 = new Pricebook2(
            Name = pc4.Valor__c,
            Description = pc4.Valor__c,
            IdExternoListaPrecios__c = pc4.Valor__c,
            isActive = true
        );
        insert customPriceBook3;

        Pricebook2 customPriceBook4 = new Pricebook2(
            Name = pc5.Valor__c,
            Description = pc5.Valor__c,
            IdExternoListaPrecios__c = pc5.Valor__c,
            isActive = true
        );
        insert customPriceBook4;

        Product2 miProd= new Product2(
            Name='Test01',
            IdExternoProducto__c='Test01',
            ProductCode='Test01',
            Description= 'Test product2',
            CantidadMaxima__c = 5,
            PartesRobadas__c = true,
            RecordTypeId = rtParte.Id,
            IsActive = true
        );
        insert miProd;

        Product2 miProdFinal = new Product2(
            Name='Test02',
            IdExternoProducto__c='Test02',
            ProductCode='Test02',
            Description= 'Test product2',
            CantidadMaxima__c = 5,
            PartesRobadas__c = true,
            RecordTypeId = rtProductoFinal.Id,
            IsActive = true
        );

        insert miProdFinal;

        ProductoFinalComponente__c pcFinal = new ProductoFinalComponente__c(
            Producto__c = miProd.Id,
            ProductoFinal__c = miProdFinal.Id,
            Name='Test'
        );
        insert pcFinal;

        //Entradas de listas de precios estándar
        PricebookEntry pbeStandardProd1= new PricebookEntry(
            Pricebook2Id= standardPricebookId,
            Product2Id= miProd.Id,
            UnitPrice= 0.0,
            IdExterno__c = 'standard2',
            IsActive = true
        );
        insert pbeStandardProd1; 

        PricebookEntry pbeStandardProdFinal= new PricebookEntry(
            Pricebook2Id= standardPricebookId,
            Product2Id= miProdFinal.Id,
            UnitPrice= 0.0,
            IdExterno__c = 'standard3',
            IsActive = true
        );
        insert pbeStandardProdFinal;

        
        PricebookEntry pbe1= new PricebookEntry(
            Pricebook2Id= customPriceBook1.Id,
            Product2Id= miProd.Id,
            UnitPrice= 0.0,
            IdExterno__c = miProd.Name+'-'+pc4.Valor__c,
            IsActive = true
        );
        insert pbe1; 

        
        PricebookEntry pbe2= new PricebookEntry(
            Pricebook2Id= customPriceBook2.Id,
            Product2Id= miProd.Id,
            UnitPrice= 0.0,
            IdExterno__c = miProd.Name+'-'+pc5.Valor__c,
            IsActive = true
        );
        insert pbe2;

        
        PricebookEntry pbe3= new PricebookEntry(
            Pricebook2Id= customPriceBook1.Id,
            Product2Id= miProdFinal.Id,
            UnitPrice= 0.0,
            IdExterno__c = miProdFinal.Name+'-'+pc4.Valor__c,
            IsActive = true
        );
        insert pbe3; 

    }

	@isTest 
	static void test_general() {
		Test.startTest();
			ListaPreciosPartesRobadasSchedulable che = new ListaPreciosPartesRobadasSchedulable();	
			String cron = '0 0 3 * * ? ';
			String jobID = system.schedule('Merge Job', cron, che);

			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id=:jobID];
			
		Test.stopTest();
	}	
}