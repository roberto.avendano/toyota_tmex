/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del 
                        objeto TAM_TareasPaso_Handler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    26-MAYO-2021      Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TAM_TareasPaso_Handler_tst {

    static testMethod void TAM_TareasPaso_HandlerOK() {
        ParametrosConfiguracionToyota__c config = new ParametrosConfiguracionToyota__c();
        config.name = 'PlantillaEmailNotificaOwner';
        config.Valor__c = 'Plantilla de candidato nuevo';
        config.consecutivo__c = 754;
        config.DescripcionParametro__c = 'Especifica los parámetros para la búsqueda de la plantilla de email '
            + 'que será utilizada para notificar a los usuarios cuando se les ha sido asignado un lead';
        insert config;
        
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '570301');
            system.debug('distribuidora metodo 1'+ac);
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id);
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', 
                username='testOwnertfsproduc@noemail.com.test');
            
            insert user;
            
            RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_RetailNuevos' and SobjectType = 'Lead'];      
            Lead l0 = new Lead();
            l0.RecordTypeId = rectype.id;
            l0.FirstName = 'Test';
            l0.FWY_Intencion_de_compra__c = 'Este mes';  
            l0.Email = 'as@a.com';
            l0.phone = '225';
            l0.Status='Nuevo Lead';
            l0.LastName = 'Test1';
            l0.FWY_codigo_distribuidor__c = '57031';
            insert l0;
            
            /*Lead l1 = new Lead();
            l1.RecordTypeId = rectype.id;
            l1.FirstName = 'Test1';
            l1.FWY_Intencion_de_compra__c = 'Este mes'; 
            l1.Email = 'a@a.com';
            l1.phone = '223';
            l1.Status='Nuevo Lead';
            l1.LastName = 'Test2';
            insert l1;
            
            Lead l2 = new Lead();
            l2.RecordTypeId = rectype.id;
            l2.FirstName = 'Test12';
            l2.FWY_Intencion_de_compra__c = 'Este mes'; 
            l2.Email = 'aw@a.com';
            l2.phone = '224';
            l2.Status='Nuevo Lead';
            l2.LastName = 'Test3';
            insert l2;*/
            
            List<task> lt1 = new List<task>{ new task(
                //WhatID = l0.id,
                WhoId = l0.OwnerId,
                Subject='Donni',
                Status='Completed',
                Priority='Normal'
            )
            };
            insert lt1;              
            
            TAM_TareasPaso__c TP1 = new TAM_TareasPaso__c();
            TP1.TAM_IdPropietario__c = lt1.get(0).WhoId;
            TP1.TAM_IdRegRelacionado__c = l0.id;
            TP1.TAM_IdTarea__c = lt1.get(0).id; 
            insert TP1;
            
        Test.stopTest();        
    }
}