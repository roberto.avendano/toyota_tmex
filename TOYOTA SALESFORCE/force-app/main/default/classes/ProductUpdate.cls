public class ProductUpdate {
    public static void updateExternalId(List<Product2>itemsNew){
        if(!itemsNew.isEmpty()){
            Id recordTypeUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
            for(Product2 productIn : itemsNew){
                if(productIn.recordtypeId == recordTypeUnidad){
                    productIn.IdExternoProducto__c = productIn.Name+productIn.Anio__c;
                    //productIn.IdUnidad__c		   = productIn.Name+productIn.Anio__c; 
                }
            }
        }       
    }       
}