/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladira para el componente TAM_ConsultaDatosClientesFacturacion

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    03-Junio-2020        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class SFDC_UpdateResultClienteFactura_ctrl_rst {

    public List<DatosProspectoResponse> lstDatosProspectoResponse;      
    public class DatosProspectoResponse{
        public String IdProspectoSFDC {get;set;}    //Id
        public Boolean estatusProceso {get;set;}    //Si false entonces llenar el detalle del error
        public String detalleErrorProceso {get;set;}    //Se llena solo si estatusProceso = false
        public String estatusUpdateSFDC {get;set;}  //Cuando responde SFDC al Dealer
        //Lista de Vines
        public List<DatosVinResponse> lstVines {get;set;}
    }

    public class DatosVinResponse{
        public String VIN {get;set;}                //TAM_VINFacturado__c   
        public String uIDFactura {get;set;}         //FistName
        public String fechaFacturacion {get;set;}   //FistName
        public String numeroFactura {get;set;}      //QUITER LA nueva refencia de la factura      
        public String idnumeroFacturacancelacion {get;set;}     //QUITER La refencia que se cancela
        public String estatus {get;set;}            //Si false entonces llenar el detalle del error
        public String detalleEstatus {get;set;}     //Se llena solo si estatusProceso = false
        public String estatusUpdateSFDC {get;set;}  //Cuando responde SFDC al Dealer        
    }

    //Ahora Crea el llamado para la serializacion JSON.deserialize
    public List<DatosProspecto> lstDatosProspecto;          
    public class DatosProspecto{    
        //Datos del cliente para facturación    
        public String IdProspectoSFDC {get;set;}    //Id
        public String numeroCliente {get;set;}      //FWY_ID_de_cliente__c
        public String nombre {get;set;}             //FistName
        public String apellidoPaterno {get;set;}    //LastName
        public String apellidoMaterno {get;set;}    //FWY_ApellidoMaterno__c
        public String tipoPersona {get;set;}        //FWY_Tipo_de_persona__c
        public String rfc {get;set;}                //TAM_RFC__c
        public String Telefono {get;set;}           //Phone
        public String celular {get;set;}            //MobilePhone
        public String direccion {get;set;}          //TAM_Calle__c
        public String noInt {get;set;}              //TAM_Estado__c
        public String noExt {get;set;}              //TAM_Pais__c
        public String colonia {get;set;}            //TAM_Colonia__c
        public String localidad {get;set;}          //TAM_Ciudad__c
        public String cp {get;set;}                 //TAM_CodigoPostal__c
        public String delegacionMunicipio {get;set;}//
        public String estado {get;set;}             //TAM_Estado__c
        public String pais {get;set;}               //TAM_Pais__c
        public String email {get;set;}              //Email
        //Daos del contacto
        public String nombreContacto {get;set;}             //FistName
        public String apellidoPaternoContacto {get;set;}    //LastName
        public String apellidoMaternoContacto {get;set;}    //FWY_ApellidoMaterno__c
        public String rfcContacto {get;set;}                //TAM_RFC__c
        public String TelefonoContacto {get;set;}           //Phone
        public String celularContacto {get;set;}            //MobilePhone
        public String emailContacto {get;set;}              //Email
        public String direccionContacto {get;set;}          //TAM_Calle__c
        public String noIntContacto {get;set;}              //TAM_Estado__c
        public String noExtContacto {get;set;}              //TAM_Pais__c
        public String cpContacto {get;set;}                 //TAM_CodigoPostal__c
        public String localidadContacto {get;set;}          //TAM_Ciudad__c
        public String coloniaContacto {get;set;}            //TAM_Colonia__c
        public String delegacionMunicipioContacto {get;set;}//
        public String estadoContacto {get;set;}             //TAM_Estado__c
        public String paisContacto {get;set;}               //TAM_Pais__c
        //Daos para Quiter
        public String TipoOferta {get;set;}                 //TipoOferta
        public String AsesorVentas {get;set;}               //AsesorVentas
        public String Origen {get;set;}                     //Origen
        public String Agencia {get;set;}                    //Agencia
        public String EstadoPedidoInicial {get;set;}        //EstadoPedidoInicial
        public String TipoVenta {get;set;}                  //TipoVenta
        public String TipoVentaDestino {get;set;}           //TipoVentaDestino
        //Lista de Vines
        public List<DatosVin> lstVines {get;set;}
    }

    public class DatosVin{
        public String VIN {get;set;}                //TAM_VINFacturado__c   
        public String uIDFactura {get;set;}         //FistName
        public String fechaFacturacion {get;set;}   //FistName
        public String numeroFactura {get;set;}      //FistName      
        public String estatus {get;set;}            //Si false entonces llenar el detalle del error
        public String detalleEstatus {get;set;}     //Se llena solo si estatusProceso = false
        public String estatusUpdateSFDC {get;set;}  //Cuando responde SFDC al Dealer        
    }


    //El metodo que va a consultar los datos de los clientes que ya estan en la etapa de pedido en proceso y 
    //ya estan listos para facturarrse
    public static String updateResultClienteFactura(String sBody){ 
        System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura sBody: ' + sBody);
        String sListaResultClientesFacturar = '';
        
        List<DatosProspectoResponse> lstDatosProspectoFinal = new List<DatosProspectoResponse>();
        Map<String, Lead> mapIdLeadObjetoLead = new Map<String, Lead>();
        Map<String, String> mapIdLeadErrorMsg = new Map<String, String>();
        Map<String, String> mapVinesErrorMsg = new Map<String, String>();
        Map<String, DatosVin> mapVinesErrorObj = new Map<String, DatosVin>();

        Set<String> setIdSfdc = new Set<String>();
        Map<String, Lead> mapIdLeadObjLead = new Map<String, Lead>();
        Map<String, Map<String, TAM_LeadInventarios__c >> mapIdLeadLstObjInventLead = new Map<String, Map<String, TAM_LeadInventarios__c >>();
        Map<String, TAM_LeadInventarios__c> mapIdExtInvtLead = new Map<String, TAM_LeadInventarios__c>();
        Map<String, String> mapIdLeadError = new Map<String, String>();
        Map<String, String> mapIdLeadSuccess = new Map<String, String>();
        Map<String, String> mapIdLeadInvError = new Map<String, String>();
        Map<String, String> mapIdLeadInvSuccess = new Map<String, String>();
        
        //Un ibjeto de tipo  Savepoint
        Savepoint sv = Database.setSavepoint();     
        try{

            //Inicializa la variable sListaResultClientesFacturar
            sListaResultClientesFacturar = '{"lstDatosProspecto":';         

            //String sJsonPrueba = '{\"lstDatosProspecto\": [{\"IdProspectoSFDC\": \"00Qe000000BbcbXEAR\",\"numeroCliente\": \"123456789\",\"uIDFactura\": \"b315ab4e-515c-403e-8fc2-ba274029a9d2\",\"estatusProceso\": true,\"detallErrorProceso\" : \"\",\"estatusUpdateSFDC\" : false},{\"IdProspectoSFDC\": \"00Qe000000BbS5gEAF\",\"numeroCliente\": \"123456789\",\"uIDFactura\": \"b315ab4e-515c-403e-8fc2-ba274029a9d2\",\"estatusProceso\": false,\"detallErrorProceso\" : \"Error a la hora de obtener el UID de la factura: servicio no disponible.\",\"estatusUpdateSFDC\" : false}]}';
            //Serializa la lista de prspectos a facturar sJsonPrueba
            SFDC_UpdateResultClienteFactura_ctrl_rst objUpdResulCteFacu = (SFDC_UpdateResultClienteFactura_ctrl_rst) System.JSON.deserialize(sBody, SFDC_UpdateResultClienteFactura_ctrl_rst.class);        
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura objUpdResulCteFacu: ' + objUpdResulCteFacu);
            
            //Recorre la lista y toma todos los id de los prospectos para ver si existen en SFDC
            for (DatosProspecto objDatosProspectoPaso : objUpdResulCteFacu.lstDatosProspecto){
                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura objDatosProspectoPaso: ' + objDatosProspectoPaso);
                if (objDatosProspectoPaso.IdProspectoSFDC != null && objDatosProspectoPaso.IdProspectoSFDC != '')
                    setIdSfdc.add(objDatosProspectoPaso.IdProspectoSFDC);
            }
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura setIdSfdc: ' + setIdSfdc);
            
            //Si es una prueba solo inicializa la lista de setIdSfdc
            //setIdSfdc.add('00Qe000000BcxpsEAB');
            //setIdSfdc.add('00Qe000000Bcy0vEAB');
            
            //Busca los leads setIdSfdc             
            for(Lead candidatoPaso : [Select id, Name, TAM_PedidoFacturado__c, status,
                (Select id, Name, TAM_InventarioVehiculosFyG__c, TAM_Prospecto__c, TAM_NoFactura__c
                   From TAM_LeadInventProspecto__r )
                From Lead Where id IN :setIdSfdc]){
                mapIdLeadObjLead.put(candidatoPaso.id, candidatoPaso);
                //Ve si tiene inventario asociado
                if (!candidatoPaso.TAM_LeadInventProspecto__r.isEmpty()){
                    for (TAM_LeadInventarios__c objInvLead : candidatoPaso.TAM_LeadInventProspecto__r){
                        if (mapIdLeadLstObjInventLead.containsKey(candidatoPaso.id))
                            mapIdLeadLstObjInventLead.get(candidatoPaso.id).put(objInvLead.Name, objInvLead);
                        if (!mapIdLeadLstObjInventLead.containsKey(candidatoPaso.id))
                            mapIdLeadLstObjInventLead.put(candidatoPaso.id, new Map<String, TAM_LeadInventarios__c >{objInvLead.Name => objInvLead});                           
                    }//Fin del for ára candidatoPaso.TAM_LeadInventProspecto__r
                }//Fin si !candidatoPaso.TAM_LeadInventProspecto__r.isEmpty()
            }//Fin del for para los leads que se estan enviando 
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadLstObjInventLead: ' + mapIdLeadLstObjInventLead.KeySet());
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadLstObjInventLead: ' + mapIdLeadLstObjInventLead.Values());
            
            //Recorre la lista de reg del tipo  DatosProspecto
            for (DatosProspecto objDatosProspectoPaso : objUpdResulCteFacu.lstDatosProspecto){
                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura objDatosProspectoPaso2: ' + objDatosProspectoPaso);
                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura IdProspectoSFDC: ' + objDatosProspectoPaso.IdProspectoSFDC + ' Llave: ' + mapIdLeadLstObjInventLead.KeySet());
                //Ve si existe objDatosProspectoPaso.IdProspectoSFDC en el mapa de mapIdLeadObjLead
                if (mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)){
                    //System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura YA LO ENCONTRO IdProspectoSFDC: ' + objDatosProspectoPaso.IdProspectoSFDC + ' Llave: ' + mapIdLeadLstObjInventLead.KeySet());                    
                    
                    //Ve cuando reg vienen en la lista de vines
                    Integer cntTotVines = 0;
                    Integer cntTotVinesError = 0;
                    
                    //Recorre la lista de vines asociado al candidato
                    if (!objDatosProspectoPaso.lstVines.isEmpty()){
                        //Inicializa cntTotVines
                        cntTotVines = objDatosProspectoPaso.lstVines.size();
                        
                        //Busca el Prospecto en el mapa de mapIdLeadLstObjInventLead
                        //Map<String, Map<String, TAM_LeadInventarios__c >> mapIdLeadLstObjInventLead = new Map<String, Map<String, TAM_LeadInventarios__c >>();
                        Map<String, TAM_LeadInventarios__c > mapVinObjInventLead = new Map<String, TAM_LeadInventarios__c>();
                        if (mapIdLeadLstObjInventLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC))
                            mapVinObjInventLead = mapIdLeadLstObjInventLead.get(objDatosProspectoPaso.IdProspectoSFDC);
                        //Una lista para los vinos
                        Integer intCntVines = 1;    
                        //Recore la lista de Vines que vienen en la respuesta.
                        for (DatosVin objDatosVin : objDatosProspectoPaso.lstVines){
                            String IdExterno = objDatosProspectoPaso.IdProspectoSFDC + '-' + objDatosVin.VIN;
                            Date dtFechaFactura = objDatosVin.fechaFacturacion != null ? ValidaFechaFacturacion(objDatosVin.fechaFacturacion) : null;   
                            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura VIN: ' + objDatosVin.VIN + ' uIDFactura: ' + objDatosVin.uIDFactura + ' fechaFacturacion: ' + objDatosVin.fechaFacturacion);
                                
                            //String sDetalle error
                            String sDetalleError = '';
                            //Valida si contiene los campos que son indispensables para la facturación
                            if (objDatosVin.VIN == null || objDatosVin.VIN == '')
                                  sDetalleError += 'El campo del VIN viene vacio, ';
                            if (objDatosVin.uIDFactura == null || objDatosVin.uIDFactura == '')
                                  sDetalleError += 'El campo del UID Factura viene vacio, ';                                
                            if (objDatosVin.fechaFacturacion == null || objDatosVin.fechaFacturacion == '')
                                  sDetalleError += 'El campo del Fecha Facturacion viene vacio.';

                            //Metelo al mapa de mapIdExtInvtLead
                            if (sDetalleError == ''){
                                 mapIdExtInvtLead.put(IdExterno, new TAM_LeadInventarios__c(
                                    Name = mapIdLeadLstObjInventLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC) 
                                        ? ( mapVinObjInventLead.containsKey(objDatosVin.VIN) ? mapVinObjInventLead.get(objDatosVin.VIN).Name :  objDatosVin.VIN ) 
                                        : objDatosVin.VIN,
                                    TAM_Idexterno__c = IdExterno,
                                    TAM_uIDFactura__c = objDatosVin.uIDFactura, 
                                    TAM_FechaFacturacion__c = dtFechaFactura,
                                    TAM_VINFacturacion__c = objDatosVin.VIN,
                                    TAM_Prospecto__c = objDatosProspectoPaso.IdProspectoSFDC,
                                    TAM_NoFactura__c = objDatosVin.numeroFactura != null ? objDatosVin.numeroFactura : null
                                    )
                                 );                         
                            }//Fin si sDetalleError == ''
                                                        
                            //Hay error 
                            if (sDetalleError != ''){                                
                                String sIdExtErrorVinReg = (String.valueOf(intCntVines)) + '-' + (objDatosVin.VIN != null ? objDatosVin.VIN : '');
                                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura sIdExtErrorVinReg: ' + sIdExtErrorVinReg + ' sDetalleError: ' + sDetalleError);                                
                                mapVinesErrorMsg.put(sIdExtErrorVinReg, sDetalleError);
                                mapVinesErrorObj.put(sIdExtErrorVinReg, objDatosVin);
                                cntTotVinesError++;                             
                            }//Fin si 
                            
                            //Sigue contando los vines    
                            intCntVines++;    
                        }//Fin del for para objDatosProspectoPaso.lstVines
                        
                    }//Fin si !objDatosProspectoPaso.lstVines.isEmpty()
                    System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura cntTotVines: ' + cntTotVines + ' cntTotVinesError: ' + cntTotVinesError);
                    System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapVinesErrorMsg: ' + mapVinesErrorMsg);
                    System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapVinesErrorObj: ' + mapVinesErrorObj);

                    //Toma los datos del objeto y objDatosProspectoPaso y ve si no hubo error en el campo estatusProceso
                    mapIdLeadObjetoLead.put(objDatosProspectoPaso.IdProspectoSFDC, new Lead(
                            id = objDatosProspectoPaso.IdProspectoSFDC,
                            //Datos del cliente
                            FWY_ID_de_cliente__c = objDatosProspectoPaso.numeroCliente, 
                            TAM_NombreRL__c = objDatosProspectoPaso.nombre,
                            TAM_ApellidoPaternoRL__c = objDatosProspectoPaso.apellidoPaterno,
                            TAM_ApellidoMaternoRL__c = objDatosProspectoPaso.apellidoMaterno,
                            //FWY_Tipo_de_persona__c = objDatosProspectoPaso.tipoPersona,
                            TAM_RFCRL__c = objDatosProspectoPaso.rfc,
                            TAM_CorreoElectronicoRL__c = objDatosProspectoPaso.email,
                            TAM_TelefonoRL__c = objDatosProspectoPaso.Telefono,
                            TAM_TelefonoCelularRL__c = objDatosProspectoPaso.celular,
                            //Dirección contacto
                            TAM_Calle__c = objDatosProspectoPaso.direccion,
                            TAM_NoExt__c = objDatosProspectoPaso.noExt,
                            TAM_NoInt__c = objDatosProspectoPaso.noInt,
                            TAM_CodigoPostal__c = objDatosProspectoPaso.cp,
                            TAM_Colonia__c = objDatosProspectoPaso.colonia,
                            TAM_DelegacionMunicipio__c = objDatosProspectoPaso.delegacionMunicipio,
                            TAM_Ciudad__c = objDatosProspectoPaso.localidad,
                            TAM_Estado__c = objDatosProspectoPaso.estado,
                            TAM_Pais__c = objDatosProspectoPaso.pais,
                            //Daos para Quiter TRADUCIR LOS VALORES DEL CATALOGO
                            TAM_TipoOfertaFac__c = objDatosProspectoPaso.TipoOferta,
                            TAM_AsesorVentasFac__c = objDatosProspectoPaso.AsesorVentas,
                            TAM_OrigenFac__c = objDatosProspectoPaso.Origen,
                            TAM_AgenciaFac__c = objDatosProspectoPaso.Agencia,
                            TAM_EstadoPedidoInicialFac__c = objDatosProspectoPaso.EstadoPedidoInicial,
                            TAM_TipoVentaFac__c = objDatosProspectoPaso.TipoVenta,
                            TAM_TipoVentaDestinoFac__c = objDatosProspectoPaso.TipoVentaDestino,
                            //Datos del contacto
                            TAM_NombreContFact__c = objDatosProspectoPaso.nombreContacto,
                            TAM_ApellidoPaternoContFact__c = objDatosProspectoPaso.apellidoPaternoContacto,
                            TAM_ApellidoMaternoContFact__c = objDatosProspectoPaso.apellidoMaternoContacto,
                            TAM_RFCContFact__c = objDatosProspectoPaso.rfcContacto,
                            TAM_TelefonoContFact__c = objDatosProspectoPaso.TelefonoContacto,
                            TAM_TelefonoCelularContFact__c = objDatosProspectoPaso.celularContacto,
                            TAM_CorreoElectronicoContFact__c = objDatosProspectoPaso.emailContacto,
                            //Dirección del contacto
                            TAM_CalleContFact__c = objDatosProspectoPaso.direccionContacto,
                            TAM_NoExtContacto__c = objDatosProspectoPaso.noExtContacto,
                            TAM_NoIntContacto__c = objDatosProspectoPaso.noIntContacto,
                            TAM_CodigoPostalContFact__c = objDatosProspectoPaso.cpContacto,
                            TAM_CiudadContFact__c = objDatosProspectoPaso.localidadContacto,
                            TAM_ColoniaContFact__c = objDatosProspectoPaso.coloniaContacto,
                            TAM_DelegacionMunicipioContFact__c = objDatosProspectoPaso.delegacionMunicipioContacto,
                            TAM_EstadoContFact__c = objDatosProspectoPaso.estadoContacto,
                            TAM_PaisContFact__c = objDatosProspectoPaso.paisContacto,
                            //Estado del prospecto
                            TAM_PedidoFacturado__c = cntTotVines > cntTotVinesError ? true : false,
                            status = cntTotVines > cntTotVinesError ? 'Facturado' : mapIdLeadObjLead.get(objDatosProspectoPaso.IdProspectoSFDC).status,
                            FWY_Estado_del_candidatoWEB__c = cntTotVines > cntTotVinesError ? 'Facturado' : mapIdLeadObjLead.get(objDatosProspectoPaso.IdProspectoSFDC).status
                        )
                    );
                    
                }//Fin si mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)           
                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura YA LO ENCONTRO mapIdLeadObjetoLead: ' + mapIdLeadObjetoLead.KeySet());
            
            }//Fin del for para la lista de objUpdResulCteFacu.lstDatosProspecto            
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadObjetoLead: ' + mapIdLeadObjetoLead);
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdExtInvtLead: ' + mapIdExtInvtLead);
            
            //Actualiza entonces el objeto del lead del mapa mapIdLeadObjetoLead
            if (!mapIdLeadObjetoLead.isEmpty()){
                Integer iCntCont = 0;
                List<Lead> lCandUps = mapIdLeadObjetoLead.values();             
                List<Database.UpsertResult> lUpsertResultCand = Database.upsert(lCandUps, Lead.id, false);
                for (Database.UpsertResult objDtbrs : lUpsertResultCand){
                    if (!objDtbrs.isSuccess())
                        mapIdLeadError.put(lUpsertResultCand.get(iCntCont).id, 'Hubo un error a la hora de crear/actualizar el lead: ' + objDtbrs.getErrors()[0].getMessage()); 
                    if (objDtbrs.isSuccess())
                        mapIdLeadSuccess.put(objDtbrs.getId(), 'El candidato se crear/actualizar correctamente: ' + objDtbrs.getId());
                    iCntCont++;     
                }//Fin del for para iCntCont
            }//Fin si !mapIdLeadObjetoLead.isEmpty()                
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadError: ' + mapIdLeadError);
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadSuccess: ' + mapIdLeadSuccess);

            //Actualiza los datos de los vines del mapa mapIdLeadLstObjInventLead
            if (!mapIdExtInvtLead.isEmpty()){
                Integer iCntCont = 0;
                List<TAM_LeadInventarios__c> lCandUps = mapIdExtInvtLead.values();              
                List<Database.UpsertResult> lUpsertResultCand = Database.upsert(lCandUps, TAM_LeadInventarios__c.TAM_Idexterno__c, false);
                for (Database.UpsertResult objDtbrs : lUpsertResultCand){
                    if (!objDtbrs.isSuccess())
                        mapIdLeadInvError.put(lUpsertResultCand.get(iCntCont).id, 'Hubo un error a la hora de crear/Actualizar el Inventario: ' + objDtbrs.getErrors()[0].getMessage());    
                    if (objDtbrs.isSuccess())
                        mapIdLeadInvSuccess.put(objDtbrs.getId(), 'El Inventario se creo/actualizo correctamente: ' + objDtbrs.getId());
                    iCntCont++;     
                }//Fin del for para iCntCont
            }//Fin si mapIdLeadLstObjInventLead.isEmpty()
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadInvError: ' + mapIdLeadInvError);
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadInvSuccess: ' + mapIdLeadInvSuccess);
            
            Map<String, Map<String, TAM_LeadInventarios__c>> mapIdLeadIdExtInvObjInv = new Map<String, Map<String, TAM_LeadInventarios__c>>();
            //Consulta los leads que se acabande actualizar
            for (TAM_LeadInventarios__c objLeadInven : [Select Id, Name, TAM_Idexterno__c, TAM_uIDFactura__c, TAM_FechaFacturacion__c, 
                TAM_Prospecto__c, TAM_VINFacturacion__c, TAM_NoFactura__c
                From TAM_LeadInventarios__c Where id IN :mapIdLeadInvSuccess.KeySet()]){
                System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura objLeadInven: ' + objLeadInven);
                if (mapIdLeadIdExtInvObjInv.containsKey(objLeadInven.TAM_Prospecto__c))
                    mapIdLeadIdExtInvObjInv.get(objLeadInven.TAM_Prospecto__c).put(objLeadInven.TAM_Idexterno__c, objLeadInven);
                if (!mapIdLeadIdExtInvObjInv.containsKey(objLeadInven.TAM_Prospecto__c))                    
                    mapIdLeadIdExtInvObjInv.put(objLeadInven.TAM_Prospecto__c, new Map<String, TAM_LeadInventarios__c>{objLeadInven.TAM_Idexterno__c => objLeadInven});
            }
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadIdExtInvObjInv.KeySet(): ' + mapIdLeadIdExtInvObjInv.KeySet());
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura mapIdLeadIdExtInvObjInv.Values(): ' + mapIdLeadIdExtInvObjInv.Values());
            
            //Recorre la lista de lstDatosProspecto y crea la salida
            for (DatosProspecto objDatosProspectoPaso : objUpdResulCteFacu.lstDatosProspecto){
                //Crea el objeto del tipo DatosProspectoResponse
                DatosProspectoResponse objDatosProspectoResponse = new DatosProspectoResponse();
                objDatosProspectoResponse.IdProspectoSFDC = objDatosProspectoPaso.IdProspectoSFDC;
                objDatosProspectoResponse.estatusProceso = true;
                objDatosProspectoResponse.detalleErrorProceso = '';
                objDatosProspectoResponse.estatusUpdateSFDC = 'OK';
                //Crea una lista del tipo List<DatosVinResponse> lstVines
                List<DatosVinResponse> lstVines = new List<DatosVinResponse>(); 
                //Ve si existe objDatosProspectoPaso.IdProspectoSFDC en el mapa de mapIdLeadObjLead
                if (mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)){
                    //Ve si tiene vines asociados en mapVinObjInventLead
                    if (mapIdLeadIdExtInvObjInv.containsKey(objDatosProspectoPaso.IdProspectoSFDC)){
                        Map<String, TAM_LeadInventarios__c > mapVinObjInventLead = mapIdLeadIdExtInvObjInv.get(objDatosProspectoPaso.IdProspectoSFDC);
                        for (String sVin : mapVinObjInventLead.KeySet()){
                            String sIdInvnLead = mapVinObjInventLead.get(sVin).id;
                            //Crea el objeto del tipo DatosVinResponse 
                            DatosVinResponse objDatosVinResponse = new DatosVinResponse();
                            objDatosVinResponse.VIN = mapVinObjInventLead.get(sVin).TAM_VINFacturacion__c;      
                            objDatosVinResponse.uIDFactura = mapVinObjInventLead.get(sVin).TAM_uIDFactura__c;
                            objDatosVinResponse.numeroFactura = mapVinObjInventLead.get(sVin).TAM_NoFactura__c != null ? mapVinObjInventLead.get(sVin).TAM_NoFactura__c : null;
                            objDatosVinResponse.fechaFacturacion = mapVinObjInventLead.get(sVin).TAM_FechaFacturacion__c != NULL ? String.valueOf(mapVinObjInventLead.get(sVin).TAM_FechaFacturacion__c) : '';                          
                            objDatosVinResponse.estatus = mapIdLeadInvError.containsKey(sIdInvnLead) ? 'true' : 'false';
                            objDatosVinResponse.detalleEstatus = mapIdLeadInvError.containsKey(sIdInvnLead) ? mapIdLeadInvError.get(sIdInvnLead) : mapIdLeadInvSuccess.get(sIdInvnLead);
                            objDatosVinResponse.estatusUpdateSFDC = mapIdLeadInvError.containsKey(sIdInvnLead) ? 'true' : 'false';
                            lstVines.add(objDatosVinResponse);
                        }//Fin del for para  mapIdLeadIdExtInvObjInv.KeySet()
                        //Actualiza el camopo objDatosProspectoResponse.lstVines
                        objDatosProspectoResponse.lstVines = lstVines;
                    }//Fin si mapIdLeadIdExtInvObjInv.containsKey(objDatosProspectoPaso.IdProspectoSFDC)    
                    
                    //Ve si hay errorres
                    if (!mapVinesErrorMsg.isEmpty()){
                        //Recorre la lista de vines que tuvieron errores
                        for (String sVin : mapVinesErrorMsg.KeySet()){
                            //Crea el objeto del tipo DatosVinResponse 
                            DatosVinResponse objDatosVinResponse = new DatosVinResponse();
                            objDatosVinResponse.VIN = mapVinesErrorObj.get(sVin).VIN;      
                            objDatosVinResponse.uIDFactura = mapVinesErrorObj.get(sVin).uIDFactura;
                            objDatosVinResponse.fechaFacturacion = mapVinesErrorObj.get(sVin).fechaFacturacion;                          
                            objDatosVinResponse.estatus = 'true';
                            objDatosVinResponse.detalleEstatus = mapVinesErrorMsg.get(sVin);
                            objDatosVinResponse.estatusUpdateSFDC = 'false';
                            lstVines.add(objDatosVinResponse);                      
                        }//Fin del for para mapVinesErrorMsg.KeySet()
                        //Actualiza el camopo objDatosProspectoResponse.lstVines
                        objDatosProspectoResponse.lstVines = lstVines;                      
                    }//Fin si !mapVinesErrorMsg.isEmpty()
                    
                    //System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura objDatosProspectoResponse: ' + objDatosProspectoResponse); 
                    //Agrega el objeto objDatosProspectoResponse a la lista de lstDatosProspectoFinal.add(objDatosProspectoResponse);                   
                    lstDatosProspectoFinal.add(objDatosProspectoResponse);                  
                }//Fin si mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)
                //Ve si no existe objDatosProspectoPaso.IdProspectoSFDC en el mapa de mapIdLeadObjLead
                if (!mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)){
                    objDatosProspectoResponse.estatusProceso = false;
                    objDatosProspectoResponse.detalleErrorProceso = 'El Lead con el id: ' + objDatosProspectoPaso.IdProspectoSFDC + ' No exixte en SFDC';
                    objDatosProspectoResponse.estatusUpdateSFDC = 'Error';
                    //Actualiza el camopo objDatosProspectoResponse.lstVines
                    objDatosProspectoResponse.lstVines = lstVines;                  
                    //Agregalo a la lista de lstDatosProspecto
                    lstDatosProspectoFinal.add(objDatosProspectoResponse);                  
                }//Fin si no mapIdLeadObjLead.containsKey(objDatosProspectoPaso.IdProspectoSFDC)
                
            }//Fin del for para la lista de objUpdResulCteFacu.lstDatosProspecto
            //System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura lstDatosProspectoFinal: ' + lstDatosProspectoFinal);                                     

        }catch(Exception ex){
            System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura ERROR: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber());
            DatosProspectoResponse objDatosProspectoResponse = new DatosProspectoResponse();
            objDatosProspectoResponse.IdProspectoSFDC = '';
            objDatosProspectoResponse.estatusProceso = false;
            objDatosProspectoResponse.detalleErrorProceso = 'Error al tratar de actualizar los reg. de los candidatos en SFDC Error: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber();
            objDatosProspectoResponse.estatusUpdateSFDC = 'Error';
            //Crea una lista del tipo List<DatosVinResponse> lstVines
            List<DatosVinResponse> lstVines = new List<DatosVinResponse>(); 
            //Actualiza el camopo objDatosProspectoResponse.lstVines
            objDatosProspectoResponse.lstVines = lstVines;
            //Agregalo a la lista de lstDatosProspecto
            lstDatosProspectoFinal.add(objDatosProspectoResponse);
        }
        System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura lstDatosProspectoFinal: ' + lstDatosProspectoFinal);

        //Serializa la lista de  lstDatosProspecto en sListaResultClientesFacturar
        sListaResultClientesFacturar += JSON.serialize(lstDatosProspectoFinal);
        sListaResultClientesFacturar += '}';             
        //System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura sListaResultClientesFacturar: ' + sListaResultClientesFacturar);

        
        /*//Para pruebas nada mas *** LO BORRAS **** 
        sListaResultClientesFacturar = 
'{"lstDatosProspecto": ['+
    '{'+
      '"IdProspectoSFDC": "00Qe000000BbcbXEAR",'+
      '"NumeroCliente": "82040",'+
      '"Nombre": "JOSE",'+
      '"ApellidoPaterno": "ORTEGA",'+
      '"ApellidoMaterno": "HURTADO",'+
      '"TipoPersona": "PERSONA FISICA CON ACTIVIDAD EMPRESARIAL",'+
      '"Rfc": "HUHJ890101CG3",'+
      '"Telefono": "2345678905",'+
      '"Celular": "2345678905",'+
      '"Direccion": "PIRULES NO 5 1 3",'+
      '"Cp": "34567",'+
      '"Localidad": "IZTACALCO",'+
      '"Colonia": "EL SALDADO",'+
      '"DelegacionMunicipio": "IZTACALCO",'+
      '"Estado": "COLIMA",'+
      '"Pais": "MX",'+
      '"Email": "josehh@hotmail.com",'+
      '"NombreContacto": "ERNESTO",'+
      '"ApellidoPaternoContacto": "BARRERA",'+
      '"ApellidoMaternoContacto": "BARRERA",'+
      '"RfcContacto": "BABE780103HJ5",'+
      '"TelefonoContacto": "5567585555",'+
      '"CelularContacto": "5578945678",'+
      '"DireccionContacto": "CALLE 1 163",'+
      '"CpContacto": "02900",'+
      '"LocalidadContacto": "AZCAPOTZALCO",'+
      '"ColoniaContacto": "AGUILERA",'+
      '"DelegacionMunicipioContacto": "AZCAPOTZALCO",'+
      '"EstadoContacto": "CIUDAD DE MEXICO",'+
      '"PaisContacto": "MX",'+
      '"EmailContacto": "ernestobb@hotmail.com",'+
      '"estatusProceso": true,'+
      '"detallErrorProceso": "",'+
      '"estatusUpdateSFDC": false,'+
      '"lstVines": ['+
        '{'+
          '"VIN": "3TMAZ5CN0KM087137",'+
          '"uIDFactura": "0d94d403-5dba-4bff-b9a6-3bf4eb62d183",'+
          '"fechaFacturacion": "2020-08-25T00:00:00-05:00",'+
          '"estatus": "false",'+
          '"detalleEstatus": "",'+
          '"estatusUpdateSFDC": "false"'+
        '}'+
      ']'+
    '}'+
  ']'+
'}';    */
        
        //Solo para prueba
        //Database.rollback(sv);
        
        //Ve que tiene sListaResultClientesFacturar
        System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura sListaResultClientesFacturar: ' + sListaResultClientesFacturar);
        return sListaResultClientesFacturar;
    }
    
    
    public static Date ValidaFechaFacturacion(String ValidaFechaFacturacion){
        System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.ValidaFechaFacturacion ValidaFechaFacturacion: ' + ValidaFechaFacturacion);
        
        Date dFechaAct = Date.today();
        String sFechaPasoFinal = '';
        
        //Ve si ValidaFechaFacturacion tiene un '-'
        if (ValidaFechaFacturacion.contains('T')){
            sFechaPasoFinal = ValidaFechaFacturacion.substring(0 , ValidaFechaFacturacion.LastindexOf('T'));
            String sAnio = sFechaPasoFinal.substring(0, sFechaPasoFinal.IndexOf('-'));
            String sMes = sFechaPasoFinal.substring(sFechaPasoFinal.IndexOf('-')+1, sFechaPasoFinal.LastindexOf('-'));
            String sDia = sFechaPasoFinal.substring(sFechaPasoFinal.LastindexOf('-')+1, sFechaPasoFinal.length());
            //System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.ValidaFechaFacturacion sAnio: ' + sAnio + ' ' + sMes + ' ' + sDia);
            dFechaAct = Date.newInstance(Integer.valueOf(sAnio), Integer.valueOf(sMes), Integer.valueOf(sDia));
        }//Fin si ValidaFechaFacturacion.contains('-')
            
        System.debug('EN SFDC_UpdateResultClienteFactura_ctrl_rst.ValidaFechaFacturacion dFechaAct: ' + dFechaAct);
        return dFechaAct;
    }
    
}