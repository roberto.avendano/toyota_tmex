/**
Descripción General: Clase Batch para Provisión de Incentivos
________________________________________________________________
Autor               Fecha               Descripción
________________________________________________________________
Cecilia Cruz        30/Junio/2020       Versión Inicial
________________________________________________________________
*/
public class TAM_ProvisionBatchBonos implements Database.Batchable<sObject>, Database.Stateful{
    
    //Variables y constantes.
    static final String STRING_SALESCODE_AUTODEMO_06 = '06';
    static final String STRING_SALESCODE_AUTODEMO_6 = '6';
    static final String STRING_FLEET_C = 'C';
    static final String STRING_RDR = 'RDR';
    static final String STRING_DEALERCODE_57000 = '57000';
    
    public final String recordId;
    public final String strMesProv;
    public final String strAnioProv;
    public final String AllVinesDelMes;
    public final Id RecordTypeId_Bono;
    public final Id RecordTypeId_Excepcion;
    
    public static Map<String, List<Movimiento__c>> mapMovimientosRetail;
    public static Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVtaCorporativa;
    public static Map<String, List<TAM_DetalleEstadoCuenta__c>> mapEdoCuenta;
    public static Set<String> setReversas;
    public static Set<String> setAllVinesDelMes;
    public static List<Movimiento__c> lstMovRetail;
    public static Map<String, List<TAM_DetalleBonos__c>> mapBonos;
    public static Map<String, TAM_SolicitudDeBono__c> mapExcepcionesBono;
    public static Map<String, TAM_DetalleProvisionIncentivo__c> mapDetalleBonos;
    public static Map<String, TAM_ModeloCodigoProducto__c> mapModeloCodigoProd;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaExcepciones;
    public static Map<String, TAM_CalendarioToyota__c> mapCalendarioTOYOTA;
    public static List<SolicitudAutoDemo__c> lstDemos;

    
    
    //Constructor
    public TAM_ProvisionBatchBonos(String record_Id, String strMesProvision, String strAnioProvision){

        recordId = record_Id;
        strAnioProv = strAnioProvision;
        strMesProv = strMesProvision;
        
        AllVinesDelMes = 'SELECT  VIN__r.Name FROM Movimiento__c WHERE Month__c =: strMesProv AND Year__c =: strAnioProv' +
                         ' AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_06  AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_6' +
                         ' AND Fleet__c !=: STRING_FLEET_C  AND VIN__r.Ultimo_Movimiento__r.Trans_Type__c =: STRING_RDR' + 
                         ' AND Distribuidor__r.Codigo_Distribuidor__c !=: STRING_DEALERCODE_57000';
        
        //Records Types
        RecordTypeId_Bono = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Bono').getRecordTypeId();
        RecordTypeId_Excepcion = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Excepcion').getRecordTypeId();

    }
    
    //Start
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(AllVinesDelMes);
    }
    
    //Excecute
    public void execute(Database.BatchableContext BC, List<sObject> scope){

        Set<String> setVIN = new Set<String>();
        for(sObject objScope : scope){
            Movimiento__c objVIN = (Movimiento__c)objScope;
            setVIN.add(objVIN.VIN__r.Name);
        }
        mapCalendarioTOYOTA = TAM_ProvisionHelperClass_Batch.getCalendarioTOYOTA();
        mapEdoCuentaExcepciones = TAM_ProvisionHelperClass_Batch.getEstadoCuentaExcepciones(strAnioProv, strMesProv);
        mapVtaCorporativa = TAM_ProvisionHelperClass_Batch.getVtaCorporativa(setVIN,strAnioProv, strMesProv, mapCalendarioTOYOTA);
        mapEdoCuenta = TAM_ProvisionHelperClass_Batch.getEstadoCuenta(setVIN);
        mapMovimientosRetail = TAM_ProvisionHelperClass_Batch.getMovimientosRetail(setVIN);
        lstMovRetail = TAM_ProvisionHelperClass_Batch.getlistaRetail(setVIN);
        mapBonos = TAM_ProvisionHelperClass_Batch.getBonos(strAnioProv, strMesProv);
        mapExcepcionesBono = TAM_ProvisionHelperClass_Batch.getExcepcionesBonos(strAnioProv, strMesProv);
        mapDetalleBonos = TAM_ProvisionHelperClass_Batch.getDetalleBonos(mapEdoCuentaExcepciones);
        mapModeloCodigoProd = TAM_ProvisionHelperClass_Batch.getModeloCodProd();
        lstDemos = TAM_ProvisionHelperClass_Batch.getDemos(setVIN);

        //Bonos
        setReversas = TAM_ProvisionHelperClass_Batch.setBonos(strMesProv, strAnioProv, recordId, RecordTypeId_Bono, 
                                                              setVIN, mapMovimientosRetail, lstMovRetail, mapBonos, mapVtaCorporativa,
                                                              mapExcepcionesBono, mapEdoCuenta, mapModeloCodigoProd, lstDemos);

        //Reversas
        TAM_ProvisionHelperClass_Batch.setReversasBonos(strMesProv, strAnioProv, recordId, RecordTypeId_Bono, lstMovRetail, setReversas, mapEdoCuenta, mapModeloCodigoProd, mapBonos,mapVtaCorporativa);
        
        //Excepciones
        TAM_ProvisionHelperClass_Batch.setExcepcionBonos( recordId,  mapEdoCuentaExcepciones,  mapDetalleBonos,  RecordTypeId_Excepcion, mapModeloCodigoProd);
        
        
    }
    
    //Finish
    public void finish(Database.BatchableContext BC){}
    
}