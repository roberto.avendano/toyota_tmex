/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_AutorizacionProspectoCtrl_Tst {

	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
	static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();

	@TestSetup static void loadData(){
		
		Account clienteMoral = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'TestAccount',
			LastName = 'TestAccountLastName',
			Codigo_Distribuidor__c = '570551',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  
		
        Lead cand = new Lead(
	        RecordTypeId = VaRtLeadFlotilla,
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '224',
	        Status='Nuevo Lead',
	        LastName = 'Test3',
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Programa'      	        
        );
	    insert cand;
             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}


    static testMethod void TAM_AutorizacionProspectoCtrlLeadOK() {
        Test.startTest();
			//Consulta los datos del cliente
			candidatoPrueba = [Select Id, Name, TAM_EnviarAutorizacion__c, RecordTypeId, FWY_Tipo_de_persona__c, 
			TAM_TipoCandidato__c From Lead LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk candidatoPrueba: ' + candidatoPrueba);
	        
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(candidatoPrueba.id);
	
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Person física';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(candidatoPrueba.id);
	
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Persona moral';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(candidatoPrueba.id);
	
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Persona moral';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Flotilla'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(candidatoPrueba.id);
	
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Person física';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getDatosCandidato
	       TAM_AutorizacionProspectoCtrl.getDatosCandidato(candidatoPrueba.id);
	       
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Fisica con actividad empresarial';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getDatosCandidato
	       TAM_AutorizacionProspectoCtrl.getDatosCandidato(candidatoPrueba.id);
	       
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Person física';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Flotilla'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getDatosCandidato
	       TAM_AutorizacionProspectoCtrl.getDatosCandidato(candidatoPrueba.id);
	       
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Persona moral';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getDatosCandidato
	       TAM_AutorizacionProspectoCtrl.getDatosCandidato(candidatoPrueba.id);
	
		   candidatoPrueba.FWY_Tipo_de_persona__c = 'Fisica con actividad empresarial';
		   candidatoPrueba.TAM_TipoCandidato__c = 'Flotilla'; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo getDatosCandidato
	       TAM_AutorizacionProspectoCtrl.getDatosCandidato(candidatoPrueba.id);
	       
	       candidatoPrueba.TAM_EnviarAutorizacion__c = true; 
	       update candidatoPrueba;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo enviaSolicitudAproba
	       TAM_AutorizacionProspectoCtrl.enviaSolicitudAproba(candidatoPrueba.id);
      Test.stopTest();
    }
 
     static testMethod void TAM_AutorizacionProspectoCtrlClienteOK() {
        Test.startTest();
			//Consulta los datos del cliente
			clientePruebaMoral = [Select Id, TAM_EstatusCliente__c, RecordTypeId From Account 
				Where Codigo_Distribuidor__c = '570550' LIMIT 1];
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk clientePruebaMoral: ' + clientePruebaMoral);
	
			clientePruebaFisica = [Select Id, TAM_EstatusCliente__c, RecordTypeId From Account 
				Where Codigo_Distribuidor__c = '570551' LIMIT 1];
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk clientePruebaFisica: ' + clientePruebaFisica);
	        
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(clientePruebaMoral.id);
	
		   clientePruebaMoral.RecordTypeId = VaRtCteMoralNoVigente;
	       update clientePruebaMoral;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(clientePruebaMoral.id);
	
		   clientePruebaMoral.RecordTypeId = VaRtCteMoralPreAuto;
	       update clientePruebaMoral;
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(clientePruebaMoral.id);
	
		   clientePruebaFisica.RecordTypeId = VaRtCteFisicaNoVigente;  
	       update clientePruebaFisica;
	       
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
	       TAM_AutorizacionProspectoCtrl.consultDatosCand(clientePruebaFisica.id);
	
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo enviaSolicitudPreAuto
	       TAM_AutorizacionProspectoCtrl.enviaSolicitudPreAuto(clientePruebaFisica.id);
	
	       //Llama la clase de TAM_CargaArchivosCtrl y metodo enviaSolicitudActiva
	       TAM_AutorizacionProspectoCtrl.enviaSolicitudActiva(clientePruebaFisica.id);
        Test.stopTest();
    }
    
}