public class SolicitudSiteCambioPassConfController {
    public String csId{get;set;}
    
    public SolicitudSiteCambioPassConfController(ApexPages.StandardController stdcontroller){
        csId =stdcontroller.getId();
    }
    
    public PageReference goLogin(){
        PageReference page = System.Page.SolicitudSiteLogin;
        //PageReference page = new PageReference('/SiteRegisterConfirm');
        page.setRedirect(true);
        return page;
    }
    
}