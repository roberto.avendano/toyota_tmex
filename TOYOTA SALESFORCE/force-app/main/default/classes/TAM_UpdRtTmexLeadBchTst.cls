/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 /******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_UpdRtTmexLeadSch y TAM_UpdRtTmexLeadBch.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    03-Sep-2020          Héctor Figueroa             Creación
******************************************************************************* */
 
/**
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_UpdRtTmexLeadBchTst {
	static String VaRtLeadTraficoPiso = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Tráfico Piso').getRecordTypeId();        

	static  Lead objLead = new Lead();

	@TestSetup static void loadData(){

		Lead candidatoPrueba = new Lead( 
	        RecordTypeId = VaRtLeadTraficoPiso,
	        FWY_Intencion_de_compra__c = 'Posterior',
	        LeadSource = 'TMEX Cotizador web',
	        FirstName = 'Hector',
	        Email = 'hectfa@test.com',
	        phone = '5571123922',
	        Status='Nuevo Candidato',
	        LastName = 'Hector',
	        TAM_ConfirmaDatos__c = true 
		);						 
		insert candidatoPrueba;
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}


    static testMethod void TAM_UpdRtTmexLeadBchOk() {

		//Consulta los datos del cliente
		objLead = [Select Id, RecordTypeId, LeadSource, FWY_Intencion_de_compra__c From Lead LIMIT 1];        
   		System.debug('EN TAM_UpdRtTmexLeadBchTst objLead: ' + objLead);

		//Inicia las pruebas    	
        Test.startTest();

			List<String> lOrigenLead = new List<String>{'TMEX Cotizador web', 'TMEX Micrositio', 'TMEX Facebook'};
			List<String> lRTOrigenLead = new List<String>{'Tráfico Piso'};
			
			String sLOrigenLead = '(';
			//Recorre la lista de lOrigenLead
			for (String sOrigenLeadPaso : lOrigenLead){
				sLOrigenLead += '\'' + sOrigenLeadPaso + '\',';
			}
			sLOrigenLead = sLOrigenLead.substring(0, sLOrigenLead.lastIndexOf(','));
			sLOrigenLead += ')';
			System.debug('EN TAM_UpdRtTmexLeadSch.execute sLOrigenLead: ' + sLOrigenLead);
			
			String sLRTOrigenLead = '(';
			//Recorre la lista de lOrigenLead
			for (String sRTOrigenLeadPaso : lRTOrigenLead){
				sLRTOrigenLead += '\'' + sRTOrigenLeadPaso + '\',';
			}
			sLRTOrigenLead = sLRTOrigenLead.substring(0, sLRTOrigenLead.lastIndexOf(','));
			sLRTOrigenLead += ')';
			System.debug('EN TAM_UpdRtTmexLeadSch.execute sLRTOrigenLead: ' + sLRTOrigenLead);

			//Crea la consulta para los Leads
			String sQuery = 'Select Id, LeadSource, RecordTypeId ';
			sQuery += ' From Lead ';
			sQuery += ' Where LeadSource IN ' + sLOrigenLead;
			sQuery += ' And RecordType.Name IN ' + sLRTOrigenLead;
			sQuery += ' Order by LeadSource ';
			sQuery += ' Limit 1';
			System.debug('EN TAM_UpdRtTmexLeadSch.execute sQuery: ' + sQuery);
			
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	    	    
	        TAM_UpdRtTmexLeadBch objUpdRtTmexLeadBch = new TAM_UpdRtTmexLeadBch(sQuery);
    	    
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objUpdRtTmexLeadBch, 1);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_UpdRtTmexLeadSch objTAMMergeOppSch = new TAM_UpdRtTmexLeadSch();       		
       		System.schedule('EN TAM_UpdRtTmexLeadSch.execute.execute:', sch, objTAMMergeOppSch);
       		
        Test.stopTest();
        
    }
}