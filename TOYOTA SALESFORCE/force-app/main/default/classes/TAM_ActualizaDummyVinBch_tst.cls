/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_ActualizaDummyVinBch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    26-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActualizaDummyVinBch_tst {
	
	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
	
	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

	static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	
	static String sRectorTypePasoVinesFlotilla = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String sRectorTypePasoVinesPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
	
	static TAM_DODSolicitudesPedidoEspecial__c objTAMDODSolicitudesPedidoEspecial = new TAM_DODSolicitudesPedidoEspecial__c();
	
	static	Account clienteDealer = new Account();
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
	static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
	static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
	static	Rangos__c rango = new Rangos__c();
	static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
	static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
	static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();

	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;

		Rangos__c rangoPrograma = new Rangos__c(
			Name = 'UBER',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'UBER | 10 | 50',
			RecordTypeId = sRectorTypePasoPrograma
			 
		);
		insert rangoPrograma;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  

        TAM_InventarioVehiculosToyota__c objInvTransito = new TAM_InventarioVehiculosToyota__c(
        		  Name = 'XXXXXX1234561',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM',
                  Dummy_VIN__c = 'XXXXXX1234561'
        );
        insert objInvTransito;

		TAM_DODSolicitudesPedidoEspecial__c objTAMDODSolicitudesPedidoEspecial = new TAM_DODSolicitudesPedidoEspecial__c(       
	        TAM_DummyVin__c = 'XXXXXX1234561',
            TAM_Codigo__c  = '22060'
	    );
	    insert objTAMDODSolicitudesPedidoEspecial;  
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_ActualizaDummyVinBchOK() {
    	
		//Consulta los datos del cliente
		objTAMDODSolicitudesPedidoEspecial = [Select Id, TAM_VIN__c, TAM_DummyVin__c, TAM_Codigo__c  From TAM_DODSolicitudesPedidoEspecial__c LIMIT 1];        
   		System.debug('EN TAM_ActualizaDummyVinBchOK objTAMDODSolicitudesPedidoEspecial: ' + objTAMDODSolicitudesPedidoEspecial);
    	
        Test.startTest();
			String sQuery = 'Select Id, Name, TAM_VIN__c, TAM_DummyVin__c, TAM_Codigo__c, TAM_Estatus__c, TAM_TieneCheckout__c, TAM_DummyVINFormula__c, TAM_Historico__c ';
			sQuery += ' From TAM_DODSolicitudesPedidoEspecial__c ';
			sQuery += ' where TAM_VIN__c = null and TAM_DummyVin__c != null ';
			sQuery += ' Limit 1';			
			System.debug('EN TAM_ActualizaDummyVinBchOK.execute sQuery: ' + sQuery);
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	
    	    TAM_ActualizaDummyVinBch_cls objUpdDummyVinCls = new TAM_ActualizaDummyVinBch_cls(sQuery);
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objUpdDummyVinCls, 5);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_ActualizaDummyVinSch_cls objTAM_Jobh = new TAM_ActualizaDummyVinSch_cls();
       		System.schedule('Ejecuta_TAM_ActualizaDummyVinSch_cls', sch, objTAM_Jobh);
       		
        Test.stopTest();    	
    	
    }

}