public class TransportTriggerHandler extends TriggerHandler{
	private List<Transport__c> newItemsList;

	public TransportTriggerHandler() {
		this.newItemsList = (List<Transport__c>) Trigger.new;
	}

	public override void beforeInsert() {
		setCamposBefore(this.newItemsList);
	}

	public override void beforeUpdate() {
		setCamposBefore(this.newItemsList);
	}

	public static void setCamposBefore(List<Transport__c> newItems){
		Integer minutosDia = 24 * 60;

		for(Transport__c t : newItems){
			t.ExternalIdTransport__c = t.Name;
			if(t.TimeArrivalText__c!=null && t.TimeArrivalText__c!=''){
				List<String> horas = t.TimeArrivalText__c.split(':');
				if(horas.size()>=2){
					Integer vHoras = Integer.valueOf(horas.get(0));
					Integer vMinutos = Integer.valueOf(horas.get(1));
					Decimal totalMinutos = (vHoras * 60) + vMinutos;
					System.debug(totalMinutos);
					if(totalMinutos>0){
						t.TimeArrivalNumber__c = totalMinutos / minutosDia;
					}else{
						t.TimeArrivalNumber__c = 0;
					}
				}else{
					t.TimeArrivalNumber__c = 0;
				}
			}else{
				t.TimeArrivalText__c = '00:00';
				t.TimeArrivalNumber__c = 0;
			}
		}
	}
}