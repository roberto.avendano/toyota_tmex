@isTest
public class ConsolidadoPagoTFSTriggerHandlerTest {
    
    @isTest static void test_method_one(){
        Vehiculo__c vin = new Vehiculo__c(
        					Name='TESTSOLICITUD01');
        try{
            insert vin;
        }catch(Exception e){
            System.debug('Error vehiculo: '+e.getMessage());
        }
        SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c(
        							VIN__c=vin.Id, Estatus__c='Alta');
        try{
            insert sad;
        }catch(Exception e){
            System.debug('Error sad: '+e.getMessage());
        }
        ConsolidadoPagoTFS__c cp = new ConsolidadoPagoTFS__c(
        							SerialNumber__c=sad.Id);        
        try{
            insert cp;
        }catch(Exception e){
            System.debug('Error cp: '+e.getMessage());
        }
        
        sad.Estatus__c = 'Baja anticipada';
        try{
            update sad;
            update cp;
        }catch(Exception e){
            System.debug('Error update sad: '+e.getMessage());
            System.debug('Error update cp: '+e.getMessage());
        }
        
        cp.SerialNumber__c = null;
        cp.VIN__c = 'TESTSOLICITUD01';
        try{
            update cp;
        }catch(Exception e){
            System.debug('Error update cp sin sad, con vin sin vin existente: '+e.getMessage());
        }
        
        cp.VIN__c = 'TESTSOLICITUD02';
        try{
            update cp;
        }catch(Exception e){
            System.debug('Error update cp sin sad, con vin no existente: '+e.getMessage());
        }
        
        cp.VIN__c = '';
        try{
            update cp;
        }catch(Exception e){
            System.debug('Error update cp sin sad, sin vin: '+e.getMessage());
        }
    }  
}