@isTest
public class ResumenPartesRobadasTest {
    public static testmethod void test1(){ 
        Test.startTest();
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        Account accTest = new Account();
        accTest.recordTypeId = '012i00000002BxyAAE';
        accTest.Name = 'Cuenta de prueba AVX';
        insert accTest;
                
        String jobId = System.schedule('ResumenPartesRobadasTest', CRON_EXP, new ResumenPartesRobadas());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId]; 
        System.assertEquals(0, ct.TimesTriggered); 
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));   
        Test.stopTest();  

    }
    
}