@isTest 
public class TestControllerClonarFicha {
    
    @testSetup static void setup(){
        //Crear Serie de prueba 
        Serie__c serieTest = new Serie__c();
        serieTest.Name = 'RAV4Test';
        serieTest.Marca__c = 'Toyota';
        insert serieTest;
        
        //Crear Producto de prueba
        Product2 productModelo = new Product2();
        productModelo.RecordTypeId = '0121Y000001clPwQAI';
        productModelo.Family = 'Toyota';
        productModelo.Anio__c = '2019';
        productModelo.Serie__c = serieTest.id;
        productModelo.Name = '1402';
        productModelo.NombreVersion__c = 'Confortline';
        productModelo.IdExternoProducto__c = '18313';
        insert productModelo;
        
        //Se crea la ficha de producto de prueba
        FichaTecnicaProducto__c fichaProdTest = new FichaTecnicaProducto__c();
        fichaProdTest.Anio__c = '1010';
        fichaProdTest.Serie__c = serieTest.id;
        fichaProdTest.Estatus__c = 'Final';
        insert fichaProdTest;
        
        //Se crean las versionaes relacionadas a la ficha de tecnica de producto de prueba
        CodigoModeloFichaTecnica__c versionesFichaTecnica = new CodigoModeloFichaTecnica__c ();
        versionesFichaTecnica.CodigoModeloVersion__c = productModelo.id;
        versionesFichaTecnica.Estatus__c = 'Final';
        versionesFichaTecnica.FichaTecnica__c = fichaProdTest.Id;
        versionesFichaTecnica.Orden__c = 1; 
        insert versionesFichaTecnica;
        
        //Crear especificacion atributo
        EspecificacionAtributo__c atributosTest = new EspecificacionAtributo__c();
        atributosTest.Activo__c = true;
        atributosTest.TipoAtributo__c = 'Motor';
        atributosTest.Categoria__c = 'Especificaciones Técnicas';
        atributosTest.Opciones__c = '1|2|3';
        atributosTest.TipoEspecificacion__c = 'Motor';
        insert atributosTest;
        
        //Agregar atributos a la ficha de producto 
        EspecificacionFichaTecnica__c atributosFicha = new EspecificacionFichaTecnica__c();
        atributosFicha.Atributo__c = 'Motor';
        atributosFicha.EstatusEspecificacion__c = 'Final';
        atributosFicha.FichaTecnicaProducto__c = fichaProdTest.Id;
        atributosFicha.NoAplica__c = false;
        atributosFicha.CodigoModeloFichaTecnica__c = versionesFichaTecnica.id;
        atributosFicha.EspecificacionAtributo__c = atributosTest.Id;        
        insert atributosFicha;
        
        //Datos de prueba para la ficha de colores
        FichaColores__c fichaColoresTest = new FichaColores__c();
        fichaColoresTest.ColoresAgregados__c = 'Rojo,Azul,Negro';
        fichaColoresTest.Activo__c = true;
        fichaColoresTest.FichaTcnicaProducto__c = fichaProdTest.Id;
        insert fichaColoresTest;
        
        //Se crean un registro de acabado interior
        AcabadoInterior__c acabadoInteriorTest = new AcabadoInterior__c();
        acabadoInteriorTest.Codigo_de_Acabado__c = '20';
        acabadoInteriorTest.Name = 'Tela';
        insert acabadoInteriorTest;
        
        //Versiones relacionadas a las fichas de colores
        Versiones_Ficha_Colores__c versionesFichaColores = new Versiones_Ficha_Colores__c();
        versionesFichaColores.Ficha_Colores__c = fichaColoresTest.Id;
        versionesFichaColores.Orden__c = 1;
        versionesFichaColores.AcabadoInterior__c = acabadoInteriorTest.id;
        insert versionesFichaColores;
        
    }    
    
    @isTest
    static void methodTestObtenerVeriones(){
        FichaTecnicaProducto__c fichaTest = [Select id from FichaTecnicaProducto__c WHERE Anio__c = '1010' limit 1];
        Product2 prod = [Select id from Product2 WHERE name = '1402'];    
        Serie__c serieTest = [Select id From Serie__c where Name = 'RAV4Test' limit 1];
        CodigoModeloFichaTecnica__c codigoFichaTest = [Select id from CodigoModeloFichaTecnica__c WHERE Estatus__c = 'Final'];
        ControllerClonarFicha.obtenerVeriones('2019', serieTest.id, fichaTest.Id);
        
    }
    
    @isTest
    static void methodTestObtenerVerionesExeption(){
        ControllerClonarFicha.obtenerVeriones('1000','8765456789','8765456781');
        
    }
    
    @isTest
    static void clonarFichaTest(){
        FichaTecnicaProducto__c fichaTest = [Select id from FichaTecnicaProducto__c WHERE Anio__c = '1010' limit 1];    
        String anioNuevo = '2019';
        Map<String,String> jsonVersiones = new Map<String,String>();
        jsonVersiones.put('1402','1402');
        controllerClonarFicha.clonacionFichaTP(fichaTest.id,anioNuevo,jsonVersiones);    
        
        
    }
    
    @isTest
    static void clonarFichaTestExp(){
        FichaTecnicaProducto__c fichaTest = [Select id from FichaTecnicaProducto__c WHERE Anio__c = '1010' limit 1];    
        String anioNuevo = '2020';
        Map<String,String> jsonVersiones = new Map<String,String>();
        jsonVersiones.put('1402','1402');
        controllerClonarFicha.clonacionFichaTP(fichaTest.id,anioNuevo,jsonVersiones);    
        
        
    }
    
}