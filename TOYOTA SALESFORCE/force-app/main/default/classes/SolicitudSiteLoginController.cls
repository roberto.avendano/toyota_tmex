public class SolicitudSiteLoginController {
    public String csId{get;set;}
    public String username {get;set;}
    public String password {get;set;}
    public boolean showError{get;set;}
    
    public SolicitudSiteLoginController(ApexPages.StandardController stdcontroller){
        csId =stdcontroller.getId();
        showError=false;
    }
    
    public PageReference login() {
        Map<String,ContactoSite__c> mapCs = new Map<String,ContactoSite__c>();
        Map<Id,ContactoSite__c> mapCsId = new Map<Id,ContactoSite__c>();
        for(ContactoSite__c cs: [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c FROM ContactoSite__c]){
            mapCsId.put(cs.Id, cs);
            if(cs.Contacto__r.Email!=null){
                mapCs.put(cs.Contacto__r.Email, cs);
                System.debug(mapCs);
            }
        }
        
        if(mapCs.containsKey(username)){
            if(mapCs.get(username).Activo__c==true){
                System.debug('El usuario si esta registrado');
                if(mapCs.get(username).Contrasena__c==password){
                    System.debug('Usuario y contraseña correctos, coinciden para cs: ' +mapCs.get(username).Contacto__r.Email);
                    PageReference  page = new PageReference('/SolicitudSiteHome?id=' +mapCs.get(username).Id);
                    page.setRedirect(true);
                    return page;
                }else{
                     showError = true;
                    System.debug('Contraseña incorrecta,escribiste: ' +password + 'contraseña guardada: ' +mapCs.get(username).Contrasena__c);
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Contraseña incorrecta');
                    ApexPages.addMessage(msg);
                }
            } else{
                 showError = true;
                System.debug('El ususario no esta ACTIVO');
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Su usuario esta inactivo, contacte con su administrador para activar su usuario');
                ApexPages.addMessage(msg);  
            }
        }else{
             showError = true;
            System.debug('El ususario no esta registrado');
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'El nombre de usuario no existe, intente nuevamente o registrese');
            ApexPages.addMessage(msg);
        }
        
        return null;
    }
    
    public PageReference register() {
        PageReference page = System.Page.SolicitudSiteRegister;
        page.setRedirect(true);
        return page;
    }
    
    public PageReference olvidoPassword(){ 
        PageReference page = System.Page.SolicitudSiteOlvidoPassword;
        page.setRedirect(true);
        return page;
    }
}