@isTest
private class EvaluacionKodawariWizardControllerTest{

	private static String tipoEvaluacion = 'Kodawari';

	@isTest
	static void evaluaciones(){	
		Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(evalDealer);
		
		//Registro
		String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
		String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
		RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
		RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
		RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
		
		User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;
		User u2 = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM2@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser2@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u2;

		Seccion__c seccion01 = new Seccion__c(
			Name = 'Secccion01',
			RecordTypeId = recordTypeSec.Id,
			Activo__c = true
		);
		insert seccion01;
		Seccion__c seccion02 = new Seccion__c(
			Name = 'Secccion02',
			RecordTypeId = recordTypeSec.Id,
			Activo__c = true
		);
		insert seccion02;

		Pregunta__c pregunta01 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id,
			Activo__c = true
		);
		insert pregunta01;

		Pregunta__c pregunta02 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion02.Id,
			Activo__c = true
		);
		insert pregunta02;

		Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta01.Id
		);
		insert oe01;

		Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta02.Id
		);
		insert oe02;

		Account dealer = new Account(
			RecordTypeId = recordTypeDealer.Id,
			Name = 'Test Record'
		);
		insert dealer;

		evalDealer = new Evaluaciones_Dealer__c(
			RecordTypeId = recordTypeED.Id,
			Consultor_TSM_Titular__c = u2.Id,
			Nombre_Dealer__c = dealer.Id
		);
		insert evalDealer;

		Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
            Evaluacion_Dealer__c = evalDealer.Id,
            Usuario__c = u.Id,
            Seccion_Toyota_Mexico__c = seccion01.Id
        );
        insert rsc01;
		Relacion_Seccion_Consultor__c rsc02 = new Relacion_Seccion_Consultor__c(
            Evaluacion_Dealer__c = evalDealer.Id,
            Usuario__c = u2.Id,
            Seccion_Toyota_Mexico__c = seccion02.Id
        );
        insert rsc02;
        update evalDealer;

		Test.startTest();

			ApexPages.currentPage().getParameters().put(Constantes.EVALUACIONES_DEALER_FIELD_DEALER_ID,dealer.Name);
			ApexPages.currentPage().getParameters().put(Constantes.EVALUACIONES_DEALER_FIELD_DEALER_ID + '_lkid',dealer.Id);
        	EvaluacionKodawariWizardController wdc = new EvaluacionKodawariWizardController(stdController);

        	List<SelectOption> tiposRegistro = EvaluacionKodawariWizardController.getTipoCetificacionesRA(recordTypeED.Id);
        	Map<Id,Seccion__c> secciones = EvaluacionKodawariWizardController.getSeccionesRA('Seccion_Kodawari');
        	
    		System.debug(tiposRegistro.size());
    		System.debug(secciones.size());

        	if(tiposRegistro.size()>0 && secciones.size()>1){
        		System.debug(tiposRegistro);
        		System.debug(secciones);
	        	wdc.dealerSelected = dealer.Id;
				wdc.titularElegido = u.Id ;
				wdc.adjuntosElegidos = u2.Id + ',';

				String userSecc = u.Id + '-' + secciones.values().get(0).Id + ',' +
					u2.Id + '-' + secciones.values().get(1).Id;

				wdc.seccionesConsultor = userSecc;
				wdc.evalDealer.Tipo_de_Evaluacion_Kodawari__c = tiposRegistro.get(0).getValue();
				wdc.evalDealer.RecordTypeId = recordTypeED.Id;

	        	wdc.saveWizard();
        	}

        	stdController = new ApexPages.StandardController(evalDealer);
        	wdc = new EvaluacionKodawariWizardController(stdController);
        	wdc.saveWizard();

        	wdc.getTipoCetificaciones();
        	wdc.tipoCertificacion = 'Test';
        	EvaluacionKodawariWizardController.getAccountACRA(null);
        	EvaluacionKodawariWizardController.getAccountACRA('Test');

    		stdController = new ApexPages.StandardController(evalDealer);
        	wdc = new EvaluacionKodawariWizardController(stdController);
        	wdc.saveWizard();

        	wdc.titular = wdc.titular + 'titular';
        	wdc.adjuntos = new List<String>();

        	try{
        		wdc.evalDealer.Id = null;
        		wdc.evalDealer.RecordTypeId = recordTypeSec.Id;
        		wdc.saveWizard();
        	}catch(Exception e){
        		System.debug('Valida DML Exception:' + e.getMessage());
        	}
        Test.stopTest();
	}
}