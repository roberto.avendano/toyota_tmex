/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase para hacer un llamado de Web Service en Rest

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    28-Julio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class TAM_CallRestWebService {
    
    public class datosWebService{
        @AuraEnabled        
        public String sUrl {get;set;}
        @AuraEnabled        
        public String sMethod {get;set;}
        @AuraEnabled        
        public String sBody {get;set;}
        @AuraEnabled
        public Map<String, String> mapParameters {get;set;}
        
        public datosWebService(){
            this.sUrl = '';
            this.sMethod = '';
            this.sBody = '';        
            this.mapParameters = new Map<String, String>();         
        }
          
        public datosWebService(String sUrl, String sMethod, String sBody, Map<String, String> mapParameters){
            this.sUrl = sUrl;
            this.sMethod = sMethod;
            this.sBody = sBody;     
            this.mapParameters = mapParameters;         
        }
          
    }

    public class loginResponse{
        @AuraEnabled        
        public String UnniqId {get;set;}
        @AuraEnabled 
        public String DealarID {get;set;}
        @AuraEnabled 
        public String Dealar {get;set;}     
        @AuraEnabled 
        public String Token {get;set;}      
        @AuraEnabled
        public String strDate {get;set;}
        @AuraEnabled 
        public String access_token {get;set;}      

        public loginResponse(){
            this.UnniqId = '';
            this.DealarID = '';
            this.Dealar = '';       
            this.Token = '';
            this.strDate = '';
            this.access_token = '';
        }
          
        public loginResponse(String UnniqId, String DealarID, String Dealar, String Token, String strDate){
            this.UnniqId = UnniqId;
            this.DealarID = DealarID;
            this.Dealar = Dealar;       
            this.Token = Token;
            this.strDate = strDate;
        }
          
    }

    //Un Metodo static para el llamado del metodo Call
    public static TAM_RespCallWebService login(datosWebService objDatosWebService){
        System.debug('EN TAM_CallRestWebService.login objDatosWebService: ' + objDatosWebService);

        String sResponse = '';
        TAM_RespCallWebService objRespCallWebServ = new TAM_RespCallWebService();
        String sMetodo = 'api/login/authenticate';

        try{

            //Inicializa el URL para el servicio                
            String endpoint = objDatosWebService.sUrl;
            if (objDatosWebService.sMethod == 'GET')
                 endpoint += objDatosWebService.sBody;
            //Una prueba utilzando HTTP 
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setMethod(objDatosWebService.sMethod);
                        
            //Crea los parametros de encabezado
            for (String sParam : objDatosWebService.mapParameters.KeySet()){
                request.setHeader(sParam, objDatosWebService.mapParameters.get(sParam));
                System.debug('EN TAM_CallRestWebService.login Llave: ' + sParam + ' Valor: ' + objDatosWebService.mapParameters.get(sParam));           
            }//Fin del for para objDatosWebService.mapParameters.KeySet()
                        
            //Incializa del timeout
            request.setTimeout(20000);  
            //Incializa el Body con JSONBody            
            if (objDatosWebService.sMethod == 'POST')
                request.setBody(objDatosWebService.sBody);
                
            //'{"dealerId": "57039","apiKey": "7cb2035d-3be2-46f6-af82-27981714bcc0","apiSecret": "k/XkOJenZFARrNzL/M27iOh/FtBr1Guz"'
            System.debug('EN TAM_CallRestWebService.login request.getEndpoint: '+ request.getEndpoint());
            System.debug('EN TAM_CallRestWebService.login request.getHeader: '+ request.getHeader('Content-Type'));
            System.debug('EN TAM_CallRestWebService.login request.getMethod: '+ request.getMethod());
            System.debug('EN TAM_CallRestWebService.login request.getBody: '+ request.getBody());
                                
            //Has en llamado        
            HTTPResponse res;
            String sJasonRes;
            
            //Es una prueba
            if (Test.isRunningTest())
                res = new HTTPResponse();
            //Si no es una prueba
            if (!Test.isRunningTest())
                res = new Http().send(request);
            
            //Inicializa sJasonRes
            sJasonRes = res.getBody();
            System.debug('EN TAM_CallRestWebService.login sMethod: '+ objDatosWebService.sMethod + ' HTTP: ' + res + ' sJasonRes: ' + sJasonRes);

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(200);
            //No hubo errores
            if (res.getStatusCode() == 200){
                String sPruebaPaso = sJasonRes;             
                objRespCallWebServ.NombreMetodo = sMetodo;
                objRespCallWebServ.Error = false;
                objRespCallWebServ.Detalle = '';
                objRespCallWebServ.JsonRes = sPruebaPaso;
                System.debug('ANTES DE SALIR DE ' + sMetodo + ' objRespCallWebServ.JsonRes: ' + objRespCallWebServ.JsonRes);
            }

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(400);         
            //Hubo errores
            if (res.getStatusCode() == 400 || res.getStatusCode() == 401 || res.getStatusCode() == 404 || res.getStatusCode() == 405 || res.getStatusCode() == 500){ //|| Test.isRunningTest()  503
                objRespCallWebServ.NombreMetodo = sMetodo;              
                objRespCallWebServ.Error = true;
                objRespCallWebServ.Detalle = sJasonRes;
                objRespCallWebServ.JsonRes = sJasonRes;
            }//Fin si sJasonRes != null && sJasonRes.length() == 0          

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(503);
            if (res.getStatusCode() == 503){
                objRespCallWebServ.NombreMetodo = sMetodo;              
                objRespCallWebServ.Error = true;
                objRespCallWebServ.Detalle = res.getStatus();
                objRespCallWebServ.JsonRes = res.getStatus();
            }//Fin si sJasonRes != null && sJasonRes.length() == 0          
            
        }catch(Exception ex){
            System.debug('ERROR EN respRest AL HACER EL LLAMADO AL WS: ' + ex.getMessage() + ' getLineNumber: ' + ex.getLineNumber());
        }       

        System.debug('EN TAM_CallRestWebService.login objRespCallWebServ: '+ objRespCallWebServ);
        //Regresa la respuesta      
        return objRespCallWebServ;
    }

    
    //Un Metodo static para el llamado del metodo Call
    public static TAM_RespCallWebService call(datosWebService objDatosWebService){
        System.debug('EN TAM_CallRestWebService.call objDatosWebService: ' + objDatosWebService);

        String sResponse = '';
        TAM_RespCallWebService objRespCallWebServ = new TAM_RespCallWebService();
        String sMetodo = 'api/ConsultaDatosCliente';

        try{

            //Inicializa el URL para el servicio                
            String endpoint = objDatosWebService.sUrl;
            if (objDatosWebService.sMethod == 'GET')
                 endpoint += objDatosWebService.sBody;
            
            //Una prueba utilzando HTTP 
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setMethod(objDatosWebService.sMethod);
                        
            //Crea los parametros de encabezado
            for (String sParam : objDatosWebService.mapParameters.KeySet()){
                request.setHeader(sParam, objDatosWebService.mapParameters.get(sParam));
                System.debug('EN TAM_CallRestWebService.call Llave: ' + sParam + ' Valor: ' + objDatosWebService.mapParameters.get(sParam));            
            }//Fin del for para objDatosWebService.mapParameters.KeySet()

            //Incializa del timeout
            request.setTimeout(120000);  
            //Incializa el Body con JSONBody
            if (objDatosWebService.sMethod == 'POST' || objDatosWebService.sMethod == 'PUT')
                 request.setBody(objDatosWebService.sBody);

            System.debug('EN TAM_CallRestWebService.call request.getEndpoint: '+ request.getEndpoint());
            System.debug('EN TAM_CallRestWebService.call request.getHeader: '+ request.getHeader('Accept-Language'));
            System.debug('EN TAM_CallRestWebService.call request.getHeader: '+ request.getHeader('Authorization'));
            System.debug('EN TAM_CallRestWebService.call request.getMethod: '+ request.getMethod());
            System.debug('EN TAM_CallRestWebService.call request.getBody: '+ request.getBody());
            
            //Has en llamado        
            HTTPResponse res;
            String sJasonRes;

            //Es una prueba
            if (Test.isRunningTest())
                res = new HTTPResponse();
            //Si no es una prueba
            if (!Test.isRunningTest())
                res = new Http().send(request);
            
            sJasonRes = res.getBody();
            System.debug('EN TAM_CallRestWebService.call sMethod: '+ objDatosWebService.sMethod + ' HTTP: ' + res + ' sJasonRes: ' + sJasonRes);

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(200);                     
            //No hubo errores
            if (res.getStatusCode() == 200){
                String sPruebaPaso = sJasonRes;
                
                /*sPruebaPaso = '{ ' +
  '"listaClientes": [ ' +
    '{' +
      '"nombreClienteSFDC": "ALONSO ROMAN",' +
      '"apellidoPaterno": "DAVID",' + 
      '"apellidoMaterno": "CAMARENA",' + 
      '"rfc": "DACA900202BG5",' + 
      '"noCliente": "34587654",' + 
      '"telefono": "5545654345",' + 
      '"celular": "5545654345",' + 
      '"direccion": "DE LAS ROSAS",' + 
      '"cp": "34567",' + 
      '"localidad": "CDMX",' + 
      '"colonia": "AMP LAS ROSAS",' + 
      '"delegacionMunicipio": "IZTACALCO",' + 
      '"estado": "CDMX",' + 
      '"pais": "MEXICO",' + 
      '"Email": "alonsods@hotmail.com" ' +
    '}, ' +
    '{ ' +
      '"nombreClienteSFDC": "ALONSO",' +
      '"apellidoPaterno": "DAVID",' + 
      '"apellidoMaterno": "DAVID",' + 
      '"rfc": "DADA910203GH5",' + 
      '"noCliente": "45676545",' + 
      '"telefono": "5545654345",' + 
      '"celular": "5545654345",' + 
      '"direccion": "DE LAS PALMAS",' + 
      '"cp": "34567",' + 
      '"localidad": "CDMX",' + 
      '"colonia": "AMP LAS PALMAS",' + 
      '"delegacionMunicipio": "IZTACALCO",' + 
      '"estado": "CDMX",' + 
      '"pais": "MEXICO",' + 
      '"Email": "alonsodd@hotmail.com" ' +
    '}, ' +
    '{ ' +
      '"nombreClienteSFDC": "ALONSO",' +
      '"apellidoPaterno": "DAVID",' + 
      '"apellidoMaterno": "SOLIS",' + 
      '"rfc": "DASA900101CH5",' + 
      '"noCliente": "34567654",' + 
      '"telefono": "5545654345",' + 
      '"celular": "5545654345",' + 
      '"direccion": "DE LAS ROSAS",' + 
      '"cp": "34567",' + 
      '"localidad": "CDMX",' + 
      '"colonia": "AMP DE LAS ROSAS",' + 
      '"delegacionMunicipio": "IZTACALCO",' + 
      '"estado": "CDMX",' + 
      '"pais": "MEXICO",' + 
      '"Email": "alonsods@hotmail.com" ' +
    '} ' +
  '] ' +
'}';*/
                objRespCallWebServ.NombreMetodo = sMetodo;
                objRespCallWebServ.Error = false;
                objRespCallWebServ.Detalle = '';
                objRespCallWebServ.JsonRes = sPruebaPaso;
                System.debug('ANTES DE SALIR DE ' + sMetodo + ' objRespCallWebServ.JsonRes: ' + objRespCallWebServ.JsonRes);
            }

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(400);         
            //Hubo errores
            if (res.getStatusCode() == 400 || res.getStatusCode() == 401 || res.getStatusCode() == 404 || res.getStatusCode() == 405 || res.getStatusCode() == 500){ //|| Test.isRunningTest()  503
                objRespCallWebServ.NombreMetodo = sMetodo;              
                objRespCallWebServ.Error = true;
                objRespCallWebServ.Detalle = sJasonRes;
                objRespCallWebServ.JsonRes = sJasonRes;
            }//Fin si sJasonRes != null && sJasonRes.length() == 0          

            //Es una prueba
            if (Test.isRunningTest())
                res.setStatusCode(503);
            if (res.getStatusCode() == 503){
                objRespCallWebServ.NombreMetodo = sMetodo;              
                objRespCallWebServ.Error = true;
                objRespCallWebServ.Detalle = res.getStatus();
                objRespCallWebServ.JsonRes = res.getStatus();
            }//Fin si sJasonRes != null && sJasonRes.length() == 0          
            
        }catch(Exception ex){
            System.debug('ERROR EN call AL HACER EL LLAMADO AL WS: ' + ex.getMessage() + ' linea: ' + ex.getLineNumber());
            objRespCallWebServ.NombreMetodo = sMetodo;              
            objRespCallWebServ.Error = true;
            objRespCallWebServ.Detalle = ex.getMessage();
            objRespCallWebServ.JsonRes = ex.getMessage();
        }       

        System.debug('ANTES DE SALIR DE TAM_RespCallWebService.call: ' + objRespCallWebServ);       
        //Regresa la respuesta      
        return objRespCallWebServ;
    }
    
}