public with sharing class CCHeaderController {
    public static String CAMPOS_MENU_COMUNIDAD = 'Id, Name, Parent__c, TipoElemento__c, URL__c, Ficha__c, Ficha__r.Name, Ficha__r.URL__c';

    public static User userApp{get;set;}
    public List<MenuComunidadWrapper> aplicaciones{get; set;}
    public Map<Id,List<MenuComunidadWrapper>> mapMenus{get; set;}
    public boolean flagAppsAvailables{get;set;}
    public Document logo{
        get{
            return [SELECT Id FROM Document WHERE DeveloperName='LogoCCommunity' limit 1];
        }
        set;
    }


    public CCHeaderController(){
        mapMenus = new Map<Id,List<MenuComunidadWrapper>>();
        aplicaciones = new List<MenuComunidadWrapper>();
        userApp = [SELECT AppSelected__c, NombreMenuComunidad__c,Distribuidor__c FROM User WHERE Id =: UserInfo.getUserId()];
        System.debug(userApp);System.debug(userApp.Distribuidor__c);
        String idMenuComunidad = userApp.NombreMenuComunidad__c;
        flagAppsAvailables= false;
        if(idMenuComunidad!=null && idMenuComunidad!=''){
            List<MenuComunidad__c> listApps = (List<MenuComunidad__c>)Database.query('SELECT ' + CAMPOS_MENU_COMUNIDAD + ', (SELECT Id FROM Submenus__r order by Orden__c) FROM MenuComunidad__c WHERE Parent__r.Name =:idMenuComunidad  order by Orden__c');
            if(listApps.size() > 0){
                flagAppsAvailables= true;
                Set<Id> setIdMC = new Set<Id>();
                for(MenuComunidad__c mc : listApps){
                    setIdMC.add(mc.Id);
                    aplicaciones.add(new MenuComunidadWrapper(mc));
                    if(!mapMenus.containsKey(mc.Id)){
                        mapMenus.put(mc.Id,new List<MenuComunidadWrapper>());
                    }
                }
                for(MenuComunidad__c mc : (List<MenuComunidad__c>)Database.query('SELECT ' + CAMPOS_MENU_COMUNIDAD + ', (SELECT ' + CAMPOS_MENU_COMUNIDAD + ' FROM Submenus__r order by Orden__c) FROM MenuComunidad__c WHERE Parent__c IN:setIdMC  order by Orden__c')){
                    mapMenus.get(mc.Parent__c).add(new MenuComunidadWrapper(mc));
                }

            }
        }
    }
    
    @RemoteAction
    public static void saveApp(String app){        
        userApp = [SELECT AppSelected__c, NombreMenuComunidad__c FROM User WHERE Id =: UserInfo.getUserId()];
        userApp.AppSelected__c = '';
        userApp.AppSelected__c = app;        
        update userApp;
    }

    public class MenuComunidadWrapper{
        public MenuComunidad__c menuComunidad{get;set;}
        public List<MenuComunidadWrapper> subMenus{get;set;}

        public MenuComunidadWrapper(){
            this.subMenus = new List<MenuComunidadWrapper>();
        }
        public MenuComunidadWrapper(MenuComunidad__c menuComunidad){
            this();
            this.menuComunidad = menuComunidad;
            if(menuComunidad.Submenus__r != null){
                for(MenuComunidad__c item : menuComunidad.Submenus__r){
                    this.subMenus.add(new MenuComunidadWrapper(item));
                }
            }
            System.debug(getTieneSubmenu());
        }

        public Boolean getTieneSubmenu(){
            if(subMenus != null && subMenus.size() > 0){
                return true;
            }else{
                return false;
            }
        }

    }

    /*
    public List<Schema.DescribeTabSetResult> tabSetDesc{get;set;}
    public String appActive{get;set;} //Nombre de la aplicacion actual.
    public Map<String, InfoApp> tabsxApp{get;set;} //Mapa que retorna todas las tabs de cada aplicacion 
    public static User userApp{get;set;}
    public List<String> appsAvailables{get;set;}
    public String hostFinal{get;set;}
    public String imgProfileUrl{get;set;}

    public CCHeaderController() {    	
    	tabSetDesc = Schema.describeTabs();
    	tabsxApp = new Map<String, InfoApp>();
        userApp = [SELECT AppSelected__c, ListApps__c FROM User WHERE Id =: UserInfo.getUserId()];
        imgProfileUrl= [SELECT FullPhotoUrl FROM User WHERE Id =: UserInfo.getUserId()].FullPhotoUrl;		
        hostFinal  = ApexPages.currentPage().getHeaders().get('Referer');

        for(DescribeTabSetResult tsr : tabSetDesc) {
            InfoApp app= new InfoApp(tsr.getDescription(), tsr.getLabel(), tsr.getLogoUrl(), tsr.getNamespace(), tsr.isSelected());                     
            List<Schema.DescribeTabResult> tabDesc = tsr.getTabs();
            for(Schema.DescribeTabResult tr : tabDesc) {
                if(appActive==null && (tsr.isSelected())){
                    appActive= tsr.getLabel();                  
                }
                                
                if(!tabsxApp.containsKey(tsr.getLabel())){
                    tabsxApp.put(tsr.getLabel(), app);
                    tabsxApp.get(tsr.getLabel()).addTab(new InfoTab(tr.getColors(),  tr.getIconUrl(), tr.getIcons(), tr.getLabel(), tr.getMiniIconUrl(), tr.getSobjectName(), tr.getUrl(), tr.isCustom()));
                }else{
                    tabsxApp.get(tsr.getLabel()).addTab(new InfoTab(tr.getColors(),  tr.getIconUrl(), tr.getIcons(), tr.getLabel(), tr.getMiniIconUrl(), tr.getSobjectName(), tr.getUrl(), tr.isCustom()));
                }
                                                                    
            }                                               
        }

        if(!tabsxApp.containsKey(userApp.AppSelected__c)){
                userApp.AppSelected__c='';                
        }   

        if(userApp.ListApps__c==null || userApp.ListApps__c==''){
            flagAppsAvailables= false;

        }else{
            System.debug(userApp.ListApps__c);
            appsAvailables = userApp.ListApps__c.split(';');
            
            for(String key: tabsxApp.keySet()){
                if(!ifContainsKey(key)){
                    tabsxApp.remove(key);
                }
            }
           appsAvailables.clear();
           appsAvailables.addAll(tabsxApp.keySet());                     
        }        

        flagAppsAvailables= true;
        System.debug(JSON.serialize(userApp));
    	System.debug(JSON.serialize(tabsxApp));
    }
    
   @RemoteAction
    public static void saveApp(String app){        
        userApp = [SELECT ListApps__c, AppSelected__c FROM User WHERE Id =: UserInfo.getUserId()];
        userApp.AppSelected__c = app;        
        update userApp;
    }   
    
    public boolean ifContainsKey(String keyMap){
        boolean ifContain= false;
        for(String appFromPick: appsAvailables){
            if(keyMap==appFromPick){
                ifContain= true;
                break;
            }
        }
        return ifContain;
    }

    public class InfoTab{
    	public List<Schema.DescribeColorResult> colors{get;set;}
    	public String iconUrl{get;set;}
    	public List<Schema.DescribeIconResult> icons{get;set;}
    	public String label{get;set;}
    	public String miniIconUrl{get;set;}
    	public String sobjectName{get;set;}
    	public String url{get;set;}
    	public Boolean isCustom{get;set;}
    	
    	public InfoTab(List<Schema.DescribeColorResult> colors, String iconUrl, List<Schema.DescribeIconResult> icons, String label, String miniIconUrl, String sobjectName, String url, boolean isCustom){
    		this.colors= colors;
    		this.iconUrl = iconUrl;
    		this.icons= icons;
    		this.label= label;
    		this.miniIconUrl = miniIconUrl;
    		this.sobjectName = sobjectName;
    		this.url= url;
    		this.isCustom= isCustom;
    	}
    }
    
    public class InfoApp{
    	public String description{get;set;}
    	public String label{get;set;}
    	public String logoUrl{get;set;}
    	public String nameSpace{get;set;}
    	public boolean isSelected{get;set;}
    	public List<InfoTab> tabs{get;set;}
    	
    	public InfoApp(String description, String label, String logoUrl, String nameSpace, boolean isSelected){
    		this.description= description;
    		this.label= label;
    		this.logoUrl= logoUrl;
    		this.nameSpace= nameSpace;
    		this.isSelected = isSelected;
    		tabs = new List<InfoTab>();
    	}
    	        
    	public void addTab(InfoTab tab){
    		tabs.add(tab);    		
    	}
    }
 */
}