public  without sharing class TAM_SearchInventarioLead {
    
    
    @AuraEnabled
    public static Lead estatusLead(String leadId){
        System.debug('EN TAM_SearchInventarioLead.estatusLead leadId: ' + leadId);
        Lead[] candidatoEstatus = [Select id, Status, FWY_Estado_del_candidatoWEB__c From Lead Where Id =: leadId];

        System.debug('EN TAM_SearchInventarioLead.estatusLead candidatoEstatus: ' + candidatoEstatus);
        if(!candidatoEstatus.isEmpty()){
        	return candidatoEstatus[0];
        }else{
			return null;
        }
    }
    
    @AuraEnabled
    public static Boolean estatusFacturacionByLead(String leadId){
        Boolean DatosValidosFacturacion;
        Lead candidatoInit = [Select TAM_DatosValidosFacturacion__c From Lead Where Id =: leadId];
        if(candidatoInit.TAM_DatosValidosFacturacion__c == true){
            DatosValidosFacturacion = true;
            
        }
        if(candidatoInit.TAM_DatosValidosFacturacion__c == false){
            DatosValidosFacturacion = false ;
            
        }
        
        return DatosValidosFacturacion;
        
    }
    
    
    @AuraEnabled
    public static List <wrapVIN> getInventariosByLead (String leadId){
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List<TAM_LeadInventarios__c> listaInventarios = [Select id,TAM_InventarioVehiculosFyG__r.TAM_Assigned_Dealer__c,TAM_AnioModelo__c,TAM_VINFacturacion__c,TAM_Serie__c,TAM_Version__c,TAM_CodigoModelo__c,TAM_ColorExterior__c,TAM_ColorInterior__c,TAM_DiasPiso__c,TAM_Inventario__c,
                                                         TAM_InventarioVehiculosFyG__r.Numero_de_Orden__c,TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c,TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c,TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c,TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c,name,TAM_InventarioVehiculosFyG__r.id,TAM_InventarioVehiculosFyG__r.Model_Year__c,TAM_InventarioVehiculosFyG__r.Name, 
                                                         TAM_InventarioVehiculosFyG__r.Model_Number__c,TAM_InventarioVehiculosFyG__c,TAM_InventarioVehiculosFyG__r.Tipo_Inventario__c,TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c,
                                                         TAM_InventarioVehiculosFyG__r.TAM_Version__c,TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c from TAM_LeadInventarios__c Where TAM_Prospecto__c =: leadId];
        
        
        if(!listaInventarios.isEmpty()){
            for(TAM_LeadInventarios__c i : listaInventarios){
                wrapVIN recordVIN = new wrapVIN();
                recordVIN.dealerAsignadoOriginal = i.TAM_InventarioVehiculosFyG__r.TAM_Assigned_Dealer__c;
                recordVIN.VIN  = I.TAM_VINFacturacion__c;
                recordVIN.Id = i.TAM_InventarioVehiculosFyG__c;
                recordVIN.Model_Year = i.TAM_AnioModelo__c;
                recordVIN.Model_Number = i.TAM_CodigoModelo__c;
                recordVIN.codigoColorInterior = i.TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c;
                recordVIN.descripcionColorInterior = i.TAM_ColorInterior__c;
                recordVIN.codigoColorExterior = i.TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c;
                recordVIN.descripcionColorExterior = i.TAM_ColorExterior__c;
                recordVIN.Tipo_Inventario = i.TAM_Inventario__c;
                recordVIN.Toms_Series_Name = i.TAM_Serie__C;
                recordVIN.TAM_Version = i.TAM_Version__c;
                recordVIN.TAM_DiasPiso = i.TAM_DiasPiso__c;
                recordVIN.IdInventarioLead = i.Id;
                wrapListVIN.add(recordVIN);
            }
            
        }
        
        
        return wrapListVIN; 
    }
    
    
    @AuraEnabled
    public static List <wrapVIN > vinInventarioLead(String searchKeyWord,String userId,List<wrapVIN> listaVINES) {
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        String searchKey = searchKeyWord + '%';
        List<TAM_InventarioVehiculosToyota__c> listaInventarios = new List<TAM_InventarioVehiculosToyota__c>();
        List<TAM_InventarioVehiculosToyota__c> listaInventariosIN = new List<TAM_InventarioVehiculosToyota__c>();
        User[] usr;
        
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];  
        
        //VINES agregados para (discriminar) y  no vovler a agregar el mismo VIN
        List<String> listaVIN = new List<String>();
        if(!listaVINES.isEmpty()){
            for(wrapVIN reg : listaVINES){
                listaVIN.add(reg.VIN);
                
            }
            
        }
        
        //Con este query se obtienen los inventatios discriminando los VINES que ya esten siendo ocupados en la vista
        listaInventariosIN = [Select id,TAM_Assigned_Dealer__c,Numero_de_Orden__c,Exterior_Color_Code__c,Interior_Color_Code__c,Exterior_Color_Description__c,Interior_Color_Description__c,
                              name,Model_Year__c,Model_Number__c,Tipo_Inventario__c,Toms_Series_Name__c,TAM_Version__c,TAM_DiasPiso__c 
                              from TAM_InventarioVehiculosToyota__c WHERE Dealer_Code__c =: usr[0].CodigoDistribuidor__c 
                              AND (Toms_Series_Name__c like : searchKey  OR Model_Year__c like :  searchKey or Model_Number__c like : searchKey or name like : searchKey ) 
                              AND NAME NOT IN : listaVIN limit 50];    
        
        
        //Se guarda en un Set la lista de VINES obtenida
        Set<String> VINResult = new Set<String>();
        if(!listaInventariosIN.isEmpty()){
            for(TAM_InventarioVehiculosToyota__c i : listaInventariosIN){
                VINResult.add(i.name);        
            }
        }
        
        //Se consultan en el objeto TAM_LeadInventarios__c para consultar los VINES agregados y con estatus de facturacion en TAM_DatosValidosFacturacion__c TRUE
        List<TAM_LeadInventarios__c> listaLeadInvent = [Select id,TAM_InventarioVehiculosFyG__r.Numero_de_Orden__c,TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c,TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c,TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c,TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c,name,TAM_InventarioVehiculosFyG__r.id,TAM_InventarioVehiculosFyG__r.Model_Year__c,TAM_InventarioVehiculosFyG__r.Name, 
                                                        TAM_InventarioVehiculosFyG__r.Model_Number__c,TAM_InventarioVehiculosFyG__c,TAM_InventarioVehiculosFyG__r.Tipo_Inventario__c,TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c,
                                                        TAM_InventarioVehiculosFyG__r.TAM_Version__c,TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c from TAM_LeadInventarios__c Where TAM_InventarioVehiculosFyG__r.Name IN : VINResult AND TAM_Prospecto__c != null];
        
        
        //Se recorre la lista listaLeadInvent y se agrega a una lista de Set para descriminar
        Set<String> VINDescriminar = new Set<String>();
        if(!listaLeadInvent.isEmpty()){
            for(TAM_LeadInventarios__c i : listaLeadInvent){
                VINDescriminar.add(i.TAM_InventarioVehiculosFyG__r.name);
            }
        }
        
        //Query final para descriminar los VINES que ya estan ocupados en otros lead con estatus de facturación TRUE
        listaInventarios = [Select id,TAM_Assigned_Dealer__c,Numero_de_Orden__c,Exterior_Color_Code__c,Interior_Color_Code__c,Exterior_Color_Description__c,Interior_Color_Description__c,
                            name,Model_Year__c,Model_Number__c,Tipo_Inventario__c,Toms_Series_Name__c,TAM_Version__c,TAM_DiasPiso__c 
                            from TAM_InventarioVehiculosToyota__c WHERE Dealer_Code__c =: usr[0].CodigoDistribuidor__c 
                            AND (Toms_Series_Name__c like : searchKey  OR Model_Year__c like :  searchKey or Model_Number__c like : searchKey or name like : searchKey ) 
                            AND NAME NOT IN : VINDescriminar AND NAME NOT IN : listaVIN limit 50];    
        
        if(!listaInventarios.isEmpty()){
            for(TAM_InventarioVehiculosToyota__c i : listaInventarios){
                wrapVIN recordVIN = new wrapVIN();
                recordVIN.VIN  = I.Name;
                recordVIN.Id = i.id;
                recordVIN.dealerAsignadoOriginal = i.TAM_Assigned_Dealer__c;
                recordVIN.numeroOrden = i.Numero_de_Orden__c;
                recordVIN.Model_Year = i.Model_Year__c;
                recordVIN.Model_Number = i.Model_Number__c;
                recordVIN.codigoColorInterior = i.Interior_Color_Code__c;
                recordVIN.descripcionColorInterior = i.Interior_Color_Description__c;
                recordVIN.codigoColorExterior = i.Exterior_Color_Code__c;
                recordVIN.descripcionColorExterior = i.Exterior_Color_Description__c;
                recordVIN.Tipo_Inventario = i.Tipo_Inventario__c;
                recordVIN.Toms_Series_Name = i.Toms_Series_Name__c;
                recordVIN.TAM_Version = i.TAM_Version__c;
                recordVIN.TAM_DiasPiso = i.TAM_DiasPiso__c;
                wrapListVIN.add(recordVIN);
            }
        }
        
        system.debug('RESULTADO'+wrapListVIN);
        return wrapListVIN;
        
        
    }
    
    @AuraEnabled
    public static wrapVIN saveRecordInventario (wrapVIN VINInventario, String leadId){
        System.debug('EN saveRecordInventario leadId: ' + leadId);
        System.debug('EN saveRecordInventario VINInventario: ' + VINInventario);

        TAM_InventarioVehiculosToyota__c objInvApdo;
        Account objDist = new Account();
        String sCodigoDistribuidor = '';
        
        for (User usuarioAct : [Select id,CodigoDistribuidor__c FROM User where id =:UserInfo.getUserId()]){
            sCodigoDistribuidor = usuarioAct.CodigoDistribuidor__c != null ? usuarioAct.CodigoDistribuidor__c : '57000';
        }
        System.debug('En saveRecordInventario sCodigoDistribuidor: ' + sCodigoDistribuidor);
        
        for (Account dist : [Select a.TAM_NoDiasApartado__c From Account a
          Where a.Codigo_Distribuidor__c  =:sCodigoDistribuidor ]){
            objDist = dist;         
        }
        System.debug('En saveRecordInventario objDist: ' + objDist);

        //Inicializa los campos del objeto TAM_LeadInventarios__c
        TAM_LeadInventarios__c recordLeadInventario = new TAM_LeadInventarios__c();
        recordLeadInventario.TAM_Idexterno__c = leadId+'-'+VINInventario.VIN;
        recordLeadInventario.Name =  VINInventario.VIN;
        recordLeadInventario.TAM_Prospecto__c = leadId;
        recordLeadInventario.TAM_InventarioVehiculosFyG__c = VINInventario.Id;
        recordLeadInventario.TAM_AnioModelo__c  =  VINInventario.Model_Year;
        recordLeadInventario.TAM_Serie__c = VINInventario.Toms_Series_Name;
        recordLeadInventario.TAM_Version__c = VINInventario.TAM_Version;
        recordLeadInventario.TAM_CodigoModelo__c = VINInventario.Model_Number;
        recordLeadInventario.TAM_ColorExterior__c = VINInventario.descripcionColorExterior;
        recordLeadInventario.TAM_ColorInterior__c = VINInventario.descripcionColorInterior;
        recordLeadInventario.TAM_DiasPiso__c = VINInventario.TAM_DiasPiso;
        recordLeadInventario.TAM_Inventario__c = VINInventario.Tipo_Inventario;
        recordLeadInventario.TAM_VINFacturacion__c = VINInventario.VIN;
        //Consulta los datos del inventario en TAM_InventarioVehiculosToyota__c     
        for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id, t.Interior_Color_Code__c, 
            t.Exterior_Color_Code__c From TAM_InventarioVehiculosToyota__c t Where Name = :VINInventario.VIN]){
            //Si existe apartalo objInv
            recordLeadInventario.TAM_CodigoColorInterior__c = objInv.Exterior_Color_Code__c;
            recordLeadInventario.TAM_CodigoColorInterior__c = objInv.Interior_Color_Code__c;
        }
                  
        try{
            
	        //TAM_FechaFinApartado__c del distribuidor 
	        if (recordLeadInventario.TAM_Inventario__c == 'G' || Test.isRunningTest()){
	            recordLeadInventario.TAM_Apartado__c = true;
	            //Ve si tiene algo el campo de TAM_NoDiasApartado__c en el objeto de objDist
	            if (objDist.TAM_NoDiasApartado__c != null && objDist.TAM_NoDiasApartado__c != ''){
	              if (Integer.valueOf(objDist.TAM_NoDiasApartado__c) > 0){
	                  Date dtFechaAct = Date.today();
	                  Date dtFechaFinApdo = dtFechaAct.addDays(Integer.valueOf(objDist.TAM_NoDiasApartado__c)); 
	                  recordLeadInventario.TAM_FechaFinApartado__c = dtFechaFinApdo;
	                  //Consulta el inventario en el objeto de TAM_InventarioVehiculosToyota__c 
	                  for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
	                      Where Name = :VINInventario.VIN]){
	                      //Si existe apartalo objInv
	                      objInv.TAM_Apartado__c = true; 
	                      objInvApdo = new TAM_InventarioVehiculosToyota__c(id = objInv.id, 
	                      TAM_Apartado__c = true);
	                  }
	              }//Fin si Integer.valueOf(objDist.TAM_NoDiasApartado__c) > 0
	            }//Fin si objDist.TAM_NoDiasApartado__c != null && objDist.TAM_NoDiasApartado__c != ''
	        }//Fin si recordLeadInventario.TAM_Inventario__c == 'G'
	        System.debug('EN saveRecordInventario recordLeadInventario: ' + recordLeadInventario);      
	        System.debug('EN saveRecordInventario objInvApdo: ' + objInvApdo);      

            //Crea el reg del inventario
            insert recordLeadInventario;
                        
        }catch(Exception e){
            system.debug('EN saveRecordInventario Error general ' + e.getMessage());
            
        }        
        
        //Inizializa los campos del objeto recordVIN
        wrapVIN recordVIN = new wrapVIN();
        recordVIN.VIN  = VINInventario.VIN;
        recordVIN.Id = VINInventario.id;
        recordVIN.Model_Year = VINInventario.Model_Year;
        recordVIN.Model_Number = VINInventario.Model_Number;
        recordVIN.codigoColorInterior = VINInventario.codigoColorInterior;
        recordVIN.descripcionColorInterior = VINInventario.descripcionColorInterior;
        recordVIN.codigoColorExterior = VINInventario.codigoColorExterior;
        recordVIN.descripcionColorExterior = VINInventario.descripcionColorExterior;
        recordVIN.Tipo_Inventario = VINInventario.Tipo_Inventario;
        recordVIN.Toms_Series_Name = VINInventario.Toms_Series_Name;
        recordVIN.TAM_Version = VINInventario.TAM_Version;
        recordVIN.TAM_DiasPiso = VINInventario.TAM_DiasPiso;
        recordVIN.IdInventarioLead = recordLeadInventario.Id;
        recordVIN.dealerAsignadoOriginal  = VINInventario.dealerAsignadoOriginal;
        System.debug('EN saveRecordInventario recordVIN: ' + recordVIN);      
        
        //Regresa el objeto 
        return recordVIN;
        
    } 
    
    @AuraEnabled
    public static void deleteRecordInventario (String idToDelete){
        System.debug('EN deleteLeadInventario idToDelete: ' + idToDelete);
        TAM_InventarioVehiculosToyota__c objInvApdo;
        TAM_LeadInventarios__c  [] recordtoDelete ;
        recordtoDelete = [Select id from TAM_LeadInventarios__c Where Id =: idToDelete];
        String sVinInventario = '';
        
        //Consulta los datos del VIN en TAM_LeadInventarios__c
        for (TAM_LeadInventarios__c objLedInv : [Select id, Name From TAM_LeadInventarios__c Where id =:idToDelete]){
            sVinInventario = objLedInv.Name;
        }
        System.debug('EN deleteLeadInventario sVinInventario: ' + sVinInventario);
        
        //Si esta apartado en el objeto de TAM_InventarioVehiculosToyota__c liberalo
        for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
            Where Name = :sVinInventario]){
            objInvApdo = new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false);
        }
        System.debug('EN deleteLeadInventario objInvApdo: ' + objInvApdo);
        
        //Si se pudo apartar
        if (objInvApdo != null)
          update objInvApdo;
        
        //Elimina el reg del tipo recordtoDelete
        if(!recordtoDelete.isEmpty())
            delete recordtoDelete[0];
        
    }
    
    //Wrapper wrapVIN
    public class wrapVIN {
        @AuraEnabled public String VIN {get; set;} 
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String numeroOrden {get; set;}
        @AuraEnabled public String IdInventarioLead {get; set;}
        @AuraEnabled public String Model_Year {get; set;}
        @AuraEnabled public String Tipo_Inventario  {get; set;} 
        @AuraEnabled public String Toms_Series_Name {get; set;} 
        @AuraEnabled public String TAM_Version {get; set;}
        @AuraEnabled public String TAM_DiasPiso {get; set;}
        @AuraEnabled public String Model_Number {get; set;}
        @AuraEnabled public String codigoColorInterior {get; set;}
        @AuraEnabled public String descripcionColorInterior {get; set;}
        @AuraEnabled public String codigoColorExterior {get; set;}
        @AuraEnabled public String descripcionColorExterior {get; set;}
        @AuraEnabled public String dealerAsignadoOriginal {get; set;}
    }
}