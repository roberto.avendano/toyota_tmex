public class EspecificacionFichaTecnicaTriggerHandler Extends TriggerHandler {
    private List<EspecificacionFichaTecnica__c > atributosFTList;
    
    public EspecificacionFichaTecnicaTriggerHandler(){
        this.atributosFTList = (List<EspecificacionFichaTecnica__c>) Trigger.new;    
        
    }
    
    public override void afterUpdate(){
        actualizaEstatusAtributo(atributosFTList);	
    }
    
    public static void actualizaEstatusAtributo(List<EspecificacionFichaTecnica__c> atributosFT){
        List<String> idFichaProd = new List<String>();
        List<EspecificacionFichaTecnica__c> atributosFichaT = new List<EspecificacionFichaTecnica__c>();
        List<EspecificacionFichaTecnica__c> nuevosAtributosUpd = new  List<EspecificacionFichaTecnica__c>();
        
        for(EspecificacionFichaTecnica__c atributosNewFT : atributosFT){
            if(atributosNewFT.SubgrupoEspecificacion__c == 'Generales' && atributosNewFT.TipoAtributo__c == 'Estatus' && atributosNewFT.EstatusEspecificacion__c == 'Final' && atributosNewFT.Atributo__c == 'Final'){
                idFichaProd.add(atributosNewFT.FichaTecnicaProducto__c);
            }
        }
               
        if(!idFichaProd.isEmpty()){
        	atributosFichaT = [Select TipoAtributo__c,EspecificacionAtributo__c,EspecificacionAtributo__r.TipoEspecificacion__c,EspecificacionAtributo__r.Categoria__c,EstatusEspecificacion__c,CodigoModeloFichaTecnica__c,EspecificacionAtributo__r.name,Atributo__c,NoAplica__c,codeModel__c  from EspecificacionFichaTecnica__c WHERE FichaTecnicaProducto__c IN : idFichaProd];    
        }
        
        if(!atributosFichaT.isEmpty()){
            for(EspecificacionFichaTecnica__c atributosFichaProd : atributosFichaT){
                if(atributosFichaProd.TipoAtributo__c != 'Estatus'){
                    atributosFichaProd.EstatusEspecificacion__c = 'Final';
                    nuevosAtributosUpd.add(atributosFichaProd);
                    
                } 
            }
            
        }    
        if(!nuevosAtributosUpd.isEmpty()){
        	update nuevosAtributosUpd;    
        }
        
    }
    
}