@isTest
private class ProductRelatedPBEControllerTest {
	
	public static Product2 getProducto(){
		RecordType rtParte = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Parte' and IsActive=true];
		Id standardPricebookId = Test.getStandardPricebookId();

		Product2 miProd = new Product2(
			Name= 'PROD1',
			IdExternoProducto__c= 'PROD1',
			ProductCode = 'P001',
			Description= 'Test product', 
			PartesRobadas__c = true,
			RecordTypeId = rtParte.Id,
			IsActive = true
		);

		insert miProd;

        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IdExternoListaPrecios__c='CustomPricebook', 
            isActive=true
        );
        insert customPB;

		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id=standardPricebookId,
			UnitPrice=0.0,
			Product2Id=miProd.Id,			
			IsActive=true,
			IdExterno__c='Test1'
		);
		insert pbeStandard;

        PricebookEntry pbePro2CPB= new PricebookEntry(
            Pricebook2Id= customPB.Id,
            UnitPrice= 0.0,            
            Product2Id= miProd.Id,            
            IsActive=true,
            IdExterno__c = 'expbe1'
        );    
        insert pbePro2CPB;

        return miProd;  
	}
	
	@isTest static void test_method() {
		Product2 miProd = ProductRelatedPBEControllerTest.getProducto();
		ApexPages.StandardController stcCtl = new ApexPages.StandardController(miProd);
		ProductRelatedPBEController ctmCtl = new ProductRelatedPBEController(stcCtl);
		ctmCtl.getSize();
	}
	
}