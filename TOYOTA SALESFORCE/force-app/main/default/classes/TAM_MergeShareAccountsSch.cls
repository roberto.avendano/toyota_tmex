/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar crear la colaboración de las cuentas con el 
    					proceso de Merge Accounts

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    01-Septiembre-2020 	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeShareAccountsSch implements Schedulable{

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeShareAccountsSch.execute...');
		
		this.sQuery = 'Select Id, TAM_Cliente__c, TAM_IdUsuarioShare__c, TAM_Procesado__c, TAM_TipoObjeto__c ';
		this.sQuery += ' From TAM_MergeShareClientes__c ';
		this.sQuery += ' Where TAM_Procesado__c != true';
		this.sQuery += ' Order by TAM_Cliente__c ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeShareAccountsSch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_MergeShareAccountsBch objMergShareAccCls = new TAM_MergeShareAccountsBch(sQuery);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objMergShareAccCls, 100);
			    	 
    }
    
}