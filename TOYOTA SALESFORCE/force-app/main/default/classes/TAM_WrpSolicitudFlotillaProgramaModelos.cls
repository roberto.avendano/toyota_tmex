/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase wrp que sirve para almacenar los datos de las solicitudes cerradas
    					y los modelos para Flotillas y Programas.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    11-Mayo-2020      Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_WrpSolicitudFlotillaProgramaModelos {

    public class wrpVinesExcepcion{
	    @AuraEnabled 
	    public String strVin {get;set;}   

	    //Un constructor por default
		public wrpVinesExcepcion(){
			this.strVin = '';
		}
	    
	    //Un constructor con parametros
		public wrpVinesExcepcion(String strVin){
			this.strVin = strVin;
		}
    }
    
    public class wrpDatosModeloSelecionados{
	    @AuraEnabled 
	    public String strIdSolicitud {get;set;}    	
	    @AuraEnabled 
	    public String strNombre {get;set;}
	    @AuraEnabled 
	    public String strVersion {get;set;}
	    @AuraEnabled
	    public String strDescripcionColorExterior {get;set;}
	    @AuraEnabled
	    public String strDescripcionColorInterior {get;set;}
	    @AuraEnabled
	    public String strCantidadSolicitada {get;set;}    
	    @AuraEnabled
	    public String strIdCatCentrModelos {get;set;}
	    @AuraEnabled
    	public String strTipoVenta {get;set;}
    	@AuraEnabled 
   	 	public String strIdExterno {get;set;}
    	@AuraEnabled 
   	 	public Boolean bolMismoDistribuidor {get;set;}
    	@AuraEnabled 
   	 	public Boolean bolCreaExcepcion {get;set;}
    	@AuraEnabled
    	public String strRtTipoSolicitud {get;set;}   	 	
    }

    @AuraEnabled 
    public String strNombreCorto {get;set;}
    @AuraEnabled 
    public String strDatosSolicitd {get;set;}
    @AuraEnabled 
    public String strFolioSolicitd {get;set;}
    @AuraEnabled 
    public String strFechaCierre {get;set;}
    @AuraEnabled
    public String strNombreCliente {get;set;}
    @AuraEnabled
    public String strTipoSolicitud {get;set;}
    @AuraEnabled
    public String strRangoPrograma {get;set;}
    @AuraEnabled
    public String strPropietario {get;set;}
    @AuraEnabled
    public String strFechaVigencia {get;set;}
    
    @AuraEnabled
    public List<wrpVinesExcepcion> lVinesSeleccionados {get;set;}
    @AuraEnabled
    public List<wrpDatosModeloSelecionados> lModelosSeleccionados {get;set;}
    @AuraEnabled
    public List<wrpDatosModeloSelecionados> lModelosSeleccionadosVin {get;set;}

	//Un constructor por default
	public TAM_WrpSolicitudFlotillaProgramaModelos(){
		this.strNombreCorto = '';
		this.strDatosSolicitd = '';
	    this.strFolioSolicitd = '';
	    this.strFechaCierre = '';
	    this.strNombreCliente = '';
	    this.strTipoSolicitud = '';
	    this.strRangoPrograma = '';
	    this.strPropietario = '';
	    this.strFechaVigencia = '';
	    this.lModelosSeleccionados = new List<wrpDatosModeloSelecionados>(); 
	    this.lModelosSeleccionadosVin = new List<wrpDatosModeloSelecionados>(); 	    
	}

	//Un constructor por default
	public TAM_WrpSolicitudFlotillaProgramaModelos(String strNombreCorto, String strDatosSolicitd, 
		String strFolioSolicitd, String strFechaCierre, String strNombreCliente, String strTipoSolicitud, 
		String strRangoPrograma, String strPropietario, String strFechaVigencia, 
		List<wrpDatosModeloSelecionados> lModelosSeleccionados){
		this.strNombreCorto = strNombreCorto;
	    this.strDatosSolicitd = strDatosSolicitd;			
	    this.strFolioSolicitd = strFolioSolicitd;
	    this.strFechaCierre = strFechaCierre;
	    this.strNombreCliente = strNombreCliente;
	    this.strTipoSolicitud = strTipoSolicitud;
	    this.strRangoPrograma = strRangoPrograma;
	    this.strPropietario = strPropietario;
	    this.strFechaVigencia = strFechaVigencia;
	    this.lModelosSeleccionados = lModelosSeleccionados; 
	}
    
}