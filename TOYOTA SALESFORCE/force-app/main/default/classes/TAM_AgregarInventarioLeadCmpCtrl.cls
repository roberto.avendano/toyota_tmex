/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladira para el componente TAM_AgregarInventario

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    10-Julio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class TAM_AgregarInventarioLeadCmpCtrl {

	public class wrpInventarioFyG{
	    @AuraEnabled public TAM_InventarioVehiculosToyota__c inventario {get;set;}
	    @AuraEnabled public TAM_LeadInventarios__c LeadInventario {get;set;}
	    @AuraEnabled public String sIdVin {get;set;}
	    @AuraEnabled public String sVin {get;set;}
	    @AuraEnabled public Boolean sApartado {get;set;}
        @AuraEnabled public String sVersion {get;set;}
	    @AuraEnabled public String sModelo {get;set;}
	    @AuraEnabled public String sAnio {get;set;}
	    @AuraEnabled public String sColExt {get;set;}
	    @AuraEnabled public String sColInt {get;set;}
	    @AuraEnabled public String sDias {get;set;}
	    @AuraEnabled public String sTipo {get;set;}
	    @AuraEnabled public Boolean bEliminar {get;set;}
	    @AuraEnabled public Boolean bCerrada {get;set;}
	    @AuraEnabled public String sSerie {get;set;}
        @AuraEnabled public Boolean blnExisteDD  {get;set;}
        @AuraEnabled public Boolean blnExisteDDNoVta  {get;set;}
        @AuraEnabled public Boolean blnExisteDDMismoDealer  {get;set;}
        @AuraEnabled public Boolean blnExisteDDLeadInventDifDealer  {get;set;}
        @AuraEnabled public String sCodDistFinal {get;set;}
        @AuraEnabled public String sCodColExt {get;set;}
        @AuraEnabled public String sCodColInt {get;set;}
        @AuraEnabled public Datetime dtFechaVenta {get;set;}
        @AuraEnabled public String strNombreCteDD {get;set;}
        @AuraEnabled public String strNombreUserActual {get;set;}
		
		public wrpInventarioFyG(){
			this.inventario = new TAM_InventarioVehiculosToyota__c();
			this.LeadInventario = new TAM_LeadInventarios__c();
			this.sVersion = '';
		}
		
		public wrpInventarioFyG(TAM_InventarioVehiculosToyota__c inventario, String sVersion, Boolean bEliminar){
			this.inventario = inventario;
			this.sVersion = sVersion;			
			this.bEliminar = bEliminar;
		}
	}

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<wrpInventarioFyG> getLeadInventario(String recordId) {
    	System.debug('EN getLeadInventario recordId: ' + recordId);
		List<wrpInventarioFyG> lWrpInventarioFyG = new List<wrpInventarioFyG>();
		Set<String> setInvExist = new Set<String>();
        Set<String> setIdExterno = new Set<String>();
        
        //Consulta los datos de los vines que ya estan cargados
        for (TAM_LeadInventarios__c objLeadInventarios : [select id, Name,
        	TAM_Prospecto__r.TAM_ValidaDatosFactura__c, TAM_Prospecto__r.TAM_CuentaInventarioDisponible__c,
    		TAM_InventarioVehiculosFyG__r.Model_Number__c, TAM_InventarioVehiculosFyG__r.Model_Year__c
        	From TAM_LeadInventarios__c	Where TAM_Prospecto__c = :recordId]){
        	//Crea la llave externa del modelo
        	String sCodigoModelo = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Number__c + '' + objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Year__c;
        	//Agrega sCodigoModelo a setIdExterno
        	setIdExterno.add(sCodigoModelo);
        }
    	System.debug('ENTRO A consultaDatosVines setIdExterno: ' + setIdExterno);
        
        Map<String, String> mapIdExternoSerie = new Map<String, String>();
    	//Consulta los datos del catalogo de 
    	for (TAM_CatalogoCodigosModelo__c objCatCodMod : [Select t.TAM_Descripcion__c, t.TAM_Codigo__c, t.TAM_Activo__c, t.Name 
    		From TAM_CatalogoCodigosModelo__c t Where TAM_Codigo__c IN :setIdExterno ]){
    		//Inicializa objCatalogoCodigosModelo
    		mapIdExternoSerie.put(objCatCodMod.TAM_Codigo__c, objCatCodMod.TAM_Descripcion__c);
    	}
    	System.debug('ENTRO A consultaDatosVines mapIdExternoSerie: ' + mapIdExternoSerie);

		//Consulta los datos de los vines asociadoas
		//List<TAM_LeadInventarios__c> lLeadInvent = new List<TAM_LeadInventarios__c>();	
		for (TAM_LeadInventarios__c objLeadInvet : [Select id, Name, TAM_Prospecto__r.TAM_ValidaDatosFactura__c,	
			TAM_AnioModelo__c, TAM_CodigoModelo__c, TAM_Serie__c, TAM_Version__c, TAM_Apartado__c, 
			TAM_ColorExterior__c, TAM_ColorInterior__c, TAM_DiasPiso__c, TAM_Inventario__c
			From TAM_LeadInventarios__c t Where TAM_Prospecto__c = :recordId ]){
        	String sCodigoModelo = objLeadInvet.TAM_CodigoModelo__c + '' + objLeadInvet.TAM_AnioModelo__c;
        	//String sCodigoModelo = objLeadInvet.TAM_InventarioVehiculosFyG__r.Model_Number__c + '' + objLeadInvet.TAM_InventarioVehiculosFyG__r.Model_Year__c;        		
	    	//Ya tienes los datos para el objeto del tipo wrpInventarioFyG
    		//wrpInventarioFyG objWrpInventarioFyG = new wrpInventarioFyG(objLeadInvet.TAM_InventarioVehiculosFyG__r, 
    		//mapIdExternoSerie.get(sCodigoModelo), false);
    		wrpInventarioFyG objWrpInventarioFyG = new wrpInventarioFyG(new TAM_InventarioVehiculosToyota__c(), 
    		mapIdExternoSerie.get(sCodigoModelo), false);
    		//Actualiza el resto de los campos
    		objWrpInventarioFyG.LeadInventario = objLeadInvet;    		
    		objWrpInventarioFyG.sIdVin = objLeadInvet.id;
    		objWrpInventarioFyG.sVin = objLeadInvet.Name;
    		objWrpInventarioFyG.sApartado = objLeadInvet.TAM_Apartado__c;
            objWrpInventarioFyG.sSerie = objLeadInvet.TAM_Serie__c;
    		objWrpInventarioFyG.sVersion = objLeadInvet.TAM_Version__c;
    		objWrpInventarioFyG.sModelo = String.valueOf(objLeadInvet.TAM_CodigoModelo__c);
    		objWrpInventarioFyG.sAnio = objLeadInvet.TAM_AnioModelo__c;
    		objWrpInventarioFyG.sColExt = objLeadInvet.TAM_ColorExterior__c;
    		objWrpInventarioFyG.sColInt = objLeadInvet.TAM_ColorInterior__c;
    		objWrpInventarioFyG.sDias = objLeadInvet.TAM_DiasPiso__c;
    		objWrpInventarioFyG.sTipo = objLeadInvet.TAM_Inventario__c; 
    		objWrpInventarioFyG.bCerrada = objLeadInvet.TAM_Prospecto__r.TAM_ValidaDatosFactura__c;
    		    		
    		//Agregalo a la lista de lWrpInventarioFyG	
    		lWrpInventarioFyG.add(objWrpInventarioFyG);
	    	System.debug('ENTRO A consultaDatosVines objWrpInventarioFyG: ' + objWrpInventarioFyG);
    		
    		//Agrega el VIN A setInvExist
    		//setInvExist.add(objLeadInvet.TAM_InventarioVehiculosFyG__c);
    		setInvExist.add(objLeadInvet.Name);
		}
				
		System.debug('EN getTipoVentaDestinoFact lWrpInventarioFyG: ' + lWrpInventarioFyG);
        return lWrpInventarioFyG;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<TAM_LeadInventarios__c> actualizaInventario(String recordId, String strIdledInvNewId) {
    	System.debug('EN actualizaInventario recordId: ' + recordId + ' strIdledInvNewId: ' + strIdledInvNewId);
		List<TAM_LeadInventarios__c> lLeadInvent = new List<TAM_LeadInventarios__c>();	

		String sVinInventario = '';
		Boolean sApartado = false;
        String sSerie = '';
		String sVersion = '';
		String sCodigoModelo = '';
		String sAnioModelo = '';
		String sColorExterior = '';
		String sColorInterior = '';
		String sDiasPiso = '';
		String sTipoInventario = '';
		String sInventario = '';
        String sCodColExt = '';
        String sCodColInt = '';

		//Consulta el VIN del inventario TAM_LeadInventarios__c
		for (TAM_LeadInventarios__c objLeadInv : [Select id, TAM_InventarioVehiculosFyG__r.Name,
        	TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c, TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c, 
    		TAM_InventarioVehiculosFyG__r.Model_Number__c, TAM_InventarioVehiculosFyG__r.Model_Year__c, 
    		TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c, 
    		TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c,
    		TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c, 
    		TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c,
    		TAM_InventarioVehiculosFyG__r.Tipo_Inventario__c, TAM_InventarioVehiculosFyG__r.TAM_Version__c,
    		TAM_InventarioVehiculosFyG__r.TAM_TipoInventarioLetra__c, TAM_Prospecto__r.TAM_ValidaDatosFactura__c,
            TAM_InventarioVehiculosFyG__r.TAM_Apartado__c        
			From TAM_LeadInventarios__c	Where id =:strIdledInvNewId]){
			sVinInventario = objLeadInv.TAM_InventarioVehiculosFyG__r.Name;
            sApartado = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_Apartado__c;    
			sSerie = objLeadInv.TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c;
			sVersion = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_Version__c;
			sCodigoModelo = objLeadInv.TAM_InventarioVehiculosFyG__r.Model_Number__c;
			sAnioModelo = objLeadInv.TAM_InventarioVehiculosFyG__r.Model_Year__c;
			sColorExterior = objLeadInv.TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c;
			sColorInterior = objLeadInv.TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c;
			sDiasPiso = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c;
			sTipoInventario = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_TipoInventarioLetra__c;
			sCodColExt = objLeadInv.TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c;
			sCodColInt = objLeadInv.TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c;
		}
    	System.debug('EN actualizaInventario sVinInventario: ' + sVinInventario);		
		
		String sIdExterno = recordId + '-' + sVinInventario;
    	System.debug('EN actualizaInventario sIdExterno: ' + sIdExterno);
		//Actualiza el nuevo reg con recordId
		TAM_LeadInventarios__c objLedInvUld = new TAM_LeadInventarios__c(id = strIdledInvNewId,	TAM_Prospecto__c = recordId, 
			Name = sVinInventario, TAM_Idexterno__c = sIdExterno, TAM_AnioModelo__c = sAnioModelo, 
			TAM_CodigoModelo__c = sCodigoModelo, TAM_Serie__c = sSerie, TAM_Version__c = sVersion, 
			TAM_ColorExterior__c = sColorExterior, TAM_ColorInterior__c = sColorInterior, 
			TAM_DiasPiso__c = sDiasPiso, TAM_Inventario__c = sTipoInventario,
            TAM_Apartado__c = true, TAM_CodigoColorExterior__c = sCodColExt,
            TAM_CodigoColorInterior__c = sCodColInt
		);
    	System.debug('EN actualizaInventario objLedInvUld: ' + objLedInvUld);

		//Actualiza objLedInvUld
		update objLedInvUld;

		//Consulta los reg en donde el candidato seas igual a recordId
		for (TAM_LeadInventarios__c objLeadInvet : [Select t.TAM_Prospecto__c, t.TAM_InventarioVehiculosFyG__c 
			From TAM_LeadInventarios__c t Where TAM_Prospecto__c = :recordId ]){
			lLeadInvent.add(objLeadInvet);
		}
		System.debug('EN actualizaInventario lLeadInvent: ' + lLeadInvent);

		//Regresa la lista de lLeadInvent
        return lLeadInvent;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<wrpInventarioFyG> updateLeadInventario(String recordId, String strIdledInvNewId) {
    	System.debug('EN updateLeadInventario recordId: ' + recordId + ' strIdledInvNewId: ' + strIdledInvNewId);
		List<wrpInventarioFyG> lWrpInventarioFyG = new List<wrpInventarioFyG>();
		TAM_InventarioVehiculosToyota__c objInvApdo;
		
		String sVinInventario = '';
        Boolean sApartado = false;
		String sSerie = '';
		String sVersion = '';
		String sCodigoModelo = '';
		String sAnioModelo = '';
		String sColorExterior = '';
		String sColorInterior = '';
		String sDiasPiso = '';
		String sTipoInventario = '';
		String sInventario = '';
        String sCodigoDistribuidor = '';
        String sCodColExt = '';
        String sCodColInt = '';

        Account objDist = new Account();
        
        for (User usuarioAct : [Select id,CodigoDistribuidor__c FROM User where id =:UserInfo.getUserId()]){
            sCodigoDistribuidor = usuarioAct.CodigoDistribuidor__c != null ? usuarioAct.CodigoDistribuidor__c : '57000';
        }
        System.debug('En updateLeadInventario sCodigoDistribuidor: ' + sCodigoDistribuidor);
		
		for (Account dist : [Select a.TAM_NoDiasApartado__c From Account a
		  Where a.Codigo_Distribuidor__c  =:sCodigoDistribuidor ]){
            objDist = dist;		    
		}
        System.debug('En updateLeadInventario objDist: ' + objDist);
		
		//Consulta el VIN del inventario TAM_LeadInventarios__c
		for (TAM_LeadInventarios__c objLeadInv : [Select id, 
			TAM_InventarioVehiculosFyG__c, TAM_InventarioVehiculosFyG__r.Name, 
        	TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c, TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c, 
    		TAM_InventarioVehiculosFyG__r.Model_Number__c, TAM_InventarioVehiculosFyG__r.Model_Year__c,    		 
    		TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c, 
            TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c,
    		TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c,
            TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c,
    		TAM_InventarioVehiculosFyG__r.Tipo_Inventario__c, TAM_InventarioVehiculosFyG__r.TAM_Version__c,
    		TAM_InventarioVehiculosFyG__r.TAM_TipoInventarioLetra__c, TAM_Prospecto__r.TAM_ValidaDatosFactura__c,
            TAM_InventarioVehiculosFyG__r.TAM_Apartado__c
 			From TAM_LeadInventarios__c	Where id =:strIdledInvNewId]){
			sVinInventario = objLeadInv.TAM_InventarioVehiculosFyG__r.Name;
            sApartado = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_Apartado__c;
			sSerie = objLeadInv.TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c;
			sVersion = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_Version__c;
			sCodigoModelo = objLeadInv.TAM_InventarioVehiculosFyG__r.Model_Number__c;
			sAnioModelo = objLeadInv.TAM_InventarioVehiculosFyG__r.Model_Year__c;
			sColorExterior = objLeadInv.TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c;
			sColorInterior = objLeadInv.TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c;
			sDiasPiso = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c;
			sTipoInventario = objLeadInv.TAM_InventarioVehiculosFyG__r.TAM_TipoInventarioLetra__c;
            sCodColExt = objLeadInv.TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c;
            sCodColInt = objLeadInv.TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c;
		}
    	System.debug('EN updateLeadInventario sVinInventario: ' + sVinInventario);		
		
		String sIdExterno = recordId + '-' + sVinInventario;
    	System.debug('EN actualizaInventario sIdExterno: ' + sIdExterno);		
		//Actualiza el nuevo reg con recordId
		TAM_LeadInventarios__c objLedInvUld = new TAM_LeadInventarios__c(id = strIdledInvNewId,	TAM_Prospecto__c = recordId, 
			Name = sVinInventario, TAM_Idexterno__c = sIdExterno, TAM_AnioModelo__c = sAnioModelo, 
			TAM_CodigoModelo__c = sCodigoModelo, TAM_Serie__c = sSerie, TAM_Version__c = sVersion, 
			TAM_ColorExterior__c = sColorExterior, TAM_ColorInterior__c = sColorInterior, 
			TAM_DiasPiso__c = sDiasPiso, TAM_Inventario__c = sTipoInventario,
			TAM_CodigoColorExterior__c = sCodColExt,
            TAM_CodigoColorInterior__c = sCodColInt
		);
		
		//Si es inventario en G entonces pon la fecha de fin de apartado de acuerdo al campo de
		//TAM_FechaFinApartado__c del distribuidor 
		if (objLedInvUld.TAM_Inventario__c == 'G' || Test.isRunningTest()){
		    objLedInvUld.TAM_Apartado__c = true;
		    //Ve si tiene algo el campo de TAM_NoDiasApartado__c en el objeto de objDist
		    if (objDist.TAM_NoDiasApartado__c != null && objDist.TAM_NoDiasApartado__c != ''){
		      if (Integer.valueOf(objDist.TAM_NoDiasApartado__c) > 0){
		          Date dtFechaAct = Date.today();
		          Date dtFechaFinApdo = dtFechaAct.addDays(Integer.valueOf(objDist.TAM_NoDiasApartado__c)); 
		          objLedInvUld.TAM_FechaFinApartado__c = dtFechaFinApdo;
		          //Consulta el inventario en el objeto de TAM_InventarioVehiculosToyota__c 
		          for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
		              Where Name = :sVinInventario]){
		              //Si existe apartalo objInv
		              objInv.TAM_Apartado__c = true; 
		              objInvApdo = new TAM_InventarioVehiculosToyota__c(id = objInv.id, 
		              TAM_Apartado__c = true);
		          }
		      }//Fin si Integer.valueOf(objDist.TAM_NoDiasApartado__c) > 0
		    }//Fin si objDist.TAM_NoDiasApartado__c != null && objDist.TAM_NoDiasApartado__c != ''
		}//Fin si objLedInvUld.TAM_Inventario__c == 'G'
    	System.debug('EN updateLeadInventario objLedInvUld: ' + objLedInvUld);		
        System.debug('EN updateLeadInventario objInvApdo: ' + objInvApdo);      
		
		//Actualiza objLedInvUld
		update objLedInvUld;
		
		//Si se pudo apartar
		if (objInvApdo != null)
		  update objInvApdo;
		
		//Actualiza la lista de lWrpInventarioFyG				
		lWrpInventarioFyG = getLeadInventario(recordId);				
		System.debug('EN updateLeadInventario lWrpInventarioFyG: ' + lWrpInventarioFyG);

		//Regresa la lista de lLeadInvent
        return lWrpInventarioFyG;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<wrpInventarioFyG> deleteLeadInventario(String recordId, String strIdledInvNewId) {
    	System.debug('EN deleteLeadInventario recordId: ' + recordId + ' strIdledInvNewId: ' + strIdledInvNewId);
		List<wrpInventarioFyG> lWrpInventarioFyG = new List<wrpInventarioFyG>();
		TAM_InventarioVehiculosToyota__c objInvApdo;
		String sVinInventario = '';
		
		//Actualiza el nuevo reg con recordId
		TAM_LeadInventarios__c objLedInvUld = new TAM_LeadInventarios__c(id = strIdledInvNewId, 
		TAM_Prospecto__c = recordId);
		//Actualiza objLedInvUld
		delete objLedInvUld;
        
        for (TAM_LeadInventarios__c objLedInv : [Select id, Name From TAM_LeadInventarios__c Where id =:strIdledInvNewId ]){
            sVinInventario = objLedInv.Name;
        }
        System.debug('EN deleteLeadInventario sVinInventario: ' + sVinInventario);
        
        //Si esta apartado en el objeto de TAM_InventarioVehiculosToyota__c liberalo
        for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
            Where Name = :sVinInventario]){
            objInvApdo = new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false);
        }
        System.debug('EN deleteLeadInventario objInvApdo: ' + objInvApdo);
        
        //Si se pudo apartar
        if (objInvApdo != null)
          update objInvApdo;

		//Actualiza la lista de lWrpInventarioFyG				
		lWrpInventarioFyG = getLeadInventario(recordId);				
		System.debug('EN deleteLeadInventario lWrpInventarioFyG: ' + lWrpInventarioFyG);

		//Regresa la lista de lLeadInvent
        return lWrpInventarioFyG;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<wrpInventarioFyG> deleteLeadInventarioSel(String recordId, List<wrpInventarioFyG> lVinesSeleProspFactDel) {
    	System.debug('EN deleteLeadInventarioSel recordId: ' + recordId);
    	System.debug('EN deleteLeadInventarioSel lVinesSeleProspFactDel: ' + lVinesSeleProspFactDel);
		
		List<wrpInventarioFyG> lWrpInventarioFyG = new List<wrpInventarioFyG>();
		List<TAM_LeadInventarios__c> lLeadInventariosDel = new List<TAM_LeadInventarios__c>();
		Set<String> setVinInvDel = new Set<String>();
		List<TAM_InventarioVehiculosToyota__c> lInvVehUpd = new List<TAM_InventarioVehiculosToyota__c>();
		
		//Recorre la lista de vines selecionados para ser eliminados
		for (wrpInventarioFyG objWrpInventarioFyG : lVinesSeleProspFactDel){
			//Aregalo a la lista para ser eliminado
			lLeadInventariosDel.add(new TAM_LeadInventarios__c(
					id = objWrpInventarioFyG.sIdVin
				)
			);
			if (objWrpInventarioFyG.sVin != null)
			 setVinInvDel.add(objWrpInventarioFyG.sVin);
		}//Fin del for para lVinesSeleProspFactDel
        System.debug('EN deleteLeadInventario setVinInvDel: ' + setVinInvDel);
		
		//Elimina la lista de lLeadInventariosDel
		if (!Test.isRunningTest())
			delete lLeadInventariosDel;

        //Si esta apartado en el objeto de TAM_InventarioVehiculosToyota__c liberalo
        for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
            Where Name IN :setVinInvDel]){
            lInvVehUpd.add(new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false));
        }
        System.debug('EN deleteLeadInventario lInvVehUpd: ' + lInvVehUpd);
        
        //Si se pudo apartar
        if (!lInvVehUpd.isEmpty())
            update lInvVehUpd;
		
		//Actualiza la lista de lWrpInventarioFyG				
		lWrpInventarioFyG = getLeadInventario(recordId);

		//Regresa la lista de lLeadInvent
        return lWrpInventarioFyG;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion consultaLeadInventario(String recordId, String strIdledInvNewId) {
    	System.debug('EN consultaLeadInventario recordId: ' + recordId + ' strIdledInvNewId: ' + strIdledInvNewId);
		String sInventarioVehiculosFyG = '';
		String sVinInventario = '';
		TAM_LeadInventarios__c objLeadInventariosDel;
		
        TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
        objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se actualizarón con exito.');		
		
		//Consulta los reg en donde el candidato seas igual a recordId
		for (TAM_LeadInventarios__c objLeadInvet : [Select t.TAM_Prospecto__c, t.TAM_InventarioVehiculosFyG__c,
			t.TAM_InventarioVehiculosFyG__r.Name  
			From TAM_LeadInventarios__c t Where id = :strIdledInvNewId ]){
			sInventarioVehiculosFyG = objLeadInvet.TAM_InventarioVehiculosFyG__c;
			sVinInventario = objLeadInvet.TAM_InventarioVehiculosFyG__r.Name;
		}
		System.debug('EN consultaLeadInventario sInventarioVehiculosFyG: ' + sInventarioVehiculosFyG + ' sVinInventario: ' + sVinInventario);

        //Inicializa un SavePoint para hacer el rollback en caso de error
        SavePoint svUpsDatos = Database.setSavepoint();

		//Ya tienes el vin que se va a agregrar a la lista de inventario ve si ya existe en otro candidato
		if (sVinInventario != null && sVinInventario != ''){
		    //Ve si el incventario ya esta en otro dis o usuario
		    TAM_ConsultaVinDD.wrpInventDifDealer objWrpInventDifDealer = TAM_ConsultaVinDD.consultaVinInvLead(sVinInventario, recordId);
		    //Ve si ya existe el VIN asocuad a otro lead o al mismo
		    if (objWrpInventDifDealer.blnExisteVin){
                //Ya existe ese inventario asociado a otro lead asi que despliega el error
                if (objWrpInventDifDealer.blnMismoProspecto && objWrpInventDifDealer.blnMismoProp)
                    objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'El Vin que estas seleccionando para este prospecto.');  
                if (objWrpInventDifDealer.blnMismoProspecto && !objWrpInventDifDealer.blnMismoProp)
                    objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'El Vin que estas seleccionando ya esta ocupado por otro usuario.');  
                //Elimina el reg que se dio de alta
                objLeadInventariosDel = new TAM_LeadInventarios__c(id = strIdledInvNewId);		        
		    }//Fin si objWrpInventDifDealer.blnExisteVin
            System.debug('EN consultaLeadInventario objLeadInventariosDel: ' + objLeadInventariosDel);
			//Tiene algo el objeto de objLeadInventariosDel
			if (objLeadInventariosDel != null)
				if (!Test.isRunningTest())
					delete objLeadInventariosDel;
		}//Fin si sVinInventario != null && sVinInventario != ''
        
        //Un rollback para todo solo estamos en pruebas
        //Database.rollback(svUpsDatos);
        
    	System.debug('EN consultaLeadInventario objRespoPaso: ' + objRespoPaso);
		//Regresa la lista de lLeadInvent
        return objRespoPaso;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static wrpInventarioFyG buscaVinDD(String recordId, String sstrVinBusqPrm) {
        System.debug('EN buscaVinDD recordId: ' + recordId + ' sstrVinBusqPrm: ' + sstrVinBusqPrm);
        String sInventarioVehiculosFyG = '';
        String sVinInventario = '';
        TAM_LeadInventarios__c objLeadInventariosDel;
        TAM_LeadInventarios__c objLedInvUld;
        Lead CandUpd = new Lead();
        Boolean blnExisteOtroDealer = false;
        String sCodDist = '';
        
        //Consta el Cod del Dist del lead
        for (Lead objProsp : [Select id, TAM_CodDistribuidorUsr__c From Lead Where ID =: recordId]){
            sCodDist = objProsp.TAM_CodDistribuidorUsr__c != null ? objProsp.TAM_CodDistribuidorUsr__c : '57000';
        }
        System.debug('EN buscaVinDD sCodDist: ' + sCodDist);
        
        wrpInventarioFyG objWrpInventarioFyG = new wrpInventarioFyG(new TAM_InventarioVehiculosToyota__c(), null, false);
        objWrpInventarioFyG.blnExisteDD = false; 
        objWrpInventarioFyG.blnExisteDDNoVta = false;
        objWrpInventarioFyG.blnExisteDDMismoDealer = false;
        objWrpInventarioFyG.blnExisteDDLeadInventDifDealer = false;

        Map<String, String> mapIdExternoSerie = new Map<String, String>();
        //Consulta los datos del catalogo de 
        for (TAM_CatalogoCodigosModelo__c objCatCodMod : [Select t.TAM_Descripcion__c, t.TAM_Codigo__c, t.TAM_Activo__c, t.Name 
            From TAM_CatalogoCodigosModelo__c t]){
            //Inicializa objCatalogoCodigosModelo
            mapIdExternoSerie.put(objCatCodMod.TAM_Codigo__c, objCatCodMod.TAM_Descripcion__c);
        }
        System.debug('ENTRO A buscaVinDD mapIdExternoSerie: ' + mapIdExternoSerie);

        //Consulta los datos del vehiculo       
        for (Vehiculo__c objVehic : [Select id, Name, SerieAD__c, TAM_Version__c, TAM_Codigo_Modelo_Producto__c,
            AnioModel__c, ColorExternoVehiculo__r.Name, ColorInternoVehiculo__r.Name, Estatus__c, CodigoDistribuidor__c,
            Codigo_color_exterior__c, Codigo_color_interior__c, TAM_Fecha_Envio_Ultimo_Mov__c, 
            Ultimo_Movimiento__r.TAM_NombrecCompletoFrm__c    
            From Vehiculo__c t Where Name = :sstrVinBusqPrm //And Estatus__c = 'RDR'
            ]){
            System.debug('ENTRO A buscaVinDD objVehic: ' + objVehic);
                        
            String sCodigoModelo = objVehic.TAM_Codigo_Modelo_Producto__c + '' + objVehic.AnioModel__c;
            //Ve si el ultimo mov es una venta RDR
            if ( (objVehic.Estatus__c == 'RDR' && objVehic.CodigoDistribuidor__c == sCodDist) || Test.isRunningTest()){
	            objWrpInventarioFyG = new wrpInventarioFyG(new TAM_InventarioVehiculosToyota__c(), 
	            mapIdExternoSerie.get(sCodigoModelo), false);
	            //Actualiza el resto de los campos
	            objWrpInventarioFyG.LeadInventario = null;          
	            objWrpInventarioFyG.sIdVin = '';
	            objWrpInventarioFyG.sVin = sstrVinBusqPrm;
	            objWrpInventarioFyG.sApartado = false;
	            objWrpInventarioFyG.sSerie = mapIdExternoSerie.containsKey(sCodigoModelo) ? mapIdExternoSerie.get(sCodigoModelo) : objVehic.SerieAD__c;
	            objWrpInventarioFyG.sVersion = objVehic.TAM_Version__c;
	            objWrpInventarioFyG.sModelo = String.valueOf(objVehic.TAM_Codigo_Modelo_Producto__c);
	            objWrpInventarioFyG.sAnio = objVehic.AnioModel__c;
	            objWrpInventarioFyG.sColExt = objVehic.ColorExternoVehiculo__r.Name;
	            objWrpInventarioFyG.sColInt = objVehic.ColorInternoVehiculo__r.Name;
                objWrpInventarioFyG.sCodColExt = objVehic.Codigo_color_exterior__c;
                objWrpInventarioFyG.sCodColInt = objVehic.Codigo_color_interior__c;
	            objWrpInventarioFyG.sDias = '';
	            objWrpInventarioFyG.sTipo = 'F'; 
	            objWrpInventarioFyG.bCerrada = false;
	            objWrpInventarioFyG.dtFechaVenta = objVehic.TAM_Fecha_Envio_Ultimo_Mov__c;
	            objWrpInventarioFyG.strNombreCteDD = objVehic.Ultimo_Movimiento__r.TAM_NombrecCompletoFrm__c;
	            
                //Inicializa el campo de blnExisteDD
                objWrpInventarioFyG.blnExisteDD = true; 	            
                objWrpInventarioFyG.blnExisteDDNoVta = true;
                objWrpInventarioFyG.blnExisteDDMismoDealer = true;
                objWrpInventarioFyG.blnExisteDDLeadInventDifDealer = false;
            }
            if (objVehic.Estatus__c == 'RDR' && objVehic.CodigoDistribuidor__c != sCodDist){
                objWrpInventarioFyG.blnExisteDDNoVta = true;
                objWrpInventarioFyG.blnExisteDDMismoDealer = false;
            }//Fin si //Fin si objVehic.Estatus__c == 'RDR' && objVehic.CodigoDistribuidor__c == sCodDist 
            if (objVehic.Estatus__c != 'RDR'){
                objWrpInventarioFyG.blnExisteDDNoVta = false;
                objWrpInventarioFyG.blnExisteDDMismoDealer = false;
            }//Fin si //Fin si objVehic.Estatus__c == 'RDR' && objVehic.CodigoDistribuidor__c == sCodDist 
           
        }//Fin del for para la consulta de Vehiculo__c
        System.debug('ENTRO A buscaVinDD objWrpInventarioFyG: ' + objWrpInventarioFyG);

        //Inicializa un SavePoint para hacer el rollback en caso de error
        SavePoint svUpsDatos = Database.setSavepoint();
        
        //Ve si no existe en objWrpInventarioFyG.blnExisteDD = true
        if ( (objWrpInventarioFyG.blnExisteDD && objWrpInventarioFyG.blnExisteDDNoVta && objWrpInventarioFyG.blnExisteDDMismoDealer) || Test.isRunningTest()){
            //Ve si esta asignado a otro usuarios de este u otro dist
            TAM_ConsultaVinDD.wrpInventDifDealer objWrpInventDifDealer = TAM_ConsultaVinDD.consultaVinDD(sstrVinBusqPrm, sCodDist);
            //Ve si ya existe el sstrVinBusqPrm en otro Dealer
            if (objWrpInventDifDealer.blnExisteDDLeadInventDifDealer){
                objWrpInventarioFyG.blnExisteDDLeadInventDifDealer = true;
                objWrpInventarioFyG.sCodDistFinal = objWrpInventDifDealer.sCodDistFinal;
                objWrpInventarioFyG.strNombreUserActual = objWrpInventDifDealer.sOwnerFinal;
            }//Fin si objWrpInventDifDealer.blnExisteDDLeadInventDifDealer
            if (!objWrpInventDifDealer.blnExisteDDLeadInventDifDealer){
                String sIdExterno = recordId + '-' + sstrVinBusqPrm;
                //Crea el registro del tipo TAM_LeadInventarios__c
		        objLedInvUld = new TAM_LeadInventarios__c(TAM_Prospecto__c = recordId, 
		            Name = sstrVinBusqPrm, TAM_Idexterno__c = sIdExterno, 
		            TAM_AnioModelo__c = objWrpInventarioFyG.sAnio, 
		            TAM_CodigoModelo__c = objWrpInventarioFyG.sModelo, 
		            TAM_Serie__c = objWrpInventarioFyG.sSerie, 
		            TAM_Version__c = objWrpInventarioFyG.sVersion, 
		            TAM_ColorExterior__c = objWrpInventarioFyG.sColExt, 
		            TAM_ColorInterior__c = objWrpInventarioFyG.sColInt, 
		            TAM_DiasPiso__c = '0', TAM_Inventario__c = 'F',
		            TAM_CodigoColorExterior__c = objWrpInventarioFyG.sCodColExt,
		            TAM_CodigoColorInterior__c = objWrpInventarioFyG.sCodColInt,
                    TAM_NombreClienteHistorico__c =  objWrpInventarioFyG.strNombreCteDD
		        );
                System.debug('ENTRO A buscaVinDD objLedInvUld: ' + objLedInvUld);
                //Crea el registro para actualizar el Lead a Entregado
	            CandUpd = new Lead(
	                id = recordId, TAM_PedidoReportado__c = true, 
	                Status = 'Entregado', FWY_Estado_del_candidatoWEB__c = 'Entregado', 
	                TAM_PorcentajeAvance__c = '100%', FWY_Porcentaje_de_avance__c = 100.00,
	                TAM_FechaEntrega__c = objWrpInventarioFyG.dtFechaVenta	            
	            );
                System.debug('ENTRO A buscaVinDD CandUpd: ' + CandUpd);
            }//Fin si !objWrpInventDifDealer.blnExisteDDLeadInventDifDealer
        }//Fin si !objWrpInventarioFyG.blnExisteDD
        
        //Ve si tiene algo el objeto de objLedInvUld
        if (objLedInvUld != null)
            if (objLedInvUld.TAM_Idexterno__c != null)
                insert objLedInvUld;
        
        //Ve si tiene algo el objeto de CandUpd
        if (CandUpd != null)
            if (CandUpd.Id != null)
                update CandUpd;
        
        //Un rollback para todo solo estamos en pruebas
        //Database.rollback(svUpsDatos);
        
        System.debug('EN buscaVinDD objWrpInventarioFyG: ' + objWrpInventarioFyG);
        //Regresa la lista de lLeadInvent
        return objWrpInventarioFyG;
    }
    
    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static String getEstatusLead(String recordId) {
    	System.debug('EN getEstatusLead recordId: ' + recordId);
		String sEstatusLead = '';
		
		//Busca el estado del candidato		
		for (Lead cand : [Select id , Status, FWY_Estado_del_candidatoWEB__c From Lead Where id =:recordId ]){
			sEstatusLead = cand.Status != null ? cand.Status : cand.FWY_Estado_del_candidatoWEB__c;
		}
		System.debug('EN getEstatusLead sEstatusLead: ' + sEstatusLead);

		//Regresa la lista de lLeadInvent
        return sEstatusLead;
    }

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static Boolean getProfileuser() {
        System.debug('EN getProfileuser....');
        boolean blnManager = false;
        
        //Busca el estado del candidato     
        for (User usrAct : [Select u.UserRole.PortalRole, u.UserRoleId From User u Where id =: UserInfo.getUserId()]){
            if (usrAct.UserRole.PortalRole == 'Manager')
                blnManager = true;
        }
        System.debug('EN getProfileuser blnManager: ' + blnManager);

        //Regresa la lista de lLeadInvent
        return blnManager;
    }
    
}