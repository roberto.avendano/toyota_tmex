/**
 *  Descripción General: Clase de prueba para Solicitudes de Auto Demo
 *  ________________________________________________________________
 *      Autor                   Fecha               Descripción
 *  ________________________________________________________________
 *      Cecilia Cruz            27/Agosto/2020      Versión Inicial
 *  ________________________________________________________________
 */
@isTest(SeeAllData=false)
public class TAM_SolicitudAutoDemoTestClass {
    
    @testSetup static void setup() {
        DateTime fechaVenta = DateTime.now();
        Date fechaVenta2 = System.today() +10;
        String month = String.valueOf(fechaVenta.month());
        String year = String.valueOf(fechaVenta.year());
        String month2 = String.valueOf(fechaVenta2.month());
        String year2 = String.valueOf(fechaVenta2.year());
        
        //User
        User usuarioGuadalajara = TAM_TestUtilityClass.crearUsuarioPartnerCommunity('Gerente Guadalajara', '57011');
        
        //Vehiculo
        Vehiculo__c objVehiculo = TAM_TestUtilityClass.crearVIN('3TYAZ5CNXMT006796', '2020');
        Vehiculo__c objVehiculo2 = TAM_TestUtilityClass.crearVIN('4T1C11AK7MU451033', '2020');
        
        //Serie
        Serie__c objSerie = TAM_TestUtilityClass.crearSerie('RAV4');
        
        //Producto
        Product2 objProducto = TAM_TestUtilityClass.crearProducto(objSerie.Id, '4501', '2020', true);
        
        //Movimiento
        Account acc = [SELECT Id FROM Account WHERE Name='Gerente Guadalajara' LIMIT 1];
        Movimiento__c objMovimiento = TAM_TestUtilityClass.crearMovimientos('2020','RDR', '6', fechaVenta, 'N',month, year, objProducto.Id, objVehiculo.Id, acc.Id);
        Movimiento__c objMovimiento2 = TAM_TestUtilityClass.crearMovimientos('2020','RDR', '6', fechaVenta, 'C',month, year, objProducto.Id, objVehiculo2.Id, acc.Id);

        //Factura
        Factura__c objFactura = TAM_TestUtilityClass.crearFactura(objVehiculo.Id, fechaVenta, 491100, acc.Id);
        Factura__c objFactura2 = TAM_TestUtilityClass.crearFactura(objVehiculo2.Id, fechaVenta, 491100, acc.Id);
        
        //AutoDemo
        SolicitudAutoDemo__c objDemo = TAM_TestUtilityClass.crearAutoDemo(objVehiculo.Id, 'Baja por pago completado sin solicitud');
        objDemo.FechaBajaAutoDemoSolicitud__c = system.today();
        objDemo.FechaBajaPagoCompletado__c = system.today();
        objDemo.FechaAltaAutoDemo__c = system.today().addMonths(-1);
        objDemo.VIN__c = objVehiculo.Id;
        objDemo.Distribuidor__c = acc.Id;
        update objDemo;
        
        SolicitudAutoDemo__c objDemo2 = TAM_TestUtilityClass.crearAutoDemo(objVehiculo2.Id, 'Autorización baja TMEX');
        objDemo2.FechaBajaAutoDemoSolicitud__c = system.today().addDays(-100);
        objDemo2.FechaBajaPagoCompletado__c = system.today().addDays(-100);
        objDemo2.FechaAltaAutoDemo__c = system.today().addMonths(-6);
        objDemo2.VIN__c = objVehiculo2.Id;
        objDemo2.Distribuidor__c = acc.Id;
        update objDemo2;
        
        //Inventario en Piso
        TAM_TestUtilityClass.crearInventarioPiso('RAV4', '4501', '2020', 'x', Date.valueOf(fechaVenta), 50, 20);
        
        //Catálogo Centralizado Modelos
        TAM_TestUtilityClass.crearModeloCatalogoCentralizado('RAV4', '4501','2020', 'x');
        
        //PriceBook
        Id standardPricebookId = Test.getStandardPricebookId();
        TAM_TestUtilityClass.crearPricebookEntryCustom(standardPricebookId, objProducto, true, 400000, 10, 40000);
        
        //Política Incentivos
        TAM_PoliticasIncentivos__c objPoliticaIncentivos = TAM_TestUtilityClass.crearPoliticaIncentivos('Retail', 'Politica_01', Date.valueOf(fechaVenta), Date.valueOf(fechaVenta), false, 'Vigente');
        
        //Detalle Politica Incentivos
        TAM_TestUtilityClass.crearDetallePoliticaIncentivos('RAV4', '4501','2020', 'x',10000, 5000, 5000,0,0,0,false, objPoliticaIncentivos.Id,400000); 
        
        //Estado de Cuenta TFS
        TAM_EstadoCuentaTFS__c objTFS = new TAM_EstadoCuentaTFS__c();
        objTFS.TAM_FechaUltimoPagoCapital__c = fechaVenta2;
        objTFS.TAM_InteresesVencidos__c = 1560.88;
		objTFS.Name = '3TYAZ5CNXMT006796';
        insert objTFS;
        
        //Nota de Credito
        TAM_Notas_Credito__c objNotaCreditoInteresesTFS = new TAM_Notas_Credito__c();
        objNotaCreditoInteresesTFS.TAM_MetodoPago__c = 'PUE';
        objNotaCreditoInteresesTFS.TAM_NombreReceptor__c = 'TOYOTA MOTOR SALES DE MEXICO S. DE R.L. DE C.V.';
        objNotaCreditoInteresesTFS.TAM_Codigo_Distribuidor__c = '57011';
        objNotaCreditoInteresesTFS.SubTotal__c = 200;
        objNotaCreditoInteresesTFS.TAM_Folio__c = '123Prueba';
        insert objNotaCreditoInteresesTFS;
        
        //Estado de Cuenta
        TAM_DetalleEstadoCuenta__c objEdoCta =  new TAM_DetalleEstadoCuenta__c();
        objEdoCta.TAM_VIN__c = objVehiculo2.Name;
        insert objEdoCta;
    }
    
    /*Buscador de VIN con Estado de Cuenta TFS*/
    @isTest
    public static void searchAutoDemo(){
		User usuarioGuadalajara = [SELECT Id,CodigoDistribuidor__c FROM User WHERE CodigoDistribuidor__c = '57011'];       
        Vehiculo__c objVehiculo = [SELECT Id, Name FROM Vehiculo__c WHERE Name = '3TYAZ5CNXMT006796'];
        List<TAM_SearchAutoDemo.wrapVIN> lstAux;
        Test.startTest();
            system.runAs(usuarioGuadalajara){
                lstAux = TAM_SearchAutoDemo.vinBDDealerDaily(objVehiculo.Name , usuarioGuadalajara.Id);
            }
            TAM_SearchAutoDemo.wrapVIN objWrapper = lstAux[0];
            String myJSON = JSON.serialize(objWrapper);
            TAM_SolicitudIncentivo_AutoDemo.saveRecord(myJSON);
        Test.stopTest();
        
        System.assertEquals(objVehiculo.Name, lstAux[0].VIN);  
    }
    
    /*Buscador Nota de Crédito*/
    @isTest
    public static void searchNotaCredito(){
        User usuarioGuadalajara = [SELECT Id,CodigoDistribuidor__c FROM User WHERE CodigoDistribuidor__c = '57011'];
        TAM_Notas_Credito__c objNotaCreditoInteresesTFS = [SELECT TAM_Folio__c,SubTotal__c FROM TAM_Notas_Credito__c WHERE TAM_NombreReceptor__c = 'TOYOTA MOTOR SALES DE MEXICO S. DE R.L. DE C.V.'];
        List<TAM_SearchNotaCredito.wrapVIN> lstAux;
        Test.startTest();
            system.runAs(usuarioGuadalajara){
                lstAux = TAM_SearchNotaCredito.vinNotaCredito(objNotaCreditoInteresesTFS.TAM_Folio__c , usuarioGuadalajara.Id);
            }
            TAM_SearchNotaCredito.wrapVIN objWrapper = lstAux[0];
            String myJSON = JSON.serialize(objWrapper);
        Test.stopTest();
        System.assertEquals(objNotaCreditoInteresesTFS.TAM_Folio__c, lstAux[0].folio); 
        
    }
    
    /*Buscador de VIN sin Estado de Cuenta TFS*/
    @isTest
    public static void searchAutoDemo2(){
		User usuarioGuadalajara = [SELECT Id,CodigoDistribuidor__c FROM User WHERE CodigoDistribuidor__c = '57011'];       
        Vehiculo__c objVehiculo2 = [SELECT Id, Name FROM Vehiculo__c WHERE Name = '4T1C11AK7MU451033'];
        List<TAM_SearchAutoDemo.wrapVIN> lstAux;
        Test.startTest();
            system.runAs(usuarioGuadalajara){
                lstAux = TAM_SearchAutoDemo.vinBDDealerDaily(objVehiculo2.Name , usuarioGuadalajara.Id);
            }System.debug('**********' + lstAux);
            TAM_SearchAutoDemo.wrapVIN objWrapper = lstAux[0];
            String myJSON = JSON.serialize(objWrapper);
            TAM_SolicitudIncentivo_AutoDemo.saveRecord(myJSON);
        Test.stopTest();
        System.assertEquals(objVehiculo2.Name, lstAux[0].VIN);  
    }
}