/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción General: Clase Batch para Provisión de Incentivos
________________________________________________________________
Autor               Fecha               Descripción
________________________________________________________________
Cecilia Cruz        30/Junio/2020       Versión Inicial
________________________________________________________________
********************************************************************************/
public class TAM_ProvisionBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    //Variables y constantes.
    static final String STRING_SALESCODE_AUTODEMO_06 = '06';
    static final String STRING_SALESCODE_AUTODEMO_6 = '6';
    static final String STRING_FLEET_C = 'C';
    static final String STRING_RDR = 'RDR';
    static final String STRING_DEALERCODE_57000 = '57000';
    
    public final String recordId;
    public final String strMesProv;
    public final String strAnioProv;
    public final String AllVinesDelMes;
    public final Id RecordTypeId_Retail;
    public final Id RecordTypeId_Excepcion;
    public final Id RecordTypeId_Demos;
    
    public static Map<String, List<Movimiento__c>> mapMovimientosRetail;
    public static Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVtaCorporativa;
    public static Map<String, List<TAM_DetalleEstadoCuenta__c>> mapEdoCuenta;
    public static Map<String, TAM_SolicitudExpecionIncentivo__c> mapExcepcionesRetail;
    public static Map<String, List<TAM_DetallePoliticaJunction__c>> mapIncentivosRetail;
    public static Set<String> setReversas;
    public static Set<String> setAllVinesDelMes;
    public static List<Movimiento__c> lstMovRetail;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaTransferidos;
    public static Map<String, TAM_DetalleProvisionIncentivo__c> mapDetalleProvisionVtaCorp;
    public static Map<String, TAM_ModeloCodigoProducto__c> mapModeloCodigoProd;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaFacturados;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaExcepciones;
    public static List<SolicitudAutoDemo__c> lstDemos;
    public static Map<String, TAM_CalendarioToyota__c> mapCalendarioTOYOTA;
    
    
    //Constructor
    public TAM_ProvisionBatch(String record_Id, String strMesProvision, String strAnioProvision){

        recordId = record_Id;
        strAnioProv = strAnioProvision;
        strMesProv = strMesProvision;
        
        AllVinesDelMes = 'SELECT  VIN__r.Name FROM Movimiento__c WHERE Month__c =: strMesProv AND Year__c =: strAnioProv' +
                         ' AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_06  AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_6' +
                         ' AND Fleet__c !=: STRING_FLEET_C '+
                         ' AND Distribuidor__r.Codigo_Distribuidor__c !=: STRING_DEALERCODE_57000';
        
        //Records Types
        RecordTypeId_Retail = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        RecordTypeId_Excepcion = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Excepcion').getRecordTypeId();
        RecordTypeId_Demos = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('AutoDemo').getRecordTypeId();

        //Auto Demo
        TAM_ProvisionHelperClass_Batch.getSolicitudesDemo(strMesProv, strAnioProv, RecordTypeId_Demos, recordId);
    }
    
    //Start
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(AllVinesDelMes);
    }
    
    //Excecute
    public void execute(Database.BatchableContext BC, List<sObject> scope){

        Set<String> setVIN = new Set<String>();
        if(!scope.isEmpty()){
            for(sObject objScope : scope){
                Movimiento__c objVIN = (Movimiento__c)objScope;
                setVIN.add(objVIN.VIN__r.Name);
            }
        }
        mapCalendarioTOYOTA = TAM_ProvisionHelperClass_Batch.getCalendarioTOYOTA();
        mapEdoCuentaExcepciones = TAM_ProvisionHelperClass_Batch.getEstadoCuentaExcepciones(strAnioProv, strMesProv);
		mapIncentivosRetail = TAM_ProvisionHelperClass_Batch.getIncentivosRetail(strAnioProv, strMesProv);
        mapExcepcionesRetail = TAM_ProvisionHelperClass_Batch.getExcepcionesRetail(strAnioProv, strMesProv, setVIN);
        mapVtaCorporativa = TAM_ProvisionHelperClass_Batch.getVtaCorporativa(setVIN,strAnioProv, strMesProv, mapCalendarioTOYOTA);
        mapEdoCuenta = TAM_ProvisionHelperClass_Batch.getEstadoCuenta(setVIN);
        mapMovimientosRetail = TAM_ProvisionHelperClass_Batch.getMovimientosRetail(setVIN);
        lstMovRetail = TAM_ProvisionHelperClass_Batch.getlistaRetail(setVIN);
        mapEdoCuentaTransferidos = TAM_ProvisionHelperClass_Batch.getEstadoCuentaTransferidos(strAnioProv, strMesProv);
        mapDetalleProvisionVtaCorp = TAM_ProvisionHelperClass_Batch.getDetalleVentaCorp(mapEdoCuentaTransferidos);
        mapModeloCodigoProd = TAM_ProvisionHelperClass_Batch.getModeloCodProd();
        mapEdoCuentaFacturados = TAM_ProvisionHelperClass_Batch.getEstadoCuentaFacturados(strAnioProv, strMesProv);
        lstDemos = TAM_ProvisionHelperClass_Batch.getDemos(setVIN);
       
        //Retail
        setReversas = TAM_ProvisionHelperClass_Batch.setRetailSinReversa(strMesProv, strAnioProv, recordId, RecordTypeId_Retail, 
                                           setVIN, mapMovimientosRetail, mapVtaCorporativa, mapExcepcionesRetail, mapEdoCuenta, 
                                           lstMovRetail,mapIncentivosRetail, mapModeloCodigoProd, lstDemos);
        
        //Reversas
        TAM_ProvisionHelperClass_Batch.setRetailConReversa(strMesProv, strAnioProv, recordId, RecordTypeId_Retail, lstMovRetail, setReversas, mapEdoCuenta, mapModeloCodigoProd,mapIncentivosRetail, mapVtaCorporativa);
        TAM_ProvisionHelperClass_Batch.setReversasRetailPendientes(strMesProv, strAnioProv,  recordId, mapEdoCuentaFacturados);
        
        
        //Excepciones
        TAM_ProvisionHelperClass_Batch.setExcepcionRetail(recordId, mapEdoCuentaExcepciones, lstMovRetail, RecordTypeId_Excepcion, mapModeloCodigoProd);

        //Transferidos
        TAM_ProvisionHelperClass_Batch.setTransferidosParaRetail(strMesProv, strAnioProv, recordId,  mapEdoCuentaTransferidos,  mapDetalleProvisionVtaCorp,  RecordTypeId_Excepcion, mapModeloCodigoProd);
    }
    
    //Finish
    public void finish(Database.BatchableContext BC){}
}