/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar el archivo de ventas.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    28-Junio-2019     Héctor Figueroa             Modificación
******************************************************************************* */

public without sharing class VentasHTriggerHandler extends TriggerHandler{
	
    private List<Venta__c> ventasHList; 
    private Map<Id,Venta__c> mapVentas;
    private Map<String,Map<String,RecordType>> recordTypesMap;
    
    public VentasHTriggerHandler() {
        this.ventasHList = Trigger.new;
        this.mapVentas = (Map<Id,Venta__c>)Trigger.newMap;
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
    }
    
    public override void afterInsert(){
        validateRecords(ventasHList);
        actFechaNotLead(mapVentas);
        eliminaRecords(mapVentas);
        //cancelaVentaEspacial(ventasHList);
    }

    public override void afterUpdate(){
        actFechaNotLead(mapVentas);
    }
    
    private void eliminaRecords(Map<Id,Venta__c> mapVentas){
        Set<String> vines = new Set<String>();
        for(Venta__c v: mapVentas.values()){
            vines.add(v.Name);
        }
        
        if(vines.size()>0){
            List<Venta__c> borrar = new List<Venta__c>();
            for(Venta__c v : [SELECT Id, Name FROM Venta__c WHERE Name IN :vines]){
                if(!mapVentas.containsKey(v.Id)){
                    borrar.add(v);
                }
            }
            if(borrar.size()>0){
                delete borrar;
            }
        }
    }
    
    private void actFechaNotLead(Map<Id,Venta__c> mapVentas){
        Map<String, Venta__c> vines = new Map<String, Venta__c>();
        Map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new Map<String, TAM_LeadInventarios__c>();
        
        for(Venta__c v: mapVentas.values()){
            vines.put(v.Name, v);
        }//Fin del for para los vines
        System.debug('EM VentasHTriggerHandlerTest vines: ' + vines);     
        
        //Busca el VIN en el objeto de LeadInventario
        for (TAM_LeadInventarios__c objLeadInven : [Select id, Name From TAM_LeadInventarios__c
            Where Name IN :vines.KeySet() And TAM_FechaNotificacion__c = null]){
            //Toma la fecha de envio y sumale unos 180 dias
            Date dtFechNot = vines.get(objLeadInven.Name).SubmittedDate__c.Date();
            Date dtFechaNoyFinal = dtFechNot.addDays(180);
            //Crea el objeto del tipo TAM_LeadInventarios__c y metelo al mapa de mapLeadInvUpd
            mapLeadInvUpd.put(objLeadInven.Name, new TAM_LeadInventarios__c(
                    id = objLeadInven.id,
                    TAM_FechaNotificacion__c = dtFechaNoyFinal
                )
            );
        }
        
        if (!mapLeadInvUpd.isEmpty())
            update mapLeadInvUpd.values();                
    }
    
    /*//Metodo para cancelar las ventas espaciales que cambiaron de tipo de venta y de cliente
    private void cancelaVentaEspacial(List<Venta__c> ventasHList){
        System.debug('EM cancelaVentaEspacial...');     
        
        Date dFechaCancelaFinal = Date.today();
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();        
        Map<String, Venta__c> vines = new Map<String, Venta__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVinCheckout = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new Map<String, TAM_LeadInventarios__c>();
        Map<String, List<Movimiento__c>> mapVinLstMov = new Map<String, List<Movimiento__c>>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdCheckoutObjUps = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        for(Venta__c v: mapVentas.values()){
            if (v.Name != null)
                vines.put(v.Name, v);
        }//Fin del for para los vines
        System.debug('EM cancelaVentaEspacial vines: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial vines: ' + vines.values());     
        
        //Consulta los mov asociados a los vines vines.keySet()
        for (Movimiento__c objMovPaso : [Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c,  
                Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, 
                Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c
                From Movimiento__c Where RecordTypeId =: strRecordTypeId
                And VIN__r.Name IN :vines.keySet()]){
            //Mete los mov en el mapa de mapVinLstMov
            if (mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
                mapVinLstMov.get(objMovPaso.VIN__r.Name).add(objMovPaso);            
            if (!mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
                mapVinLstMov.put(objMovPaso.VIN__r.Name, new List<Movimiento__c>{objMovPaso});
        }
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.values());     
        
        //Consulta todas las solicitudes de Pedido especial asociada a estos vines
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso : [Select t.id, t.TAM_VIN__c, 
            t.TAM_SolicitudFlotillaPrograma__c, t.TAM_NombreDistribuidor__c 
            From TAM_CheckOutDetalleSolicitudCompra__c t
            Where TAM_TipoVenta__c != 'Inventario' And TAM_EstatusDOD__c != 'Cancelado' 
            And TAM_EstatusDealerSolicitud__c != 'Cancelada'And TAM_EstatusDealerSolicitud__c != 'Rechazada']){
            //Metelo al mapa de mapVinCheckout
            mapVinCheckout.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso);
        }
        System.debug('EM cancelaVentaEspacial mapVinCheckout: ' + mapVinCheckout.keySet());     
        System.debug('EM cancelaVentaEspacial mapVinCheckout: ' + mapVinCheckout.values());     
        
        
        //Reorre la lista de vines y obten su ultimo mov
        for (String sVinPaso : vines.keySet()){
            //Busca el vin en mapVinLstMov
            if (mapVinLstMov.containsKey(sVinPaso)){
                Movimiento__c objMvPaso = new Movimiento__c();
                List<Movimiento__c> lstMov = mapVinLstMov.get(sVinPaso);
                //Busca su ultimo mov en TAM_ActTotVtaInvBch_cls.getUltMov();
                if (lstMov.size() > 1)
                    objMvPaso =  TAM_ActTotVtaInvBch_cls.getUltMov(lstMov);
                if (lstMov.size() == 1)
                    objMvPaso =  lstMov.get(1);
                //Ve si el VIN esta en el para de mapVinCheckout asociado a una solicutude de pedido especial
                if (mapVinCheckout.containsKey(sVinPaso)){
                    TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = mapVinCheckout.get(sVinPaso);
                    //Ve si el ultimo mov es un pedido especial y cambio a INVENTARIO y ademas esta rechazado
                    if (objMvPaso.Fleet__c == 'N' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c){
		                //Agregalo al mapa de mapIdCheckoutObjUps 
		                TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPasoUpd = new TAM_CheckOutDetalleSolicitudCompra__c(
		                        Id = objCheckOutPaso.id, TAM_ListaParaCancelar__c = true,
		                        TAM_EstatusDealerSolicitud__c = 'Cancelada',
		                        TAM_FechaCancelacion__c = dFechaCancelaFinal,
		                        TAM_VIN__c = objCheckOutPaso.TAM_VIN__c
		                );
		                //Agregar el objeto al mapa de mapIdCheckoutObjUps
		                mapIdCheckoutObjUps.put(objCheckOutPaso.id, objCheckOutPasoUpd);
		                //Crea el registro en el log de errores                
		                lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutPaso.id + '-' + objCheckOutPaso.TAM_VIN__c, TAM_VIN__c = objCheckOutPaso.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Pedio Especial Cambio Fleet', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutPaso) );            
                    }//Fin si  objMvPaso.Fleet__c == 'N' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c
                }//Fin si mapVinCheckout.containsKey(sVinPaso)
            }//Fin si mapVinLstMov.containsKey(sVinPaso)
        }//Fin del for para vines.keySet()

        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdCheckoutObjUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdCheckoutObjUps.values(), TAM_CheckOutDetalleSolicitudCompra__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActCancelSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Checkout ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Crea los reg en el objeto de Log
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);
        
    }*/
        
    private void validateRecords(List<Venta__c> ventasList){
        Map<String, Account> dealers;                            
        System.debug(JSON.serialize(mapVentas));
        Map<String, Vehiculo__c> vines = new Map<String, Vehiculo__c>();          
        Map<String, Product2> productos = new Map<String, Product2>();
        Map<String, SolicitudAutoDemo__c> autosDemo = new Map<String, SolicitudAutoDemo__c>();
        List<Movimiento__c> movimientos = new List<Movimiento__c>();
        Set<String> dealersCode = new Set<String>();  
        Map<String, ColorExterno__c> coloresExt = new Map<String, ColorExterno__c>(); 
        Map<String, ColorInterno__c> coloresInt = new Map<String, ColorInterno__c>(); 
        
        //Obtener los colores externos e internos del archivo de ventas
        Set<String> ColorExt = new Set<String>();
        Set<String> ColorInt = new Set<String>();
        
        for(Venta__c v: ventasList){
            //Para obtener los Colores Externos        		
            if(v.IdColorExt__c != null)
                ColorExt.add(v.IdColorExt__c.right(3));
            //Para obtener los Colores Internos        		
            if(v.IdColorInt__c != null)
                ColorInt.add(v.IdColorInt__c.right(2));     
        }
        
        //Consulta colores externos 
        for (ColorExterno__c ColExt : [Select c.Name, c.Id, c.CodigoColor__c From ColorExterno__c c
                                       Where CodigoColor__c IN :ColorExt]){
                                           coloresExt.put(ColExt.CodigoColor__c, ColExt);
                                       }
        //Crear color externos faltantes
        List<ColorExterno__c> listColorExt = new List<ColorExterno__c>();
        for(String colorExtNuevo : ColorExt){
            if(!coloresExt.containsKey(colorExtNuevo.right(3))){
                ColorExterno__c colorExtNew = new ColorExterno__c();
                colorExtNew.CodigoColor__c = colorExtNuevo.right(3);
                colorExtNew.Name = 'Por definir';
                colorExtNew.Descripcion__c = 'Por definir';
                listColorExt.add(colorExtNew);
                
            } 
            
        }
        if(!listColorExt.isEmpty()){
            upsert listColorExt CodigoColor__c;            
        }        
        
        //Llenar el mapa coloresExt para asociar los colores al VIN
        for(ColorExterno__c colorNuevosExt : listColorExt){
            coloresExt.put(colorNuevosExt.CodigoColor__c, colorNuevosExt);
            
        }
        
        //Consulta colores internos 
        for (ColorInterno__c ColInt : [Select c.Name, c.Id, c.CodigoColor__c From ColorInterno__c c
                                       Where CodigoColor__c IN :ColorInt]){
                                           coloresInt.put(ColInt.CodigoColor__c, ColInt);
                                       }
        //Crear color internos faltantes
        List<ColorInterno__c> listColorInt = new List<ColorInterno__c>();
        for(String colorIntNuevo : ColorInt){
            if(!coloresInt.containsKey(colorIntNuevo.right(3))){
                ColorInterno__c colorIntNew = new ColorInterno__c();
                colorIntNew.CodigoColor__c = colorIntNuevo.right(2);
                colorIntNew.Name = 'Por definir';
                colorIntNew.Descripcion__c = 'Por definir';
                listColorInt.add(colorIntNew);
                
            } 
            
        }
        if(!listColorInt.isEmpty()){
            upsert listColorInt CodigoColor__c;            
        }        
        
        //Llenar el mapa coloresInt para asociar los colores al VIN
        for(ColorInterno__c colorNuevosInt : listColorInt){
            coloresInt.put(colorNuevosInt.CodigoColor__c, colorNuevosInt);
            
        }
        
        for(Venta__c v: ventasList){
            if(v.IdDealerCode__c !=null){	
                dealersCode.add(v.IdDealerCode__c);
            } 
            
            if(v.Name != null){
                if(!vines.containsKey(v.Name)){
                    vines.put(v.Name, new Vehiculo__c(
                        Name = v.Name,
                        AnioModel__c = v.YearModelo__c,
                        Id_Externo__c = v.Name,
                        ColorExternoVehiculo__c = coloresExt.containsKey(v.IdColorExt__c.right(3)) ? coloresExt.get(v.IdColorExt__c.right(3)).id : null,
                        ColorInternoVehiculo__c = coloresInt.containsKey(v.IdColorInt__c.right(2)) ? coloresInt.get(v.IdColorInt__c.right(2)).id : null,
                        EstatusVINAutoDemo__c = 'H',                        
                        RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id                    
                    ));
                }                
            }
            
            if(v.IdModelo__c!= null){
                if(!productos.containsKey(v.IdModelo__c)){
                    productos.put(v.IdModelo__c, new Product2(
                        Name = v.IdModelo__c,
                        IdExternoProducto__c = v.IdModelo__c+v.YearModelo__c,
                        //IdUnidad__c = v.IdModelo__c,
                        IsActive = true,
                        RecordTypeId = recordTypesMap.get('Product2').get('Unidad').Id                        
                    ));
                }
            }
        }
        
        System.debug(dealersCode);
        dealers = AutoDemoTriggerMethods.getDealerCodeInfoMap(dealersCode);
        System.debug(JSON.serialize(dealers));
        
        if(productos.size() > 0){
            try{
                upsert productos.values() IdExternoProducto__c;                
            }catch(Exception ex){
                System.debug('Error upsert productos: '+ex.getMessage());
            }
        }        
        
        try{
            upsert vines.values() Id_Externo__c;            	
            
            for(Venta__c v: ventasList){
                Integer saleCode = v.SaleCode__c != null? Integer.valueOf(v.SaleCode__c): 0;                              
                Vehiculo__c vin = vines.get(v.Name);                
                Product2 prod = productos.get(v.IdModelo__c);                
                vin.Producto__c = prod!=null? prod.Id: null;
                
                if(dealers.containsKey(v.IdDealerCode__c)){                    
                    vin.Distribuidor__c = dealers.get(v.IdDealerCode__c).Id;
                    movimientos.add(new Movimiento__c(
                        VIN__c = vin.Id,
                        Distribuidor__c = dealers.get(v.IdDealerCode__c).Id,                        
                        Modelo__c = productos.get(v.IdModelo__c).Id,                        
                        Year_Modelo__c = v.YearModelo__c,
                        Subfijo__c = v.ModelSubfijo__c,
                        //Eliminar campo ColorExterior__c = coloresExt.get(v.IdColorExt__c).Id,           
                        //Eliminar campo Descr_Color_Ext__c = v.DescrColorExt__c,
                        //Eliminar campo ColorInterno__c = coloresInt.get(v.IdColorInt__c).Id,
                        //Eliminar campo Descr_Color_Int__c = v.DescrColorInt__c,
                        Serial__c = v.Serial__c,
                        Trans_Type__c = v.TransType__c,
                        Factura__c = v.Factura__c,                        
                        Sale_Code__c = v.SaleCode__c,
                        Sale_Date__c = v.SaleDate__c,                        
                        Submitted_Date__c = v.SubmittedDate__c,
                        Submitted_Time__c = v.SubmittedTime__c,
                        Sales_Manager__c = v.SalesManager__c,
                        First_Name__c = v.FirstName__c,
                        Last_Name__c = v.LastName__c,
                        Address_2__c = v.Address2__c,                        
                        City__c = v.City__c,
                        State__c = v.State__c,
                        Zip__c = v.Zip__c,
                        Tel_Home__c = v.TelHome__c,
                        Tel_Work__c = v.TelWork__c,
                        Tel_Work_Ext__c = v.TelWorkExt__c,
                        E_Mail_2__c = v.Email__c,
                        First_Name_Add__c = v.FirstNameAdd__c,
                        Last_Name_Add__c = v.LastNameAdd__c,
                        Address_1_Add__c = v.Address1Add__c,
                        Address_2_Add__c = v.Address2Add__c,
                        City_Add__c = v.CityAdd__c,
                        State_Add__c = v.StateAdd__c,
                        Zip_Add__c = v.ZipAdd__c,                        
                        Tel_Home_Add__c = v.TelHomeAdd__c,
                        Tel_Work_Add__c = v.TelWorkAdd__c,
                        Tel_Work_Ext_Add__c = v.TelWorkExtAdd__c,
                        E_Mail_Add_2__c = v.EmailAdd__c,
                        Ls_Assesor__c = v.LsAssesor__c,
                        Qty__c = v.Count__c,
                        Year__c = v.Year__c,
                        Month__c = v.Month__c,                        
                        Id_Externo__c = v.IdExterno__c,
                        RecordTypeId = recordTypesMap.get('Movimiento__c').get('Movimiento').Id,
                        Fleet__c = v.TAM_FLEET__c
                    ));
                    
                    if(saleCode == 6){
                        //System.debug('Auto demo');
                        /*if(!autosDemo.containsKey(vin.Name)){
						autosDemo.put(vin.Name, new SolicitudAutoDemo__c(
						VIN__c = vin.Id,
						IdVIN__c = vin.Name,
						Distribuidor__c = dealers.get(v.IdDealerCode__c).Id,
						Comentarios__c = 'auto demo desde movimiento H6',
						Estatus__c = Constantes.AUTO_DEMO_SIN_SOLICITUD,
						AnioModeloAutoDemo__c = v.YearModelo__c,
						ColorExteriorAutoDemo__c = coloresExt.get(v.IdColorExt__c).Id,
						ColorInteriorAutoDemo__c = coloresInt.get(v.IdColorInt__c).Id,
						RecordTypeId = recordTypesMap.get('SolicitudAutoDemo__c').get('SolicitudAutoDemoCerrada').Id
						));
						}*/                
						        
                    }//Fin si saleCode == 6
                    
                }//Fin si dealers.containsKey(v.IdDealerCode__c
                
            }//Fin del for para 
            
        } catch(Exception ex){
            System.debug('Error upsert vines: '+ ex.getMessage());
        }
        
        System.debug(JSON.serialize(movimientos));
        if(movimientos.size() > 0){
            try{
                upsert movimientos Id_Externo__c;                
            }catch(Exception ex){
                System.debug('Error Upsert movimientos: '+ex.getMessage());
            }
        }
        
        System.debug(JSON.serialize(autosDemo));
        System.debug(autosDemo.size());
        
        if(autosDemo.size() > 0){
            /*List<SolicitudAutoDemo__c> demos = autosDemo.values();
			Database.SaveResult[] result = Database.insert(demos, false);
			
			for(Integer i=0; i< result.size(); i++){
			if(!result[i].isSuccess()){
			for(Database.Error err: result[i].getErrors()){
			System.debug(err.getMessage());
			}
			
			} else{
			Vehiculo__c vin = vines.get(demos[i].IdVIN__c);
			vin.EstatusVINAutoDemo__c = 'H6';
			System.debug('Auto demo correcto');
			}
			}*/
        } 
        
        try{
            update vines.values();
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }    
       
}