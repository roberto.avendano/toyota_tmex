/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de colonias para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_QuiterWrpColonias {

    //Ahora Crea el llamado para la serializacion JSON.deserialize
    public List<QuiterColonia> colonies;    

    public class QuiterColonia{
        @AuraEnabled 
        public String id {get;set;}    
        @AuraEnabled 
        public String delegationId {get;set;}    
        @AuraEnabled 
        public String description {get;set;}    
        @AuraEnabled 
        public String postalCode {get;set;}    
                    
        //Un contructor por default
        public QuiterColonia(){
            this.description = '';
            this.id = ''; 
            this.delegationId = '';
            this.postalCode = '';
        }
        
        //Un contructor por default
        public QuiterColonia(String id, String delegationId, String description, String postalCode){
            this.id = id;
            this.delegationId = delegationId;   
            this.description = description;
            this.postalCode = postalCode;
        }
    }
    
    public class QuiterColoniaCreate{
        @AuraEnabled 
        public String delegationId {get;set;}    
        @AuraEnabled 
        public String description {get;set;}    
        @AuraEnabled 
        public String postalCode {get;set;}    
                    
        //Un contructor por default
        public QuiterColoniaCreate(){
            this.description = '';
            this.delegationId = '';
            this.postalCode = '';
        }
        
        //Un contructor por default
        public QuiterColoniaCreate(String delegationId, String description, String postalCode){
            this.delegationId = delegationId;   
            this.description = description;
            this.postalCode = postalCode;
        }
    }
    
}