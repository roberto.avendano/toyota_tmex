/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del archivo de 
                        TAM_UsrActSegLeadPasoTgrHandler
                        Actualiza los datos del candidato asociado a la solicitud.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_UsrActSegLeadPasoTgrHandler extends TriggerHandler {

    private List<TAM_UsrActSegLeadPaso__c> solicitudes;
    private Map<id,TAM_UsrActSegLeadPaso__c> mapsolicitudesOld;

    public TAM_UsrActSegLeadPasoTgrHandler(){
        System.debug(LoggingLevel.INFO,'Inicia TAM_UsrActSegLeadPasoTgrHandler');  
        this.solicitudes = (List<TAM_UsrActSegLeadPaso__c>) trigger.new;
        this.mapsolicitudesOld = (Map<id,TAM_UsrActSegLeadPaso__c>) trigger.oldMap;
    }

    public override void afterInsert(){
        System.debug(LoggingLevel.INFO,'Inicia afterInsert...');
        actualizaEstatusUsr(solicitudes);
    }
    
    //Para el evwento del afterUpdate
    public override void afterUpdate(){
        System.debug(LoggingLevel.INFO,'Inicia afterUpdate...');
        actualizaEstatusUsr(solicitudes);
    }        

   public static void actualizaEstatusUsr (List<TAM_UsrActSegLeadPaso__c> lUsrSegLead){
        System.debug(LoggingLevel.INFO,'EN TAM_UsrActSegLeadPasoTgrHandler actualizaEstatusUsr...');
    
        Map<String, boolean> mapIdUsrSfdcEstatus = new Map<String, boolean>();
        Map<String, TAM_UsuariosSeguimientoLead__c> mapIdUsrSegUpd = new Map<String, TAM_UsuariosSeguimientoLead__c>();
        
        //Recorre la lista de usuarios
        for (TAM_UsrActSegLeadPaso__c objUsrPaso : lUsrSegLead){
            mapIdUsrSfdcEstatus.put(objUsrPaso.YAM_IdUsuarioPartner__c, objUsrPaso.TAM_Estatus__c);
        }//Fin del for para lUsrSegLead

        //Busca los reg del objeto TAM_UsuariosSeguimientoLead__c
        for (TAM_UsuariosSeguimientoLead__c objUsrSegLead : [Select ID, Name, TAM_IdUsuarioPartner__c 
            From TAM_UsuariosSeguimientoLead__c
            Where TAM_IdUsuarioPartner__c IN: mapIdUsrSfdcEstatus.KeySet()]){
            //Agregalo a mapIdUsrSegUpd
            mapIdUsrSegUpd.put(objUsrSegLead.id, new TAM_UsuariosSeguimientoLead__c(
                    id = objUsrSegLead.id,
                    TAM_Activo__c = mapIdUsrSfdcEstatus.get(objUsrSegLead.TAM_IdUsuarioPartner__c)
                )
            );
        }
        System.debug('EN AcctUsrSegLead mapIdUsrSegUpd: ' + mapIdUsrSegUpd);
        
        //Ve si tiene algo la lista de lUsuariosSeguimientoLeadUps
        if (!mapIdUsrSegUpd.isEmpty()){
            List<Database.UpsertResult> ldtSvr = Database.upsert(mapIdUsrSegUpd.values(), TAM_UsuariosSeguimientoLead__c.id, false);
            for(Database.UpsertResult objSvr : ldtSvr){
                if (!objSvr.isSuccess())
                    System.debug('Hubo un error a la hora de Actualizar los datos de los usuarios en mapIdUsrSegUpd:' + objSvr.getErrors()[0].getMessage());
            }
        }//Fin si !lUsuariosSeguimientoLeadUps.isEmpty()
    
   }    
   
    
}