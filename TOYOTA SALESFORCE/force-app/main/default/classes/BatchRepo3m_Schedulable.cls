global class BatchRepo3m_Schedulable implements Schedulable{
    global static String scheduleit(){
        string strSeconds = '0';
        string strMinutes = '0';
        string strHours = '1';
        string strDay_of_month = 'L';
        string strMonth = '3,6,9,12';
        string strDay_of_week = '?';
        string strYear = '2019-2200';
        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
        
        BatchRepo3m_Schedulable job = new BatchRepo3m_Schedulable();
        return System.schedule('Ejecuta_BatchRepo3m',sch,job);
    }
    
    global void execute(SchedulableContext sc) {
        BatchRepo3m uc = new BatchRepo3m();
        Database.executeBatch(uc);
    }
}