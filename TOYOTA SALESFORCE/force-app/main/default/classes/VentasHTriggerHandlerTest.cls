@isTest
private class VentasHTriggerHandlerTest {
	@TestSetup static void loadData(){
		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cuenta;
	}

	public static List<Venta__c> loadVentasH(){
		List<Venta__c> ventasH = new List<Venta__c>();
		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};
		
		String sTAMTipoCliente = 'Primero';
		for(Integer i=0; i < 10; i++){			
		 	String sufix = i < 10 ? '0'+String.valueOf(i):String.valueOf(i);  
		 	ventasH.add(new Venta__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),            		
        		IdModelo__c = modelos.get(aleatorio(modelos.size())),
        		YearModelo__c = '2017',
        		IdColorExt__c = colores.get(aleatorio(colores.size())),
        		DescrColorExt__c = 'External color description',
        		IdColorInt__c = colores.get(aleatorio(colores.size())),
        		DescrColorInt__c = 'Internal color description',
    			Serial__c = seriales.get(aleatorio(seriales.size())),
    			SaleCode__c = sufix,
    			SubmittedDate__c = Datetime.newInstance(2017, 10, 13),
        		IdExterno__c = 'JTDKBRFUXH30310'+sufix+'-'+String.valueOf(Date.today()),
        		TAM_TipoCliente__c = sTAMTipoCliente,
        		FirstName__c = 'PruebaFN',
        		LastName__c = 'PruebaLN'
	 		));
	 		//Cambia el estatus
	 		if (sTAMTipoCliente == 'Primero')
				sTAMTipoCliente = 'Segundo';
	 		else if (sTAMTipoCliente == 'Segundo')
				sTAMTipoCliente = 'Primero';
		}		
				
		System.debug(JSON.serialize(ventasH));		
		return ventasH;
	}

    public static List<TAM_LeadInventarios__c> loadLeadInvNvo(){
        String sTAMTipoCliente = 'Primero';
        List<TAM_LeadInventarios__c> leadInvNvo = new List<TAM_LeadInventarios__c>();
        
        for(Integer i=0; i < 2; i++){
            String sufix = i < 10 ? '0'+String.valueOf(i):String.valueOf(i);
            leadInvNvo.add(new TAM_LeadInventarios__c(
                    Name = 'JTDKBRFUXH30310'+sufix
                )
            );                      
        }
        
        return leadInvNvo;
    }

	@isTest static void test_one() {
        List<TAM_LeadInventarios__c> leadInv = VentasHTriggerHandlerTest.loadLeadInvNvo();
	    insert leadInv;
	    System.debug('EM VentasHTriggerHandlerTest leadInv: ' + leadInv);
	    
		List<Venta__c> ventas = VentasHTriggerHandlerTest.loadVentasH();
		insert ventas;
        System.debug('EM VentasHTriggerHandlerTest ventas: ' + ventas);		
	}

	@isTest static void test_two() {
		List<Venta__c> ventas = VentasHTriggerHandlerTest.loadVentasH();
		for(Integer i=0; i<ventas.size(); i++){
			ventas[i].SubmittedDate__c = Datetime.newInstance(2017, 10, 13);
			ventas[i].SaleCode__c = String.valueOf(i);
		}
		insert ventas;
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
}