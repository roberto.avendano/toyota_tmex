public with sharing class EvaluacionServicioWizardController{

    public Evaluaciones_Dealer__c evalDealer{get; set;}
    public String errores{get;set;}

    public Map<String,Seccion__c> mapSecciones{get;set;}
    public Map<String, List<Respuestas_Preguntas_TSM__c>> mapRespuestas{get;set;}
    
    public Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPI{get; set;}
    public Actividad_Plan_Integral__c currentRespuestaActividadPI{get; set;}

    public Respuestas_Preguntas_TSM__c respuestaEval{get;set;}
    
    public Id idRTAPI{get; set;}

    public List<Seccion__c> listSeccionesOrdenados{get; set;}

    public Map<Id,List<Respuestas_ObjetosTSM__c>> objetosEvaluacion{get; set;}

    public Boolean avancePorSeccion{get;set;}

    public EvaluacionServicioWizardController(ApexPages.StandardController stdController){
        errores = '';
        avancePorSeccion = false;
        respuestaEval = new Respuestas_Preguntas_TSM__c();
        evalDealer = (Evaluaciones_Dealer__c)stdController.getRecord();
        objetosEvaluacion = obtieneObjetoEvaluacion(evalDealer.Id);
        Set<Id> setIdsPreguntas = new Set<Id>();

        mapSecciones = new Map<String,Seccion__c>();
        listSeccionesOrdenados = new List<Seccion__c>();
        mapRespuestas = new Map<String, List<Respuestas_Preguntas_TSM__c>>();

        if(evalDealer!=null && evalDealer.Id!=null){
            evalDealer = this.getEvaluacion();
        
            Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
            if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_EDER').Id){
                idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_FTS').Id;
            }else if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_SSC').Id){
                idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_FTS').Id;
            }else if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_OperacionServicio').Id){
                idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_DSPM').Id;
                avancePorSeccion = true;
            }else if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_BodyPaint').Id){
                idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('Body_Paint').Id;
                avancePorSeccion = true;
            }
            
            currentRespuestaActividadPI = new Actividad_Plan_Integral__c(Cuenta__c=evalDealer.Nombre_Dealer__c);
            
            if(evalDealer.Respuestas_Preguntas_TSM__r!=null && evalDealer.Respuestas_Preguntas_TSM__r.size()>0){
                for(Respuestas_Preguntas_TSM__c resp : evalDealer.Respuestas_Preguntas_TSM__r){
                    if(!objetosEvaluacion.containsKey(resp.Id)){
                        objetosEvaluacion.put(resp.Id, new List<Respuestas_ObjetosTSM__c>());
                    }
                    if(!mapSecciones.containsKey(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
                        mapSecciones.put(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r);
                        listSeccionesOrdenados.add(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r);
                        mapRespuestas.put(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, new List<Respuestas_Preguntas_TSM__c>());
                    }
                    mapRespuestas.get(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c).add(resp);
                    setIdsPreguntas.add(resp.Id);
                }
            }
            
            //mapActividadesPI = this.loadMapActividadesPI(setIdsPreguntas);
            mapActividadesPI = this.loadMapActividadesPI(obtieneIdsObjetosEval(objetosEvaluacion));
            
        }
    }
    
    private Map<Id, List<Actividad_Plan_Integral__c>> loadMapActividadesPI(Set<Id> setIdsPreguntas){
        Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPITmp = new Map<Id, List<Actividad_Plan_Integral__c>>();
        Map<Id,Respuestas_ObjetosTSM__c> mapRespuestasConAPI = new Map<Id,Respuestas_ObjetosTSM__c>([
            SELECT Id, (
                select Id, Name, Condicion_Observada__c, Prioridad__c, Responsable_Dealer__c, Actividad_Mejora__c, Respuestas_Preguntas_TSM__c, Celula_Area__c, Fecha_Compromiso__c, RecordTypeId 
                from Actividades_Planes_Integrales__r),
                (select Id, Name from Attachments) 
            FROM Respuestas_ObjetosTSM__c WHERE Id IN :setIdsPreguntas
        ]);
        
        for(Id item : setIdsPreguntas){
            if(!mapRespuestasConAPI.containsKey(item) || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r==null || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r.size()<1){
                Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
                    Respuestas_Preguntas_TSM__c =item,
                    Cuenta__c = evalDealer.Nombre_Dealer__c
                );
                if(idRTAPI!=null){
                    api.RecordTypeId = idRTAPI;
                }
                mapActividadesPITmp.put(item,new List<Actividad_Plan_Integral__c>{api});
            }else{
                mapActividadesPITmp.put(item,mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r);
            }
        }
        return mapActividadesPITmp;
    }
    
    public List<SelectOption> getAreas(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Celula_Area__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //options.add(new SelectOption('', ''));
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getReferencia(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Referencia__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //options.add(new SelectOption('', ''));
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    private Evaluaciones_Dealer__c getEvaluacion(){
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        String queryRespuestas = '(SELECT Name, Realizado__c, Respuesta_Pregunta__c, Pregunta_Relacionada__c, Pregunta_Relacionada__r.Name, Pregunta_Relacionada__r.No_de_Reactivo__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Id, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,  Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Objetivo__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Orden__c, Pregunta_Relacionada__r.Reactivo__c, Pregunta_Relacionada__r.Area__c, Pregunta_Relacionada__r.Seccion__c, Pregunta_Relacionada__r.Obligatoria__c, Pregunta_Relacionada__r.Programa__c, Pregunta_Relacionada__r.Criterio_Negativo__c, Pregunta_Relacionada__r.Criterio_Positivo__c, Pregunta_Relacionada__r.Metodo_de_Evaluacion__c, Pregunta_Relacionada__r.Ponderacion__c, Pregunta_Relacionada__r.Fotografia_Obligatoria_del__c, Pregunta_Relacionada__r.Foto__c, Observaciones__c FROM Respuestas_Preguntas_TSM__r ORDER BY Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Orden__c, Pregunta_Relacionada__r.No_de_Reactivo__c), ';
        query += queryRespuestas + ' (select Id, Name from Attachments), Id FROM Evaluaciones_Dealer__c WHERE Id=\'' + evalDealer.Id + '\'';
        return Database.query(query);
    }

    public Map<Id,List<Respuestas_ObjetosTSM__c>> obtieneObjetoEvaluacion(Id idEval){
        Map<Id,List<Respuestas_ObjetosTSM__c>> respObjs = new Map<Id,List<Respuestas_ObjetosTSM__c>>();
        for(Respuestas_ObjetosTSM__c item : [
                select Id, Name, Objeto_Evaluacion_Relacionado__r.Name, Si__c, No__c, Observaciones__c, 
                Objeto_Evaluacion_Relacionado__r.Metodos_de_Evaluacion__c, Pregunta_Relacionada_del__c, Respuesta__c, Respuestas_Preguntas_TSM_del__c, 
                Objeto_Evaluacion_Relacionado__r.Foto__c, Respuestas_Preguntas_TSM_del__r.Respuesta_Pregunta__c, Pregunta_Relacionada_del__r.Reactivo__c 
                from Respuestas_ObjetosTSM__c 
                where Respuestas_Preguntas_TSM_del__r.Evaluacion_Dealer__c=:idEval order by Objeto_Evaluacion_Relacionado__r.Orden__c]){
            if(!respObjs.containsKey(item.Respuestas_Preguntas_TSM_del__c)){
                respObjs.put(item.Respuestas_Preguntas_TSM_del__c, new List<Respuestas_ObjetosTSM__c>());
            }
            respObjs.get(item.Respuestas_Preguntas_TSM_del__c).add(item);
        }
        return respObjs;
    }

    public Set<Id> obtieneIdsObjetosEval(Map<Id,List<Respuestas_ObjetosTSM__c>> lista){
        Set<Id> ids = new Set<Id>();
        for(List<Respuestas_ObjetosTSM__c> pregunta : lista.values()){
            for(Respuestas_ObjetosTSM__c item : pregunta){
                ids.add(item.Id);
            }
        }
        return ids;
    }

    public String getEvalDealerTxt(){
        return JSON.serializePretty(mapSecciones); 
    }

    public List<Schema.FieldSetMember> getFields(){
             return SObjectType.Evaluaciones_Dealer__c.FieldSets.Evaluacion_Integral_01.getFields();
    }
    
    public List<SelectOption> getResponsablesDealer(){
        List<SelectOption> options = new List<SelectOption>();
        
        for( Contact c : [Select Id, Name, Title From Contact Where AccountId=:evalDealer.Nombre_Dealer__c]){
            String titulo = (c.Title!=null && c.Title!='')?' - ' + c.Title : '';
            options.add(new SelectOption(c.Id, c.Name + titulo));
        }       
        return options;
    }

    @RemoteAction
    public static ResultGuardaRespuesta guardarRespuesta(String idRP, Boolean estado, String observaciones, Double puntos){
        String resp = 'Se guardo correctamente';
        Respuestas_ObjetosTSM__c obj = new Respuestas_ObjetosTSM__c(Id = Id.valueOf(idRP));
        try{
            if(observaciones!=null){
                obj.Observaciones__c = observaciones;
            }else{
                obj.Respuesta__c = estado?'Si':'No';
            }
            update obj;
            obj = [SELECT Id, Pregunta_Relacionada_del__r.Ponderacion__c, Respuestas_Preguntas_TSM_del__r.Respuesta_Pregunta__c, Pregunta_Relacionada_del__c FROM Respuestas_ObjetosTSM__c WHERE Id=:idRP];
        }catch(DMLException  dmle){
            System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
        return new ResultGuardaRespuesta(resp,obj);
    }

    public class ResultGuardaRespuesta{
        public String resp{get;set;}
        public Respuestas_ObjetosTSM__c obj{get;set;}
        public ResultGuardaRespuesta(String resp, Respuestas_ObjetosTSM__c obj){
            this.resp = resp;
            this.obj = obj;
        }
    }
    
    @RemoteAction
    public static Actividad_Plan_Integral__c guardaAPI(String idEval, String idRespuesta, String idAPI, String condicion, String actividad, String celula, String responsable, String evalDealerId, String fechaComp, String rtAPI){
        try{
            System.debug(Id.valueOf(idAPI));
        }catch(Exception e){
            System.debug(e);
            idAPI=null;
        }
        Date fechaCompDate = null;
        if(fechaComp!=null && fechaComp!=''){
            try{
                fechaCompDate = Date.parse(fechaComp);
            }catch(Exception e){
                throw new ToyotaException('La fecha no esta en el formato requerido');
            }
        }
        try{
            Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
                RespuestasObjetosTSM__c = idRespuesta,
                Id = idAPI,
                Condicion_Observada__c = condicion,
                Actividad_Mejora__c = actividad,
                Celula_Area__c = celula,
                Responsable_Dealer__c = responsable,
                Cuenta__c=evalDealerId,
                Fecha_Compromiso__c=fechaCompDate,
                RecordTypeId = rtAPI,
                EvaluacionDealer__c = idEval
            ); 
            
            System.debug(api);
            upsert api;
            return api;//[SELECT Id, Name, Respuestas_Preguntas_TSM__c, Condicion_Observada__c, Actividad_Mejora__c, Celula_Area__c, Responsable_Dealer__c, Fecha_Compromiso__c FROM Actividad_Plan_Integral__c WHERE Id=:api.Id];
        }catch(DMLException  dmle){
            System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
    }
}