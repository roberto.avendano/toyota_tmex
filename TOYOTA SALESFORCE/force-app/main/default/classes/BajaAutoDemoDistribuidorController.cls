public with sharing class BajaAutoDemoDistribuidorController {
    public ApexPages.StandardSetController setController{get;set;}
    public String solicitudID{get;set;}

    public BajaAutoDemoDistribuidorController(ApexPages.StandardController controller) {
		solicitudID='';
		setController = new ApexPages.StandardSetController(Database.getQueryLocator([
			SELECT Id, Name, FechaAltaAutoDemo__c, DuracionAutoDemoEstatusAlta__c,
				   FechaBajaAutoDemoVigenciaPlanPiso__c, Estatus__c
			FROM SolicitudAutoDemo__c WHERE Estatus__c =: Constantes.CASE_STATUS_ALTA
		]));
		setController.setPageSize(10);
    }

	public List<SolicitudAutoDemo__c> getSolicitudes(){
		return (List<SolicitudAutoDemo__c>) setController.getRecords();
	}

	public PageReference anterior(){
		setController.previous();
		return null;
	}

	public PageReference siguiente(){
		setController.next();
		return null;
	}

	public PageReference solicitudEdit(){
		PageReference ret = new PageReference('/apex/BajaAutoDemoEdit?id='+solicitudID);
		ret.setRedirect(true);
		return ret;
	}
}