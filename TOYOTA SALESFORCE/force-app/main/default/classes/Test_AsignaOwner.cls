// Clase de prueba de trigger AsignaOwner 
@isTest
public class Test_AsignaOwner {
    static testmethod void test(){
        List<fwy_ReporteComunidades__c> ej = new List<fwy_ReporteComunidades__c>();               
        Id p = [select id from profile where name='Partner Community User'].id;

       	Account ac = new Account(
            name ='Grazitti',
        	Codigo_Distribuidor__c = '57031');
        insert ac; 
        
        Contact con = new Contact(
            LastName ='testCon',            
            AccountId = ac.Id);
        insert con;  
        
        User user = new User(
            alias = 'test123p', 
            Owner_De_Registros__c = true,
            email='test123tfsproduc@noemail.com',
            emailencodingkey='UTF-8', 
            lastname='Testingproductfs', 
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p, 
            country='United States',
            IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', 
            username='testOwnertfsproduc@noemail.com.test');
        
        insert user;
        
        
        
        for(integer x=0; x<10; x++){
            fwy_ReporteComunidades__c y = new fwy_ReporteComunidades__c();
            y.fwy_Nombre_del_Dealer__c = 'Ejemplo '+x;
            y.fwy_Dealer_Code__c = '57031';        
            ej.add(y);
        }    
        insert ej;
        
    }    
}