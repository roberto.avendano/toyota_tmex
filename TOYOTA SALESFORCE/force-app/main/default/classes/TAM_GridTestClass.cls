/**
Descripción General: Controlador para el componente "TAM_Grid"
________________________________________________________________
Autor               Fecha               Descripción
________________________________________________________________
Cecilia Cruz        08/Abril/2020         Versión Inicial
Cecilia Cruz        09/Octubre/2020       Duplicar y actualizar politica
________________________________________________________________
**/
@isTest(SeeAllData=false)
public class TAM_GridTestClass {
    
    @testSetup static void setup() {
        
        //Producto
        Product2 objProduct = new Product2();
        objProduct.Name='2205';
        objProduct.IdExternoProducto__c='22052020';
        objProduct.ProductCode='22052020';
        objProduct.CantidadMaxima__c = 5;
        objProduct.IsActive = true;
        insert objProduct;
        
        //PricebookEntry
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry objPricebookEntry = new PricebookEntry();
        objPricebookEntry.Pricebook2Id= standardPricebookId;
        objPricebookEntry.Product2Id= objProduct.Id;
        objPricebookEntry.UnitPrice= 0.0;
        objPricebookEntry.IsActive = true;
        objPricebookEntry.PrecioSinIva__c = 211176.00;
        objPricebookEntry.TAM_MargenesGananciaTMEX_Porc__c = 0.04;
        objPricebookEntry.TAM_MargenesGananciaTMEX_Pesos__c = 8033;
        objPricebookEntry.TAM_MSRP__c = 251400;
        insert objPricebookEntry;
        
        //Creación Catálogo Centralizado.
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = new List<CatalogoCentralizadoModelos__c>();
        for(Integer i=0; i<20; i++){
            CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
            objCatalogo.Name = 'AVANZA-2205-2020-1E7-10-LE MT' + i;
            objCatalogo.Serie__c = 'AVANZA';
            objCatalogo.AnioModelo__c = '2020';
            objCatalogo.Modelo__c = '2205';
            objCatalogo.Version__c = 'LE MT';
            lstCatalogoCentralizado.add(objCatalogo);
        }
        insert lstCatalogoCentralizado;
        
        //Rangos
        List<Rangos__c> lstRangos = new List<Rangos__c>();
        for(Integer i=0; i<5; i++){
            Rangos__c objRangos = new Rangos__c();
            objRangos.Name = 'A' + i;
            objRangos.NumUnidadesMinimas__c = 5;
            objRangos.NumUnidadesMaximas__c = 30;
            objRangos.DiasVigentes__c = 180;
            objRangos.PerGraciaRango__c = 15;
            objRangos.Activo__c = true;
            lstRangos.add(objRangos);
        }
        insert lstRangos;
        
        //Inventario en Piso
        InventarioPiso__c objInventario = new InventarioPiso__c();
        objInventario.Name = 'Avanza-2205-2020-1E7-2020-04-05';
        objInventario.FechaInventario__c = Date.today()-2;
        objInventario.Modelo__c = '2205';
        objInventario.AnioModelo__c = '2020';
        objInventario.Serie__c = 'Avanza';
        objInventario.Version__c = 'LE MT';
        objInventario.Cantidad__c = 73;
        objInventario.DiasVenta__c = 19;
        insert objInventario;
        
        //Politica de Incentivos
        List<TAM_PoliticasIncentivos__c> lstPoliticas = new List<TAM_PoliticasIncentivos__c>();
        //Retail
        TAM_PoliticasIncentivos__c objPoliticaRetail = new TAM_PoliticasIncentivos__c();
        objPoliticaRetail.Name = 'Politica de Incentivos Retail';
        objPoliticaRetail.TAM_InicioVigencia__c = Date.today()-1;
        objPoliticaRetail.TAM_FinVigencia__c = Date.today() + 1;
        objPoliticaRetail.TAM_PeriodoDeGracia__c = 5;
        objPoliticaRetail.TAM_EstatusIncentivos__c = 'En proceso';
        objPoliticaRetail.TAM_CargaHistorica__c = true;
        objPoliticaRetail.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        lstPoliticas.add(objPoliticaRetail);
        
        
        //Vta. Corporativa
        TAM_PoliticasIncentivos__c objPoliticaVtaCorp = new TAM_PoliticasIncentivos__c();
        objPoliticaVtaCorp.Name = 'Politica de Incentivos Vta Corporativa';
        objPoliticaVtaCorp.TAM_InicioVigencia__c = Date.today() - 5;
        objPoliticaVtaCorp.TAM_FinVigencia__c = Date.today() + 1;
        objPoliticaVtaCorp.TAM_PeriodoDeGracia__c = 5;
        objPoliticaVtaCorp.TAM_EstatusIncentivos__c = 'En proceso';
        objPoliticaVtaCorp.TAM_CargaHistorica__c = true;
        objPoliticaVtaCorp.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
        lstPoliticas.add(objPoliticaVtaCorp);
        insert lstPoliticas;
        
        //Detalle Politica retail
        TAM_DetallePoliticaJunction__c objDetalle1_Retail = TAM_TestUtilityClass.crearDetallePoliticaIncentivos('AVANZA', '2205','2020', 'x',
                                                                                                                10000, 5000, 5000, 
                                                                                                                0,0,0,false,objPoliticaRetail.Id, 200000);
        //DetallePolitica Corp
        TAM_DetallePoliticaJunction__c objDetalle1_Corp = TAM_TestUtilityClass.crearDetallePoliticaIncentivos('AVANZA', '2205','2020', 'x',
                                                                                                              10000, 5000, 5000, 
                                                                                                              0,0,0,false,objPoliticaVtaCorp.Id, 200000);

        objDetalle1_Corp.TAM_IdExterno__c = objPoliticaRetail.Id + '2020' + 'AVANZA' + '2205' + 0;
        update objDetalle1_Corp;

        
    }
    
    @isTest
    public static void updatePoliticaIncentivos(){
        
        //Obtener información de pruebas
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Id, Serie_AnioModelo__c, Modelo__c, AnioModelo__c, Serie__c, Version__c 
                                                                        FROM 	CatalogoCentralizadoModelos__c ];
        
        List<Rangos__c> lstRangos = [SELECT Id, Name FROM Rangos__c];
        
        /*List<InventarioPiso__c> lstInventario = [SELECT Id, AnioModelo__c, Modelo__c, Serie__c, Version__c, Cantidad__c, DiasVenta__c, Serie_AnioModelo__c
FROM	InventarioPiso__c];*/
        
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        
        PricebookEntry objPricebookEntry = [SELECT 	Id, 
                                            ProductCode, 
                                            PrecioSinIva__c, 
                                            TAM_MargenesGananciaTMEX_Pesos__c, 
                                            TAM_MargenesGananciaTMEX_Porc__c,
                                            TAM_MSRP__c
                                            FROM		PricebookEntry];
        
        //Detalle Politica Junction
        String idPolitica = objPolitica.Id;
        List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaJunction = [SELECT 	Id, Name, TAM_PoliticaIncentivos__c, TAM_IncPropuesto_Cash__c
                                                                           FROM		TAM_DetallePoliticaJunction__c
                                                                           WHERE	TAM_PoliticaIncentivos__c =: idPolitica];
        
        List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaAUX = new List<TAM_DetallePoliticaJunction__c>();
        for(TAM_DetallePoliticaJunction__c objDetalle : lstDetallePoliticaJunction){
            objDetalle.TAM_IncPropuesto_Cash__c = 10000;
            lstDetallePoliticaAUX.add(objDetalle);
        }
        
        test.startTest();
        upsert lstDetallePoliticaAUX;
        
        objPolitica.TAM_EstatusIncentivos__c = 'Vigente';
        update objPolitica;
        test.stopTest();
        
        TAM_DetallePolitica__c objDetalle = [SELECT Id, TAM_PoliticaIncentivos__c FROM TAM_DetallePolitica__c LIMIT 1];
        System.assertEquals(idPolitica, objDetalle.TAM_PoliticaIncentivos__c);
    }
    
    @isTest
    public static void test_TAM_Grid_getRangos(){
        
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        test.startTest();
        TAM_Grid.getRangos();
        test.stopTest();
        
        Rangos__c objRango = [SELECT Id, Activo__c FROM Rangos__c LIMIT 1];
        System.assertEquals(true, objRango.Activo__c);
    }
    
    @isTest
    public static void test_TAM_Grid_getRecordType(){
        
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c,RecordTypeId
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        test.startTest();
        TAM_Grid.TipoRegistroPolitica(objPolitica.Id);
        test.stopTest();
        
        String strRecordTypeId = objPolitica.RecordTypeId;
        String strRecordTypeName = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosById().get(strRecordTypeId).getname();
        System.assertEquals('Retail', strRecordTypeName);
        
    }
    
    @isTest
    public static void test_TAM_Grid_getDetalleJunction(){
        
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        test.startTest();
        TAM_Grid.getDetalleJunction(objPolitica.Id);
        test.stopTest();
        
        String idPolitica = objPolitica.Id;
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT Id, TAM_PoliticaIncentivos__c FROM TAM_DetallePoliticaJunction__c LIMIT 1];
        System.assertEquals(idPolitica, objDetalle.TAM_PoliticaIncentivos__c);
    }
    
    @isTest
    public static void test_TAM_Grid_getSeries(){
        
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        test.startTest();
        TAM_Grid.getSeries(objPolitica.Id);
        test.stopTest();
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT TAM_Serie__c FROM TAM_DetallePoliticaJunction__c WHERE TAM_Clave__c = '2205' LIMIT 1];
        System.assertEquals('AVANZA', objDetalle.TAM_Serie__c);
    }
    
    @isTest
    public static void test_TAM_Grid_Save(){
        
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        //Detalle Politica Junction
        String idPolitica = objPolitica.Id;
        List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaJunction = [SELECT  	Id, 
                                                                           Name, 
                                                                           TAM_PoliticaIncentivos__c,
                                                                           TAM_AnioModelo__c,
                                                                           TAM_Serie__c,
                                                                           TAM_Version__c,
                                                                           TAM_Clave__c,
                                                                           TAM_Inventario_Dealer__c,
                                                                           TAM_Dias_de_Inventario__c,
                                                                           TAM_Venta_MY__c,
                                                                           TAM_BalanceOut__c,
                                                                           TAM_Pago_TFS__c,
                                                                           TAM_IncPropuesto_Cash__c,
                                                                           TAM_IncPropuesto_Porcentaje__c,
                                                                           TAM_IncentivoTMEX_Cash__c,
                                                                           TAM_IncentivoTMEX_Porcentaje__c,
                                                                           TAM_IncDealer_Cash__c,
                                                                           TAM_IncDealer_Porcentaje__c,
                                                                           TAM_Tripleplay__c,
                                                                           TAM_ProductoFinanciero__c,
                                                                           TAM_Descripcion__c,
                                                                           TAM_TotalEstimado__c,
                                                                           TAM_TP_IncTMEX_Cash__c,
                                                                           TAM_TP_IncTMEX_Porcentaje__c,
                                                                           TAM_TP_IncDealer_Porcentaje__c,
                                                                           TAM_TP_IncDealer_Cash__c,
                                                                           TAM_IncTFS_Cash__c,
                                                                           TAM_IncTFS_Porcentaje__c,
                                                                           TAM_Lealtad__c,
                                                                           TAM_BonoLealtad__c,
                                                                           TAM_MSRP__c,
                                                                           TAM_Duplicar__c,
                                                                           TAM_TMEX_Margen_Pesos__c,
                                                                           TAM_Consecutivo__c,
                                                                           TAM_Rango__c,
                                                                           TAM_TMEX_Margen_Porcentaje__c,
                                                                           TAM_BonoLealtadTMEX__c,
                                                                           TAM_BonoLealtadDealer__c,
                                                                           TAM_FechaInventario__c,
                                                                           TAM_Lanzamiento__c,
                                                                           TAM_MotivoIncentivo__c,
                                                                           TAM_AplicanAmbosIncentivos__c,
                                                                           TAM_Incentivo_actual__c
                                                                           FROM		TAM_DetallePoliticaJunction__c
                                                                           WHERE	TAM_PoliticaIncentivos__c =: idPolitica];
        
        test.startTest();
        TAM_Grid.guardarPolitica(lstDetallePoliticaJunction, objPolitica.Id);
        test.stopTest();
        
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT Id, TAM_PoliticaIncentivos__c FROM TAM_DetallePoliticaJunction__c LIMIT 1];
        System.assertEquals(idPolitica, objDetalle.TAM_PoliticaIncentivos__c);
    }
    
    @isTest
    public static void test_ActualizarModelos(){
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Id, Serie_AnioModelo__c, Modelo__c, AnioModelo__c, Serie__c, Version__c 
                                                                        FROM 	CatalogoCentralizadoModelos__c ];
        CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
        objCatalogo.Name = 'AVANZA-2205-2020-1E7-10-LE MT';
        objCatalogo.Serie__c = 'AVANZA';
        objCatalogo.AnioModelo__c = '2030';
        objCatalogo.Modelo__c = '2205';
        objCatalogo.Version__c = 'LE MT';
        lstCatalogoCentralizado.add(objCatalogo);
        upsert lstCatalogoCentralizado;
        
        test.startTest();
        TAM_Grid.actualizarModelos(objPolitica.Id);
        test.stopTest();
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT TAM_Serie__c FROM TAM_DetallePoliticaJunction__c WHERE TAM_Clave__c = '2205' LIMIT 1];
        System.assertEquals('AVANZA', objDetalle.TAM_Serie__c);
    }
    
    @isTest
    public static void test_ActualizarModelosConIncentivo(){
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Id, TAM_EstatusIncentivos__c 
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Retail' LIMIT 1];
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Id, Serie_AnioModelo__c, Modelo__c, AnioModelo__c, Serie__c, Version__c 
                                                                        FROM 	CatalogoCentralizadoModelos__c ];
        CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
        objCatalogo.Name = 'AVANZA-2205-2021-1E7-10-LE MT';
        objCatalogo.Serie__c = 'AVANZA';
        objCatalogo.AnioModelo__c = '2021';
        objCatalogo.Modelo__c = '2205';
        objCatalogo.Version__c = 'LE MT';
        lstCatalogoCentralizado.add(objCatalogo);
        upsert lstCatalogoCentralizado;
        
        
        
        
        test.startTest();
        TAM_Grid.actualizarModelos(objPolitica.Id);
        test.stopTest();
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT TAM_Serie__c FROM TAM_DetallePoliticaJunction__c WHERE TAM_Clave__c = '2205' LIMIT 1];
        System.assertEquals('AVANZA', objDetalle.TAM_Serie__c);
    }
    
    @isTest
    public static void test_DuplicarPolitica_politica(){
        //Obtener información de pruebas.
        TAM_PoliticasIncentivos__c objPolitica = [SELECT 	Name,TAM_InicioVigencia__c,TAM_FinVigencia__c,TAM_No_Boletin__c,TAM_PeriodoDeGracia__c,RecordTypeId, RecordType.Name
                                                  FROM 		TAM_PoliticasIncentivos__c 
                                                  WHERE 	Name = 'Politica de Incentivos Vta Corporativa' LIMIT 1];
        
        List<TAM_DetallePoliticaJunction__c> lstDetalle = [SELECT TAM_AnioModelo__c,
                                                           TAM_Serie__c,
                                                           TAM_Version__c,
                                                           TAM_Clave__c,
                                                           TAM_Inventario_Dealer__c,
                                                           TAM_Dias_de_Inventario__c,
                                                           TAM_Venta_MY__c,
                                                           TAM_BalanceOut__c,
                                                           TAM_Pago_TFS__c,
                                                           TAM_IncPropuesto_Cash__c,
                                                           TAM_IncPropuesto_Porcentaje__c,
                                                           TAM_IncentivoTMEX_Cash__c,
                                                           TAM_IncentivoTMEX_Porcentaje__c,
                                                           TAM_IncDealer_Cash__c,
                                                           TAM_IncDealer_Porcentaje__c,
                                                           TAM_Tripleplay__c,
                                                           TAM_ProductoFinanciero__c,
                                                           TAM_Descripcion__c,
                                                           TAM_TotalEstimado__c,
                                                           TAM_TP_IncTMEX_Cash__c,
                                                           TAM_TP_IncTMEX_Porcentaje__c,
                                                           TAM_TP_IncDealer_Porcentaje__c,
                                                           TAM_TP_IncDealer_Cash__c,
                                                           TAM_IncTFS_Cash__c,
                                                           TAM_IncTFS_Porcentaje__c,
                                                           TAM_MSRP__c,
                                                           TAM_Duplicar__c,
                                                           TAM_TMEX_Margen_Pesos__c,
                                                           TAM_TMEX_Margen_Porcentaje__c,
                                                           TAM_Consecutivo__c,
                                                           TAM_Rango__c,
                                                           TAM_Rango__r.Name,
                                                           TAM_FechaInventario__c,
                                                           TAM_Lanzamiento__c,
                                                           TAM_MotivoIncentivo__c,
                                                           TAM_Incentivo_actual__c,
                                                           TAM_AplicanAmbosIncentivos__c,
                                                           TAM_IdExterno__c,
                                                           TAM_DuplicarDetalle__c
                                                           FROM TAM_DetallePoliticaJunction__c WHERE TAM_Clave__c = '2205'];

        
        
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Id, Serie_AnioModelo__c, Modelo__c, AnioModelo__c, Serie__c, Version__c 
                                                                        FROM 	CatalogoCentralizadoModelos__c ];
        CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
        objCatalogo.Name = 'AVANZA-2205-2021-1E7-10-LE MT';
        objCatalogo.Serie__c = 'AVANZA';
        objCatalogo.AnioModelo__c = '2021';
        objCatalogo.Modelo__c = '2205';
        objCatalogo.Version__c = 'LE MT';
        lstCatalogoCentralizado.add(objCatalogo);
        upsert lstCatalogoCentralizado;
        
        test.startTest();
        TAM_DuplicarPolitica.politica(objPolitica.Id);
        List<TAM_DetallePoliticaJunction__c> lstDetalleOriginal = TAM_DuplicarPolitica.getDetalleOriginal(objPolitica.Id);
        TAM_DuplicarPolitica.clonarDetalle(lstDetalleOriginal, objPolitica);
        test.stopTest();
        TAM_DetallePoliticaJunction__c objDetalle = [SELECT TAM_Serie__c FROM TAM_DetallePoliticaJunction__c WHERE TAM_AnioModelo__c = '2021' AND TAM_Clave__c = '2205' LIMIT 1];
        System.assertEquals('AVANZA', objDetalle.TAM_Serie__c);
        
    }
}