@isTest 
public class TAM_BatchClassUltimoMov_Test 
{
    static testMethod void testMethod1() 
    {
        
        List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
        Account a01 = new Account(
            RecordTypeId = rtDealer.get(0).Id,
            Name = 'Dealer',
            Codigo_Distribuidor__c = '12345'
        );
        insert a01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        
        
        Vehiculo__c vin = new Vehiculo__c();
        vin.Name ='MR2B29F38M1222917';
        
        
        insert vin;
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = vin.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA'                      
        );
        insert m01;
        
        
         Movimiento__c m02 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = vin.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA2',
            Last_Name__c = 'PRUEBA2',
            First_Name_Add__c = 'PRUEBA2',
            Last_Name_Add__c = 'PUREBA2'                      
        );
        insert m02;
        
        
        
        Test.startTest();
        
        TAM_BatchClassUltimoMov obj = new TAM_BatchClassUltimoMov();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
    }
}