global class BatchRepo3m implements Database.Batchable<sObject>,Database.Stateful{
	
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
        	'SELECT Id,Name,fwy_Segmento__c,fwy_Subsegmento__c FROM fwy_ReporteComunidades__c WHERE (fwy_Segmento__c = \'Prospectos\' AND fwy_Subsegmento__c=\'Incompletos y Condicionados\')'
        );
    }
    global void execute(Database.BatchableContext bc,List<fwy_ReporteComunidades__c>ReporteComunidad3m){
        Database.delete(ReporteComunidad3m,false);
    }
    global void finish(Database.BatchableContext bc){
        
    }
}