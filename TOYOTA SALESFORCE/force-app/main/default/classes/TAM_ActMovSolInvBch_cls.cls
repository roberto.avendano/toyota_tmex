/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_MovimientosSolicitudes__c
                        y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActMovSolInvBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActMovSolInvBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActMovSolInvBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    //global void execute(Database.BatchableContext BC, List<Movimiento__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
    global void execute(Database.BatchableContext BC, List<TAM_CheckOutDetalleSolicitudCompra__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
    //global void execute(Database.BatchableContext BC, List<TAM_FechasProceso__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>

        System.debug('EN TAM_ActMovSolInvBch_cls.');

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        String sRectorTypeCheckOutInvGeneral = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        //Crea la lista para poder eliminar los reg de ClientesPaso__c
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapFechaDDUpd = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, Set<String>> mapIdVinReg = new Map<String, Set<String>>();
        Set<String> setDodCons = new Set<String>();
        Set<String> setDodAsig = new Set<String>();
        Set<String> setCheckOutAsig = new Set<String>();
        Set<String> setDodAsigNoExiste = new Set<String>();
        Map<String, DateTime> mapVinesVtas = new map<String, DateTime>();
        Map<String, TAM_CheckOutDD__c> mapCheckOutDDUpsSinMov = new Map<String, TAM_CheckOutDD__c>();
        Map<String, TAM_CheckOutDD__c> mapCheckOutDDUpsFinal = new Map<String, TAM_CheckOutDD__c>();

        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdObjCheckOut = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, String> mapVinObjVehic = new Map<String, String>();
        Map<String, Vehiculo__c> mapVinVehicCons = new Map<String, Vehiculo__c>();
        Set<String> setCheckVinCons = new Set<String>();
        
        //Un Objero para el error en caso de exista
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        Set<String> setIdCheckout = new Set<String>();
        
        String sHabilitaMergeAccounts = System.Label.TAM_FechaMesToyota;
        //Date dFechaConsulta = sHabilitaMergeAccounts != 'null' ? Date.valueOf(sHabilitaMergeAccounts) : null;
        Date dFechaConsulta = sHabilitaMergeAccounts != 'null' ? Date.valueOf(sHabilitaMergeAccounts) : Date.today();
        System.debug('EN TAM_ActMovSolInvBch_cls dFechaConsulta: ' + dFechaConsulta);
        
        Date dtFechsIniNoInv;
        Date dFechaFinConsultaMov;
        if (dFechaConsulta != null)
            dFechaFinConsultaMov = dFechaConsulta;
        if (dFechaConsulta == null)
            dFechaConsulta = Date.today();
        System.debug('EN TAM_ActMovSolInvBch_cls dFechaConsulta2: ' + dFechaConsulta);            
        //Busca en el objeto de TAM_CalendarioToyota__c el rango al que le corresponda
        for (TAM_CalendarioToyota__c objCalToy : [Select id, TAM_FechaInicio__c, TAM_FechaFin__c 
            From TAM_CalendarioToyota__c Where TAM_FechaInicio__c <= :dFechaConsulta 
            And TAM_FechaFin__c >= :dFechaConsulta]){
            dtFechsIniNoInv = objCalToy.TAM_FechaInicio__c;    
            if (dFechaFinConsultaMov == null)
                dFechaFinConsultaMov = objCalToy.TAM_FechaFin__c;
        }
        //Es una prueba
        if (Test.isRunningTest()){
            dFechaFinConsultaMov = Date.today();
            dtFechsIniNoInv = Date.today();
        }//Fin si Test.isRunningTest()
        System.debug('EN TAM_ActMovSolInvBch_cls dtFechsIniNoInv: ' + dtFechsIniNoInv + ' dFechaFinConsultaMov: ' + dFechaFinConsultaMov);
        String strdtFechsIniNoInv = String.valueOf(dtFechsIniNoInv);
        String strdFechaFinConsultaMov = String.valueOf(dFechaFinConsultaMov);
        System.debug('EN TAM_ActMovSolInvBch_cls strdtFechsIniNoInv: ' + strdtFechsIniNoInv + ' strdFechaFinConsultaMov: ' + strdFechaFinConsultaMov);
        
        Set<String> setVinMov = new Set<String>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
                
        Date dtFechaActual = Date.today();
        Map<String, TAM_MovimientosSolicitudes__c> mapMovimientosSolicitudesUps = new Map<String, TAM_MovimientosSolicitudes__c>();
        Map<String, Map<Date, TAM_MovimientosSolicitudes__c>> MapVinFechaMovObjMovSol = new Map<String, Map<Date, TAM_MovimientosSolicitudes__c>>();

        //Recorre el mapa de mapIdVinReg 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckoutPaso : scope){
             System.debug('EN TAM_ActMovSolInvBch_cls1 objCheckoutPaso: ' + objCheckoutPaso);

             //Ya tienes todos los mov del objCheckoutPaso.TAM_VIN__c busca los registros en la table de TAM_MovimientosSolicitudes__c
            for (TAM_MovimientosSolicitudes__c objMovSolPaso : [Select t.id, t.TAM_VIN__c, 
                t.TAM_UltimoMovimientoDD__c, 
                t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
                From TAM_MovimientosSolicitudes__c t Where TAM_VIN__c =:objCheckoutPaso.TAM_VIN__c ]){
                //Mete los mov al mapa de MapVinFechaMovObjMovSol
                if (MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)){
                    //Agregarlo al mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                    MapVinFechaMovObjMovSol.get(objMovSolPaso.TAM_VIN__c).put(objMovSolPaso.TAM_FechaMovimiento__c, objMovSolPaso);
                }//Fin si MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)
                //No exite el objMovSolPaso.TAM_VIN__c en MapVinFechaMovObjMovSol
                if (!MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c))
                   MapVinFechaMovObjMovSol.put(objMovSolPaso.TAM_VIN__c, new Map<Date, TAM_MovimientosSolicitudes__c>{objMovSolPaso.TAM_FechaMovimiento__c => objMovSolPaso});                
            }//Fin del for para TAM_MovimientosSolicitudes__c
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.KeySet());
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.Values());
                                                                            
            //Ve la fecha de cancelación
            if (objCheckoutPaso.TAM_EstatusDOD__c == 'Cancelado' || objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cancelada' 
                || objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Rechazada' || Test.isRunningTest()){
                //Crea la fecha de rango
                Date dtFechaIni = objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c;
                Date dtFechaFin;
                //Para la prueba 
                if (Test.isRunningTest())
                    dtFechaFin = Date.today().addDays(1);
                else
                    dtFechaFin = objCheckoutPaso.TAM_FechaCancelacion__c;
                if (objCheckoutPaso.TAM_FechaCancelacion__c == null){
                    lError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'Solicitud Sin fecha Cancelación',
                          TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'Sin  fecha cancela VIN: ' + objCheckoutPaso.TAM_VIN__c + ' Folio sol: ' + objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name
                       )
                    );
                }
                //Si es na prueba
                if (Test.isRunningTest()){ 
                    objCheckoutPaso.TAM_FechaCancelacion__c = Date.today();
                    dtFechaIni = Date.today();
                }
                //Ya tiene fecha de cancelación
                if (objCheckoutPaso.TAM_FechaCancelacion__c != null){
                    //Es una prueba
                    if (Test.isRunningTest()){
                        dtFechaFin = Date.today();
                        dtFechaIni = Date.today();
                    }
                    Integer intNoDias = dtFechaIni.daysBetween(dtFechaFin);
                    System.debug('EN TAM_ActMovSolInvBch_cls0 intNoDias: ' + intNoDias + ' dtFechaIni: ' + dtFechaIni + ' dtFechaFin: ' + dtFechaFin);
                    if (intNoDias > 0){
                        System.debug('EN TAM_ActMovSolInvBch_cls1 intNoDias: ' + intNoDias + ' dtFechaIni: ' + dtFechaIni + ' dtFechaFin: ' + dtFechaFin);
                        //Recorre la lista de los dias de dtFechaAct
                        for (Integer intCnt = 0; intCnt <= intNoDias; intCnt++){
                            String strDtFechaCheckOut = String.valueOf(dtFechaIni);                    
                            //Crea el id externo para el objeto de TAM_CheckOutDD__c
                            String sIdExterno = objCheckoutPaso.TAM_VIN__c + '-' + objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                
                            String sEstatusSol = 'Cancelado';
                            if (dtFechaIni < dtFechaFin) //dtFechaFinUpd
                                sEstatusSol = 'Autorizada';
                            if (dtFechaIni == dtFechaFin) //dtFechaFinUpd
                                sEstatusSol = 'Cancelado';                                         
                            System.debug('EN TAM_ActMovSolInvBch_cls2 CANCELADA sIdExterno: ' + sIdExterno + ' sEstatusSol: ' + sEstatusSol);                            
                            TAM_MovimientosSolicitudes__c objMovSolPaso = new TAM_MovimientosSolicitudes__c();
                            //Obten el total de acuerdo al mapa 
                            if (MapVinFechaMovObjMovSol.containsKey(objCheckoutPaso.TAM_VIN__c))
                                if (MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).containsKey(dtFechaIni))
                                    objMovSolPaso = MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).get(dtFechaIni);
                            System.debug('EN TAM_ActMovSolInvBch_cls2 CANCELADA objMovSolPaso: ' + objMovSolPaso);
                            //Metelo al mapa  
                            TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
                                Name = objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name + ' - ' + strDtFechaCheckOut,
                                TAM_IdExternoSFDC__c = sIdExterno,
                                TAM_EstatusSolicitud__c = sEstatusSol, //objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cerrada' ? 'Autorizada' : objCheckoutPaso.TAM_EstatusDealerSolicitud__c,
                                TAM_Total__c = objMovSolPaso.id != null ? objMovSolPaso.TAM_Total__c : 0,
                                TAM_FechaMovimiento__c = dtFechaIni,
                                TAM_VIN__c = objCheckoutPaso.TAM_VIN__c,
                                TAM_CheckOutSolicitud__c = objCheckoutPaso.id
                            );
                            System.debug('EN TAM_ActMovSolInvBch_cls3 CANCELADA objMovSol: ' + objMovSol);
                            //Metelo al mapa
                            mapMovimientosSolicitudesUps.put(sIdExterno, objMovSol);
                            //Sumale un dia a la fecha incial dtFechaCheckOut
                            Date dtDiasig = dtFechaIni.addDays(1);
                            dtFechaIni = Date.newInstance(dtDiasig.year(), dtDiasig.month(), dtDiasig.day());
                            System.debug('EN TAM_ActMovSolInvBch_cls4 CANCELADA dtFechaIni: ' + dtFechaIni);    
                        }//Fin del for para intCnt
                    }//Fin si intNoDias > 0
                    //La creo y la cancelo el mismo dia
                    if (intNoDias == 0){
                        String strDtFechaCheckOut = String.valueOf(dtFechaIni);                            
                        //Crea el id externo para el objeto de TAM_CheckOutDD__c
                        String sIdExterno = objCheckoutPaso.TAM_VIN__c + '-' + objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                
                        System.debug('EN TAM_ActMovSolInvBch_cls CANCELADA 1 sIdExterno: ' + sIdExterno);
                        TAM_MovimientosSolicitudes__c objMovSolPaso = new TAM_MovimientosSolicitudes__c();
                        //Obten el total de acuerdo al mapa 
                        if (MapVinFechaMovObjMovSol.containsKey(objCheckoutPaso.TAM_VIN__c))
                            if (MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).containsKey(dtFechaIni))
                                objMovSolPaso = MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).get(dtFechaIni);
                        System.debug('EN TAM_ActMovSolInvBch_cls2 CANCELADA 1 objMovSolPaso: ' + objMovSolPaso);                     
                        //Metelo al mapa  
                        TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
                            Name = objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name + ' - ' + strDtFechaCheckOut,
                            TAM_IdExternoSFDC__c = sIdExterno,
                            TAM_EstatusSolicitud__c = objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cerrada' ? 'Autorizada' : objCheckoutPaso.TAM_EstatusDealerSolicitud__c,
                            TAM_Total__c = objMovSolPaso.id != null ? objMovSolPaso.TAM_Total__c : 0,
                            TAM_FechaMovimiento__c = dtFechaFin,
                            TAM_VIN__c = objCheckoutPaso.TAM_VIN__c,
                            TAM_CheckOutSolicitud__c = objCheckoutPaso.id
                        );
                        System.debug('EN TAM_ActMovSolInvBch_cls CANCELADA objMovSol: ' + objMovSol);
                        //Metelo al mapa
                        mapMovimientosSolicitudesUps.put(sIdExterno, objMovSol);                            
                    }//Fin si intNoDias == 0
                }//Fin si objCheckoutPaso.TAM_FechaCancelacion__c == null
            }//Fin si objCheckoutPaso.TAM_EstatusDOD__c == 'Cancelado' || objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cancelada'
                
            //Ve la fecha de cancelación
            if (objCheckoutPaso.TAM_EstatusDOD__c != 'Cancelado' && objCheckoutPaso.TAM_EstatusDealerSolicitud__c != 'Cancelada' && objCheckoutPaso.TAM_EstatusDealerSolicitud__c != 'Rechazada'){
                //Crea la fecha de rango
                Date dtFechaIni = objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c;
                Date dtFechaFin = Date.today();
                Integer intNoDias = dtFechaIni.daysBetween(dtFechaFin);
                System.debug('EN TAM_ActMovSolInvBch_cls NO CANCELADA intNoDias: ' + intNoDias + ' dtFechaIni: ' + dtFechaIni + ' dtFechaFin: ' + dtFechaFin);
                //Recorre la lista de los dias de dtFechaAct
                for (Integer intCnt = 0; intCnt <= intNoDias; intCnt++){
                    String strDtFechaCheckOut = String.valueOf(dtFechaIni);                    
                    //Crea el id externo para el objeto de TAM_CheckOutDD__c
                    String sIdExterno = objCheckoutPaso.TAM_VIN__c + '-' + objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                
                    String sEstatusSol = 'Autorizada';
                    System.debug('EN TAM_ActMovSolInvBch_cls NO CANCELADA sIdExterno: ' + sIdExterno + ' sEstatusSol: ' + sEstatusSol);
                    TAM_MovimientosSolicitudes__c objMovSolPaso = new TAM_MovimientosSolicitudes__c();
                    //Obten el total de acuerdo al mapa 
                    if (MapVinFechaMovObjMovSol.containsKey(objCheckoutPaso.TAM_VIN__c))
                        if (MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).containsKey(dtFechaIni))
                            objMovSolPaso = MapVinFechaMovObjMovSol.get(objCheckoutPaso.TAM_VIN__c).get(dtFechaIni);
                    System.debug('EN TAM_ActMovSolInvBch_cls2 CANCELADA 1 objMovSolPaso: ' + objMovSolPaso);
                    //Metelo al mapa  
                    TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
                        Name = objCheckoutPaso.TAM_SolicitudFlotillaPrograma__r.Name + ' - ' + strDtFechaCheckOut,
                        TAM_IdExternoSFDC__c = sIdExterno,
                        TAM_EstatusSolicitud__c = objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cerrada' ? 'Autorizada' : objCheckoutPaso.TAM_EstatusDealerSolicitud__c,
                        TAM_Total__c = objMovSolPaso.id != null ? objMovSolPaso.TAM_Total__c : 0,
                        TAM_FechaMovimiento__c = dtFechaIni,                                    
                        TAM_VIN__c = objCheckoutPaso.TAM_VIN__c,
                        TAM_CheckOutSolicitud__c = objCheckoutPaso.id                        
                    );
                    System.debug('EN TAM_ActMovSolInvBch_cls NO CANCELADA objMovSol: ' + objMovSol);
                    //Metelo al mapa
                    mapMovimientosSolicitudesUps.put(sIdExterno, objMovSol);
                    //Sumale un dia a la fecha incial dtFechaCheckOut
                    Date dtDiasig = dtFechaIni.addDays(1);
                    dtFechaIni = Date.newInstance(dtDiasig.year(), dtDiasig.month(), dtDiasig.day());
                    System.debug('EN TAM_ActMovSolInvBch_cls NO CANCELADA dtFechaIni: ' + dtFechaIni);    
                }//Fin del for para intCnt                    
            }//Fin si objCheckoutPaso.TAM_EstatusDOD__c == 'Cancelado' || objCheckoutPaso.TAM_EstatusDealerSolicitud__c == 'Cancelada'
                                
        }//Fin del for para los Vines
        
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        //Ve si tiene datos en mapMovimientosSolicitudesUps
        if (!mapMovimientosSolicitudesUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapMovimientosSolicitudesUps.values(), TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActMovSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapMovimientosSolicitudesUps.isEmpty()
        
        //Roleback a todo
        //Database.rollback(sp);

        System.debug('EN TAM_ActMovSolInvBch_cls mapFechaDDUpd: ' + mapFechaDDUpd.keySet());
        System.debug('EN TAM_ActMovSolInvBch_cls mapFechaDDUpd: ' + mapFechaDDUpd.values());
        System.debug('EN TAM_ActMovSolInvBch_cls mapCheckOutDDUpsFinal: ' + mapCheckOutDDUpsFinal.keySet());
        System.debug('EN TAM_ActMovSolInvBch_cls mapCheckOutDDUpsFinal: ' + mapCheckOutDDUpsFinal.values());
        System.debug('EN TAM_ActMovSolInvBch_cls mapCheckOutDDUpsSinMov: ' + mapCheckOutDDUpsSinMov.keySet());
        System.debug('EN TAM_ActMovSolInvBch_cls mapCheckOutDDUpsSinMov: ' + mapCheckOutDDUpsSinMov.values());
        System.debug('EN TAM_ActMovSolInvBch_cls setCheckVinCons: ' + setCheckVinCons);  
                    
    }   
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActMovSolInvBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}