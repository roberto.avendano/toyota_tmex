@isTest
public class SolicitudSiteLoginControllerTest {
    
    @isTest
    static void test_one(){
        Date inicio = date.parse('01/03/2018');
        Date fin = date.parse('31/03/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='ToyotaGroup'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='TMEX'];
        
        Contact c1 = new Contact(AccountId= acc.Id, 
                                 LastName= 'Contact1 TMEX Test', 
                                 Departamento__c = 'Human Resources',
                                 Codigo__c = '456RTY',
                                 Puesto__c	= 'Chief Coordinating Officer',
                                 PuestoEmpleado__c = paa.Id,
                                 Email ='aramos@grupoassa.com', 
                                 RecordTypeId = conRT.Id);
        
        Contact c2 = new Contact(AccountId= acc.Id, 
                                 LastName= 'Contact2 TMEX Test', 
                                 Departamento__c = 'Human Resources',
                                 Codigo__c = '456RZY',
                                 Puesto__c	= 'Chief Coordinating Officer',
                                 PuestoEmpleado__c = paa.Id,
                                 Email ='aramo2s@grupoassa.com', 
                                 RecordTypeId = conRT.Id);
        Contact c3 = new Contact(AccountId= acc.Id, 
                                 LastName= 'Contact3 TMEX Test', 
                                 Departamento__c = 'Human Resources',
                                 Codigo__c = '456RWM',
                                 Puesto__c	= 'Chief Coordinating Officer',
                                 PuestoEmpleado__c = paa.Id,
                                 Email ='aramos10@grupoassa.com', 
                                 RecordTypeId = conRT.Id);
        List<Contact> contacts = new List<Contact>{c1,c2,c3};
            
            insert contacts;
        
        ContactoSite__c cs1 = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                               FROM ContactoSite__c WHERE Contacto__c =:contacts[0].Id];
        cs1.Activo__c = true;
        cs1.Contrasena__c = '12345';
        
        ContactoSite__c cs2 = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                               FROM ContactoSite__c WHERE Contacto__c =:contacts[1].Id];
        
        ContactoSite__c cs3 = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                               FROM ContactoSite__c WHERE Contacto__c =:contacts[1].Id];
        
        cs3.Activo__c = false;
        try{
            update cs1;
            update cs3;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        test.startTest();
        
        PageReference page = System.Page.SolicitudSiteLogin;
        Test.setCurrentPage(page);
        SolicitudSiteLoginController ssl1 = new SolicitudSiteLoginController(new ApexPages.standardController(cs1));
        ssl1.username = 'aramos@grupoassa.com';
        ssl1.password = '12345';
        ssl1.Login();
        ssl1.register();
        ssl1.olvidoPassword();
        SolicitudSiteLoginController ssl2 = new SolicitudSiteLoginController(new ApexPages.standardController(cs2));
        ssl2.username = 'aramos@grupoassa.com';
        ssl2.password = '123';
        ssl2.Login();
        ssl2.register();
        ssl2.olvidoPassword();
        SolicitudSiteLoginController ssl3 = new SolicitudSiteLoginController(new ApexPages.standardController(cs2));
        ssl3.username = 'aramos5@grupoassa.com';
        ssl3.password = '123';
        ssl3.Login();
        ssl3.register();
        ssl3.olvidoPassword();
        SolicitudSiteLoginController ssl4 = new SolicitudSiteLoginController(new ApexPages.standardController(cs3));
        ssl4.username = 'aramos10@grupoassa.com';
        ssl4.password = '123';
        ssl4.Login();
        ssl4.register();
        ssl4.olvidoPassword();
        test.stopTest();
    }
    
}