@isTest
public class SolicitudSiteOlvidoPassControllerTest {
    
    @isTest
    static void test_one(){
        Date inicio = date.parse('01/03/2018');
        Date fin = date.parse('31/03/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='ToyotaGroup'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='TMEX'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Departamento__c = 'Human Resources',
                                Codigo__c = '456RTY',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                RecordTypeId = conRT.Id);
        insert c;
        
        ContactoSite__c cs = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                               FROM ContactoSite__c WHERE Contacto__c =:c.Id];
        cs.UsuarioRegistrado__c = true;
        cs.OlvidoContrasena__c = true;
        
        try{
            update cs;
        }catch(Exception e){
            System.debug('Eror:' +e.getMessage());
        }
        
        PageReference page = System.Page.SolicitudSiteOlvidoPassword;
        Test.setCurrentPage(page);
        SolicitudSiteOlvidoPasswordController ssopc1 = new SolicitudSiteOlvidoPasswordController(new ApexPages.standardController(cs));
        ssopc1.username = 'aramos@grupoassa.com';
        ssopc1.actualizarPassword(String.valueOf(cs.Id));
        ssopc1.cambiarPassword();
        
        SolicitudSiteOlvidoPasswordController ssopc2 = new SolicitudSiteOlvidoPasswordController(new ApexPages.standardController(cs));
        ssopc2.username = 'aramos2@grupoassa.com';
        ssopc2.actualizarPassword(String.valueOf(cs.Id));
        ssopc2.cambiarPassword();
    }
}