/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActNomKbanLeadBch_tst {

    static String VaRtLeadRetailNuevos = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Nuevos').getRecordTypeId();
    static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
    static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
    static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
    static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
    static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  Lead candidatoPrueba = new Lead();

    @TestSetup static void loadData(){

        Account clienteMoral = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '57039',           
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            TAM_TokenSFDC__c = '4cb2035d-4be2-66f6-af82-57981714bcc0'
            
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'TestAccount',
            LastName = 'TestAccountLastName',
            Codigo_Distribuidor__c = '570551',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización'
        );
        insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  
        
        Lead cand = new Lead(
            RecordTypeId = VaRtLeadRetailNuevos,
            FirstName = 'Test12',
            FWY_Intencion_de_compra__c = 'Este mes',    
            Email = 'aw@a.com',
            phone = '5554565432',
            Status='Pedido en Proceso',
            LastName = 'Test3',
            FWY_Tipo_de_persona__c = 'Person física',
            TAM_TipoCandidato__c = 'Retail Nuevos',
            TAM_DatosValidosFacturacion__c = true,
            FWY_codigo_distribuidor__c = '57039'      
        );
        insert cand;
                                
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_ActNomKbanLeadBchOK() {
        
        Test.startTest();
        
            //Consulta el lead que se acaba de crear
            Lead objLeadCons = [select id, name, FirstName, LastName, FWY_codigo_distribuidor__c From Lead Where FirstName = 'Test12' LIMIT 1];
            //Actualizalo 
            objLeadCons.FWY_codigo_distribuidor__c = '57030';
            update objLeadCons;
            System.debug('EN TAM_ActNomKbanLeadBchOK.execute objLeadCons: ' + objLeadCons);

            Leadhistory lh = new Leadhistory(Field='FWY_codigo_distribuidor__c',LeadId = objLeadCons.id);
            insert lh;
            System.debug('EN TAM_ActNomKbanLeadBchOK.execute lh: ' + lh);

            String sHoraZonaHoraria = TAM_ActCteVehicSch_cls.getHoraZonaHoraria();
            System.debug('EN TAM_ActNomKbanLeadBchOK.execute sHoraZonaHoraria: ' + sHoraZonaHoraria);
        
            //Para la fecha Ini        
            DateTime dtFechaCreacIni = DateTime.now().addDays(-2);
            DateTime dtFechaCreacIni2 = dtFechaCreacIni.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacIni;
            String sdtFechaCreacIni = String.valueOf(dtFechaCreacIni2);
            String sdtFechaCreacIniFinal = sdtFechaCreacIni.replace(' ', 'T') + 'Z';
            //Para la fecha Fin
            DateTime dtFechaCreacFin = DateTime.now();
            DateTime dtFechaCreacFin2 = dtFechaCreacFin.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCreacFin;
            String sdtFechaCreacFin = String.valueOf(dtFechaCreacFin2);
            String sdtFechaCreacFinFinal = sdtFechaCreacFin.replace(' ', 'T') + 'Z';
            System.debug('EN TAM_ActNomKbanLeadBchOK.execute sdtFechaCreacIniFinal: ' + sdtFechaCreacIniFinal + ' sdtFechaCreacFinFinal: ' + sdtFechaCreacFinFinal);
                
	        String sQuery = 'Select Lead.TAM_CodDistribuidorUsr__c, Lead.FWY_codigo_distribuidor__c,';
            sQuery += ' Lead.TAM_NomPropUsr__c, Lead.TAM_NomPropKban__c, LeadId, Lead.FWY_Distribuidor_f__c,';
            sQuery += ' Lead.Distribuidor_para_cuenta__c';
	        sQuery += ' From LeadHistory l';
	        sQuery += ' Limit 1';
            System.debug('EN TAM_ActNomKbanLeadBchOK.execute sQuery: ' + sQuery);
            
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActNomKbanLeadBch_cls objActNomKbanLeadBch = new TAM_ActNomKbanLeadBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActNomKbanLeadBch, 1);

            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_ActNomKbanLeadSch_cls objActNomKbanLeadSch_Jobh = new TAM_ActNomKbanLeadSch_cls();
            System.schedule('Ejecuta_TAM_ActNomKbanLeadSch_cls', sch, objActNomKbanLeadSch_Jobh);
            
        Test.stopTest();
        
    }

}