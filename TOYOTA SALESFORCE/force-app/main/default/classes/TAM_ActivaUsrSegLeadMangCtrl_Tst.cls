/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActivaUsrSegLeadMangCtrl_Tst {

	static	Account Distrib = new Account();
	
	@TestSetup static void loadData(){
        Test.startTest();
        
	        Id p = [select id from profile where name='Partner Community User'].id;
	        
	        Account ac = new Account(
	            name ='TOYOTA POLANCO',
	            Codigo_Distribuidor__c = '57000',
	            TAM_GestorLeads__c = UserInfo.getUserId()
	        );	            
	        insert ac; 
	        	        
	        Contact con = new Contact(
	            LastName ='testCon',            
	            AccountId = ac.Id
	        );
	        insert con;  
	        
	        User user = new User(
	            alias = 'test123p', 
	            email='test123tfsproduc@noemail.com',
	            Owner_Candidatos__c= true,
	            emailencodingkey='UTF-8', 
                firstname='Testingproductfs', 
	            lastname='Testingproductfs', 
	            languagelocalekey='en_US',
	            localesidkey='en_US', 
	            profileid = p, 
	            country='United States',
	            IsActive =true,
	            ContactId = con.Id,
	            //AccountId = ac.id,    
	            timezonesidkey='America/Los_Angeles', 
	            username='testOwnertfsproduc@noemail.com.test'
	        );
	        insert user;
        
	        TAM_MaximoNumeroLeads__c testMaxNumLead = new TAM_MaximoNumeroLeads__c();
	        testMaxNumLead.TAM_CodigoDealer__c = '57000';
	        testMaxNumLead.Id_Externo__c = '57000';
	        testMaxNumLead.TAM_MaximoLeads__c = '20';
			insert testMaxNumLead;
       
            TAM_RedireccionaLeads__c testRedirectUsr = new TAM_RedireccionaLeads__c();
            testRedirectUsr.Name = 'Testingproductfs Testingproductfs';
            testRedirectUsr.TAM_CodigoDealerOrgen__c = '57000';
            testRedirectUsr.TAM_IdExterno__c = '57000 - 57000';
            testRedirectUsr.TAM_Activo__c = true;
            testRedirectUsr.TAM_Principal__c = true;
            testRedirectUsr.TAM_UsuariosRelacionados__c = UserInfo.getUserId() + ';';
            insert testRedirectUsr;       

            TAM_UsuariosSeguimientoLead__c testUsrSegLead = new TAM_UsuariosSeguimientoLead__c();
            testUsrSegLead.Name = 'Testingproductfs Testingproductfs';
            testUsrSegLead.TAM_NoDistribuidor__c = '57000';
            testUsrSegLead.TAM_IdExterno__c = 'Testingproductfs - 57000';
            testUsrSegLead.TAM_Activo__c = true;
            testUsrSegLead.TAM_ActivoSFDC__c = true;
            testUsrSegLead.TAM_NombreCompleto__c = 'Testingproductfs Testingproductfs'; 
            testUsrSegLead.TAM_NombreDistribuidor__c = 'TOYOTA POLANCO';  
            testUsrSegLead.TAM_IdUsuarioPartner__c = UserInfo.getUserId();
            insert testUsrSegLead;  
                 
        Test.stopTest();

	}

    static testMethod void TAM_ActivaUsrSegLeadMangCtrlOK() {
    	System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK...');
		
		Distrib = [Select id, Codigo_Distribuidor__c, TAM_GestorLeads__c From Account Limit 1];
    	System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK Distrib: ' + Distrib);
		
		//Llama al metodo getNoDistlUserActual
		TAM_ActivaUsrSegLeadMangCtrl.getNoDistlUserActual();
		//Llama al metodo getUsuarosDealer		
		TAM_ActivaUsrSegLeadMangCtrl.getUsuarosDealer(Distrib.Codigo_Distribuidor__c);
		
		//Crea un objeto del tipo TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist
		TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist objWrpListaUsuariosDist = 
			new TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist('Testingproductfs Testingproductfs','Usuario Pruba', 
			'pruebauser@hotmail.com', 'Gestor Leads', 'Gestor Leads', true, UserInfo.getUserId(), 100, 100, 100);
		TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist objWrpListaUsuariosDist2 = new TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist();
		//Una lista del tipo TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist
		List<TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist>	lObjWrpListaUsuariosDist = 
			new List<TAM_ActivaUsrSegLeadMangCtrl.wrpListaUsuariosDist>{objWrpListaUsuariosDist};
    	System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK antes de AcctUsrSegLead lObjWrpListaUsuariosDist: ' + lObjWrpListaUsuariosDist);

        //Un objeto del tipo TAM_ActivaUsrSegLeadMangCtrl.wrpDelaer
        TAM_ActivaUsrSegLeadMangCtrl.wrpDelaer objWrpDelaer = 
            new TAM_ActivaUsrSegLeadMangCtrl.wrpDelaer('57000', 'TOYOTA POLANCO');
        //Una lista del tipo objWrpDelaer
        List<TAM_ActivaUsrSegLeadMangCtrl.wrpDelaer> lObjWrpDelaer = 
            new List<TAM_ActivaUsrSegLeadMangCtrl.wrpDelaer>{objWrpDelaer};
        
        //Un objeto del tipo wrpDelaerUsruario
        TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerUsruario objWrpDelaerUsruario = 
            new TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerUsruario();
        
        //Un objto del tipo wrpDelaerRel
        TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerRel objWrpDelaerRel = 
            new TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerRel(objWrpDelaer, lObjWrpListaUsuariosDist);
       
        //Una lista del tipo List<TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerRel>
        List<TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerRel> lWrpDealerRel = 
            new List<TAM_ActivaUsrSegLeadMangCtrl.wrpDelaerRel>{objWrpDelaerRel};
            
        objWrpDelaerUsruario.lDealers = lObjWrpDelaer;
        objWrpDelaerUsruario.lUsuarios = lObjWrpListaUsuariosDist;
        objWrpDelaerUsruario.lUsuariosRedirect = lObjWrpListaUsuariosDist;
        objWrpDelaerUsruario.objWrpDealerRedirect = new TAM_ActivaUsrSegLeadMangCtrl.wrpDealerRedirect();
        objWrpDelaerUsruario.blnLstReasigLeads = true;
        objWrpDelaerUsruario.blnGestorLeads = true;
        objWrpDelaerUsruario.lWrpDealerRel =  lWrpDealerRel;
                            
        //Lalama al metod de  clsGetListaDealerRedirect
        TAM_ActivaUsrSegLeadMangCtrl.clsGetListaDealerRedirect(UserInfo.getUserId());
		//Lalama al metod de  AcctUsrSegLead
        TAM_ActivaUsrSegLeadMangCtrl.clsAcctUsrSegLead(Distrib.Codigo_Distribuidor__c, lObjWrpListaUsuariosDist, '57000', objWrpDelaerUsruario);
        
    }
    
     static testMethod void TAM_ActivaUsrSegLeadMangCtrlOK2() {
        User usr = [Select id from User Where email='test123tfsproduc@noemail.com'];
		TAM_ActivaUsrSegLeadMangCtrl.consultaMaximoLeads(usr.id);
        TAM_ActivaUsrSegLeadMangCtrl.seteaValorMaximoLeads(usr.id,100);
    }
    
}