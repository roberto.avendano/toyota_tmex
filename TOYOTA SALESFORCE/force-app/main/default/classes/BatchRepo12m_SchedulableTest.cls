@isTest
private class BatchRepo12m_SchedulableTest
{    
    static testmethod void schedulerTest() {
        String CRON_EXP = '0 0 1 L 12 ? 2019-2200';
		List<fwy_ReporteComunidades__c> pruebalist = new List<fwy_ReporteComunidades__c>();      
        for(integer a=0;a<10;a++){
            fwy_ReporteComunidades__c ej1 = new fwy_ReporteComunidades__c(); 
            ej1.fwy_Segmento__c = 'Seguros';
            ej1.fwy_Subsegmento__c = 'Bono 5%';
            ej1.Name = 'Reporte1'+a;
            pruebalist.add(ej1);
      }
        for(integer b=0;b<10;b++){
            fwy_ReporteComunidades__c ej2 = new fwy_ReporteComunidades__c(); 
            ej2.fwy_Segmento__c = 'Clientes';
            ej2.fwy_Subsegmento__c = 'Comisiones';
            ej2.Name = 'Reporte2'+b;
            pruebalist.add(ej2);
        }
         for(integer c=0;c<10;c++){
            fwy_ReporteComunidades__c ej3 = new fwy_ReporteComunidades__c(); 
            ej3.fwy_Segmento__c = 'Dealer';
            ej3.fwy_Subsegmento__c = 'Eficiencia';
            ej3.Name = 'Reporte3'+c;
            pruebalist.add(ej3);
        }
         for(integer d=0;d<10;d++){
            fwy_ReporteComunidades__c ej4 = new fwy_ReporteComunidades__c(); 
            ej4.fwy_Segmento__c = 'Dealer';
            ej4.fwy_Subsegmento__c = 'Premium WIP';
            ej4.Name = 'Reporte4'+d;
            pruebalist.add(ej4);
        } 
         for(integer e=0;e<10;e++){
            fwy_ReporteComunidades__c ej5 = new fwy_ReporteComunidades__c(); 
            ej5.fwy_Segmento__c = 'Plan Piso';
            ej5.fwy_Subsegmento__c = 'Control de Seguros Plan Piso';
            ej5.Name = 'Reporte3'+e;
            pruebalist.add(ej5);
        }
        insert pruebalist;        
        Test.StartTest();
        	
            BatchRepo12m_Schedulable job = new BatchRepo12m_Schedulable();
            System.schedule('ScheduleApexClassTest',CRON_EXP,job);
        	BatchRepo12m_Schedulable.scheduleit();
			BatchRepo6m_Schedulable.scheduleit();
			BatchRepo3m_Schedulable.scheduleit();
			BatchRepo1m_Schedulable.scheduleit();        
         
        Test.StopTest();
    }
}