/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_UsuariosSeguimientoLead__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Mayo-202          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActDatosUsrSegLeadSch_cls implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActDatosUsrSegLeadSch_cls.execute...');
        String strIdUsr = '0055d00000Bj5PQAAZ';
                
        this.sQuery = 'Select Id, Name, CodigoDistribuidor__c ';
        this.sQuery += ' From User ';
        this.sQuery += ' where CodigoDistribuidor__c != null and IsPortalEnabled = true ';
        //if (!Test.isRunningTest())
        //    this.sQuery += ' And ID = \'' + String.escapeSingleQuotes(strIdUsr) + '\'';
        this.sQuery += ' Order by id, Name';
        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActDatosUsrSegLeadSch_cls.execute sQuery: ' + sQuery);
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActDatosUsrSegLeadBch_cls objActDatosUsrSegLeadBch = new TAM_ActDatosUsrSegLeadBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActDatosUsrSegLeadBch, 25);
    }
    
}