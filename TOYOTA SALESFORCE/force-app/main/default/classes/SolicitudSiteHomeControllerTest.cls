@isTest
public class SolicitudSiteHomeControllerTest {
    
    @isTest
    static void testone(){
        Map<String,Map<String,RecordType>> recordTypesMap;
        recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        Date inicio = date.parse('01/04/2018');
        Date fin = date.parse('30/04/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='Dealer'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='TMEX'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Departamento__c = 'Human Resources',
                                Estatus__c = 'Activo',
                                Codigo__c = '456RTY',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                RecordTypeId = conRT.Id);
        insert c;
        
        SolicitudInternaVehiculos__c siv1 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolSolicitante').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv2 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolPlacas').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv3 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolLogistica').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv4 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolFacturacion').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv5 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolEntrega').Id,
                                                                             Estatus__c='Nuevo');
        /*
        SolicitudInternaVehiculos__c siv6 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraSolicitante').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv7 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraLogistica').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv8 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraFacturacin').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv9 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraEntrega').Id,
                                                                             Estatus__c='Nuevo');
        SolicitudInternaVehiculos__c siv10 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                              RecordTypeId = recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraPlanZero').Id,
                                                                              Estatus__c='Nuevo');
        
        */
        List<SolicitudInternaVehiculos__c> sivs = new List<SolicitudInternaVehiculos__c>{siv1,siv2,siv3,siv4,siv5}; //,siv6,siv7,siv8,siv9,siv10
            insert sivs;
        
        ContactoSite__c cs = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c 
                               FROM ContactoSite__c WHERE Contacto__c =:c.Id];
        
        test.startTest();
        PageReference page = System.Page.SolicitudSiteHome;
        Test.setCurrentPage(page);
        SolicitudSiteHomeController sshic1 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic1.solicitudID = sivs[0].Id;
        sshic1.ligtningMode = true;
        sshic1.inicio();
        sshic1.abrirSolicitud();
        sshic1.getSolicitudes();
        SolicitudSiteHomeController sshic2 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic2.solicitudID = sivs[1].Id;
        sshic2.inicio();
        sshic2.abrirSolicitud();
        SolicitudSiteHomeController sshic3 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic3.solicitudID = sivs[2].Id;
        sshic3.inicio();
        sshic3.abrirSolicitud();
        SolicitudSiteHomeController sshic4 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic4.solicitudID = sivs[3].Id;
        sshic4.inicio();
        sshic4.abrirSolicitud();
        SolicitudSiteHomeController sshic5 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic5.solicitudID = sivs[4].Id;
        sshic5.inicio();
        sshic5.abrirSolicitud();
/*
        SolicitudSiteHomeController sshic6 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic6.solicitudID = sivs[5].Id;
        sshic6.inicio();
        sshic6.abrirSolicitud();
        SolicitudSiteHomeController sshic7 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic7.solicitudID = sivs[6].Id;
        sshic7.inicio();
        sshic7.abrirSolicitud();
        SolicitudSiteHomeController sshic8 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic8.solicitudID = sivs[7].Id;
        sshic8.inicio();
        sshic8.abrirSolicitud();
        SolicitudSiteHomeController sshic9 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic9.solicitudID = sivs[8].Id;
        sshic9.inicio();
        sshic9.abrirSolicitud();
        SolicitudSiteHomeController sshic10 = new SolicitudSiteHomeController(new ApexPages.standardController(cs));
        sshic10.solicitudID = sivs[9].Id;
        sshic10.inicio();
        sshic10.abrirSolicitud();
        */
        test.stopTest();
        
    }
}