/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Samuel Galindo Rodríguez
Proyecto: Toyota Incentivos
Descripción: Clase de data para las clases de prueba de Toyota Incentivos
-------------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------
0.1    07-05-2018 Samuel Galindo Rodríguez   Creación
*******************************************************************************/
@isTest
public class IN_TestData {

	public static void crearRegistrosLN () {

		// Tipo de registro LN
		RecordType objTipoRegLN = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		// Persona Fisica
		RecordType objPersonaFisica = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'Account' AND DeveloperName = 'ClientePersonaFisica'
			LIMIT 1
		];

		// Persona Moral
	  RecordType objPersonaMoral = [
	    SELECT Id, DeveloperName
	    FROM RecordType
	    WHERE SObjectType = 'Account' AND DeveloperName = 'ClientePersonaMoral'
	    LIMIT 1
	  ];

		Account objAccountF = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objPersonaFisica.Id
			LIMIT 1
		];

		Account objAccountM = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objPersonaMoral.Id
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// Reporte Lista Negra Moral
		List<IN_Reporte_de_Listas_Negras__c> lstLN = new List<IN_Reporte_de_Listas_Negras__c>();

		IN_Reporte_de_Listas_Negras__c objLNM1 = new IN_Reporte_de_Listas_Negras__c();
		objLNM1.IN_Comentario_de_Listas_Negras__c = 'no paga';
		objLNM1.IN_Estatus__c = 'Boletinado';
		objLNM1.IN_Modelo_1__c = objModelo.Id;
		objLNM1.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
		objLNM1.IN_Razon__c = 'No cumplio con Volumen de Venta Corporativa';
		objLNM1.IN_Salvado__c = false;
		objLNM1.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
		objLNM1.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
		objLNM1.RecordTypeId = objTipoRegLN.Id;
		objLNM1.IN_Nombre_de_la_Cuenta__c = objAccountM.Id;
		lstLN.add(objLNM1);

		IN_Reporte_de_Listas_Negras__c objLNM2 = new IN_Reporte_de_Listas_Negras__c();
		objLNM2.IN_Calle_RLN__c= 'calle avx moral';
		objLNM2.IN_Codigo_Postal_RLN__c = '09000';
		objLNM2.IN_Colonia_RLN__c = 'colonia avx moral';
		objLNM2.IN_Color_Externo_1__c = 'Rojo';
		objLNM2.IN_Comentario_de_Listas_Negras__c = 'no paga 2';
		objLNM2.IN_Estatus__c = 'Boletinado';
		objLNM2.IN_Modelo_1__c = objModelo.Id;
		objLNM2.IN_Nombre_de_la_Cuenta__c = objAccountM.Id;
		objLNM2.IN_No_Exterior_RLN__c = '14123';
		objLNM2.IN_No_Interior_RLN__c = '0900';
		objLNM2.IN_Razon__c = 'No cumplio con Volumen de Venta Corporativa';
		objLNM2.IN_Salvado__c = false;
		objLNM2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
		objLNM2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
		objLNM2.RecordTypeId = objTipoRegLN.Id;
		lstLN.add(objLNM2);

		// Reporte Lista Negra Fisico
		IN_Reporte_de_Listas_Negras__c objLNF1 = new IN_Reporte_de_Listas_Negras__c();
		objLNF1.IN_Calle_RLN__c= 'calle avx fisico';
		objLNF1.IN_Codigo_Postal_RLN__c = '09000';
		objLNF1.IN_Colonia_RLN__c = 'colonia avx fisico 1';
		objLNF1.IN_Color_Externo_1__c = 'Rojo';
		objLNF1.IN_Comentario_de_Listas_Negras__c = 'no paga';
		objLNF1.IN_Estatus__c = 'Boletinado';
		objLNF1.IN_Modelo_1__c = objModelo.Id;
		objLNF1.IN_Nombre_de_la_Cuenta__c = objAccountF.Id;
		objLNF1.IN_No_Exterior_RLN__c = '14123';
		objLNF1.IN_No_Interior_RLN__c = '0900';
		objLNF1.IN_Razon__c = 'No cumplio con Volumen de Venta Corporativa';
		objLNF1.IN_Salvado__c = false;
		objLNF1.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
		objLNF1.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
		objLNF1.RecordTypeId = objTipoRegLN.Id;
		lstLN.add(objLNF1);

		IN_Reporte_de_Listas_Negras__c objLNF2 = new IN_Reporte_de_Listas_Negras__c();
		objLNF2.IN_Calle_RLN__c= 'calle avx fisico';
		objLNF2.IN_Codigo_Postal_RLN__c = '09000';
		objLNF2.IN_Colonia_RLN__c = 'colonia avx fisico 2';
		objLNF2.IN_Color_Externo_1__c = 'Rojo';
		objLNF2.IN_Comentario_de_Listas_Negras__c = 'no paga';
		objLNF2.IN_Estatus__c = 'Boletinado';
		objLNF2.IN_Modelo_1__c = objModelo.Id;
		objLNF2.IN_Nombre_de_la_Cuenta__c = objAccountF.Id;
		objLNF2.IN_No_Exterior_RLN__c = '14123';
		objLNF2.IN_No_Interior_RLN__c = '0900';
		objLNF2.IN_Razon__c = 'No cumplio con Volumen de Venta Corporativa';
		objLNF2.IN_RFC__c = 'AVXMoral841215';
		objLNF2.IN_Salvado__c = false;
		objLNF2.IN_Telefono_RLN__c = '5564341231';
		objLNF2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
		objLNF2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
		objLNF2.RecordTypeId = objTipoRegLN.Id;
		lstLN.add(objLNF2);

		insert lstLN;

	}

	public static void crearSerie() {

		Serie__c objSerie = new Serie__c();
		objSerie.Marca__c = 'Toyota';
		objSerie.Name = '2849';

		insert objSerie;
	}

	public static void crearModelo() {

		// Unidad
		RecordType objUnidad = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'Product2' AND DeveloperName = 'Unidad'
			LIMIT 1
		];

		// Serie
		crearSerie();
		Serie__c objSerie = [
			SELECT Id
			FROM Serie__c
			LIMIT 1
		];

		Product2 objProd = new Product2();
		objProd.Name = '270600H111';
		objProd.IdExternoProducto__c = '4648451';
		objProd.PartesRobadas__c = false;
		objProd.RecordTypeId = objUnidad.Id;
		objProd.Serie__c = objSerie.Id;
		objProd.Anio__c  = '2018';
		objProd.NombreVersion__c = '270600H111'; 
		insert objProd;
	}

	public static void crearCuentaFisica() {

		// Persona Fisica
		RecordType objPersonaFisica = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'Account' AND DeveloperName = 'ClientePersonaFisica'
			LIMIT 1
		];

		Account objAccount = new Account();
		objAccount.FirstName = 'Valerie';
		objAccount.LastName	= 'Duncan';
		objAccount.RecordTypeId	= objPersonaFisica.Id;
		objAccount.IN_Estatus__pc = 'No_Boletinado';
		objAccount.TAM_IdExternoNombre__c = 'Valerie Duncan'; 
		insert objAccount;
	}

	public static void crearCuentaMoral() {

		// Persona Moral
	  RecordType objPersonaMoral = [
	    SELECT Id, DeveloperName
	    FROM RecordType
	    WHERE SObjectType = 'Account' AND DeveloperName = 'ClientePersonaMoral'
	    LIMIT 1
	  ];

		Account objAccount = new Account();
		objAccount.Name = 'Universal Solutions';
		objAccount.RecordTypeId = objPersonaMoral.Id;
		objAccount.IN_Estatus__c = 'No_Boletinado';
		objAccount.TAM_IdExternoNombre__c = 'Universal Solutions'; 		
		insert objAccount;

	}

	public static void crearLNBoletinadoPrev() {

	}
}