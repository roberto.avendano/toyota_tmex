global class DeleteWholesale_Batch implements Database.Batchable<sObject> {
    
    private String query;
    private List<InventarioWholesale__c> inventariosList;
    
    global DeleteWholesale_Batch(String q) {
        this.query = q;
        this.inventariosList = new List<InventarioWholesale__c>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(scope.size() > 0){
            inventariosList.addAll((List<InventarioWholesale__c>)scope);

            try{
                delete inventariosList;
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('Deleted Wholesale Batch finished');
    }
    
}