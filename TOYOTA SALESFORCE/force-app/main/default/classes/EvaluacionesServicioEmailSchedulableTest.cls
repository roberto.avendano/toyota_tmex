@isTest
private class EvaluacionesServicioEmailSchedulableTest{

	@testSetup 
	static void loadData(){
		Evaluaciones_DealerTriggerHandlerTest.getTestDealerIntegral('EDER');
	}

	@isTest 
	static void testGeneral() {
		Test.startTest();
			EvaluacionesServicioEmailSchedulable che = new EvaluacionesServicioEmailSchedulable();	
			String cron = '0 0 3 * * ? ';
			String jobID = system.schedule('Merge Job', cron, che);

			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id=:jobID];
			
		Test.stopTest();
	}

}