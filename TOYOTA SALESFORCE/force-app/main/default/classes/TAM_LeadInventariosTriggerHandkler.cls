/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS TAM_LeadInventariosTriggerHandkler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    12-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class TAM_LeadInventariosTriggerHandkler extends TriggerHandler{

    private List<TAM_LeadInventarios__c > itemsNew;
    private List<TAM_LeadInventarios__c > itemsOld;
	private Map<ID, TAM_LeadInventarios__c > mapItemsOld;        
        
	//Un constructor por default
	public TAM_LeadInventariosTriggerHandkler() {
        this.itemsNew = (List<TAM_LeadInventarios__c >) Trigger.new;
        this.itemsOld = (List<TAM_LeadInventarios__c >) Trigger.Old;        
        this.mapItemsOld = (Map<Id, TAM_LeadInventarios__c >) Trigger.oldMap;
	}

	//Sobreescribe el metodo de beforeUpdate
    public override void beforeDelete() {
		System.debug('EN beforeDelete....');
		//validaEstadoFacturadoEmtregado(mapItemsOld, itemsOld);    	
    }

	//Sobreescribe el metodo de beforeUpdate
    public override void afterUpdate() {
		System.debug('EN afterDelete....');
		verificaApartado(mapItemsOld, itemsNew);    	
    }

	//Sobreescribe el metodo de beforeUpdate
    public override void afterDelete() {
		System.debug('EN afterDelete....');
		validaEstadoFacturadoEmtregado(mapItemsOld, itemsOld);    	
    }

    public static void verificaApartado(Map<Id, TAM_LeadInventarios__c > mapItemsOld, List<TAM_LeadInventarios__c> itemsNew){
        System.debug('ENTRO verificaApartado TriggerHandler itemsNew: ' + itemsNew);           

		Set<String> setInv = new Set<String>();
		List<TAM_InventarioVehiculosToyota__c> lInvUpd = new List<TAM_InventarioVehiculosToyota__c>();
		
        for(TAM_LeadInventarios__c objLeadInv : itemsNew){
        	//Ve si tiene algo el cammpo de OwnerId
        	if (objLeadInv.TAM_Apartado__c && !mapItemsOld.get(objLeadInv.id).TAM_Apartado__c)
				setInv.add(objLeadInv.Name);
        }
        System.debug('ENTRO verificaApartado TriggerHandler setInv: ' + setInv);           
		        
        //Ve si no hubo problema al eominaro
        if (!setInv.isEmpty()){
        	//Libera el VIN del inventario TAM_Apartado
        	for (TAM_InventarioVehiculosToyota__c objInv : [Select id From TAM_InventarioVehiculosToyota__c
        		Where Name IN :setInv]){
        		lInvUpd.add(new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false));
        	}
        	//Actualiza los reg
        	if (!lInvUpd.isEmpty())
        		update lInvUpd;
        }//Fin si !bErrorElimina
                	
    }

    public static void validaEstadoFacturadoEmtregado(Map<Id, TAM_LeadInventarios__c > mapItemsOld, List<TAM_LeadInventarios__c> itemsOld){
        System.debug('ENTRO validaEstadoFacturadoEmtregado TriggerHandler itemsOld: ' + itemsOld);           

       	Set<String> setIdOwner = new Set<String>();
		Map<String, String> mapIdRegIdLead = new Map<String, String>();
		Set<String> setIdLeadFactEntre = new Set<String>();
		Boolean bErrorElimina = false;
		Set<String> setInv = new Set<String>();
		List<TAM_InventarioVehiculosToyota__c> lInvUpd = new List<TAM_InventarioVehiculosToyota__c>();
		
        for(TAM_LeadInventarios__c  objLeadInv : itemsOld){
        	//Ve si tiene algo el cammpo de OwnerId
        	if (objLeadInv.TAM_Prospecto__c != null)
				mapIdRegIdLead.put(objLeadInv.id, objLeadInv.TAM_Prospecto__c);
        }
        System.debug('ENTRO validaEstadoFacturadoEmtregado TriggerHandler mapIdRegIdLead: ' + mapIdRegIdLead);           
		
		//selecciona los lead asociados a mapIdRegIdLead
		for (Lead cand : [Select id From Lead Where id IN: mapIdRegIdLead.Values()
			And (TAM_PedidoFacturado__c = true or TAM_PedidoReportado__c = true)]){
			setIdLeadFactEntre.add(cand.id);
		}
        System.debug('ENTRO validaEstadoFacturadoEmtregado TriggerHandler setIdLeadFactEntre: ' + setIdLeadFactEntre);
		
		//Recorre al lista de TAM_LeadInventarios__c y ve si algino de esos ya esta facturado o entregado y desplieda un error
        for(TAM_LeadInventarios__c objLeadInv : itemsOld){
        	//Ve si tiene algo el cammpo de OwnerId
        	if (setIdLeadFactEntre.contains(objLeadInv.TAM_Prospecto__c)){
        		if (!Test.isRunningTest()){
        			objLeadInv.addError('No puedes eliminar el resgistro del inventyario porque ya se encuentra Facturado / Entregado.');
        			bErrorElimina = true;
			        System.debug('ENTRO validaEstadoFacturadoEmtregado NO SE PUEDE ELIMINAR EL REG...');		
        		}//Fin si !Test.isRunningTest()
        		setInv.add(objLeadInv.Name);
        	}//Fin si setIdLeadFactEntre.contains(objLeadInv.TAM_Prospecto__c
        }
        
        //Ve si no hubo problema al eominaro
        if (!bErrorElimina){
        	//Libera el VIN del inventario TAM_Apartado
        	for (TAM_InventarioVehiculosToyota__c objInv : [Select id From TAM_InventarioVehiculosToyota__c
        		Where Name IN :setInv]){
        		lInvUpd.add(new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false));
        	}
        	//Actualiza los reg
        	if (!lInvUpd.isEmpty())
        		update lInvUpd;
        }//Fin si !bErrorElimina
                	
    }
    
}