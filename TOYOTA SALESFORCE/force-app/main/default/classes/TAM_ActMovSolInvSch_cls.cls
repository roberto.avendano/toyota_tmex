/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_MovimientosSolicitudes__c
                        y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActMovSolInvSch_cls implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActMovSolInvSch_cls.execute...');

        String strFechaCalToyota = System.Label.TAM_FechaMesToyota;        
        Date dFechaConsulta = strFechaCalToyota != 'null' ? Date.valueOf(strFechaCalToyota) : Date.today();
        if (Test.isRunningTest()) dFechaConsulta = Date.today();

        String strVinPrueba = '2T3W1RFV0MW149668';
        String strTipoPedido = 'Inventario';
        String EstatusDOD = 'Cancelado';
        String EstDealerSol = 'Cancelada';
        String EstDealerSol2 = 'Rechazada';

        String strFechaIni = '2020-11-02';
        String strFechaFin = '2021-08-02';
        String sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
        this.sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name ';
        this.sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
        this.sQuery += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
        //this.sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= ' + String.valueOf(strFechaIni); 
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c <= ' + String.valueOf(strFechaFin); 
        this.sQuery += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';

        //this.sQuery += ' And TAM_EstatusDOD__c != \'' + String.escapeSingleQuotes(EstatusDOD) + '\'';
        //this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol) + '\'';
        //this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol2) + '\'';

        this.sQuery += ' Order by TAM_VIN__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c ASC';
        System.debug('EN TAM_ActMovSolInvSch_cls.execute this.sQuery: ' + this.sQuery);
        
        
        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActMovSolInvSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActMovSolInvBch_cls objActMovSolInvBch = new TAM_ActMovSolInvBch_cls(sQuery);        
        //No es una prueba entonces procesa de 50 en 50
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActMovSolInvBch, 50);
                     
    }
    
}