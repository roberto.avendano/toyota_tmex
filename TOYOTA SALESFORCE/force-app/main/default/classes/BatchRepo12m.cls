global class BatchRepo12m implements Database.Batchable<sObject>,Database.Stateful{
	
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            'SELECT Id,Name,fwy_Segmento__c,fwy_Subsegmento__c FROM fwy_ReporteComunidades__c WHERE (fwy_Segmento__c = \'Seguros\' AND fwy_Subsegmento__c=\'Bono 5%\') OR (fwy_Segmento__c = \'Clientes\' AND fwy_Subsegmento__c=\'Comisiones\') OR (fwy_Segmento__c = \'Dealer\' AND (fwy_Subsegmento__c=\'Eficiencia\'OR fwy_Subsegmento__c=\'Premium WIP\')) OR (fwy_Segmento__c = \'Plan piso\' AND fwy_Subsegmento__c=\'Control de Seguros Plan Piso\')'
        );
    }
    global void execute(Database.BatchableContext bc,List<fwy_ReporteComunidades__c>ReporteComunidad12m){
        Database.delete(ReporteComunidad12m,false);
    }
    global void finish(Database.BatchableContext bc){
        
    }
}