/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_DODSolicitudesPedidoEspecial__c
    					y toma los reg que ya tienen un TAM_DummyVin__c y no tienen valor en el campo de TAM_VIN__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    20-Abril-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActualizaDummyVinSch_cls implements Schedulable{
    
	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_ActualizaDummyVinSch_cls.execute...');
        String strIdLeadPrueba = 'MR0DA3CD9M011AC28';

		this.sQuery = 'Select Id, Name, TAM_VIN__c, TAM_DummyVin__c, TAM_Codigo__c, TAM_Estatus__c, TAM_TieneCheckout__c, TAM_DummyVINFormula__c, TAM_Historico__c ';
		this.sQuery += ' From TAM_DODSolicitudesPedidoEspecial__c ';
		this.sQuery += ' where TAM_VIN__c = null and TAM_DummyVin__c != null ';
        //this.sQuery += ' And TAM_DummyVin__c = \'' + String.escapeSingleQuotes(strIdLeadPrueba) + '\'';

		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_ActualizaDummyVinSch_cls.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_ActualizaDummyVinBch_cls objUpdDummyVinCls = new TAM_ActualizaDummyVinBch_cls(sQuery);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objUpdDummyVinCls, 50);
			    	 
    }
    
}