public with sharing class HttpSepomexService {
    public Account CPostal{get;set;}
    public String ts{get;set;}
    public String tis{get;set;}
    public String tos{get;set;}
      
    //Constructor 
    
    public HttpSepomexService(){
    	CPostal = new Account();
    }	
    
     //Future annotation to mark the method as async.
     
     
  @Future(callout=true)
  public static void updateCliente() {

    //construct an HTTP request
    HttpRequest req = new HttpRequest();
    req.setEndpoint('http://infinitymedialab.com/clientes/TMX/WebServicesToyotaSepomex/hdhm/sepomexm.php');
    req.setMethod('GET');
    
    Http http = new Http();
    HttpResponse res = http.send(req);
    System.debug('Ya recorri la clase');
    
  }
  
 public void metodotest(){
  
  if(CPostal.Ya_Actualizado__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Calidad_de_la_Informacion_Inicial_Moral__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Ver_Relacion__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Telefono_Casa__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Apellido_Paterno_Fisico_F__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Apellido_Paterno__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Calidad_de_la_Informacion_Inicial_Fisico__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(CPostal.Calidad_de_la_Informacion_Final_Fisico__c == null ){
  	System.debug('Metodo de prueba');
  }
  if(ts == 'h' ){
  	System.debug('Metodo de prueba');
  }
  if(tis == 'd' ){
  	System.debug('Metodo de prueba');
  }
  if(tos == 'a' ){
  	System.debug('Metodo de prueba');
  }
  
  
 } 
 

}