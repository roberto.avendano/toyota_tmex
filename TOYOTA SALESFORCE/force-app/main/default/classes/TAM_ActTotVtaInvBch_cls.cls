/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_MovimientosSolicitudes__c
                        y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActTotVtaInvBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActTotVtaInvBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActTotVtaInvBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_CheckOutDetalleSolicitudCompra__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_ActTotVtaInvBch_cls.');

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        String sFleet = 'N';
                      
        //Un Objero para el error en caso de exista
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        String sHabilitaMergeAccounts = System.Label.TAM_FechaMesToyota;                
        Set<String> setVinMov = new Set<String>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c>();

        Map<String, TAM_MovimientosSolicitudes__c> mapIdExtObjMovSolUps = new Map<String, TAM_MovimientosSolicitudes__c>();
        Map<String, TAM_MovimientosSolicitudes__c> mapMovSolFechActUps = new Map<String, TAM_MovimientosSolicitudes__c>();

        Date dtFechaActual = Date.today();
        String strDtFechaCheckOut = String.valueOf(dtFechaActual);
        System.debug('EN TAM_ActTotVtaInvBch_cls strDtFechaCheckOut: ' + strDtFechaCheckOut);
                
        //Recorre el mapa de mapIdVinReg 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutConsFin : scope){
            //Crea el id externo para el objeto de TAM_CheckOutDD__c
            String sIdExterno = objCheckOutConsFin.TAM_VIN__c + '-' + objCheckOutConsFin.TAM_SolicitudFlotillaPrograma__r.Name  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                
            String sEstatusSol = 'Autorizada';
            //Metelo al mapa  
            TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
                Name = objCheckOutConsFin.TAM_SolicitudFlotillaPrograma__r.Name + ' - ' + strDtFechaCheckOut,
                TAM_IdExternoSFDC__c = sIdExterno,
                TAM_EstatusSolicitud__c = sEstatusSol,
                TAM_Total__c = 0,
                TAM_FechaMovimiento__c = dtFechaActual,
                TAM_VIN__c = objCheckOutConsFin.TAM_VIN__c,
                TAM_CheckOutSolicitud__c = objCheckOutConsFin.id
            );
            System.debug('EN TAM_ActMovSolInvBch_cls3 CANCELADA objMovSol: ' + objMovSol);
            //Metelo al mapa
            mapMovSolFechActUps.put(sIdExterno, objMovSol);
        }//Fin del for para los reg de scope
        System.debug('EN TAM_ActTotVtaInvBch_cls mapMovSolFechActUps: ' + mapMovSolFechActUps.keyset());
        System.debug('EN TAM_ActTotVtaInvBch_cls mapMovSolFechActUps: ' + mapMovSolFechActUps.values());

        //Un objeto del tipo Savepoint
        Savepoint sp1 = Database.setSavepoint();        

        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapMovSolFechActUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapMovSolFechActUps.values(), TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActTotVtaInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR1: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActTotVtaInvBch_cls los datos se actualizaron con exito1: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Roleback a todo
        //Database.rollback(sp1);
                
        //Recorre el mapa de mapIdVinReg 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutConsFin : scope){
        
            Map<String, Map<Date, List<Movimiento__c>>> MapVinFechaMovObjMov = new Map<String, Map<Date, List<Movimiento__c>>>();
            Set<Date> setFechaSubmite = new Set<Date>();
            Map<String, Map<Date, TAM_MovimientosSolicitudes__c>> MapVinFechaMovObjMovSol = new Map<String, Map<Date, TAM_MovimientosSolicitudes__c>>();
            
            //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
            String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                //' And TAM_FechaEnvForm__c = ' + strDtFechaConsulta +
                ' And VIN__r.Name = \'' + String.escapeSingleQuotes(objCheckOutConsFin.TAM_VIN__c) + '\'' +
                //' And Fleet__c = \'' + String.escapeSingleQuotes(sFleet) + '\'' +
                ' Order by Submitted_Date__c DESC';
            //Ees una prueba
            if (Test.isRunningTest())    
                queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' Order by Submitted_Date__c DESC LIMIT 2';
            System.debug('EN TAM_ActTotVtaInvBch_cls queryMovimiento: ' + queryMovimiento);
            
            //Busca los movimientos    
            List<sObject> sObjLstMov = Database.query(queryMovimiento);
            
            //Recorre la lista de los mov por vin y metelos al mapa de
            for (sObject objsObjectPaso : sObjLstMov){
                Movimiento__c objMovPaso = (Movimiento__c) objsObjectPaso;
                //Agrega la fecha de envio del mov al set de setFechaSubmite 
                setFechaSubmite.add(objMovPaso.TAM_FechaEnvForm__c);
                //Ve si existe en MapVinFechaMovObjMov
                if (MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name)){
                    if (MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c)){
                        List<Movimiento__c> lMovPaso = MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).get(objMovPaso.TAM_FechaEnvForm__c);
                        //Agrega a la lista el nuevo mov
                        lMovPaso.add(objMovPaso);
                        //Actualiza el mapa de la posición MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name)
                        MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).put(objMovPaso.TAM_FechaEnvForm__c, lMovPaso);
                    }//Fin si MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c)
                    if (!MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c))
                        MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).put(objMovPaso.TAM_FechaEnvForm__c, new List<Movimiento__c>{objMovPaso});
                }//Fin si MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name)
                //No exite el objMovPaso.VIN__r.Name en MapVinFechaMovObjMov
                if (!MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name))
                   MapVinFechaMovObjMov.put(objMovPaso.VIN__r.Name, new Map<Date, List<Movimiento__c>>{objMovPaso.TAM_FechaEnvForm__c => new List<Movimiento__c>{objMovPaso}});
            }//Fin del for para los mov de sObjLstMov
            System.debug('EN TAM_ActTotVtaInvBch_cls setFechaSubmite: ' + setFechaSubmite);
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMov: ' + MapVinFechaMovObjMov.KeySet());
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMov: ' + MapVinFechaMovObjMov.Values());

            //Ya tienes todos los mov del objCheckOutConsFin.TAM_VIN__c busca los registros en la table de TAM_MovimientosSolicitudes__c
            for (TAM_MovimientosSolicitudes__c objMovSolPaso : [Select t.TAM_VIN__c, 
                t.TAM_UltimoMovimientoDD__c, 
                t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
                From TAM_MovimientosSolicitudes__c t Where TAM_VIN__c =:objCheckOutConsFin.TAM_VIN__c ]){
                //Mete los mov al mapa de MapVinFechaMovObjMovSol
                if (MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)){
                    //Agregarlo al mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                    MapVinFechaMovObjMovSol.get(objMovSolPaso.TAM_VIN__c).put(objMovSolPaso.TAM_FechaMovimiento__c, objMovSolPaso);
                }//Fin si MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)
                //No exite el objMovSolPaso.TAM_VIN__c en MapVinFechaMovObjMovSol
                if (!MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c))
                   MapVinFechaMovObjMovSol.put(objMovSolPaso.TAM_VIN__c, new Map<Date, TAM_MovimientosSolicitudes__c>{objMovSolPaso.TAM_FechaMovimiento__c => objMovSolPaso});                
            }//Fin del for para TAM_MovimientosSolicitudes__c
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.KeySet());
            System.debug('EN TAM_ActTotVtaInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.Values());
            
            //Ve si el vin tiene mov en el mapa de MapVinFechaMovObjMov
            if (MapVinFechaMovObjMov.containsKey(objCheckOutConsFin.TAM_VIN__c)){
                Map<Date, List<Movimiento__c>> mapVinPasoMov = MapVinFechaMovObjMov.get(objCheckOutConsFin.TAM_VIN__c);  
                //Muy bien ahora recorre la lista de fechas del mapa MapVinFechaMovObjMov
                for (Date objFechaMovPaso : mapVinPasoMov.KeySet()){
                    System.debug('EN TAM_ActTotVtaInvBch_cls objFechaMovPaso: ' + objFechaMovPaso);
                    //Busca la fecha objFechaMovPaso en el mapa de MapVinFechaMovObjMovSol
                    if (MapVinFechaMovObjMovSol.containsKey(objCheckOutConsFin.TAM_VIN__c)){
                        //Toma la lista de mov 
                        if (mapVinPasoMov.containsKey(objFechaMovPaso)){
	                        List<Movimiento__c> lMovCons = mapVinPasoMov.get(objFechaMovPaso);
                            System.debug('EN TAM_ActTotVtaInvBch_cls lMovCons: ' + lMovCons);
	                        //Ya tienes la fecha adecuada entonces busca sus mov y obten el ultimo
	                        Movimiento__c objMovInv = getUltMov(lMovCons);
                            System.debug('EN TAM_ActTotVtaInvBch_cls objMovInv: ' + objMovInv);
	                        //Solo tiene un mov
	                        if (lMovCons.size() == 1)
	                            objMovInv = lMovCons.get(0);
	                        System.debug('EN TAM_ActTotVtaInvBch_cls objMovInv: ' + objMovInv);
	                        Integer dSuma = 0;
	                        //Ya tienes el ultimo mov ahora haz la suma aritmetica de los mov
	                        for (Movimiento__c obMovPasoSum : lMovCons)
	                            dSuma += Integer.valueOf(obMovPasoSum.Qty__c != null && obMovPasoSum.Qty__c != '' ? Integer.valueOf(obMovPasoSum.Qty__c) : 0);
	                        System.debug('EN TAM_ActTotVtaInvBch_cls dSuma: ' + dSuma);
	                        //Ve si existe la fecha en el mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
	                        if (MapVinFechaMovObjMovSol.get(objCheckOutConsFin.TAM_VIN__c).containsKey(objFechaMovPaso)){
	                            //Busca el objeto de tipo TAM_MovimientosSolicitudes__c y Actualuzalo
	                            TAM_MovimientosSolicitudes__c objMovSolPaso = MapVinFechaMovObjMovSol.get(objCheckOutConsFin.TAM_VIN__c).get(objFechaMovPaso);
	                            TAM_MovimientosSolicitudes__c objMovSolUpd = new TAM_MovimientosSolicitudes__c(
	                                TAM_IdExternoSFDC__c = objMovSolPaso.TAM_IdExternoSFDC__c,
	                                TAM_CheckOutSolicitud__c = objCheckOutConsFin.id,
	                                TAM_UltimoMovimientoDD__c = objMovInv.id
	                            );
                                System.debug('EN TAM_ActTotVtaInvBch_cls objMovInv.TAM_CodigoDistribuidorFrm__c: ' + objMovInv.TAM_CodigoDistribuidorFrm__c + ' objCheckOutConsFin.TAM_NombreDistribuidor__c: ' + objCheckOutConsFin.TAM_NombreDistribuidor__c);	                            
	                            //Solo aplica si la solicitud esta autorizada 
	                            if (objMovSolPaso.TAM_EstatusSolicitud__c == 'Autorizada')
                                    if ( objMovSolPaso.TAM_Total__c == 0 || objMovSolPaso.TAM_Total__c == null)
	                                   objMovSolUpd.TAM_Total__c = objMovInv.TAM_CodigoDistribuidorFrm__c == objCheckOutConsFin.TAM_NombreDistribuidor__c ? dSuma : 0;
	                            if ( (objMovSolPaso.TAM_EstatusSolicitud__c == 'Cancelada' || objMovSolPaso.TAM_EstatusSolicitud__c == 'Rechazada') &&  (objMovSolPaso.TAM_Total__c == 0 || objMovSolPaso.TAM_Total__c == null))
	                                objMovSolUpd.TAM_Total__c = 0;
	                            System.debug('EN TAM_ActTotVtaInvBch_cls existe en Mov Sol objMovSolUpd: ' + objMovSolUpd);                               
	                            //Agregalo al mapa de mapIdExtObjMovSolUps
	                           mapIdExtObjMovSolUps.put(objMovSolPaso.TAM_IdExternoSFDC__c, objMovSolUpd);
	                        }//Fin si MapVinFechaMovObjMovSol.get(objCheckOutConsFin.TAM_VIN__c).containsKey(objFechaMovPaso)
                            //Ve si existe la fecha en el mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                            if (!MapVinFechaMovObjMovSol.get(objCheckOutConsFin.TAM_VIN__c).containsKey(objFechaMovPaso)){
                                System.debug('EN TAM_ActTotVtaInvBch_cls NO EXISTE ESA FECHA objFechaMovPaso: ' + objFechaMovPaso);                                                            
                            }//Fin si !MapVinFechaMovObjMovSol.get(objCheckOutConsFin.TAM_VIN__c).containsKey(objFechaMovPaso)
                        }//Fin si mapVinPasoMov.contains(objFechaMovPaso)
                    }//Fin si MapVinFechaMovObjMovSol.containsKey(objCheckOutConsFin.TAM_VIN__c)
                }//Fin del for para la mapVinPasoMov.KeySet()
            }//Fin si MapVinFechaMovObjMov.containsKey(objCheckOutConsFin.TAM_VIN__c)
            
        }//Fin del for para la lista de TAM_CheckOutDetalleSolicitudCompra__c
        System.debug('EN TAM_ActTotVtaInvBch_cls mapCheckOutDDUpsFinal: ' + mapIdExtObjMovSolUps.keySet());
        System.debug('EN TAM_ActTotVtaInvBch_cls mapCheckOutDDUpsFinal: ' + mapIdExtObjMovSolUps.values());
        
        //Un objeto del tipo Savepoint
        Savepoint sp2 = Database.setSavepoint();        
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdExtObjMovSolUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdExtObjMovSolUps.values(), TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActTotVtaInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR2: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActTotVtaInvBch_cls los datos se actualizaron con exito2: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()
        
        //Roleback a todo
        //Database.rollback(sp2);
        
        //Ve si hay errores          
        //if (!lError.isEmpty())
        //    insert lError;
            
    }
    
    //Ve  buscar la lista de mov para ese dia
    public static Movimiento__c getUltMov(List<Movimiento__c> lMovimientos){
        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov...: ');

        Movimiento__c objUltMov = new Movimiento__c();
                        
            //Ve si la lista de mov es mayor a 1, toma elID del Vehiculo
            if (lMovimientos.size() > 1 || Test.isRunningTest()){
                Boolean TieneMovMismaFecha = false;
                Map<String, Map<String, Time>> mapDateSubmMapIdMovTime = new Map<String, Map<String, Time>>();
                Movimiento__c objMovPaso = lMovimientos.get(0);
                Time HoraEnvio = getHoraEnvio(objMovPaso.Submitted_Time__c);
                Date dtFechaEnvio = objMovPaso.Submitted_Date__c.date();
                System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov dtFechaEnvio0: ' + dtFechaEnvio + ' HoraEnvio: ' + HoraEnvio);
                //Toma el primero mov y metelo a al mapa de mapDateSubmMapIdMovTime
                mapDateSubmMapIdMovTime.put(String.valueOf(dtFechaEnvio), new Map<String, Time>{objMovPaso.id => HoraEnvio});
                //Recorre la lista de mov y toma a partir del segundo mof
                for (Integer iCnt = 0; iCnt < lMovimientos.size(); iCnt++){
                   //Toma el mov en la posicion iCnt
                    Movimiento__c objMovPaso2 = lMovimientos.get(iCnt);
                    //Inicializa la fecha del mov
                    Date dtFechaEnvio2 = objMovPaso2.Submitted_Date__c.date();                
                    Time HoraEnvio2 = getHoraEnvio(objMovPaso2.Submitted_Time__c);
                    //Ve si existe la fecha  Submitted_Date__c en el mapa de mapDateSubmMapIdMovTime
                    if (mapDateSubmMapIdMovTime.containsKey( String.valueOf(dtFechaEnvio2) )){
                       mapDateSubmMapIdMovTime.get( String.valueOf(dtFechaEnvio2) ).put(objMovPaso2.id, HoraEnvio2);
                       TieneMovMismaFecha = true;
                    }//Fin si mapDateSubmMapIdMovTime.containsKey(dtFechaEnvio2) 
                }//Fin del for para los mov
                System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov mapDateSubmMapIdMovTime: ' + mapDateSubmMapIdMovTime);
                
                //Mas de un Mov con la misma fecha
                if (TieneMovMismaFecha){
                    List<Time> lHoraEnvio = new List<Time>();
                    for (Map<String, Time> MapPaso : mapDateSubmMapIdMovTime.Values()){
                        for (String sIdMov : MapPaso.keySet()){
                            lHoraEnvio.add(MapPaso.get(sIdMov));                            
                        }
                    }//Fin del for para mapDateSubmMapIdMovTime.Values()
                    //Ordena la lista lHoraEnvio
                    lHoraEnvio.sort();
                    Time sFechaEnv = lHoraEnvio.get(0);
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvio: ' + lHoraEnvio + ' sFechaEnv: ' + sFechaEnv);

                    List<Time> lHoraEnvioPaso = new List<Time>();
                    List<String> lHoraMill = new List<String>();
                    List<String> lHoraEnvioFinal = new List<String>();

                    //Recorre la lista y ver si hay dos registros exactamente iguales en la Hora
                    for (Integer inCntFec = 1; inCntFec < lHoraEnvio.size(); inCntFec++){
                        if ( lHoraEnvio[inCntFec] == sFechaEnv)
                            lHoraEnvioPaso.add(sFechaEnv);
                    }
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvioPaso: ' + lHoraEnvioPaso);
                    
                    //Tinene algo la lista de lHoraEnvioPaso
                    if (!lHoraEnvioPaso.isEmpty()){
                        //Ve si tiene algo la lista de lHoraEnvioPaso
                        for (Integer iCnt = 0; iCnt < lMovimientos.size(); iCnt++){
                           Movimiento__c objMovPaso3 = lMovimientos.get(iCnt);
                           //Inicializa la fecha del mov
                           Time HoraEnvio2 = getHoraEnvio(objMovPaso3.Submitted_Time__c);
                           if (HoraEnvio2 == sFechaEnv){
                               String sSubmittedTime = String.valueOf(objMovPaso3.Submitted_Time__c);
                               System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov Submitted_Time__c: ' + sSubmittedTime);
                               List<String> strHoraLst = new List<String>();
                               Integer iPosIni = 0;
                               for (Integer iCntCarc = 0; iCntCarc < sSubmittedTime.length() -1 ; iCntCarc++){
                                   //System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sSubmittedTime.charAt(iCntCarc): ' + sSubmittedTime.charAt(iCntCarc));
                                   if (sSubmittedTime.charAt(iCntCarc) == 46){
                                      strHoraLst.add(sSubmittedTime.substring(iPosIni,iCntCarc));
                                      //System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov strHoraEnvio: ' + sSubmittedTime.substring(iPosIni,iCntCarc));                                   
                                      iPosIni = iCntCarc + 1;
                                   }//Fin si strHoraEnvio.charAt(iCnt) == 46
                               }//Fin del for para strHoraEnvio
                               String sHoraMill = sSubmittedTime.substring(iPosIni, sSubmittedTime.length());
                               System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov arrSdtFechaEnvio2: ' + sHoraMill + ' HoraEnvio2: ' + HoraEnvio2 + ' sFechaEnv: ' + sFechaEnv);
                               lHoraMill.add(sHoraMill);
                           }//Fin si sdtFechaEnvio2.contains(sFechaEnv)
                        }//Fin del for para los mov
                        //Ordena la lista
                        lHoraMill.sort();
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraMill: ' + lHoraMill);
                        
                        String sFechaEnvPaso = String.valueOf(sFechaEnv);
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sFechaEnvPaso: ' + sFechaEnvPaso);
                        String sHoraFinal = sFechaEnvPaso + '.' + lHoraMill.get(lHoraMill.size()-1);
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sHoraFinal: ' + sHoraFinal);

                        //Agrega el ultimo elemento a la lista de lHoraEnvio
                        lHoraEnvioFinal.add( sHoraFinal );
                    }//Fin si !lHoraEnvioPaso.isEmpty()
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvioFinal: ' + lHoraEnvioFinal);
                 
                    //No tiene nada la lista de lHoraEnvioPaso
                    if (lHoraEnvioPaso.isEmpty())
                        lHoraEnvioFinal.add( String.valueOf(lHoraEnvio.get(lHoraEnvio.size()-1)) );                        
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvioFinal: ' + lHoraEnvioFinal);
                    
                    //Toma el 1er reg de la lista ordenada
                    Time horaFinal = lHoraEnvio.get(lHoraEnvio.size()-1);
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvio: ' + lHoraEnvio + ' horaFinal: ' + horaFinal);
                    Boolean blnExiste = false;
                    String sIdMovFinal;
                    
                    //Recorre el mapa de mapDateSubmMapIdMovTime
                    for (Map<String, Time> MapPaso : mapDateSubmMapIdMovTime.Values()){
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov MapPaso: ' + MapPaso);
                        for (String sIdMov : MapPaso.keySet()){
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lHoraEnvio.get(lHoraEnvio.size()-1): ' + lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1) + ' MapPaso.get(sIdMov): ' + String.valueOf(MapPaso.get(sIdMov)) );                    
                            //Toma la hora y ve si se trata de la misma
                            if (lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1) == String.valueOf(MapPaso.get(sIdMov)) ){
                               sIdMovFinal = sIdMov;
                               blnExiste = true; 
                               break;
                            }//Fin si lHoraEnvio.get(0) == MapPaso.get(sIdMov)
                        }//Fin del for para MapPaso.keySet()
                        //Salte de este ciclo si ya lo encontro
                        if (blnExiste)
                            break;
                    }//Fin del for para mapDateSubmMapIdMovTime.Values() 
                    //Ve si blnExiste y crea el objeto del auto para poder actuliza el mov
                    if (blnExiste){
                        System.debug('EN TAM_getUltimoMovVin.getUltimoMovVin blnExiste: ' + blnExiste + ' sIdMovFinal: ' + sIdMovFinal);                        
                        List<String> lTimeFinal = new List<String>();
                        for (Movimiento__c objMovFinal : lMovimientos){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sUltHoraMovFinal = sUltHoraMov.left(9); 
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            String sHoraMovFinal = sHoraMov.left(9);
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            if (sUltHoraMovFinal == sHoraMovFinal){
                                lTimeFinal.add(sHoraMov.right(6));
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov EXISTE lTimeFinal: ' + lTimeFinal);
                        lTimeFinal.sort();                        
                        //Toma el ultimo
                        String sHoraMovPaso = lTimeFinal.size() == 1 ? lTimeFinal.get(0) : lTimeFinal.get(lTimeFinal.size()-1);
                        for (Movimiento__c objMovFinal : lMovimientos){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov EXISTE sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            String sHoraMovPasoFinal = sHoraMov.right(6);
                            if (sHoraMovPaso == sHoraMovPasoFinal){
                                objUltMov = objMovFinal;
                                break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos                        
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov objUltMov: ' + objUltMov);
                    }//Fin si blnExiste
                    //No existe porque es la mia fecha y hora solo cambian los milisegundos
                    if (!blnExiste){ 
                        List<String> lTimeFinal = new List<String>();
                        for (Movimiento__c objMovFinal : lMovimientos){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sUltHoraMovFinal = sUltHoraMov.left(9); 
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            String sHoraMovFinal = sHoraMov.left(9);
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            if (sUltHoraMovFinal == sHoraMovFinal){
                                lTimeFinal.add(sHoraMov.right(6));
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov lTimeFinal: ' + lTimeFinal);
                        lTimeFinal.sort();                        
                        //Toma el ultimo
                        String sHoraMovPaso = lTimeFinal.size() == 1 ? lTimeFinal.get(0) : lTimeFinal.get(lTimeFinal.size()-1);
                        for (Movimiento__c objMovFinal : lMovimientos){
                            String sUltHoraMov = lHoraEnvioFinal.get(lHoraEnvioFinal.size()-1).replace(':','.').replace('000Z.','');
                            String sHoraMov = objMovFinal.Submitted_Time__c;
                            System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov sUltHoraMov: ' + sUltHoraMov + ' sHoraMov: ' + sHoraMov);
                            //Toma los ultimos 6 digitos de la hora 09.38.11.687135
                            String sHoraMovPasoFinal = sHoraMov.right(6);
                            if (sHoraMovPaso == sHoraMovPasoFinal){
                                objUltMov = objMovFinal;
                                break;
                            }//Fin si lMovimientos
                        }//Fin del for para lMovimientos                        
                        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov objUltMov: ' + objUltMov);
                    }//Fin si no existe debe ser la misma fecha y hora
                    System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov blnExiste: ' + blnExiste + ' objUltMov: ' + objUltMov);
                }//Fin si TieneMovMismaFecha
            }//Fin si lMovimientos.size() > 1

            //Ve si la lista de mov es mayor a 1, toma elID del Vehiculo
            if (lMovimientos.size() == 1 || Test.isRunningTest()){
               objUltMov = lMovimientos.get(0);                
            }
            
        System.debug('EN TAM_ActTotVtaInvBch_cls.getUltMov objUltMov: ' + objUltMov);                                                
        //Regresa el ultimo mov
        return objUltMov;
        
    }
    
    //La funcion para poder converttir la hora de envio a un objeto del tipo Time
    public static Time getHoraEnvio(String strHoraEnvio){
        System.debug('EN TAM_ActTotVtaInvBch_cls.getHoraEnvio strHoraEnvio: ' + strHoraEnvio);      

        Time HoraEnvioPaso;
        List<String> strHoraLst = new List<String>();
        
        //'15.42.11.872359' 15.42.11.872359
        if (strHoraEnvio != null){
            String[] lstValores = strHoraEnvio.split('.');
            //Ve si la hora contiene un punto
            if (strHoraEnvio.contains('.')){
                Integer iPosIni = 0;
                for (Integer iCnt = 0; iCnt < strHoraEnvio.length() -1 ; iCnt++){
                    if (strHoraEnvio.charAt(iCnt) == 46){
                       System.debug('EN TAM_ActTotVtaInvBch_cls.getHoraEnvio strHoraEnvio: ' + strHoraEnvio.substring(iPosIni,iCnt));                            
                       strHoraLst.add(strHoraEnvio.substring(iPosIni,iCnt));
                       iPosIni = iCnt + 1;
                    }//Fin si strHoraEnvio.charAt(iCnt) == 46
                }//Fin del for para strHoraEnvio
                String sHoraMill = strHoraEnvio.substring(iPosIni, strHoraEnvio.length());
                String sHoraMillFinal = sHoraMill.left(5);
                System.debug('EN TAM_ActTotVtaInvBch_cls.getHoraEnvio sHoraMillFinal: ' + sHoraMillFinal);
                strHoraLst.add(sHoraMillFinal);
                //Inicializa la hora finalmente
                //HoraEnvioPaso = Time.newInstance(Integer.valueOf(strHoraLst[0]), Integer.valueOf(strHoraLst[1]), Integer.valueOf(strHoraLst[2]), Integer.valueOf(strHoraLst[3]));
                HoraEnvioPaso = Time.newInstance(Integer.valueOf(strHoraLst[0]), Integer.valueOf(strHoraLst[1]), Integer.valueOf(strHoraLst[2]), 00);
            }//Fin si strHoraEnvio.contains('.')
        }//Fin si strHoraEnvio != null

        System.debug('EN TAM_ActTotVtaInvBch_cls.getHoraEnvio HoraEnvioPaso: ' + HoraEnvioPaso);        
        //Regresa la hora 
        return HoraEnvioPaso;
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActTotVtaInvBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}