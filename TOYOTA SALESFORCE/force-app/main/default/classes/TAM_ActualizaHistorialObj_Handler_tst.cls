/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActualizaHistorialObj_Handler_tst {

    @TestSetup static void loadData(){

        TAM_DODSolicitudesPedidoEspecial__c objTAMDODSolicitudesPedidoEspecial = new TAM_DODSolicitudesPedidoEspecial__c(       
            TAM_VIN__c = 'XXXXX123456',
            TAM_DummyVin__c = 'XXXXX123456',
            TAM_Codigo__c  = '22060',
            TAM_Estatus__c = 'Asignado',
            TAM_Historico__c = false,
            TAM_TieneCheckout__c = false
        );
        insert objTAMDODSolicitudesPedidoEspecial;  
        
        TAM_ActualizaHistorialObj__c objActHistObj = new TAM_ActualizaHistorialObj__c(
            TAM_IdExterno__c = 'a211Y0000053xydQAA' + '-' + Date.today(),          
            TAM_OLDVALUE__c = 'Asignado',
            TAM_NEWVALUE__c = 'Pendiente',
            TAM_Proceso__c = 'Update Vin DOD y CheckOut',
            TAM_PARENTID__c = objTAMDODSolicitudesPedidoEspecial.id
        );
        insert objActHistObj;
                            
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_ActualizaHistorialObjOK() {
        // TO DO: implement unit test
        Test.startTest();
            TAM_ActualizaHistorialObj__c objPaso = [Select id, Name, TAM_IdExterno__c, TAM_OLDVALUE__c, TAM_NEWVALUE__c, 
                TAM_Proceso__c From TAM_ActualizaHistorialObj__c LIMIT 1];
        Test.stopTest();
    }
}