/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladira para el componente TAM_CargaMasivaVinesDod

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    03-Junio-2020        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_CargaMasivaVinesDodCtrl {
    public String solicitudID{get;set;}
    public Integer intTotRegSel{get;set;}
    public List<TAM_DODSolicitudesPedidoEspecial__c> lDODSolPedEsp{get;set;}
    public List<wrpRegDod> lwrpRegDod {get;set;}
    public String sVinesSelecc{get;set;}
    public Boolean blnEstatusOk{get;set;}
    
    public TAM_CargaMasivaVinesDodCtrl(ApexPages.StandardSetController setCon) {
        System.debug('En TAM_CargaMasivaVinesDodCtrl setCon.' + setCon);
        String solicitudID='';
        blnEstatusOk = false;
        intTotRegSel = setCon.getSelected().size();
        System.debug('En TAM_CargaMasivaVinesDodCtrl intTotRegSel.' + intTotRegSel);
        lDODSolPedEsp = new List<TAM_DODSolicitudesPedidoEspecial__c>();
        lwrpRegDod = new List<wrpRegDod>();
        Integer intCntNoReg = 1;
        
        Map<Id,SObject> accMap = new Map<Id, SObject>(setCon.getSelected());
        System.debug('En TAM_CargaMasivaVinesDodCtrl accMap: ' + accMap.size());
        
        for (TAM_DODSolicitudesPedidoEspecial__c objDodSel : [SELECT ID, Name, TAM_FechaPeticion__c,
            TAM_ClaveDealer__c, TAM_Modelo__c, TAM_Anio__c, TAM_Codigo__c, TAM_Serie__c, TAM_Color__c,
            TAM_DummyVin__c, TAM_IdExterno__c FROM TAM_DODSolicitudesPedidoEspecial__c 
            Where ID IN :accMap.keyset() and TAM_Estatus__c = 'Pendiente' and TAM_DummyVin__c = null]){
            lDODSolPedEsp.add(objDodSel);
            Date dFechaPaso = objDodSel.TAM_FechaPeticion__c.Date();
            String sFechaPaso = String.valueOf(dFechaPaso);
            lwrpRegDod.add(new wrpRegDod(objDodSel, sFechaPaso, intCntNoReg));
            intCntNoReg++;
        }//Fin del for para TAM_DODSolicitudesPedidoEspecial__c
        System.debug('En TAM_CargaMasivaVinesDodCtrl lDODSolPedEsp: ' + lDODSolPedEsp);        
    }

    public PageReference seleccionarVines(){
        System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVinesSelecc: ' + sVinesSelecc + ' ' + sVinesSelecc.length());        
        Boolean error = false;

        String sVinFinalPaso = ''; //substring(0,19);
        Integer inyCntReg = 0;
        Integer intPosIni = 0;
        Integer intPosFin = 0;
        Integer intPosFin2 = 0;
        Boolean bExiste10 = false;
        Boolean bExiste13 = false;        

        try{

	        for (Integer cnt=0; cnt < sVinesSelecc.length(); cnt++){
	            if (sVinesSelecc.codePointAt(cnt) == 13){
	                intPosFin = cnt;
	            }//Inf sVinFinal.codePointAt(cnt) != 10 && sVinFinal.codePointAt(cnt) != 13
	            if (sVinesSelecc.codePointAt(cnt) == 10){
	                intPosFin2 = cnt;
	                bExiste10 = true;
	            }//Inf sVinFinal.codePointAt(cnt) != 10 && sVinFinal.codePointAt(cnt) != 13
	            String sVinFinal = '';   
	            if (bExiste10 || Test.isRunningTest()){
	                System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines intPosIni:' + intPosIni + ' intPosFin: ' + intPosFin);                
	                sVinFinal = sVinesSelecc.substring(intPosIni, intPosFin);
	                intPosIni = intPosFin2 + 1;
	                bExiste10 = false; 
	                if (inyCntReg < lwrpRegDod.size())                
	                    lwrpRegDod.get(inyCntReg).objDODSolPedEsp.TAM_DummyVin__c = sVinFinal;
	                else
	                    break;                    
	                inyCntReg++;
	                System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVinFinal:' + sVinFinal + ' ' + sVinFinal.length());
	            }//Fin si bExiste
	        }//Fin del for para sVin
	        
	        /*System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVin: ' + sVin + ' length: ' + sVin.length() +  ' intPosIni:' + intPosIni + ' intPosFin: ' + intPosFin);
	        String sVinFinal = ''; //sVinFinalPaso.substring(intPosIni, intPosFin); 
	        sVinFinal = sVin.substring(intPosIni, sVin.length());
	        System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVinFinal:' + sVinFinal + ' ' + sVinFinal.length());
	        lwrpRegDod.get(inyCntReg).objDODSolPedEsp.TAM_DummyVin__c = sVinFinal;*/
	
	        /*String[] sArrVines;
	        Integer inyCntReg = 0;
	        if (sVinesSelecc.contains(',')){
	            sArrVines = sVinesSelecc.split(',');
	            //separa los vines para que los actualices en la lista de lwrpRegDod
	            for(String sVin : sArrVines){
	                if (inyCntReg < lwrpRegDod.size()){
	                    String sVinFinalPaso = ''; //substring(0,19);
	                    Integer intPosIni = 0;
	                    Integer intPosFin = 0;
	                    Boolean bExiste = true;
	                    for (Integer cnt=0; cnt < sVin.length(); cnt++){
	                        if (sVin.codePointAt(cnt) != 10 && sVin.codePointAt(cnt) != 13){
	                           intPosIni = cnt;
	                           bExiste = true;
	                           break;
	                        }//Inf sVinFinal.codePointAt(cnt) != 10 && sVinFinal.codePointAt(cnt) != 13
	                        System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVin.codePointAt(cnt): ' + sVin.codePointAt(cnt));
	                    }//Fin del for para sVin
	                    System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVin: ' + sVin + ' length: ' + sVin.length() +  ' intPosIni:' + intPosIni + ' intPosFin: ' + intPosFin);
	                    String sVinFinal = '';
	                    //sVinFinal = sVin.substring(intPosIni, sVin.length());
	                    System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines sVinFinal:' + sVinFinal + ' ' + sVinFinal.length());
	                    lwrpRegDod.get(inyCntReg).objDODSolPedEsp.TAM_DummyVin__c = sVinFinal;
	                }//Fin si inyCntReg < lwrpRegDod.size()
	                inyCntReg++;
	            }//Fin del for para sArrVines
	        }//Fin si sVinesSelecc.contains(',')
	        */
	        System.debug('En TAM_CargaMasivaVinesDodCtrl.seleccionarVines lwrpRegDod: ' + lwrpRegDod);        
            blnEstatusOk = true;    
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Los datos se actualizarón con exito.'));
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        //Solo null
        return null;
        
    }

    public PageReference guardar(){
        System.debug('En TAM_CargaMasivaVinesDodCtrl.guardar...'); 
        Boolean blnError = false;
        String sErrorDet = '';
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapObjDodPedEsp = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();
        //Toma la lista de los reg que tienen un VIN y actualizalos
        for (wrpRegDod objWrpRegDod : lwrpRegDod){
            if (objWrpRegDod.objDODSolPedEsp.TAM_DummyVin__c != null){
                mapObjDodPedEsp.put(objWrpRegDod.objDODSolPedEsp.TAM_IdExterno__c, new TAM_DODSolicitudesPedidoEspecial__c(
                        TAM_IdExterno__c = objWrpRegDod.objDODSolPedEsp.TAM_IdExterno__c,
                        TAM_DummyVin__c = objWrpRegDod.objDODSolPedEsp.TAM_DummyVin__c
                    )
                );                
            }
        }

        //Actualiza los datos del candidato
        List<Database.UpsertResult> lDtSvr = Database.upsert(mapObjDodPedEsp.values(), TAM_DODSolicitudesPedidoEspecial__c.TAM_IdExterno__c, false);
        for (Database.UpsertResult objDtSvr : lDtSvr){
	        if (!objDtSvr.isSuccess()){
	            sErrorDet = 'Error: ' + objDtSvr.getErrors()[0].getMessage();
	            blnError = true;
	        }//Fin !objDtSvr.isSuccess()
        }//Fin del for para lDtSvr
        System.debug('En TAM_CargaMasivaVinesDodCtrl.guardar sErrorDet: ' + sErrorDet); 

        //Hubo error
        if (blnError)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sErrorDet));
        else
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Los datos se actualizaron con éxito.'));                
        //Solo null
        return null;
    }

    public PageReference regresar(){
        System.debug('En TAM_CargaMasivaVinesDodCtrl.regresar...');
        String sUrlVista = System.Label.TAM_UrlCargaMasivaVines;
        PageReference targetVista = new PageReference(sUrlVista);
        targetVista.setRedirect(true);
        //Vamos a regresar a la ruta de la lista de pendientes por default https://toyotamexico--qa.lightning.force.com/lightning/o/TAM_DODSolicitudesPedidoEspecial__c/list?filterName=00B1Y0000085DA1UAM        
        return targetVista;
    }
    
    public class wrpRegDod{
        public TAM_DODSolicitudesPedidoEspecial__c objDODSolPedEsp{get;set;}
        public String sFecha{get;set;}
        public Integer intNoReg{get;set;}
        
        public wrpRegDod(TAM_DODSolicitudesPedidoEspecial__c objDODSolPedEsp, String sFecha, Integer intNoReg){
            this.objDODSolPedEsp = objDODSolPedEsp;
            this.sFecha = sFecha;
            this.intNoReg = intNoReg;
        }
    }
    
}