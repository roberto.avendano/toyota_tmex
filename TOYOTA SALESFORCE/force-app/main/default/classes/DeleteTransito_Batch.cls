global class DeleteTransito_Batch implements Database.Batchable<sObject> {
    
    private String query;
    private List<Inventario_en_Transito__c> inventariosList;
    
    global DeleteTransito_Batch(String q) {
        this.query = q;
        this.inventariosList = new List<Inventario_en_Transito__c>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);

    } 

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(scope.size() > 0){
            inventariosList.addAll((List<Inventario_en_Transito__c>)scope);

            try{
                delete inventariosList;
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('Deleted Transito Batch finished');
    }
    
}