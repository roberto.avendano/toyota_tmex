@isTest
public class TAM_SolicitudAutoDemoTriggerHandler_Test {
    //Se crea la data necesaria para poder crear la solicitud de auto demo
    @testSetup static void setup() {
        //Se crea el Vehiculo relacionado 
        Vehiculo__c v01 = new Vehiculo__c(
            Name='JTDKARFU2L3122771',
            Id_Externo__c='JTDKARFU2L3122771'
        );
        insert v01;
        
        //Dealer de prueba
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordtypeId = recordTypeDistribuidor
        );
        insert a01;
        
        //Se crea la serie 
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        //Se crea el modelo
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        //Se crea el movimiento relacionado
        Movimiento__c m05 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            Sale_Date__c = date.today(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '04',
            Fleet__c = 'C'
        ); 
        insert m05;
        
        //Se crea la factura relacionada
        Factura__c factVIN  = new Factura__c();
        factVIN.VIN__c = v01.Id;
        factVIN.AnioModelo__c = 2020;
        factVIN.Distribuidor__c = a01.Id;
        factVIN.Name = 'TEST FACTURA';
        factVIN.TAM_FechaTimbrado__c = date.today();
        factVIN.TAM_Folio__c = 'TESTA001';
        factVIN.TAM_Importe__c = 25000;
        insert factVIN;
        
        //Se inserta el custom Setting de días de vigencia
		TAM_DetalleEdoCta__c customSetting1 = new TAM_DetalleEdoCta__c();
        customSetting1.TAM_DiasVigencia__c = 97;
        insert customSetting1;
        
    }
    
    //Metodo para insertar una solicitud de auto demo
    @isTest static void testMethod1(){
        
        //Se obtiene el id del VIN
        Vehiculo__c idVIN = [SELECT ID FROM Vehiculo__c WHERE Name = 'JTDKARFU2L3122771'];
        
        //Se obtiene el movimiento
        Movimiento__c idMov = [SELECT ID FROM Movimiento__c WHERE VIN__r.name = 'JTDKARFU2L3122771'];
        
        //Se obtiene la factura de prueba
        Factura__c idFact = [SELECT ID FROM Factura__c WHERE VIN__r.name = 'JTDKARFU2L3122771'];
        
        TAM_SolicitudAutoDemo__c solAutoDemo = new  TAM_SolicitudAutoDemo__c();
        solAutoDemo.TAM_AnioModelo__c = '2020';
        solAutoDemo.TAM_CodigoDistribuidor__c = '57605';
        solAutoDemo.TAM_CodigoVenta__c = '06';
        solAutoDemo.TAM_CostoTMEXSinIVA__c = 5172.413793103448;
        solAutoDemo.TAM_EdoCuenta__c =  false;
        solAutoDemo.TAM_FechaVenta__c = date.today();
        solAutoDemo.TAM_IncentivoDealerDefinido__c = 6000.0;
        solAutoDemo.TAM_IncentivoDefinidoPoliticas__c = 12000.0;
        solAutoDemo.TAM_IncentivoOtorgado__c = -92462.0;
        solAutoDemo.TAM_IncentivoTMEXDefinido__c = 6000;
        solAutoDemo.TAM_Modelo__c = '7497';
        solAutoDemo.TAM_NombreDistribuidor__c = 'TOYOTA TEST';
        solAutoDemo.TAM_ParticipacionTMEX__c = 50;
        solAutoDemo.TAM_PrecioLista__c = 266900.0;
        solAutoDemo.TAM_PrecioMenosIncentivo__c = 254900.0; 
        solAutoDemo.TAM_Serie__c = 'HILUX';
        solAutoDemo.TAM_ValorFactura__c = 359362.0;
        solAutoDemo.TAM_VIN__c = idVIN.id;
        solAutoDemo.TAM_Movimiento__c = idMov.id;
        solAutoDemo.TAM_Factura__c = idFact.id;
        try{
          system.debug('registro a insertar'+solAutoDemo);
                 Test.startTest();
          insert solAutoDemo;  
               Test.stopTest();
            system.debug('id registro'+solAutoDemo);
        }catch(exception e){
        	system.debug('error al insertar' + e.getMessage());
            
        }     
        
    }
    
}