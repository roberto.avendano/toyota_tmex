/** 
Descripción General: Pantalla de provición de incetivos a distribuidores.
________________________________________________________________
Autor                   Fecha               Descripción
________________________________________________________________
Cecilia Cruz            06/Ene/2020         Versión Inicial
________________________________________________________________
**/
public without sharing class TAM_ProvisionRetailControllerClass {
    
    /** SIN REVERSA */
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getRetailSinReversa(String recordId){
        Id RecordTypeId = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  TAM_VIN__r.Name,
                                                                      		  TAM_VIN__r.Id,
                                                                              TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                              TAM_CodigoDealer__c,
                                                                              TAM_NombreDealer__c,
                                                                              TAM_CodigoVenta__c,
                                                                              TAM_Serie__c,
                                                                              TAM_Modelo__c,
                                                                              TAM_AnioModelo__c,
                                                                              TAM_FechaEnvio__c,
                                                                              TAM_TipoMovimiento__c,
                                                                              TAM_FirstName__c,
                                                                              TAM_LastName__c,
                                                                              CantidadTotalConReversa__c,
                                                                              TAM_PoliticaIncentivos__c,
                                                                      		  TAM_PoliticaIncentivos__r.Id,
                                                                              TAM_PoliticaIncentivos__r.Name,
                                                                      		  TAM_PoliticaIncentivos__r.TAM_FinVigencia__c,
                                                                      		  TAM_Version__c,
                                                                      		  TAM_FechaVenta__c,
                                                                              TAM_Factura__c,
                                                                              TAM_IncentivoTMEX_PF__c,
                                                                              TAM_IncentivoTMEX_Efectivo__c,
                                                                              TAM_ProductoFinanciero__c,
                                                                              TAM_ProvicionarEfectivo__c,
                                                                              TAM_TransmitirDealer__c,
                                                                              TAM_ProvisionarProdFinanciero__c,
                                                                              TAM_IdExterno__c,
                                                                              TAM_AplicanAmbos__c,
                                                                              TAM_TMEXEfectivoMenor__c,
                                                                              TAM_FechaCierre__c,
                                                                              TAM_ProvisionCerrada__c,
                                                                              TAM_CodigoContable__c
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Sin Reversa'
                                                                      AND     RecordTypeId =: RecordTypeId
                                                                      ORDER BY TAM_AnioModelo__c ASC,TAM_Modelo__c ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        return lstDetalleProvision;
    }

    /*GUARDADO DE REGISTROS*/
    @AuraEnabled
    public static Boolean setRecordsRetail(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
                
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
            return true;
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return false;
        }        
    } 

    /*CERRAR PROVISION*/
    @AuraEnabled
    public static Boolean cerrarProvision(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        TAM_CerrarProvisionQueueable updateJob = new TAM_CerrarProvisionQueueable(lstDetalleProvision, recordId, 'Retail');
        ID jobID = System.enqueueJob(updateJob);
        if(jobID != null){
            return true;
        } else{
            return false;
        }
    }
}