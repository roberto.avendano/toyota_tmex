/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de respuesta para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_RespQuiterWebService {

    public class objRespQuiterWebServ{
        @AuraEnabled 
        public Boolean Error {get;set;}        
        @AuraEnabled        
        public String code {get;set;}
        @AuraEnabled        
        public String message {get;set;}
        @AuraEnabled        
        public String JsonRes {get;set;}
        
        public objRespQuiterWebServ(){
            this.Error = false;
            this.code = '';
            this.message = '';
            this.JsonRes = '';
        }
          
        public objRespQuiterWebServ(Boolean Error, String code, String message,
            String JsonRes){
            this.Error = Error;
            this.code = code;
            this.message = message;
            this.JsonRes = JsonRes;            
        }
    }    
    
}