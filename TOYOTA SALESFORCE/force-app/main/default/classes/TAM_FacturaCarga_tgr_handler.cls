/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del archivo de facturas

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    20-Junio-2019     Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_FacturaCarga_tgr_handler extends TriggerHandler{

    private List<TAM_FacturaCarga__c> itemsNew;
    private Map<Id, TAM_FacturaCarga__c> mapItemsOld;    
    private Map<String, Factura__c> mapFacturasUps;
    private Map<String, String> mapIdVinIdFacturasUps;    
    private Map<String, Factura__c> mapIdFactNumeroUps;    
    private Map<String, Set<String>> mapVinSetIdOpp;
    private Map<String, Set<TAM_OportunidadFactura__c>> mapVinSetIdOppFact;
    private Map<String, String> mapVinIdVehiculo;    
    private Map<String, TAM_OportunidadFactura__c> mapVinObjOppFactIns;    
    private Set<String> setVin;
    private Set<String> setIdDealer;
    private Map<String, Account> mapNoDistrObjDist;    
    private Map<String, String> mapVinIdCte;    
    private Map<String, String> mapVinNoCte;  
    private Map<String, Set<String>> mapNoCteVin;
    private Map<String, Contact> mapContactosUpsCtaPrinc;
	private	Set<String> setIdContactCreados;
	private	Set<String> setErroresContactosUps;		    	
        
	//Un constructor por default
	public TAM_FacturaCarga_tgr_handler() {
        this.itemsNew = (List<TAM_FacturaCarga__c>) Trigger.new;
        this.mapItemsOld = (Map<Id, TAM_FacturaCarga__c>) Trigger.oldMap;
        this.mapVinSetIdOpp = new Map<String, Set<String>>();
        this.mapVinSetIdOppFact = new Map<String, Set<TAM_OportunidadFactura__c>>();
        this.mapVinIdVehiculo = new Map<String, String>();
        this.mapIdVinIdFacturasUps = new Map<String, String>();
        this.mapIdFactNumeroUps = new Map<String, Factura__c>();
        this.mapVinObjOppFactIns = new Map<String, TAM_OportunidadFactura__c>();
        this.mapFacturasUps = new Map<String, Factura__c>();		
        this.setVin = new Set<String>();
        this.setIdDealer = new Set<String>();    
        this.mapNoDistrObjDist = new Map<String, Account>(); 
        this.mapVinIdCte = new Map<String, String>();
        this.mapVinNoCte = new Map<String, String>();
        this.mapNoCteVin = new Map<String, Set<String>>();
    	this.mapContactosUpsCtaPrinc = new Map<String, Contact>();
        this.setIdContactCreados = new Set<String>();
        this.setErroresContactosUps = new Set<String>();
	}

	//Sobreescribe el metodo de afterInsert
    public override void afterInsert() {
		System.debug('EN TAM_FacturaCarga_tgr_handler.afterInsert....');
		actualizaDatosOportunidad(this.itemsNew, this.mapItemsOld);
        //actualizaDatosLead(this.itemsNew);
		eliminaRecords();
    }

    //Sobreescribe el metodo de afterInsert
    public override void afterUpdate() {
        System.debug('EN TAM_FacturaCarga_tgr_handler.afterInsert....');
        //actualizaDatosLead(this.itemsNew);
    }

    //Funcion que sirve para actualizar las listas de precio que se estan crgando en TAM_CatalogoProductos__c
    private void actualizaDatosOportunidad(List<TAM_FacturaCarga__c> itemsNew, Map<Id, TAM_FacturaCarga__c> mapItemsOld){
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad itemsNew: ' + itemsNew);
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapItemsOld: ' + mapItemsOld);		
		
    	String VaRtContRegPF = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contactos-Clientes').getRecordTypeId();    	
		
		Map<String, TAM_LeadInventarios__c> MapIdLeadInvObjLeadInv = new Map<String, TAM_LeadInventarios__c>();
		Map<String, Lead> MapIdLeadObjLead = new Map<String, Lead>();
		
		//Recorre la lista de reg que se estan insertando y actualizaos en Factura__c
		for (TAM_FacturaCarga__c objTAMFacturaCarga : itemsNew){
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad objTAMFacturaCarga: ' + objTAMFacturaCarga);			
			//Crea el objeto del tipo Factura__c
			if (objTAMFacturaCarga.VIN__c != null && objTAMFacturaCarga.TAM_UUID__c != null){
				//CreA la llave compuesta con el numero de factura
				String sLlave = objTAMFacturaCarga.VIN__c + '-' +  objTAMFacturaCarga.TAM_Numero__c;
				setVin.add(objTAMFacturaCarga.VIN__c);
				if (objTAMFacturaCarga.TAM_IdDealerCode__c != null)
					setIdDealer.add(objTAMFacturaCarga.TAM_IdDealerCode__c);
					mapFacturasUps.put(sLlave, new Factura__c(TAM_UUID__c = objTAMFacturaCarga.TAM_UUID__c,
						Name = sLlave, 
						TAM_NombreArchivo__c = objTAMFacturaCarga.TAM_NombreArchivo__c,
						TAM_IdDealerCode__c = objTAMFacturaCarga.TAM_IdDealerCode__c,
						TAM_Total__c = objTAMFacturaCarga.TAM_Total__c,
						TAM_TipoDeComprobante__c = objTAMFacturaCarga.TAM_TipoDeComprobante__c,
						TAM_SubTotal__c = objTAMFacturaCarga.TAM_SubTotal__c,
						TAM_Serie__c = objTAMFacturaCarga.TAM_Serie__c, 
						TAM_LugarExpedicion__c = objTAMFacturaCarga.TAM_LugarExpedicion__c,
						TAM_MetodoPago__c = objTAMFacturaCarga.TAM_MetodoPago__c,
						TAM_Moneda__c = objTAMFacturaCarga.TAM_Moneda__c,
						TAM_FormaPago__c = objTAMFacturaCarga.TAM_FormaPago__c,
						TAM_Folio__c = objTAMFacturaCarga.TAM_Folio__c,
						TAM_FechaFolio__c = objTAMFacturaCarga.TAM_FechaFolio__c,
						TAM_CondicionesDePago__c = objTAMFacturaCarga.TAM_CondicionesDePago__c,
						TAM_RfcEmisor__c = objTAMFacturaCarga.TAM_RfcEmisor__c,
						TAM_RegimenFiscal__c = objTAMFacturaCarga.TAM_RegimenFiscal__c,
						TAM_NombreEmisor__c = objTAMFacturaCarga.TAM_NombreEmisor__c,
						TAM_RfcReceptor__c = objTAMFacturaCarga.TAM_RfcReceptor__c,
						TAM_UsoCFDI__c = objTAMFacturaCarga.TAM_UsoCFDI__c,
						TAM_NombreReceptor__c = objTAMFacturaCarga.TAM_NombreReceptor__c,
						TAM_ClaveVehicular__c = objTAMFacturaCarga.TAM_ClaveVehicular__c,
						TAM_Numero__c = objTAMFacturaCarga.TAM_Numero__c,
						TAM_FechaAduana__c = objTAMFacturaCarga.TAM_FechaAduana__c != null ? getFecha(objTAMFacturaCarga.TAM_FechaAduana__c) : null, //Validar
						TAM_Aduana__c = objTAMFacturaCarga.TAM_Aduana__c,
						TAM_FechaTimbrado__c = objTAMFacturaCarga.TAM_FechaTimbrado__c,
						TAM_UUIDRelacionado__c = objTAMFacturaCarga.TAM_UUIDRelacionado__c != null ? objTAMFacturaCarga.TAM_UUIDRelacionado__c : null, //VALIDAR NULL
						TAM_UUIDRelacionado1__c = objTAMFacturaCarga.TAM_UUIDRelacionado1__c != null ? objTAMFacturaCarga.TAM_UUIDRelacionado1__c : null, //VALIDAR NULL
						TAM_UUIDRelacionado2__c = objTAMFacturaCarga.TAM_UUIDRelacionado2__c != null ? objTAMFacturaCarga.TAM_UUIDRelacionado2__c : null, //VALIDAR NULL
						TAM_ValorUnitario__c = objTAMFacturaCarga.TAM_ValorUnitario__c,
						TAM_Unidad__c = objTAMFacturaCarga.TAM_Unidad__c,
						TAM_NoIdentificacion__c = objTAMFacturaCarga.TAM_NoIdentificacion__c != null ? objTAMFacturaCarga.TAM_NoIdentificacion__c : null, //VALIDAR NULL
						TAM_Importe__c = objTAMFacturaCarga.TAM_Importe__c,
						TAM_Descripcion__c = objTAMFacturaCarga.TAM_Descripcion__c, //COMENTARIOS
						TAM_ClaveUnidad__c = objTAMFacturaCarga.TAM_ClaveUnidad__c != null ? objTAMFacturaCarga.TAM_ClaveUnidad__c : null,
						TAM_ClaveProdServ__c = objTAMFacturaCarga.TAM_ClaveProdServ__c != null ? objTAMFacturaCarga.TAM_ClaveProdServ__c : null,
						TAM_Cantidad__c = objTAMFacturaCarga.TAM_Cantidad__c != null ? objTAMFacturaCarga.TAM_Cantidad__c : null,
						TAM_ImporteTraslado__c = (objTAMFacturaCarga.TAM_ImporteTraslado__c != 'NULL' && objTAMFacturaCarga.TAM_ImporteTraslado__c != null) ? Decimal.valueOf(objTAMFacturaCarga.TAM_ImporteTraslado__c) : 0.00, //COMENTARIOS
						TAM_TipoFactor__c = objTAMFacturaCarga.TAM_TipoFactor__c,
						TAM_TasaOCuota__c = (objTAMFacturaCarga.TAM_TasaOCuota__c != 'NULL' && objTAMFacturaCarga.TAM_TasaOCuota__c != null)? Decimal.valueOf(objTAMFacturaCarga.TAM_TasaOCuota__c) : 0.00, //COMENTARIOS
						TAM_Impuesto__c = objTAMFacturaCarga.TAM_Impuesto__c,
						TAM_Base__c = (objTAMFacturaCarga.TAM_Base__c != 'NULL' && objTAMFacturaCarga.TAM_Base__c != null) ? Decimal.valueOf(objTAMFacturaCarga.TAM_Base__c) : 0.00 //COMENTARIOS
					)
				);
				//Agrega el VIN con el nombre
				mapVinNoCte.put(objTAMFacturaCarga.VIN__c, objTAMFacturaCarga.TAM_NombreReceptor__c);
				//Inializa el mapa de mapNoCteVin
				if (mapNoCteVin.containsKey(objTAMFacturaCarga.TAM_NombreReceptor__c))
					mapNoCteVin.get(objTAMFacturaCarga.TAM_NombreReceptor__c).add(objTAMFacturaCarga.VIN__c);
				if (!mapNoCteVin.containsKey(objTAMFacturaCarga.TAM_NombreReceptor__c))
					mapNoCteVin.put(objTAMFacturaCarga.TAM_NombreReceptor__c, new Set<String>{objTAMFacturaCarga.VIN__c});				
			}//Fin si objTAMFacturaCarga.VIN__c != null && objTAMFacturaCarga.TAM_UUID__c != null
		}//Fin del for para itemsNew
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapFacturasUps.values(): ' + mapFacturasUps.values());
				
		//Consulta los reg de los vehiculos asociados a setVin
		for (Vehiculo__c OBJVehiculo : [Select v.Name, v.Id_Externo__c From Vehiculo__c v 
			Where Id_Externo__c IN :setVin]){
			mapVinIdVehiculo.put(OBJVehiculo.Id_Externo__c, OBJVehiculo.id);
		}
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapVinIdVehiculo: ' + mapVinIdVehiculo);

		//Consulta los reg de los Dealers asociados a setIdDealer
		for (Account objDealer : [Select id, Name, Codigo_Distribuidor__c From Account v 
			Where Codigo_Distribuidor__c IN :setIdDealer]){
			mapNoDistrObjDist.put(objDealer.Codigo_Distribuidor__c, objDealer);
		}
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapNoDistrObjDist: ' + mapNoDistrObjDist);
		
		//Crea los registros en el objeto de Factura__c
		for (String sFacturaPaso : mapFacturasUps.KeySet()){
			Factura__c objFacturaPaso = mapFacturasUps.get(sFacturaPaso);
			String sVinPasoFact = sFacturaPaso.contains('-') ? sFacturaPaso.substring(0,sFacturaPaso.indexOf('-')) : sFacturaPaso;
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad sVinPasoFact: ' + sVinPasoFact);			  
			if (mapVinIdVehiculo.containsKey(sVinPasoFact))
				objFacturaPaso.VIN__c = mapVinIdVehiculo.get(sVinPasoFact);
			//Actualiza el campo del distribuidor
			if (mapNoDistrObjDist.containsKey(objFacturaPaso.TAM_IdDealerCode__c))
				objFacturaPaso.Distribuidor__c = mapNoDistrObjDist.get(objFacturaPaso.TAM_IdDealerCode__c).id;
		}
		System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapFacturasUps2: ' + mapFacturasUps);
		
		//Un objeto del tipo Savepoint sp para el rollback en caso de que haya un error
		Savepoint sp = Database.setSavepoint();
		try{
					
			Integer iCmtReg = 0;
			//Muy bien ya tienes los reg de las facturas, crealos
			if (!mapFacturasUps.isEmpty()){
				List<Factura__c> lFacturaUps = mapFacturasUps.values();
				List<Database.UpsertResult> lDtSvr = Database.upsert(lFacturaUps, Factura__c.TAM_UUID__c, false);
				for (Database.UpsertResult objDtSvr : lDtSvr){
					if(!objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Error al la hora de crear la factura: ' + objDtSvr.getErrors()[0].getMessage());
					if(objDtSvr.isSuccess()){
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad factura el ID: ' + objDtSvr.getId());
						String sIdLlavePaso = lFacturaUps.get(iCmtReg).Name;
						String sVinPasoFact = sIdLlavePaso.contains('-') ? sIdLlavePaso.substring(0,sIdLlavePaso.indexOf('-')) : sIdLlavePaso;
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad sVinPasoFact: ' + sVinPasoFact);					
						mapIdVinIdFacturasUps.put(sVinPasoFact, objDtSvr.getId());
						mapIdFactNumeroUps.put(objDtSvr.getId(), lFacturaUps.get(iCmtReg));
					}//Fin si objDtSvr.isSuccess()
					iCmtReg++;									
				}//Fin del for para lDtSvr
			}//Fin si !mapFacturasUps.isEmpty()
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapIdVinIdFacturasUps: ' + mapIdVinIdFacturasUps);
			
			/*//Busca los vines mapIdVinIdFacturasUps.keySet() en la table de TAM_LeadInventarios__c
			for (TAM_LeadInventarios__c objLeaInv : [Select id, Name, TAM_VINFacturacion__c, TAM_Prospecto__c 
				From TAM_LeadInventarios__c Where Name IN :mapIdVinIdFacturasUps.keySet() OR TAM_VINFacturacion__c IN :mapIdVinIdFacturasUps.keySet()]){
				String sIdFactura = objLeaInv.TAM_VINFacturacion__c != null 
							? mapIdVinIdFacturasUps.get(objLeaInv.TAM_VINFacturacion__c) 
							: mapIdVinIdFacturasUps.get(objLeaInv.Name);
				//Agrega los reg a la lista de MapIdLeadInvObjLeadInv
				MapIdLeadInvObjLeadInv.put(objLeaInv.id, new TAM_LeadInventarios__c(
						id = objLeaInv.id,
						Factura__c = sIdFactura
					)
				);
				//Crea el objeto del tipo Lead
				MapIdLeadObjLead.put(objLeaInv.TAM_Prospecto__c, new Lead(id = objLeaInv.TAM_Prospecto__c, 
						TAM_PedidoFacturado__c = true,
						status = 'Facturado',
						FWY_Estado_del_candidatoWEB__c = 'Facturado',
						TAM_FechaFacturacionDealer__c = Date.today()
					)
				);
			}
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad MapIdLeadInvObjLeadInv: ' + MapIdLeadInvObjLeadInv.KeySet());
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad MapIdLeadInvObjLeadInv: ' + MapIdLeadInvObjLeadInv.Values());
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad MapIdLeadObjLead: ' + MapIdLeadObjLead.KeySet());
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad MapIdLeadObjLead: ' + MapIdLeadObjLead.Values());

			//Ve si tiene algo el mapa de MapIdLeadInvObjLeadInv
			if (!MapIdLeadInvObjLeadInv.isEmpty()){
				List<TAM_LeadInventarios__c> lLeadInvUps = MapIdLeadInvObjLeadInv.values();
				List<Database.UpsertResult> lDtSvr = Database.upsert(lLeadInvUps, TAM_LeadInventarios__c.id, false);
				for (Database.UpsertResult objDtSvr : lDtSvr){
					if(!objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Error al la hora de act el Inv Lead: ' + objDtSvr.getErrors()[0].getMessage());
					if(objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Inv Lead - opp el ID: ' + objDtSvr.getId());									
				}//Fin del for para lDtSvr
			}//Fin si !MapIdLeadInvObjLeadInv.isEmpty()
			
			//Ve si tiene algo el mapa de MapIdLeadObjLead
			if (!MapIdLeadObjLead.isEmpty()){
				List<Lead> lLeadInvUps = MapIdLeadObjLead.values();
				List<Database.UpsertResult> lDtSvr = Database.upsert(lLeadInvUps, Lead.id, false);
				for (Database.UpsertResult objDtSvr : lDtSvr){
					if(!objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Error al la hora de act Lead: ' + objDtSvr.getErrors()[0].getMessage());
					if(objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad act Lead el ID: ' + objDtSvr.getId());									
				}//Fin del for para lDtSvr
			}//Fin si !MapIdLeadObjLead.isEmpty()
			*/
			
			//Muy bien ya tienes lo datos de los VIN´S busca las oportunidades relacionada a estos vins
			for (Opportunity  objOpportunity : [Select id, TAM_Vin__c, AccountId From Opportunity 
				Where TAM_Vin__c IN :setVin]){
				//Metelo al mapa de mapVinSetIdOpp
				if (mapVinSetIdOpp.containsKey(objOpportunity.TAM_Vin__c))
					mapVinSetIdOpp.get(objOpportunity.TAM_Vin__c).add(objOpportunity.id);
				if (!mapVinSetIdOpp.containsKey(objOpportunity.TAM_Vin__c))
					mapVinSetIdOpp.put(objOpportunity.TAM_Vin__c, new Set<String>{objOpportunity.id});				
				//Ve si tiene cliente asociado el vin en  el campo AccountId
				if (objOpportunity.AccountId != null)
					mapVinIdCte.put(objOpportunity.TAM_Vin__c, objOpportunity.AccountId);
			}//Fin del for para la consulta de objOpportunity 		
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapVinSetIdOpp: ' + mapVinSetIdOpp);
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapVinIdCte: ' + mapVinIdCte);
			
			for (TAM_OportunidadFactura__c obTAMOportunidadFactura : [Select id, TAM_Oportunidad__r.TAM_Vin__c, 
				TAM_Oportunidad__c, TAM_Factura__c From TAM_OportunidadFactura__c Where TAM_Oportunidad__r.TAM_Vin__c IN :setVin]){
				if (mapVinSetIdOppFact.containsKey(obTAMOportunidadFactura.TAM_Oportunidad__r.TAM_Vin__c))
					mapVinSetIdOppFact.get(obTAMOportunidadFactura.TAM_Oportunidad__r.TAM_Vin__c).add(obTAMOportunidadFactura);
				if (!mapVinSetIdOppFact.containsKey(obTAMOportunidadFactura.TAM_Oportunidad__r.TAM_Vin__c))					
					mapVinSetIdOppFact.put(obTAMOportunidadFactura.TAM_Oportunidad__r.TAM_Vin__c, new Set<TAM_OportunidadFactura__c>{obTAMOportunidadFactura});
			}//Fin del for para el select de TAM_OportunidadFactura__c
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapVinSetIdOppFact: ' + mapVinSetIdOppFact);
						
			//Recorre la lista de setVin y ve cuales no estan asociadoa una opp en mapVinSetIdOpp
			for (String sIdVin : setVin){
				//Ve si existe el Nip en mapVinSetIdOpp
				if (mapVinSetIdOpp.containsKey(sIdVin)){
					//Ve si sIdVin existe en mapVinSetIdOppFact
					if (mapVinSetIdOppFact.containsKey(sIdVin)){
						//Toma las opp asociadas a mapVinSetIdOppFact.containsKey(sIdVin)
						Set<TAM_OportunidadFactura__c> listOpp = mapVinSetIdOppFact.get(sIdVin);
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad listOpp: ' + listOpp);						
						//Recorre la lista de Opp asociadas a mapVinSetIdOpp.containsKey(sIdVin)
						for (String sIdOpp : mapVinSetIdOpp.get(sIdVin)){
							//Ve si existe el VIN existe en mapIdVinIdFacturasUps
							if (mapIdVinIdFacturasUps.containsKey(sIdVin)){
								Boolean bExiste = false; 
								String sIdFactura = mapIdVinIdFacturasUps.get(sIdVin);								
								//Ve si existe el id de la OPP y el ID de la Facrura en listOpp
								for (TAM_OportunidadFactura__c objFacturaOpp : listOpp){
									System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad sIdOpp: ' + sIdOpp + ' TAM_Oportunidad__c: ' + objFacturaOpp.TAM_Oportunidad__c + ' sIdFactura: ' + sIdFactura + ' TAM_Factura__c: ' + objFacturaOpp.TAM_Factura__c);									
									if (sIdOpp == objFacturaOpp.TAM_Oportunidad__c && sIdFactura == objFacturaOpp.TAM_Factura__c){
										bExiste = !Test.isRunningTest() ? true : false;
										Break;
									}//Fin si sIdOpp == objFacturaOpp.TAM_Oportunidad__c && sIdFactura == objFacturaOpp.TAM_Factura__c
								}//Fin del for para 
								//Ve si ya existe la factura asociada a la opp
								if (!bExiste){
									//Crea el objeto del tipo TAM_OportunidadFactura__c
									TAM_OportunidadFactura__c objTAMOportunidadFacturaPaso = new TAM_OportunidadFactura__c();
									objTAMOportunidadFacturaPaso.Name = sIdVin + ' - ' + mapIdFactNumeroUps.get(sIdFactura).TAM_Numero__c;
									objTAMOportunidadFacturaPaso.TAM_Factura__c =  sIdFactura;
									objTAMOportunidadFacturaPaso.TAM_Oportunidad__c = sIdOpp;
									String sIdExterno = sIdOpp + '-' +  sIdFactura;
									//Metelo a la lista de mapVinObjOppFactIns
									mapVinObjOppFactIns.put(sIdExterno, objTAMOportunidadFacturaPaso);
								}//Fin si !bExiste
							}//Fin si mapIdVinIdFacturasUps.containsKey(sIdVin)
						}//Fin del for para mapVinSetIdOpp.get(sIdVin)
					}//Fin si mapVinSetIdOppFact.containsKey(sIdVin)
					if (!mapVinSetIdOppFact.containsKey(sIdVin)){
						//Recorre la lista de Opp asociadas a mapVinSetIdOpp.containsKey(sIdVin)
						for (String sIdOpp : mapVinSetIdOpp.get(sIdVin)){
							//Ve si existe el VIN existe en mapIdVinIdFacturasUps
							if (mapIdVinIdFacturasUps.containsKey(sIdVin)){
								String sIdFactura = mapIdVinIdFacturasUps.get(sIdVin);								
								//Crea el objeto del tipo TAM_OportunidadFactura__c
								TAM_OportunidadFactura__c objTAMOportunidadFacturaPaso = new TAM_OportunidadFactura__c();
								objTAMOportunidadFacturaPaso.Name = sIdVin + ' - ' + mapIdFactNumeroUps.get(sIdFactura).TAM_Numero__c;
								objTAMOportunidadFacturaPaso.TAM_Factura__c =  sIdFactura;
								objTAMOportunidadFacturaPaso.TAM_Oportunidad__c = sIdOpp;
								String sIdExterno = sIdOpp + '-' +  sIdFactura;
								//Metelo a la lista de mapVinObjOppFactIns
								mapVinObjOppFactIns.put(sIdExterno, objTAMOportunidadFacturaPaso);
							}//Fin si mapIdVinIdFacturasUps.containsKey(sIdVin)
						}//Fin del for para mapVinSetIdOpp.get(sIdVin)
					}//Fin si !mapVinSetIdOppFact.containsKey(sIdVin)
				}//Fin si mapVinSetIdOpp.containsKey(sIdVin)
			}//Fin el for para setVin

			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapVinObjOppFactIns: ' + mapVinObjOppFactIns);			
			//Ve si tiene algo el mapa de mapVinObjOppFactIns
			if (!mapVinObjOppFactIns.isEmpty()){
				List<TAM_OportunidadFactura__c> lFacturaUps = mapVinObjOppFactIns.values();
				List<Database.SaveResult> lDtSvr = Database.insert(lFacturaUps, false);
				for (Database.SaveResult objDtSvr : lDtSvr){
					if(!objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Error al la hora de crear la factgura - opp: ' + objDtSvr.getErrors()[0].getMessage());
					if(objDtSvr.isSuccess())
						System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad factura - opp el ID: ' + objDtSvr.getId());									
				}//Fin del for para lDtSvr
			}//Fin si !mapVinObjOppFactIns.isEmpty()

			//Recorre los VINES del mapa mapVinIdCte
			for (String sVin : mapVinIdCte.KeySet()){
				//Busca el VIN en el mapa de 
				String sNombreCte = mapVinNoCte.containsKey(sVin) ? mapVinNoCte.get(sVin) : null;
				//Crea el objeto del tipo Contact
				mapContactosUpsCtaPrinc.put(sNombreCte.toUpperCase() + '-' + Label.TAM_IdCuentaPrincipalContactosCRM, new Contact(
						TAM_IdExternoNombre__c = sNombreCte.toUpperCase() + '-' + Label.TAM_IdCuentaPrincipalContactosCRM,
						FirstName = sNombreCte,
						//LastName = sLlaveNombreCompuestoCont[1] != null ? sLlaveNombreCompuestoCont[1].toUpperCase() : null,
						recordTypeId = VaRtContRegPF,
						LeadSource = 'Facturas',
						AccountId = Label.TAM_IdCuentaPrincipalContactosCRM
						//contactoPrincipal.TAM_IdExternoNombre__c =  sNombreContact + '-' + Label.TAM_IdCuentaPrincipalContactosCRM;
       				)
       			);
			}//Fin del for para mapVinIdCte.KeySet()
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapContactosUpsCtaPrinc: ' + mapContactosUpsCtaPrinc);

        	Integer icntUpsCtes = 0;
			if (!mapContactosUpsCtaPrinc.isEmpty()){
        		List<Contact> lContactosCtaPrinc = mapContactosUpsCtaPrinc.values();
        		//Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lContactosCtaPrinc, Contact.TAM_IdExternoNombre__c, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						setErroresContactosUps.add('En el contacto : ' + lContactosCtaPrinc.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
					if (objDtbUpsRes.isSuccess()){
						setIdContactCreados.add(objDtbUpsRes.getId());
          				System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad objDtbUpsRes.getId() Contacts: ' + objDtbUpsRes.getId());							
					}
					icntUpsCtes++;	
				}//Fin del for para lDtbUpsRes				
			}//Fin si !mapContactosUpsCtaPrinc.isEmpty()        						
			
			/*Map<String, String> mapIdContactosId = new Map<String, String>();
			//Consulta los clientes que se acaban de crear en setIdCtesCreados y metelos a mapIdClientesId
			for (Contact Contacto : [Select id, TAM_IdExternoNombre__c From Contact Where id IN: setIdContactCreados]){
       			String sIdExtPaso = Contacto.TAM_IdExternoNombre__c;
	   			String sNombreContact = sIdExtPaso.contains('-') ? sIdExtPaso.substring(0,sIdExtPaso.IndexOf('-')) : null;				
				mapIdContactosId.put(sNombreContact, Contacto.id);				
			}			
           	System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapIdContactosId: ' + mapIdContactosId);				
    		
	    	Map<String, AccountContactRelation> mapAccountContactRelationUps = new Map<String, AccountContactRelation>();    	
			Set<String> setErroresAccContacRelErr = new Set<String>();
			Set<String> setIdAccContacRelSucc = new Set<String>();    		
    		//Recrre el mapa de mapIdContactosId
			for (String sIdExtrnoPaso : mapIdContactosId.keySet()){				
	           	System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad sIdExtrnoPaso: ' + sIdExtrnoPaso);				
				//Busca el sIdExtrnoPaso en mapNoCteVin
				if (mapNoCteVin.containsKey(sIdExtrnoPaso)){
					Set<String> setVines = mapNoCteVin.get(sIdExtrnoPaso); 
					for (String sVinPaso : setVines){
						//Busca el vin en mapVinIdCte.put(objOpportunity.TAM_Vin__c, objOpportunity.AccountId);
						if (mapVinIdCte.containsKey(sVinPaso)){
							String sIdExternoFinal = sIdExtrnoPaso + '-' + mapVinIdCte.get(sVinPaso);
							//Crea el objeto del tipo AccountContactRelation
           					AccountContactRelation AccountContactRelationPaso = new AccountContactRelation(
		       					TAM_IdExterno__c = sIdExternoFinal,
		       					ContactId = mapIdContactosId.get(sIdExtrnoPaso),
		       					AccountId = mapVinIdCte.get(sVinPaso),
		       					TAM_VIN__c = sVinPaso 
		       				);
		       				//Agregala al mapa de mapAccountContactRelationUps
		       				mapAccountContactRelationUps.put(sIdExternoFinal, AccountContactRelationPaso);							
           					System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad TAM_IdExterno: ' + sIdExternoFinal + ' AccountContactRelationPaso: ' + AccountContactRelationPaso);
						}//Fin si mapVinIdCte.containsKey(sVinPaso)
					}//Fin del for para setVines
				}//Fin si mapNoCteVin.containsKey(sIdExtrnoPaso)
			}//Fin del for para mapIdContactosId.keySet()
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad mapAccountContactRelationUps: ' + mapAccountContactRelationUps);
			
        	icntUpsCtes = 0;			
			//Ve si tiene algo el mapa de  mapAccountContactRelationUps
			if (!mapAccountContactRelationUps.isEmpty()){
        		List<AccountContactRelation> lAccContacRel = mapAccountContactRelationUps.values();
        		//Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lAccContacRel, AccountContactRelation.TAM_IdExterno__c, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						setErroresAccContacRelErr.add('En TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad AccountContactRelation : ' + lAccContacRel.get(icntUpsCtes).TAM_VIN__c + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
					if (objDtbUpsRes.isSuccess())
						setIdAccContacRelSucc.add(objDtbUpsRes.getId());					
					icntUpsCtes++;	
				}//Fin del for para lDtbUpsRes				
			}//Fin si !mapAccountContactRelationUps.isEmpty()
           	System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad setErroresAccContacRelErr: ' + setErroresAccContacRelErr);        	
           	System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad setIdAccContacRelSucc: ' + setIdAccContacRelSucc);				
			*/
			
			//Para las pruebas nada mas
			//Database.rollback(sp);
		
		}catch(Exception ex){
			System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad Error general: ' + ex.getMessage() + ' ' + ex.getLineNumber());           			
			Database.rollback(sp);			
		}		
		
    }

	private void eliminaRecords(){
		System.debug('EN eliminaRecords...');	
		List<TAM_FacturaCarga__c> FacturasDel = new List<TAM_FacturaCarga__c>();
		String squery = 'SELECT Id FROM TAM_FacturaCarga__c WHERE CreatedDate < TODAY Order by CreatedDate ASC LIMIT 200';
		if (Test.isRunningTest())
			squery = 'SELECT Id FROM TAM_FacturaCarga__c Order by CreatedDate ASC LIMIT 1';
		System.debug('EN eliminaRecords squery: ' + squery);			
		List<SObject> lFacturasCarga = Database.query(squery);
		if (!lFacturasCarga.isEmpty()){
			for (SObject objFactTrans : lFacturasCarga){
				FacturasDel.add( (TAM_FacturaCarga__c) objFactTrans );
			}
		}//Fin si !lFacturasCarga.isEmpty()
		System.debug('EN eliminaRecords FacturasDel: ' + FacturasDel);		
		//Eliminalos
		if (!Test.isRunningTest())
			delete FacturasDel;
	}

    /*private void actualizaDatosLead(List<TAM_FacturaCarga__c> itemsNew){
    
        Set<String> setIdVinActCand = new Set<String>();
        map<String, Lead> mapLeadUpsUps = new map<String, Lead>();
        Set<String> setErroresLeadErr = new Set<String>();
        Set<String> setIdLeadSucc = new Set<String>();              

        map<String, TAM_LeadInventarios__c> mapLeadInvComs = new map<String, TAM_LeadInventarios__c>();
        map<String, TAM_FacturaCarga__c> mapVinNomCte = new map<String, TAM_FacturaCarga__c>();
        map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new map<String, TAM_LeadInventarios__c>();

        //Recorre la lista de reg que se estan nsertando
        for(TAM_FacturaCarga__c v: itemsNew){                
            //Toma el Vin para la busqueda del candidato asociado a ese vin
            if(v.VIN__c != null){
               //Agregalo a la mapa de mapVinNomCte
               mapVinNomCte.put(v.VIN__c, v); //v.TAM_NombreReceptor__c != null ? v.TAM_NombreReceptor__c : ''
            }
        }        
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.KeySet());         
        System.debug('EN actualizaDatosLead mapVinNomCte: ' + mapVinNomCte.Values()); 

        //Consulta los datos de los vines que ya estan cargados
        for (TAM_LeadInventarios__c objLeadInventarios : [Select t.id, t.TAM_VINFacturacion__c, t.TAM_Prospecto__c, t.Name 
            From TAM_LeadInventarios__c t Where (Name IN :mapVinNomCte.KeySet() OR TAM_VINFacturacion__c IN :mapVinNomCte.KeySet())
            And TAM_Prospecto__c != null]){
            Lead CandUpd = new Lead(
                id = objLeadInventarios.TAM_Prospecto__c, TAM_PedidoFacturado__c = true, 
                Status = 'Facturado', FWY_Estado_del_candidatoWEB__c = 'Facturado', TAM_PorcentajeAvance__c = '90%', 
                TAM_FechaFacturacionDealer__c = mapVinNomCte.containsKey(objLeadInventarios.Name) ? Date.valueOf(mapVinNomCte.get(objLeadInventarios.Name).TAM_FechaTimbrado__c) : null   
            );
            //mete los datos en el mapa de mapIdInveCand para mandar llamar el proceso ActualizaVinCandito
            mapLeadUpsUps.put(objLeadInventarios.TAM_Prospecto__c, CandUpd);     
            mapLeadInvComs.put(objLeadInventarios.Name, objLeadInventarios);
            if (objLeadInventarios.TAM_VINFacturacion__c != null)
                mapLeadInvComs.put(objLeadInventarios.TAM_VINFacturacion__c, objLeadInventarios);
        }    
        System.debug('EN actualizaDatosLead mapLeadUpsUps: '+ mapLeadUpsUps);
        System.debug('EN actualizaDatosLead mapLeadInvComs: '+ mapLeadInvComs);

        //Ve si tiene algo el mapa de  mapLeadUpsUps             
        //mapLeadInvComs.put(objLeadInventarios.id, objLeadInventarios);
        if (!mapLeadUpsUps.isEmpty()){
                                                
           List<Lead> lLeadUpd = mapLeadUpsUps.values();
           //Recorre la lista de lLeadUpd y actualiza el campo de TAM_NombreClienteFacturacion__c
           for (Lead CandUpd : lLeadUpd){
                //Busca el lead en el mapa de mapLeadInvComs
                if (mapLeadUpsUps.containsKey(CandUpd.id)){
                   String sVinLead = '';
                   String sVinLeadFact = '';
                   //Toma el nombre del objeto  objLeadInventarios
                   for (TAM_LeadInventarios__c objLeadInv : mapLeadInvComs.Values()){
                        if (objLeadInv.TAM_Prospecto__c == CandUpd.id){
                            sVinLead = objLeadInv.Name;
                            sVinLeadFact = objLeadInv.TAM_VINFacturacion__c != null ? objLeadInv.TAM_VINFacturacion__c : objLeadInv.Name;
                            break;
                        }//Fin si objLeadInv.TAM_Prospecto__c == CandUpd.id
                   }//Fin si 
                   TAM_LeadInventarios__c objLeadInvUpd;
                   //Busca el cliente asociado a sVinLead o sVinLeadFact
                   if (mapVinNomCte.containsKey(sVinLead)){ 
                        objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLead).id);
                        objLeadInvUpd.TAM_NombreClienteFacturacion__c = mapVinNomCte.get(sVinLead).TAM_NombreReceptor__c;
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   if (!mapVinNomCte.containsKey(sVinLeadFact)) {
                      if (mapVinNomCte.containsKey(sVinLeadFact)){ 
                          objLeadInvUpd = new TAM_LeadInventarios__c(id = mapLeadInvComs.get(sVinLeadFact).id);
                          objLeadInvUpd.TAM_NombreClienteFacturacion__c = mapVinNomCte.get(sVinLeadFact).TAM_NombreReceptor__c;
                      }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   }//Fin si mapVinNomCte.containsKey(sVinLeadFact)
                   System.debug('EN actualizaDatosLead objLeadInvUpd: ' + objLeadInvUpd);
                   //Ve si tiene algo el obj de objLeadInvUpd
                   if (objLeadInvUpd != null)
                      mapLeadInvUpd.put(objLeadInvUpd.id, objLeadInvUpd);  
                }//Fin si mapLeadInvComs.containsKey(CandUpd.id)
           }//Fin del for para lLeadUpd
                
           Integer icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lLeadUpd, Lead.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
               if (!objDtbUpsRes.isSuccess())
                   setErroresLeadErr.add('En Lead : ' + lLeadUpd.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbUpsRes              

           icntUpsCtes = 0;
           //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
           List<Database.Upsertresult> lDtbLiUpsRes = Database.upsert(mapLeadInvUpd.values(), TAM_LeadInventarios__c.id, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbLiUpsRes){
               if (!objDtbUpsRes.isSuccess())
                  setErroresLeadErr.add('En Lead : ' + mapLeadInvUpd.values().get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
               icntUpsCtes++;  
           }//Fin del for para lDtbLiUpsRes              

        }//Fin si !mapLeadUpsUps.isEmpty()
        System.debug('EN actualizaDatosLead setErroresLeadErr: ' + setErroresLeadErr);         
        System.debug('EN actualizaDatosLead setIdLeadSucc: ' + setIdLeadSucc);
    
    }*/
	    
    private Date getFecha(String sFechaPaso){
    	System.debug('EN getFecha sFechaPaso: ' + sFechaPaso);
    	
    	Date dFechaConvert = Date.today();
    	//Ve si la fecha tiene una diagonal
    	if (sFechaPaso.contains('/')){
    		String sDia = sFechaPaso.substring(0, sFechaPaso.indexOf('/'));
    		String sMes = sFechaPaso.substring(sFechaPaso.indexOf('/') + 1, sFechaPaso.lastIndexOf('/'));
    		String sAnio = sFechaPaso.substring(sFechaPaso.lastIndexOf('/') + 1, sFechaPaso.length());
    		System.debug('EN getFecha sDia: ' + sDia + ' sMes: ' + sMes + ' sAnio: ' + sAnio);
    		Date FechaPasoFinal = Date.newInstance(Integer.valueOf(sAnio), Integer.valueOf(sMes), Integer.valueOf(sDia));
    		dFechaConvert = FechaPasoFinal;
    	}//Fin si
    	System.debug('EN getFecha dFechaConvert: ' + dFechaConvert);
    	
    	//Regresa la fecha que corresponde
    	return dFechaConvert;
    }
    
    
}