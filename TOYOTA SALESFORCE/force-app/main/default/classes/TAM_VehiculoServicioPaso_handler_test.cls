/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba que contiene la logica para probar la clase: TAM_VehiculoServicioPaso_tgr_handler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    21-Agosto-2019    Héctor Figueroa             Creación
******************************************************************************* */

@isTest
private class TAM_VehiculoServicioPaso_handler_test {

	static	List<Venta__c> ventasH = new List<Venta__c>();
	static	List<Opportunity> opp = new List<Opportunity>();
	static	List<TAM_VehiculoPaso__c> vehiculos = new List<TAM_VehiculoPaso__c>();	
	static	List<TAM_ClienteServicioPaso__c> clienteServicio = new List<TAM_ClienteServicioPaso__c>();
	static	List<TAM_VehiculoServicioPaso__c> vehiculoServicio = new List<TAM_VehiculoServicioPaso__c>();

	static String VaRtAccRegPM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String VaRtAccRegPF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();    	
    static String VaRtContRegPF = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contactos-Clientes').getRecordTypeId();    	
    static String VaRtProdReg = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    static String VaRtEntregado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Entregado').getRecordTypeId();    	
	
	@TestSetup static void loadData(){
		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cuenta;
	}

	public static void LoadVentasH(){
		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};

		String sIdStdPriceBoook = Test.getStandardPricebookId();
		Account cuentaPaso = [Select id, Codigo_Distribuidor__c From Account Limit 1];

		String sTAMTipoCliente = 'Primero';
		for(Integer i=0; i < 2; i++){			
		 	String sufix = i < 2 ? '0'+String.valueOf(i):String.valueOf(i);  
		 	ventasH.add(new Venta__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),            		
        		IdModelo__c = modelos.get(aleatorio(modelos.size())),
        		YearModelo__c = '2017',
        		IdColorExt__c = colores.get(aleatorio(colores.size())),
        		DescrColorExt__c = 'External color description',
        		IdColorInt__c = colores.get(aleatorio(colores.size())),
        		DescrColorInt__c = 'Internal color description',
    			Serial__c = seriales.get(aleatorio(seriales.size())),
    			SaleCode__c = sufix,
    			SubmittedDate__c = Datetime.newInstance(2017, 10, 13),
        		IdExterno__c = 'JTDKBRFUXH30310'+sufix+'-'+String.valueOf(Date.today()),
        		TAM_TipoCliente__c = sTAMTipoCliente,
        		FirstName__c = 'PruebaFN',
        		LastName__c = 'PruebaLN'
	 			)
	 		);
	 		//Cambia el estatus
	 		if (sTAMTipoCliente == 'Primero')
				sTAMTipoCliente = 'Segundo';
	 		else if (sTAMTipoCliente == 'Segundo')
				sTAMTipoCliente = 'Primero';

			opp.add(
				new Opportunity( 
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_Vin__c = 'JTDKBRFUXH30310'+sufix,						
					CloseDate = Date.today(),
					StageName = 'Closed Won',
					Pricebook2Id = sIdStdPriceBoook,
					Amount = 100.00,
       				TAM_IdExterno__c = 'JTDKBRFUXH30310'+sufix + '-TestAccount',
       				recordTypeId = VaRtOppRegVCrm,
       				TAM_Distribuidor__c = cuentaPaso.ID,
       				AccountId = cuentaPaso.id
				)
			);

			//Crea los registros del tipo TAM_VehiculoPaso__c
			vehiculos.add(new TAM_VehiculoPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),
					TAM_IdModelo__c = modelos.get(aleatorio(modelos.size())),
					TAM_Serial__c = seriales.get(aleatorio(seriales.size())),
					TAM_YearModelo__c = '2018',
					TAM_TransType__c = 'RDR',
					TAM_Factura__c = '123456789',
					TAM_SaleCode__c = '1',
					TAM_IdColorExt__c = '03R3',
					TAM_DescrColorExt__c = 'BARCELONA RED MET',
					TAM_IdColorInt__c = 'EA20',
					TAM_DescrColorInt__c = 'EA20',
					TAM_SaleDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedTime__c = '19.05.30.077173',
					TAM_SalesManager__c = '169621364',
					TAM_lsAssesor__c = '',
					TAM_FLEET__c = 'N',
					TAM_Year__c = '2018',
					TAM_Month__c = '6'
				)
			);
			
			//Crea los registros del tipo TAM_ClienteServicioPaso__c
			clienteServicio.add(new TAM_ClienteServicioPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_RFC__c = 'HUVN6210097G1',
					TAM_FirstName__c = 'NORMA ANGELICA',
					TAM_LastName__c = 'HUITRON',
					TAM_LastName2__c = 'VAZQUEZ',
					TAM_Address1__c = 'GRANADA',
					TAM_Address2__c = '',
					TAM_Numero__c = '5989',
					TAM_Colonia__c = 'CUMBRES DE SANTA CLARA 2 SEC',
					TAM_DelMunc__c = 'MONTERREY',
					TAM_Zip__c = '64349',
					TAM_City__c = 'MONTERREY',
					TAM_State__c = 'NUEVO LEÓN',
					TAM_country__c = 'MEXICO',
					TAM_TelCel__c = '448116362676',
					TAM_Telhome__c = '8116362676',
					TAM_TelWork__c = '',
					TAM_Email__c = 'NORMAHUITRON9@HOTMAIL.COM',
					TAM_MetodoPreferidoContacto__c = 'EMAIL',
					TAM_UsoDatosPersonales__c = '',
					TAM_Year__c = '2018',
					TAM_Month__c = '9',
					TAM_Day__c = '10',
					TAM_TipoCliente__c = 'Primero'
				)
			);

			//Crea los registros del tipo TAM_VehiculoServicioPaso__c
			vehiculoServicio.add(new TAM_VehiculoServicioPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),
					TAM_RepairOrder__c = '16156',
					TAM_dofu__c = '06/19/2018',
					TAM_katashiki__c = 'ZVW50L-AHXEBA',
					TAM_Version__c = 'Prius Base',
					TAM_Model__c = 'Prius',
					TAM_YearModelo__c = '2018',
					TAM_Color__c = 'Plata',
					TAM_ServiceDate__c = '1/2/1900',
					TAM_ServiceType__c = 'Publico',
					TAM_PreviousOdometer__c = '5436',
					TAM_CurrentOdometer__c = '12980',
					TAM_ServiceRepDescription__c = 'SERVICIO DE 20 000KMS',
					TAM_MontoTotal__c = '1188',
					TAM_Year__c = '2019',
					TAM_Month__c = '6',
					TAM_Day__c = '18'
				) 
			);
							
		}//Fin el for para los registros que se va a crear		
				
		System.debug(JSON.serialize(ventasH));		
		System.debug(JSON.serialize(vehiculos));		
		System.debug(JSON.serialize(clienteServicio));		
		//return ventasH;
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void myUnitTest() {
		//Crea la Data para las pruebas
		TAM_VehiculoServicioPaso_handler_test.LoadVentasH();
		
		List<Venta__c> ventasPaso = ventasH;
		List<Opportunity> oppPaso = opp;
		List<TAM_VehiculoPaso__c> vehiculosPaso = vehiculos;
		List<TAM_ClienteServicioPaso__c> clienteServicioPaso = clienteServicio;
		List<TAM_VehiculoServicioPaso__c> vehiculoServicioPaso = vehiculoServicio;
		
		System.debug('EN test_one ventasPaso: ' + ventasPaso);
		System.debug('EN test_one vehiculosPaso: ' + vehiculosPaso);		
		System.debug('EN test_one clienteServicioPaso: ' + clienteServicioPaso);
		System.debug('EN test_one vehiculoServicioPaso: ' + vehiculoServicioPaso);
				
		//Crea los registros
		insert ventasPaso;
		insert oppPaso;
		insert vehiculosPaso;
		insert clienteServicioPaso;
		insert vehiculoServicioPaso;

		//Llama al metodo de ConvierteFecha
		TAM_VehiculoServicioPaso_tgr_handler objTAM_VehiculoPasoHandler = new TAM_VehiculoServicioPaso_tgr_handler();
		Time horaAct = Time.newInstance(4,26,18,0);
		objTAM_VehiculoPasoHandler.ConvierteFechaFechHora('12/10/2020 14:26:18.627000000', '14:26:18.627000000');
		objTAM_VehiculoPasoHandler.ConvierteFechaFechHora('12-Jan-2020 14:26:18.627000000', '14:26:18.627000000');
		objTAM_VehiculoPasoHandler.ConvierteFechaAAAAMMDD('12/10/2020 14:26:18.627000000');
		
    }
    
}