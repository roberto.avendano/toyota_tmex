global class BatchRepo1m implements Database.Batchable<sObject>,Database.Stateful{
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            'SELECT Id,Name,fwy_Segmento__c,fwy_Subsegmento__c FROM fwy_ReporteComunidades__c WHERE ((fwy_Segmento__c = \'Plan piso\' OR fwy_Segmento__c = \'Clientes\') AND (fwy_Subsegmento__c=\'Consolidado unidades fuera PP\' OR fwy_Subsegmento__c=\'Intercambios\' ))'                          
        ); 
    }
    global void execute(Database.BatchableContext bc,List<fwy_ReporteComunidades__c>ReporteComunidad1m){
        Database.delete(ReporteComunidad1m,false);
    }
    global void finish(Database.BatchableContext bc){
        
    }
}