@isTest
private class MovimientoTriggerHandlerTest {
	
	@isTest static void generalTest() {

        String VaRtLeadRetailNuevos = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Nuevos').getRecordTypeId();
    	List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
    	
    	Account a01 = new Account(
    		RecordTypeId = rtDealer.get(0).Id,
    		Name = 'Dealer',
    		Codigo_Distribuidor__c = '12345'
    	);
    	insert a01;

        Lead cand = new Lead(
            RecordTypeId = VaRtLeadRetailNuevos,
            FirstName = 'Test12',
            FWY_Intencion_de_compra__c = 'Este mes',    
            Email = 'aw@a.com',
            phone = '5554565432',
            Status='Pedido en Proceso',
            LastName = 'Test3',
            FWY_Tipo_de_persona__c = 'Person física',
            TAM_TipoCandidato__c = 'Retail Nuevos',
            TAM_DatosValidosFacturacion__c = true,
            FWY_codigo_distribuidor__c = '57039'      
        );
        insert cand;
        System.debug('EN TAM_VentasCRM_handler_tst loadData cand: '+ cand);

        TAM_InventarioVehiculosToyota__c objInvAFG = new TAM_InventarioVehiculosToyota__c(
              Name = 'JTDKBRFUXH3031000',
              Dealer_Code__c = '57039',
              Interior_Color_Description__c = 'Azul', 
              Exterior_Color_Description__c = 'Gris',
              Model_Number__c = '22060',
              Model_Year__c = '2020', 
              Toms_Series_Name__c = 'AVANZA',
              Exterior_Color_Code__c = '00B79', 
              Interior_Color_Code__c =  '010',
              Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvAFG;
        System.debug('EN TAM_VentasCRM_handler_tst loadData objInvAFG: '+ objInvAFG);

        TAM_LeadInventarios__c objLeadInvAFG = new TAM_LeadInventarios__c(
              Name = 'JTDKBRFUXH3031000',
              TAM_InventarioVehiculosFyG__c = objInvAFG.id,
              TAM_Prospecto__c = cand.id, 
              TAM_Idexterno__c = cand.id + ' JTDKBRFUXH3031000',
              TAM_VINFacturacion__c = 'JTDKBRFUXH3031000'
        );
        insert objLeadInvAFG;
        System.debug('EN TAM_VentasCRM_handler_tst loadData objLeadInvAFG: '+ objLeadInvAFG);
    	
        Vehiculo__c v01 = new Vehiculo__c(
        	Name='JTDKBRFUXH3031000',
        	Id_Externo__c='JTDKBRFUXH3031000'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
        	Id_Externo_Serie__c = 'X',
        	Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
        	Serie__c = ser01.Id,
        	ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        Objetivo_de_Venta__c ov01 = new Objetivo_de_Venta__c(
        	Distribuidor__c = a01.Id,
        	Serie__c = ser01.Id,
        	Anio__c = '2015',
        	Mes__c = '1',
        	MK__c = 1,
        	Objetivo__c = 1
        );
        insert ov01;
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA'                      
        );
        insert m01;
        update m01;

        RecordType movimientoManual = Constantes.TIPOS_REGISTRO_DEVNAME_ID.get('Movimiento__c').get('MovimientoManual');
        Movimiento__c m02 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            RecordTypeId = movimientoManual.Id
        );
        insert m02;

    }
	
}