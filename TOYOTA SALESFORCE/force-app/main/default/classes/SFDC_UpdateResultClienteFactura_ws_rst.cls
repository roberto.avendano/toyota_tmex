/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Publicaciín del WS con tecnologia REST para la actualizacion del proceso de facturación
    					para un prospecto en especifico

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    03-Junio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

@RestResource(urlMapping='/SFDC_UpdateResultClienteFactura_ws_rst/*')
global with sharing class SFDC_UpdateResultClienteFactura_ws_rst {

	@HttpPost  
	global static void UpdateResultClienteFacturaRst(){
		RestRequest req = RestContext.request;
		Blob bBody = req.requestBody;
		String sBody =  bBody.toString();
		
		//Manda llamar la clase que se llama SFDC_UpdDatosInvestigacion_ws_rst y el metodo updInvestigaciones
		String sResUpdInves = SFDC_UpdateResultClienteFactura_ctrl_rst.updateResultClienteFactura(sBody);				
		System.debug('EN SFDC_ConsultaClientesParaFacturar_ws_rst sResUpdInves: ' + sResUpdInves);

		RestContext.response.addHeader('Content-Type', 'application/json');
		RestContext.response.responseBody = Blob.valueOf(sResUpdInves);
	}
    
}