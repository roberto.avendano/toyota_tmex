public without sharing  class TAM_ColaboracionNivel1DOD {
    
    @AuraEnabled
    public static void shareRecordsDODNivel1 (Map<String,TAM_DistribuidoresFlotillaPrograma__c> mapEntregaVIN,
        List<TAM_DODSolicitudesPedidoEspecial__c> registrosDOD, Map<String, String> mapIdExtProdSerie){
            
        List<TAM_DODSolicitudesPedidoEspecial__Share > lColaboraDODFlotillaPrograma = new List<TAM_DODSolicitudesPedidoEspecial__Share >();
        Group nivel1 = new Group();
        
        nivel1 = [Select Name, Id From Group 
                  where Name LIKE '%Aprobador DTM (Nivel 1)%' limit 1];
        
        System.debug('registro dod: ' + registrosDOD);
        System.debug('EN TAM_ColaboracionNivel1DOD mapEntregaVIN: ' + mapEntregaVIN.KeySet());
        System.debug('EN TAM_ColaboracionNivel1DOD mapEntregaVIN: ' + mapEntregaVIN.Values());
        System.debug('EN TAM_ColaboracionNivel1DOD mapIdExtProdSerie: ' + mapIdExtProdSerie.KeySet());
        System.debug('EN TAM_ColaboracionNivel1DOD mapIdExtProdSerie: ' + mapIdExtProdSerie.Values());
        
        //Ve si tiene algo registrosDOD
        if(!registrosDOD.isEmpty()){
            System.debug('EN TAM_ColaboracionNivel1DOD VAL 0');
            for(TAM_DODSolicitudesPedidoEspecial__c regDOD : registrosDOD){
                System.debug('EN TAM_ColaboracionNivel1DOD regDOD: ' + regDOD);
                String sIdExtProd = regDOD.TAM_Codigo__c + '' + regDOD.TAM_Anio__c;
                //String sLlaveExt = regDOD.TAM_Entrega__c +'-'+ regDOD.TAM_Anio__c +'-'+ regDOD.TAM_Modelo__c;
                String sLlaveExt = regDOD.TAM_Entrega__c +'-'+ regDOD.TAM_Anio__c +'-'+ (mapIdExtProdSerie.containsKey(sIdExtProd) ? mapIdExtProdSerie.get(sIdExtProd) : regDOD.TAM_Modelo__c);
                System.debug('EN TAM_ColaboracionNivel1DOD sLlaveExt: ' + sLlaveExt);
                //Ve si existe sLlaveExt en el mapa de mapEntregaVIN
                if(mapEntregaVIN.containsKey(sLlaveExt)){
                    //TAM_DistribuidoresFlotillaPrograma__c dealerEntrega = mapEntregaVIN.get(regDOD.TAM_Entrega__c+'-'+regDOD.TAM_Anio__c+'-'+regDOD.TAM_Modelo__c);
                    TAM_DistribuidoresFlotillaPrograma__c dealerEntrega = mapEntregaVIN.get(sLlaveExt);
                    System.debug('EN TAM_ColaboracionNivel1DOD VAL 1 dealerEntrega: ' + dealerEntrega);
                    if(dealerEntrega.TAM_Estatus__c == 'Aceptar Recepción'){
                        TAM_DODSolicitudesPedidoEspecial__Share shareRecord = new TAM_DODSolicitudesPedidoEspecial__Share();
                        shareRecord.UserOrGroupId = nivel1.id;
                        shareRecord.ParentId = regDOD.id;
                        shareRecord.AccessLevel = 'Edit';
                        shareRecord.RowCause = 'Manual';
                        lColaboraDODFlotillaPrograma.add(shareRecord);
                    }//Fin si dealerEntrega.TAM_Estatus__c == 'Aceptar Recepción'
                }//Fin si mapEntregaVIN.containsKey(sLlaveExt)
            }//Fin del for para registrosDOD
        }//Fin si !registrosDOD.isEmpty()
        
        //Ve si si tiene algo lColaboraDODFlotillaPrograma
        if(!lColaboraDODFlotillaPrograma.isEmpty()){
            System.debug('registros a colaborar: ' + lColaboraDODFlotillaPrograma);
            Database.SaveResult[] jobShareInsertResult = Database.insert(lColaboraDODFlotillaPrograma,false);
            System.debug('resultados: ' + jobShareInsertResult);
        }//Fin si !lColaboraDODFlotillaPrograma.isEmpty()
        
    }
    
}