/**
    Descripción General: Controlador para el componente "TAM_Grid"
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        05/Abril/2020       Versión Inicial
		Cecilia Cruz        23/Julio/2020       actualizarModelos
        Cecilia Cruz        23/Agosto/2021      Modo solo lectura para usuarios de auditoria
    ________________________________________________________________
**/
public without sharing class TAM_Grid {
    
    
    /**Obtener información */
    @AuraEnabled
    public static Map<String, List<TAM_DetallePoliticaJunction__c>> getDetalleJunction(String recordId) {
        Map<String, List<TAM_DetallePoliticaJunction__c>>  mapDetalleJunction = new Map<String, List<TAM_DetallePoliticaJunction__c>>();
        List<TAM_DetallePoliticaJunction__c> lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
        List<TAM_DetallePoliticaJunction__c> lstDetalleJunction = [SELECT 	TAM_AnioModelo__c,
                                                                   			TAM_Serie__c,
                                                                   			TAM_Version__c,
                                                                   			TAM_Clave__c,
                                                                   			TAM_Inventario_Dealer__c,
                                                                   			TAM_Dias_de_Inventario__c,
                                                                   			TAM_Venta_MY__c,
                                                                   			TAM_BalanceOut__c,
                                                                   			TAM_Pago_TFS__c,
                                                                   			TAM_IncPropuesto_Cash__c,
                                                                   			TAM_IncPropuesto_Porcentaje__c,
                                                                   			TAM_IncentivoTMEX_Cash__c,
                                                                   			TAM_IncentivoTMEX_Porcentaje__c,
                                                                   			TAM_IncDealer_Cash__c,
                                                                   			TAM_IncDealer_Porcentaje__c,
                                                                   			TAM_Tripleplay__c,
                                                                   			TAM_ProductoFinanciero__c,
                                                                   			TAM_Descripcion__c,
                                                                   			TAM_TotalEstimado__c,
                                                                   			TAM_TP_IncTMEX_Cash__c,
                                                                   			TAM_TP_IncTMEX_Porcentaje__c,
                                                                   			TAM_TP_IncDealer_Porcentaje__c,
                                                                   			TAM_TP_IncDealer_Cash__c,
                                                                   			TAM_IncTFS_Cash__c,
                                                                   			TAM_IncTFS_Porcentaje__c,
                                                                   			TAM_MSRP__c,
                                                                   			TAM_Duplicar__c,
                                                                   			TAM_TMEX_Margen_Pesos__c,
                                                                   			TAM_TMEX_Margen_Porcentaje__c,
                                                                   			TAM_Consecutivo__c,
                                                                            TAM_Rango__c,
                                                                            TAM_FechaInventario__c,
                                                                            TAM_Lanzamiento__c,
                                                                            TAM_MotivoIncentivo__c,
                                                                            TAM_Incentivo_actual__c,
                                                                            TAM_AplicanAmbosIncentivos__c,
                                                                   			TAM_IdExterno__c
                                                                   FROM		TAM_DetallePoliticaJunction__c
                                                                   WHERE 	TAM_PoliticaIncentivos__c =:recordId
                                                                   ORDER BY TAM_Serie__c ASC, TAM_Clave__c ASC, TAM_Consecutivo__c ASC];
        
        //Mapa Detalle
        for(TAM_DetallePoliticaJunction__c objDetalleJunction : lstDetalleJunction){
            String strAnioandModelo = objDetalleJunction.TAM_Serie__c + objDetalleJunction.TAM_AnioModelo__c;
            lstDetalleAux = mapDetalleJunction.get(strAnioandModelo);

            if(mapDetalleJunction.containsKey(strAnioandModelo)){
               
                if(!lstDetalleAux.contains(objDetalleJunction)){
                    
                    lstDetalleAux.add(objDetalleJunction);
                    mapDetalleJunction.put(strAnioandModelo, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
                lstDetalleAux.add(objDetalleJunction);
                mapDetalleJunction.put(strAnioandModelo, lstDetalleAux);
            }
        }
        return mapDetalleJunction;
    }

    /** Obtener lista de rangos disponibles*/
    @AuraEnabled
    public static List<Rangos__c> getRangos(){
        List<Rangos__c> lstRangos = [SELECT Id, Name FROM Rangos__c WHERE Activo__c = true];
        return lstRangos;
    }

    /**Obtener tipo de registro de la politica de incentivos */
    @AuraEnabled
    public static String TipoRegistroPolitica(String recordId){
        List<TAM_PoliticasIncentivos__c> lstPoliticasIncentivos = [SELECT RecordTypeId FROM TAM_PoliticasIncentivos__c WHERE Id =: recordId];
        String strRecordTypeId = lstPoliticasIncentivos[0].RecordTypeId;
        String strRecordTypeName = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosById().get(strRecordTypeId).getname();
        return strRecordTypeName;
    }
    
    /*Obtener mapa de series disponibles*/
	@AuraEnabled
    public static Map<String, List<String>> getSeries(String recordId) {
        Map<String, List<String>>  mapSeries = new Map<String, List<String>>();
        List<String> lstDetalleAux = new List<String>();
        List<TAM_DetallePoliticaJunction__c> lstDetalleJunction = [SELECT 	TAM_AnioModelo__c,
                                                                   			TAM_Serie__c
                                                                   FROM		TAM_DetallePoliticaJunction__c
                                                                   WHERE 	TAM_PoliticaIncentivos__c =:recordId
                                                                   ORDER BY	TAM_Serie__c ASC];
        
        //Mapa Detalle
        for(TAM_DetallePoliticaJunction__c objDetalleJunction : lstDetalleJunction){
            lstDetalleAux = mapSeries.get(objDetalleJunction.TAM_AnioModelo__c);

            if(mapSeries.containsKey(objDetalleJunction.TAM_AnioModelo__c)){
               
                if(!lstDetalleAux.contains(objDetalleJunction.TAM_Serie__c)){
                    
                    lstDetalleAux.add(objDetalleJunction.TAM_Serie__c);
                    mapSeries.put(objDetalleJunction.TAM_AnioModelo__c, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<String>();
                lstDetalleAux.add(objDetalleJunction.TAM_Serie__c);
                mapSeries.put(objDetalleJunction.TAM_AnioModelo__c, lstDetalleAux);
            }
        }
        
        return mapSeries;
    }
    
    /*Guardado*/
    @AuraEnabled
    public static Boolean guardarPolitica(List<TAM_DetallePoliticaJunction__c> lstDetallePoliticaAUX, String recordId){
        
        List<TAM_DetallePoliticaJunction__c> lstAUX = new List<TAM_DetallePoliticaJunction__c>();
        for (TAM_DetallePoliticaJunction__c objDetalleJunction  : lstDetallePoliticaAUX){
            TAM_DetallePoliticaJunction__c objDetalleAux = new TAM_DetallePoliticaJunction__c();
            
            objDetalleAux.TAM_AnioModelo__c = objDetalleJunction.TAM_AnioModelo__c;
            objDetalleAux.TAM_Serie__c = objDetalleJunction.TAM_Serie__c;
            objDetalleAux.TAM_Version__c = objDetalleJunction.TAM_Version__c;
            objDetalleAux.TAM_Clave__c = objDetalleJunction.TAM_Clave__c;
            objDetalleAux.TAM_Inventario_Dealer__c = objDetalleJunction.TAM_Inventario_Dealer__c;
            objDetalleAux.TAM_Dias_de_Inventario__c = objDetalleJunction.TAM_Dias_de_Inventario__c;
            objDetalleAux.TAM_Venta_MY__c = objDetalleJunction.TAM_Venta_MY__c;
            objDetalleAux.TAM_BalanceOut__c = objDetalleJunction.TAM_BalanceOut__c;
            objDetalleAux.TAM_Pago_TFS__c = objDetalleJunction.TAM_Pago_TFS__c;
            objDetalleAux.TAM_IncPropuesto_Cash__c = objDetalleJunction.TAM_IncPropuesto_Cash__c;
            objDetalleAux.TAM_IncPropuesto_Porcentaje__c = objDetalleJunction.TAM_IncPropuesto_Porcentaje__c;
            objDetalleAux.TAM_IncentivoTMEX_Cash__c = objDetalleJunction.TAM_IncentivoTMEX_Cash__c;
            objDetalleAux.TAM_IncentivoTMEX_Porcentaje__c = objDetalleJunction.TAM_IncentivoTMEX_Porcentaje__c;
            objDetalleAux.TAM_IncDealer_Cash__c = objDetalleJunction.TAM_IncDealer_Cash__c;
            objDetalleAux.TAM_IncDealer_Porcentaje__c = objDetalleJunction.TAM_IncDealer_Porcentaje__c;
            objDetalleAux.TAM_Tripleplay__c = objDetalleJunction.TAM_Tripleplay__c;
            objDetalleAux.TAM_ProductoFinanciero__c = objDetalleJunction.TAM_ProductoFinanciero__c;
            objDetalleAux.TAM_Descripcion__c = objDetalleJunction.TAM_Descripcion__c;
            objDetalleAux.TAM_TotalEstimado__c = objDetalleJunction.TAM_TotalEstimado__c;
            objDetalleAux.TAM_TP_IncTMEX_Cash__c = objDetalleJunction.TAM_TP_IncTMEX_Cash__c;
            objDetalleAux.TAM_TP_IncTMEX_Porcentaje__c = objDetalleJunction.TAM_TP_IncTMEX_Porcentaje__c;
            objDetalleAux.TAM_TP_IncDealer_Porcentaje__c = objDetalleJunction.TAM_TP_IncDealer_Porcentaje__c;
            objDetalleAux.TAM_TP_IncDealer_Cash__c = objDetalleJunction.TAM_TP_IncDealer_Cash__c;
            objDetalleAux.TAM_IncTFS_Cash__c = objDetalleJunction.TAM_IncTFS_Cash__c;
            objDetalleAux.TAM_IncTFS_Porcentaje__c = objDetalleJunction.TAM_IncTFS_Porcentaje__c;
            objDetalleAux.TAM_MSRP__c = objDetalleJunction.TAM_MSRP__c;
            objDetalleAux.TAM_Duplicar__c = objDetalleJunction.TAM_Duplicar__c;
            objDetalleAux.TAM_TMEX_Margen_Pesos__c = objDetalleJunction.TAM_TMEX_Margen_Pesos__c;
            objDetalleAux.TAM_Consecutivo__c = objDetalleJunction.TAM_Consecutivo__c;
            objDetalleAux.TAM_IdExterno__c = recordId + objDetalleJunction.TAM_AnioModelo__c + objDetalleJunction.TAM_Serie__c + objDetalleJunction.TAM_Clave__c + objDetalleJunction.TAM_Consecutivo__c;
            objDetalleAux.Name = recordId + objDetalleJunction.TAM_AnioModelo__c + objDetalleJunction.TAM_Serie__c + objDetalleJunction.TAM_Clave__c + objDetalleJunction.TAM_Consecutivo__c;
            objDetalleAux.TAM_PoliticaIncentivos__c = recordId;
            objDetalleAux.TAM_Rango__c = objDetalleJunction.TAM_Rango__c;
            objDetalleAux.TAM_FechaInventario__c = objDetalleJunction.TAM_FechaInventario__c;
            objDetalleAux.TAM_Lanzamiento__c = objDetalleJunction.TAM_Lanzamiento__c;
            objDetalleAux.TAM_MotivoIncentivo__c = objDetalleJunction.TAM_MotivoIncentivo__c;
            objDetalleAux.TAM_AplicanAmbosIncentivos__c = objDetalleJunction.TAM_AplicanAmbosIncentivos__c;
            
            lstAUX.add(objDetalleAux);
        }
        try {
            Database.upsert(lstAUX, TAM_DetallePoliticaJunction__c.TAM_IdExterno__c, false);
            return true; 
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return false; 
        }
    }
    
    /*Borrar*/
    @AuraEnabled
    public static Boolean borrarRegistro(TAM_DetallePoliticaJunction__c objRegistroBorrar){
        try {
            delete objRegistroBorrar;
            return true;
        } catch (DmlException e) {
           System.debug(e.getMessage());
            return false; 
		}	
    }
    
    /*Actualizar Modelos*/
    @AuraEnabled
    public static void actualizarModelos(String recordId){
        
        TAM_UpdateGridClass.setDetalleJunction(recordId);
    }

    /*Validar usuario de Auditoria*/
    @AuraEnabled 
    public static Boolean getCurrentUser (String userId){
        Boolean boolUsuarioLectura = false;
        List<Profile> profileId = [SELECT Id FROM Profile WHERE Name='Auditor Finanzas Platform' LIMIT 1];
        User objUser = [SELECT id, ProfileId  FROM User WHERE Id =: userId];
        if(objUser.ProfileId == profileId[0].Id){
            boolUsuarioLectura = true;
        }
        return boolUsuarioLectura;
    }
}