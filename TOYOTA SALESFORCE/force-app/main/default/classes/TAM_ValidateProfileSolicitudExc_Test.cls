@isTest
public class TAM_ValidateProfileSolicitudExc_Test {
    
    @testSetup static void setup() {
        
        //Se crea una cuenta parent que se utilizara para agrupar a los contactos
        Account a = new Account(Name='Test Account Name');
        insert a;
        
        
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a.id;
        insert contactRH1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Prime Platform'];
        
        //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id,PortalRole  = 'Manager',
                          TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
        system.debug('usuario'+u);
        insert u;
        
        
        
        
    }  
    
    @isTest static void testMethod6(){
        User userTest = [Select id from User WHERE UserName='test99@testorg.com'];
        TAM_ValidateProfileSolicitudExc.getProfileByUser(userTest.id);
        
        
    }
}