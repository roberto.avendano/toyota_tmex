global class DeleteNullOrders_Batch implements Database.Batchable<sObject> {
	
	private String query;
	private final Map<String,Map<String,RecordType>> recordTypeMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
	private List<Order> ordersDelete;
	private Map<String, List<Order>> accountOrders;	

	global DeleteNullOrders_Batch(String q) {
		this.query = q;
		this.ordersDelete = new List<Order>();
		this.accountOrders = new Map<String, List<Order>>();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		if(scope.size() > 0){
			for(Account a: (List<Account>) scope){
				Boolean existOrderPartesRobadas = false;

				if(!accountOrders.containsKey(a.Id)){
					accountOrders.put(a.Id, new List<Order>());
				}
				
				for(Order order: a.Orders){
					if(order.RecordTypeId == recordTypeMap.get('Order').get('Solicitud_partes_robadas').Id){
						existOrderPartesRobadas = true;
					}

					else if(order.RecordTypeId == recordTypeMap.get('Order').get('SolicitudNula').Id){
						accountOrders.get(a.Id).add(order);
					}
				}

				if(existOrderPartesRobadas){
					if(accountOrders.get(a.Id).size() > 0){
						ordersDelete.addAll(accountOrders.get(a.Id));
					}
				}				

			}
		}

		if(ordersDelete.size() > 0){
			try{
				delete ordersDelete;
				System.debug('ordenes eliminadas satisfactoriamente');

			} catch(Exception ex){
				System.debug(ex.getMessage());
			}
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('Batch process finished.');
	}
	
}