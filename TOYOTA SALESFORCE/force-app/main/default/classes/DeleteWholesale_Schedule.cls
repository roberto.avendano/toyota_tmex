global class DeleteWholesale_Schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        String query = 'SELECT Id, Name, CreatedDate FROM InventarioWholesale__c WHERE CreatedDate < TODAY';
        List<InventarioWholesale__c> inventariosHoy = this.getInventariosDeHoy();
        
        if(inventariosHoy.size() > 0){
            DeleteWholesale_Batch b = new DeleteWholesale_Batch(query);
            database.executebatch(b);
        }
    }


    public List<InventarioWholesale__c> getInventariosDeHoy(){
        return [SELECT Id, Name, CreatedDate FROM InventarioWholesale__c WHERE CreatedDate = TODAY LIMIT 5];
    }

}