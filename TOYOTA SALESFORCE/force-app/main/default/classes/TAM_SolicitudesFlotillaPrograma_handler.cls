/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM 
    Descripción:        Clase que contiene la logica para procesar los registros de las nuevas solicitudes 
    					y actualiza los datos del nombre completo del cliente y el RANGO FINAL.

    Infomación de cambios (versiones)VaRtAccRegPF
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    05-Mayo-2020    		Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_SolicitudesFlotillaPrograma_handler extends TriggerHandler{

	public String VaRtRangosFlot = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	public String VaRtRangosProg = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
	
	public String VaRtRangosVentaCorp = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	public String VaRtRangosPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();

	private List<TAM_SolicitudesFlotillaPrograma__c> solCompFlotList;
	private Map<Id,TAM_SolicitudesFlotillaPrograma__c> mapSolCompFlot;
    private Map<Id,TAM_SolicitudesFlotillaPrograma__c> mapSolCompFlotOld;
    
    public TAM_SolicitudesFlotillaPrograma_handler() {
		this.solCompFlotList = Trigger.new;
		this.mapSolCompFlot = (Map<Id, TAM_SolicitudesFlotillaPrograma__c>) Trigger.newMap;
        this.mapSolCompFlotOld = (Map<Id, TAM_SolicitudesFlotillaPrograma__c>) Trigger.oldMap;
	}

	public override void beforeInsert(){
		//Llama la funcion que valida el tipo de venta
		actualizaProgramaRango(solCompFlotList);
	}

	public override void beforeUpdate(){
		//Llama la funcion que valida el tipo de venta
		actualizaOtrosCampos(solCompFlotList);
	}

	public override void afterInsert(){
		//Llama la funcion que valida el tipo de venta
		validaTipoVenta(solCompFlotList);
	}

    public override void afterUpdate(){
        //Llama la funcion que valida el tipo de venta
        crearMovSolAutorizada(solCompFlotList, mapSolCompFlotOld);
    }
	
	//El metodo que valida los datos del rango de la flotilla 
	private void actualizaProgramaRango(List<TAM_SolicitudesFlotillaPrograma__c> lNewSol){
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta...');

        String sCambiaDatosSol = System.Label.TAM_CambiaDatosSol;
		Set<String> setClientes = new Set<String>();		
		Set<String> setRangos = new Set<String>();
		Set<String> setOwners = new Set<String>();
		Map<String, String>  NomRangoIdReg = new Map<String, String>();
		Map<String, String>  mapIdCteNomComp = new Map<String, String>();
		Map<String, String>  mapIdCteNomDistrib = new Map<String, String>();
		Map<String, String>  mapIdNoDistNomDist = new Map<String, String>();

		//Recorre la lista de TAM_SolicitudesFlotillaPrograma__c y toma los datos del cliente
		for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSol){
			if (objFlotProg.TAM_Cliente__c != null){
				setClientes.add(objFlotProg.TAM_Cliente__c);
				setOwners.add(objFlotProg.OwnerId);	
			}//Fin si (objFlotProg.TAM_Cliente__c != null
		}		
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.setOwners : ' + setOwners + ' setClientes: ' + setClientes);
		
		//Busca el no de distribuidor del usuario propietrio de la solicitud
		for (User usuario : [Select id, u.CodigoDistribuidor__c From User u Where id =: setOwners]){
			String sCodDist = usuario.CodigoDistribuidor__c != null ? usuario.CodigoDistribuidor__c : '57000';
			mapIdCteNomDistrib.put(usuario.id, sCodDist);
		}
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.mapIdCteNomDistrib : ' + mapIdCteNomDistrib);

		//Consulta el campo de TAM_Rango__c de los reg de flotilla y ve si tiene un valor diferente a un rango
		for (Account objCliente : [Select id, Name, Codigo_Distribuidor__c 
			From Account r Where Codigo_Distribuidor__c =: mapIdCteNomDistrib.Values()]){
			mapIdNoDistNomDist.put(objCliente.Codigo_Distribuidor__c, objCliente.Name);
		}
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta mapIdNoDistNomDist: ' + mapIdNoDistNomDist.KeySet());
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta mapIdNoDistNomDist: ' + mapIdNoDistNomDist.Values());		
		
		//Consulta el campo de TAM_Rango__c de los reg de flotilla y ve si tiene un valor diferente a un rango
		for (Account objCliente : [Select id, TAM_ProgramaRango__c, TAM_ProgramaRango__r.Name, TAM_IdExternoNombre__c  
			From Account r Where id =: setClientes]){
			if (objCliente.TAM_ProgramaRango__c != null)
				NomRangoIdReg.put(objCliente.id, objCliente.TAM_ProgramaRango__c);
			if (objCliente.TAM_IdExternoNombre__c != null)			
				mapIdCteNomComp.put(objCliente.id, objCliente.TAM_IdExternoNombre__c);
		}
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdReg: ' + NomRangoIdReg.KeySet());
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdReg: ' + NomRangoIdReg.Values());
		
		//Recorre la lista de TAM_SolicitudesFlotillaPrograma__c y actualiza TAM_ProgramaRango__c
		for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSol){
			if (objFlotProg.TAM_Cliente__c != null){
				if (NomRangoIdReg.containsKey(objFlotProg.TAM_Cliente__c))
					   objFlotProg.TAM_ProgramaRango__c = NomRangoIdReg.get(objFlotProg.TAM_Cliente__c);
				if (mapIdCteNomComp.containsKey(objFlotProg.TAM_Cliente__c))
					   objFlotProg.TAM_NombreClienteCompleto__c = mapIdCteNomComp.get(objFlotProg.TAM_Cliente__c);
				if (mapIdCteNomDistrib.containsKey(objFlotProg.OwnerId))
                    if ( (UserInfo.getUserId() != '0051Y000009UruJQAS' && !Boolean.valueOf(sCambiaDatosSol)) || Test.isRunningTest() )
					   objFlotProg.TAM_NombreCompletoDistribuidor__c = mapIdCteNomDistrib.get(objFlotProg.OwnerId);				
				String sNoDist = mapIdCteNomDistrib.get(objFlotProg.OwnerId);
				if (mapIdNoDistNomDist.containsKey(sNoDist))
				    if ( (UserInfo.getUserId() != '0051Y000009UruJQAS' && !Boolean.valueOf(sCambiaDatosSol)) || Test.isRunningTest())
					   objFlotProg.TAM_NomDistribuidorPropietario__c = mapIdNoDistNomDist.get(sNoDist);
			}//Fin si objFlotProg.TAM_Cliente__c != null
			System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta objFlotProg: ' + objFlotProg);		
		}
	}

    //El metodo que valida los datos del rango de la flotilla 
    private void actualizaOtrosCampos(List<TAM_SolicitudesFlotillaPrograma__c> lNewSol){
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta...');

        String sCambiaDatosSol = System.Label.TAM_CambiaDatosSol;
        Set<String> setClientes = new Set<String>();        
        Set<String> setRangos = new Set<String>();
        Set<String> setOwners = new Set<String>();
        Map<String, String>  NomRangoIdReg = new Map<String, String>();
        Map<String, String>  mapIdCteNomComp = new Map<String, String>();
        Map<String, String>  mapIdCteNomDistrib = new Map<String, String>();
        Map<String, String>  mapIdNoDistNomDist = new Map<String, String>();

        //Recorre la lista de TAM_SolicitudesFlotillaPrograma__c y toma los datos del cliente
        for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSol){
            if (objFlotProg.TAM_Cliente__c != null){
                setClientes.add(objFlotProg.TAM_Cliente__c);
                setOwners.add(objFlotProg.OwnerId); 
            }//Fin si (objFlotProg.TAM_Cliente__c != null
        }       
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.setOwners : ' + setOwners + ' setClientes: ' + setClientes);
        
        //Busca el no de distribuidor del usuario propietrio de la solicitud
        for (User usuario : [Select id, u.CodigoDistribuidor__c From User u Where id =: setOwners]){
            String sCodDist = usuario.CodigoDistribuidor__c != null ? usuario.CodigoDistribuidor__c : '57000';
            mapIdCteNomDistrib.put(usuario.id, sCodDist);
        }
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.mapIdCteNomDistrib : ' + mapIdCteNomDistrib);

        //Consulta el campo de TAM_Rango__c de los reg de flotilla y ve si tiene un valor diferente a un rango
        for (Account objCliente : [Select id, Name, Codigo_Distribuidor__c 
            From Account r Where Codigo_Distribuidor__c =: mapIdCteNomDistrib.Values()]){
            mapIdNoDistNomDist.put(objCliente.Codigo_Distribuidor__c, objCliente.Name);
        }
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta mapIdNoDistNomDist: ' + mapIdNoDistNomDist.KeySet());
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta mapIdNoDistNomDist: ' + mapIdNoDistNomDist.Values());      
        
        //Consulta el campo de TAM_Rango__c de los reg de flotilla y ve si tiene un valor diferente a un rango
        for (Account objCliente : [Select id, TAM_ProgramaRango__c, TAM_ProgramaRango__r.Name, TAM_IdExternoNombre__c  
            From Account r Where id =: setClientes]){
            if (objCliente.TAM_ProgramaRango__c != null)
                NomRangoIdReg.put(objCliente.id, objCliente.TAM_ProgramaRango__c);
            if (objCliente.TAM_IdExternoNombre__c != null)          
                mapIdCteNomComp.put(objCliente.id, objCliente.TAM_IdExternoNombre__c);
        }
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdReg: ' + NomRangoIdReg.KeySet());
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdReg: ' + NomRangoIdReg.Values());
        
        //Recorre la lista de TAM_SolicitudesFlotillaPrograma__c y actualiza TAM_ProgramaRango__c
        for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSol){
            if (objFlotProg.TAM_Cliente__c != null){
                if (mapIdCteNomComp.containsKey(objFlotProg.TAM_Cliente__c))
                       objFlotProg.TAM_NombreClienteCompleto__c = mapIdCteNomComp.get(objFlotProg.TAM_Cliente__c);
                if (mapIdCteNomDistrib.containsKey(objFlotProg.OwnerId))
                    if ( (UserInfo.getUserId() != '0051Y000009UruJQAS' && !Boolean.valueOf(sCambiaDatosSol)) || Test.isRunningTest() )
                       objFlotProg.TAM_NombreCompletoDistribuidor__c = mapIdCteNomDistrib.get(objFlotProg.OwnerId);             
                String sNoDist = mapIdCteNomDistrib.get(objFlotProg.OwnerId);
                if (mapIdNoDistNomDist.containsKey(sNoDist))
                    if ( (UserInfo.getUserId() != '0051Y000009UruJQAS' && !Boolean.valueOf(sCambiaDatosSol)) || Test.isRunningTest())
                       objFlotProg.TAM_NomDistribuidorPropietario__c = mapIdNoDistNomDist.get(sNoDist);
            }//Fin si objFlotProg.TAM_Cliente__c != null
            System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta objFlotProg: ' + objFlotProg);     
        }
    }
		
	//El metodo que valida los datos del rango de la flotilla 
	private void validaTipoVenta(List<TAM_SolicitudesFlotillaPrograma__c> lNewSol){
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta...');
		
		Set<String> setRangos = new Set<String>();
		Map<String, String>  NomRangoIdTipoReg = new Map<String, String>();

		//Consulta el campo de TAM_Rango__c de los reg de flotilla y ve si tiene un valor diferente a un rango
		for (Rangos__c objRangos : [Select r.RecordTypeId, r.Name From Rangos__c r]){
			NomRangoIdTipoReg.put(objRangos.Name, objRangos.RecordTypeId);			
		}
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdTipoReg: ' + NomRangoIdTipoReg.KeySet());
		System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta NomRangoIdTipoReg: ' + NomRangoIdTipoReg.Values());
		
		//Recorre la lista de TAM_SolicitudesFlotillaPrograma__c
		for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSol){
			if (objFlotProg.TAM_RangoPrograma__c != null){
				System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.validaTipoVenta TAM_RangoPrograma__c: ' + objFlotProg.TAM_RangoPrograma__c);
				if (objFlotProg.recordTypeId == VaRtRangosVentaCorp)
					if (NomRangoIdTipoReg.get(objFlotProg.TAM_RangoPrograma__c) == VaRtRangosProg)
						objFlotProg.TAM_Cliente__c.addError('No puedes crear solicitudes para Flotillas solo puedes crear para Programas.');
				if (objFlotProg.recordTypeId == VaRtRangosPrograma)
					if (NomRangoIdTipoReg.get(objFlotProg.TAM_RangoPrograma__c) == VaRtRangosFlot)
						objFlotProg.TAM_Cliente__c.addError('No puedes crear solicitudes para Programas solo puedes crear para Flotillas.');
			}//fin si objFlotProg.TAM_RangoPrograma__c != null
		}

	}
	

    //El metodo que valida los datos del rango de la flotilla 
    private void crearMovSolAutorizada(List<TAM_SolicitudesFlotillaPrograma__c> lNewSolPrm, 
        Map<Id, TAM_SolicitudesFlotillaPrograma__c> oldMapSolFloProgPrm){
        System.debug('EN TAM_SolicitudesFlotillaPrograma_handler.crearMovSolAutorizada...');
        
        Set<String> setRangos = new Set<String>();
        Map<String, String>  NomRangoIdTipoReg = new Map<String, String>();

        //Select t.TAM_FechaCierreSolicitudForm__c, t.TAM_Estatus__c, t.RecordTypeId From TAM_SolicitudesFlotillaPrograma__c t
        
        //Recorre la lista de TAM_SolicitudesFlotillaPrograma__c
        for (TAM_SolicitudesFlotillaPrograma__c objFlotProg : lNewSolPrm){
            //Ve si la solicitud corresponde con una de Invcentario
            if (objFlotProg.RecordTypeId == VaRtRangosPrograma && objFlotProg.TAM_Estatus__c == 'Cerrada' && oldMapSolFloProgPrm.get(objFlotProg.id).TAM_Estatus__c != 'Cerrada' ){
                
            }//Fin si objFlotProg.RecordTypeId == VaRtRangosPrograma && objFlotProg.TAM_Estatus__c == 'Cerrada' && oldMapSolFloProgPrm.get(objFlotProg.id).TAM_Estatus__c != 'Cerrada'
            
        }//Fin del for para lNewSolPrm


    }

	
    
}