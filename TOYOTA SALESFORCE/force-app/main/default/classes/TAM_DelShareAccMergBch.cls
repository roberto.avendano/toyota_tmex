/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_DelShareAccMergSch y
    					eliminar los registros del objeto AccountShare para las cuentas que ya 
    					estan con la badera de merge
    					y para que nadielas vea

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    08-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_DelShareAccMergBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_DelShareAccMergBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_DelShareAccMergBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Account> scope){
        System.debug('EN TAM_DelShareAccMergBch.');
		List<AccountShare> lAccShareDel = new List<AccountShare>();
		Set<String> setIdAcc = new Set<String>();
		List<Account> lAccMergeUpd = new List<Account>();
				
        //Recorre la lista de Casos para cerrarlos 
        for (Account objAccountId : scope){
        	setIdAcc.add(objAccountId.id);
        	lAccMergeUpd.add(new Account(id = objAccountId.id, TAM_IdExternoNombre__c = null));
        }
        System.debug('EN TAM_DelShareAccMergBch setIdAcc: ' + setIdAcc);
        System.debug('EN TAM_DelShareAccMergBch lAccMergeUpd: ' + lAccMergeUpd);

		//Consulta los registros asociados a setIdAcc en el objeto de AccountShare
		for (AccountShare objAccountShare : [Select a.Id, a.AccountId From AccountShare a Where AccountId IN :setIdAcc
			And RowCause IN ('Manual')]){
			lAccShareDel.ADD(new AccountShare(id = objAccountShare.id));
		}
        System.debug('EN TAM_DelShareAccMergBch lAccShareDel: ' + lAccShareDel);

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lAccShareDel.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Deleteresult> lDtbUpsRes = Database.delete(lAccShareDel);
			//Ve si hubo error
			for (Database.Deleteresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_DelShareAccMergBch Hubo un error a la hora de eliminarl los registros en AccountShare ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lAccMergeUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Saveresult> lDtbUpsRes = Database.update(lAccMergeUpd);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_DelShareAccMergBch Hubo un error a la hora de actuelizar los registros en Account ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_DelShareAccMergBch.finish Hora: ' + DateTime.now());     
        Integer intDelShareAccMergeMinutos = Integer.valueOf(System.Label.TAM_DelShareAccMerge);
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraMediaHoraDesp = dtHoraActual.addMinutes(intDelShareAccMergeMinutos);
		String CRON_EXP = dtHoraMediaHoraDesp.second() + ' ' + dtHoraMediaHoraDesp.minute() + ' ' + dtHoraMediaHoraDesp.hour() + ' ' + dtHoraMediaHoraDesp.day() + ' ' + dtHoraMediaHoraDesp.month() + ' ? ' + dtHoraMediaHoraDesp.year();
    	System.debug('EN TAM_DelShareAccMergBch.finish CRON_EXP: ' + CRON_EXP);

		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
		TAM_DelShareAccMergSch objDelShareAccMergSch = new TAM_DelShareAccMergSch();
    	System.debug('EN TAM_DelShareAccMergBch.finish objDelShareAccMergSch: ' + objDelShareAccMergSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest())
			System.schedule('TAM_DelShareAccMergSch: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objDelShareAccMergSch);         
    } 
    
}