/*******************************************************************************
Desarrollado por: Globant México
Autor: Roberto Carlos Avendaño Quintana
Proyecto: Assestment Center TMEX
*********************************************************************************/

@isTest
public class ContactoRHTriggerTest {
    @isTest static void testMethod1(){
        
        //Generar casos de negocio de la evaluaciónes
        RH_CasoNegocio__c casoEval1 = new RH_CasoNegocio__c();
        casoEval1.name = 'CHEMICAL INDUSTRIES';
        insert casoEval1;
        
        //Generar los email relacionados al caso de uso
        RH_EmailCasoUso__c emailCasoUso = new RH_EmailCasoUso__c();
        emailCasoUso.name = 'Asunto de prueba AVX';
        emailCasoUso.RH_CasoNegocio__c = casoEval1.id;
        emailCasoUso.RH_CC__c = 'Director de RH';
        emailCasoUso.RH_CCO__c = '';
        emailCasoUso.Correo_Adjunto__c = '';
        emailCasoUso.RH_Descripcion__c = 'Atención este es un correo de prueba';
        emailCasoUso.RH_De__c = 'TMEX';
        emailCasoUso.RH_Fecha__c = date.today();
        emailCasoUso.RH_Para__c = 'TMEX TOYOTA';
        emailCasoUso.Prioridad__c = 'Urgente';
        insert emailCasoUso;
        
        //Se genera la evaluación RH
        RH_Evaluacion__c evaluacionRH = new RH_Evaluacion__c();
        evaluacionRH.RH_CasoNegocio__c = casoEval1.id;
        evaluacionRH.RH_DuracionExamen__c = '100';
        evaluacionRH.Name = 'Gerente de TMEX';
        insert evaluacionRH;
        
        //Se crea una cuenta parent que se utilizara para agrupar a los contactos
        Account a = new Account(Name='Test Account Name');
        insert a;
        
        //Generemos el contacto: Contact para asociarlo al candidato a la evaluación
        Id recordTypeIdContactoRH = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contactos RH').getRecordTypeId();
        
        
        Contact contactRH1 = new Contact();
        contactRH1.RecordTypeId  = recordTypeIdContactoRH;
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.RH_LicenciaDisponible__c = true;
        contactRH1.AccountId = a.id;
        insert contactRH1;
        
        //Generemos el contacto: Contact para asociarlo al candidato a la evaluación
        Contact contactRH2 = new Contact();
        contactRH2.RecordTypeId  = recordTypeIdContactoRH;
        contactRH2.NombreUsuario__c = 'Contacto RH2';
        contactRH2.lastName = 'Contacto RH2';
        contactRH2.Email = 'test@test2.com';
        contactRH2.RH_LicenciaDisponible__c = true;
        contactRH2.AccountId = a.id;
        insert contactRH2;
        
        
        //Obtenemos el usuario de ayuda toyota , para ejecutar las pruebas como AMDIN
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        system.runAs(thisUser){
            
            //Obtenemos el rol de la instancia 
                UserRole uR = [SELECT Id FROM UserRole WHERE Name='Contactos RH Socio Ejecutivo'];
            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community Contacto RH']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,ContactId = contactRH1.id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u2 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p.Id,ContactId = contactRH2.id,PortalRole  = 'Manager',
                               TimeZoneSidKey='America/Los_Angeles', UserName='test9910@testorg.com');
            system.debug('usuario'+u);
            insert u2;
            
            
            
            //Se genera el contacto_RH relacionado a la evaluación creada, que sera el registro que denotara el trigger : ContactoRHTriggerHandler
            RH_Contacto__c candidatoRH2 = new RH_Contacto__c();
            candidatoRH2.RH_ApellidoMaterno__c = 'Lopez';
            candidatoRH2.RH_ApellidoPaterno__c  ='Sola';
            candidatoRH2.RH_Contacto__c = contactRH2.id;
            candidatoRH2.RH_Email__c = 'test@test.com';
            candidatoRH2.Estatus__c  ='Activo';
            candidatoRH2.RH_Evaluacion__c = evaluacionRH.id;
            candidatoRH2.FechaEvaluacionRH__c = date.today();
            candidatoRH2.Name = 'Jose Luis';
            insert candidatoRH2;
            
            //Se genera el contacto_RH relacionado a la evaluación creada, que sera el registro que denotara el trigger : ContactoRHTriggerHandler
            RH_Contacto__c candidatoRH = new RH_Contacto__c();
            candidatoRH.RH_ApellidoMaterno__c = 'Lopez';
            candidatoRH.RH_ApellidoPaterno__c  ='Sola';
            candidatoRH.RH_Contacto__c = contactRH1.id;
            candidatoRH.RH_Email__c = 'test@test.com';
            candidatoRH.Estatus__c  ='Pendiente';
            candidatoRH.RH_Evaluacion__c = evaluacionRH.id;
            candidatoRH.FechaEvaluacionRH__c = date.today();
            candidatoRH.Name = 'Jose Luis';
            candidatoRH.RH_evaluacionFinalizada__c = false;
            candidatoRH.RH_evaluacionGenerada__c = false;
            insert candidatoRH;
            
            //Se actualiza el registro con estatus activo ,para detonar el trigger en el evento after update
            RH_Contacto__c evalUpd = [Select id,RH_ApellidoMaterno__c,RH_evaluacionFinalizada__c,RH_evaluacionGenerada__c,RH_ApellidoPaterno__c,RH_Contacto__c,RH_Email__c,Estatus__c from RH_Contacto__c WHERE id =: candidatoRH.id];
            evalUpd.RH_Email__c = 'tmex@test.com';
            evalUpd.Estatus__c  ='Activo';
            evalUpd.RH_evaluacionFinalizada__c = false;
            evalUpd.RH_evaluacionGenerada__c = false;
            update evalUpd;
            
        }
    }
}