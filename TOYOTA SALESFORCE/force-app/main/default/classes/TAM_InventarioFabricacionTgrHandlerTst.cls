/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_InventarioFabricacionTriggerHandler.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    15-Sep-2020    		Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_InventarioFabricacionTgrHandlerTst {

 	@TestSetup static void loadData(){

		TAM_Inventario_A__c InventarioTransito = new TAM_Inventario_A__c(
			Name='MR2B29F36K1168300', 
			TAM_Dealer_Code__c='57002', 
			//Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			TAM_Dummy_VIN__c='36K1168300', 
			TAM_Exterior_Color_Code__c='01G3', 
			TAM_Exterior_Color_Description__c='GRAY ME.', 
			TAM_Interior_Color_Code__c='FC20', 
			TAM_Interior_Color_Description__c='S/D BLACK', 
			TAM_Model_Number__c='2005', 
			TAM_Model_Year__c='2019', 
			TAM_Toms_Series_Name__c='YARIS SD'
	    );
	    insert InventarioTransito;  

		CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
        	Name = 'AVANZA-22060-2020-B79-10-LE AT',
        	Marca__c = 'Toyota',
        	Serie__c = 'AVANZA',
        	Modelo__c = '22060',
        	AnioModelo__c = '2020',
        	CodigoColorExterior__c = 'B79', 
        	CodigoColorInterior__c = '10',
        	DescripcionColorExterior__c = 'Azul', 
        	DescripcionColorInterior__c = 'Gris',
        	Version__c = 'LE AT',
        	Disponible__c = 'SI'
	    );
	    insert catCenMod;  

        TAM_DODSolicitudesPedidoEspecial__c objTAMDODSolicitudesPedidoEspecial = new TAM_DODSolicitudesPedidoEspecial__c(       
            TAM_DummyVin__c = 'MR2B29F36K1168300',
            TAM_Codigo__c  = '22060'
        );
        insert objTAMDODSolicitudesPedidoEspecial;  

        TAM_AdminInventarioDealer__c objAdminInvDealer2 = new TAM_AdminInventarioDealer__c(
            Name = '57002 DistRelMod',
            TAM_CodigoDistribuidor__c = '57002',          
            TAM_DistribuidorAsociado__c = '57010, TOYOTA PRUEBA;',
            TAM_IdExterno__c = '57002 DistRelMod',
            TAM_Funcion__c = 'EJECUTIVO',
            TAM_TipoInventario__c = 'PISO'      
        );
        insert objAdminInvDealer2;
				
		TAM_InventarioVehiculosToyota__c TAMInventarioVehiculosToyota = new TAM_InventarioVehiculosToyota__c(
			Name='MR2B29F36K1168300', 
			Dealer_Code__c='57002', 
			Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			Dummy_VIN__c='36K1168300', 
            TAM_Dummy_VIN_Temp__c='K1168300', 
			Exterior_Color_Code__c='01G3', 
			Exterior_Color_Description__c='GRAY ME.', 
			Interior_Color_Code__c='FC20', 
			Interior_Color_Description__c='S/D BLACK', 
			Model_Number__c='22060', 
			Model_Year__c='2020', 
			Toms_Series_Name__c='YARIS SD',
			TAM_ModelPhaseCode__c = '001',
			TAM_SerialNumberCDig__c = '001'
	    );
	    insert TAMInventarioVehiculosToyota;
      				
	}

    static testMethod void TAM_InventarioFabricacionTgrHandlerOK() {
        
        TAM_Inventario_A__c objInvTransUpd = new TAM_Inventario_A__c();
		//Consulta los datos del cliente    	
    	for (TAM_Inventario_A__c objInvTransuto : [Select Id, Name From TAM_Inventario_A__c LIMIT 1]){
    	    objInvTransUpd = objInvTransuto;
	   		System.debug('EN TAM_InventarioFabricacionTgrHandlerOK objInvTransuto: ' + objInvTransuto);		
    	}
        //Actualiza el objeto objInvTransUpd
        update objInvTransUpd;

    }
    
}