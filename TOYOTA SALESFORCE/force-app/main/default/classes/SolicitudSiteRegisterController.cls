public class SolicitudSiteRegisterController {
    public String csId{get;set;}
    public String email {get;set;}
    public String password {get;set;}
    public String confirmPassword {get;set;}
    
    public SolicitudSiteRegisterController(ApexPages.StandardController stdcontroller) {
        csId =stdcontroller.getId();
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }
        
        Map<String,ContactoSite__c> mapCs = new Map<String,ContactoSite__c>();
        Map<Id,ContactoSite__c> mapCsId = new Map<Id,ContactoSite__c>();
        for(ContactoSite__c cs: [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Contrasena__c, UsuarioRegistrado__c,OlvidoContrasena__c FROM ContactoSite__c]){
            mapCsId.put(cs.Id, cs);
            if(cs.Contacto__r.Email!=null){
                mapCs.put(cs.Contacto__r.Email, cs);
                System.debug(mapCs);
            }
        }
        
        List<ContactoSite__c> csUpdList = new List<ContactoSite__c>();
        if(mapCs!=null && mapCs.containsKey(email)){
            if(mapCs.get(email).UsuarioRegistrado__c==true){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Ya estas registrado, inicia sesión');
                ApexPages.addMessage(msg);
            }else{
                ContactoSite__c csUpd = new ContactoSite__c(Id=mapCs.get(email).Id);
                System.debug(mapCS.get(email).Contacto__c);
                csUpd.NombreUsuario__c = email;
                if(isValidPassword()){
                    csUpd.Contrasena__c = password;
                    csUpd.UsuarioRegistrado__c = true;
                }
                System.debug(email);
                try{
                    update csUpd;
                    PageReference page = System.Page.SolicitudSiteRegisterConfirm;
                    page.setRedirect(true);
                    return page;
                }catch(Exception e){
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() );
                    System.debug(e.getMessage());
                }
            }
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No puedes crear un usuario, porque no tienes acceso a esta función');
            ApexPages.addMessage(msg);
        }
        return null;
    }
    
    public PageReference login() {
        PageReference page = System.Page.SolicitudSiteLogin;
        page.setRedirect(true);
        return page;
    }
}