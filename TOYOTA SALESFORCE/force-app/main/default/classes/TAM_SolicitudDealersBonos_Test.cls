@isTest
public class TAM_SolicitudDealersBonos_Test {
    
    @testSetup static void setup() {
        Serie__c objSerie = new Serie__c();
        objSerie.Name = 'CAMRY';
        insert objSerie; 
        
        Product2 objProduct = new Product2();
        objProduct.Name = '2351';
        objProduct.NombreVersion__c = 'Rojo Tornado';
        objProduct.Anio__c = '2020';
        objProduct.Family = 'Toyota';
        objProduct.IdExternoProducto__c = '23512020';
        objProduct.Serie__c = objSerie.Id;
        objProduct.IsActive = true;
        objProduct.ProductCode='23512020';
        insert objProduct;
        
        TAM_Bonos__c objBono = new TAM_Bonos__c();
        objBono.Name = 'Prueba';
        objBono.TAM_InicioVigencia__c = Date.today()-5;
        objBono.TAM_FinVigencia__c = Date.today()+3;
        insert objBono;      
        
        
        TAM_DetalleBonos__c detalleBonoTest = new TAM_DetalleBonos__c();
        detalleBonoTest.Bono__c = objBono.id;
        detalleBonoTest.TAM_AnioModelo__c = '2020';
        detalleBonoTest.TAM_BonoDealerEfectivo__c = 4000;
        detalleBonoTest.TAM_BonoTFSEfectivo__c = 0;
        detalleBonoTest.TAM_BonoTMEXEfectivo__c = 4000;
        detalleBonoTest.TAM_BonoTotalEfectivo__c = 8000;
        detalleBonoTest.TAM_Clave__c = '2020';
        detalleBonoTest.TAM_CondicionesBonoEfectivo__c = 'Licencia, Factura';
        detalleBonoTest.Name = 'Bono de Verano';
        detalleBonoTest.TAM_Serie__c = 'Avanza';
        detalleBonoTest.TAM_ProductoFinanciero__c = 'Fin de año';
        detalleBonoTest.TAM_BonoTMEXProdFinanciero__c = 12000;
        detalleBonoTest.TAM_BonoDealerProdFinanciero__c = 12000;
        detalleBonoTest.TAM_BonoTotalProductoFinanciero__c = 24000;
        detalleBonoTest.TAM_BonoTFSProdFinanciero__c = 12000;
        insert detalleBonoTest;
        
        //Cuenta de tipo distribuidor
        Account acct = new Account();
        acct.name = 'DEALER DE PRUEBA';
        acct.Codigo_Distribuidor__c = '570002';
        acct.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        insert acct;
        
    }
    
    
    @isTest
    public static void metodo1(){
        TAM_SolicitudDealerBonos.consultarBonosVIN ('AVANZA','2020','2020','QWERTYU34567');
        TAM_SolicitudDealerBonos.getIncentivoCorrespondienteVIN('AVANZA','2020','2020', 'QWERTYU34567');
        
    }
    
    @isTest
    public static void metodo2(){
        TAM_SolicitudDealerBonos.getIncentivoCorrespondienteFechaVenta('AVANZA','2020','2020','QWERTYU34567','2020-06-07');
        
        TAM_DetalleBonos__c detalleBono = [Select id From TAM_DetalleBonos__c Where name  = 'Bono de Verano'];
        
        TAM_SolicitudDealerBonos.obtieneResumenBonoSeleccionado(detalleBono.id);
        
    }
    
    @isTest
    public static void metodo3(){
        TAM_DetalleBonos__c detalleBono = [Select id From TAM_DetalleBonos__c Where name  = 'Bono de Verano'];
        
        String mapaValores = '{"VIN":"5TDYZ3DC5LS035861","TAM_Serie":"COROLLA","TAM_codigoModelo":"1780","TAM_anioModelo":"2020","TAM_codigoDealer":"570002","TAM_nombreDealer":"TOYOTA TEST","TAM_cumpleConCondiciones":true,"TAM_comentarioDelaer":"<p>PRUEBA</p>","TAM_bonoSolicitado":"a27e0000001qS2NAAU","TAM_estatusVIN":"VIN en Dealer Daily","TAM_codigoVenta":"01","TAM_BonoCorrespondiente":"a27e0000001u8p1AAA"}';
        TAM_SolicitudDealerBonos.guardaSolicitudBono(mapaValores, 'En proceso aprobación (Ventas)','Con Excepción');
        
        
    }
    
    
}