public without sharing class TAM_NewProvisionControllerClass {
    
    /** Retail **/
    @AuraEnabled
    public static Id executeBatchJob(String recordId){
        Map<String, String> mapMes = new Map<String, String>();
        mapMes.put('Enero', '1');
        mapMes.put('Febrero', '2');
        mapMes.put('Marzo', '3');
        mapMes.put('Abril', '4');
        mapMes.put('Mayo', '5');
        mapMes.put('Junio', '6');
        mapMes.put('Julio', '7');
        mapMes.put('Agosto', '8');
        mapMes.put('Septiembre', '9');
        mapMes.put('Octubre', '10');
        mapMes.put('Noviembre', '11');
        mapMes.put('Diciembre', '12');
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        String mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        String anioProvision = objProvision.TAM_AnioDeProvision__c;
        
        Id batchJobId = Database.executeBatch(new TAM_ProvisionBatch(objProvision.Id, mesProvision, anioProvision));
        
        return batchJobId;
    }
    
    @AuraEnabled
    public static AsyncApexJob getBatchJobStatus(Id jobID){
        AsyncApexJob jobInfo = [SELECT Status, NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id = :jobID];
        return jobInfo;
    }
    
    /** Bonos **/
    @AuraEnabled
    public static Id executeBatchJob_Bonos(String recordId){
        Map<String, String> mapMes = new Map<String, String>();
        mapMes.put('Enero', '1');
        mapMes.put('Febrero', '2');
        mapMes.put('Marzo', '3');
        mapMes.put('Abril', '4');
        mapMes.put('Mayo', '5');
        mapMes.put('Junio', '6');
        mapMes.put('Julio', '7');
        mapMes.put('Agosto', '8');
        mapMes.put('Septiembre', '9');
        mapMes.put('Octubre', '10');
        mapMes.put('Noviembre', '11');
        mapMes.put('Diciembre', '12');
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        String mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        String anioProvision = objProvision.TAM_AnioDeProvision__c;
        
        Id batchJobId_Bonos = Database.executeBatch(new TAM_ProvisionBatchBonos(objProvision.Id, mesProvision, anioProvision));
        
        return batchJobId_Bonos;
    }
    
    @AuraEnabled
    public static AsyncApexJob getBatchJobStatus_Bonos(Id jobID){
        AsyncApexJob jobInfoBonos = [SELECT Status, NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id = :jobID];
        return jobInfoBonos;
    }

    /** Vta Corporativa **/
    @AuraEnabled
    public static Id executeBatchJob_VtaCorp(String recordId){
        Map<String, String> mapMes = new Map<String, String>();
        mapMes.put('Enero', '1');
        mapMes.put('Febrero', '2');
        mapMes.put('Marzo', '3');
        mapMes.put('Abril', '4');
        mapMes.put('Mayo', '5');
        mapMes.put('Junio', '6');
        mapMes.put('Julio', '7');
        mapMes.put('Agosto', '8');
        mapMes.put('Septiembre', '9');
        mapMes.put('Octubre', '10');
        mapMes.put('Noviembre', '11'); 
        mapMes.put('Diciembre', '12');
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        String mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        String anioProvision = objProvision.TAM_AnioDeProvision__c;
        
        Id batchJobId_VtaCorp = Database.executeBatch(new TAM_ProvisionBatch_VtaCorp(objProvision.Id, mesProvision, anioProvision, objProvision));
        
        return batchJobId_VtaCorp;
    }
    
    @AuraEnabled
    public static AsyncApexJob getBatchJobStatus_VtaCorp(Id jobID){
        AsyncApexJob jobInfo = [SELECT Status, NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id = :jobID];
        return jobInfo;
    }   
}