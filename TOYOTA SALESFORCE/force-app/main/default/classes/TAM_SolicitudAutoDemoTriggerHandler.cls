/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de procesar solicitudes de autodemo para el estado de cuenta
******************************************************************************* */
public class TAM_SolicitudAutoDemoTriggerHandler extends TriggerHandler {
    private List<TAM_SolicitudAutoDemo__c> listSolicitudAutoDemo;
    public TAM_SolicitudAutoDemoTriggerHandler(){
        this.listSolicitudAutoDemo = (List<TAM_SolicitudAutoDemo__c>) Trigger.new;
    } 
     
    public override void afterInsert(){
        //Verifica si existe un VIN , con solicitudes de auto demo (Proyecto de Incentivos)
        generaEdoCtAutosDemo(listSolicitudAutoDemo); 
    } 
    
    public override void afterUpdate(){ 
        //Verifica si existe un VIN , con solicitudes de auto demo (Proyecto de Incentivos)
        generaEdoCtAutosDemo(listSolicitudAutoDemo); 
    } 
    
    public static void generaEdoCtAutosDemo(List<TAM_SolicitudAutoDemo__c> items){
        List<TAM_EstadoCuenta__c> listadealerEstadoCuenta 	= new List<TAM_EstadoCuenta__c> ();
        List<Movimiento__c> listaMovimientos    		  	= new List<Movimiento__c> ();
        List<TAM_DetalleEstadoCuenta__c> listaDetalleEdoCta = new List<TAM_DetalleEstadoCuenta__c> ();
        List<TAM_DetalleEstadoCuenta__c> detalleEdoCta 		= new List<TAM_DetalleEstadoCuenta__c>();
        List<Date> fechaVenta 								= new List<Date>();
        Map<Id, TAM_EstadoCuenta__c> mapEstadoCta 			= new Map<Id, TAM_EstadoCuenta__c>();
        map<String,TAM_EstadoCuenta__c> edoCtaMap 			= new map<String,TAM_EstadoCuenta__c>();
        Map<String,Movimiento__c> mapInformacionVIN 		= new Map<String,Movimiento__c> ();
        Map<String,TAM_EstadoCuenta__c> mapaEdoCtas 		= new  Map<String,TAM_EstadoCuenta__c>();
        Set<TAM_EstadoCuenta__c> edoCtaSet 					= new Set<TAM_EstadoCuenta__c>();
        Set<String> VINSolicitud 							= new Set<String>();
        Set<String> codigoDealer 							= new Set<String>();
        Set<String> idMovimiento 							= new Set<String>();
        TAM_EstadoCuenta__c estadoCuenta 					= new TAM_EstadoCuenta__c ();
        Movimiento__c  detalleVIN        					= new Movimiento__c ();
        
        decimal diasVigencia = [Select TAM_DiasVigencia__c From TAM_DetalleEdoCta__c Limit 1].TAM_DiasVigencia__c;
        Integer diasVigenciaInt = diasVigencia.intValue();
        
        //Recordtype detalle estado de cuenta de tipo Auto Demo
        String recordIdBono = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('AutoDemo').getRecordTypeId();
        
        //Se agrega a una lista de String el VIN y código dealer de cada una de las solicitudes insertadas
        if(!items.isEmpty()){
            for(TAM_SolicitudAutoDemo__c solIn : items){
                VINSolicitud.add(solIn.TAM_VIN__c);
                codigoDealer.add(solIn.TAM_CodigoDistribuidor__c);
                idMovimiento.add(solIn.TAM_Movimiento__c);
                fechaVenta.add(solIn.createdDate.date());
            }
        }
        
        //Si la lista de VIN es diferente a vacia se hace un query para obtener información de la venta del VIN del objeto Movimiento
        if(!VINSolicitud.isEmpty()){
            listaMovimientos = [Select id,vin__r.name,Submitted_Date__c,name,TAM_Serie__c,TAM_VIN__c,Last_Name__c,NombreCliente__c,Sale_Date__c,
                                Trans_Type__c   From Movimiento__c WHERE id IN : idMovimiento
                               ];  
        } 
        
        //Si la el query a Movimiento es diferente de vacio se agrega en un mapa teniendo como llave el VIN y como valor el objeto Movimiento
        if(!listaMovimientos.isEmpty()){
            for(Movimiento__c mov : listaMovimientos){
                mapInformacionVIN.put(mov.vin__r.name,mov);
                
            }   
        } 
        
        //SE CREA EL ESTADO DE CUENTA A PARTIR DE LA FECHA DE VENTA  
        List<String>listaFechas = TAM_ScheduledSolicitudIncentivos.obtieneFechaTextoList(fechaVenta);
        if(!listaFechas.isEmpty()){
            for(TAM_SolicitudAutoDemo__c inSol : items){
                if(mapInformacionVIN.containsKey(inSol.TAM_NumeroVIN__c)){
                    detalleVIN  = mapInformacionVIN.get(inSol.TAM_NumeroVIN__c);
                    String fechaEdoCta = TAM_ScheduledSolicitudIncentivos.obtieneFechaTexto(inSol.createdDate.date().addMonths(-1));
                    TAM_EstadoCuenta__c dealerEstadoCuenta = new TAM_EstadoCuenta__c();
                    dealerEstadoCuenta.TAM_CodigoDealer__c = inSol.TAM_CodigoDistribuidor__c;
                    dealerEstadoCuenta.TAM_NombreDealer__c = inSol.TAM_NombreDistribuidor__c;
                    dealerEstadoCuenta.TAM_FechaEstadoCuenta__c = fechaEdoCta;
                    dealerEstadoCuenta.Name = inSol.TAM_CodigoDistribuidor__c+'-'+fechaEdoCta;                
                    dealerEstadoCuenta.Id_Externo__c      = inSol.TAM_CodigoDistribuidor__c+'-'+fechaEdoCta;
                    edoCtaSet.add(dealerEstadoCuenta);
                    edoCtaMap.put(inSol.TAM_CodigoDistribuidor__c+'-'+fechaEdoCta,dealerEstadoCuenta);
                }
            }
            
            //Si la lista es diferente de vacia se insertan los registros
            if(!edoCtaMap.isEmpty()){
                Schema.SObjectField idExterno = TAM_EstadoCuenta__c.Fields.Id_Externo__c;
                try{
                    Database.UpsertResult [] cr = Database.upsert(edoCtaMap.values(),idExterno, false);  
                    for (Database.UpsertResult objDtSvr : cr){
                        if(!objDtSvr.isSuccess())
                            System.debug('Regitros edos de cuenta con errores autodemo' + objDtSvr.getErrors()[0].getMessage());
                        
                        if(objDtSvr.isSuccess())
                            System.debug('Regitros edos de cuenta insertados autodemo' + objDtSvr.id);
                        
                    } 
                }catch(Exception e){
                    system.debug('Error en upsert de la lista listadealer EstadoCuenta'+e.getMessage());  
                } 
            } 
        }
        
        
        for(String mapValue : edoCtaMap.keySet()){
            mapaEdoCtas.put(mapValue,edoCtaMap.get(mapValue));
            
        }
        
        //Se crean las lineas del estado de cuenta de tipo "Bono"
        map<String,TAM_DetalleEstadoCuenta__c> edoDetalleEdoCta = new map<String,TAM_DetalleEstadoCuenta__c>();
        if(!items.isEmpty()){ 
            for(TAM_SolicitudAutoDemo__c solBono : items){
                if(mapInformacionVIN.containsKey(solBono.TAM_NumeroVIN__c)){
                    detalleVIN  = mapInformacionVIN.get(solBono.TAM_NumeroVIN__c);
                    String periodoVenta = TAM_ScheduledSolicitudIncentivos.obtieneFechaTexto(solBono.createdDate.date().addMonths(-1));
                    if(mapaEdoCtas.containsKey(solBono.TAM_CodigoDistribuidor__c+'-'+periodoVenta) && mapInformacionVIN.containsKey(solBono.TAM_NumeroVIN__c)){
                        estadoCuenta = mapaEdoCtas.get(solBono.TAM_CodigoDistribuidor__c+'-'+periodoVenta);
                        detalleVIN  = mapInformacionVIN.get(solBono.TAM_NumeroVIN__c);
                        TAM_DetalleEstadoCuenta__c lineaDetalle = new TAM_DetalleEstadoCuenta__c();
                        lineaDetalle.TAM_FechaCaducidad__c = date.today()+diasVigenciaInt;
                        lineaDetalle.TAM_SolicitudAutoDemo__c = solBono.id;
                        lineaDetalle.TAM_FacturaAutoDemo__c =  solBono.TAM_Factura__c;
                        lineaDetalle.TAM_RegistroAutoDemo__c = solBono.TAM_AutoDemo__c;
                        lineaDetalle.TAM_CodigoDealer__c = solBono.TAM_CodigoDistribuidor__c;
                        lineaDetalle.TAM_NombreDealer__c = solBono.TAM_NombreDistribuidor__c;
                        lineaDetalle.TAM_VIN__c    =  detalleVIN.vin__r.name;
                        lineaDetalle.TAM_Serie__c  =  detalleVIN.TAM_Serie__c;
                        lineaDetalle.TAM_Codigo_Producto__c = solBono.TAM_Modelo__c;
                        lineaDetalle.TAM_AnioModelo__c     = solBono.TAM_AnioModelo__c;
                        lineaDetalle.TAM_EstadoCuenta__c   = estadoCuenta.Id;
                        lineaDetalle.RecordTypeId = recordIdBono;
                        lineaDetalle.TAM_Movimiento__c = detalleVIN.Trans_Type__c;
                        lineaDetalle.TAM_FechaVenta__c = solBono.TAM_FechaTimbradoFactura__c.date();
                        lineaDetalle.TAM_NombrePropietario__c = detalleVIN.NombreCliente__c;
                        lineaDetalle.TAM_ApellidosPropietario__c = detalleVIN.Last_Name__c;
                        lineaDetalle.TAM_IncentivoPropuestoDealer__c = solBono.TAM_CostoTMEXSinIVA__c;
                        lineaDetalle.TAM_Monto_sin_IVA__c = solBono.TAM_CostoTMEXSinIVA__c; 
                        lineaDetalle.TAM_IntercambioEdoCta__c = false;
                        lineaDetalle.TAM_IDExterno__c = estadoCuenta.Id+detalleVIN.TAM_VIN__c;
                        lineaDetalle.TAM_Estatus_Finanzas__c = 'Pendiente';
                        detalleEdoCta.add(lineaDetalle);
                        edoDetalleEdoCta.put(estadoCuenta.Id+detalleVIN.TAM_VIN__c,lineaDetalle);
                    }
                }
                
                if(!detalleEdoCta.isEmpty()){
                    Schema.SObjectField idExterno = TAM_DetalleEstadoCuenta__c.Fields.TAM_IDExterno__c;
                    try{
                        Set<String> registrosInsertados = new Set<String>();
                        List<Database.UpsertResult> lcr = Database.upsert(edoDetalleEdoCta.values(),idExterno, false);
                        for (Database.UpsertResult objDtSvr : lcr){
                            if(objDtSvr.isSuccess())  
                                registrosInsertados.add(objDtSvr.id);
                            system.debug('Id Registros insertados'+objDtSvr.id);
                            //update items; 
                            if(!objDtSvr.isSuccess())
                                System.debug('Registros con error:' + objDtSvr.getErrors()[0].getMessage());
                        }      
                        actualizaFlagAutoDemo(registrosInsertados);
                    }catch(Exception e){
                        system.debug('Error general al momento de insertar los registros de estado de cuenta de Auto Demo'+ e.getMessage());
                    }
                }  
            }
        }
    }  
    
    //Se actualiza la bandera a nivel solicitud de auto demo  para indicar que ya fue creada en un estado de cuenta y no se puede volver a crear
    public static void actualizaFlagAutoDemo(Set<String>listDetallesCreados){   
        Set<String> listaVINES = new Set<String>();
        List<TAM_DetalleEstadoCuenta__c> detalleEdoCta  = new  List<TAM_DetalleEstadoCuenta__c> ();
        List<TAM_SolicitudAutoDemo__c> solicitudesAutoDemo  = new List<TAM_SolicitudAutoDemo__c>();
        List<TAM_SolicitudAutoDemo__c> solicitudesAutoDemoUpd  = new List<TAM_SolicitudAutoDemo__c>();
        if(!listDetallesCreados.isEmpty()){
            detalleEdoCta = [Select id,TAM_VIN__C From TAM_DetalleEstadoCuenta__c WHERE ID IN: listDetallesCreados];
            
        }
        
        if(!detalleEdoCta.isEmpty()){
            for(TAM_DetalleEstadoCuenta__c detalle : detalleEdoCta){
                listaVINES.add(detalle.TAM_VIN__C);
                
            }
            
        }
        
        
        if(!listaVINES.isEmpty()){
            solicitudesAutoDemo = [select id,TAM_NumeroVIN__c,TAM_EdoCuenta__c  FROM TAM_SolicitudAutoDemo__c WHERE 
                                   TAM_NumeroVIN__c IN:listaVINES AND TAM_EdoCuenta__c = false];
            
        }
        
        if(!solicitudesAutoDemo.isEmpty()){
            for(TAM_SolicitudAutoDemo__c inSol : solicitudesAutoDemo){
                TAM_SolicitudAutoDemo__c newItem = new TAM_SolicitudAutoDemo__c();
                newItem.id = inSol.id;
                newItem.TAM_EdoCuenta__c = true;
                solicitudesAutoDemoUpd.add(newItem);
            }
            
        }
        
        if(!solicitudesAutoDemoUpd.isEmpty()){       
            update solicitudesAutoDemoUpd;
        }
        
        
    }
    
}