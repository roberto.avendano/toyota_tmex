/**
    Descripción General: Controlador para el componente "greedIncentivos"
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        14/Nov/2019         Versión Inicial
    ________________________________________________________________
**/
public without sharing class TAM_GridClass {
    
    /**Obtener un Set de las series disponibles */
    @AuraEnabled
    public static Map<String, List<String>> getSeries() {
        List<CatalogoCentralizadoModelos__c> lstSeries = [  SELECT  Serie__c,
                                                                    AnioModelo__c
                                                            FROM    CatalogoCentralizadoModelos__c 
                                                            ORDER BY Name];
        List<String> lstSerie;
        Map<String, List<String>> mapModelos = new Map<String, List<String>>();

        for(CatalogoCentralizadoModelos__c objCatalogoModelo : lstSeries){
            lstSerie = mapModelos.get(objCatalogoModelo.AnioModelo__c);

            if(mapModelos.containsKey(objCatalogoModelo.AnioModelo__c)){
               
                if(!lstSerie.contains(objCatalogoModelo.Serie__c)){
                    lstSerie.add(objCatalogoModelo.Serie__c);
                    mapModelos.put(objCatalogoModelo.AnioModelo__c, lstSerie);
                }
            } else {
                lstSerie = new List<String>();
                lstSerie.add(objCatalogoModelo.Serie__c);
                mapModelos.put(objCatalogoModelo.AnioModelo__c, lstSerie);
            }
        }        
        return mapModelos;
    }

    /** Obtener lista de rangos disponibles*/
    @AuraEnabled
    public static List<Rangos__c> getRangos(){
        List<Rangos__c> lstRangos = [SELECT Id, Name FROM Rangos__c WHERE Activo__c = true];
        return lstRangos;
    }

    /**Obtener tipo de registro de la politica de incentivos */
    @AuraEnabled
    public static String TipoRegistroPolitica(String recordId){
        List<TAM_PoliticasIncentivos__c> lstPoliticasIncentivos = [SELECT RecordTypeId FROM TAM_PoliticasIncentivos__c WHERE Id =: recordId];
        String strRecordTypeId = lstPoliticasIncentivos[0].RecordTypeId;
        String strRecordTypeName = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosById().get(strRecordTypeId).getname();
        return strRecordTypeName;
    }
    

    /** Añadir Información a Lista de tipo Wrapper "TAM_GridWrapperClass" */
    @AuraEnabled
    public static List<TAM_GridWrapperClass> setWrapper(List<String> openSections, String recordId){
        String strCodigoModelo; 
        String idExterno;
        String idExternoInventario;
        Integer intCantidadInventario;
        Double intMargenTMEX_Porcentaje;
        Integer intMargenTMEX_Pesos;
        Integer intMSRP;
        Integer intDiasInventario;
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado;
        List<InventarioPiso__c> lstInventarioPiso;
        List<TAM_GridWrapperClass> lstGridWrapper = new List<TAM_GridWrapperClass>();
        Set<String> setIdExternoModelos = new Set<String>();
        Map<String, CatalogoCentralizadoModelos__c> mapCatalogoCentralizado  = new Map<String, CatalogoCentralizadoModelos__c>();
        Map<String, InventarioPiso__c> mapInventarioPiso = new Map<String, InventarioPiso__c>();
        Map<String, TAM_DetallePolitica__c> mapDetallePoliticas = new Map<String, TAM_DetallePolitica__c>();

        //Obtener información del obj Detalle de Politicas
        String idExternoDetallePolitica;
        List<Integer> lstConsecutivo = new List<Integer>();
        Map<String, List<Integer>> mapConsecutivo = new Map<String, List<Integer>>();
        List<String> lstIdsDetalle = new List<String>();

        List<TAM_DetallePolitica__c> lstDetallePolitica = [SELECT   TAM_AnioModelo__c,
                                                                    TAM_Serie__c,
                                                                    TAM_Clave__c,
                                                                    TAM_Version__c,
                                                                    TAM_Inventario_Dealer__c,
                                                                    TAM_Dias_de_Inventario__c,
                                                                    TAM_Venta_MY__c,
                                                                    TAM_BalanceOut__c,
                                                                    TAM_Pago_TFS__c,
                                                                    TAM_Incentivo_actual__c,
                                                                    TAM_IncPropuesto_Cash__c,
                                                                    TAM_IncPropuesto_Porcentaje__c,
                                                                    TAM_IncentivoTMEX_Cash__c,
                                                                    TAM_IncentivoTMEX_Porcentaje__c,
                                                                    TAM_IncDealer_Cash__c,
                                                                    TAM_IncDealer_Porcentaje__c,
                                                                    TAM_Tripleplay__c,
                                                                    TAM_ProductoFinanciero__c,
                                                                    TAM_Descripcion__c,
                                                                    TAM_TotalEstimado__c,
                                                                    TAM_IncTFS_Cash__c,
                                                                    TAM_IncTFS_Porcentaje__c,
                                                                    TAM_MSRP__c,
                                                                    TAM_MSRPIncentivo__c,
                                                                    TAM_TMEX_Margen_Pesos__c,
                                                                    TAM_TMEX_Margen_Porcentaje__c,
                                                                    TAM_TMEX_MargenDespuesInc_Pesos2__c,
                                                                    TAM_TMEX_MargenDespuesInc_Porcen2__c,
                                                                    TAM_CostoTMexIVAF__c,
                                                                    TAM_IdExterno__c,
                                                                    TAM_Rango__c,
                                                                    TAM_TP_IncTMEX_Cash__c,
                                                                    TAM_TP_IncTMEX_Porcentaje__c,
                                                                    TAM_TP_IncDealer_Cash__c,
                                                                    TAM_TP_IncDealer_Porcentaje__c,
                                                                    TAM_Duplicar__c,
                                                                    TAM_Consecutivo__c,
                                                                    TAM_Lealtad__c,
                                                                    TAM_BonoLealtad__c
                                                            FROM    TAM_DetallePolitica__c];
        for(TAM_DetallePolitica__c objDetalle : lstDetallePolitica){
            idExternoDetallePolitica = objDetalle.TAM_IdExterno__c;
            mapDetallePoliticas.put(idExternoDetallePolitica, objDetalle);

            lstConsecutivo.add(Integer.valueOf(objDetalle.TAM_Consecutivo__c));
            mapConsecutivo.put(idExternoDetallePolitica, lstConsecutivo);
            lstIdsDetalle.add(idExternoDetallePolitica);
        }

        //Obtener Información de catálogo centralizado
        if(mapCatalogoCentralizado.isEmpty()){
            lstCatalogoCentralizado = [SELECT   Modelo__c,
                                                AnioModelo__c,
                                                Serie__c,
                                                Disponible__c,
                                                Version__c,
                                                Serie_AnioModelo__c
                                        FROM    CatalogoCentralizadoModelos__c 
                                        WHERE   Serie_AnioModelo__c IN: openSections];

            for(CatalogoCentralizadoModelos__c objCatalogo : lstCatalogoCentralizado){
                idExterno = objCatalogo.Serie__c + objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
                idExterno = idExterno.toUpperCase();
                setIdExternoModelos.add(idExterno);
                mapCatalogoCentralizado.put(idExterno,objCatalogo);
            }
        }

        //Obtener Información de Inventario
        if(mapInventarioPiso.isEmpty()){
            Date dateFechaInventario;
            Boolean boolCargaHistorica = false;
            List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
            boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
            if(boolCargaHistorica){
                dateFechaInventario = CargaHistorica[0].TAM_InicioVigencia__c;
            } else {
                dateFechaInventario= System.today() - 1;
            }
            
            lstInventarioPiso = [SELECT AnioModelo__c,
                                        Modelo__c,
                                        Serie__c,
                                        Version__c,
                                        Cantidad__c,
                                        DiasVenta__c,
                                        Serie_AnioModelo__c
                                FROM    InventarioPiso__c
                                WHERE   FechaInventario__c =:dateFechaInventario AND Serie_AnioModelo__c IN: openSections];
            
            Decimal intCantidadTotal;
            Decimal intDiasInventarioAux;
            Integer intDiasInventarioTotal;
            if(!lstInventarioPiso.isEmpty()){
                for(InventarioPiso__c objInventario : lstInventarioPiso){
                    idExternoInventario = objInventario.Serie__c + objInventario.Modelo__c +  objInventario.AnioModelo__c;
                    idExternoInventario = idExternoInventario.toUpperCase();
    
                    if(mapInventarioPiso.containsKey(idExternoInventario)){
                        intCantidadTotal =  mapInventarioPiso.get(idExternoInventario).Cantidad__c + objInventario.Cantidad__c;
                        objInventario.Cantidad__c = intCantidadTotal;
    
                        intDiasInventarioAux = mapInventarioPiso.get(idExternoInventario).DiasVenta__c;
                        intDiasInventarioTotal = Integer.valueOf(intDiasInventarioAux.round(System.RoundingMode.HALF_UP));
    
                        if(objInventario.DiasVenta__c < intDiasInventarioTotal){
                            objInventario.DiasVenta__c = intDiasInventarioTotal;
                        }
                    }
                    mapInventarioPiso.put(idExternoInventario,objInventario);
                }
            }
        }

        //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual;
        Boolean boolCargaHistorica = false;
        List<TAM_PoliticasIncentivos__c> CargaHistorica = [SELECT TAM_CargaHistorica__c, TAM_InicioVigencia__c FROM TAM_PoliticasIncentivos__c WHERE id =:recordId LIMIT 1];
        boolCargaHistorica = CargaHistorica[0].TAM_CargaHistorica__c;
        if(boolCargaHistorica){
            fechaActual = CargaHistorica[0].TAM_InicioVigencia__c;
        } else {
            fechaActual= System.today();
        }
        //Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'Enero');
        mapMes.put(2, 'Febrero');
        mapMes.put(3, 'Marzo');
        mapMes.put(4, 'Abril');
        mapMes.put(5, 'Mayo');
        mapMes.put(6, 'Junio');
        mapMes.put(7, 'Julio');
        mapMes.put(8, 'Agosto');
        mapMes.put(9, 'Septiembre');
        mapMes.put(10, 'Octubre');
        mapMes.put(11, 'Noviembre');
        mapMes.put(12, 'Diciembre');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;

        //Mapa de catálogo de lista de precios.
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                            ProductCode,
                                                            PrecioSinIva__c,
                                                            TAM_MargenesGananciaTMEX_Pesos__c,
                                                            TAM_MargenesGananciaTMEX_Porc__c,
                                                            TAM_MSRP__c
                                                    FROM    PricebookEntry
                                                    WHERE   IsActive = true 
                                                    AND     Pricebook2.Name =:strNombreCatalogoPrecios
        ];
        String strCodigoProducto;
        Double dblPrecioSinIVA;
        
        Map<String,PricebookEntry> mapPriceBookEntry = new Map<String,PricebookEntry>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.ProductCode;
            mapPriceBookEntry.put(strCodigoProducto, objListaPrecio);
            
        }

        //Llenado para mostrar lista wrapper
        if(setIdExternoModelos.contains(idExterno)){
            for(String strIdModelos : setIdExternoModelos){

                strIdModelos= strIdModelos.toUpperCase();

                //validacion de mapa de pricebookEntry
                strCodigoModelo = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) +  String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
                strCodigoModelo = strCodigoModelo.toUpperCase();
                if(mapPriceBookEntry.containsKey(strCodigoModelo)){
                    intMargenTMEX_Porcentaje = Double.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Porc__c);
                    intMargenTMEX_Pesos = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MargenesGananciaTMEX_Pesos__c);
                    intMSRP = Integer.valueOf(mapPriceBookEntry.get(strCodigoModelo).TAM_MSRP__c);
                } else {
                    intMargenTMEX_Porcentaje = 0;
                    intMargenTMEX_Pesos = 0;
                    intMSRP = 0;
                }

                //validación de mapa de inventarios
                if(mapInventarioPiso.containsKey(strIdModelos)){
                    intCantidadInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).Cantidad__c);
                    intDiasInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).DiasVenta__c);
                } else {
                    intCantidadInventario = 0;
                    intDiasInventario = 0;
                }

                String strSerieAnio = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
                if(openSections.contains(strSerieAnio)){

                    String strIdDetalles = recordId + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c);
                    String strIdDetallePolitica = '';
                    for(String strIdDetalle : lstIdsDetalle){
                        if(strIdDetalle.contains(strIdDetalles)){
                            strIdDetallePolitica = strIdDetalle;

                            if(mapDetallePoliticas.containsKey(strIdDetallePolitica)){
                                TAM_GridTripePayWrapperClass objTriplePlay = new TAM_GridTripePayWrapperClass(  /** Triple Play */   mapDetallePoliticas.get(strIdDetallePolitica).TAM_Tripleplay__c,
                                                                                                                /**Prod_Fnz     */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_ProductoFinanciero__c),
                                                                                                                /**Descripción  */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Descripcion__c),
                                                                                                                /**TotalEstimad */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TotalEstimado__c),
                                                                                                                /**Incen_TFS$   */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncTFS_Cash__c),
                                                                                                                /**Incen_TFS%   */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncTFS_Porcentaje__c),
                                                                                                                /**Incen_TMEX$  */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TP_IncTMEX_Cash__c),
                                                                                                                /**Incen_TMEX%  */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TP_IncTMEX_Porcentaje__c),
                                                                                                                /**Incen_Dealer$*/   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TP_IncDealer_Cash__c),
                                                                                                                /**Incen_Dealer%*/   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TP_IncDealer_Porcentaje__c)
                                                                                                            );

                                lstGridWrapper.add(new TAM_GridWrapperClass(    /** Año Modelo */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_AnioModelo__c) +'-'+String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Serie__c),
                                                                                /** Modelo     */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Clave__c),
                                                                                /** Versión    */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Version__c),
                                                                                /** Inventario */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Inventario_Dealer__c),
                                                                                /** Dias Venta */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Dias_de_Inventario__c),
                                                                                /** Venta MY   */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Venta_MY__c),
                                                                                /** Balance Out*/   mapDetallePoliticas.get(strIdDetallePolitica).TAM_BalanceOut__c,
                                                                                /** Pago TFS   */   mapDetallePoliticas.get(strIdDetallePolitica).TAM_Pago_TFS__c,
                                                                                /**Incen_Act   */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Incentivo_actual__c),
                                                                                /**Incen_Prop$ */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncPropuesto_Cash__c),
                                                                                /**Incen_Prop% */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncPropuesto_Porcentaje__c),
                                                                                /**Incen_TMEX$ */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncentivoTMEX_Cash__c),
                                                                                /**Incen_TMEX% */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncentivoTMEX_Porcentaje__c),                                         
                                                                                /**Incen_Deal$ */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncDealer_Cash__c),
                                                                                /**Incen_Deal% */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_IncDealer_Porcentaje__c),
                                                                                /**Triple Play */   objTriplePlay,
                                                                                /**MSRP        */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_MSRP__c),
                                                                                /**MSRP-Incen  */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_MSRPIncentivo__c),
                                                                                /**TMEX_Marg$  */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TMEX_Margen_Pesos__c),
                                                                                /**TMEX_Marg%  */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TMEX_Margen_Porcentaje__c),
                                                                                /**TMEX_Marg_A$*/   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TMEX_MargenDespuesInc_Pesos2__c),
                                                                                /**TMEX_Marg_A%*/   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_TMEX_MargenDespuesInc_Porcen2__c),
                                                                                /**Costo_TMEX  */   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_CostoTMexIVAF__c),
                                                                                /**Rango       */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Rango__c),
                                                                                /**Clonado     */   mapDetallePoliticas.get(strIdDetallePolitica).TAM_Duplicar__c,
                                                                                /**Serie       */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Serie__c),
                                                                                /** Año Modelo */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_AnioModelo__c),
                                                                                /**Consecutivo */   Integer.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Consecutivo__c),
                                                                                /**Clave Orden */   String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Clave__c) + String.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_Consecutivo__c),
                                                                                /**Lealtad     */   mapDetallePoliticas.get(strIdDetallePolitica).TAM_Lealtad__c,
                                                                                /**Bono Lealtad*/   Double.valueOf(mapDetallePoliticas.get(strIdDetallePolitica).TAM_BonoLealtad__c)
                                ));
                            } 
                        } 
                    }

                    if(String.isEmpty(strIdDetallePolitica)) {
                        TAM_GridTripePayWrapperClass objTriplePlay = new TAM_GridTripePayWrapperClass(   /** Triple Play */   false,
                                                                                                            /**Prod_Fnz     */   null,
                                                                                                            /**Descripción  */   null,
                                                                                                            /**TotalEstimad */   null,
                                                                                                            /**Incen_TFS$   */   null,
                                                                                                            /**Incen_TFS%   */   null,
                                                                                                            /**Incen_TMEX$  */   null,
                                                                                                            /**Incen_TMEX%  */   null,
                                                                                                            /**Incen_Dealer$*/   null,
                                                                                                            /**Incen_Dealer%*/   null
                                                                                                        );

                        lstGridWrapper.add(new TAM_GridWrapperClass(    /** Año Modelo */   String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) +'-'+String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c),
                                                                        /** Modelo     */   String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c),
                                                                        /** Versión    */   String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Version__c),
                                                                        /** Inventario */   intCantidadInventario,
                                                                        /** Dias Venta */   intDiasInventario,
                                                                        /** Venta MY   */   null,
                                                                        /** Balance Out*/   false,
                                                                        /** Pago TFS   */   false,
                                                                        /**Incen_Act   */   null,
                                                                        /**Incen_Prop$ */   null,
                                                                        /**Incen_Prop% */   null,
                                                                        /**Incen_TMEX$ */   null,
                                                                        /**Incen_TMEX% */   null,                                         
                                                                        /**Incen_Deal$ */   null,
                                                                        /**Incen_Deal% */   null,
                                                                        /**Triple Play */   objTriplePlay,
                                                                        /**MSRP        */   intMSRP,
                                                                        /**MSRP-Incen  */   null,
                                                                        /**TMEX_Marg$  */   intMargenTMEX_Pesos,
                                                                        /**TMEX_Marg%  */   intMargenTMEX_Porcentaje,
                                                                        /**TMEX_Marg_A$*/   null,
                                                                        /**TMEX_Marg_A%*/   null,
                                                                        /**Costo_TMEX  */   null,
                                                                        /**Rango       */   null,
                                                                        /**Clonado     */   false,
                                                                        /**Serie       */   String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c),
                                                                        /** Año Modelo */   String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c),
                                                                        /**Consecutivo */   0,
                                                                        /**Clave Orden */	String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c) + '0',
                                                                        /**Lealtad     */   false,
                                                                        /**Bono Lealtad*/   null
                        ));
                    }
                }
            }
        }
        return lstGridWrapper;
    }

    /** Guardado de politica de incentivos dentro del obj "Detalle de la politica" */
    @AuraEnabled
    public static List<TAM_DetallePolitica__c> guardarPolitica(String lstWrapper, String recordId){ 

        List<TAM_DetallePolitica__c> lstDetallePolitica = new List<TAM_DetallePolitica__c>();
        List<TAM_GridWrapperClass> lstWrapperContent = new List<TAM_GridWrapperClass>();

		if (lstWrapper != NULL){
			lstWrapperContent = TAM_GridWrapperClass.JSONParserSFDC(lstWrapper);
            if (!lstWrapperContent.isEmpty()){

                for (TAM_GridWrapperClass objDetalleWrapper  : lstWrapperContent){
                    if(objDetalleWrapper.intIncentivoPropuesto_Pesos != null || objDetalleWrapper.intIncentivoPropuesto_Porcentaje != null){
                        TAM_DetallePolitica__c objDetallePoliticaAux = new TAM_DetallePolitica__c();
                        objDetallePoliticaAux.TAM_Serie__c = objDetalleWrapper.strSerie;
                        objDetallePoliticaAux.TAM_AnioModelo__c = objDetalleWrapper.strAnioModelo;
                        objDetallePoliticaAux.TAM_Clave__c = objDetalleWrapper.strClaveModelo;
                        objDetallePoliticaAux.TAM_Version__c = objDetalleWrapper.strVersion;
                        objDetallePoliticaAux.TAM_Inventario_Dealer__c = objDetalleWrapper.intInventario;
                        objDetallePoliticaAux.TAM_Dias_de_Inventario__c = objDetalleWrapper.intDiasInventario;
                        objDetallePoliticaAux.TAM_Venta_MY__c = objDetalleWrapper.intVentaMY;
                        objDetallePoliticaAux.TAM_BalanceOut__c = objDetalleWrapper.boolBalanceOut;
                        objDetallePoliticaAux.TAM_Pago_TFS__c = objDetalleWrapper.boolPagoTFS;
                        objDetallePoliticaAux.TAM_Incentivo_actual__c = objDetalleWrapper.intIncentivoActual;
                        objDetallePoliticaAux.TAM_IncPropuesto_Cash__c = objDetalleWrapper.intIncentivoPropuesto_Pesos;
                        objDetallePoliticaAux.TAM_IncPropuesto_Porcentaje__c = objDetalleWrapper.intIncentivoPropuesto_Porcentaje;
                        objDetallePoliticaAux.TAM_IncentivoTMEX_Cash__c = objDetalleWrapper.intIncentivoTMEX_Pesos;
                        objDetallePoliticaAux.TAM_IncentivoTMEX_Porcentaje__c = objDetalleWrapper.intIncentivoTMEX_Porcentaje;
                        objDetallePoliticaAux.TAM_IncDealer_Cash__c = objDetalleWrapper.intIncentivoDealer_Pesos;
                        objDetallePoliticaAux.TAM_IncDealer_Porcentaje__c = objDetalleWrapper.intIncentivoDealer_Porcentaje;
                        objDetallePoliticaAux.TAM_Tripleplay__c = objDetalleWrapper.objTriplePlay.boolTriplePlay;
                        objDetallePoliticaAux.TAM_ProductoFinanciero__c = objDetalleWrapper.objTriplePlay.strProductoFinanciero;
                        objDetallePoliticaAux.TAM_Descripcion__c = objDetalleWrapper.objTriplePlay.strDescripcion;
                        objDetallePoliticaAux.TAM_TotalEstimado__c = objDetalleWrapper.objTriplePlay.intTotalEstimado;
                        objDetallePoliticaAux.TAM_IncTFS_Cash__c = objDetalleWrapper.objTriplePlay.intIncentivoTFS_Pesos;
                        objDetallePoliticaAux.TAM_IncTFS_Porcentaje__c = objDetalleWrapper.objTriplePlay.intIncentivoTFS_Porcentaje;
                        objDetallePoliticaAux.TAM_TP_IncTMEX_Cash__c = objDetalleWrapper.objTriplePlay.intIncentivoTMEX_Pesos;
                        objDetallePoliticaAux.TAM_TP_IncTMEX_Porcentaje__c = objDetalleWrapper.objTriplePlay.intIncentivoTMEX_Porcentaje;
                        objDetallePoliticaAux.TAM_TP_IncDealer_Cash__c = objDetalleWrapper.objTriplePlay.intIncentivoDealer_Pesos;
                        objDetallePoliticaAux.TAM_TP_IncDealer_Porcentaje__c = objDetalleWrapper.objTriplePlay.intIncentivoDealer_Porcentaje;
                        objDetallePoliticaAux.TAM_MSRP__c = objDetalleWrapper.intMSRP;
                        objDetallePoliticaAux.TAM_TMEX_Margen_Pesos__c = objDetalleWrapper.intTMEXMargen_Pesos;
                        objDetallePoliticaAux.TAM_TMEX_Margen_Porcentaje__c = objDetalleWrapper.intTMEXMargen_Porcentaje;
                        //objDetallePoliticaAux.TAM_CostoTMexIVAF__c = objDetalleWrapper.intCostoTMEX;
                        objDetallePoliticaAux.TAM_IdExterno__c = recordId + objDetalleWrapper.strAnioModelo + objDetalleWrapper.strSerie + objDetalleWrapper.strClaveModelo + objDetalleWrapper.intConsecutivo;
                        objDetallePoliticaAux.TAM_PoliticaIncentivos__c = recordId;
                        objDetallePoliticaAux.TAM_Rango__c = objDetalleWrapper.strRango;
                        objDetallePoliticaAux.TAM_Duplicar__c = objDetalleWrapper.boolClonado;
                        objDetallePoliticaAux.Name = objDetalleWrapper.strAnioModelo + '-' + objDetalleWrapper.strSerie + '-' + objDetalleWrapper.strClaveModelo + '-' + objDetalleWrapper.strVersion + objDetalleWrapper.intConsecutivo;
                        objDetallePoliticaAux.TAM_Consecutivo__c = objDetalleWrapper.intConsecutivo;
                        objDetallePoliticaAux.TAM_Lealtad__c = objDetalleWrapper.boolLealtad;
                        objDetallePoliticaAux.TAM_BonoLealtad__c = objDetalleWrapper.dblBonoLealtad;
                        
                        lstDetallePolitica.add(objDetallePoliticaAux);
                    }   
                }
            }
        }
        try {
            Database.upsert(lstDetallePolitica, TAM_DetallePolitica__c.TAM_IdExterno__c, false);
            //upsert lstDetallePolitica; 
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetallePolitica; 
    }
}