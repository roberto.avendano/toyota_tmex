public with sharing class MergeCellsProductTabPDFController {
	public List<EspecificacionFichaTecnica__c> atributos{get;set;}
	public Map<Integer, Contadores> rowsMap{get;set;}

	public MergeCellsProductTabPDFController() {
		this.rowsMap = new Map<Integer, Contadores>();
		this.atributos = new List<EspecificacionFichaTecnica__c>();
	}

	public String getObtenerMapa(){
		Integer claveMap = 0;

		for(Integer i = 0; i < atributos.size(); i++){
			if(i == 0){
				rowsMap.put(claveMap, new Contadores(atributos[i].Atributo__c, 1));
			}

			try{
				if(atributos[i].Atributo__c == atributos[i+1].Atributo__c){
					
					rowsMap.get(claveMap).colspan++;
				
				} else {
					claveMap++;
					rowsMap.put(claveMap, new Contadores(atributos[i+1].Atributo__c, 1));
				}

			} catch(Exception ex){
				//System.debug(ex.getMessage());
			}
		}

		//System.debug(JSON.serialize(rowsMap));

		return null;
	}


	public class Contadores {
		public String atributo{get;set;}
		public Integer colspan{get;Set;}

		public Contadores(String attr, Integer cont){
			this.atributo = attr;
			this.colspan = cont;
		}
	}
}