/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_Integraciones__c y toma los reg del dist 57205

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    28-Jul-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_SirenaInteracWebServiceBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String slstIdLead;
    
    //Un constructor por default
    global TAM_SirenaInteracWebServiceBch_cls(string query, String slstIdLead){
        this.query = query;
        this.slstIdLead = slstIdLead;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_SirenaInteracWebServiceBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_Integraciones__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_SirenaInteracWebServiceBch_cls...');

        TAM_CallRestWebService.datosWebService objDatosWebServiceFinal = 
            new TAM_CallRestWebService.datosWebService();

        //Recorre el mapa de mapIdVinReg 
        for (TAM_Integraciones__c objIntegraCons : scope){
            String strUrlPaso = objIntegraCons.TAM_UrlConsultaCtesDealer__c;
            if (Test.isRunningTest())
                strUrlPaso += 'prospect/60fda1356e4b97000864dab6/interactions?api-key=pFm8HEczABluOuN2YjhOyWaba2f5c75H';                        
            //Vamos a consultar todos los leads de los ultimos 5 minutos
            if (!Test.isRunningTest()){
                strUrlPaso += 'prospect/60fda1356e4b97000864dab6/interactions?api-key=pFm8HEczABluOuN2YjhOyWaba2f5c75H';                        
            }//Fin si !Test.isRunningTest()
            System.debug('EN TAM_SirenaWebService.getLeadsSirena strUrlPaso: ' + strUrlPaso);            
            //Inicializa el objeto de objIntegra
            objDatosWebServiceFinal = new TAM_CallRestWebService.datosWebService(strUrlPaso, 'GET',
                '', new Map<String, String>());        
        }

        System.debug('EN TAM_SirenaInteracWebServiceBch_cls this.slstIdLead: ' + this.slstIdLead);
        //Llama al webservice de TAM_RespCallWebService con el objeto objDatosWebServiceFinal
        TAM_RespCallWebService objRespCallWebService = TAM_SirenaWebService.getLeadsSirenaInteracciones(objDatosWebServiceFinal, this.slstIdLead);        
        
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_SirenaInteracWebServiceBch_cls.finish Hora: ' + DateTime.now());    
    } 
    
}