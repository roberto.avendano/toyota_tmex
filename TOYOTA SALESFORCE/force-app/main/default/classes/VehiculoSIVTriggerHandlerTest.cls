@isTest
public class VehiculoSIVTriggerHandlerTest {
    @isTest 
    static void test_method_one(){
        Date inicio = date.parse('01/01/2018');
        Date fin = date.parse('30/01/2018');
        
        List<VehiculoSIV__c> vinesSIV = new List<VehiculoSIV__c>();
        VehiculoSIV__c vinSIV1 = new VehiculoSIV__c(
            Name = 'SIV 1',
            NombreVehiculo__c = 'SIV 1',
            AnoModelo__c = 'MY18',
            PrecioPublico__c = 237800,
            PrecioTotalEmpleado__c = 215900,
            // Dedescuento__c = -0.0921,
            InicioVigencia__c = inicio,
            FinVigencia__c = fin,
            VehiculoDisponiblePuestos__c = 'Puesto 1;Puesto 2; Puesto 3'
        );
        VehiculoSIV__c vinSIV2 = new VehiculoSIV__c(
            Name = 'SIV 2',
            NombreVehiculo__c = 'SIV 2',
            AnoModelo__c = 'MY18',
            PrecioPublico__c = 243100,
            PrecioTotalEmpleado__c = 220700,
            //  Dedescuento__c = -0.0921,
            InicioVigencia__c = inicio,
            FinVigencia__c = fin,
            VehiculoDisponiblePuestos__c = 'Puesto 1;Puesto 2; Puesto 3;Puesto 4'
        );
        vinesSIV.add(vinSIV1);
        vinesSIV.add(vinSIV2);
        
        try{
            upsert vinesSIV;
        }catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }
}