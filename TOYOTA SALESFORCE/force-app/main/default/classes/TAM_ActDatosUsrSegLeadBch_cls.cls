/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_UsuariosSeguimientoLead__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Mayo-202          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActDatosUsrSegLeadBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActDatosUsrSegLeadBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<User> scope){
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls.');
        
        Map<String, TAM_UsuariosSeguimientoLead__c > mapUsrSegUpd = new Map<String, TAM_UsuariosSeguimientoLead__c >();
        Set<String> setIdUsr = new Set<String>();
        Set<String> setNoDist = new Set<String>();

        Map<String, String> mapCodDistNom = new Map<String, String>();
        Map<String, List<User>> mapCodDistListUser = new Map<String, List<User>>();

        Map<String, List<TAM_UsuariosSeguimientoLead__c>> mapIdUsrSfdcObj = new Map<String, List<TAM_UsuariosSeguimientoLead__c>>();
                        
        //Recorre la lista de Casos para cerrarlos 
        for (User objUsrSegLead : scope){
            if (objUsrSegLead.id != null)
                setIdUsr.add(objUsrSegLead.id);
            if (objUsrSegLead.CodigoDistribuidor__c != null)                
                setNoDist.add(objUsrSegLead.CodigoDistribuidor__c);
        }
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls setIdUsr: ' + setIdUsr);
        
        //Consulta los datos del usuario en TAM_UsuariosSeguimientoLead__c
        for (TAM_UsuariosSeguimientoLead__c objPaso : [Select id, TAM_IdExterno__c, TAM_IdUsuarioPartner__c, TAM_NoDistribuidor__c 
            From TAM_UsuariosSeguimientoLead__c Where TAM_IdUsuarioPartner__c IN : setIdUsr]){
            //Crea una llave externa con el id y el Cod del dist
            String sIdExterno = objPaso.TAM_IdUsuarioPartner__c + '-' + objPaso.TAM_NoDistribuidor__c + '-' + objPaso.TAM_IdExterno__c;
            //Ve si ya existe el idexterno en mapIdUsrSfdcObj
            if (mapIdUsrSfdcObj.containsKey(objPaso.TAM_IdUsuarioPartner__c))
                mapIdUsrSfdcObj.get(objPaso.TAM_IdUsuarioPartner__c).add(objPaso);
            if (!mapIdUsrSfdcObj.containsKey(objPaso.TAM_IdUsuarioPartner__c))            
                mapIdUsrSfdcObj.put(objPaso.TAM_IdUsuarioPartner__c, new List<TAM_UsuariosSeguimientoLead__c>{objPaso});
        } 
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls mapIdUsrSfdcObj: ' + mapIdUsrSfdcObj.keySet());
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls mapIdUsrSfdcObj: ' + mapIdUsrSfdcObj.values());
        
        //Busca a los usr asociados a noDistAct
        for (User objUser : [Select u.Name, u.IsActive, u.Id, u.Distribuidor__c, u.ContactId, 
            u.CommunityNickname, u.CodigoDistribuidor__c, u.Alias, u.AccountId, u.Email 
            From User u where u.CodigoDistribuidor__c != null and id IN: setIdUsr ]){
            System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUser: ' + objUser);
            Boolean blnExiste = false;
            //Crea el ID Externo
            String sIdExterno = objUser.CommunityNickname + '-' + objUser.CodigoDistribuidor__c;
            String sIdExtFinal = objUser.id + '-' + objUser.CodigoDistribuidor__c + '-' + sIdExterno; 
            System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUser.id: ' + objUser.id);
            //Dale vuelta a los usuarios que vienen en el mapa de mapIdUsrSfdcObj
            if (mapIdUsrSfdcObj.containsKey(objUser.id)){
	            for (TAM_UsuariosSeguimientoLead__c objUsrSegPaso : mapIdUsrSfdcObj.get(objUser.id)){ 
	                blnExiste = true;
	                String sIdExternoUsrSeg = objUsrSegPaso.TAM_IdUsuarioPartner__c + '-' + objUsrSegPaso.TAM_NoDistribuidor__c + '-' + objUsrSegPaso.TAM_IdExterno__c;
	                System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUsrSegPaso: ' + objUsrSegPaso);
	                //Crea el objeto del tipo TAM_UsuariosSeguimientoLead__c
		            TAM_UsuariosSeguimientoLead__c objUsrSegPasoUps = new TAM_UsuariosSeguimientoLead__c(
	                        TAM_IdExterno__c = sIdExterno,
	                        Name = objUser.Name,
	                        TAM_NombreDistribuidor__c = objUser.Distribuidor__c,
	                        TAM_NombreCompleto__c = objUser.Name,
	                        TAM_NoDistribuidor__c = objUser.CodigoDistribuidor__c,
	                        TAM_IdUsuarioPartner__c = objUser.id,
	                        TAM_ActivoSFDC__c = objUser.IsActive,
	                        TAM_Correo__c = objUser.Email
	                );
		            //Agrega el usuario al mapa de mapUsrSegUpd
		            if (sIdExternoUsrSeg == sIdExtFinal){
                       System.debug('EN TAM_ActDatosUsrSegLeadBch_cls sIdExtFinal1: ' + sIdExtFinal + ' sIdExternoUsrSeg: ' + sIdExternoUsrSeg);
		               mapUsrSegUpd.put(sIdExternoUsrSeg, objUsrSegPasoUps);
	                   System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUsrSegPasoUps: ' + objUsrSegPasoUps);
		            }//Fin si sIdExternoUsrSeg == sIdExtFinal
	                if (sIdExternoUsrSeg != sIdExtFinal && !objUser.IsActive){
                       System.debug('EN TAM_ActDatosUsrSegLeadBch_cls sIdExtFinal2: ' + sIdExtFinal + ' sIdExternoUsrSeg: ' + sIdExternoUsrSeg);
		               TAM_UsuariosSeguimientoLead__c objUsrSegPasoUps2 = new TAM_UsuariosSeguimientoLead__c(
		                        TAM_IdExterno__c = objUsrSegPaso.TAM_IdExterno__c,
		                        //id = objUsrSegPaso.id,
		                        TAM_ActivoSFDC__c = false
		               );                    
	                   mapUsrSegUpd.put(sIdExternoUsrSeg, objUsrSegPasoUps2);                
	                   System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUsrSegPasoUps2: ' + objUsrSegPasoUps2);
	                }//Fin del si objUsrSegPaso != sIdExternoPaso
                    if (sIdExternoUsrSeg != sIdExtFinal && objUser.IsActive){
                       System.debug('EN TAM_ActDatosUsrSegLeadBch_cls sIdExtFinal3: ' + sIdExtFinal + ' sIdExternoUsrSeg: ' + sIdExternoUsrSeg);
                       mapUsrSegUpd.put(sIdExtFinal, objUsrSegPasoUps);
                       System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUsrSegPasoUps3: ' + objUsrSegPasoUps);
                    }//Fin del si objUsrSegPaso != sIdExternoPaso
	            }//Fin del for para mapIdUsrSfdcObj.get(objUser.id)
            }//Fin si mapIdUsrSfdcObj.containsKey(objUser.id)
            //Ve si no existe es un usuario nuevo
            if (!blnExiste){
                //Crea el objeto del tipo TAM_UsuariosSeguimientoLead__c
                TAM_UsuariosSeguimientoLead__c objUsrSegPasoUps3 = new TAM_UsuariosSeguimientoLead__c(
                        TAM_IdExterno__c = sIdExterno,
                        Name = objUser.Name,
                        TAM_NombreDistribuidor__c = objUser.Distribuidor__c,
                        TAM_NombreCompleto__c = objUser.Name,
                        TAM_NoDistribuidor__c = objUser.CodigoDistribuidor__c,
                        TAM_IdUsuarioPartner__c = objUser.id,
                        TAM_ActivoSFDC__c = objUser.IsActive,
                        TAM_Correo__c = objUser.Email
                );
                System.debug('EN TAM_ActDatosUsrSegLeadBch_cls sIdExtFinal4: ' + sIdExtFinal);
                //Agregalo al mapa de mapUsrSegUpd
                mapUsrSegUpd.put(sIdExtFinal, objUsrSegPasoUps3);
                System.debug('EN TAM_ActDatosUsrSegLeadBch_cls objUsrSegPasoUps4: ' + objUsrSegPasoUps3);
            }//Fin si !blnExiste
        }//Fin del for para User
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls mapUsrSegUpd: ' + mapUsrSegUpd);

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        

        //Ve si tiene algo la lista de mapUsrSegUpd
        if (!mapUsrSegUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.UpsertResult> lDtbUpsRes = Database.upsert(mapUsrSegUpd.values(), TAM_UsuariosSeguimientoLead__c.TAM_IdExterno__c, false);
            //Ve si hubo error
            for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActDatosUsrSegLeadBch_cls Hubo un error a la hora de crear/Actualizar los registros en TAM_UsuariosSeguimientoLead__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapUsrSegUpd.isEmpty()        

        //Roleback a todo
        //Database.rollback(sp);

    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls.finish Hora: ' + DateTime.now());
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_ActDatosUsrSegLeadMinut);
        DateTime dtHoraActual = DateTime.now();
        DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //5
        String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls.finish CRON_EXP: ' + CRON_EXP);

        //Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
        TAM_ActDatosUsrSegLeadSch_cls objActDatosUsrSegLeadSch = new TAM_ActDatosUsrSegLeadSch_cls();
        System.debug('EN TAM_ActDatosUsrSegLeadBch_cls.finish objActDatosUsrSegLeadSch: ' + objActDatosUsrSegLeadSch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('TAM_ActDatosUsrSegLeadSch_cls: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objActDatosUsrSegLeadSch);
    } 
    
}