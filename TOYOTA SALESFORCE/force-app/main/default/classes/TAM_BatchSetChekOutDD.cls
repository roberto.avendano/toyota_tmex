/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento del estado de cuenta (INCENTIVOS)
******************************************************************************* */
global class TAM_BatchSetChekOutDD implements Database.Batchable<sObject>{

ConfigCheckOutDD__c paramConfig; 

global TAM_BatchSetChekOutDD() 
{
    paramConfig = [Select TAM_AnioCalToyota__c,TAM_Fleet__c,TAM_MesCalToyota__c,TAM_TipoMovimiento__c,TAM_TipoRegistro__c
                    FROM ConfigCheckOutDD__c  WHERE TAM_TipoRegistro__c = 'Pedido Especial'];
}

global Database.QueryLocator start(Database.BatchableContext BC) {
    String tipoMovimiento = paramConfig.TAM_TipoMovimiento__c;
    String  fleet = paramConfig.TAM_Fleet__c;
    String year = paramConfig.TAM_AnioCalToyota__c;
    String month = paramConfig.TAM_MesCalToyota__c;
    
    String query = 'Select id,name from Vehiculo__c where Ultimo_Movimiento__r.year__c =: year AND Ultimo_Movimiento__r.month__C =: month AND Ultimo_Movimiento__r.fleet__C =: fleet';
    return Database.getQueryLocator(query);
    
}

global void execute(Database.BatchableContext BC, List<Vehiculo__c> vinList) {
    Id recordTypePedidoEsp =  Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByDeveloperName().get('TAM_PedidoEspecial').getRecordTypeId();
    String tipoMovimiento = paramConfig.TAM_TipoMovimiento__c;
    String fleet = paramConfig.TAM_Fleet__c;
    String year = paramConfig.TAM_AnioCalToyota__c;
    String month = paramConfig.TAM_MesCalToyota__c;
    
    Set<String> listaVIN = new Set<String>();
    List<TAM_CheckOutDetalleSolicitudCompra__c> listCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
    List<TAM_CheckOutDD__c> listCheckOutDD =  new List<TAM_CheckOutDD__c>();
    
    if(!vinList.isEmpty()){
        for(Vehiculo__c vinIN : vinList){
            listaVIN.add(vinIN.name);
        }
        
    }
    
    Map<String,String[]> mapVINWithMov = new Map<String,String[]> ();
    for(Movimiento__c mov : [Select id,VIN__R.NAME,Qty__c from Movimiento__C where fleet__C =: fleet AND year__c =: year and month__c =: month AND VIN__R.NAME IN : listaVIN]){
        if(!mapVINWithMov.containsKey(mov.VIN__R.NAME)){    
            mapVINWithMov.put(mov.VIN__R.NAME, new String[]{});
        }
        mapVINWithMov.get(mov.VIN__R.NAME).add(mov.Qty__c);
    }
    
    system.debug('movs count:'+mapVINWithMov);
    
    //Se obtienen los registros del checkOut y en mapa se agregan los checkOuts que tenga relacionado el VIN
    Map<String,TAM_CheckOutDetalleSolicitudCompra__c[]> mapVINWithCheckOut = new Map<String,TAM_CheckOutDetalleSolicitudCompra__c[]> ();
    if(!listaVIN.isEmpty()){
        for(TAM_CheckOutDetalleSolicitudCompra__c chekOut : [Select id,TAM_VIN__c,TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c From TAM_CheckOutDetalleSolicitudCompra__c where TAM_VIN__c IN : listaVIN 
                        order by TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c desc]){
            if(!mapVINWithCheckOut.containsKey(chekOut.TAM_VIN__c)){    
                mapVINWithCheckOut.put(chekOut.TAM_VIN__c, new TAM_CheckOutDetalleSolicitudCompra__c[]{});
            }
            mapVINWithCheckOut.get(chekOut.TAM_VIN__c).add(chekOut);

            }
    }

    system.debug('map checkout'+mapVINWithCheckOut);
        
    //Se recorre la lista de Vehiculos entrantes para realizar las asignaciones
    for(Vehiculo__C detalleVIN : vinList){
        TAM_CheckOutDD__c registroCheckOutDD = new TAM_CheckOutDD__c ();
        registroCheckOutDD.recordTypeId = recordTypePedidoEsp;
        registroCheckOutDD.TAM_VIN__c = detalleVIN.id;
        registroCheckOutDD.TAM_AnioCalToyota__c = year;
        registroCheckOutDD.TAM_MesCalToyota__c = month;
        registroCheckOutDD.TAM_IDExterno__c = detalleVIN.id+'-'+month+'-'+year;
        //Existe en checkOut y existe en dealer daily
        if(mapVINWithCheckOut.containsKey(detalleVIN.Name)){
            List<TAM_CheckOutDetalleSolicitudCompra__c> detalleCheckout = mapVINWithCheckOut.get(detalleVIN.Name);
            registroCheckOutDD.TAM_Escenario__c = label.TAM_EstatusCheckOutDD_1;
            registroCheckOutDD.TAM_CheckOut__c = detalleCheckout[0].id;
        }else{
            //No existe en checkOut y existe en deaker daily 
            registroCheckOutDD.TAM_Escenario__c = label.TAM_EstatusCheckOutDD_2;
        }
        //Se checan Los movimientos de cada VIN 
        if(mapVINWithMov.containsKey(detalleVIN.Name)){
            List<String> movCount = mapVINWithMov.get(detalleVIN.Name);
            if(!movCount.isEmpty()){
                integer suma=0;
                for(String  mov : movCount){
                    suma += integer.valueof(mov);
                    registroCheckOutDD.TAM_CountMov__c = suma;
                }
            }
        }
        
        listCheckOutDD.add(registroCheckOutDD);
        
    } 
    
    if(!listCheckOutDD.isEmpty()){
        upsert listCheckOutDD TAM_IDExterno__c;
        
    }
    
}   

global void finish(Database.BatchableContext BC) {
    // execute any post-processing operations like sending email
}
}