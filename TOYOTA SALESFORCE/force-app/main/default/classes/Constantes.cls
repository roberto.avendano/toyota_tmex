public class Constantes {
	
	public static final String EVALUACIONES_DEALER_FIELD_DEALER_ID = 'CF00Ni000000G3V1d'; //Id del campo Dealer que se pasa al dar click en nueva evaluacion dealer desde el cliente
	public static final Set<Id> EVALUACIONES_DEALER_PROFILES_ALL_ACCESS = new Set<Id>{'00ei0000000wytDAAQ'};// Set de Ids que pueden ver todas las evaluaciones
	public static final String PROFILE_CONSULTOR_TSM = '00ei0000001RXinAAG';
	public static final String ACTIVIDAD_PLAN_INTEGRAL_REFERENCIA_TSM = 'TSM'; 
	public static final Set<Id> PROFILES_COMMUNITY_AUTOEVAL_ONLY = new Set<Id>{'00ei0000000wyteAAA','00ei0000000wytcAAA','00e63000000HjN6AAK'};//POR Contingencia se retira el id de promotor kaisen covid-19
	//public static final Set<Id> PROFILES_COMMUNITY_AUTOEVAL_ONLY = new Set<Id>{'00ei0000001JLm1AAG','00ei0000000wyteAAA','00ei0000000wytcAAA','00e63000000HjN6AAK'};// Set de Ids de perfiles de comunidad que unicamente tienen acceso a revisar autoevaluaciones 
    public static final Set<String> PROFILES_ADMIN = new Set<String>{'Administrador del sistema', 'Administrador del sistemaAD', 'System Administrator'};
	public static final String PRIME_PROFILE_ID = '00e5B000000DkpZQAS';

	public static Map<String,Map<String,RecordType>> TIPOS_REGISTRO_DEVNAME_ID{
		get{
			if(TIPOS_REGISTRO_DEVNAME_ID==null){
				TIPOS_REGISTRO_DEVNAME_ID = new Map<String,Map<String,RecordType>>();
				for(RecordType rt : [SELECT Id, DeveloperName, Name, SobjectType FROM RecordType Where IsActive=true]){
					if(!TIPOS_REGISTRO_DEVNAME_ID.containsKey(rt.SobjectType)){
						TIPOS_REGISTRO_DEVNAME_ID.put(rt.SobjectType,new Map<String,RecordType>());
					}
					TIPOS_REGISTRO_DEVNAME_ID.get(rt.SobjectType).put(rt.DeveloperName,rt);
					TIPOS_REGISTRO_DEVNAME_ID.get(rt.SobjectType).put(rt.Id,rt);
				}
			}
			return TIPOS_REGISTRO_DEVNAME_ID;
		}
	}

	public static final String EVALUACIONES_KODAWARI_DEALER_TIPOS_AUTOEVALUACION = 'Mensual';

	public static List<SelectOption> EVALUACIONES_RETENCION_TIPOS{
		get{
			EVALUACIONES_RETENCION_TIPOS = new List<SelectOption>();
			EVALUACIONES_RETENCION_TIPOS.add(new SelectOption('Pre Evaluación', 'Pre Evaluación'));
			EVALUACIONES_RETENCION_TIPOS.add(new SelectOption('Certificación', 'Certificación'));
			EVALUACIONES_RETENCION_TIPOS.add(new SelectOption('Mensual', 'Mensual'));

			return EVALUACIONES_RETENCION_TIPOS;
		}
	}
	public static String ACCOUNT_RT_MORAL='Cliente - Persona Moral';
	public static String ACCOUNT_RT_FISICA='Cliente - Persona Fisica';
	public static String CONCILIACION_RT_AUTO_DEMO='Conciliación VIN Auto demo';
	public static String CONCILIACION_RT_AUTO_POOL='Conciliación VIN Auto Pool o Asignado';

	public static String VIN_RT_MANUAL='Vehículo Manual';

	public static String CASE_RT_AUTO_DEMO='Solicitud Auto Demo';
	public static String CONTACT_RT_TMEX='TMEX';
	public static String CASE_STATUS_SOLICITUD_ALTA = 'Solicitud alta';
	public static String CASE_STATUS_ALTA = 'Alta';
	public static String CASE_STATUS_SOLICITUD_BAJA = 'Solicitud baja';
	public static String CASE_STATUS_BAJA_POR_VIGENCIA = 'Baja por vigencia';
	public static String CASE_STATUS_BAJA_POR_VIGENCIA_PLAN_PISO = 'Baja por vigencia plan piso';
	public static String CASE_STATUS_BAJA = 'Baja';
	public static String CASE_STATUS_BAJA_PAGO_COMPLETADO = 'Baja por pago completado';
	public static String CASE_STATUS_AUTORIZACION_ALTA_TMEX = 'Autorización alta TMEX';
	public static String CASE_STATUS_AUTORIZACION_BAJA_TMEX = 'Autorización baja TMEX';
	public static String CASE_STATUS_ALTA_RECHAZADA_TMEX = 'Alta rechazada por TMEX';
	public static String CASE_STATUS_ALTA_RECHAZADA_TFS = 'Alta rechazada por TFS';
	public static String CASE_STATUS_BAJA_RECHAZADA_TFS = 'Baja rechazada por TFS';


	//AUTOS DEMO ESTATUS OCTUBRE 2017	
    public static String AUTO_DEMO_BAJA_SIN_SOLICITUD = 'Baja por venta sin solicitud';
    public static String AUTO_DEMO_SOLICITUD_ALTA = 'Solicitud alta';
    public static String AUTO_DEMO_DUPLICADO = 'Duplicado';
    public static String AUTO_DEMO_SIN_SOLICITUD = 'Sin Solicitud';
    public static String AUTO_DEMO_AUTORIZACION_ALTA_TMEX = 'Autorización alta TMEX';
    public static String AUTO_DEMO_ALTA_RECHAZADA_TMEX = 'Alta rechazada por TMEX';
    public static String AUTO_DEMO_ALTA = 'Alta';
    public static String AUTO_DEMO_ALTA_RECHAZADA_TFS = 'Alta rechazada por TFS';
    public static String AUTO_DEMO_SOLICITUD_BAJA = 'Solicitud Baja';
    public static String AUTO_DEMO_BAJA_ANTICIPADA = 'Baja anticipada';
    public static String AUTO_DEMO_BAJA_PAGO_COMPLETADO_SIN_SOLICITUD = 'Baja por pago completado sin solicitud';
	public static String AUTO_DEMO_AUTORIZACION_BAJA_TMEX = 'Autorización baja TMEX';
	public static String AUTO_DEMO_PAGO_COMPLETADO_SIN_SOLICITUD = 'Pago completado sin solicitud';
	public static String AUTO_DEMO_BAJA_POR_VIGENCIA = 'Baja por vigencia';

	//Tipos de registro de Solicitud.	
	public static String SOLICITUD_RT_BAJA = 'SolicitudAutoDemoBaja';
	public static String SOLICITUD_RT_CERRADA = 'SolicitudAutoDemoCerrada';
	public static String SOLICITUD_RT_ALTA = 'Solicitud auto demo alta';
	public static String SOLICITUD_RT_NUEVA = 'Solicitud auto demo nueva';

	public static String CASE_TIPO_VIN = 'Auto demo';
	public static String ASIGNACION_PRESUPUESTO_INSERT_ERROR = 'El presupuesto asignado a distribuidores no debe exceder el presupuesto autorizado';
	
	public static String PRESUPUESTO_RT_AUTO_DEMO='Autos demo';
	public static String REPORT_ID{
		get{
			ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('ADIdInformeSolicitud');
			return pc.Valor__c;
		} set;
	}

	    public static List<String> MESES_ORDENADOS{
        get{
            if(MESES_ORDENADOS==null){
                MESES_ORDENADOS = new List<String>{
                    'Abril',
                    'Mayo',
                    'Junio',
                    'Julio',
                    'Agosto',
                    'Septiembre',
                    'Octubre',
                    'Noviembre',
                    'Diciembre',
                    'Enero',
                    'Febrero',
                    'Marzo'
                };
            }
            return MESES_ORDENADOS;
        }
        set;
    }
}