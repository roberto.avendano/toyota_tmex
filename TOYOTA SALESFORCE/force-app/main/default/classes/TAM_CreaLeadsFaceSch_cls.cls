global with sharing class TAM_CreaLeadsFaceSch_cls implements Schedulable {

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_CreaLeadsFaceSch_cls.execute...');
        String strProcesado = 'Procesado';

        this.sQuery = 'Select Id, Name ';
        this.sQuery += ' From TAM_LeadFacebook__c ';
        this.sQuery += ' Where TAM_EstatusProceso__c != \'' + String.escapeSingleQuotes(strProcesado) + '\'';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_CreaLeadsFaceSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_CreaLeadsFaceBch_cls objCreaLeadsFaceBch = new TAM_CreaLeadsFaceBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCreaLeadsFaceBch, 5);
    }
    
}