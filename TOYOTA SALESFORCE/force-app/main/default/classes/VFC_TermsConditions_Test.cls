@istest
public class VFC_TermsConditions_Test {
    static testmethod void test(){
        ParametrosConfiguracionToyota__c obj1= new ParametrosConfiguracionToyota__c (
        	name = 'redireccionaextranet',
            valor__c = 'https://tfs-toyotadealerconnect.cs9.force.com/extranet/s/',
            Consecutivo__c = 752
        );
        insert obj1;
        
        ParametrosConfiguracionToyota__c obj2= new ParametrosConfiguracionToyota__c (
        	name = 'redireccionatermsconditions',
            valor__c = '/apex/VF_TermsConditions',
            Consecutivo__c = 753
        );
        insert obj2;
        
        Id p = [select id from profile where name='Partner Community User'].id;       
        Account ac = new Account(
            name ='Grazitti',
            Codigo_Distribuidor__c = '57031');
        insert ac; 
        
        Contact con = new Contact(
            LastName ='testCon',            
            AccountId = ac.Id);
        insert con;                 
        User user = new User(
            alias = 'test123q', 
            AceptaTerminos__c=true,
            email='test123testtfs@noemail.com',
            emailencodingkey='UTF-8', 
            lastname='Testing', 
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p, 
            country='United States',
            IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', 
            username='testertfsproduc0@noemail.com');        
        insert user;
        system.runAs(user){
            VFC_TermsConditions obj = new VFC_TermsConditions();
			obj.HomePage();		
			obj.RedireccionaExtranet();
 
        }
    }
}