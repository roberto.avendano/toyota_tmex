/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS TAM_CatalogoVendedoresTriggerHandler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    11-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class TAM_CatalogoVendedoresTriggerHandler extends TriggerHandler {

    private List<TAM_CatalogoVendedores__c > itemsNew;
	private Map<ID, TAM_CatalogoVendedores__c > mapItemsOld;        
        
	//Un constructor por default
	public TAM_CatalogoVendedoresTriggerHandler() {
        this.itemsNew = (List<TAM_CatalogoVendedores__c >) Trigger.new;
        this.mapItemsOld = (Map<Id, TAM_CatalogoVendedores__c >) Trigger.oldMap;
	}

	//Sobreescribe el metodo de beforeUpdate
    public override void afterInsert() {
		System.debug('EN afterInsert....');
		creaColaboracionDistribuidor(itemsNew);    	
    }

	//Sobreescribe el metodo de beforeUpdate
    public override void afterUpdate() {
		System.debug('EN afterUpdate....');
		creaColaboracionDistribuidor(itemsNew);
    }

    public static void creaColaboracionDistribuidor(List<TAM_CatalogoVendedores__c > lstNewCatVend){
       	Set<String> setIdOwner = new Set<String>();
		Map<String, String> mapIdUsrNomDis = new Map<String, String>();
		Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
		List<TAM_CatalogoVendedores__Share>  lCatVendShare = new List<TAM_CatalogoVendedores__Share>();
       	Set<String> setIdOpp = new Set<String>();
		Map<String, String  > mapIdOppNoDist = new Map<String, String >();
		Map<String, String  > mapNoDistNomb = new Map<String, String >();
		
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler ...');           
        for(TAM_CatalogoVendedores__c  objProvision : lstNewCatVend){
        	//Ve si tiene algo el cammpo de OwnerId
			setIdOpp.add(objProvision.id);        	   
        }
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler setIdOpp: ' + setIdOpp);           

        for(TAM_CatalogoVendedores__c objProvision : [Select id, TAM_NoDistribuidor__c From TAM_CatalogoVendedores__c 
        	Where id IN :setIdOpp]){
        	//Ve si tiene algo el cammpo de OwnerId
        	if (objProvision.TAM_NoDistribuidor__c != null)
				mapIdOppNoDist.put(objProvision.id, objProvision.TAM_NoDistribuidor__c);
        }
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler mapIdOppNoDist: ' + mapIdOppNoDist.keySet());           
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler mapIdOppNoDist: ' + mapIdOppNoDist.Values());           

		for (Account dist : [Select id, Name, Codigo_Distribuidor__c From Account 
			where Codigo_Distribuidor__c IN :mapIdOppNoDist.Values()]){
			mapNoDistNomb.put(dist.Codigo_Distribuidor__c, dist.Name);
		}
                
        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group WHERE NAME LIKE '%TOYOTA%' OR NAME LIKE '%TMEX%' 
        	ORDER BY NAME]){
        	mapNomGrupoIdGrupo.put(Gropo.Name, Gropo.id);
        }
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.keySet());           
        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           
        
        //Crea la colaboracion para el reg 
        for(TAM_CatalogoVendedores__c  objCatDist : lstNewCatVend){
			String IdNomdis = mapNoDistNomb.containsKey(objCatDist.TAM_NoDistribuidor__c) ? mapNoDistNomb.get(objCatDist.TAM_NoDistribuidor__c) : ' ';			
	        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler IdNomdis: ' + IdNomdis); 
			if (Test.isRunningTest())
				IdNomdis = 'TOYOTA INNOVA FLETEROS';
			TAM_CatalogoVendedores__Share objPasoShare = new TAM_CatalogoVendedores__Share(
					ParentId = objCatDist.id,
					UserOrGroupId = mapNomGrupoIdGrupo.get(IdNomdis),
					AccessLevel = 'Read'
			);
	        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler objCatDist.ID: ' + objPasoShare); 
			//Agregalo a la lista lCatVendShare 	
	        if (mapNomGrupoIdGrupo.containsKey(IdNomdis)){
				lCatVendShare.add(objPasoShare);
		        	System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler NO EXISTE IdNomdis: ' + IdNomdis);
	        }
	        
        	String sNombreGrupo2 = mapNoDistNomb.containsKey(objCatDist.TAM_NoDistribuidor__c) ? mapNoDistNomb.get(objCatDist.TAM_NoDistribuidor__c) + ' ASESOR': ' ';			
        	System.debug('ENTRO TAM_TAM_CatalogoVendedores__c sNombreGrupo2: ' + sNombreGrupo2);
			if (Test.isRunningTest()) 
				sNombreGrupo2 = 'TOYOTA INNOVA FLETEROS';
			TAM_CatalogoVendedores__Share objPasoShare2 = new TAM_CatalogoVendedores__Share(
					ParentId = objCatDist.id,
					UserOrGroupId = mapNomGrupoIdGrupo.get(sNombreGrupo2),
					AccessLevel = 'Read'
			);
	        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c objPasoShare2: ' + objPasoShare2); 
			//Agregalo a la lista lInvenVehiculoShare
			//Ve si existe el grup en mapNomGrupoIdGrupo
	        if (mapNomGrupoIdGrupo.containsKey(sNombreGrupo2)){			 	
				lCatVendShare.add(objPasoShare2);
		        System.debug('ENTRO TAM_TAM_CatalogoVendedores__c NO EXISTE sNombreGrupo2: ' + sNombreGrupo2);
	        }
        }
        //System.debug('ENTRO TAM_TAM_CatalogoVendedores__c TriggerHandler lCatVendShare IdNomdis: ' + lCatVendShare);           
		
		if (!lCatVendShare.isEmpty()){
       		//Actualiza las Opp 
			List<Database.Saveresult> lDtbUpsRes = Database.insert(lCatVendShare, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
           			System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el precio de lista de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
           			System.debug('En la AccSh: crear/Actualizar ID: ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
		}//Fin si !lCatVendShare.isEmpty()
        	
    }
        
}