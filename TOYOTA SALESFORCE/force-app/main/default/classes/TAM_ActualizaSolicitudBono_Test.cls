@isTest
public class TAM_ActualizaSolicitudBono_Test {
    
    
    @testSetup static void setup() {
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        //Dealer de prueba
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordtypeId = recordTypeDistribuidor
        );
        insert a01;
        
        //Se crean las solicitudes de bono 
        TAM_SolicitudDeBono__c solBono01 = new TAM_SolicitudDeBono__c();
        solBono01.TAM_AnioModelo__c = '2020';
        solBono01.TAM_AprobacionFinanzas__c = 'Aprobado';
        solBono01.TAM_AprobacionVentas__c = 'Aprobado';
        solBono01.TAM_CodigoModelo__c = '1251';
        solBono01.TAM_ComentarioDealer__c = 'Test de comentario';
        solBono01.TAM_ComentarioFinanzas__c = 'Aprabado finanzas';
        solBono01.TAM_ComentarioVentas__c = 'Prueba de ventas';
        solBono01.TAM_CumpleConCondiciones__c = true;
        solBono01.TAM_Distribuidor__c = a01.id;
        solBono01.TAM_EstatusSolicitudBono__c = 'En proceso aprobación (Ventas)';
        solBono01.TAM_EstatusVIN__c = 'VIN en Inventario';
        solBono01.TAM_ExisteEdoCta__c = false;
        solBono01.TAM_Fecha_Autorizacion_DM__c = date.today();
        solBono01.TAM_Fecha_Autorizacion_DM__c = date.today();
        solBono01.TAM_Serie__c = 'PRIUS';
        solBono01.TAM_VIN__c	 = 'JTDKARFU2L3122771';
        insert solBono01;
        
        Vehiculo__c v01 = new Vehiculo__c(
            Name='JTDKARFU2L3122771',
            Id_Externo__c='JTDKARFU2L3122771'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            Sale_Date__c = date.today(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '04',
            Fleet__c = 'C'
        ); 
        insert m01;
        
        
    }
    
    @isTest static void testMethod1(){
        Test.startTest();
        TAM_ActualizaSolicitudBono sh1 = new TAM_ActualizaSolicitudBono();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test Bono', sch, sh1);  
        Test.stopTest();
    }
    
    
}