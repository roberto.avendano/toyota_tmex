@isTest
public class TestEspecificacionTriggerHandler {
    
    @testSetup static void setupData(){
        
        //Crear Serie de prueba 
        Serie__c serieTest = new Serie__c();
        serieTest.Name = 'RAV4Test';
        serieTest.Marca__c = 'Toyota';
        insert serieTest;
        
        //Crear Producto de prueba
        Product2 productModelo = new Product2();
        productModelo.RecordTypeId = '0121Y000001clPwQAI';
        productModelo.Family = 'Toyota';
        productModelo.Anio__c = '2019';
        productModelo.Serie__c = serieTest.id;
        productModelo.Name = '1402';
        productModelo.NombreVersion__c = 'Confortline';
        productModelo.IdExternoProducto__c = '18313';
        insert productModelo;   
        
        //Se crea la ficha de producto de prueba
        FichaTecnicaProducto__c fichaProdTest = new FichaTecnicaProducto__c();
        fichaProdTest.Anio__c = '1010';
        fichaProdTest.Serie__c = serieTest.id;
        fichaProdTest.Estatus__c = 'Preliminar';
        insert fichaProdTest;
        
        //Se crean las versionaes relacionadas a la ficha de tecnica de producto de prueba
        CodigoModeloFichaTecnica__c versionesFichaTecnica = new CodigoModeloFichaTecnica__c ();
        versionesFichaTecnica.CodigoModeloVersion__c = productModelo.id;
        versionesFichaTecnica.Estatus__c = 'Final';
        versionesFichaTecnica.FichaTecnica__c = fichaProdTest.Id;
        versionesFichaTecnica.Orden__c = 1; 
        insert versionesFichaTecnica;
        
        //Crear especificacion atributo
        EspecificacionAtributo__c atributosTest = new EspecificacionAtributo__c();
        atributosTest.Activo__c = true;
        atributosTest.TipoAtributo__c = 'Estatus';
        atributosTest.Categoria__c = 'Especificaciones Técnicas';
        atributosTest.Opciones__c = '1|2|3';
        atributosTest.TipoEspecificacion__c = 'Generales';
        insert atributosTest;
        
         //Crear especificacion atributo
        EspecificacionAtributo__c atributosTest2 = new EspecificacionAtributo__c();
        atributosTest2.Activo__c = true;
        atributosTest2.TipoAtributo__c = 'Motor';
        atributosTest2.Categoria__c = 'Especificaciones Técnicas';
        atributosTest2.Opciones__c = '1|2|3';
        atributosTest2.TipoEspecificacion__c = 'Generales';
        insert atributosTest2;
        
        //Agregar atributos a la ficha de producto 
        EspecificacionFichaTecnica__c atributosFicha = new EspecificacionFichaTecnica__c();
        atributosFicha.Atributo__c = 'Final';
        atributosFicha.EstatusEspecificacion__c = 'Preliminar';
        atributosFicha.FichaTecnicaProducto__c = fichaProdTest.Id;
        atributosFicha.NoAplica__c = false;
        atributosFicha.CodigoModeloFichaTecnica__c = versionesFichaTecnica.id;
        atributosFicha.EspecificacionAtributo__c = atributosTest.Id;  
        insert atributosFicha;
        
    }   
    
    @isTest
    public static void actualizarEspecificacion(){
        EspecificacionFichaTecnica__c especifacionFT = [Select id,Atributo__c,EstatusEspecificacion__c,FichaTecnicaProducto__c,CodigoModeloFichaTecnica__c,EspecificacionAtributo__c FROM EspecificacionFichaTecnica__c  WHERE Atributo__c = 'Final'];
        especifacionFT.EstatusEspecificacion__c = 'Final';
        update especifacionFT;
        
    }
    
    
}