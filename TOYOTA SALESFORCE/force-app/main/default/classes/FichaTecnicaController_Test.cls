@isTest
private class FichaTecnicaController_Test {	
    private static final Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
    public static  List<id> idFichaEsp = new List<id>();
    
    private static FichaTecnicaProducto__c getMyFTP(){
        List<Product2> newProducts = new List<Product2>();
        List<FichaColores__c> newFichasColores = new List<FichaColores__c>();
        List<Versiones_Ficha_Colores__c> versionesColores = new List<Versiones_Ficha_Colores__c>();
        List<Color_version__c> coloresVersion = new List<Color_version__c>();
        List<Producto_Version__c> prodsVersion = new List<Producto_Version__c>();
        List<ColorExterno__c> coloresExtList = new List<ColorExterno__c>();
        
        
        List<EspecificacionAtributo__c> especificacionAttrList = new List<EspecificacionAtributo__c>();
        
        
        
        Serie__c serieTest1 = new Serie__c(
            Name = 'Test Serie 1',
            Id_Externo_Serie__c = 'SerieTest1'
        );
        
        insert serieTest1;
        
        for(Integer i = 0; i < 5; i++){
            newProducts.add(new Product2(
                Name = 'TestProduct'+i,
                ProductCode = 'TestProduct'+i,
                family = 'Toyota',
                anio__c = '2019',
                IsActive = true,
                NombreVersion__c = 'PROD'+i,
                IdExternoProducto__c = 'TESTPROD'+i,
                Serie__c = serieTest1.Id,
                RecordTypeId = FichaTecnicaController_Test.recordTypesMap.get('Product2').get('Unidad') != null ? FichaTecnicaController_Test.recordTypesMap.get('Product2').get('Unidad').Id : null
            ));
        }
        
        insert newProducts;
        
        List<EspecificacionFichaTecnica__c> newEFTs = new List<EspecificacionFichaTecnica__c>();
        FichaTecnicaProducto__c ftp = new FichaTecnicaProducto__c(
            Serie__c = serieTest1.Id,
            Anio__c = '2019'
        );
        insert ftp;
        
        FichaColores__c fichaColores1 = new FichaColores__c(
            FichaTcnicaProducto__c = ftp.Id,
            Activo__c = true
        );
        
        FichaColores__c fichaColores2 = new FichaColores__c(
            FichaTcnicaProducto__c = ftp.Id, 
            Activo__c = false
        );
        
        newFichasColores.add(fichaColores1);
        newFichasColores.add(fichaColores2);
        
        insert newFichasColores;
        
        //Crear acabado interior.
        AcabadoInterior__c acabadoInt = new AcabadoInterior__c(
            Name = 'AZTECA TRIM',
            Codigo_de_Acabado__c = 'AZ'
        );
        
        insert acabadoInt;
        
        //Crear color interior
        ColorInterno__c colorInt1 = new ColorInterno__c(
            Name = 'MID BLUE BLACK',
            descripcion__C = 'MID BLUE BLACK',
            CodigoColor__c = '71',
            Orden__c = 1
        );
        
        insert colorInt1;
        
        //Crear colores exteriores
        ColorExterno__c colorExt1 = new ColorExterno__c(
            Name = '(BRONCE) GRAY METALLIC',
            descripcion__c = 'MID BLUE BLACK',
            CodigoColor__c = '011'
        );
        
        ColorExterno__c colorExt2 = new ColorExterno__c(
            Name = 'WHITE',
            descripcion__c = 'MID BLUE BLACK',
            CodigoColor__c = '021'
        );
        
        coloresExtList.add(colorExt1);
        coloresExtList.add(colorExt2);
        
        insert coloresExtList;
        
        for(FichaColores__c fc: newFichasColores){
            for(Integer i = 0; i < 2; i++){
                versionesColores.add(new Versiones_Ficha_Colores__c(
                    Ficha_Colores__c = fc.Id,
                    Orden__c = i,
                    AcabadoInterior__c = acabadoInt.Id
                ));
            }
        }
        
        insert versionesColores;
        
        for(Integer i = 0; i < versionesColores.size(); i++){        	
            coloresVersion.add(new Color_version__c(
                Version__c = versionesColores.get(i).Id,
                ColorInterior__c = colorInt1.Id,
                Orden__c = i
            ));
            
            FichaColoresController.agregaProductoVersion(
                newProducts.get(FichaTecnicaController_Test.random(newProducts.size())).Id,
                versionesColores.get(i).Id
            );
            prodsVersion.add(new Producto_Version__c(
                Version__c = versionesColores.get(i).Id,
                Producto__c = newProducts.get(FichaTecnicaController_Test.random(newProducts.size())).Id,
                Orden__c = i
            ));
        }
        
        insert coloresVersion;
        insert prodsVersion;
        
        for(FichaColores__c fc: newFichasColores){
            if(fc.Activo__c){
                Map<Id, Map<Id, Color_Externo_Seleccionado__c>> mapaColores = FichaColoresController.getColoresSeleccionados(fc.Id);
                
                for(String colorIntID : mapaColores.keySet()){
                    for(Color_Externo_Seleccionado__c ces: mapaColores.get(colorIntID).values()){
                        ces.Agregado__c = true;
                        FichaColoresController.guardarColor(ces);
                    }
                }
                
                break;
            }        	
        }	
        
        for(Integer i = 0; i < 10; i++){
            especificacionAttrList.add(new EspecificacionAtributo__c(
                Categoria__c =  'Especificaciones Técnicas',
                TipoEspecificacion__c = 'Generales',
                TipoAtributo__c = 'Test Attribute'+i,
                Orden__c = i,
                Activo__c = true,
                Opciones__c = '|FWD|RWD|AWD on demand|2WD (FWD)|4WD Full time|4WD Part time'
            ));
        }
        
        insert especificacionAttrList;
        
        List<CodigoModeloFichaTecnica__c> versiones = FichaTecnicaController.getSeries(ftp.Id);
        
        if(versiones.size() > 0){
            for(CodigoModeloFichaTecnica__c cm: versiones){
                if(cm.Id == null){
                    FichaTecnicaController.insertSerie(cm);
                } 
            }				
            
            for(CodigoModeloFichaTecnica__c cm: FichaTecnicaController.getFTProducto(ftp.Id).ModelosFichaProducto__r){
                Integer randomNum = FichaTecnicaController_Test.random(especificacionAttrList.size());
                
                newEFTs.add(new EspecificacionFichaTecnica__c(
                    FichaTecnicaProducto__c = ftp.Id,
                    Atributo__c = 'Tanque',
                    EspecificacionAtributo__c = especificacionAttrList.get(randomNum).Id,
                    EstatusEspecificacion__c = 'Preliminar',
                    CodigoModeloFichaTecnica__c = cm.Id,
                    NoAplica__c = false
                ));
            }
        }
        
        insert newEFTs;
        for(EspecificacionFichaTecnica__c espfc : newEFTs){
            idFichaEsp.add(espfc.id);
            
            
        }
        
        MergeCellsProductTabPDFController mergeCells = new MergeCellsProductTabPDFController();
        mergeCells.atributos = newEFTs;
        mergeCells.getObtenerMapa();
        return ftp;
        
        
    }
    
    
    
    
    @isTest static void test_method_one() {
        FichaTecnicaProducto__c ftp = FichaTecnicaController_Test.getMyFTP();
        FichaTecnicaController.setFTSU(ftp.Id,'Prueba Producto');
        String modelosNameConcat = '';
        
        ApexPages.currentPage().getParameters().put('Id', ftp.Id);
        ApexPages.currentPage().getParameters().put('f', 'xls');
        
        FichaTecnicaController myFTPController = new FichaTecnicaController();
        FichaColoresController myFTPColores = new FichaColoresController(myFTPController);
        FichaColoresController.guardaColoresAgregados('01,', ftp.Id);
        FichaColoresController.getColoresAgregados(ftp.Id);
        myFTPColores.actualizaColoresSeleccionados();
        FichaColoresController.getModelosFichaProducto(ftp.Id);
        myFTPController.getCurrentDate();
        FichaTecnicaController.getPrimeProfile();
        FichaTecnicaController.getCFGPersonalizada();
        FichaTecnicaController.obtieneSerie(ftp.id);
        FichaTecnicaController.activeFTSU('test','ftp'); 
        Id fichaCloneID = FichaTecnicaController.clonarFicha(ftp.Id);
        
        ftp = FichaTecnicaController.getFTProducto(ftp.Id);
        List<String> modelosName = new List<String>();
        List<String> modelosId = new List<String>();
        
        for(CodigoModeloFichaTecnica__c cm: ftp.ModelosFichaProducto__r){
            modelosName.add(cm.Name);
            if(cm.Id != null){
                FichaTecnicaController.deleteSerie(cm);
                break;
            }
        }
        
        for(String m: modelosName){
            modelosNameConcat+= m + ',';
        }
        
        List<EspecificacionFichaTecnica__c> eftf = FichaTecnicaController.getEFTFila(new List<Id>{ftp.Id});
        List<EspecificacionFichaTecnica__c> listFtp = new  List<EspecificacionFichaTecnica__c> ();
        EspecificacionFichaTecnica__c espFT = [Select id,Atributo__c,SubgrupoEspecificacion__c,TipoAtributo__c,FichaTecnicaProducto__c,EspecificacionAtributo__c,EstatusEspecificacion__c,CodigoModeloFichaTecnica__c,NoAplica__c FROM EspecificacionFichaTecnica__c WHERE ID IN : idFichaEsp limit 1];
        listFtp.add(espFT);
        FichaTecnicaController.actualizaEFTs(listFtp);
        
        List<EspecificacionFichaTecnica__c> eftList = FichaTecnicaController.getEFT(ftp.Id);
        for(EspecificacionFichaTecnica__c eft: eftList){
            eft.EstatusEspecificacion__c = 'Final';
        }
        
        update eftList;
        
        FichaTecnicaController.saveFTSU(ftp.Id, new Map<String,Boolean>());
        ftp = FichaTecnicaController.getFTProducto(ftp.Id);
        FichaTecnicaController.saveSeriesOrder(ftp.ModelosFichaProducto__r);
        FichaTecnicaController.saveFTP(ftp.Id, modelosNameConcat);
        ftp.Estatus__c = 'Final';
        update ftp;
        FichaTecnicaController.getSeriesComparative(ftp.Id);
        List<String> versionList = new List<String>();
        versionList.add('limited');
        versionList.add('sport');
        FichaTecnicaController.getEFTByVersions(versionList);
    }
    
    @isTest static void test_method_two() {
        FichaTecnicaProducto__c ftp = FichaTecnicaController_Test.getMyFTP();
        FichaTecnicaController.savePdfSFDC(ftp.id);
        FichaTecnicaController.saveXLSFDC(ftp.id);
    }
    
    @isTest static void test_method_three() {
        FichaTecnicaProducto__c ftp = FichaTecnicaController_Test.getMyFTP();
        FichaTecnicaController.getTablasFicha(ftp.id);
        
    }   
    public static Integer random(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }
}