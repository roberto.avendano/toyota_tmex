/**
    Descripción General: Clase de prueba para el módulo de Bonos.
    ________________________________________________________________
    Autor               Fecha               Descripción
    ________________________________________________________________
    Cecilia Cruz        01/Junio/2020         Versión Inicial
    ________________________________________________________________
**/
@isTest(SeeAllData=false)
public class TAM_BonosTestClass {
    
    @testSetup static void setup(){
        
        /*Catálogo Centralizado de Modelos*/
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = new List<CatalogoCentralizadoModelos__c>();
        //Avanza 2020
        CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
        objCatalogo.Name = 'AVANZA-2205-2020-1E7-10-LE MT';
        objCatalogo.Serie__c = 'AVANZA';
        objCatalogo.AnioModelo__c = '2020';
        objCatalogo.Modelo__c = '2205';
        objCatalogo.Version__c = 'LE MT';
        lstCatalogoCentralizado.add(objCatalogo);
        //Avanza 2019
        CatalogoCentralizadoModelos__c objCatalogo2 = new CatalogoCentralizadoModelos__c();
        objCatalogo2.Name = 'AVANZA-2205-2019-1E7-10-LE MT';
        objCatalogo2.Serie__c = 'AVANZA';
        objCatalogo2.AnioModelo__c = '2019';
        objCatalogo2.Modelo__c = '2205';
        objCatalogo2.Version__c = 'LE MT';
        lstCatalogoCentralizado.add(objCatalogo2);
        insert lstCatalogoCentralizado;
        
        /*Inventario G*/
        List<InventarioPiso__c> lstInventario = new List<InventarioPiso__c>();
        //Avanza 2020
        InventarioPiso__c objInventario = new InventarioPiso__c();
        objInventario.Name = 'Avanza-2205-2020-1E7-2020-04-05';
        objInventario.FechaInventario__c = Date.today();
        objInventario.Modelo__c = '2205';
        objInventario.AnioModelo__c = '2020';
        objInventario.Serie__c = 'Avanza';
        objInventario.Version__c = 'LE MT';
        objInventario.Cantidad__c = 73;
        objInventario.DiasVenta__c = 19;
        lstInventario.add(objInventario);
        //Avanza 2019
        InventarioPiso__c objInventario2 = new InventarioPiso__c();
        objInventario2.Name = 'Avanza-2205-2019-1E7-2020-04-05';
        objInventario2.FechaInventario__c = Date.today();
        objInventario2.Modelo__c = '2205';
        objInventario2.AnioModelo__c = '2019';
        objInventario2.Serie__c = 'Avanza';
        objInventario2.Version__c = 'LE MT';
        objInventario2.Cantidad__c = 45;
        objInventario2.DiasVenta__c = 30;
        lstInventario.add(objInventario2);
        insert lstInventario;
    }
    
    /*Creación de Bono*/
    @isTest
    public static void newBono(){
        
        test.startTest();
        
        //Obtener información
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Name,Serie__c,AnioModelo__c,Modelo__c,Version__c
                                                                        FROM	CatalogoCentralizadoModelos__c];
        List<InventarioPiso__c> lstInventario = [SELECT Name, FechaInventario__c,Modelo__c,AnioModelo__c,Serie__c,Version__c,Cantidad__c,DiasVenta__c
                                                 FROM	InventarioPiso__c];
        
        List<TAM_Bonos__c> lstBonos = new List<TAM_Bonos__c>();
        for(integer i=1; i<=10; i++){
            TAM_Bonos__c objBono = new TAM_Bonos__c();
            objBono.Name = 'Bono Prueba ' + i;
            objBono.TAM_InicioVigencia__c = Date.today();
            objBono.TAM_FinVigencia__c = Date.today();
            lstBonos.add(objBono);
        }
        insert lstBonos;
        
        Date datToday = Date.today();
        List<TAM_Bonos__c> lstBonosInsertados = [SELECT Id FROM TAM_Bonos__c WHERE TAM_InicioVigencia__c =: datToday];
        System.assertEquals(10, lstBonosInsertados.size());
        
        String idBonoPrueba = lstBonosInsertados.get(0).Id;
        List<TAM_DetalleBonos__c> lstDetallesInsertados = [SELECT Id FROM TAM_DetalleBonos__c WHERE Bono__c =:idBonoPrueba];
        System.assertEquals(2, lstDetallesInsertados.size());
        test.stopTest();
    }
    
    /*Obtener lista de Series*/
    @isTest
    public static void getSeries(){
        //Obtener información
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Name,Serie__c,AnioModelo__c,Modelo__c,Version__c
                                                                        FROM	CatalogoCentralizadoModelos__c];
        TAM_Bonos__c objBono = new TAM_Bonos__c();
        objBono.Name = 'Bono Prueba ';
        objBono.TAM_InicioVigencia__c = Date.today();
        objBono.TAM_FinVigencia__c = Date.today();
        insert objBono;
        
        test.startTest();
        	TAM_BonoControllerClass.getSeries(objBono.Id);
        test.stopTest();
        TAM_DetalleBonos__c objDetalle = [SELECT TAM_Serie__c FROM TAM_DetalleBonos__c WHERE TAM_Clave__c = '2205' LIMIT 1];
        System.assertEquals('AVANZA', objDetalle.TAM_Serie__c);
    }
    
    /*Obtener información de detalle de bonos*/
    @isTest
    public static void getDetalleBonos(){
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Name,Serie__c,AnioModelo__c,Modelo__c,Version__c
                                                                        FROM	CatalogoCentralizadoModelos__c];
        List<InventarioPiso__c> lstInventario = [SELECT Name, FechaInventario__c,Modelo__c,AnioModelo__c,Serie__c,Version__c,Cantidad__c,DiasVenta__c
                                                 FROM	InventarioPiso__c];

        TAM_Bonos__c objBono = new TAM_Bonos__c();
        objBono.Name = 'Bono Prueba';
        objBono.TAM_InicioVigencia__c = Date.today();
        objBono.TAM_FinVigencia__c = Date.today();
        insert objBono;

        test.startTest();
        TAM_BonoControllerClass.getDetalleBonos(objBono.Id);
        test.stopTest();
        
        String idBono = objBono.Id;
        TAM_DetalleBonos__c objDetalle = [SELECT Id, Bono__c FROM TAM_DetalleBonos__c LIMIT 1];
        System.assertEquals(idBono, objDetalle.Bono__c);
    }
    
    /*Guardado de Bono*/
    @isTest
    public static void updateBonoDetalle(){
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT 	Name,Serie__c,AnioModelo__c,Modelo__c,Version__c
                                                                        FROM	CatalogoCentralizadoModelos__c];
        List<InventarioPiso__c> lstInventario = [SELECT Name, FechaInventario__c,Modelo__c,AnioModelo__c,Serie__c,Version__c,Cantidad__c,DiasVenta__c
                                                 FROM	InventarioPiso__c];

        TAM_Bonos__c objBono = new TAM_Bonos__c();
        objBono.Name = 'Bono Prueba';
        objBono.TAM_InicioVigencia__c = Date.today();
        objBono.TAM_FinVigencia__c = Date.today();
        insert objBono;
        
        //Llenado de Detalle Bono
        String idBono = objBono.Id;
        List<TAM_DetalleBonos__c> lstDetalleBono = [SELECT 	
                                                            TAM_AnioModelo__c,
                                                            TAM_BalanceOut__c,
                                                            Bono__c,
                                                    		TAM_BonoDealerEfectivo__c,
                                                    		TAM_BonoDealerProdFinanciero__c,
                                                    		TAM_BonoTFSEfectivo__c,
                                                    		TAM_BonoTFSProdFinanciero__c,
                                                    		TAM_BonoTMEXEfectivo__c,
                                                    		TAM_BonoTMEXProdFinanciero__c,
                                                    		TAM_BonoTotalProductoFinanciero__c,
                                                    		TAM_BonoTotalEfectivo__c,
                                                    		TAM_Clave__c,
                                                    		TAM_ComentariosEfectivo__c,
                                                    		TAM_CondicionesBonoEfectivo__c,
                                                    		TAM_CondicionesProdFinanciero__c,
                                                    		TAM_CostoTMEX__c,	
                                                    		TAM_DescripcionProdFinanciero__c,
                                                    		TAM_DiasVenta__c,
                                                    		TAM_IdExterno__c,
                                                    		TAM_InventarioDealer__c,
                                                    		Name,
                                                    		TAM_ProductoFinanciero__c,
                                                    		TAM_Serie__c,
                                                    		TAM_VentasMY__c,
                                                    		TAM_AplicaAmbosBonos__c
                                                    FROM	TAM_DetalleBonos__c
                                                    WHERE	Bono__c =:idBono];
        test.startTest();
        	TAM_BonoControllerClass.guardarDetalleBonos(lstDetalleBono, idBono);
        test.stopTest();
        
        TAM_DetalleBonos__c objDetalle = [SELECT Id, Bono__c FROM TAM_DetalleBonos__c LIMIT 1];
        System.assertEquals(idBono, objDetalle.Bono__c);
    }

}