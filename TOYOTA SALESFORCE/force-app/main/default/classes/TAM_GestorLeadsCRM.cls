public without sharing class TAM_GestorLeadsCRM {

    @AuraEnabled
    public static Account getGestorIdByAccount (String userId){
        //Se obtiene el código de dealer relacionado al Usuario en sesión
        Account [] acct;
        User[] usr;
        usr = [Select id,Profile.Name,Name,CodigoDistribuidor__c From User where id =: userId];   
        
        //Se obtiene el Id del Gestor de Lead Asociado a la cuenta
        if(usr.size() > 0){
            acct = [Select id,TAM_GestorLeads__c,TAM_GestorLeadsTFS__c 
            From Account WHERE recordtype.name = 'Distribuidor' AND Codigo_Distribuidor__c =: usr[0].CodigoDistribuidor__c];
        }
        
        if(acct.size() > 0){
            return acct[0];            
        }else{
            return null;
        }
    }

    //Obtener getDatosCandidato.
    @AuraEnabled
    public static User getUsuarioRedirect(String noDistAct) {
        System.debug('En getUsuarioRedirect.TAM_GestorLeadsCRM..');
        User objUsuarioPaso = new User();
        
        for (User usuaruiCons: [Select id, TAM_RedireccionaLeads__c, TAM_ReasignacionMasivaLeads__c,
            TAM_ColaborarInventario__c From User Where id =:UserInfo.getUserId()]){
            objUsuarioPaso = usuaruiCons;
        }

        System.debug('En getUsuarioRedirect.TAM_GestorLeadsCRM objUsuarioPaso: ' + objUsuarioPaso);        
        return objUsuarioPaso;
    }
}