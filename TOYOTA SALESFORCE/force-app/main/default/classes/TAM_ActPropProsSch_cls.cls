/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto Lead
                        y actueliza el nombre del propietario TAM_ActPropProsSch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    19-Feb-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActPropProsSch_cls implements Schedulable{
    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActPropProsSch_cls.execute...');
        String strIdLeadPrueba = 'JTDKDTB34M1141773';

        this.sQuery = 'Select Id, Owner.Name, TAM_PropietarioAnterior__c, TAM_PropietarioAnterior__r.Name, ';
        this.sQuery += ' TAM_NuevoPropietario__c, FWY_codigo_distribuidor__c From Lead ';
        this.sQuery += ' where TAM_NomPropKban__c = null OR TAM_CambioPropietario__c = true';
        this.sQuery += ' OR TAM_NuevoPropietario__c != null ';

        //this.sQuery += ' And Id = \'' + String.escapeSingleQuotes(strIdLeadPrueba) + '\'';
        //this.sQuery += ' Limit 5';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActPropProsSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActPropProsBch_cls objActPropProsBch = new TAM_ActPropProsBch_cls(sQuery);
        
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActPropProsBch, 25);
                     
    }
    
}