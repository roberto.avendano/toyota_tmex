public with sharing class ReporteResultadoEvaluacionController {

    public Evaluaciones_Dealer__c evalDealer{get; set;}
    public Map<Id,ResumenRespuestas> resumenRespuestas{get; set;}
    public Map<Id,String> mapSecciones{get; set;}
    public Map<String,Integer> mapEstatusAPIs{get; set;}
    public String maxAxis{get;set;}
    public Map<Integer, String> mapaEtiquetasChart{get;set;}
    public Set<Integer> indicesEtiquetas{get;set;}
    public Integer totalSections{get;set;}
    public Integer anchoDivChart{get;set;}
    public boolean isEvRetencion{get;set;}
    public boolean isEvKodawari{get;set;}
    public boolean isEmail{get;set;}
    public Integer totalsi{get;set;}
	public Integer totalno{get;set;}

    public List<Respuestas_Preguntas_TSM__c> respuestasPreguntasTSM;
    public List<Actividad_Plan_Integral__c> actividadPlanIntegral{get;set;}

    public String tituloEvaluacion{get;set;}
    public String tipoReporteEvaluacion{get;set;}

    public String tipoRender{get;set;}

    public String logoFormato{ 
        get{
            logoFormato = '';
            tipoReporteEvaluacion ='TSM';
            List<Document> items = [SELECT Id, Name, DeveloperName from Document WHERE DeveloperName='Logo_TSM' limit 1];
            if(items.size()>0){
                logoFormato = items[0].Id;    
            }
            if(evalDealer.RecordType.DeveloperName=='EvaluacionRetencionClientes'){
                items = [SELECT Id, Name, DeveloperName from Document WHERE DeveloperName='logo_Retencion' limit 1];
                if(items.size()>0){
                    logoFormato = items[0].Id;    
                }
                tituloEvaluacion = 'Certificación Retención de Clientes';
            }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_EDER'){
                tituloEvaluacion = 'Check list EDER';
                tipoReporteEvaluacion ='Servicio';
            }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_SSC'){
                tituloEvaluacion = 'Check list SSC';
                tipoReporteEvaluacion ='Servicio';
            }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_OperacionServicio'){
                tituloEvaluacion = 'Check list Operación de Servicio';
                tipoReporteEvaluacion ='Servicio';
            }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_BodyPaint'){
                tituloEvaluacion = 'Check list Body & Paint';
                tipoReporteEvaluacion ='Servicio';
            }else{
                tituloEvaluacion = 'Certificación TSM';
            }
            return logoFormato;
        }
        set;
    }

    public ReporteResultadoEvaluacionController(){}

    public ReporteResultadoEvaluacionController(ApexPages.StandardController controller) {
        isEvRetencion=false;
        isEvKodawari=false;
        resumenRespuestas = new Map<Id,ResumenRespuestas>();
        mapSecciones = new Map<Id,String>();
        mapEstatusAPIs = new Map<String,Integer>();
        
        evalDealer = (Evaluaciones_Dealer__c)controller.getRecord();
        if(evalDealer!=null && evalDealer.Id!=null){
            evalDealer = obtenerEvaluacion(evalDealer.Id);
            respuestasPreguntasTSM = getRespuestasPreguntas(evalDealer.Id);
            actividadPlanIntegral = getActividadesPlanIntegral(evalDealer.Id);

            mapeaInformacion(evalDealer);
            getTotalSiNo();
        }
        
        if(evalDealer.RecordType.DeveloperName=='EvaluacionRetencionClientes'){
            isEvRetencion=true;
        }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_Kodawari'){
            isEvKodawari=true;
        }

        if(ApexPages.currentPage().getParameters().get('debug')!=null){
            tipoRender = 'html';
        }else{
            tipoRender = 'pdf';
        }

    }
    
    public Evaluaciones_Dealer__c getObjEvaluaciones() {
        isEvRetencion=false;
        isEvKodawari=false;
        evalDealer = obtenerEvaluacion(evalDealer.Id);
        respuestasPreguntasTSM = getRespuestasPreguntas(evalDealer.Id);
        actividadPlanIntegral = getActividadesPlanIntegral(evalDealer.Id);

        mapeaInformacion(evalDealer);
        getTotalSiNo();
        if(evalDealer.RecordType.DeveloperName=='EvaluacionRetencionClientes'){
            isEvRetencion=true;
        }else if(evalDealer.RecordType.DeveloperName=='Evaluacion_Kodawari'){
            isEvKodawari=true;
        }
        return evalDealer;
    }

    public List<Respuestas_Preguntas_TSM__c> getRespuestasPreguntas(String idEval){
        return [select Id, Name, Respuesta_Pregunta__c, Total_de_Objetos_Sin_Contestar__c,
                    Pregunta_Relacionada__r.Name, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,
                    Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name, Evaluacion_Dealer__c
                from Respuestas_Preguntas_TSM__c
                WHERE Evaluacion_Dealer__c =:idEval
                ORDER BY Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name, Pregunta_Relacionada__r.Orden_de_Aparicion__c];
    }

    public List<Actividad_Plan_Integral__c> getActividadesPlanIntegral(String idEval){
        return [select Estatus__c, RespuestasObjetosTSM__r.Objeto_Evaluacion_Relacionado__r.Metodos_de_Evaluacion__c, 
                RespuestasObjetosTSM__r.Pregunta_Relacionada_del__r.Reactivo__c, Condicion_Observada__c, Actividad_Mejora__c, 
                Fecha_Compromiso__c, Semaforo__c, EvaluacionDealer__c 
                from Actividad_Plan_Integral__c WHERE EvaluacionDealer__c =:idEval];
    }



    public void mapeaInformacion(Evaluaciones_Dealer__c myNewEval){
        resumenRespuestas = new Map<Id,ResumenRespuestas>();
        mapSecciones = new Map<Id,String>();
        mapEstatusAPIs = new Map<String,Integer>();

        if(respuestasPreguntasTSM!=null && respuestasPreguntasTSM.size()>0){
            
            for(Respuestas_Preguntas_TSM__c rp :respuestasPreguntasTSM){
             if(rp.Respuesta_Pregunta__c != null && myNewEval.RecordType.name == 'Evaluación Kodawari' ){	
                if(!resumenRespuestas.containsKey(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
                   system.debug('seccion'+rp.Respuesta_Pregunta__c);
                    resumenRespuestas.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, new ResumenRespuestas(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c));
                    mapSecciones.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name);
               	 }
               }else 
                 if(myNewEval.RecordType.name != 'Evaluación Kodawari' ){	
                   if(!resumenRespuestas.containsKey(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
                   system.debug('seccion'+rp.Respuesta_Pregunta__c);
                    resumenRespuestas.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, new ResumenRespuestas(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c));
                    mapSecciones.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name);
               	 }
               }               
               if(rp.Respuesta_Pregunta__c=='Si'){
                    (resumenRespuestas.get(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)).sumaSi();
               }else if(rp.Respuesta_Pregunta__c=='No'){
                    (resumenRespuestas.get(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)).sumaNo();
                }
            }
        }
    
       if(actividadPlanIntegral!=null && actividadPlanIntegral.size()>0){
            for(Actividad_Plan_Integral__c api : actividadPlanIntegral){
                if(!mapEstatusAPIs.containsKey(api.Estatus__c)){
                    mapEstatusAPIs.put(api.Estatus__c,0);
                }
                mapEstatusAPIs.put(api.Estatus__c,mapEstatusAPIs.get(api.Estatus__c)+1);
            }
        }
		
    }

    
    public Evaluaciones_Dealer__c obtenerEvaluacion(String idEval){        
        return [SELECT 
                ResultadoEvaluacion__c,
                Total_de_Respuestas_Cumplidas__c, Total_de_Respuestas_No_Cumplidas__c, Preguntas_Sin_Contestar__c, AvanceEvaluacion__c,
                CreatedDate, Evaluacion_Cerrada__c, Tipo_de_Evaluacion_Kodawari__c,
                Consultor_TSM_Titular__c, Id, Name, Nombre_Dealer__c, RecordTypeId,
                RecordType.Name, RecordType.DeveloperName,
                Nombre_Dealer__r.Id, Nombre_Dealer__r.Name,
                Consultor_TSM_Titular__r.Id, Consultor_TSM_Titular__r.Name,
                (select Id, Name, 
                        Usuario__r.Id, Usuario__r.Name, 
                        Seccion_Toyota_Mexico__r.Id, Seccion_Toyota_Mexico__r.Name 
                    from Relaciones_Seccion_Consultor__r)
                FROM Evaluaciones_Dealer__c WHERE Id =: idEval];
    }

    public List<ResumenRespuestas> getResumenRespuestasList(){
        return resumenRespuestas.values();
    }
    
    public String getValoresBarras2(){        
        String vectorSi = '';
        String vectorNo = '';
        Decimal totalRespuestas=0;
        Decimal cumplidos;
        Decimal noCumplidos;

        for(Id idsMapa: resumenRespuestas.keySet()){                                
            ResumenRespuestas rr= resumenRespuestas.get(idsMapa);
            totalRespuestas = rr.si + rr.no;
            cumplidos = rr.si / totalRespuestas;
            cumplidos.setScale(2);            
            vectorSi += cumplidos + ',';
            noCumplidos = rr.no/ totalRespuestas;
            noCumplidos.setScale(2);            
            vectorNo += noCumplidos + ',';            
        }
            
        vectorSi = vectorSi.left(vectorSi.length()-1);
        vectorNo = vectorNo.left(vectorNo.length()-1);
        //return 'chd=t:'+vectorSi+'|'+vectorNo +'&chxl=0:|'+barLabels;
        anchoDivChart = resumenRespuestas.size() >=10 ? 1000: resumenRespuestas.size()*100;
        totalSections = resumenRespuestas.size() <=28 ? 20 : ((896/resumenRespuestas.size())/2)-2;      
        return EncodingUtil.urlEncode(vectorSi+'|'+vectorNo, 'UTF-8');
    }

    public String getBarLabels(){
        String barLabels = '';
        Integer label = 1;
        for(Id idsMapa: resumenRespuestas.keySet()){ 
            barLabels += label + '|';
            label++;
        }

        barLabels = barLabels.left(barLabels.length()-1);
        return EncodingUtil.urlEncode(barLabels, 'UTF-8'); 
    }

    public String getValoresBarras(){
        System.debug(JSON.serialize(resumenRespuestas));
        String cadenaRetorno='';
        String si='';
        String no='';
        List<Integer> maxSi= new List<Integer>();
        List<Integer> maxNo= new List<Integer>();
        for(Id idsMapa: resumenRespuestas.keySet()){
            ResumenRespuestas rr= resumenRespuestas.get(idsMapa);
            si = si+ rr.si +',';
            no = no+ rr.no +',';
            maxSi.add(rr.si);
            maxNo.add(rr.no);
        }
        maxSi.sort();
        maxNo.sort();
        
        if(maxSi[maxSi.size()-1]==maxNo[maxNo.size()-1]){
            maxAxis = String.valueOf(maxSi[maxSi.size()-1]);
        }else{
            if(maxSi[maxSi.size()-1]>maxNo[maxNo.size()-1]){
                maxAxis = String.valueOf(maxSi[maxSi.size()-1]);
            }else{
                maxAxis = String.valueOf(maxNo[maxNo.size()-1]);
            }
        }
        
        
        
        si = si.left(si.length()-1);
        no = no.left(no.length()-1);
        cadenaRetorno = si + '|' +no;
        return EncodingUtil.urlEncode('t:'+cadenaRetorno, 'UTF-8');
    }
    
    public String getColoresBarras(){
        String color1='2F75B5';
        String color2='C00000';
        String cadenaRetorno= color1+','+color2;
        return EncodingUtil.urlEncode(cadenaRetorno, 'UTF-8');
    }

    public Set<Integer> getEtiquetasBarras(){
        mapaEtiquetasChart= new Map<Integer, String>();
        indicesEtiquetas = new Set<Integer>();
        Integer index= 0;
        for(Id idsMapa: resumenRespuestas.keySet()){
            ResumenRespuestas rr= resumenRespuestas.get(idsMapa);
            mapaEtiquetasChart.put(index, mapSecciones.get(rr.seccion));            
            index++;
        }
        indicesEtiquetas = mapaEtiquetasChart.keySet();
        if(indicesEtiquetas.size()<=28){totalSections=15;}else{totalSections= ((896/indicesEtiquetas.size())/2)-2;}
        if(indicesEtiquetas.size()>=10){anchoDivChart=1000;}else{anchoDivChart=(indicesEtiquetas.size()*100);}
        return indicesEtiquetas;
    }
    
    public String getLeyendBarras(){
        String leyend1='Cumplidos';
        String leyend2='Faltantes';
        String cadenaRetorno=leyend1+'|'+leyend2;
        return EncodingUtil.urlEncode(cadenaRetorno, 'UTF-8');
    }
    
    public String getColoresLeyend(){
        String color1='2F75B5';
        String color2='C00000';
        String cadenaRetorno= color1+'|'+color2;
        return cadenaRetorno;
    }        
    
    public void getTotalSiNo(){
		totalsi=0;
		totalno=0;        
        for(Id Idsecc: resumenRespuestas.keySet()){
          totalsi = totalsi+resumenRespuestas.get(Idsecc).si;
          totalno = totalno+resumenRespuestas.get(Idsecc).no;  
        } 
    }
    
    public class ResumenRespuestas{
        public String seccion{get; set;}
        public Integer si{get; set;}
        public Integer no{get; set;}
        public ResumenRespuestas(){
            this('',0,0);
        }
        ResumenRespuestas(String seccion){
            this(seccion,0,0);
        }
        ResumenRespuestas(String seccion, Integer si, Integer no){
            this.seccion = seccion;
            this.si = si;
            this.no = no;
        }
        public void sumaSi(){
            this.si++;
        }
        public void sumaNo(){
            this.no++;
        }
    }

}