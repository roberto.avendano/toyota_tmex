/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que se utiliza para el resultado de una actualización..

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    12-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_WrpResultadoActualizacion {

    @AuraEnabled 
    public Boolean blnEstatus {get;set;}    
    @AuraEnabled 
    public String strDetalle {get;set;}    
    @AuraEnabled 
    public TAM_ActivaClienteCorporativoCtrl.wrpDatosClienteCorporativo objParamOpcional {get;set;}    
    @AuraEnabled 
    public String strParamOpcional2 {get;set;}    
    
    //Un constructor por default
    public TAM_WrpResultadoActualizacion(){
    	this.blnEstatus = false;
    	this.strDetalle = '';    	
    }
    
    //Un constructor que recibe parametros 
    public TAM_WrpResultadoActualizacion(Boolean blnEstatus, String strDetalle){
    	this.blnEstatus = blnEstatus;
    	this.strDetalle = strDetalle;
    }
    
}