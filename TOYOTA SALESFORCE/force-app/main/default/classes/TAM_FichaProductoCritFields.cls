public class TAM_FichaProductoCritFields {

@AuraEnabled

    public static User getCurrentProfile(String userId){
        
    User profUser = new User();
    profUser = [Select Id,Name,Profile.Name From User where id  =: userId];
        
    return profUser;    
        
    }    
    
    
}