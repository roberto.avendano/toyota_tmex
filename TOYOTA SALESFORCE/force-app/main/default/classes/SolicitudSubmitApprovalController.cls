public class SolicitudSubmitApprovalController {
    private final SolicitudInternaVehiculos__c siv;
    
    public SolicitudSubmitApprovalController(ApexPages.StandardController stdController) {
        this.siv = getSIV(stdController.getId());
    }
    
    public SolicitudInternaVehiculos__c getSIV(String sivId){
        return [SELECT Id, EnviarSolicitud__c FROM SolicitudInternaVehiculos__c WHERE Id =:sivId];
    }
    
    public PageReference submitApproval(){
         System.debug(siv.EnviarSolicitud__c);
        siv.EnviarSolicitud__c = true;
        try{
            update siv;
            System.debug('Se actualizo siv: ' +siv.EnviarSolicitud__c);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'La solicitud se envió para aprobación '));
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: ' + e.getMessage()));
        }
        PageReference pageRef;
        return pageRef;
    } 
    
        public pagereference backMethod(){
        PageReference  pageRef;
        String csId = siv.Id;  
        pageRef = new PageReference('/' +csId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}