/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS TAM_RecompraLeadManagmt

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    11-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_RecompraLeadManagmt_tst {

    static testMethod void TAM_RecompraLeadManagmtOK() {

        Test.startTest();
            Account acT = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '57051');
                system.debug('TAM_LeadInventariosTriggerHandklerTst;' + acT);
            insert acT; 
            
            Id p = [select id from profile where name='Partner Community User'].id;
            Id ac = [Select id,name from account where  Codigo_Distribuidor__c = '57051'].id;
            
            Contact con = new Contact(
                LastName ='Toms',            
                AccountId = ac);
            insert con;  

            Contact con2 = new Contact(
                LastName ='Toms',            
                AccountId = ac);
            insert con2;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', 
                username='testOwnertfsproduc@noemail.com.test');
            
            insert user;

            User user2 = new User(
                alias = 'test124p', 
                email='test1234tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive = true,
                ContactId = con2.Id,
                timezonesidkey='America/Los_Angeles', 
                username='testOwnertfsproduc2@noemail.com.test'
                );
            
            insert user2;
            
            RecordType rectype = [select id,DeveloperName from RecordType where Name = :'Retail Nuevos' and SobjectType = 'Lead'];      
                        
            Lead l2vin = new Lead();
            l2vin.RecordTypeId = rectype.id;
            l2vin.LeadSource = 'DISTRIBUIDOR Tráfico de piso'; //'Landing Page';
            l2vin.FirstName = 'Hector';
            l2vin.Email = 'hectfa@test.com';
            l2vin.phone = '5571123922';
            l2vin.Status='Nuevo Prospecto';
            l2vin.LastName = 'Hector';
            l2vin.TAM_Inventario__c  = null;
            l2vin.FWY_codigo_distribuidor__c = '57051';
            l2vin.TAM_EnviarAutorizacion__c = false;
            System.debug('EN TestLeadTrigger.testLead2 l2vin: ' + l2vin);
            //Crea el candidato
            insert l2vin;

            //Manda llamar al metodo TAM_RecompraLeadManagmt.
            TAM_RecompraLeadManagmt.getDatosLead(l2vin.id);
            
            //Crea el objeto del tipo TAM_RecompraLeadManagmt.wrpDatosRecompraLead
            TAM_RecompraLeadManagmt.wrpDatosRecompraLead objWrpDatosRecompraLead =
                new TAM_RecompraLeadManagmt.wrpDatosRecompraLead(l2vin, true);
            //Manda llamar al metodo TAM_RecompraLeadManagmt.creaNuevoProspecto
            TAM_RecompraLeadManagmt.creaNuevoProspecto(l2vin.id, objWrpDatosRecompraLead, 'Prospecto Caliente');
            
        Test.stopTest();
        
    }
}