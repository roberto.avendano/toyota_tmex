/*
* Autor: Carlos Cruz Ordoñez
* Email: ccruz@freewayconsulting.com

* Se agregan metodos de prueba para procesos de TMEX
* Email: roberto.avendano@globant.com

*/

@isTest(SeeAllData=true)
public class TestLeadTrigger {

    //Metodo de prueba desarrollado por equipo de Globant
    static testMethod void testLead2(){
        Test.startTest();
        	
	        Id p = [select id from profile where name='Partner Community User'].id;
	        Id ac = [Select id,name from account where  Codigo_Distribuidor__c = '57050'].id;
	        
	        Contact con = new Contact(
	            LastName ='Toms',            
	            AccountId = ac);
	        insert con;  

	        Contact con2 = new Contact(
	            LastName ='Toms',            
	            AccountId = ac);
	        insert con2;  
	        
	        User user = new User(
	            alias = 'test123p', 
	            email='test123tfsproduc@noemail.com',
	            Owner_Candidatos__c= true,
	            emailencodingkey='UTF-8', 
	            lastname='Testingproductfs', 
	            languagelocalekey='en_US',
	            localesidkey='en_US', 
	            profileid = p, 
	            country='United States',
	            IsActive =true,
	            ContactId = con.Id,
	            timezonesidkey='America/Los_Angeles', 
	            username='testOwnertfsproduc@noemail.com.test');
	        
	        insert user;

	        User user2 = new User(
	            alias = 'test124p', 
	            email='test1234tfsproduc@noemail.com',
	            Owner_Candidatos__c= true,
	            emailencodingkey='UTF-8', 
	            lastname='Testingproductfs', 
	            languagelocalekey='en_US',
	            localesidkey='en_US', 
	            profileid = p, 
	            country='United States',
	            IsActive = true,
	            ContactId = con2.Id,
	            timezonesidkey='America/Los_Angeles', 
	            username='testOwnertfsproduc2@noemail.com.test'
	            );
	        
	        insert user2;
	        
	        RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_NuevoCandidato' and SobjectType = 'Lead'];   	
	        
	        //Se crea un registro de tipo InventarioWholesale__c
	        InventarioWholesale__c invWhsale = new InventarioWholesale__c();
	        invWhsale.Dealer_Code__c	 = '57045';
	        invWhsale.Toms_Series_Name__c  = 'COROLLA';
	        invWhsale.name = '5YFBPRBEXLP033275';
	        invWhsale.Model_Year__c = '2020';
	        invWhsale.Model_Number__c = '1792';
	        insert invWhsale;
	        
	        Lead l2vin = new Lead();
	        l2vin.RecordTypeId = rectype.id;
	        l2vin.LeadSource = 'Landing Page';
	        l2vin.FirstName = 'Hector';
	        l2vin.Email = 'hectfa@test.com';
	        l2vin.phone = '5571123922';
	        l2vin.Status='Nuevo Prospecto';
	        l2vin.LastName = 'Hector';
	        l2vin.TAM_Inventario__c  = null;
	        l2vin.FWY_codigo_distribuidor__c = '57050';
	        l2vin.TAM_EnviarAutorizacion__c = false;
            l2vin.TAM_OrigenProspectoFacebook__c = 'TMEX - Facebook Lunave';
	        insert l2vin;
	        
	        System.debug('EN TestLeadTrigger.testLead2 l2vin: ' + l2vin);
	        Lead candidatoUpd = [Select id,FWY_codigo_distribuidor__c,TAM_Inventario__c,LastName,Status,phone,Email,FirstName,LeadSource from lead where id =: l2vin.id ];
	        //candidatoUpd.LastName = 'Figueroa';
	        candidatoUpd.TAM_Inventario__c = invWhsale.id;
	        candidatoUpd.TAM_TipoCandidato__c = 'Retail Nuevos';
            candidatoUpd.TAM_ConfirmaDatos__c = true;
            candidatoUpd.TAM_EnviarAutorizacion__c = false;
            candidatoUpd.Status = 'Prueba de Manejo';
            candidatoUpd.OwnerId = user.id;
            System.debug('EN TestLeadTrigger.testLead2 candidatoUpd: ' + candidatoUpd);
	        //Actualiza el reg
	        update candidatoUpd;
	        System.debug('EN TestLeadTrigger.testLead2 candidatoUpd2: ' + candidatoUpd);
			        	
	        Lead candidatoUpd2 = [Select id,FWY_codigo_distribuidor__c,TAM_Inventario__c,LastName,Status,phone,Email,FirstName,LeadSource from lead where id =: l2vin.id ];
	        candidatoUpd2.TAM_TipoCandidato__c = 'Retail Comonuevos';
            candidatoUpd2.TAM_ConfirmaDatos__c = true;
            candidatoUpd2.TAM_EnviarAutorizacion__c = true;
            candidatoUpd2.Status = 'Cotización';
            candidatoUpd2.OwnerId = user2.id;
            System.debug('EN TestLeadTrigger.testLead2 candidatoUpd20: ' + candidatoUpd2);
	        //Actualiza el reg
	        update candidatoUpd2;
	        System.debug('EN TestLeadTrigger.testLead2 candidatoUpd21: ' + candidatoUpd2);
        
            //Manda llamar el metodo actualizaNoDistCRM
            LeadTriggerHandler.actualizaNoDistCRM(candidatoUpd2);
        
        Test.stopTest();
    }    

    //Metodo de prueba desarrollado por equipo de Globant
    static testMethod void testLead3(){
        Test.startTest();

            String VaRtLeadRtPrecalificado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cotización TFS').getRecordTypeId();
            String VaRtLeadRtCotizaTmex = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cotización.').getRecordTypeId();
            String VaRtLeadRtBdc = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('BDC').getRecordTypeId();
        	
	        Id p = [select id from profile where name='Partner Community User'].id;
	        Id ac = [Select id,name from account where  Codigo_Distribuidor__c = '57050'].id;
	        
	        Contact con = new Contact(
	            LastName ='Toms',            
	            AccountId = ac);
	        insert con;  

	        Contact con2 = new Contact(
	            LastName ='Toms',            
	            AccountId = ac);
	        insert con2;  
	        
	        User user = new User(
	            alias = 'test123p', 
	            email='test123tfsproduc@noemail.com',
	            Owner_Candidatos__c= true,
	            emailencodingkey='UTF-8', 
	            lastname='Testingproductfs', 
	            languagelocalekey='en_US',
	            localesidkey='en_US', 
	            profileid = p, 
	            country='United States',
	            IsActive =true,
	            ContactId = con.Id,
	            timezonesidkey='America/Los_Angeles', 
	            username='testOwnertfsproduc@noemail.com.test');
	        
	        insert user;

	        User user2 = new User(
	            alias = 'test124p', 
	            email='test1234tfsproduc@noemail.com',
	            Owner_Candidatos__c= true,
	            emailencodingkey='UTF-8', 
	            lastname='Testingproductfs', 
	            languagelocalekey='en_US',
	            localesidkey='en_US', 
	            profileid = p, 
	            country='United States',
	            IsActive = true,
	            ContactId = con2.Id,
	            timezonesidkey='America/Los_Angeles', 
	            username='testOwnertfsproduc2@noemail.com.test'
	            );
	        
	        insert user2;
	        
	        RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_NuevoCandidato' and SobjectType = 'Lead'];   	
	        	        
	        Lead l2vin = new Lead();
	        l2vin.RecordTypeId = rectype.id;
	        l2vin.LeadSource = 'DISTRIBUIDOR Tráfico de piso'; //'Landing Page';
	        l2vin.FirstName = 'Hector';
	        l2vin.Email = 'hectfa@test.com';
	        l2vin.phone = '5571123922';
	        l2vin.Status='Nuevo Prospecto';
	        l2vin.LastName = 'Hector';
	        l2vin.TAM_Inventario__c  = null;
	        l2vin.FWY_codigo_distribuidor__c = '57050';
	        l2vin.TAM_EnviarAutorizacion__c = false;
            l2vin.TAM_OrigenProspectoFacebook__c = 'TMEX - Facebook Lunave';
	        insert l2vin;
	        
	        System.debug('EN TestLeadTrigger.testLead2 l2vin: ' + l2vin);
	        Lead candidatoUpd = [Select id, FWY_Estatus__c from lead where id =: l2vin.id ];
			candidatoUpd.FWY_Estatus__c = 'Comprará posteriormente';
	        //Actualiza el reg
	        update candidatoUpd;
	        System.debug('EN TestLeadTrigger.testLead2 candidatoUpd2: ' + candidatoUpd);
	        
			candidatoUpd.TAM_EstatusBaja__c = 'Reactivar lead';
	        //Actualiza el reg
	        update candidatoUpd;
        
            Lead l3vin = new Lead();
            l3vin.RecordTypeId = VaRtLeadRtBdc;
            l3vin.LeadSource = 'DISTRIBUIDOR Tráfico de piso'; //'Landing Page';
            l3vin.FirstName = 'Hector';
            l3vin.Email = 'hectfa01@test.com';
            l3vin.phone = '5511123922';
            l3vin.Status='Nuevo Prospecto';
            l3vin.LastName = 'Hector';
            l3vin.TAM_Inventario__c  = null;
            l3vin.FWY_codigo_distribuidor__c = '57050';
            l3vin.TAM_EnviarAutorizacion__c = false;
            l3vin.TAM_OrigenProspectoFacebook__c = 'TMEX - Facebook Lunave';
            insert l3vin;
        
        Test.stopTest();
    }        
}