/**
 *  Descripción General: Clase de apoyo para creación de objetos de prueba.
 *  ________________________________________________________________
 *      Autor                   Fecha               Descripción
 *  ________________________________________________________________
 *      Cecilia Cruz            26/Agosto/2020      Versión Inicial
 *  ________________________________________________________________
 */
@isTest
public class TAM_TestUtilityClass {

    //User
    public static User crearUsuarioPartnerCommunity(String strName, String codigoDistribuidor){
        Id p = [SELECT id FROM profile WHERE name='Partner Community User'].id;
        Id RecordTypeId_Dealer = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        Account ac = new Account(name = strName, Codigo_Distribuidor__c = codigoDistribuidor, recordTypeId = RecordTypeId_Dealer);
        insert ac; 
        
        Contact con = new Contact(LastName = strName ,AccountId = ac.Id);
        insert con;  
        
        User user = new User(alias = 'test123', email= codigoDistribuidor +'@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             PortalRole  = 'Manager',
                             timezonesidkey='America/Los_Angeles', username= codigoDistribuidor +'@noemail.com');
        
        insert user;
        return user;
    }
    
    //Serie
    public static Serie__c crearSerie(String strSerie){
        Serie__c objSerie = new Serie__c( Id_Externo_Serie__c = strSerie,Name = strSerie);
        insert objSerie;
        return objSerie;
    }
    
    //Producto
    public static Product2 crearProducto(Id idSerie, String strClaveModelo, String strAnioModelo, Boolean isActive){
    	Product2 objProduct = new Product2();
        objProduct.Name= strClaveModelo;
        objProduct.IdExternoProducto__c = strClaveModelo + strAnioModelo;
        objProduct.ProductCode= strClaveModelo + strAnioModelo;
        objProduct.IsActive = isActive;
        objProduct.Serie__c = idSerie;
        insert objProduct;
        return objProduct;
    }
    
    //Pricebook Custom
    public static PricebookEntry crearPricebookEntryCustom(id objPricebook2, Product2 objProduct, Boolean isActive, Double MSRP, Double margenGanaciaPorcentaje, Double margenGananciaPesos){

        PricebookEntry objPricebookEntry = new PricebookEntry();
        objPricebookEntry.Pricebook2Id= objPricebook2;
        objPricebookEntry.Product2Id= objProduct.Id;
        objPricebookEntry.UnitPrice= MSRP;
        objPricebookEntry.IsActive = isActive;
        objPricebookEntry.TAM_MargenesGananciaTMEX_Porc__c = margenGanaciaPorcentaje;
        objPricebookEntry.TAM_MargenesGananciaTMEX_Pesos__c = margenGananciaPesos;
        objPricebookEntry.TAM_MSRP__c = MSRP;
        objPricebookEntry.PrecioSinIva__c = MSRP;
        //objPricebookEntry.IdExterno__c = objProduct.Name + objProduct.Anio__c + '-' + objPricebook2.Name;
        insert objPricebookEntry;
        return objPricebookEntry;
    }
    
    //Catálogo Centralizado de Modelos
    public static CatalogoCentralizadoModelos__c crearModeloCatalogoCentralizado(String strSerie, String claveModelo,String anioModelo, String version){
        CatalogoCentralizadoModelos__c objCatalogo = new CatalogoCentralizadoModelos__c();
        objCatalogo.Name = strSerie + '-' + claveModelo + '-' + anioModelo + '-' + version;
        objCatalogo.Serie__c = strSerie;
        objCatalogo.AnioModelo__c = anioModelo;
        objCatalogo.Modelo__c = claveModelo;
        objCatalogo.Version__c = version;
        insert objCatalogo;
        crearCodigosContablesModelos(objCatalogo);
        return objCatalogo;
    }
    
    //Modelos con codigos de producto
    public static void crearCodigosContablesModelos(CatalogoCentralizadoModelos__c objCatalogo){
        TAM_ModeloCodigoProducto__c objmodelo = new TAM_ModeloCodigoProducto__c();
        objmodelo.Name = objCatalogo.Modelo__c;
        objmodelo.TAM_Modelo__c = objCatalogo.Serie__c;
        objmodelo.TAM_Codigo__c = objCatalogo.Modelo__c + objCatalogo.AnioModelo__c;
        insert objmodelo;
    }
	
    //Inventario en Piso
    public static InventarioPiso__c crearInventarioPiso(String strSerie, String claveModelo,String anioModelo, String version, 
                                           Date fechaInventario, Integer intCantidad, Integer intDiasVenta){
        InventarioPiso__c objInventario = new InventarioPiso__c();
        objInventario.Name = strSerie + '-' + claveModelo + '-' + anioModelo + '-' + version;
        objInventario.FechaInventario__c = fechaInventario;
        objInventario.Modelo__c = claveModelo;
        objInventario.AnioModelo__c = anioModelo;
        objInventario.Serie__c = strSerie;
        objInventario.Version__c = version;
        objInventario.Cantidad__c = intCantidad;
        objInventario.DiasVenta__c = intDiasVenta;
        insert objInventario;
        return objInventario;
    }
    
    //Politica de Incentivos
    public static TAM_PoliticasIncentivos__c crearPoliticaIncentivos(String strTipo, String strNombre, Date inicioVigencia, Date finVigencia, Boolean cargaHistorica, String strStatus){
		TAM_PoliticasIncentivos__c objPolitica;
        
        //Retail
        if(strTipo == 'Retail'){
            TAM_PoliticasIncentivos__c objPoliticaRetail = new TAM_PoliticasIncentivos__c();
            objPoliticaRetail.Name = strNombre;
            objPoliticaRetail.TAM_InicioVigencia__c = inicioVigencia;
            objPoliticaRetail.TAM_FinVigencia__c = finVigencia;
            objPoliticaRetail.TAM_PeriodoDeGracia__c = 10;
            objPoliticaRetail.TAM_EstatusIncentivos__c = strStatus;
            objPoliticaRetail.TAM_CargaHistorica__c = cargaHistorica;
            objPoliticaRetail.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
        	insert objPoliticaRetail;
            objPolitica = objPoliticaRetail;
        }
        
        //Vta. Corporativa
        if(strTipo == 'Venta Corporativa'){
            TAM_PoliticasIncentivos__c objPoliticaVtaCorp = new TAM_PoliticasIncentivos__c();
            objPoliticaVtaCorp.Name = strNombre;
            objPoliticaVtaCorp.TAM_InicioVigencia__c = inicioVigencia;
            objPoliticaVtaCorp.TAM_FinVigencia__c = finVigencia;
            objPoliticaVtaCorp.TAM_PeriodoDeGracia__c = 10;
            objPoliticaVtaCorp.TAM_EstatusIncentivos__c = strStatus;
            objPoliticaVtaCorp.TAM_CargaHistorica__c = cargaHistorica;
            objPoliticaVtaCorp.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
            insert objPoliticaVtaCorp;
            objPolitica = objPoliticaVtaCorp;
        }
        return objPolitica;
    }
    
    //Detalle Politica Incentivos solo Efectivo.
    public static TAM_DetallePoliticaJunction__c crearDetallePoliticaIncentivos(String strSerie, String claveModelo,String anioModelo, String version,
                                                     							Double incentivoPropuesto, Double incentivoTMEX, Double IncentivoDealer, 
                                                                                Double incentivoPropuestoTFS, Double incentivoTMEX_TFS, Double IncentivoDealer_TFS,Boolean aplicanAmbos,
                                                                                Id idPolitica, Double MSRP)
    {
        TAM_DetallePoliticaJunction__c objDetalle = new TAM_DetallePoliticaJunction__c();
        objDetalle.TAM_Serie__c = strSerie;
        objDetalle.TAM_Clave__c = claveModelo;
        objDetalle.TAM_AnioModelo__c = anioModelo;
        objDetalle.TAM_Version__c = version;
        objDetalle.TAM_IncPropuesto_Cash__c = incentivoPropuesto;
        objDetalle.TAM_IncentivoTMEX_Cash__c = incentivoTMEX;
        objDetalle.TAM_IncDealer_Cash__c = IncentivoDealer;
        objDetalle.TAM_TotalEstimado__c = incentivoPropuestoTFS;
        objDetalle.TAM_TP_IncTMEX_Cash__c = incentivoTMEX_TFS;
        objDetalle.TAM_TP_IncDealer_Cash__c = IncentivoDealer;
        objDetalle.TAM_AplicanAmbosIncentivos__c =aplicanAmbos;
        objDetalle.TAM_PoliticaIncentivos__c = idPolitica;
        objDetalle.TAM_MSRP__c = MSRP;
        insert objDetalle;
        return objDetalle;
    }
    
	//VIN
    public static Vehiculo__c crearVIN(String strVIN, String strAnioModelo){
        Vehiculo__c objVehiculo = new Vehiculo__c();
        objVehiculo.Name = strVIN;
        objVehiculo.Id_Externo__c = strVIN;
        objVehiculo.AnioModel__c = strAnioModelo;
        return objVehiculo;
    }
    
    //Movimiento
    public static Movimiento__c crearMovimientos(String anioModelo,String tipoMovimiento, String strCodigoVta, DateTime fechaVenta, String fleet,
                                        String mes, String anio, Id idProducto, Id idVIN, Id distribuidor)
    {
        Movimiento__c objMovimiento = new Movimiento__c();
        objMovimiento.Year_Modelo__c = anioModelo;
        objMovimiento.Trans_Type__c = tipoMovimiento;
        objMovimiento.Sale_Code__c = strCodigoVta;
        objMovimiento.Submitted_Date__c = fechaVenta;
        objMovimiento.Sale_Date__c = Date.valueOf(fechaVenta);              
        objMovimiento.Fleet__c = fleet;
        objMovimiento.Month__c = mes;
        objMovimiento.Year__c = anio;
        objMovimiento.Modelo__c = idProducto;
        objMovimiento.VIN__c = idVIN;
        objMovimiento.Distribuidor__c = distribuidor;
        objMovimiento.First_Name__c = 'FirstName';
        objMovimiento.Last_Name__c = 'LastName';
        objMovimiento.Factura__c = 'Factura';
        return objMovimiento;
    }

    //Factura
    public static Factura__c crearFactura(Id VIN, DateTime fechaTimbrado, Double totalFactura, Id distribuidor){
        Factura__c objFactura = new Factura__c();
        objFactura.VIN__c = VIN;
        objFactura.TAM_FechaTimbrado__c = fechaTimbrado;
        objFactura.TAM_Total__c = totalFactura;
        objFactura.Distribuidor__c = distribuidor;
        insert objFactura;
        return objFactura;
    }
    
    //Auto Demo
    public static SolicitudAutoDemo__c crearAutoDemo(Id VIN, String estatusSeguimiento){
        SolicitudAutoDemo__c objSolicitudDemo = new SolicitudAutoDemo__c();
        objSolicitudDemo.VIN__c = VIN;
        objSolicitudDemo.Estatus__c = estatusSeguimiento;
        insert objSolicitudDemo;
        return objSolicitudDemo;
    }
    
    //Estado de Cuenta
    public static TAM_EstadoCuenta__c crearEdoCuenta(String strNombre, String strNombreDealer, String strCodDealer, Date datInicio, Date datFin){
        TAM_EstadoCuenta__c objEstadoCuenta = new TAM_EstadoCuenta__c();
        objEstadoCuenta.Name = strNombre;
        objEstadoCuenta.TAM_NombreDealer__c = strNombreDealer;
        objEstadoCuenta.TAM_CodigoDealer__c = strCodDealer;
        objEstadoCuenta.TAM_FechaInicio__c = datInicio;
        objEstadoCuenta.TAM_FechaCierre__c = datFin;
        insert objEstadoCuenta;
        return objEstadoCuenta;
    }
    
    //Detalle Estado de Cuenta
    public static void crearDetalleEdoCuenta(String strVIN, Date DatFacturaVta, String strCodDealer, Double dblIncentivo,
                                             String strRecordTypeName, String strEstatusFinanzas, String strEstatusVINEdoCuenta,
                                            String strTipoMovimiento, Boolean boolIntercambioEdoCuenta)
    {
        Map<Integer, String> mapMesPorNum = new Map<Integer, String>();
        mapMesPorNum.put(1,'ENERO');
        mapMesPorNum.put(2,'FEBRERO');
        mapMesPorNum.put(3,'MARZO');
        mapMesPorNum.put(4,'ABRIL');
        mapMesPorNum.put(5,'MAYO'); 
        mapMesPorNum.put(6,'JUNIO');
        mapMesPorNum.put(7,'JULIO');
        mapMesPorNum.put(8,'AGOSTO');
        mapMesPorNum.put(9,'SEPTIEMBRE');
        mapMesPorNum.put(10,'OCTUBRE');
        mapMesPorNum.put(11,'NOVIEMBRE');
        mapMesPorNum.put(12,'DICIEMBRE');
        
        TAM_DetalleEstadoCuenta__c objDetalle = new TAM_DetalleEstadoCuenta__c();
        objDetalle.TAM_VIN__c = strVIN;
        objDetalle.TAM_FechaFactura__c = DatFacturaVta;
        objDetalle.TAM_Periodo__c = mapMesPorNum.get(DatFacturaVta.month());
        objDetalle.TAM_CodigoDealer__c = strCodDealer;
        objDetalle.TAM_IncentivoPropuestoDealer__c = dblIncentivo;
        objDetalle.TAM_Monto_sin_IVA__c = dblIncentivo;
        objDetalle.TAM_Fecha_Aprobacion_Final__c = DatFacturaVta;
        objDetalle.TAM_Estatus_Finanzas__c = strEstatusFinanzas;
        objDetalle.TAM_EstatusVINEdoCta__c = strEstatusVINEdoCuenta;
        objDetalle.TAM_Movimiento__c = strTipoMovimiento;
        objDetalle.TAM_IntercambioEdoCta__c = boolIntercambioEdoCuenta;
        objDetalle.TAM_Fecha_Pago__c = DatFacturaVta;
        objDetalle.RecordTypeId = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
        insert objDetalle;
    }
    
    //Solicitud Excepcion Incentivo
    public static void solicitudExcepcionIncentivo(String strRecordTypeName, String strVIN, Id politicaSeleccionada, String strEstatus, Date datFechaAprobacion){
        TAM_SolicitudExpecionIncentivo__c objExcepcion = new TAM_SolicitudExpecionIncentivo__c();
        objExcepcion.TAMVIN__c = strVIN;
        objExcepcion.TAM_PoliticaSeleccionadaRetail__c = politicaSeleccionada;
        objExcepcion.TAM_AprobacionFinanzas__c = strEstatus;
        objExcepcion.TAM_Fecha_Autorizacion_Finanzas__c = datFechaAprobacion;
        objExcepcion.RecordTypeId = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByName().get(strRecordTypeName).getRecordTypeId();
        insert objExcepcion;
    }
    
    //Rango
    public static Rangos__c crearRango(String Name, Integer unidadesMin, Integer unidadesMax, Boolean boolActivo, String recordTypeName){
        Rangos__c objRango = new Rangos__c();
        objRango.Name = Name;
		objRango.NumUnidadesMinimas__c = unidadesMin;			
		objRango.NumUnidadesMaximas__c = unidadesMax;
		objRango.Activo__c = boolActivo;
        objRango.RecordTypeId = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
		insert objRango;
        return objRango;
    }
    
    //Solicitud Flotilla/Programa
    public static TAM_SolicitudesFlotillaPrograma__c crearSolicitudFlotilla(Id idClienteCorp, Id idRango, String strCodDistribuidor, String strEstatus, String recordTypeName){
        TAM_SolicitudesFlotillaPrograma__c objSolicitud = new TAM_SolicitudesFlotillaPrograma__c();
        objSolicitud.TAM_Cliente__c = idClienteCorp;
        objSolicitud.TAM_ProgramaRango__c = idRango;
        objSolicitud.TAM_NombreCompletoDistribuidor__c = strCodDistribuidor;
        objSolicitud.TAM_Estatus__c = strEstatus;
        objSolicitud.TAM_FechaCierreSolicitud__c = Date.today();
        objSolicitud.RecordTypeId = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        insert objSolicitud;
        return objSolicitud;
    }
    
    //Detalle solicitud de compra
    public static TAM_DetalleOrdenCompra__c crearDetalleCompra(String strName, Date datFechaSolicitud, String strEstatusEntrega, Id idSolicitudFlotilla, 
                                          Id idCatalogoModelos, String recordTypeName)
    {
        TAM_DetalleOrdenCompra__c objOrdenCompra = new TAM_DetalleOrdenCompra__c();
        objOrdenCompra.Name = strName;
        objOrdenCompra.TAM_FechaSolicitud__c = datFechaSolicitud;
        objOrdenCompra.TAM_EstatusEntrega__c = strEstatusEntrega;
        objOrdenCompra.TAM_SolicitudFlotillaPrograma__c = idSolicitudFlotilla;
        objOrdenCompra.TAM_CatalogoCentralizadoModelos__c = idCatalogoModelos;
        objOrdenCompra.TAM_Cantidad__c = 10;
        objOrdenCompra.RecordTypeId = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        objOrdenCompra.TAM_IdExterno__c = idSolicitudFlotilla + '-'+strName+'-'+objOrdenCompra.TAM_Cantidad__c+'-'+objOrdenCompra.RecordTypeId;
        insert objOrdenCompra;
        return objOrdenCompra;  
    }
    
    //VIN flotilla/programa
    public static void crearVinFlotilla(String strVin, Id idSolicitudFlotilla, String recordTypeName, String folioOrdenCompra, String estatus){
        String sRectorTypePasoVinesPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        TAM_VinesFlotillaPrograma__c objVIN = new TAM_VinesFlotillaPrograma__c();
        objVIN.Name = strVin; 
        objVIN.TAM_IdExterno__c = idSolicitudFlotilla + '-'+ folioOrdenCompra + '-'+ strVin + '-' + sRectorTypePasoVinesPrograma; 
        objVIN.TAM_Estatus__c = estatus;
        objVIN.TAM_SolicitudFlotillaPrograma__c = idSolicitudFlotilla;
		insert objVIN;
    }
    
    //DOD Pedido especial
    public static void crearPedidoEspecial(String strVIN, Boolean boolEntregado, String strEstatus){
        TAM_DODSolicitudesPedidoEspecial__c objDOD = new TAM_DODSolicitudesPedidoEspecial__c();
        objDOD.TAM_VIN__c = strVIN;
        objDOD.TAM_EntregadoDealer__c = boolEntregado;
        objDOD.TAM_Estatus__c = strEstatus;
        insert objDOD;
    }

    //Politica Incentivos Bonos
    public static TAM_Bonos__c crearPoliticaBonos(String strName, Date datInicioVigencia, Date datFinVigencia){
        TAM_Bonos__c objBonos = new TAM_Bonos__c();
        objBonos.Name = strName;
        objBonos.TAM_InicioVigencia__c = datInicioVigencia;
        objBonos.TAM_FinVigencia__c = datFinVigencia;
        insert objBonos;
        return objBonos;
    }
    
    //Detalle politica incentivos bono
    public static TAM_DetalleBonos__c crearDetalleBono(Id idPoliticaBono, String strAnioModelo, String strModelo, String strSerie,
                                       Double dblTotalEfectivo, Double dblTMEXEfectivo, Double dblDealerEfectivo, Double dblTFSEfectivo,
                                       Double dblTotal_PF, Double dblTMEX_PF, Double dblDealer_PF, Double dblTFS_PF,Boolean boolAplicanAmbos)
    {
        TAM_DetalleBonos__c objDetalleBono = new TAM_DetalleBonos__c();
        objDetalleBono.Bono__c = idPoliticaBono;
        objDetalleBono.TAM_AnioModelo__c = strAnioModelo;
        objDetalleBono.TAM_Clave__c = strModelo;
        objDetalleBono.TAM_Serie__c = strSerie;
        objDetalleBono.TAM_BonoTotalEfectivo__c = dblTotalEfectivo;
        objDetalleBono.TAM_BonoTMEXEfectivo__c = dblTMEXEfectivo;
        objDetalleBono.TAM_BonoDealerEfectivo__c = dblDealerEfectivo;
        objDetalleBono.TAM_BonoTFSEfectivo__c = dblTFSEfectivo;
        objDetalleBono.TAM_BonoTotalProductoFinanciero__c = dblTotal_PF;
        objDetalleBono.TAM_BonoTMEXProdFinanciero__c = dblTMEX_PF;
        objDetalleBono.TAM_BonoDealerProdFinanciero__c = dblDealer_PF;
        objDetalleBono.TAM_BonoTFSProdFinanciero__c = dblTFS_PF;
        objDetalleBono.TAM_AplicaAmbosBonos__c = boolAplicanAmbos;
        insert objDetalleBono;
        return objDetalleBono;
    }
    
    //Solicitud Bono
    public static void crearSolicitudBono(String strVIN, String strEstatusAprobacionFinanzas, Date datFechaAutorizacionFinanzas, Id idBonoSeleccionado, Id idBonoCorrespondiente){
        TAM_SolicitudDeBono__c objSolicitudBono = new TAM_SolicitudDeBono__c();
        objSolicitudBono.TAM_VIN__c = strVIN;
        objSolicitudBono.TAM_AprobacionFinanzas__c = strEstatusAprobacionFinanzas;
        objSolicitudBono.TAM_Fecha_Autorizacion_Finanzas__c = datFechaAutorizacionFinanzas;
        objSolicitudBono.TAM_BonoSeleccionado__c = idBonoSeleccionado;
        objSolicitudBono.TAM_BonoCorrespondiente__c = idBonoCorrespondiente;
        insert objSolicitudBono;
    }
    
    //Detalle Provision
    public static void crearDetalleProvision(String strNombre, Id idProvisionIncentivo, Id idVIN, Double dblTMEXEfectivo, Double dblBonoTMEXEfectivo, String recordTypeName, 
                                             Id idPolitica, String strClasificacion)
    {
        TAM_DetalleProvisionIncentivo__c objDetalleProvision = new TAM_DetalleProvisionIncentivo__c();
        objDetalleProvision.Name = strNombre;
        objDetalleProvision.TAM_FechaEnvio__c = String.valueOf(DateTime.now());
        objDetalleProvision.TAM_FechaVenta__c = String.valueOf(Date.today() -3);
        objDetalleProvision.TAM_ProvisionIncentivos__c = idProvisionIncentivo;
        objDetalleProvision.TAM_VIN__c = idVIN;
        objDetalleProvision.TAM_IncentivoTMEX_Efectivo__c = dblTMEXEfectivo;
        objDetalleProvision.TAM_BonoTMEX_Efectivo__c = dblBonoTMEXEfectivo;
        objDetalleProvision.TAM_PoliticaIncentivos__c = idPolitica;
        objDetalleProvision.TAM_Clasificacion__c = strClasificacion;
        objDetalleProvision.RecordTypeId = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        insert objDetalleProvision;
    }
}