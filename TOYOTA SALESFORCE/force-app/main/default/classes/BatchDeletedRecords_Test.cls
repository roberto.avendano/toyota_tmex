@isTest
private class BatchDeletedRecords_Test {
	private static final Map<String,Map<String,RecordType>> recordTypeMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
	
	@testSetup
	private static void loadData(){
   		List<Order> ordenes = new List<Order>();
   		List<InventarioWholesale__c> wholesale = new List<InventarioWholesale__c>();
   		List<Venta__c> ventasH = new List<Venta__c>();
   		
   		Map<String,Map<String,RecordType>> recordTypeMapa = BatchDeletedRecords_Test.recordTypeMap;
   		Integer wholesaleCounter = 0;
   		
   		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
   		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
   		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
   		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};
   		
   		Account acct = new Account(
	    	Name = 'Toyota Aeropuerto'
    	);
        insert acct; 
        
		for(Integer i=0; i<20; i++){			
		 	String sufix = i<10? '0'+String.valueOf(i):String.valueOf(i);  

		 	wholesale.add(new InventarioWholesale__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		Model_Year__c = String.valueOf(Date.today().year()),            		
        		Dealer_Code__c = '57055',
        		Exterior_Color_Description__c ='',
        		Exterior_Color_Code__c = String.valueOf(i),
        		Interior_Color_Description__c = '',
              	Interior_Color_Code__c = String.valueOf(i),
              	Toms_Series_Name__c = ''              	
	 		));
	 		
 			ventasH.add(new Venta__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(BatchDeletedRecords_Test.aleatorio(dealerCode.size())),            		
        		IdModelo__c = modelos.get(BatchDeletedRecords_Test.aleatorio(modelos.size())),
        		YearModelo__c = '2017',
        		IdColorExt__c = colores.get(BatchDeletedRecords_Test.aleatorio(colores.size())),
        		DescrColorExt__c = 'External color description',
        		IdColorInt__c = colores.get(BatchDeletedRecords_Test.aleatorio(colores.size())),
        		DescrColorInt__c = 'Internal color description',
    			Serial__c = seriales.get(BatchDeletedRecords_Test.aleatorio(seriales.size())),
    			SaleCode__c = sufix,
    			SubmittedDate__c = Datetime.newInstance(2017, 10, 13),
        		IdExterno__c = 'JTDKBRFUXH30310'+sufix+'-'+String.valueOf(Date.today())        		
	 		)); 			 				 				 		      
		}
		insert wholesale;
		insert ventasH;
		
		for(Integer j=0; j < wholesale.size(); j++){
			if(wholesaleCounter > 10){
				Time beforeTime = Time.newInstance(0, 0, 0, 0);
				Date beforeToday = Date.today()-1;
				
				Test.setCreatedDate(wholesale[j].Id, DateTime.newInstance(beforeToday, beforeTime));
				Test.setCreatedDate(ventasH[j].Id, DateTime.newInstance(beforeToday, beforeTime));
			}
			wholesaleCounter ++;
		}
		
		
			
    	Order pedido1 = new Order(
			Status='En proceso',
			Estatus__c='Solicitud capturada',
			EffectiveDate = Date.today(),
			AccountId = acct.Id,
			RecordTypeId = recordTypeMapa.get('Order').get('Solicitud_partes_robadas') != null ? recordTypeMapa.get('Order').get('Solicitud_partes_robadas').Id: null
    	);
    	
    	Order pedido2 = new Order(
			Status='En proceso',
			Estatus__c='Solicitud capturada',
			EffectiveDate = Date.today(),
			AccountId = acct.Id,
			RecordTypeId = recordTypeMapa.get('Order').get('SolicitudNula') != null ? recordTypeMapa.get('Order').get('SolicitudNula').Id: null
    	);
    	
    	ordenes.add(pedido1);
    	ordenes.add(pedido2);
    	
    	System.debug(JSON.serialize(ordenes));
    	insert ordenes;
	}
	
    private static testMethod void myUnitTest() {
        String sch = '0 32 21 * * ?';
        Test.startTest();
        	System.schedule('deleteNullOrders', sch, new DeleteNullOrders_Schedule());
        	System.schedule('deleteWholesales', sch, new DeleteWholesale_Schedule());
        	System.schedule('deleteVentasH', sch, new DeleteVentasH_Schedule());
        Test.stopTest();        
        
    }
    
	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
}