/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Trigger que contiene la logica para procesar los registros 
                        de TAM_LeadFacebookTgrHandler
                        
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    24-Junio-2021     Héctor Figueroa             Modificación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_LeadFacebookTgrHandlerTst {

    @testSetup 
    static void setupData() {        
        TAM_LeadFacebook__c objLeadFace = new TAM_LeadFacebook__c(
            Name = 'PRUEBA HECTOR 240621-01', 
            TAM_ApellidoPaterno__c = 'PRUEBA HECTOR 240621-01', 
            TAM_ApellidoMaterno__c = 'PRUEBA HECTOR 240621-01', 
            TAM_Telefono__c = '5534543234',
            TAM_CorreoElectronico__c = 'PRUEBA-HECTOR-240621-01@hotmail.com', 
            TAM_CodigoDistribuidorWeb__c = 'AGUASCALIENTES - 57019', 
            TAM_FormaContactoPreferida__c = 'Correo;Teléfono;Watsapp',
            TAM_VehIculo__c = 'Hiace', 
            TAM_TipoCompra__c = 'Contado',   
            TAM_OtrosDatosFacebook__c = '$50,000 o menos',   
            TAM_LosDatosSonCorrectos__c = true,      
            TAM_DatosComodinFacebook__c = 'DISTRIBUIDOR - Facebook Irapuato - 57045',
            TAM_OrigenProspectoFacebook__c = 'TMEX - Facebook Lunave',
            TAM_Campania__c = 'DTM Facebook Nacional'
        );
        //Crea el registro
        insert objLeadFace;
    }
    
    @isTest
    static void TAM_LeadFacebookTgrHandlerTst() {
        // Consulta los datos de TAM_LeadFacebook__c
        TAM_LeadFacebook__c objPasoCons = [SELECT ID, Name, TAM_ApellidoPaterno__c, TAM_ApellidoMaterno__c, TAM_Telefono__c,
            TAM_CorreoElectronico__c, TAM_CodigoDistribuidorWeb__c, TAM_FormaContactoPreferida__c, TAM_VehIculo__c,
            TAM_TipoCompra__c, TAM_OtrosDatosFacebook__c, TAM_LosDatosSonCorrectos__c, TAM_DatosComodinFacebook__c, 
            TAM_IdExterno__c, TAM_Campania__c FROM TAM_LeadFacebook__c LIMIT 1];
        //Actueliza el reg objPasoCons
        update objPasoCons;
    }
}