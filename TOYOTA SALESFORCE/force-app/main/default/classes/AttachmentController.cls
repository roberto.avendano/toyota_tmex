global with sharing class AttachmentController {

    public AttachmentController(ApexPages.StandardController controller) {

    }
    
    @RemoteAction
    global static List<Attachment> listaAdjuntos(String idParent){
    	return [SELECT ContentType, Description, Id, Name, ParentId FROM Attachment WHERE ParentId=:idParent];
    }

    @RemoteAction 
    global static String uploadAttachment(String recId, String attachmentBody, String attachmentName, String attachmentId) {
        if(recId != null) {
            if(!recId.equals('')) {
                if(attachmentBody != null) {
                    Attachment att = getAttachment(attachmentId);
                    String newBody = '';
                    if(att.Body != null) {
                        newBody = EncodingUtil.base64Encode(att.Body);
                    }
                    newBody += attachmentBody;
                    att.Body = EncodingUtil.base64Decode(newBody);
                    if(attachmentId == null) {
                        att.Name = attachmentName;
                        att.parentId = recId;
                    }
                    upsert att;
                    return att.Id;
                } else {
                    return 'Error: Attachment Body was null';
                }
            } else {
                return 'Error: Record Id was empty';
            }
        } else {
            return 'Error: Record Id was null';
        }
    }
     
    private static Attachment getAttachment(String attId) {
        List<Attachment> attachments = [SELECT Id, Body FROM Attachment WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }

}