@isTest
public class TAM_LeadWhatsApp_Test {
    
    @testSetup static void setup() {
        
        Lead l1 = new Lead();
        l1.FirstName = 'Test1';
        l1.FWY_Intencion_de_compra__c = 'Este mes'; 
        l1.Email = 'a@a.com';
        l1.phone = '5571123596';
        l1.Status='Nuevo Lead';
        l1.LastName = 'Test2';
        l1.TAM_NumeroContactoPorWhatsApp__c = null;
        insert L1;
        
        Lead l2 = new Lead();
        l2.FirstName = 'Test1';
        l2.FWY_Intencion_de_compra__c = 'Este mes'; 
        l2.Email = 'atest@a.com';
        l2.phone = '5571123596';
        l2.Status='Nuevo Lead';
        l2.LastName = 'Test2';
        l2.TAM_NumeroContactoPorWhatsApp__c = 2;
        insert L2;
        
    }
    
    
    @isTest static void testMethod1(){
        Test.startTest();
        Lead leadTest = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'a@a.com'];
        TAM_LeadWhatsApp.getInformationByLead(leadTest.id);
        TAM_LeadWhatsApp.saveLogRecord(leadTest.id);
        Lead leadTestResult = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'a@a.com'];
        System.assertEquals(leadTestResult.TAM_ContactadoWhatsApp__c,true);
        Test.stopTest();
        
    }
    
    @isTest static void testMethod2(){
        Test.startTest();
        Lead leadTest = [Select Id From Lead WHERE Email =  'atest@a.com'];
        TAM_LeadWhatsApp.getInformationByLead(leadTest.id);
        TAM_LeadWhatsApp.saveLogRecord(leadTest.id);
        Lead leadTestResult = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'atest@a.com'];
        System.assertEquals(leadTestResult.TAM_ContactadoWhatsApp__c,true);
        Test.stopTest();
        
    }
    
    
}