@isTest
public class TAM_SearchInventarioLead_Test {
    static String leadRecordId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cotización.').getRecordTypeId();
    
    
    @testSetup static void setup() {
        //Usuario Admin
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];

        //Cuenta
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002'
        );
        insert a01;
        
        //Contacto
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a01.id;
        insert contactRH1;
        
        //Lead inventario
        Lead l1 = new Lead();
        l1.FirstName = 'Test1';
        l1.FWY_Intencion_de_compra__c = 'Este mes'; 
        l1.Email = 'a@a.com';
        l1.phone = '5571123596';
        l1.Status='Nuevo Lead';
        l1.LastName = 'Test2';
        l1.TAM_NumeroContactoPorWhatsApp__c = null;
        l1.TAM_DatosValidosFacturacion__c = false;
        l1.RecordTypeId = leadRecordId;
        insert L1;
        
        //Inventarios TOYOTA
        TAM_InventarioVehiculosToyota__c invToyota = new TAM_InventarioVehiculosToyota__c();
        invToyota.name = 'QWERTYU56789023';
        invToyota.Dealer_Code__c = '57002';	
        invToyota.Dummy_VIN__c = 'QWERTYU56789023';
        invToyota.TAM_DiasPiso__c = '10';
        invToyota.Exterior_Color_Code__c = 'ffff';
        invToyota.Exterior_Color_Description__c = 'White';
        invToyota.TAM_IdExterno__c  = 'QWERTYU56789023';
        invToyota.Interior_Color_Code__c = 'AHQ1';
        invToyota.Interior_Color_Description__c = 'White Lunar';
        invToyota.Model_Number__c  = '1790';
        invToyota.Model_Year__c = '2020';
        invToyota.Numero_de_Orden__c = 'QAEI0101932';
        invToyota.Tipo_Inventario__c  = 'Piso';
        invToyota.Toms_Series_Name__c = 'RAV 4';
        invToyota.TAM_Version__c = 'Sport';
        invToyota.Name   = 'QWERTYU56789023';
        insert invToyota;
        
        //Relacion Lead - Inventario
        TAM_LeadInventarios__c leadInventario = new TAM_LeadInventarios__c();
        leadInventario.TAM_Prospecto__c = L1.id;
        leadInventario.TAM_InventarioVehiculosFyG__c = invToyota.id;
        leadInventario.TAM_VINFacturacion__c = 'QWERTYU56789023';
        insert leadInventario;
        
        
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia 
            for (UserRole objUserRole : [SELECT Id FROM UserRole WHERE Name='Toyota Monterrey Socio Ejecutivo']){
                UserRole uR = objUserRole;
            }
            //UserRole  = [SELECT Id FROM UserRole WHERE Name='Toyota Monterrey Socio Ejecutivo'];
            
            Profile p;
            for (Profile objProfile : [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']){
                p = objProfile;
            }            
            //Obtenemos un perfil RH de muestra de la instancia
            //Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,ContactId = contactRH1.id,
                              PortalRole  = 'Manager', TimeZoneSidKey='America/Los_Angeles', 
                              UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
        }
        
    }
    
    @isTest static void testMethod1(){
        Test.startTest();
        Lead leadTest = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'a@a.com'];
        TAM_SearchInventarioLead.estatusFacturacionByLead(leadTest.id);
        Test.stopTest();
        
    }
    
    @isTest static void testMethod2(){
        Test.startTest();
        Lead leadTest = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'a@a.com'];
        TAM_SearchInventarioLead.getInventariosByLead(leadTest.id);
        Test.stopTest();
        
    }
    
    @isTest static void testMethod3(){
        Test.startTest();
        Lead leadTest = [Select Id,TAM_ContactadoWhatsApp__c From Lead WHERE Email =  'a@a.com'];
        List<TAM_SearchInventarioLead.wrapVIN> TestVIN = new List<TAM_SearchInventarioLead.wrapVIN> ();
        TAM_SearchInventarioLead.wrapVIN wrrapperItem = new TAM_SearchInventarioLead.wrapVIN();
        wrrapperItem.VIN = 'QWERTYU56789021';
        wrrapperItem.Id = 'QWERTYU56789021';
        wrrapperItem.numeroOrden = 'TES2020';
        wrrapperItem.IdInventarioLead = 'QWERTYU56789021';
        wrrapperItem.Model_Year = '2020';
        wrrapperItem.Tipo_Inventario = 'Piso';
        wrrapperItem.Toms_Series_Name  = 'RAV 4';
        wrrapperItem.TAM_Version = 'SPORT';
        wrrapperItem.TAM_DiasPiso = '10';
        wrrapperItem.Model_Number = '1790';
        wrrapperItem.codigoColorInterior = 'ffff';
        wrrapperItem.descripcionColorInterior = 'Red';
        wrrapperItem.codigoColorExterior = 'AF12';
        wrrapperItem.descripcionColorExterior = 'Lunar Rock';
        TestVIN.add(wrrapperItem);
        
        User usrObj = [Select id,CodigoDistribuidor__c FROM User WHERE UserName='test99@testorg.com'];
        
        TAM_SearchInventarioLead.vinInventarioLead('RAV',usrObj.id,TestVIN);
        Test.stopTest();
        
    }
    
    @isTest static void testMethod4(){
        Test.startTest();
        //Inventarios TOYOTA
        TAM_InventarioVehiculosToyota__c invenTOYOTA = [select id from TAM_InventarioVehiculosToyota__c where Name = 'QWERTYU56789023'];
        
        TAM_SearchInventarioLead.wrapVIN wrrapperItem = new TAM_SearchInventarioLead.wrapVIN();
        wrrapperItem.VIN = 'QWERTYU56789021';
        wrrapperItem.Id = invenTOYOTA.id;
        wrrapperItem.numeroOrden = 'TES2020';
        wrrapperItem.IdInventarioLead = 'QWERTYU56789021';
        wrrapperItem.Model_Year = '2020';
        wrrapperItem.Tipo_Inventario = 'Piso';
        wrrapperItem.Toms_Series_Name  = 'RAV 4';
        wrrapperItem.TAM_Version = 'SPORT';
        wrrapperItem.TAM_DiasPiso = '10';
        wrrapperItem.Model_Number = '1790';
        wrrapperItem.codigoColorInterior = 'ffff';
        wrrapperItem.descripcionColorInterior = 'Red';
        wrrapperItem.codigoColorExterior = 'AF12';
        wrrapperItem.descripcionColorExterior = 'Lunar Rock';
        
        //Lead  
        Lead lead = [Select id from Lead Where Email = 'a@a.com'];
        TAM_SearchInventarioLead.saveRecordInventario(wrrapperItem,lead.id);
        
        Test.stopTest();
        
    }
    
    @isTest static void testMethod5(){
        Test.startTest();
        TAM_LeadInventarios__c idLeadInve = [Select id from TAM_LeadInventarios__c WHERE TAM_VINFacturacion__c = 'QWERTYU56789023'];
        TAM_SearchInventarioLead.deleteRecordInventario(idLeadInve.id);
        Test.stopTest();
        
    }
    
    @isTest static void testMethod6(){
        Test.startTest();
        TAM_LeadInventarios__c idLeadInve = [Select id from TAM_LeadInventarios__c WHERE TAM_VINFacturacion__c = 'QWERTYU56789023'];
        TAM_SearchInventarioLead.estatusLead(idLeadInve.id);
        Test.stopTest();
        
    }
}