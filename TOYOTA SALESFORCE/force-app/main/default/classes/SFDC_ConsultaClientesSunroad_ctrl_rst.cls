/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Publicaciín del WS con tecnologia REST para la consulta de Propectos 
                        que se crearon o modificaron en la fecha actual

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Septiembre-2021   Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class SFDC_ConsultaClientesSunroad_ctrl_rst {

    //Un constructor por default
    public SFDC_ConsultaClientesSunroad_ctrl_rst(){}
    
    //Ahora Crea el llamado para la serializacion JSON.deserialize
    
    public class responsWebService{
        public List<DatosProspecto> listaClientesSFDC;  
        public RespCallWebService respCallWebService;
    }
        
    public class DatosProspecto{    
        //Datos del cliente    
        //public String noConsecutivo {get;set;}      //Id            00Q5d000022e788EAA
        public String IdProspectoSFDC {get;set;}      //Id            00Q5d000022e788EAA
        public String distribuidor {get;set;}         //TAM_CodDistribuidorUsr__c, FWY_Distribuidor_f__c,  57614 - Toyota La Paz
        public String fechaCreacionHora {get;set;}    //CreatedDate   2020-10-01T00:00:00Z
        public String nombreCompleto {get;set;}       //FirstName, LastName, ApellidoMaterno__c 
        public String email {get;set;}                //Email
        public String telefono {get;set;}             //Phone
        public String gpoOrigenCandidato {get;set;}   //TAM_CatalogoOrigenes__c
        public String origenCandidato {get;set;}      //LeadSource
        public String estadoCandidato {get;set;}      //Status
        public String tipoRegistroCandidato {get;set;} //RecordType.Name
        public String asesor {get;set;}               //TAM_PropUsrName__c        
        
        //Otros datos del candidato
        public String fechaHora1erContacto {get;set;} //TAM_FechaPrimerContacto__c o TAM_FechaPrimerContactoBdc__c
        public String fechaCotizacion {get;set;}      //TAM_FechaCotizacion__c
        public String fecha1erCitaCumplida {get;set;} //Validar***
        public String totalCitasAgendadas {get;set;}  //Validar***
        public String totalCitasAsistidas {get;set;}  //Validar***
        public String fechaPruebaManejo {get;set;}    //TAM_FechaPruebaManejo__c
        public String fechaProspectoCaliente {get;set;} //TAM_FechaProspCal__c
        public String fechaApartadoEnganche {get;set;} //TAM_FechaPedidoProc__c
        public String fechaSolicitud {get;set;}       //TAM_FechaSolicitud__c
        public String fechaPrecalificacion {get;set;} //TAM_FechaPrecalificado__c
        public String autoACuenta {get;set;}          //TAM_AutoCuenta__c SI o NO
        //public String fechaAvaluoAutoActual {get;set;}//Validar***
        public String marcaAutoActual {get;set;}      //Validar 
        //public String versionAutoActual {get;set;}    //Validar 
        //public String anioModeloAutoActual {get;set;} //Validar 
        public String fechaProspectoPerdido {get;set;} //TAM_FechaBaja__c 
        public String causaProspectoPerdido {get;set;} //FWY_Estatus__c y FWY_motivo__c 
        public String fechaPedidoProceso {get;set;}    //TAM_FechaPedidoProc__c 
        public String formaAdquisicion {get;set;}     //Forma_de_adquisici_n__c 
        public String formaPagoDefinitiva {get;set;}  //TAM_FormaPagoDefinitiva__c 
        public String fechaEntregaUnidad {get;set;}   //TAM_FechaEntregado__c
        public String conteo {get;set;}               //Default 1

        //Datos del vehiculo 
        public String vehiculo {get;set;}             //FWY_Vehiculo__c
        public String version {get;set;}              //LeadInventario__c
        public String anioModelo {get;set;}           //LeadInventario__c
        public String fechaFacturacion {get;set;}     //TAM_leadInventario__r.TAM_FechaFacturacion__c o TAM_FechaFacturado__c o TAM_FechaFacturacionDealer__c 
        public String vinFacturado {get;set;}         //TAM_leadInventario__r.TAM_VINFacturacion__c, 
        //public String uIDFacturado {get;set;}         //TAM_leadInventario__r.AM_uIDFactura__c, 

    }

    public class DatosEntrada{
        public String IdUnicoUserSFDC {get;set;}
        public String NumeroDealer {get;set;}
        public Boolean blnTodosDistRel {get;set;}
        public String strFechaIniCons {get;set;}
        public String strFechaFinCons {get;set;}
    }   

    public class RespCallWebService{
	    public String NombreMetodo {get;set;}
	    public Boolean Error {get;set;}
	    public String Detalle {get;Set;}
    }   

    //El metodo que va a consultar los datos de los clientes que ya estan en la etapa de pedido en proceso y 
    public static String getClientesSunroad(String sBody){ 
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesSunroad sBody: ' + sBody);
        String sListaClientesSunroad = '';
        Integer intCntReg = 1;
        
        List<DatosProspecto> lDatosProspecto = new List<DatosProspecto>();
        DatosEntrada objDatosEntradaPaso = (DatosEntrada)JSON.deserialize(sBody, DatosEntrada.class);
        Date dtFechaConsIni = convierteFechaAAMMDDtoDate(objDatosEntradaPaso.strFechaIniCons);
        Date dtFechaConsFin = convierteFechaAAMMDDtoDate(objDatosEntradaPaso.strFechaFinCons);
        
        RespCallWebService objRespCallWebService = new RespCallWebService();
        objRespCallWebService.NombreMetodo = 'Consulta Clientes Sunroad';
        objRespCallWebService.Error = false;
        objRespCallWebService.Detalle = '';

        //Crea el objeto del tipo responsWebService
        responsWebService objResponsWebService = new responsWebService();
        objResponsWebService.listaClientesSFDC = lDatosProspecto;
        objResponsWebService.respCallWebService = objRespCallWebService;
        
        StrIng sCodDist = '';
        String[] arrDistRel = new List<String>();
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar objDatosEntradaPaso: ' + objDatosEntradaPaso);
        
        try{        
        //Consulta el reistro de las integraciones 
        for (TAM_Integraciones__c onjIntegraCons : [Select t.id, t.TAM_TipoIntegreacion__c, 
                TAM_Distribuidor__r.Codigo_Distribuidor__c, TAM_DistribuidoresRelacionados__c 
                From TAM_Integraciones__c t where TAM_Distribuidor__r.Codigo_Distribuidor__c =: objDatosEntradaPaso.NumeroDealer
                And TAM_TokenSFDC__c = :objDatosEntradaPaso.IdUnicoUserSFDC ]){
            //Inicializa el obieto de datosIntegra
            String[] arrDistRelCons = onjIntegraCons.TAM_DistribuidoresRelacionados__c != null ? (onjIntegraCons.TAM_DistribuidoresRelacionados__c.contains(';') ? onjIntegraCons.TAM_DistribuidoresRelacionados__c.split(';') : new List<String>{onjIntegraCons.TAM_DistribuidoresRelacionados__c}) : new List<String>();
            System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar arrDistRelCons: ' + arrDistRelCons);
            //Recotrre la lista de arrDistRelCons
            for (String sDistRelPaso : arrDistRelCons){
                String[] arrDistRelFinal = sDistRelPaso.split('-');
                System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar arrDistRelFinal: ' + arrDistRelFinal);
                arrDistRel.add(arrDistRelFinal[0].trim());    
            }//Fin del for para arrDistRelCons
        }
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar arrDistRel: ' + arrDistRel);

        //Consulta todos los reistros que estan en estapa de Pedido en Proceso
        for (Lead candidatoFacturar : [Select Id, TAM_CodDistribuidorUsr__c, FWY_Distribuidor_f__c,
            CreatedDate, FirstName, LastName, FWY_ApellidoMaterno__c, Email, Phone, FWY_Vehiculo__c, 
            TAM_CatalogoOrigenes__r.TAM_Grupo__c, LeadSource, RecordType.Name, TAM_PropUsrName__c, Status,
            TAM_FechaPrimerContacto__c, TAM_FechaPrimerContactoBdc__c, TAM_FechaCotizacion__c,
            TAM_FechaPruebaManejo__c, TAM_FechaProspCal__c, TAM_FechaSolicitud__c, TAM_FechaPrecalificado__c,
            TAM_AutoCuenta__c, TAM_FechaBaja__c, FWY_Estatus__c, FWY_motivo__c, TAM_FechaPedidoProc__c,
            Forma_de_adquisici_n__c, TAM_FormaPagoDefinitiva__c, TAM_FechaEntregado__c, TAM_EstatusBaja__c,
            TAM_FechaCitaGenerada__c, TAM_BdcGenerada__c, TAM_Marca__c, TAM_BdcConcretada__c, 
            TAM_DejoApartadoEnganche__c,           
            //Datos del inventario seleccionado
            (Select ID, TAM_Serie__c, TAM_Version__c, TAM_FechaFacturacion__c, TAM_VINFacturacion__c, TAM_AnioModelo__c,
                TAM_uIDFactura__c From TAM_LeadInventProspecto__r )           
            From Lead 
            Where (
                (TAM_FechaCreacionFormula__c >= :dtFechaConsIni And TAM_FechaCreacionFormula__c <= :dtFechaConsFin) 
                //OR (TAM_FechaUltModForm__c >= :dtFechaConsIni And TAM_FechaUltModForm__c <= :dtFechaConsFin)
            ) And FWY_codigo_distribuidor__c IN:arrDistRel]){
            //Ahora si Crear el objeto del tipo DatosProspecto
            DatosProspecto objDatosProspectoPaso = new DatosProspecto();
            
            String sCreatedDate = candidatoFacturar.CreatedDate.Date() + ' ' + candidatoFacturar.CreatedDate.time();
	        String snombreCompleto = (candidatoFacturar.FirstName != null ? candidatoFacturar.FirstName : '') + ' ' + (candidatoFacturar.LastName != null ? candidatoFacturar.LastName : '') + ' ' + (candidatoFacturar.FWY_ApellidoMaterno__c != null ? candidatoFacturar.FWY_ApellidoMaterno__c : '');

            //objDatosProspectoPaso.noConsecutivo = String.valueOf(intCntReg);
	        objDatosProspectoPaso.IdProspectoSFDC = candidatoFacturar.ID; //Id            00Q5d000022e788EAA
	        objDatosProspectoPaso.distribuidor = candidatoFacturar.TAM_CodDistribuidorUsr__c + ' - ' + candidatoFacturar.FWY_Distribuidor_f__c ;   // 57614 - Toyota La Paz
	        objDatosProspectoPaso.fechaCreacionHora = convierteFechaDDMMAAHHMMSS(candidatoFacturar.CreatedDate, true); //sCreatedDate;//CreatedDate   2020-10-01T00:00:00Z
	        objDatosProspectoPaso.nombreCompleto = snombreCompleto; //FirstName, LastName, ApellidoMaterno__c 
	        objDatosProspectoPaso.email = candidatoFacturar.Email;  //Email
	        objDatosProspectoPaso.telefono = candidatoFacturar.Phone;       //Phone
	        objDatosProspectoPaso.gpoOrigenCandidato = candidatoFacturar.TAM_CatalogoOrigenes__r.TAM_Grupo__c;
            objDatosProspectoPaso.origenCandidato = candidatoFacturar.LeadSource;
            objDatosProspectoPaso.estadoCandidato = candidatoFacturar.Status;
	        objDatosProspectoPaso.tipoRegistroCandidato = candidatoFacturar.RecordType.Name;
	        objDatosProspectoPaso.asesor = candidatoFacturar.TAM_PropUsrName__c;
            
            String sfechaHora1erContacto = candidatoFacturar.TAM_FechaPrimerContacto__c != NULL ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.CreatedDate, true) : (candidatoFacturar.TAM_FechaPrimerContactoBdc__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaPrimerContactoBdc__c, true) : '' );
	        objDatosProspectoPaso.fechaHora1erContacto = sfechaHora1erContacto; //TAM_FechaPrimerContacto__c o TAM_FechaPrimerContactoBdc__c
	        objDatosProspectoPaso.fechaCotizacion = candidatoFacturar.TAM_FechaCotizacion__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaCotizacion__c, false) : ''; // String.valueOf(candidatoFacturar.TAM_FechaCotizacion__c)  TAM_FechaCotizacion__c
	        objDatosProspectoPaso.fecha1erCitaCumplida = candidatoFacturar.TAM_FechaCitaGenerada__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaCitaGenerada__c, false) : ''; //String.valueOf(candidatoFacturar.TAM_FechaCitaGenerada__c)  candidatoFacturar.   //Validar***
	        objDatosProspectoPaso.totalCitasAgendadas = candidatoFacturar.TAM_BdcGenerada__c ? 'SI' : 'NO'; //Validar***
	        objDatosProspectoPaso.totalCitasAsistidas = candidatoFacturar.TAM_BdcConcretada__c ? 'SI' : 'NO'; //Validar***
	        objDatosProspectoPaso.fechaPruebaManejo = candidatoFacturar.TAM_FechaPruebaManejo__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaPruebaManejo__c, false) : ''; //String.valueOf(candidatoFacturar.TAM_FechaPruebaManejo__c)  TAM_FechaPruebaManejo__c
	        objDatosProspectoPaso.fechaProspectoCaliente = candidatoFacturar.TAM_FechaProspCal__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaProspCal__c, false) : ''; //String.valueOf(candidatoFacturar.TAM_FechaProspCal__c)  TAM_FechaProspCal__c
	        objDatosProspectoPaso.fechaApartadoEnganche = candidatoFacturar.TAM_DejoApartadoEnganche__c ? 'SI' : 'NO';  //TAM_FechaPedidoProc__c
	        objDatosProspectoPaso.fechaSolicitud = candidatoFacturar.TAM_FechaSolicitud__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaSolicitud__c, false) : ''; //String.valueOf(candidatoFacturar.TAM_FechaSolicitud__c)  TAM_FechaSolicitud__c
	        objDatosProspectoPaso.fechaPrecalificacion = candidatoFacturar.TAM_FechaPrecalificado__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaPrecalificado__c, false) : ''; //String.valueOf(candidatoFacturar.TAM_FechaPrecalificado__c)  TAM_FechaPrecalificado__c
	        objDatosProspectoPaso.autoACuenta = candidatoFacturar.TAM_AutoCuenta__c ? 'SI' : 'NO';            //TAM_AutoCuenta__c SI o NO
	        //objDatosProspectoPaso.fechaAvaluoAutoActual = ''; //candidatoFacturar.  //Validar***
	        objDatosProspectoPaso.marcaAutoActual = candidatoFacturar.TAM_Marca__c != null ? candidatoFacturar.TAM_Marca__c : '';  //candidatoFacturar.        //Validar 
	        //objDatosProspectoPaso.versionAutoActual = '';  //candidatoFacturar.      //Validar 
	        //objDatosProspectoPaso.anioModeloAutoActual = '';  //candidatoFacturar.   //Validar 
	        objDatosProspectoPaso.fechaProspectoPerdido = candidatoFacturar.TAM_FechaBaja__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaBaja__c, false) : '';  //String.valueOf(candidatoFacturar.TAM_FechaBaja__c)  TAM_FechaBaja__c 
	        if ( candidatoFacturar.FWY_Estatus__c != null && candidatoFacturar.FWY_motivo__c != null)
	           objDatosProspectoPaso.causaProspectoPerdido = candidatoFacturar.FWY_motivo__c != null ? candidatoFacturar.FWY_motivo__c : ''; // (candidatoFacturar.FWY_Estatus__c != null ? candidatoFacturar.FWY_Estatus__c : '') + ' ' + (candidatoFacturar.FWY_motivo__c != null ? candidatoFacturar.FWY_motivo__c : '');   //FWY_Estatus__c y FWY_motivo__c 
	        else 
               objDatosProspectoPaso.causaProspectoPerdido = '';  
	        objDatosProspectoPaso.fechaPedidoProceso = candidatoFacturar.TAM_FechaPedidoProc__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaPedidoProc__c, false) : '';     //String.valueOf(candidatoFacturar.TAM_FechaPedidoProc__c)  TAM_FechaPedidoProc__c 
	        objDatosProspectoPaso.formaAdquisicion = candidatoFacturar.Forma_de_adquisici_n__c != null ? candidatoFacturar.Forma_de_adquisici_n__c : '';     //Forma_de_adquisici_n__c 
	        objDatosProspectoPaso.formaPagoDefinitiva = candidatoFacturar.TAM_FormaPagoDefinitiva__c != null ? candidatoFacturar.TAM_FormaPagoDefinitiva__c : '';    //TAM_FormaPagoDefinitiva__c 
	        objDatosProspectoPaso.fechaEntregaUnidad = candidatoFacturar.TAM_FechaEntregado__c != null ? convierteFechaDDMMAAHHMMSS(candidatoFacturar.TAM_FechaEntregado__c, false) : '';     //String.valueOf(candidatoFacturar.TAM_FechaEntregado__c)  TAM_FechaEntregado__c
	        objDatosProspectoPaso.conteo = '1' ;   //Default 1
	        
	        //Datos del Vehiculo
	        objDatosProspectoPaso.vehiculo = candidatoFacturar.FWY_Vehiculo__c != null ? candidatoFacturar.FWY_Vehiculo__c : '';       //FWY_Vehiculo__c

            objDatosProspectoPaso.version = '';
            objDatosProspectoPaso.anioModelo = '';
            objDatosProspectoPaso.fechaFacturacion = '';
            objDatosProspectoPaso.vinFacturado = '';
            //objDatosProspectoPaso.uIDFacturado = '';
            
            //Toma la lista de vines asociados y recorrela para incializar los campos
            if (!candidatoFacturar.TAM_LeadInventProspecto__r.isEmpty()){
                for ( TAM_LeadInventarios__c objLeadInventarios : candidatoFacturar.TAM_LeadInventProspecto__r){
                    //Inicializa los c ampos de los vines asociados
                    objDatosProspectoPaso.version = objLeadInventarios.TAM_Version__c != null ? objLeadInventarios.TAM_Version__c : '';
                    objDatosProspectoPaso.anioModelo = objLeadInventarios.TAM_AnioModelo__c != null ? objLeadInventarios.TAM_AnioModelo__c : '';
                    objDatosProspectoPaso.fechaFacturacion = objLeadInventarios.TAM_FechaFacturacion__c != null ? convierteFechaDDMMAAHHMMSS(objLeadInventarios.TAM_FechaFacturacion__c, false) : ''; //String.valueOf(objLeadInventarios.TAM_FechaFacturacion__c) 
                    objDatosProspectoPaso.vinFacturado = objLeadInventarios.TAM_VINFacturacion__c != null ? objLeadInventarios.TAM_VINFacturacion__c : '';
                    //objDatosProspectoPaso.uIDFacturado = objLeadInventarios.TAM_uIDFactura__c != null ? objLeadInventarios.TAM_uIDFactura__c : '';
                }//Fin del for para candidatoFacturar.TAM_LeadInventProspecto__r                
            }//Fin si candidatoFacturar.TAM_LeadInventProspecto__r.isEmpty()
            
            //Agregalo a la lista de lDatosProspecto
            lDatosProspecto.add(objDatosProspectoPaso);
            
            //Sumale uno al contador de reg
            intCntReg++;
                
        }//Fin del for para la lista de Prospectos

        /*sListaClientesSunroad = '{"listaClientesSFDC":';        
        //Ahora si serializa la lista de lDatosProspecto
        sListaClientesSunroad += JSON.serialize(lDatosProspecto);
        sListaClientesSunroad += '}';*/
        
        //Crea el objeto del tipo responsWebService
        objResponsWebService.listaClientesSFDC = lDatosProspecto;
        objResponsWebService.respCallWebService = objRespCallWebService;
                
        }catch(Exception ex){
            objRespCallWebService.Error = true;
            objRespCallWebService.Detalle = ex.getMessage() + ' Linea: ' + ex.getLineNumber();
            objResponsWebService.respCallWebService = objRespCallWebService;
        }

        //Serializa el objeto del tipo responsWebService
        sListaClientesSunroad = JSON.serialize(objResponsWebService);
        
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesSunroad sListaClientesSunroad: ' + sListaClientesSunroad);        
        
        //Regresa la lista en JSON
        return sListaClientesSunroad;
        
    }

    public static String convierteFechaDDMMAAHHMMSS(DateTime dtFechaPrm, Boolean blnFechayHoraPrn){
        System.debug('EN SFDC_ConsultaClientesSunroad_ctrl_rst.convierteFechaDDMMAAHHMMSS dtFechaPrm: ' + dtFechaPrm);

        //DateTime dtFechaAct = DateTime.now();        
        String sFechaPasoFinal = '';
        
        //La fecha
        String sAnio = String.valueOf(dtFechaPrm.year());
        String sMes = String.valueOf(dtFechaPrm.month());
        String sDia = String.valueOf(dtFechaPrm.day());
        //La hora        
        String sHora = String.valueOf(dtFechaPrm.hour());
        String sMinut = String.valueOf(dtFechaPrm.minute());
        String sSegund = String.valueOf(dtFechaPrm.second());
        
        String sFechaPaso = ( Integer.valueOf(sDia) < 10 ? '0' + sDia : sDia) + '/' + ( Integer.valueOf(sMes) < 10 ? '0' + sMes : sMes) + '/' + sAnio;
        if (blnFechayHoraPrn)
            sFechaPaso += ' ' + ( Integer.valueOf(sHora) < 10 ? '0' + sHora : sHora) + ':' + ( Integer.valueOf(sMinut) < 10 ? '0' + sMinut : sMinut) + ':' + ( Integer.valueOf(sSegund) < 10 ? '0' + sSegund : sSegund);            
        System.debug('EN SFDC_ConsultaClientesSunroad_ctrl_rst.convierteFechaDDMMAAHHMMSS sFechaPaso: ' + sFechaPaso);
       
        //Regresa la fecha en formato string 
        return sFechaPaso;
    }

    public static Date convierteFechaAAMMDDtoDate(String strFechaPrm){
        System.debug('EN SFDC_ConsultaClientesSunroad_ctrl_rst.convierteFechaAAMMDDtoDate strFechaPrm: ' + strFechaPrm);

        Date dtFechaPasoCons = Date.today();
        
        //Ve si tiene algo el prm de strFechaPrm
        if (strFechaPrm.trim() != null && strFechaPrm.trim() != ''){
            //Separa la fecha por el caracter '-'
            String[] arrFechaPrm = strFechaPrm.trim().split('-');
            Date dtFechaFinalPaso = Date.newInstance(Integer.valueOf(arrFechaPrm[0]), Integer.valueOf(arrFechaPrm[1]), Integer.valueOf(arrFechaPrm[2]));
            //Inicializa
            dtFechaPasoCons = dtFechaFinalPaso; 
        }//Fin si strFechaPrm.trim() != null && strFechaPrm.trim() != ''        
        System.debug('EN SFDC_ConsultaClientesSunroad_ctrl_rst.convierteFechaAAMMDDtoDate dtFechaPasoCons: ' + dtFechaPasoCons);
       
        //Regresa la fecha en formato string 
        return dtFechaPasoCons;
    }

    
}