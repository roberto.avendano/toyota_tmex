global class TAM_BatchClassUltimoMov implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Datetime fechaHoy = date.today();
        String query = 'SELECT Id,Name,Ultimo_Movimiento__c,Ultimo_Movimiento__r.Submitted_Date__c,Ultimo_Movimiento__r.Submitted_Time__c FROM Vehiculo__c where Total_de_Movimientos__c > 1 AND LastModifiedDate  >=: fechaHoy';
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<Vehiculo__c> vinList) {
		
        Id recordTypeMov =  Schema.SObjectType.Movimiento__c.getRecordTypeInfosByDeveloperName().get('Movimiento').getRecordTypeId();
        Set<String> listaVIN = new Set<String>();
        List<Movimiento__c> movList = new List<Movimiento__c>();
        Map<String,Vehiculo__c> mapVIN = new map<String,Vehiculo__c> ();
        for(Vehiculo__c vin : vinList) {         
            listaVIN.add(vin.name);
            mapVIN.put(vin.name,vin);
        } 
        
        if(!listaVIN.isEmpty()){
            movList = [Select id,name,VIN__c,VIN__r.name,Submitted_Date__c,Submitted_Time__c,Trans_Type__c From Movimiento__c WHERE
                       VIN__r.name IN : listaVIN  and recordtypeId =:recordTypeMov  order by Submitted_Date__c desc, Submitted_Time__c desc];
            
        }
        
        List<Movimiento__c> movPorVIN = new List<Movimiento__c>();
        Map<String,List<Movimiento__C>> mapMovVIN = new Map<String,List<Movimiento__C>>();
        
        if(!movList.isEmpty()){
            for(Vehiculo__c vin : vinList) {  
                List<Movimiento__C> movPorVIN2 = new List<Movimiento__C>(); 
                for(Movimiento__C mov : movList){
                    if(vin.name == mov.VIN__r.name){
                        Vehiculo__c vins = mapVIN.get(mov.vin__r.name);
                        if(mov.vin__r.name == vins.name){
                            movPorVIN2.add(mov);
                        }
                        	
                        mapMovVIN.put(vin.name,movPorVIN2); 
                        
                    }
                    
                    
                }
            }
            
        }
        
        
        //Tenemos lista de movimientos por cada unos de los VINES
        Map<id,Vehiculo__C> mapVINUPD = new  Map<id,Vehiculo__C> ();
        List<Vehiculo__c> listaVINUpd = new List<Vehiculo__c> ();
        for(Vehiculo__c vinUpd : vinList){
            if(mapMovVIN.containsKey(vinUpd.name)){
                List<Movimiento__c> movListaVIN = mapMovVIN.get(vinUpd.name);
                system.debug('VIN'+vinUpd.NAME+'fecha envio :'+movListaVIN[0].name);
                Vehiculo__c recordVIN = new Vehiculo__c();
                recordVIN.id = vinUpd.id;
                recordVIN.Ultimo_Movimiento__c = movListaVIN[0].id;
                listaVINUpd.add(recordVIN);
                mapVINUPD.put(vinUpd.id,recordVIN);
                
            }
            
            
        }
        
        try {
            system.debug('lista VIN'+listaVINUpd);
            update mapVINUPD.values();
            
        } catch(Exception e) {
            System.debug(e);
        }
        
    }   
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
}