/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_UpdInvAdoVehiToyBch y
                        eliminar los registros del objeto TAM_LeadInventarios__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Febrero-2021      Héctor Figueroa             Creación
******************************************************************************* */

global class TAM_UpdInvAdoVehiToyBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_UpdInvAdoVehiToyBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_UpdInvAdoVehiToyBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_LeadInventarios__c> scope){
        System.debug('EN TAM_UpdInvAdoVehiToyBch.');
        
        List<TAM_LeadInventarios__c> lLeadInvDelFin = new List<TAM_LeadInventarios__c>();
        List<TAM_LeadInventarios__c> lLeadInvDel = new List<TAM_LeadInventarios__c>();
        Map<String, String> mapIdExtIdLead = new Map<String, String>();
        Map<String, String> mapIdExtIdLeadInv = new Map<String, String>();
        
        List<TAM_InventarioVehiculosToyota__c> lInvVehToyUpd = new List<TAM_InventarioVehiculosToyota__c>();
        Set<String> setVinInvVen = new Set<String>();
        
        Savepoint sp = Database.setSavepoint();        

        //TAM_Idexterno__c TAM_PedidoFacturado__c, TAM_PedidoReportado__c                
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_LeadInventarios__c objInvVehiToy : scope){
            //Crea el objeto del tipo TAM_LeadInventarios__c
            TAM_LeadInventarios__c OppPaso = new TAM_LeadInventarios__c(id = objInvVehiToy.id); 
            //Toma el campo de y nmetelo al set de mapIdExtIdLead
            String[] lIdexterno = objInvVehiToy.TAM_Idexterno__c.contains('-') ? objInvVehiToy.TAM_Idexterno__c.split('-') : new List<String>();
            //Ve si la lista es mayor a 1
            if (lIdexterno.size() > 1){
                mapIdExtIdLead.put(objInvVehiToy.TAM_Idexterno__c, lIdexterno[0]);
                mapIdExtIdLeadInv.put(objInvVehiToy.TAM_Idexterno__c, objInvVehiToy.id);
            }//Fin si lIdexterno.size() > 1
            if (objInvVehiToy.Name != null)
                setVinInvVen.add(objInvVehiToy.Name);
        }
        System.debug('EN TAM_UpdInvAdoVehiToyBch mapIdExtIdLead: ' + mapIdExtIdLead);
        System.debug('EN TAM_UpdInvAdoVehiToyBch setVinInvVen: ' + setVinInvVen);

        //Ya tienes los id de leads que vas a eliminar buscalos en Lead
        for (Lead objLead : [Select id, Name From Lead Where ID IN :mapIdExtIdLead.values()
            And TAM_PedidoFacturado__c = false AND TAM_PedidoReportado__c = false]){
            //Metelos al mapa de mapIdExtIdLeadInv y busca el objLead.id
            for (String sIdExtPaso : mapIdExtIdLeadInv.KeySet()){
                String[] lIdexterno = sIdExtPaso.contains('-') ? sIdExtPaso.split('-') : new List<String>();
                //Ve si la lista es mayor a 1
                if (lIdexterno.size() > 1){
                    if (lIdexterno[0] == objLead.id){
                        lLeadInvDelFin.add(new TAM_LeadInventarios__c(Id = mapIdExtIdLeadInv.get(sIdExtPaso)));
                        breaK;
                    }//Fin si lIdexterno[0] == objLead.id
                }//Fin si lIdexterno.size() > 1
            }//Fin del for para mapIdExtIdLeadInv.KeySet()
        }//Fin del for para Lead
        System.debug('EN TAM_UpdInvAdoVehiToyBch lLeadInvDelFin: ' + lLeadInvDelFin);
        
        //Consulta los Vines de setVinInvVen en 
        for (TAM_InventarioVehiculosToyota__c objInv : [Select t.id From TAM_InventarioVehiculosToyota__c t
             Where Name IN :setVinInvVen]){
             //Si existe apartalo objInv
             lInvVehToyUpd.add(new TAM_InventarioVehiculosToyota__c(id = objInv.id, TAM_Apartado__c = false));
        }
        System.debug('EN TAM_UpdInvAdoVehiToyBch lInvVehToyUpd: ' + lInvVehToyUpd);
        
        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lLeadInvDelFin.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Deleteresult> lDtbUpsRes = Database.delete(lLeadInvDelFin, false);
            //Ve si hubo error
            for (Database.Deleteresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_UpdInvAdoVehiToyBch Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                //if (objDtbUpsRes.isSuccess())
                //    System.debug('EN TAM_UpdInvAdoVehiToyBch Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lInvVehToyUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.SaveResult> lDtbUpsRes = Database.update(lInvVehToyUpd);
            //Ve si hubo error
            for (Database.SaveResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_UpdInvAdoVehiToyBch Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                //if (objDtbUpsRes.isSuccess())
                //    System.debug('EN TAM_UpdInvAdoVehiToyBch Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
        
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_UpdInvAdoVehiToyBch.finish Hora: ' + DateTime.now());        
    } 
    
}