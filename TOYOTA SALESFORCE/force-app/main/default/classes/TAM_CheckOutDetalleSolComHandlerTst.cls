/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CheckOutDetalleSolComHandlerTst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    
    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
    
    static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

    static String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
        
    static String sListaPreciosCutom = getCustomPriceBookList();
    
    static  Account clienteDealer = new Account();
    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
    static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
    static  Rangos__c rango = new Rangos__c();
    static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
    static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
    static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();
    
    @TestSetup static void loadData(){
        
        Group grupoPrueba = new Group(
            Name = 'CARSON',
            Type = 'Regular'
        );
        insert grupoPrueba;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;

        Rangos__c rangoPrograma = new Rangos__c(
            Name = 'UBER',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'UBER | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
             
        );
        insert rangoPrograma;
        
        Account clienteDealer = new Account(
            Name = 'TOYOTA PRUEBA',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoDistribuidor,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteDealer;

        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
            LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaFisica,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  

        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod;  
        CatalogoCentMod = catCenMod; 
        
        CatalogoCentralizadoModelos__c catCenMod2 = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '11',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod2;  
        CatalogoCentMod2 = catCenMod2;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        Product2 ProducStdPriceBook = new Product2(
                Name = '22060',
                Anio__c = '2020', 
                NombreVersion__c = 'LE MT',
                IdExternoProducto__c = '220602020',
                ProductCode = '220602020',
                Description= 'AVANZA-22060-2020-B79-10-LE AT', 
                RecordTypeId = sRectorTypePasoProductoUnidad,
                Family = 'Toyota',
                IsActive = true
        );
        insert ProducStdPriceBook;
        
        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id = standardPricebookId,
            UnitPrice = 0.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true,
            IdExterno__c = '220602020'
        );
        insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
        
        PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
            Pricebook2Id = customPB.id,
            UnitPrice = 1.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true
        );
        insert pbeCustomProceBookEntry;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id
        );
        insert solVentaFlotillaPrograma;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma2 = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'Cerrada',
            RecordTypeId = sRectorTypePasoSolProgramaCerrada,
            TAM_ProgramaRango__c = rangoPrograma.id,
            TAM_FechaCierreSolicitud__c = Date.today()                      
        );
        insert solVentaFlotillaPrograma2;
        
        Date dtFechaActual = Date.today();
        Date dtFechaFin = dtFechaActual.addDays(2);
        String strAnioActual = String.valueOf(dtFechaActual.year());
        //Un objeto para TAM_CalendarioToyota__c
        TAM_CalendarioToyota__c objCalendarioToyota = new TAM_CalendarioToyota__c(
            Name = 'Julio 2021'
            , TAM_FechaInicio__c = Date.today()
            , TAM_FechaFin__c = dtFechaFin
            , TAM_Anio__c = strAnioActual
        );
        System.debug('EN TAM_CheckOutDetalleSolComHandlerOK3 objCalendarioToyota: ' + objCalendarioToyota);        
        insert objCalendarioToyota;

        //Un vin para el Checkout        
        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Intercambio__c = false,
            TAM_VIN__c = 'XXXXXXXXXX1'
        );
        insert objCheckOutDetSolComp;                         

        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp2 = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma2.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Intercambio__c = false,
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_EstatusDealerSolicitud__c = 'Cerrada'
        );
        insert objCheckOutDetSolComp2;                         
        
        String sNameSol;
        //Selecciona el nombre del la solicitud solVentaFlotillaPrograma
        for (TAM_SolicitudesFlotillaPrograma__c objSolPaso : [Select id, Name From TAM_SolicitudesFlotillaPrograma__c
            Where id =:solVentaFlotillaPrograma.id] ){
            sNameSol = objSolPaso.Name;
        }
        
        String sIdExterno = 'XXXXXXXXXX1' + '-' + sNameSol  + '-' + String.valueOf(Date.today()) + '-' + sRectorTypeCheckOutInv;
        System.debug('EN TAM_CheckOutDetalleSolComHandlerOK sIdExterno: ' + sIdExterno);        

        TAM_MovimientosSolicitudes__c objMovimientosSolicitudes = new TAM_MovimientosSolicitudes__c(
            Name = sNameSol + '-' + String.valueOf(Date.today())
            ,TAM_IdExternoSFDC__c = sIdExterno
            , TAM_VIN__c = 'XXXXXXXXXX1'
            , TAM_Total__c = 0
            , TAM_FechaMovimiento__c = Date.today()
            , TAM_CheckOutSolicitud__c = objCheckOutDetSolComp.id
            , TAM_EstatusSolicitud__c = 'Autorizada'
        );
        System.debug('EN TAM_CheckOutDetalleSolComHandlerOK objMovimientosSolicitudes: ' + objMovimientosSolicitudes);        
        insert objMovimientosSolicitudes;
                                                    
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;        
    }

    static testMethod void TAM_CheckOutDetalleSolComHandlerOK() {
        
        Test.startTest();

            //Consulta los datos del cliente
            List<TAM_SolicitudesFlotillaPrograma__c> lSolFlotillaPrograma = [Select Id, Name From TAM_SolicitudesFlotillaPrograma__c LIMIT 2];                    
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK lSolFlotillaPrograma: ' + lSolFlotillaPrograma);

            //Consulta los datos del cliente
            List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutSolCompra = [Select Id, Name From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 1];                    
            //Actualiza los datos de la sol 
            lCheckOutSolCompra.get(0).TAM_TipoVenta__c = 'Inventario';
            lCheckOutSolCompra.get(0).TAM_VIN__c = 'XXXXXXXXXX1';
            lCheckOutSolCompra.get(0).TAM_EstatusDOD__c = 'Cancelado';
            lCheckOutSolCompra.get(0).TAM_EstatusDealerSolicitud__c = 'Cancelada';
            lCheckOutSolCompra.get(0).TAM_FechaCancelacion__c = Date.today();
            //Actualiza la sol
            update lCheckOutSolCompra.get(0);
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK lCheckOutSolCompra.get(0): ' + lCheckOutSolCompra.get(0));
                   
       Test.stopTest();
        
    }

    static testMethod void TAM_CheckOutDetalleSolComHandlerOK2() {
        
        Test.startTest();

            //Consulta los datos del cliente
            List<TAM_SolicitudesFlotillaPrograma__c> lSolFlotillaPrograma = [Select Id, Name, TAM_FechaCierreSolicitud__c From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];                    
            //Cierra la solicitud 
            lSolFlotillaPrograma.get(0).TAM_FechaCierreSolicitud__c = Date.today();
            lSolFlotillaPrograma.get(0).TAM_Estatus__c = 'Cerrada';
            //Actualiza la sol
            update lSolFlotillaPrograma.get(0);
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK lSolFlotillaPrograma: ' + lSolFlotillaPrograma);

            //Consulta los datos del cliente
            List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutSolCompra = [Select Id, Name From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 1];                    
            //Actualiza los datos de la sol 
            lCheckOutSolCompra.get(0).TAM_TipoVenta__c = 'Inventario';
            lCheckOutSolCompra.get(0).TAM_VIN__c = 'XXXXXXXXXX1';
            lCheckOutSolCompra.get(0).TAM_EstatusDOD__c = 'Cancelado';
            lCheckOutSolCompra.get(0).TAM_EstatusDealerSolicitud__c = 'Cancelada';
            lCheckOutSolCompra.get(0).TAM_FechaCancelacion__c = Date.today();
            lCheckOutSolCompra.get(0).TAM_SolicitudFlotillaPrograma__c = lSolFlotillaPrograma.get(0).id;
            //Actualiza la sol
            update lCheckOutSolCompra.get(0);
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK lCheckOutSolCompra.get(0): ' + lCheckOutSolCompra.get(0));

            //Consulta los datos del cliente
            List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutSolCompraCerrada = [Select Id, Name From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 2];                    
            //Actualiza los datos de la sol 
            lCheckOutSolCompraCerrada.get(1).TAM_TipoVenta__c = 'Inventario';
            lCheckOutSolCompraCerrada.get(1).TAM_VIN__c = 'XXXXXXXXXX1';
            lCheckOutSolCompraCerrada.get(1).TAM_EstatusDOD__c = 'Cerrada';
            lCheckOutSolCompraCerrada.get(1).TAM_EstatusDealerSolicitud__c = 'Cerrada';
            lCheckOutSolCompraCerrada.get(1).TAM_SolicitudFlotillaPrograma__c = lSolFlotillaPrograma.get(0).id;
            //Actualiza la sol
            update lCheckOutSolCompraCerrada.get(1);
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK lCheckOutSolCompraCerrada.get(0): ' + lCheckOutSolCompraCerrada.get(1));
                   
       Test.stopTest();
        
    }

    static testMethod void TAM_CheckOutDetalleSolComHandlerOK3() {
        
        Test.startTest();

            //Consulta los datos del cliente
            List<Account> lDistCons = [Select Id, Name From Account Where Name = 'TOYOTA PRUEBA' LIMIT 1];                    
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK3 lDistCons: ' + lDistCons);

            //Consulta los datos del cliente
            List<TAM_SolicitudesFlotillaPrograma__c> lSolFlotillaPrograma = [Select Id, Name, TAM_FechaCierreSolicitud__c From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];                    
            //Cierra la solicitud 
            lSolFlotillaPrograma.get(0).TAM_FechaCierreSolicitud__c = Date.today();
            lSolFlotillaPrograma.get(0).TAM_Estatus__c = 'Cerrada';
            //Actualiza la sol
            update lSolFlotillaPrograma.get(0);
            System.debug('EN TAM_CheckOutDetalleSolComHandlerOK3 lSolFlotillaPrograma: ' + lSolFlotillaPrograma);

	        List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
	        List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
	        List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
	        List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};
	        String VaRtVehic = Schema.SObjectType.Vehiculo__c.getRecordTypeInfosByName().get('Vehículo').getRecordTypeId();
	        String VaRtMov = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
	
	        DateTime dtFechaAct = DateTime.now();
	        DateTime dtFechaActFin = dtFechaAct.addDays(-10);
	        Time tmHoraAct = dtFechaAct.time();
	        
	        Vehiculo__c objVehiculoPaso = new Vehiculo__c(
	            Name = 'XXXXXXXXXX1',
	            AnioModel__c = '2020',
	            Id_Externo__c = 'XXXXXXXXXX1', //2020-AVANZA-22060-B79-11,
	            EstatusVINAutoDemo__c = 'H',                        
	            RecordTypeId = VaRtVehic,
	            //Se agregan estos campos al objeto de Vehiculo__c ***YA NO SE PROCESA DEDESDE AQUI*** 
	            //Modelo_b__c = mapModelos.containsKey(v.TAM_IdModelo__c) ? mapModelos.get(v.TAM_IdModelo__c).id : null,
	            //Producto__c = mapProduct2.containsKey(sIdProdcto) ? mapProduct2.get(sIdProdcto).id : null,
	            TAM_SERIAL__c = seriales.get(aleatorio(seriales.size())),
	            DescripcinColorExterno__c = 'Azul',               
	            TAM_FechaEnvio__c = dtFechaActFin
	            //ColorExternoVehiculo__c = 'B79', 
	            //ColorInternoVehiculo__c = '11'
	        );
	        insert objVehiculoPaso;
	        
	        String sIdExternoMov = 'XXXXXXXXXX1' + '-' + String.valueOf(dtFechaActFin); 
	        Movimiento__c objMovPaso = new Movimiento__c(
	            Id_Externo__c = sIdExternoMov,                     
	            RecordTypeId = VaRtMov,
	            VIN__c = objVehiculoPaso.Id,
	            Distribuidor__c = lDistCons.get(0).id, //'57052',
	            //Modelo__c = mapProduct2.containsKey(sIdModelo) ? mapProduct2.get(sIdModelo).Id : null,
	            Year_Modelo__c = '2020',
	            Serial__c = seriales.get(aleatorio(seriales.size())),                            
	            Trans_Type__c = 'RDR',
	            //Factura__c = v.TAM_Factura__c,
	            Sale_Code__c = '01',
	            Sale_Date__c = dtFechaActFin.date(),
	            Submitted_Date__c = dtFechaActFin.date(),
	            Submitted_Time__c = String.valueOf(tmHoraAct),
	            Sales_Manager__c = '001',
	            Ls_Assesor__c = '001',
	            //Agrega la parte de los colores                                                                                        
	            //ColorExterior__c = mapColorExt.containsKey(v.TAM_IdColorExt__c.right(3)) ? mapColorExt.get(v.TAM_IdColorExt__c.right(3)).id : null,                         
	            Descr_Color_Ext__c = 'Azul',
	            //ColorInterno__c = mapColorInt.containsKey(v.TAM_IdColorInt__c.right(2)) ? mapColorInt.get(v.TAM_IdColorInt__c.right(2)).id : null,
	            Descr_Color_Int__c = 'Azul'
	
	        );
	        insert objMovPaso;
                   
       Test.stopTest();
        
    }
}