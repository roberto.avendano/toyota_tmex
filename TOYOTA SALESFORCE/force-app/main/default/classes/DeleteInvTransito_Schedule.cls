global class DeleteInvTransito_Schedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        String query = 'SELECT Id, Name, CreatedDate FROM Inventario_en_Transito__c WHERE CreatedDate < TODAY';
        List<Inventario_en_Transito__c> inventariosHoy = this.getInventariosDeHoy();
        
        
        if(inventariosHoy.size() >= 0){
            DeleteTransito_Batch b = new DeleteTransito_Batch(query);
            database.executebatch(b);
        }
    }
    
    
    public List<Inventario_en_Transito__c> getInventariosDeHoy(){
        return [SELECT Id, Name, CreatedDate FROM Inventario_en_Transito__c WHERE CreatedDate = TODAY LIMIT 5];
        
    }
    
}