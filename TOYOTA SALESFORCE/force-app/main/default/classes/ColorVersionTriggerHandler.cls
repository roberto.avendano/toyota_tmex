public with sharing class ColorVersionTriggerHandler extends TriggerHandler{
	public List<Color_version__c> colorVersionList;

	public ColorVersionTriggerHandler() {
		this.colorVersionList = Trigger.new;
	}

	public override void afterInsert(){
		crearColoresSeleccionados(colorVersionList);
	}

	public override void afterUpdate(){
		crearColoresSeleccionados(colorVersionList);
	}

	private void crearColoresSeleccionados(List<Color_version__c> coloresVersion){
		List<ColorExterno__c> coloresExternos = [SELECT Id, Name, CodigoColor__c FROM ColorExterno__c];
		List<Color_Externo_Seleccionado__c> coloresSeleccionados = new List<Color_Externo_Seleccionado__c>();
		system.debug('Lista IN'+ coloresVersion);
        system.debug('Lista colores'+coloresExternos);
        
		if(coloresExternos.size() > 0){			
			for(Color_version__c cv: coloresVersion){
				for(ColorExterno__c ce: coloresExternos){
					coloresSeleccionados.add(new Color_Externo_Seleccionado__c(
						Color_version__c = cv.Id,
						ColorExterno__c = ce.Id,
						CodigoColor__c = cv.Name +'-'+ce.CodigoColor__c
					));
				}
			}
		}

		if(coloresSeleccionados.size() > 0){
			try{
                system.debug('lista de colores selc'+coloresSeleccionados);
				upsert coloresSeleccionados CodigoColor__c;

			} catch(Exception ex){
				System.debug(ex.getMessage());
			}
		}
	}
}