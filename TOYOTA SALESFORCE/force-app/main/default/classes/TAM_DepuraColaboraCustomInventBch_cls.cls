/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para eliminar la colaboración de los registros 
                        que vienen de la clases TAM_AdminVisibInventarioToyota.bpDepuraColaboracionDealer.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    08-Junio-2021        Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_DepuraColaboraCustomInventBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global string strCodDealer;
    global List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerPrm;
    global Boolean blnDepuraColaboraReglas;
    global Boolean blnDepuraGruposReglasRel;
    
    //Un constructor por default
    global TAM_DepuraColaboraCustomInventBch_cls(string query, string strCodDealer, 
        List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerPrm, Boolean blnDepuraColaboraReglas,
        Boolean blnDepuraGruposReglasRel){
        this.query = query;
        this.strCodDealer = strCodDealer;
        this.lAdminInventarioDealerPrm = lAdminInventarioDealerPrm;
        this.blnDepuraColaboraReglas = blnDepuraColaboraReglas;
        this.blnDepuraGruposReglasRel = blnDepuraGruposReglasRel;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraCustomInventBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_DepuraColaboraCustomInventBch_cls.');

        Map<String, Set<String>> mapCodDealerLstIdInv = new Map<String, Set<String>>();
        List<TAM_InventarioVehiculosToyota__Share> lInvVehToyotaDel = new List<TAM_InventarioVehiculosToyota__Share>();        
        List<TAM_InventarioVehiculosToyota__Share> lInvVehToyotaReglaDel = new List<TAM_InventarioVehiculosToyota__Share>();        

        Set<String> setIdInvDel = new Set<String>();
        String sNameDist = '';   
                        
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvVhToy : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvVhToy.Dealer_Code__c != null){
                setIdInvDel.add(objInvVhToy.id);
            }//fin si objInvVhToy.Dealer_Code__c != null            
        }//Fin del for para scope
        System.debug('ENTRO TAM_DepuraColaboraCustomInventBch_cls setIdInvDel: ' + setIdInvDel);           

        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Codigo_Distribuidor__c =:this.strCodDealer ORDER BY NAME]){
            sNameDist = Dealer.Name;    
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNameDist: ' + sNameDist);           
        }
                
        /*//Ya tienes los Id de los inventarios que va a colaborar busca las funciones e inventarios que aplica
        for (TAM_AdminInventarioDealer__c objAdminInveDealer : this.lAdminInventarioDealerPrm){
            //Toma las funciones primero TAM_Funcion__c, TAM_TipoInventario__c
            String sFuncionPsso = objAdminInveDealer.TAM_Funcion__c;
            String[] arrFunciones = sFuncionPsso != null ? sFuncionPsso.split(';') : new List<String>();
            Set<String> setFunciones = new Set<String>();
            Set<String> setUsersFunciones = new Set<String>();

            //Dale vuelta a la lista de arrFunciones
            for (String sFuncPaso : arrFunciones){
                //Crea el nombre de la funcion 
                String sNameFuncion;
                String sFuncPasoPaso = sFuncPaso.toUpperCase();
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sFuncPasoPaso: ' + sFuncPasoPaso);           
                if (sFuncPasoPaso == 'EJECUTIVO') 
                    sNameFuncion = sNameDist + ' Socio Ejecutivo';
                if (sFuncPasoPaso == 'GESTOR') 
                    sNameFuncion = sNameDist + ' Socio Gestor';
                if (sFuncPasoPaso == 'ASESOR') 
                    sNameFuncion = sNameDist + ' Socio Usuario';
                //Agregalo al set de setFunciones                    
                setFunciones.add(sNameFuncion);
            }//Fin del for para arrFunciones
            //Consulta los usuaros asociados a las funciones setFunciones 
            for (User usuario : [Select u.id, u.Name, u.CodigoDistribuidor__c, u.UserRole.Name 
                From User u Where u.IsActive = true and UserRole.Name IN :setFunciones]){
                //Metelos en el SET de setUsersFunciones
                setUsersFunciones.add(usuario.id);
            }
            System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setUsersFunciones: ' + setUsersFunciones);

            Set<String> setInventarios = new Set<String>();                       
            //Ya tienes a los usuarios ve por el tipo de inventario que le aplica
            String[] arrInventario = objAdminInveDealer.TAM_TipoInventario__c != null ? objAdminInveDealer.TAM_TipoInventario__c.split(';') : new List<String>();
            //Dale vuelta a la lista de arrFunciones
            for (String sInventPaso : arrInventario){
                //Crea el nombre de la funcion 
                String sNameInvenPaso = '';
                String sInventPasoPaso = sInventPaso.toUpperCase();
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sInventPasoPaso: ' + sInventPasoPaso);           
                if (sInventPasoPaso == 'PRODUCCIÓN') 
                    sNameInvenPaso = 'A';
                if (sInventPasoPaso == 'TRÁFICO') 
                    sNameInvenPaso = 'F';
                if (sInventPasoPaso == 'PISO') 
                    sNameInvenPaso = 'F';
                //Agregalo al set de setFunciones                    
                setInventarios.add(sNameInvenPaso);
            }//Fin del for para arrFunciones
            System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setInventarios: ' + setInventarios);
            
            //Consilta los reg del obj TAM_InventarioVehiculosToyota__Share que corresponden a 
            //setUsersFunciones y setInventarios
            for (TAM_InventarioVehiculosToyota__Share objInvShare : [Select id 
                From TAM_InventarioVehiculosToyota__Share Where ParentId IN :setIdInvDel
                And UserOrGroupId IN :setUsersFunciones] ){
                //Agregalo a la lista de lInvVehToyotaReglaDel
                lInvVehToyotaReglaDel.add(objInvShare);
            }
        }//Fin del for para this.lAdminInventarioDealerPrm
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls lInvVehToyotaReglaDel: ' + lInvVehToyotaReglaDel);*/
        
        //Ahora si consulta los datos de la colaboración relacionada al inventario en setIdInvDel
        //y el grupo diferente de mapNomGrupoIdGrupo.LeySet() y el tipo de colaboración Manual 
        for (TAM_InventarioVehiculosToyota__Share objInvShare : [Select t.id, t.ParentId 
            From TAM_InventarioVehiculosToyota__Share t
            Where t.RowCause = 'Manual' and ParentId IN :setIdInvDel]){
            //Agregalos a la lista de lInvVehToyotaDel
            lInvVehToyotaDel.add(new TAM_InventarioVehiculosToyota__Share(
                    id = objInvShare.id
                )
            );
        }
        System.debug('ENTRO TAM_DepuraColaboraCustomInventBch_cls lInvVehToyotaDel: ' + lInvVehToyotaDel);           
                
        Boolean blnError = false;
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();

        /*//Ve si elimina  this.blnDepuraColaboraReglas
        if (this.blnDepuraColaboraReglas){
            //Actualiza las Opp 
            List<Database.DeleteResult> lDtbUpsRes = Database.delete(lInvVehToyotaReglaDel, false);
            //Ve si hubo error
            for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de eliminar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes            
        }//Fin si this.blnDepuraColaboraReglas*/

        //Ve si elimina todo parejo
        //if (!this.blnDepuraColaboraReglas || !this.blnDepuraGruposReglasRel){
	        //Ve si tiene algo la lita de lInvVehToyotaDel
	        if (!lInvVehToyotaDel.isEmpty()){
	            //Actualiza las Opp 
	            List<Database.DeleteResult> lDtbUpsRes = Database.delete(lInvVehToyotaDel, false);
	            //Ve si hubo error
	            for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
	                if (!objDtbUpsRes.isSuccess()){
	                    System.debug('En la AccSh: hubo error a la hora de eliminar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
	                    blnError = true;
	                }//Fin si !objDtbUpsRes.isSuccess()
	            }//Fin del for para lDtbUpsRes
	        }//Fin si !lInvenVehiculoShare.isEmpty()
        //}//Fin si !this.blnDepuraColaboraReglas
        
        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraCustomInventBch_cls.finish Hora: ' + DateTime.now());      
        System.debug('EN TAM_DepuraColaboraCustomInventBch_cls.finish this.strCodDealer: ' + this.strCodDealer);      
        System.debug('EN TAM_DepuraColaboraCustomInventBch_cls.finish this.lAdminInventarioDealerPrm: ' + this.lAdminInventarioDealerPrm);      
        
        String sQuery = 'Select Id, Name, Dealer_Code__c ';
        sQuery += ' From TAM_InventarioVehiculosToyota__c ';
        sQuery += ' where Dealer_Code__c = \'' + String.escapeSingleQuotes(this.strCodDealer) + '\'';
        sQuery += ' Order by Dealer_Code__c';

        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 10';
        System.debug('EN EN TAM_DepuraColaboraCustomInventBch_cls.finish sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_CreaColaboraCustomInventBch_cls objCreaColaboraCustomInventBch = new TAM_CreaColaboraCustomInventBch_cls(sQuery, this.strCodDealer, 
            this.lAdminInventarioDealerPrm, this.blnDepuraColaboraReglas, this.blnDepuraGruposReglasRel );
        //No es una prueba entonces procesa de 25 en 25
        //if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCreaColaboraCustomInventBch, 50);        
        
    } 
    
}