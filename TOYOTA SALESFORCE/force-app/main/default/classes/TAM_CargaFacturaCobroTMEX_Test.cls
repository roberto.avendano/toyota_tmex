@isTest
public class TAM_CargaFacturaCobroTMEX_Test {
    
    @testSetup static void setup() {
          //VIN de prueba
        Vehiculo__c v01 = new Vehiculo__c(
            Name='12345678901234567',
            Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        //Provisión de incentivos
        TAM_ProvisionIncentivos__c provTest01 = new TAM_ProvisionIncentivos__c();
        provTest01.name = 'Provisión Agosto de prueba';
        provTest01.TAM_AnioDeProvision__c = '2020';
        provTest01.TAM_MesDeProvision__c = 'Agosto';
        insert provTest01;
        
        //Detalle de Provisión
        String recordIdProvisionRetail  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        //Retail
        TAM_DetalleProvisionIncentivo__c detalleProv01 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv01.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv01.RecordTypeId = recordIdProvisionRetail;
        detalleProv01.RetailSinReversa_Cerrado__c = false;
        detalleProv01.TAM_AnioModelo__c = '2020';
        detalleProv01.TAM_AplicanAmbos__c = false;
        detalleProv01.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv01.TAM_CodigoContable__c = '7400B2';
        detalleProv01.TAM_CodigoDealer__c = '57002';
        detalleProv01.TAM_Factura__c = 'FVA12330';
        detalleProv01.TAM_FechaCierre__c = date.today();
        detalleProv01.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv01.TAM_FechaVenta__c = '2020-06-17';
        detalleProv01.TAM_FirstName__c = 'DORA LUZ';
        detalleProv01.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv01.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv01.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv01.TAM_Lealtad__c = false;
        detalleProv01.TAM_Modelo__c = '7495';
        detalleProv01.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv01.TAM_PagadoConTFS__c = false;
        detalleProv01.TAM_PagadoSinTFS__c = true;
        detalleProv01.TAM_ProvicionarEfectivo__c = true;
        detalleProv01.TAM_Provisionado__c = false;
        detalleProv01.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv01.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv01.TAM_ProvisionarLealtad__c = false;
        detalleProv01.TAM_ProvisionCerrada__c = true;
        detalleProv01.TAM_Serie__c	 = 'HILUX';
        detalleProv01.TAM_SolicitadoDealer__c = false;
        detalleProv01.TAM_TipoMovimiento__c = 'RDR';
        detalleProv01.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv01.TAM_TransmitirDealer__c = true;
        detalleProv01.TAM_VIN__c  =  v01.id;
        detalleProv01.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv01;
        
        
        
        TAM_DetalleEstadoCuenta__c detalleEdoCta = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCta.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCta.TAM_AnioModelo__c = '2020';
        detalleEdoCta.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCta.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCta.TAM_CodigoDealer__c = '57011';
        detalleEdoCta.TAM_Codigo_Producto__c = '7400';
        detalleEdoCta.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCta.TAM_EnviadoFacturar__c = false;
        detalleEdoCta.TAM_EdoCtaCerrado__c = false;
        detalleEdoCta.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCta.TAM_VINEntregado__c = true;
        detalleEdoCta.TAM_VIN__c = '12345678901234567';
        detalleEdoCta.TAM_pagoPorTFS__c = false;
        detalleEdoCta.TAM_Serie__c = 'HILUX';
        insert detalleEdoCta;
        
        
    }
    
    
    @isTest static void testMethod1(){
        //Se obtiene el id del detalle de cuenta 
        TAM_DetalleEstadoCuenta__c detalleEdoCta = [Select id from TAM_DetalleEstadoCuenta__c WHERE TAM_VIN__C =: '12345678901234567'];
        
        List<TAM_CargaFacturaCobroTMEX.wrapperEstadoCta> wrrapperTest = new List<TAM_CargaFacturaCobroTMEX.wrapperEstadoCta>();
        TAM_CargaFacturaCobroTMEX.wrapperEstadoCta wrrapperItem = new TAM_CargaFacturaCobroTMEX.wrapperEstadoCta();
        wrrapperItem.VIN = 'QWERTYAKDKAQP20';
        wrrapperItem.Id  = detalleEdoCta.Id;
        wrrapperItem.apellidoPropietario = 'Figueroa';
        wrrapperItem.anioModelo = '2020';
        wrrapperItem.codigoDealer = '57011';
        wrrapperItem.comentarioDealer = 'Todo ok';
        wrrapperItem.EdoCtaCerrado = false;
        wrrapperItem.estatusVINEdoCta = 'Autorizado de cobro';
        wrrapperItem.facturaVenta  =  'Test001';
        wrrapperItem.nombreDealer  ='TOYOTA GUADALAJARA';
        wrrapperItem.nombrePropietario = 'Hector';
        wrrapperItem.IdDetalleProvision = 'Test';
        wrrapperItem.IdDetalleEdoCta = 'Estado cuenta';
        wrrapperItem.IdEstadoCuenta = 'EstadoCuenta';
        wrrapperItem.serie  = 'Corolla';
        wrrapperItem.codigoModelo = '5462';    
        wrrapperItem.incentivoProvisionadoDealer = 12391;
        wrrapperItem.incentivoSolicitadoDealer = 12391;
        wrrapperItem.nombreProvision = 'Provision de prueba';
        wrrapperItem.idProvision = 'AQDNJHED56474';
        wrrapperItem.idFacturaVenta = 'FACTURA2010';
        wrrapperItem.VINEntregado = true;
        wrrapperItem.enviarFacturar = true;
        wrrapperItem.estatusVINEdoCta = 'Revisado y listo para cobro';
        wrrapperItem.tipoTransaction = 'RDR';
        wrrapperItem.fechaVenta = '20/05/2020';
        wrrapperItem.mesPeriodoVenta = '05';
        wrrapperItem.anioPeriodoVenta = '2020';
        wrrapperItem.facturaCobroTMEX = 'TEST01';
        wrrapperItem.ventaTFS = false;
        wrrapperTest.add(wrrapperItem);
        
        TAM_CargaFacturaCobroTMEX.verifyChecksVIN(wrrapperTest);
        TAM_CargaFacturaCobroTMEX.getSUMofList(wrrapperTest);
        TAM_CargaFacturaCobroTMEX.getDocumentByParentId('TEST01');
		TAM_CargaFacturaCobroTMEX.saveDocumentIdDetalleEdoCta('TEST01','TEST01','TEST01');
        TAM_CargaFacturaCobroTMEX.guardaFacturaDetalleEdoCta ('TEST01','FacturaPrueba',wrrapperTest,25000,'Retail');
        TAM_CargaFacturaCobroTMEX.deleteDocument('TEST01','A001');
        
    }
    
}