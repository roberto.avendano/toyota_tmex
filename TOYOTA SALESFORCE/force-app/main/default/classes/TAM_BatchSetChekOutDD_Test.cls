@isTest 
public class TAM_BatchSetChekOutDD_Test 
{
    static testMethod void testMethod1() 
    {
        List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
        
        //Configuración Personalizada
        ConfigCheckOutDD__c config = new ConfigCheckOutDD__c();
        config.TAM_AnioCalToyota__c = '2021';
        config.TAM_Fleet__c = 'C';
        config.TAM_MesCalToyota__c = '1';
        config.TAM_TipoMovimiento__c = 'RDR';
        config.TAM_TipoRegistro__c = 'Pedido Especial';
        insert config;
        
        Account a01 = new Account(
            RecordTypeId = rtDealer.get(0).Id,
            Name = 'Dealer',
            Codigo_Distribuidor__c = '12345'
        );
        insert a01;
        
        
        Vehiculo__c v01 = new Vehiculo__c(
            Name='JTDKBRFUXH3031000',
            Id_Externo__c='JTDKBRFUXH3031000'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA',
            Year__c = '2021',
            Month__c = '1',
            fleet__C = 'C',
            Qty__c   = '1'
        );
        insert m01;
        
        Vehiculo__c v02 = new Vehiculo__c(
            Id = v01.id,
            Name='JTDKBRFUXH3031000',
            Id_Externo__c='JTDKBRFUXH3031000',
            Ultimo_Movimiento__c = m01.id
        );
        update v02;
        
        
        TAM_CheckOutDetalleSolicitudCompra__c checkOut = new TAM_CheckOutDetalleSolicitudCompra__c();
        checkOut.TAM_VIN__c = 'JTDKBRFUXH3031000';
        insert checkOut;
        
        
        Test.startTest();
        
        TAM_ScheduledCheckOutPE sh1 = new TAM_ScheduledCheckOutPE();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test', sch, sh1); 
        
        Test.stopTest();
        
    }
    
    static testMethod void testMethod2() 
    {
        List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
        
        //Configuración Personalizada
        ConfigCheckOutDD__c config = new ConfigCheckOutDD__c();
        config.TAM_AnioCalToyota__c = '2021';
        config.TAM_Fleet__c = 'N';
        config.TAM_MesCalToyota__c = '1';
        config.TAM_TipoMovimiento__c = 'RDR';
        config.TAM_TipoRegistro__c = 'Inventario';
        insert config;
        
        Account a01 = new Account(
            RecordTypeId = rtDealer.get(0).Id,
            Name = 'Dealer',
            Codigo_Distribuidor__c = '12345'
        );
        insert a01;
        
        
        Vehiculo__c v01 = new Vehiculo__c(
            Name='JTDKBRFUXH3031000',
            Id_Externo__c='JTDKBRFUXH3031000'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA',
            Year__c = '2021',
            Month__c = '1',
            fleet__C = 'N',
            Qty__c   = '1'
        );
        insert m01;
        
        Vehiculo__c v02 = new Vehiculo__c(
            Id = v01.id,
            Name='JTDKBRFUXH3031000',
            Id_Externo__c='JTDKBRFUXH3031000',
            Ultimo_Movimiento__c = m01.id
        );
        update v02;
        
        
        TAM_CheckOutDetalleSolicitudCompra__c checkOut = new TAM_CheckOutDetalleSolicitudCompra__c();
        checkOut.TAM_VIN__c = 'JTDKBRFUXH3031000';
        insert checkOut;
        
        
        Test.startTest();
        
        TAM_BatchSetChekOutInvDD obj = new TAM_BatchSetChekOutInvDD();
        DataBase.executeBatch(obj); 
        
        Test.stopTest();
        
    }
}