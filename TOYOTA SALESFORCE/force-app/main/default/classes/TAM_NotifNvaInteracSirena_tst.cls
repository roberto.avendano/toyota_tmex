/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de las 
                        Interacciones_Lead__c que se crean o actualizan.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    30-Septiembre-2021   Héctor Figueroa             Modificación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_NotifNvaInteracSirena_tst {

    @TestSetup static void loadData(){
                
        Lead objLead = new Lead(
            FirstName = 'Prueba'
            , LastName = 'Prueba'
            , TAM_IdExternoSirena__c = '60fda1356e4b97000864dab6'
            , Phone = '5534567876'
            , Email = 'prueba@hotmail.com'
            , FWY_Vehiculo__c = 'Avanza'
            , FWY_codigo_distribuidor__c = '57013'
            , TAM_FormaContactoPreferida__c = 'WhatsApp'
            , TAM_ConfirmaDatos__c = true
            , TAM_FechaCreacionSirena__c = Date.today()
        );
        insert objLead;

        Interacciones_Lead__c objIteracSirena = new Interacciones_Lead__c(
            TAM_IdExterno__c = objLead.id + '-' + aleatorio(15),
            TAM_MedioContacto__c = 'Whatsapp',
            TAM_FechaContacto__c = Date.today(),
            TAM_FechaFinContacto__c = Date.today(),
            TAM_Detalle__c = 'Prueba',
            TAM_Candidato__c = objLead.id,
            TAM_IdExternoLead__c = objLead.id + '-' + aleatorio(15),
            TAM_Notificar__c = true,
            TAM_IntegracionSirena__c = true
        );
        insert objIteracSirena;
                                            
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_NotifNvaInteracSirenaOK() {
       
        //Inician las pruebas       
        Test.startTest();
        
            //Llama al metodo de TAM_NotifNvaInteracSirena.getUltimasInteracSirena();
            List<TAM_NotifNvaInteracSirena.datosInteaccionSirena> lstObjPaso = TAM_NotifNvaInteracSirena.getUltimasInteracSirena();
            System.debug('EN TAM_NotifNvaInteracSirenaOK lstObjPaso: ' + lstObjPaso);
            System.debug('EN TAM_NotifNvaInteracSirenaOK lstObjPaso.size(): ' + lstObjPaso.size());
            
            //Crea un objeto del tipo TAM_NotifNvaInteracSirena.datosInteaccionSirena
            TAM_NotifNvaInteracSirena.datosInteaccionSirena objPaso2 = new TAM_NotifNvaInteracSirena.datosInteaccionSirena();
            objPaso2.sIdLead = '';
            objPaso2.sNomLead = '';
            objPaso2.sEstatusLead = '';
            objPaso2.sFechaCreacCand = '';
            objPaso2.sFechaCreacMsg = '';
            objPaso2.sMensaje = '';
            
            //Ve que se haya creado el registro del tipo List<TAM_NotifNvaInteracSirena.datosInteaccionSirena>
            System.assert(lstObjPaso.size() == 1,'Se cre el registro de interacciones.');
            
        //Fin del las pruebas
        Test.stopTest();
        
    }
}