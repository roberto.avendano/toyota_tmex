public with sharing class EvaluacionComoNuevosWizardController {
	
	public Evaluaciones_Dealer__c evalDealer {get; set;}
	
	public Map<String, List<Respuestas_Preguntas_TSM__c>> mapReactivos {get; set;}
	public Map<String, Seccion__c> mapSecciones {get; set;}


	public Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPI{get; set;}
	public Id idRTAPI{get; set;}
	
	public EvaluacionComoNuevosWizardController (ApexPages.StandardController stdController){
		
		evalDealer = (Evaluaciones_Dealer__c)stdController.getRecord(); 
		
		mapSecciones = new Map<String, Seccion__C>();		
		mapReactivos = new Map<String, List<Respuestas_Preguntas_TSM__c>>();
		
		Set<Id> setIdsPreguntas = new Set<Id>();
		
		if(evalDealer!=null && evalDealer.Id!=null){
			Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
    		idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_EI_Como_Nuevos').Id;
    	
			evalDealer = [SELECT Id, Name, Nombre_Dealer__c,
					(SELECT Evaluacion_Dealer__r.Name, 
					Pregunta_Relacionada__r.Name, 
					Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,
					Pregunta_Relacionada__r.Reactivo__c,
					Pregunta_Relacionada__r.Ponderacion__c,
					Pregunta_Relacionada__r.Fotografia_Obligatoria_del__c,
					EtapaReactivo__c,
					Ponderacion__c,
					PuntosObtenidos__c,
					Observaciones__c
					FROM Respuestas_Preguntas_TSM__r)
				FROM Evaluaciones_Dealer__c
				WHERE Id=:evalDealer.Id];
		
			if(evalDealer.Respuestas_Preguntas_TSM__r!=null && evalDealer.Respuestas_Preguntas_TSM__r.size()>0){
				for(Respuestas_Preguntas_TSM__c rp : evalDealer.Respuestas_Preguntas_TSM__r){
					if(!mapSecciones.containsKey(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
						mapSecciones.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r);
						mapReactivos.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, new List<Respuestas_Preguntas_TSM__c>());
					}
					mapReactivos.get(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c).add(rp);
					setIdsPreguntas.add(rp.Id);
				} 
				mapActividadesPI = this.loadMapActividadesPI(setIdsPreguntas);
			}
		}
		
	}
    
    private Map<Id, List<Actividad_Plan_Integral__c>> loadMapActividadesPI(Set<Id> setIdsPreguntas){
    	
    	Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPITmp = new Map<Id, List<Actividad_Plan_Integral__c>>();
        Map<Id,Respuestas_Preguntas_TSM__c> mapRespuestasConAPI = new Map<Id,Respuestas_Preguntas_TSM__c>([
        	SELECT Id, (
        		select Id, Name, Condicion_Observada__c, Prioridad__c, Referencia__c, Actividad_Mejora__c, Respuestas_Preguntas_TSM__c, Celula_Area__c, Fecha_Compromiso__c, Responsable_Dealer__c 
        	    from Actividades_Planes_Integrales__r),
        	    (select Id, Name from Attachments) 
        	FROM Respuestas_Preguntas_TSM__c WHERE Id IN :setIdsPreguntas
        ]);
        
        for(Id item : setIdsPreguntas){
        	if(!mapRespuestasConAPI.containsKey(item) || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r==null || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r.size()<1){
        		Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
        			Respuestas_Preguntas_TSM__c =item,
        			Cuenta__c = evalDealer.Nombre_Dealer__c
        		);
        		if(idRTAPI!=null){
        			api.RecordTypeId = idRTAPI;
        		}
        		
        		mapActividadesPITmp.put(item,new List<Actividad_Plan_Integral__c>{api});
        	}else{
        		mapActividadesPITmp.put(item,mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r);
        	}
        }
    	return mapActividadesPITmp;
    }
    
	public List<SelectOption> getAreas(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Celula_Area__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		//options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
    
    public List<SelectOption> getReferencia(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Referencia__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		//options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
	
	public List<SelectOption> getResponsablesDealer(){
		List<SelectOption> options = new List<SelectOption>();
		
		for( Contact c : [Select Id, Name, Title From Contact Where AccountId=:evalDealer.Nombre_Dealer__c]){
			options.add(new SelectOption(c.Id, c.Name + ' - ' + c.Title));
		}       
		return options;
	}

	@RemoteAction
	public static String guardaRespuesta(String idPregunta, String respuesta, Double puntos){
		try{
			update new Respuestas_Preguntas_TSM__c(
				Id = idPregunta,
				EtapaReactivo__c = respuesta,
				PuntosObtenidos__c = puntos,
				Realizado__c = true
			);
			return 'Se actualizo correctamente';
		}catch(DMLException  dmle){
			System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
	}

	@RemoteAction
	public static String guardaRespuestaObs(String idPregunta, String observaciones){
		try{
			update new Respuestas_Preguntas_TSM__c(
				Id = idPregunta,
				Observaciones__c = observaciones,
				Realizado__c = true
			);
			return 'Se actualizo correctamente';
		}catch(DMLException  dmle){
			System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
	}
    
    @RemoteAction
    public static Actividad_Plan_Integral__c guardaAPI(String idRespuesta, String idAPI, String condicion, String actividad, String celula, String responsableDealer, String evalDealerId, String fechaComp, String rtAPI){
    	try{
    		System.debug(Id.valueOf(idAPI));
    	}catch(Exception e){
    		System.debug(e);
    		idAPI=null;
    	}
        Date fechaCompDate = null;
        if(fechaComp!=null && fechaComp!=''){
            try{
                fechaCompDate = Date.parse(fechaComp);
            }catch(Exception e){
                throw new ToyotaException('La fecha no esta en el formato requerido');
            }
        }
        try{
        	Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
        		Respuestas_Preguntas_TSM__c = idRespuesta,
        		Id = idAPI,
        		Condicion_Observada__c = condicion,
        		Actividad_Mejora__c = actividad,
        		Celula_Area__c = celula,
        		Responsable_Dealer__c = responsableDealer,
        		Cuenta__c=evalDealerId,
                Fecha_Compromiso__c=fechaCompDate,
                RecordTypeId = rtAPI
        	); 
        	
            System.debug(api);
        	upsert api;
        	return api;//[SELECT Id, Name, Respuestas_Preguntas_TSM__c, Condicion_Observada__c, Actividad_Mejora__c, Celula_Area__c, Responsable_Dealer__c, Fecha_Compromiso__c FROM Actividad_Plan_Integral__c WHERE Id=:api.Id];
        }catch(Exception e){
            System.debug(e);
            throw new ToyotaException('Error: ' + e.getMessage());
        }
    }
}