/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_FacturaCarga_tgr_handler.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    23-Diciembre-2019    Héctor Figueroa             Creación
    1.0    01-Junio-2020	    Héctor Figueroa             Modificación    
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_FacturaCarga_tgr_handler_tst {

	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
    static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();
	static String sIdStdPriceBoook = Test.getStandardPricebookId();
	static TAM_FacturaCarga__c facturaCargaCons = new TAM_FacturaCarga__c();
	
	@TestSetup static void loadData(){

        Lead cand = new Lead(
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '224',
	        Status='Pedido en Proceso',
	        LastName = 'Test3',
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Programa'
	    );
	    insert cand;

        TAM_InventarioVehiculosToyota__c objInvAFG = new TAM_InventarioVehiculosToyota__c(
              Name = 'JTDKBRFUXH3031000',
              Dealer_Code__c = '57039',
              Interior_Color_Description__c = 'Azul', 
              Exterior_Color_Description__c = 'Gris',
              Model_Number__c = '22060',
              Model_Year__c = '2020', 
              Toms_Series_Name__c = 'AVANZA',
              Exterior_Color_Code__c = '00B79', 
              Interior_Color_Code__c =  '010',
              Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvAFG;

        TAM_LeadInventarios__c leadInv = new TAM_LeadInventarios__c(
              Name = 'JTDKARFU1L3109849',
              TAM_InventarioVehiculosFyG__c = objInvAFG.id,
              TAM_Prospecto__c = cand.id, 
              TAM_Idexterno__c = cand.id + ' JTDKARFU1L3109849',
              TAM_VINFacturacion__c = 'JTDKARFU1L3109849'
        );
        insert leadInv;

		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Opportunity OppPrueba =	new Opportunity( 
			Name = 'JTDKARFU1L3109849',
			TAM_Vin__c = 'JTDKARFU1L3109849',						
			CloseDate = Date.today(),
			StageName = 'Closed Won',
			Pricebook2Id = sIdStdPriceBoook,
			Amount = 100.00,
			TAM_IdExterno__c = 'JTDKARFU1L3109849-TestAccount',
			recordTypeId = VaRtOppRegVCrm,
			TAM_Distribuidor__c = clienteDealer.ID,
			AccountId = clienteDealer.id
		);
		insert 	OppPrueba;	

		Factura__c Factura = new Factura__c(
			//VIN__c = 'JTDKARFU1L3109848', 
			TAM_UUID__c	= 'b315ab4e-515c-403e-8fc2-ba274029a9d2',
			TAM_NombreArchivo__c = 'SAA000015419.xml',
			TAM_IdDealerCode__c = '570550',
			TAM_Total__c = 235300,
			TAM_TipoDeComprobante__c = 'I',
			TAM_SubTotal__c = 202844.83,
			TAM_Serie__c = 'SAA', 
			TAM_LugarExpedicion__c = '5348',
			TAM_MetodoPago__c = 'PPD',
			TAM_Moneda__c = 'MXN',
			TAM_FormaPago__c = '99',
			TAM_Folio__c = '15419',
			TAM_FechaFolio__c = Datetime.now(),
			TAM_CondicionesDePago__c = 'CONTADO',
			TAM_RfcEmisor__c = 'ANI140616N87',
			TAM_RegimenFiscal__c = '601',
			TAM_NombreEmisor__c = 'Automotriz Nihon, S.A. de C.V.',
			TAM_RfcReceptor__c = 'CIN8301189L4',
			TAM_UsoCFDI__c = 'G03',
			TAM_NombreReceptor__c = 'COMISION NACIONAL DEL AGUA ORGANISMO DE CUENCA GOLFO NORTE',
			TAM_ClaveVehicular__c = '521702',
			TAM_Numero__c = '205137880001337',
			TAM_FechaAduana__c = Date.today(), //2020-03-21, //Validar
			TAM_Aduana__c = 'LAZARO CARDENAS MICH',
			TAM_FechaTimbrado__c = DateTime.now(),
			TAM_ValorUnitario__c = 202844.83,
			TAM_Unidad__c = 'Vehículo',
			TAM_Importe__c = 202844.83,
			TAM_Descripcion__c = 'DESCRIPCION',
			TAM_ClaveUnidad__c = 'XVN',
			TAM_ClaveProdServ__c = '25101500',
			TAM_Cantidad__c = '1',
			TAM_ImporteTraslado__c = 32455.17,
			TAM_TipoFactor__c = 'Tasa',
			TAM_TasaOCuota__c = 0.16,
			TAM_Impuesto__c = '2',
			Distribuidor__c = clienteDealer.id
		);
		insert Factura;

		//Crea el objeto del tipo TAM_OportunidadFactura__c
		TAM_OportunidadFactura__c OportunidadFacturaPaso = new TAM_OportunidadFactura__c(
			Name = 'JTDKARFU1L3109849 - 205137880001337',
			TAM_Factura__c =  Factura.id,
			TAM_Oportunidad__c = OppPrueba.id
			//String sIdExterno = sIdOpp + '-' +  sIdFactura
		);
		insert OportunidadFacturaPaso;

		TAM_FacturaCarga__c facturaCarga = new TAM_FacturaCarga__c(
			VIN__c = 'JTDKARFU1L3109849', 
			TAM_UUID__c	= 'b315ab4e-515c-403e-8fc2-ba274029a9d2',
			TAM_NombreArchivo__c = 'SAA000015419.xml',
			TAM_IdDealerCode__c = '570550',
			TAM_Total__c = 235300,
			TAM_TipoDeComprobante__c = 'I',
			TAM_SubTotal__c = 202844.83,
			TAM_Serie__c = 'SAA', 
			TAM_LugarExpedicion__c = '5348',
			TAM_MetodoPago__c = 'PPD',
			TAM_Moneda__c = 'MXN',
			TAM_FormaPago__c = '99',
			TAM_Folio__c = '15419',
			TAM_FechaFolio__c = Datetime.now(),
			TAM_CondicionesDePago__c = 'CONTADO',
			TAM_RfcEmisor__c = 'ANI140616N87',
			TAM_RegimenFiscal__c = '601',
			TAM_NombreEmisor__c = 'Automotriz Nihon, S.A. de C.V.',
			TAM_RfcReceptor__c = 'CIN8301189L4',
			TAM_UsoCFDI__c = 'G03',
			TAM_NombreReceptor__c = 'COMISION NACIONAL DEL AGUA ORGANISMO DE CUENCA GOLFO NORTE',
			TAM_ClaveVehicular__c = '521702',
			TAM_Numero__c = '205137880001337',
			TAM_FechaAduana__c = '21/03/2020', //Validar
			TAM_Aduana__c = 'LAZARO CARDENAS MICH',
			TAM_FechaTimbrado__c = DateTime.now(),
			TAM_ValorUnitario__c = 202844.83,
			TAM_Unidad__c = 'Vehículo',
			TAM_Importe__c = 202844.83,
			TAM_Descripcion__c = 'DESCRIPCION',
			TAM_ClaveUnidad__c = 'XVN',
			TAM_ClaveProdServ__c = '25101500',
			TAM_Cantidad__c = '1',
			TAM_ImporteTraslado__c = '32455.17',
			TAM_TipoFactor__c = 'Tasa',
			TAM_TasaOCuota__c = '0.16',
			TAM_Impuesto__c = '2',
			TAM_Base__c = '202844.83'			 
		);
		insert facturaCarga;
		
		TAM_FacturaCarga__c facturaCarga2 = new TAM_FacturaCarga__c(
			VIN__c = 'JTDKARFU1L3109850', 
			TAM_UUID__c	= 'b315ab4e-515c-403e-8fc2-ba274029a9d2',
			TAM_NombreArchivo__c = 'SAA000015419.xml',
			TAM_IdDealerCode__c = '570550',
			TAM_Total__c = 235300,
			TAM_TipoDeComprobante__c = 'I',
			TAM_SubTotal__c = 202844.83,
			TAM_Serie__c = 'SAA', 
			TAM_LugarExpedicion__c = '5348',
			TAM_MetodoPago__c = 'PPD',
			TAM_Moneda__c = 'MXN',
			TAM_FormaPago__c = '99',
			TAM_Folio__c = '15419',
			TAM_FechaFolio__c = Datetime.now(),
			TAM_CondicionesDePago__c = 'CONTADO',
			TAM_RfcEmisor__c = 'ANI140616N87',
			TAM_RegimenFiscal__c = '601',
			TAM_NombreEmisor__c = 'Automotriz Nihon, S.A. de C.V.',
			TAM_RfcReceptor__c = 'CIN8301189L4',
			TAM_UsoCFDI__c = 'G03',
			TAM_NombreReceptor__c = 'COMISION NACIONAL DEL AGUA ORGANISMO DE CUENCA GOLFO NORTE',
			TAM_ClaveVehicular__c = '521702',
			TAM_Numero__c = '205137880001337',
			TAM_FechaAduana__c = '21/03/2020', //Validar
			TAM_Aduana__c = 'LAZARO CARDENAS MICH',
			TAM_FechaTimbrado__c = DateTime.now(),
			TAM_ValorUnitario__c = 202844.83,
			TAM_Unidad__c = 'Vehículo',
			TAM_Importe__c = 202844.83,
			TAM_Descripcion__c = 'DESCRIPCION',
			TAM_ClaveUnidad__c = 'XVN',
			TAM_ClaveProdServ__c = '25101500',
			TAM_Cantidad__c = '1',
			TAM_ImporteTraslado__c = '32455.17',
			TAM_TipoFactor__c = 'Tasa',
			TAM_TasaOCuota__c = '0.16',
			TAM_Impuesto__c = '2',
			TAM_Base__c = '202844.83'			 
		);
		insert facturaCarga2;
		
	}

    static testMethod void TAM_FacturaCarga_tgr_handlerOK() {
		//Consulta los datos del cliente
		facturaCargaCons = [Select Id, Name From TAM_FacturaCarga__c where VIN__c = 'JTDKARFU1L3109850' Limit 1];        
   		System.debug('EN TAM_FacturaCarga_tgr_handlerOK facturaCargaCons: ' + facturaCargaCons);        
    }
    
}