public class SolicitudSiteHeaderIController { 
    public String csId{get;set;}
    public String sivId{get;set;}
    public SolicitudInternaVehiculos__c siv{get;set;}
    private Map<String,Map<String,RecordType>> recordTypesMap;
    
    public SolicitudSiteHeaderIController(){  
        this.sivId =  ApexPages.currentPage().getParameters().get('id');
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        System.debug(sivId);
        getContactoSite(sivId);
    }
    
    public String getContactoSite(String sivId){
        System.debug(sivId);
        siv = [SELECT Id, Solicitante__c FROM SolicitudInternaVehiculos__c WHERE Id=:sivId ];
        System.debug(siv);
        Id rtContactoS = recordTypesMap.get('ContactoSite__c').get('UsuarioSite').Id;
        ContactoSite__c cs =[SELECT Id, RecordTypeId FROM ContactoSite__c WHERE Contacto__c =:siv.Solicitante__c AND RecordTypeId = :rtContactoS LIMIT 1];
        System.debug(cs);        
        csId = cs.Id;
        return csId;
    }
    
    public PageReference inicio(){
        PageReference page;
        page = new PageReference('/SolicitudSiteHome?id='+csId);
        page.setRedirect(true);
        return page;
    } 
}