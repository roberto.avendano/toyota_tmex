/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para desactivar las cuentas corporativas que ya 
    					vencieron su vigencia TAM_InactivaCorporateAccountsSch

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    20-Octubre-2020 		Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_InactivaCorporateAccountsSch implements Schedulable{

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeShareAccountsSch.execute...');
		String strVencido = 'Vencido';
		
		this.sQuery = 'Select Id, Name, TAM_EstatusCliente__c, TAM_FechaVigenciaFin__c, TAM_TIPOPERSONAPROSPECTO__C, RecordType.Name ';
		this.sQuery += ' From Account ';
		this.sQuery += ' Where TAM_FechaVigenciaFin__c < TODAY and TAM_EstatusCliente__c != \'' + String.escapeSingleQuotes(strVencido)+'\'';
		this.sQuery += ' Order by Name ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeShareAccountsSch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_InactivaCorporateAccountsBch objInactAccountCls = new TAM_InactivaCorporateAccountsBch(sQuery);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objInactAccountCls, 25);
			    	 
    }
        
}