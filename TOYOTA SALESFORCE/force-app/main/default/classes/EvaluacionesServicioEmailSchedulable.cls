global class EvaluacionesServicioEmailSchedulable implements Schedulable {
	
	global void execute(SchedulableContext sc) {
        String query='SELECT Id, Name, RecordTypeId, Owner.Email, Nombre_Dealer__r.Promotor_Kaizen__r.Email, Nombre_Dealer__r.Promotor_Kaizen__c FROM Evaluaciones_Dealer__c WHERE Evaluacion_Cerrada__c = false AND (RecordType.DeveloperName=\'Evaluacion_SSC\' OR RecordType.DeveloperName= \'Evaluacion_EDER\')';
        EvaluacionesServicioEmailBatch esbatch = new EvaluacionesServicioEmailBatch(query);
        Database.executeBatch(esbatch);
	}
}