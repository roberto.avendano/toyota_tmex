/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_DODSolicitudesPedidoEspecial__c
    					y toma los reg que ya tienen un TAM_DummyVin__c y no tienen valor en el campo de TAM_VIN__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    20-Abril-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActualizaDummyVinBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActualizaDummyVinBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActualizaDummyVinBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_DODSolicitudesPedidoEspecial__c> scope){
        System.debug('EN TAM_ActualizaDummyVinBch_cls.');
		
		//Crea la lista para poder eliminar los reg de ClientesPaso__c
		Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapInvDummyPasoUpd = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();
		Map<String, String> mapIdDummyIdReg = new Map<String, String>();
        Map<String, String> mapInvDummyObjDod = new Map<String, String>();
		Set<String> setDodAsig = new Set<String>();
        Set<String> setDodAsigNoExiste = new Set<String>();
        Map<String, DateTime> mapVinesVtas = new map<String, DateTime>();
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapIdDodEstatus = new map<String, TAM_DODSolicitudesPedidoEspecial__c>();
        List<TAM_LogsErrores__c> lerrorShare = new List<TAM_LogsErrores__c>();
		
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_DODSolicitudesPedidoEspecial__c objDod : scope){
        	mapIdDummyIdReg.put(objDod.TAM_DummyVin__c, objDod.id);
            String sVinPaso;
            String sVinFinal;
        	if (objDod.TAM_Codigo__c != null){
        	   sVinPaso = objDod.TAM_DummyVin__c != null ? objDod.TAM_DummyVin__c.right(6) : objDod.TAM_DummyVin__c;
               if (sVinPaso.isNumeric())
                    sVinFinal = objDod.TAM_DummyVin__c;
               else
                   sVinFinal = objDod.TAM_DummyVin__c != null ? objDod.TAM_DummyVin__c.right(8) : objDod.TAM_DummyVin__c;
               mapInvDummyObjDod.put(objDod.TAM_DummyVin__c, objDod.TAM_Codigo__c);
               mapInvDummyObjDod.put(sVinFinal, objDod.TAM_Codigo__c);
               mapIdDummyIdReg.put(sVinFinal, objDod.id);
               mapIdDodEstatus.put(objDod.id, objDod);
        	}//Fin si objDod.TAM_Codigo__c != null
        	//lInvDummyPaso.add(objDod);
        	System.debug('EN TAM_ActualizaDummyVinBch_cls TAM_DummyVin__c: ' + sVinFinal + ' id: ' + objDod.id);
        }
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyObjDod: ' + mapInvDummyObjDod.KeySet());
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyObjDod: ' + mapInvDummyObjDod.Values());
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapIdDummyIdReg: ' + mapIdDummyIdReg.KeySet());
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapIdDummyIdReg: ' + mapIdDummyIdReg.Values());
        
        //Busc en el objeto de Inventario_en_Transito__c los TAM_DummyVin__c
        for (TAM_InventarioVehiculosToyota__c objInvTransi : [Select id, Dummy_VIN__c, TAM_Dummy_VIN_Temp__c, Name, TAM_TipoInventarioLetra__c 
            From TAM_InventarioVehiculosToyota__c 
        	Where (Dummy_VIN__c IN : mapIdDummyIdReg.KeySet() 
        	   OR TAM_Dummy_VIN_Temp__c IN : mapIdDummyIdReg.KeySet() 
        	   OR Dummy_VIN__c IN : mapInvDummyObjDod.KeySet()
        	   OR TAM_Dummy_VIN_Temp__c IN : mapInvDummyObjDod.KeySet()
        	   OR Name IN :mapIdDummyIdReg.KeySet() 
        	)
        	And Model_Number__c IN : mapInvDummyObjDod.Values()]){
            String sIdDod = mapIdDummyIdReg.containsKey(objInvTransi.Dummy_VIN__c) ? mapIdDummyIdReg.get(objInvTransi.Dummy_VIN__c) : 
                (mapIdDummyIdReg.containsKey(objInvTransi.TAM_Dummy_VIN_Temp__c) ? mapIdDummyIdReg.get(objInvTransi.TAM_Dummy_VIN_Temp__c) : mapIdDummyIdReg.get(objInvTransi.Name));
            
            String sVinPaso; 
            String sPasoDummyVin = objInvTransi.Name != null ? objInvTransi.Name.right(6) : objInvTransi.Name;
            //Ve si son numeros en  sPasoDummyVin
            if (sPasoDummyVin.isNumeric())
               sVinPaso = objInvTransi.Name;
            if (!sPasoDummyVin.isNumeric())
               sVinPaso = null;

            System.debug('EN TAM_ActualizaDummyVinBch_cls sIdDod: ' + sIdDod + ' ' + sVinPaso);
        	//Crea los objetos del tipo TAM_DODSolicitudesPedidoEspecial__c y metelos a el mapa de mapInvDummyPasoUpd
            TAM_DODSolicitudesPedidoEspecial__c objDoDPaso = new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDod, TAM_VIN__c = sVinPaso);
            //Tiene un VIN Asignado 
            if (sVinPaso != null){
                String sEstatus = 'Asignado';
                //Ve si el estado del can candidatp es diferente de cancelado
                if (mapIdDodEstatus.containsKey(sIdDod))
                    if (mapIdDodEstatus.get(sIdDod).TAM_Estatus__c == 'Cancelado')
                        sEstatus = 'Cancelado';
                objDoDPaso.TAM_Estatus__c = sEstatus;
                System.debug('EN TAM_ActualizaDummyVinBch_cls objDoDPaso: ' + objDoDPaso);
	        	mapInvDummyPasoUpd.put(sIdDod, objDoDPaso);
	        	//Mete el vin a setDodAsig
	        	setDodAsig.add(objInvTransi.Name);
	        	if (objInvTransi.Dummy_VIN__c != null)
	                setDodAsig.add(objInvTransi.Dummy_VIN__c);
	            if (objInvTransi.TAM_Dummy_VIN_Temp__c != null)
	                setDodAsig.add(objInvTransi.TAM_Dummy_VIN_Temp__c);
            }//Fin si sVinPaso != null
        }
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd.KeySet(): ' + mapInvDummyPasoUpd.KeySet());
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd.Values():'  + mapInvDummyPasoUpd.Values());
        System.debug('EN TAM_ActualizaDummyVinBch_cls setDodAsig:'  + setDodAsig);
        
        //Recorre la lista de mapIdDummyIdReg.KeySet() y ve si no existe en setDodAsig
        for (String sDummy : mapIdDummyIdReg.KeySet()){
            if (!setDodAsig.contains(sDummy))
                setDodAsigNoExiste.add(sDummy);
        }
        System.debug('EN TAM_ActualizaDummyVinBch_cls setDodAsigNoExiste:'  + setDodAsigNoExiste);
        
        //Consulta los vines asociados a setVinesHist en Ventas__c
        for (Vehiculo__c ventas : [Select v.id, v.Fecha_de_Venta__c, v.Name From Vehiculo__c v
            Where Name IN :setDodAsigNoExiste]){
            String sIdDod = mapIdDummyIdReg.get(ventas.Name);                                       
            System.debug('EN TAM_ActualizaDummyVinBch_cls sIdDod2: ' + sIdDod);
            Boolean blnTieneCheckout = mapIdDodEstatus.containsKey(sIdDod) ? mapIdDodEstatus.get(sIdDod).TAM_TieneCheckout__c : false;
            String sEstatus = 'Asignado';
            //Ve si el estado del can candidatp es diferente de cancelado
            if (mapIdDodEstatus.containsKey(sIdDod))
               if (mapIdDodEstatus.get(sIdDod).TAM_Estatus__c == 'Cancelado')
                  sEstatus = 'Cancelado';            
            //Crea los objetos del tipo TAM_DODSolicitudesPedidoEspecial__c y metelos a el mapa de mapInvDummyPasoUpd
            mapInvDummyPasoUpd.put(mapIdDummyIdReg.get(ventas.Name), 
                new TAM_DODSolicitudesPedidoEspecial__c(
                    id = sIdDod,
                    TAM_VIN__c = ventas.Name,
                    TAM_Estatus__c = sEstatus,
                    TAM_TieneCheckout__c = blnTieneCheckout ? false : true
                )
            );
        }
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.KeySet());         
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.Values()); 
        
        //Recorre el mapa de mapInvDummyPasoUpd
        for (String  sIdDod : mapIdDummyIdReg.values()){
            Boolean blnExiste = false;
            Decimal dAleatorio = Math.random();
            String  sdAleatorio = String.valueOf(dAleatorio);
            String  sdAleatorioFinal = String.valueOf(dAleatorio).right(10);
             
            //Recorre la lista de mapInvDummyPasoUpd
            for (TAM_DODSolicitudesPedidoEspecial__c objDod : mapInvDummyPasoUpd.values()){
                //Ves si objDod.id == sIdDod
                if (objDod.id == sIdDod){
                    break;
                    blnExiste = true;
                }//Fin si objDod.id == sIdDod
            }//Fin del for para mapIdDummyIdReg.values()
            System.debug('EN TAM_ActualizaDummyVinBch_cls blnExiste: ' + blnExiste); 
            //Ve si no exite el objDod.id en mapIdDummyIdReg.values()
            if (!blnExiste){
                TAM_DODSolicitudesPedidoEspecial__c objDodPaso = mapIdDodEstatus.containskey(sIdDod) ? mapIdDodEstatus.get(sIdDod) : new TAM_DODSolicitudesPedidoEspecial__c();
	            //mapInvDummyPasoUpd.put(sIdDod, new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDod, TAM_Estatus__c = 'Pendiente'));                
                TAM_LogsErrores__c objLogErro = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd DOD Vin Pendiente', TAM_Fecha__c = Date.today());
                objLogErro.TAM_DetalleError__c = 'En TAM_ActualizaDummyVinBch_cls ID DOD : ' + sIdDod + ' No tiene se encontro VIN asignado: ' + objDodPaso;
                if (objDodPaso.id != null)
                    objLogErro.TAM_idExtReg__c = objDodPaso.id + '-' + objDodPaso.TAM_DummyVINFormula__c + '-' + objDodPaso.TAM_Estatus__c + '-' + objDodPaso.TAM_Historico__c + '-' + sdAleatorioFinal;
                lerrorShare.add(objLogErro);                
            }//Fin si no !blnExiste
        }//Fin del for para mapInvDummyPasoUpd.values()
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd3: ' + mapInvDummyPasoUpd.KeySet());         
        System.debug('EN TAM_ActualizaDummyVinBch_cls mapInvDummyPasoUpd3: ' + mapInvDummyPasoUpd.Values()); 
        
        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!mapInvDummyPasoUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapInvDummyPasoUpd.values(), TAM_DODSolicitudesPedidoEspecial__c.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes)
				if (!objDtbUpsRes.isSuccess()){
					System.debug('EN TAM_ActualizaDummyVinBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_ActualizaDummyVinBch_cls Los datos de los Modelos seleccionados se crear/Actualizar con exito');
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
        
        System.debug('EN TAM_ActualizaDummyVinBch_cls lerrorShare: ' + lerrorShare);        
        //Crea el registro en lerrorShare
        if (!lerrorShare.isEmpty())
            insert lerrorShare;
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActualizaDummyVinBch_cls.finish Hora: ' + DateTime.now());      
    } 
        
}