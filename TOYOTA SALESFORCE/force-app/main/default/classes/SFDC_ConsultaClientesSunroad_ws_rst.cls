/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Publicaciín del WS con tecnologia REST para la consulta de Propectos 
                        que se crearon o modificaron en la fecha actual

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Septiembre-2021   Héctor Figueroa             Creación
******************************************************************************* */

@RestResource(urlMapping='/SFDC_ConsultaClientesSunroad_ws_rst/*')
global with sharing class SFDC_ConsultaClientesSunroad_ws_rst {
    
    @HttpPost  
    global static void ConsultaClientesParaSunroadRst(){
        RestRequest req = RestContext.request;
        Blob bBody = req.requestBody;
        String sBody =  bBody.toString();

        System.debug('EN ConsultaClientesParaSunroadRst sBody: ' + sBody);        
        //Manda llamar la clase que se llama SFDC_ConsultaClientesSunroad_ctrl_rst y el metodo getClientesSunroad
        String sResLstCtesSunroad = SFDC_ConsultaClientesSunroad_ctrl_rst.getClientesSunroad(sBody);
        System.debug('EN ConsultaClientesParaSunroadRst sResLstCtesSunroad: ' + sResLstCtesSunroad);

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = Blob.valueOf(sResLstCtesSunroad);
    }
    
    
}