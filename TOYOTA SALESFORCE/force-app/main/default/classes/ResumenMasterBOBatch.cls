global class ResumenMasterBOBatch implements Database.Batchable<sObject> {
	String query;
	
	global ResumenMasterBOBatch(String q) {
	  	this.query= q;
	}
	 
	global ResumenMasterBOBatch(){
	 	this.query='Select PartNameId__c, Id, BOQTYDealerOrder__c from MasterBO__c where StatusByAsDate__c=\'Yes\' and RecordType.DeveloperName= \'MasterBO\'';
	}
	 
	global Database.QueryLocator start(Database.BatchableContext BC) {
	  	return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug(scope);
		Set<ID> setArray = new Set<ID>();
		for(MasterBO__c master: (List<MasterBO__c>)scope){
			setArray.add(master.PartNameId__c);
		}
		if(setArray.size()>0){
			AggregateResult[] resultados= [SELECT PartNameId__c, sum(BOQTYDealerOrder__c)suma, count(ID)conteo FROM MasterBO__c where StatusByAsDate__c='Yes' and RecordType.DeveloperName= 'MasterBO' and PartNameId__c IN:setArray group by PartNameId__c];
			Map<Id, AggregateResult> mapResult = new Map <Id, AggregateResult>();
		
			for(Integer i=0; i<resultados.size(); i++){
				mapResult.put((Id)resultados[i].get('PartNameId__c'), resultados[i]);
			}

			for(MasterBO__c mo: (List<MasterBO__c>)scope){
				AggregateResult respaldo = mapResult.get(mo.PartNameId__c);
				if(respaldo!=null){
					mo.TotalBOQty__c = (Decimal)respaldo.get('suma');
					mo.CountOrder__c = (Decimal)respaldo.get('conteo');
				}
			}
			
			update scope;
		}
		
	}
	 
	global void finish(Database.BatchableContext BC) {

	}

}