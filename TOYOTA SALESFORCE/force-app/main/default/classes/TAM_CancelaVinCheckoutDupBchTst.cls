/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase para el metodo de prueba de TAM_CancelaVinCheckoutDupSch_cls
                        y TAM_CancelaVinCheckoutDupBch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CancelaVinCheckoutDupBchTst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    
    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
    
    static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

    static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    static String sRectorTypePasoVinesFlotilla = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String sRectorTypePasoVinesPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

    static String sRectorTypePoliticaIncentivos = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();

    static String sRectorTypeFlotillaRep = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Pedido Especial Rep').getRecordTypeId();    
    static String sRectorTypePasoFlotillaPE = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();
    static String strRecordTypeIdMov = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
    static String sListaPreciosCutom = getCustomPriceBookList();
    
    static  Account clienteDealer = new Account();
    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
    static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
    static  Rangos__c rango = new Rangos__c();
    static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
    static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
    static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();
    
    @TestSetup static void loadData(){
        
        Group grupoPrueba = new Group(
            Name = 'CARSON',
            Type = 'Regular'
        );
        insert grupoPrueba;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;
        
        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;
        
        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod;  
        CatalogoCentMod = catCenMod; 
                
        Id standardPricebookId = Test.getStandardPricebookId();
        Product2 ProducStdPriceBook = new Product2(
                Name = '22060',
                Anio__c = '2020', 
                NombreVersion__c = 'LE MT',
                IdExternoProducto__c = '220602020',
                ProductCode = '220602020',
                Description= 'AVANZA-22060-2020-B79-10-LE AT', 
                RecordTypeId = sRectorTypePasoProductoUnidad,
                Family = 'Toyota',
                IsActive = true
        );
        insert ProducStdPriceBook;
        
        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id = standardPricebookId,
            UnitPrice = 0.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true,
            IdExterno__c = '220602020'
        );
        insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
        
        PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
            Pricebook2Id = customPB.id,
            UnitPrice = 1.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true
        );
        insert pbeCustomProceBookEntry;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id
        );
        insert solVentaFlotillaPrograma;

        TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoPedidoEspecial,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = true,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Cantidad__c = 10
        );
        insert detSolVentaFlotillaPrograma;

        TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra = new TAM_DistribuidoresFlotillaPrograma__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoDistFlotilla,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_Estatus__c = 'Pendiente',
            TAM_Cantidad__c = 10,
            TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma.id,
            TAM_Cuenta__c = clienteMoral.id,
            TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA',
            TAM_IdDistribuidor__c = '570550'        
        );
        insert detSolDistFltoProgra;
                                        
        InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c(
                  Name = 'XXXXXXXXXXX',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvWholeSale;
                
        TAM_VinesFlotillaPrograma__c objTAMVinesFlotillaPrograma3 = new TAM_VinesFlotillaPrograma__c(
          Name = 'XXXXXXXXXX1', 
          TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-2020-AVANZA-22060-B79-10-JTDBBRBE1LJ018487-' + sRectorTypePasoVinesFlotilla, 
          TAM_Estatus__c = 'Cerrada',
          TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.ID
        );  
        insert objTAMVinesFlotillaPrograma3;            
        
        TAM_PoliticasIncentivos__c objTAMPoliticasIncentivos = new TAM_PoliticasIncentivos__c(
            Name = 'ABIRL 2100 - RETAIL' ,
            TAM_InicioVigencia__c = Date.today(),
            TAM_FinVigencia__c = Date.today().addDays(10)
        );
        insert objTAMPoliticasIncentivos;
        
        TAM_DetallePolitica__c objTAMDetallePolitica = new TAM_DetallePolitica__c(
              TAM_AnioModelo__c = '2020',
              TAM_Serie__c = 'AVANZA',
              TAM_Version__c = 'LE AT',
              TAM_Pago_TFS__c = false,
              TAM_IncTFS_Porcentaje__c = 1.00,
              TAM_IncTFS_Cash__c = 1.00,
              TAM_IncDealer_Cash__c = 1.00,
              TAM_IncDealer_Porcentaje__c = 1.00,
              TAM_Clave__c = '220602020',
              TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id
        );        
        insert objTAMDetallePolitica;  

        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10'  + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoFlotillaPE + '-Arrendadora-0',
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            RecordTypeId = sRectorTypeFlotillaRep,
            TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id,
            TAM_Intercambio__c = false,
            TAM_Historico__c = false,
            TAM_EstatusDOD__c = 'Autorizado'
        );
        insert objCheckOutDetSolComp;

        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolCompHist = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10'  + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoFlotillaPE + '-Arrendadora-1',
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            RecordTypeId = sRectorTypeFlotillaRep,
            TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id,
            TAM_Intercambio__c = false,
            TAM_Historico__c = true,
            TAM_EstatusDOD__c = 'Autorizado'
        );
        insert objCheckOutDetSolCompHist;
                            
    }

    public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;        
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_CancelaVinCheckoutDupBchOK() {
        //Consulta los datos del cliente
        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutCons = [Select Id, TAM_VIN__c, TAM_FechaVentaDD__c  From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 1];        
        System.debug('EN TAM_ActFechaDDChkOutBchOK objCheckOutCons: ' + objCheckOutCons);
        
        Test.startTest();
            String sQuery = 'SELECT TAM_VIN__c ';
            sQuery += ' FROM TAM_CheckOutDetalleSolicitudCompra__c';
            sQuery += ' Where TAM_Historico__c = true';
            sQuery += ' Order by TAM_VIN__c';
            sQuery += ' Limit 1';           
            System.debug('EN TAM_ActFechaDDChkOutBchOK.execute sQuery: ' + sQuery);
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_CancelaVinCheckoutDupBch_cls objCancelaVinCheckoutDupBch = new TAM_CancelaVinCheckoutDupBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objCancelaVinCheckoutDupBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_CancelaVinCheckoutDupSch_cls CancelaVinCheckoutDupSch_Jobh = new TAM_CancelaVinCheckoutDupSch_cls();
            System.schedule('Ejecuta_TAM_CancelaVinCheckoutDupSch_cls', sch, CancelaVinCheckoutDupSch_Jobh);
            
        Test.stopTest();        
    }
    
}