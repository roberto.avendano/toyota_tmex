@isTest
public class ReporteResultadoEvaluacionControllerTest {
    private static String tipoEvaluacion = 'Integral';
	public static Evaluaciones_Dealer__c getTestDealer(){
		///Registro
		String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
		String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
		RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
		RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
		RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
		
		RecordType recordTypeAPI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType='Actividad_Plan_Integral__c' and DeveloperName='API_TSM' limit 1];
		RecordType recordTypePreguntaKodawari = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Evaluacion_Integral' limit 1];
		RecordType recordTypePreguntaEI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Kodawari' limit 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
		
		User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;

		Document testDoc = new Document(
			Name='testDoc',
			DeveloperName='withoutTypeImg',
			FolderId= u.Id		
		);
		insert testDoc;

		Document testDoc2 = new Document(
			Name='Logo_TSM',
			DeveloperName='Logo_TSM',
			FolderId= u.Id		
		);
		insert testDoc2;
		
		Seccion__c seccion01 = new Seccion__c(
			Name = 'Secccion01',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccion01;
		
		Pregunta__c pregunta01 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta01;

		Pregunta__c pregunta02 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta02;		

		Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta01.Id
		);
		insert oe01;

		Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta02.Id
		);
		insert oe02;

		Account dealer = new Account(
			RecordTypeId = recordTypeDealer.Id,
			Name = 'Test Record'
		);
		insert dealer;

		Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
			RecordTypeId = recordTypeED.Id,
			Consultor_TSM_Titular__c = u.Id,
			Nombre_Dealer__c = dealer.Id
		);
		insert evalDealer;

		Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
            Evaluacion_Dealer__c = evalDealer.Id,
            Usuario__c = u.Id,
            Seccion_Toyota_Mexico__c = seccion01.Id
        );
        insert rsc01;
        
        Respuestas_Preguntas_TSM__c resp1 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta01.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=2,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp1;
		
		Respuestas_Preguntas_TSM__c resp2 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta02.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp2;
                
        update evalDealer;
       
	        Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
	            Celula_Area__c='DO',
	            Observaciones__c='Observaciones__c',
	            Condicion_Observada__c='Condicion_Observada__c',
	            Referencia__c='TSM',
	            Cuenta__c=evalDealer.Nombre_Dealer__c,
	            Respuestas_Preguntas_TSM__c=resp1.Id
	        );
	        insert api01;
        
	    	Respuestas_ObjetosTSM__c ro01 = new Respuestas_ObjetosTSM__c(
				Objeto_Evaluacion_Relacionado__c=oe01.Id, 
				Pregunta_Relacionada_del__c=resp1.Pregunta_Relacionada__c, 
				Respuestas_Preguntas_TSM_del__c=resp1.Id,
				Respuesta__c='Si'
	    	);
	    	insert ro01;
	        				
        return evalDealer;		
	}
	
	@isTest
	static void itShould(){
		Evaluaciones_Dealer__c evalDealer = ReporteResultadoEvaluacionControllerTest.getTestDealer();
		//Schema.DescribeSObjectResult evDealerSchema = Schema.SObjectType.Evaluaciones_Dealer__c;
		//Schema.RecordTypeInfo evDealerRT = evDealerSchema.getRecordTypeInfosByName().get('Autos demo');

		Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(evalDealer);
			ReporteResultadoEvaluacionController evRetencion = new ReporteResultadoEvaluacionController(stdController);
			evRetencion.isEmail=false;
			evRetencion.getObjEvaluaciones();
			evRetencion.getResumenRespuestasList();
			evRetencion.getValoresBarras();
			evRetencion.getColoresBarras();
			evRetencion.getEtiquetasBarras();
			evRetencion.getLeyendBarras();
			evRetencion.getColoresLeyend();
			String logo = evRetencion.logoFormato;
			for(Id idsMapa: evRetencion.resumenRespuestas.keySet()){
				evRetencion.resumenRespuestas.get(idsMapa).si=3;
			}			
			evRetencion.getValoresBarras();
			evRetencion.getValoresBarras2();
			evRetencion.getBarLabels();
			
			for(Id idsMapa: evRetencion.resumenRespuestas.keySet()){
				evRetencion.resumenRespuestas.get(idsMapa).No=6;				
			}			
			evRetencion.getValoresBarras(); 
			evRetencion.resumenRespuestas.put(evalDealer.Id, new ReporteResultadoEvaluacionController.ResumenRespuestas());			
			evRetencion = new ReporteResultadoEvaluacionController();
			
		Test.stopTest();
	}
}