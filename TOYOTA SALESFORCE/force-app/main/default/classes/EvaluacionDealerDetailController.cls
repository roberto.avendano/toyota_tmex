public without sharing class EvaluacionDealerDetailController {

    public Evaluaciones_Dealer__c evalDealer{get; set;}
    public List<User> consultores{get; set;}
    public List<Seccion__c> secciones{get; set;}
    public Map<Id,List<Seccion__c>> userSeccion{get; set;}
    public Map<Id,User> consultoresSel{get; set;}
    public Map<Id,List<Respuestas_Preguntas_TSM__c>> mapSeccionPreguntas{get; set;}
    public Map<Id,Boolean> mapSeccionPreguntasPendientes{get; set;}
    public Id dealerId {get;set;}
    
    public String errores{get;set;}

    public EvaluacionDealerDetailController(ApexPages.StandardController stdController) {
        evalDealer = (Evaluaciones_Dealer__c)stdController.getRecord();
        
        if(evalDealer!=null && evalDealer.Id!=null){
            dealerId = evalDealer.id;
            evalDealer = [SELECT 
                Total_de_Respuestas_Cumplidas__c, Total_de_Respuestas_No_Cumplidas__c, Preguntas_Sin_Contestar__c, AvanceEvaluacion__c,
                CreatedDate, Evaluacion_Cerrada__c, Tipo_de_Evaluacion_Kodawari__c,
                Consultor_TSM_Titular__c, Id, Name, Nombre_Dealer__c, RecordTypeId,
                RecordType.Name, RecordType.DeveloperName,
                Nombre_Dealer__r.Id, Nombre_Dealer__r.Name,
                Consultor_TSM_Titular__r.Id, Consultor_TSM_Titular__r.Name, ResultadoEvaluacion__c,
                Perfil__c,
                (select Id, Name, Respuesta_Pregunta__c, Total_de_Objetos_Sin_Contestar__c,
                                Pregunta_Relacionada__r.Name, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c
                        from Respuestas_Preguntas_TSM__r 
                        ORDER BY Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name, Pregunta_Relacionada__r.Orden_de_Aparicion__c),
                (select Id, Name, 
                        Usuario__r.Id, Usuario__r.Name, 
                        Seccion_Toyota_Mexico__r.Id, Seccion_Toyota_Mexico__r.Name , Seccion_Toyota_Mexico__r.Orden__c 
                    from Relaciones_Seccion_Consultor__r ORDER BY Seccion_Toyota_Mexico__r.Orden__c)
                FROM Evaluaciones_Dealer__c WHERE Id =: evalDealer.Id];
        }
        errores = '';
        
        userSeccion = new Map<Id,List<Seccion__c>>();
        consultoresSel = new Map<Id,User>();
        
        Map<Id, User> valconsultores = EvaluacionKodawariWizardController.getConsultoresRA();

        Map<Id,Seccion__c> valsecciones;
        Map<String,Map<String,RecordType>> tiposRegistro = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        //FIXME 
        if(evalDealer.RecordTypeId == tiposRegistro.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id){
            valsecciones = EvaluacionKodawariWizardController.getSeccionesRA('SeccionesRetencionClientes');
        }else{
            valsecciones = EvaluacionKodawariWizardController.getSeccionesRA('Seccion_Kodawari');
        }
         //valsecciones = EvaluacionKodawariWizardController.getSeccionesRA();
                
        for(Relacion_Seccion_Consultor__c rsc : evalDealer.Relaciones_Seccion_Consultor__r){
            consultoresSel.put(rsc.Usuario__r.Id, rsc.Usuario__r);

            if(!userSeccion.containsKey(rsc.Usuario__r.Id)){
                userSeccion.put(rsc.Usuario__r.Id, new List<Seccion__c>());
            }
            userSeccion.get(rsc.Usuario__r.Id).add(rsc.Seccion_Toyota_Mexico__r);

            if(valconsultores.containsKey(rsc.Usuario__r.Id)){
                valconsultores.remove(rsc.Usuario__r.Id);
            }
            if(valsecciones.containsKey(rsc.Seccion_Toyota_Mexico__r.Id)){
                valsecciones.remove(rsc.Seccion_Toyota_Mexico__r.Id);
            }
        }
        if(valconsultores.containsKey(evalDealer.Consultor_TSM_Titular__r.Id)){
            valconsultores.remove(evalDealer.Consultor_TSM_Titular__r.Id);
        }
        consultores = valconsultores.values();
        secciones = valsecciones.values();
        
        mapSeccionPreguntas = new Map<Id,List<Respuestas_Preguntas_TSM__c>>();
        mapSeccionPreguntasPendientes = new Map<Id,Boolean>();
        for(Respuestas_Preguntas_TSM__c rp : evalDealer.Respuestas_Preguntas_TSM__r){
                if(!mapSeccionPreguntas.containsKey(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
                        mapSeccionPreguntas.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,new List<Respuestas_Preguntas_TSM__c>());
                        mapSeccionPreguntasPendientes.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,false);
                }
                mapSeccionPreguntas.get(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c).add(rp);
                if(rp.Total_de_Objetos_Sin_Contestar__c > 0){
                        mapSeccionPreguntasPendientes.put(rp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c,true);
                } 
        }
        System.debug(mapSeccionPreguntasPendientes);
        system.debug('id ProfileBecerril'+Userinfo.getProfileid());
        system.debug('Becerril tipoEVA'+evalDealer.Tipo_de_Evaluacion_Kodawari__c);
        system.debug('Becerril tiporegistro'+evalDealer.RecordTypeId);
        
        if(Constantes.PROFILES_COMMUNITY_AUTOEVAL_ONLY.contains(Userinfo.getProfileid()) 
            && Constantes.EVALUACIONES_KODAWARI_DEALER_TIPOS_AUTOEVALUACION  != evalDealer.Tipo_de_Evaluacion_Kodawari__c && 
            evalDealer.RecordTypeId != tiposRegistro.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id){
                system.debug('no tiene acceso al registro becerril');
                errores = 'No tiene acceso a este registro';
        }
        
         system.debug('error en el error'+errores);
    }

   
    
    public Boolean getTieneRespuestas(){
        return evalDealer.Respuestas_Preguntas_TSM__r!=null && evalDealer.Respuestas_Preguntas_TSM__r.size()>0 ? true : false;
    }

    public PageReference getDesbloquearEvaluacion(){
        PageReference resp = null;
        try{
            resp = new PageReference('/'+evalDealer.Id);
            evalDealer.ResultadoEvaluacion__c = '';
            evalDealer.Evaluacion_Cerrada__c = false;
            
            update evalDealer;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Se actualizo correctamente'));
        }catch(DMLException dmle){
            System.debug(dmle);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dmle.getDmlMessage(0)));
            resp = null;
        }catch(Exception e){
            System.debug(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            resp = null;
        }
        return resp;
    }

    public PageReference getAprobarEvaluacion(){
        Boolean error = false;
        try{
            evalDealer.ResultadoEvaluacion__c = 'Aprobado';
            evalDealer.Evaluacion_Cerrada__c = true;
            
            update evalDealer;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Se actualizo correctamente'));
        }catch(DMLException dmle){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dmle.getDmlMessage(0)));
        }catch(Exception e){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        if(error){
            evalDealer.ResultadoEvaluacion__c = '';
            evalDealer.Evaluacion_Cerrada__c = false;
            return null;
        }else{
            return new PageReference('/'+evalDealer.Id);
        }
    }

    public PageReference getNoAprobarEvaluacion(){
        Boolean error = false;
        try{
            evalDealer.ResultadoEvaluacion__c = 'No Aprobado';
            evalDealer.Evaluacion_Cerrada__c = true;

            update evalDealer;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Se actualizo correctamente'));
        }catch(DMLException dmle){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dmle.getDmlMessage(0)));
        }catch(Exception e){
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        if(error){
            evalDealer.ResultadoEvaluacion__c = '';
            evalDealer.Evaluacion_Cerrada__c = false;
            return null;
        }else{
            return new PageReference('/'+evalDealer.Id);
        }
    }
    
    public PageReference activaCasillaEnvioCorreo(){
        Savepoint sp = Database.setSavepoint();
        try{
            // Se Evaluacion_Cerrada__c
            evalDealer.Evaluacion_Cerrada__c = false;
            update evalDealer;
            evalDealer.EnvioAlerta__c = true;
            update evalDealer;
            evalDealer.EnvioAlerta__c = false;
            evalDealer.Evaluacion_Cerrada__c = true;
            update evalDealer;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Se actualizo correctamente'));
        }catch(DMLException dmle){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, dmle.getDmlMessage(0)));
            return null;
        }catch(Exception e){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
        return new PageReference('/'+evalDealer.Id);
    }
}