/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CargaArchivosCtrl_Tst {

	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtLeadClienteCreado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cliente Creado').getRecordTypeId();    
	
	static	Account clientePrueba = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();
	static	ContentVersion clienteContVerPrueba = new ContentVersion();
	static	ContentVersion candidatoContVerPrueba = new ContentVersion();

	@TestSetup static void loadData(){
		
		Account cliente = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert cliente;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = cliente.Id
	    );
	    insert cont;  
		
        Lead cand = new Lead(
	        RecordTypeId = VaRtLeadClienteCreado,
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '224',
	        Status='Nuevo Lead',
	        LastName = 'Test3',
	        TAM_EnviarAutorizacion__c = true,
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Programa'      
        );
	    insert cand;

        ContentVersion clienteCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data2'),
                IsMajorVersion = true
        );
        insert clienteCv;
		clienteContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :clienteCv.Id LIMIT 1];
		
        ContentVersion candCv = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = blob.valueof('Test Content Data1'),
                IsMajorVersion = true
        );
        insert candCv;
        candidatoContVerPrueba = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :candCv.Id LIMIT 1];
             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}


    static testMethod void TAM_CargaArchivosCtrlTstOkCliente() {
		
		//Consulta los datos del cliente
		clientePrueba = [Select Id, Name, Codigo_Distribuidor__c, TAM_EstatusCliente__c From Account LIMIT 1];
		//contactoPrueba = [Select Id, Name From Contact LIMIT 1];
        //candidatoPrueba = [Select Id, Name From Lead LIMIT 1];
        
   		System.debug('EN TAM_CargaArchivosCtrlTstOk clientePrueba: ' + clientePrueba);
   		System.debug('EN TAM_CargaArchivosCtrlTstOk clienteContVerPrueba: ' + clienteContVerPrueba);	
		//System.debug('EN TAM_CargaArchivosCtrlTstOk contactoPrueba: ' + contactoPrueba);
		//System.debug('EN TAM_CargaArchivosCtrlTstOk candidatoPrueba: ' + candidatoPrueba);	
		
		//Consulta los datos del documento			
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId, Description FROM ContentDocument];
        documents.get(0).Description = 'Acta Constitutiva';
        update documents.get(0);
        
        //Crea la liga hacia el documento
        ContentDocumentLink contentlink=new ContentDocumentLink(
        	 LinkedEntityId = clientePrueba.id,
             ShareType= 'V',
             ContentDocumentId = documents.get(0).Id,
             Visibility = 'AllUsers'
        );
        insert contentlink;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo getFiles
       TAM_CargaArchivosCtrl.getFiles(clientePrueba.id, 'Carta Compromiso');
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(clientePrueba.id, 'Carta Compromiso', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(clientePrueba.id, 'Orden de compra', documents.get(0).id);
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(clientePrueba.id);
       
    }
    
    static testMethod void TAM_CargaArchivosCtrlTstOkLead() {
		
		//Consulta los datos del cliente
		candidatoPrueba = [Select Id, Name, TAM_EnviarAutorizacion__c, RecordTypeId, FWY_Tipo_de_persona__c, 
		TAM_TipoCandidato__c From Lead LIMIT 1];        
   		System.debug('EN TAM_CargaArchivosCtrlTstOk candidatoPrueba: ' + candidatoPrueba);
				
		//Consulta los datos del documento			
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId, Description FROM ContentDocument];
        documents.get(0).Description = 'Acta Constitutiva';
        update documents.get(0);
        
        //Crea la liga hacia el documento
        ContentDocumentLink contentlink=new ContentDocumentLink(
        	 LinkedEntityId = candidatoPrueba.id,
             ShareType= 'V',
             ContentDocumentId = documents.get(0).Id,
             Visibility = 'AllUsers'
        );
        insert contentlink;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo getFiles
       TAM_CargaArchivosCtrl.getFiles(candidatoPrueba.id, 'Acta Constitutiva');
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Acta Constitutiva', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Cedula Fiscal', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'PDF datos adicionales', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Orden de compra', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Identificación oficial', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Comprobante de domicilio', documents.get(0).id);
       //Llama la clase de TAM_CargaArchivosCtrl y metodo updFileDesc
       TAM_CargaArchivosCtrl.updFileDesc(candidatoPrueba.id, 'Documentación de soporte', documents.get(0).id);
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(candidatoPrueba.id);
              
       candidatoPrueba.TAM_EnviarAutorizacion__c = false;
	   candidatoPrueba.FWY_Tipo_de_persona__c = 'Person física';
	   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
       update candidatoPrueba;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(candidatoPrueba.id);       

       candidatoPrueba.TAM_EnviarAutorizacion__c = false;
	   candidatoPrueba.FWY_Tipo_de_persona__c = 'Persona moral';
	   candidatoPrueba.TAM_TipoCandidato__c = 'Programa'; 
       update candidatoPrueba;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(candidatoPrueba.id);
       
       candidatoPrueba.TAM_EnviarAutorizacion__c = false;
	   candidatoPrueba.FWY_Tipo_de_persona__c = 'Person física';
	   candidatoPrueba.TAM_TipoCandidato__c = 'Flotilla'; 
       update candidatoPrueba;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(candidatoPrueba.id);
       
       candidatoPrueba.TAM_EnviarAutorizacion__c = false;
	   candidatoPrueba.FWY_Tipo_de_persona__c = 'Persona moral';
	   candidatoPrueba.TAM_TipoCandidato__c = 'Flotilla'; 
       update candidatoPrueba;
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.consultDatosCand(candidatoPrueba.id);       
       
       //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
       TAM_CargaArchivosCtrl.deleteFiles(documents.get(0).id);
       
    }
    
    
}