@isTest
public class TAM_ScheduledSolicitudIncentivos_Test {
    
    @testSetup static void setup() {
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        //Dealer de prueba
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordtypeId = recordTypeDistribuidor
        );
        insert a01;
        
        TAM_EstadoCuenta__c edoCta = new TAM_EstadoCuenta__c();
        edoCta.name = 'JULIO-TEST';
        edoCta.TAM_EstadoRegistro__c = 'Abierto';
        edoCta.TAM_CodigoDealer__c = '57002';
        edoCta.TAM_NombreDealer__c  = 'TOYOTA DE PRUEBA';
        edoCta.TAM_FechaInicio__c = date.today();
        edoCta.TAM_FechaCierre__c = date.today()+90;
        insert edoCta;
        
        //Se crean las solicitudes de bono 
        TAM_SolicitudDeBono__c solBono01 = new TAM_SolicitudDeBono__c();
        solBono01.TAM_AnioModelo__c = '2020';
        solBono01.TAM_AprobacionFinanzas__c = 'Aprobado';
        solBono01.TAM_AprobacionVentas__c = 'Aprobado';
        solBono01.TAM_CodigoModelo__c = '1251';
        solBono01.TAM_CodigoVenta__c = '03';
        solBono01.TAM_ComentarioDealer__c = 'Test de comentario';
        solBono01.TAM_ComentarioFinanzas__c = 'Aprabado finanzas';
        solBono01.TAM_ComentarioVentas__c = 'Prueba de ventas';
        solBono01.TAM_CumpleConCondiciones__c = true;
        solBono01.TAM_Distribuidor__c = a01.id;
        solBono01.TAM_EstatusSolicitudBono__c = 'En proceso aprobación (Ventas)';
        solBono01.TAM_EstatusVIN__c = 'VIN en Dealer Daily';
        solBono01.TAM_ExisteEdoCta__c = false;
        solBono01.TAM_FechaVenta__c = date.today();
        solBono01.TAM_Fecha_Autorizacion_DM__c = date.today();
        solBono01.TAM_Fecha_Autorizacion_Finanzas__c = date.today();
        solBono01.TAM_Serie__c = 'PRIUS';
        solBono01.TAM_VIN__c	 = 'JTDKARFU2L3122771';
        insert solBono01;
        
        //Se crea la serie 
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        //Se crea el modelo
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        //Se crea el Vehiculo relacionado 
         Vehiculo__c v01 = new Vehiculo__c(
            Name='JTDKARFU2L3122771',
            Id_Externo__c='JTDKARFU2L3122771'
        );
        insert v01;
        
         Vehiculo__c v03 = new Vehiculo__c(
            Name='KARFU2L3122TEST',
            Id_Externo__c='KARFU2L3122TEST'
        );
        insert v03;
        
		 Vehiculo__c v05 = new Vehiculo__c(
            Name='KARFU2L3122TES01',
            Id_Externo__c='KARFU2L3122TES01'
        );
        insert v05;        
        
        //Se crea el movimiento relacionado
         Movimiento__c m05 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            Sale_Date__c = date.today(),
            VIN__c = v05.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '04',
            Fleet__c = 'C'
        ); 
        insert m05;
        
        
            Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now(),
            Sale_Date__c = date.today(),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '04',
            Fleet__c = 'C'
        ); 
        insert m01;
        
        //Se crea el movimiento relacionado
            Movimiento__c m02 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now().addDays(40),
            Sale_Date__c = date.today()+40,
            VIN__c = v03.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '04',
            Fleet__c = 'C'
        ); 
        insert m02;
        
        
        //Se crea el Vehiculo relacionado 
         Vehiculo__c v02 = new Vehiculo__c(
            Id =  v01.id ,
            Name='JTDKARFU2L3122771',
            Id_Externo__c='JTDKARFU2L3122771',
            Ultimo_Movimiento__c = m01.id
        );
        update v02;
        
        //Se crea el Vehiculo relacionado 
         Vehiculo__c v06 = new Vehiculo__c(
            Id =  v05.id ,
            Name='KARFU2L3122TES01',
            Id_Externo__c='KARFU2L3122TES01',
            Ultimo_Movimiento__c = m05.id
        );
        update v06;
        
        //Se crea el Vehiculo relacionado 
         Vehiculo__c v04 = new Vehiculo__c(
            Id =  v03.id ,
            Name='KARFU2L3122TEST',
            Id_Externo__c='KARFU2L3122TEST',
            Ultimo_Movimiento__c = m02.id
        );
        update v04;
        
        
        //Se crea solicitud de excepciones de tipo retail
        String recordIdSolicitudRetail = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Cambio_de_Incentivo').getRecordTypeId();
        String recordIdSolicitudAutoDemo = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Auto_Demo').getRecordTypeId();
        String recordIdSolicitudVC = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('ExcepcionVentaCorporativa').getRecordTypeId();
        
        TAM_SolicitudExpecionIncentivo__c solExcepcion = new TAM_SolicitudExpecionIncentivo__c();
            solExcepcion.RecordTypeId = recordIdSolicitudRetail;
            solExcepcion.TAMVIN__c	 = 'JTDKARFU2L3122771';
            solExcepcion.TAM_AprobacionDM__c = 'Aprobado';
            solExcepcion.TAM_AprobacionFinanzas__c = 'Aprobado';
            solExcepcion.TAM_ComentarioDM__c = 'Comenatario de test';
            solExcepcion.TAM_ComentarioFinanzas__c = 'De Test';
            solExcepcion.TAM_Comentario__c = 'De Test';
            solExcepcion.TAM_Distribuidor__c  =   a01.id;
            solExcepcion.TAM_Email_DM__c = 'test@test.com';
            solExcepcion.TAM_EstadoCuenta__c = false;
            solExcepcion.TAM_Fecha_Autorizacion_DM__c = date.today()-1;
            solExcepcion.TAM_Fecha_Autorizacion_Finanzas__c = date.today();
            solExcepcion.TAM_MotivoSolicitud__c = 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.';
            solExcepcion.TAM_TipoVenta__c = 'Con TFS';
			insert solExcepcion;
        
        //Auto Demo
        TAM_SolicitudExpecionIncentivo__c solExcepcion02 = new TAM_SolicitudExpecionIncentivo__c();
            solExcepcion02.RecordTypeId = recordIdSolicitudAutoDemo;
            solExcepcion02.TAMVIN__c	 = 'KARFU2L3122TEST';
            solExcepcion02.TAM_AprobacionDM__c = 'Aprobado';
            solExcepcion02.TAM_AprobacionFinanzas__c = 'Aprobado';
            solExcepcion02.TAM_ComentarioDM__c = 'Comenatario de test';
            solExcepcion02.TAM_ComentarioFinanzas__c = 'De Test';
            solExcepcion02.TAM_Comentario__c = 'De Test';
            solExcepcion02.TAM_Distribuidor__c  =   a01.id;
            solExcepcion02.TAM_Email_DM__c = 'test@test.com';
            solExcepcion02.TAM_EstadoCuenta__c = false;
            solExcepcion02.TAM_Fecha_Autorizacion_DM__c = date.today()-1;
            solExcepcion02.TAM_Fecha_Autorizacion_Finanzas__c = date.today();
            solExcepcion02.TAM_MotivoSolicitud__c = 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.';
            solExcepcion02.TAM_TipoVenta__c = 'Con TFS';
			insert solExcepcion02;
        
        //Venta Corporativa
        TAM_SolicitudExpecionIncentivo__c solExcepcion03 = new TAM_SolicitudExpecionIncentivo__c();
            solExcepcion03.RecordTypeId = recordIdSolicitudVC;
            solExcepcion03.TAMVIN__c	 = 'KARFU2L3122TES01';
            solExcepcion03.TAM_AprobacionDM__c = 'Aprobado';
            solExcepcion03.TAM_AprobacionFinanzas__c = 'Aprobado';
            solExcepcion03.TAM_ComentarioDM__c = 'Comenatario de test';
            solExcepcion03.TAM_ComentarioFinanzas__c = 'De Test';
            solExcepcion03.TAM_Comentario__c = 'De Test';
            solExcepcion03.TAM_Distribuidor__c  =   a01.id;
            solExcepcion03.TAM_Email_DM__c = 'test@test.com';
            solExcepcion03.TAM_EstadoCuenta__c = false;
            solExcepcion03.TAM_Fecha_Autorizacion_DM__c = date.today()-1;
            solExcepcion03.TAM_Fecha_Autorizacion_Finanzas__c = date.today();
            solExcepcion03.TAM_MotivoSolicitud__c = 'El cliente adquirió el auto con las promociones del mes pasado, pero solicita que se le apliquen las vigentes.';
            solExcepcion03.TAM_TipoVenta__c = 'Con TFS';
			insert solExcepcion03;
        
        	//Se inserta el custom Setting de días de vigencia
			TAM_DetalleEdoCta__c customSetting1 = new TAM_DetalleEdoCta__c();
        	customSetting1.TAM_DiasVigencia__c = 97;
        	insert customSetting1;
		        
    }
    
    
    @isTest static void testMethod1(){
        Test.startTest();
        TAM_ScheduledSolicitudIncentivos sh1 = new TAM_ScheduledSolicitudIncentivos();
        String sch = '0 0 2 * * ?'; 
        system.schedule('Test', sch, sh1); 
        
        Test.stopTest();
    }
}