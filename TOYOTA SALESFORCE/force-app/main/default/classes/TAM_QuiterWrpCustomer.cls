/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de Customer para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_QuiterWrpCustomer {

    public class QuiterCustomerCreateUpd{

        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String externalId {get;set;}
        @AuraEnabled public String name {get;set;}
        @AuraEnabled public String surname {get;set;}   
        @AuraEnabled public String surname2 {get;set;}   
        @AuraEnabled public String documentId {get;set;}
        @AuraEnabled public String category {get;set;}
        @AuraEnabled public String email {get;set;}
        @AuraEnabled public String fullName {get;set;}
        @AuraEnabled public String gender {get;set;} //H, M
        @AuraEnabled public String mobilePhoneNumber {get;set;}   
        @AuraEnabled public String phoneNumber {get;set;}     
        @AuraEnabled public String registerDate {get;set;}

        @AuraEnabled public String postalCode {get;set;}             
        @AuraEnabled public String street {get;set;}        
        @AuraEnabled public String streetNumber {get;set;}   
        @AuraEnabled public String colony {get;set;}
        @AuraEnabled public String delegation {get;set;}
        @AuraEnabled public String city {get;set;}
        @AuraEnabled public String state {get;set;}       
        @AuraEnabled public String country {get;set;}        
        @AuraEnabled public String address {get;set;}    

        @AuraEnabled public String fiscalAddress {get;set;}    
        @AuraEnabled public String fiscalAddressNumber {get;set;}    

        @AuraEnabled public List<QuiterContact> contacts {get;set;}
        
        //Un contructor por default
        public QuiterCustomerCreateUpd(){
            this.id = '';
            this.externalId = '';
            this.name = '';
            this.surname = '';   
            this.surname2 = '';   
            this.documentId = '';
            this.category = '';
            this.email = '';
            this.fullName = '';
            this.gender = ''; //H, M
            this.mobilePhoneNumber = '';   
            this.phoneNumber = '';     
            this.registerDate = '';   
    
            this.postalCode = '';             
            this.street = '';        
            this.streetNumber = '';   
            this.colony = '';
            this.delegation = '';
            this.city = '';
            this.state = '';       
            this.country = '';        
            this.address = '';    

            this.fiscalAddress = '';
            this.fiscalAddressNumber = '';
    
            this.contacts = new List<QuiterContact>();
        }
        
    }

    public class QuiterCustomerCreateUpdResp{

        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String externalId {get;set;}
        @AuraEnabled public String name {get;set;}
        @AuraEnabled public String surname {get;set;}   
        @AuraEnabled public String surname2 {get;set;}   
        @AuraEnabled public String documentId {get;set;}
        @AuraEnabled public String category {get;set;}
        @AuraEnabled public String email {get;set;}
        @AuraEnabled public String fullName {get;set;}
        @AuraEnabled public String gender {get;set;} //H, M
        @AuraEnabled public String mobilePhoneNumber {get;set;}   
        @AuraEnabled public String phoneNumber {get;set;}     
        @AuraEnabled public String registerDate {get;set;}   

        @AuraEnabled public String postalCode {get;set;}             
        @AuraEnabled public String street {get;set;}        
        @AuraEnabled public String streetNumber {get;set;}   
        @AuraEnabled public String colony {get;set;}
        @AuraEnabled public String delegation {get;set;}
        @AuraEnabled public String city {get;set;}
        @AuraEnabled public String state {get;set;}       
        @AuraEnabled public String country {get;set;}        
        @AuraEnabled public String address {get;set;}    

        @AuraEnabled public String fiscalAddress {get;set;}    
        @AuraEnabled public String fiscalAddressNumber {get;set;}    
        
        @AuraEnabled public List<QuiterContact> contacts {get;set;}
        
        //Un contructor por default
        public QuiterCustomerCreateUpdResp(){
            this.id = '';
            this.externalId = '';
            this.name = '';
            this.surname = '';   
            this.surname2 = ''; 
            this.documentId = '';  
            this.category = '';  
            this.email = '';
            this.fullName = '';
            this.gender = ''; //H, M
            this.mobilePhoneNumber = '';   
            this.phoneNumber = '';     
            this.registerDate = '';   
    
            this.postalCode = '';             
            this.street = '';        
            this.streetNumber = '';   
            this.colony = '';
            this.delegation = '';
            this.city = '';
            this.state = '';       
            this.country = '';        
            this.address = '';    
            
            this.fiscalAddress = '';
            this.fiscalAddressNumber = '';
            
            this.contacts = new List<QuiterContact>();
        }
        
    }

    public class QuiterContact {

        @AuraEnabled public String comments {get;set;}
        @AuraEnabled public String degree {get;set;}
        @AuraEnabled public String email {get;set;}   
        @AuraEnabled public String name {get;set;}   
        @AuraEnabled public String telephone {get;set;}
        @AuraEnabled public String telephoneTime {get;set;}

        //Un contructor por default
        public QuiterContact(){
            this.comments = '';
            this.degree = '';
            this.email = '';   
            this.name = '';   
            this.telephone = '';
            this.telephoneTime = '';
        }
        
        //Un contructor por default
        public QuiterContact(String comments, String degree, String email, String name, String telephone,
            String telephoneTime){
            this.comments = comments;
            this.degree = degree;
            this.email = email;   
            this.name = name;   
            this.telephone = telephone;
            this.telephoneTime = telephoneTime;
        }        
        
    }
        
}