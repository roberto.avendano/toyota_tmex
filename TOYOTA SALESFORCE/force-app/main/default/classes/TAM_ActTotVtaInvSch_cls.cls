/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_MovimientosSolicitudes__c
                        y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ActTotVtaInvSch_cls implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActTotVtaInvSch_cls.execute...');
        
        String strTipoPedido = 'Inventario';
        String strVinPrueba = '5TDGZRAH0MS049120';
        String EstatusDOD = 'Cancelado';
        String EstDealerSol = 'Cancelada';
        String EstDealerSol2 = 'Rechazada';        
        
        String sFechaActTotMov = System.Label.TAM_FechaMesToyota;        
        Date dFechaConsulta = sFechaActTotMov != 'null' ? Date.valueOf(sFechaActTotMov) : Date.today();
        Date dtFechaIniConsPrm = Date.today();
        Date dtFechaFinConsPrm = Date.today();

        if (Test.isRunningTest()) dFechaConsulta = Date.today();
        //Consulta las fechas en el calendario de toyota
        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
            From TAM_CalendarioToyota__c t Where TAM_FechaInicio__c <=:dFechaConsulta
            And TAM_FechaFin__c >=:dFechaConsulta]){
            //Coincide la fecha del calendario Toyota
            dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
            dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
            System.debug('EN TAM_ActCancelSolInvSch_cls.execute dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
        }//Fin del for para TAM_CalendarioToyota__c
        
        this.sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
        this.sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_NombreDistribuidor__c ';
        this.sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
        this.sQuery += ' Where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
        //this.sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= ' + String.valueOf(dtFechaIniConsPrm); //2021-02-08';
        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c <= ' + String.valueOf(dtFechaFinConsPrm); //2021-02-08';
        this.sQuery += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';

        this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c != null';

        this.sQuery += ' And TAM_EstatusDOD__c != \'' + String.escapeSingleQuotes(EstatusDOD) + '\'';
        this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol) + '\'';
        this.sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol2) + '\'';

        this.sQuery += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActTotVtaInvSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActTotVtaInvBch_cls objActTotVtaInvBch = new TAM_ActTotVtaInvBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActTotVtaInvBch, 25);
                     
    }
    
}