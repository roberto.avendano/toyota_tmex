/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento del TAM_CargaDocumentosController estado de cuenta (INCENTIVOS)
******************************************************************************* */
public class TAM_CargaDocumentosController {
    
    
    @AuraEnabled
    public static TAM_DetalleEstadoCuenta__c saveDocumentId(String idRecord, String idDocument, String nombreDocumento){
        
        TAM_DetalleEstadoCuenta__c detalleIncRecord  = [SELECT id,TAMDocumento_Id__c,TAM_NombreDocumento__c FROM TAM_DetalleEstadoCuenta__c WHERE id =: idRecord];
        detalleIncRecord.TAMDocumento_Id__c = idDocument;
        detalleIncRecord.TAM_NombreDocumento__c	= nombreDocumento;
        
        try{
            update detalleIncRecord;
            return detalleIncRecord;
        }catch(Exception e){
            system.debug('Error al actualizar la linea'+e.getMessage());
            return null;
        }
        
    } 
    
    @AuraEnabled
    public static void deleteDocument (String documentId, String idRecordSolicitud){
        ContentDocument[] documentRecord;
        documentRecord = [Select id from ContentDocument WHERE id =: documentId];
        if(documentRecord.size () > 0){
            delete documentRecord[0]; 
        }
        
        TAM_DetalleEstadoCuenta__c detalleIncRecord  = [SELECT id,TAMDocumento_Id__c,TAM_NombreDocumento__c FROM TAM_DetalleEstadoCuenta__c WHERE id =: idRecordSolicitud];
        detalleIncRecord.TAMDocumento_Id__c = null;
        detalleIncRecord.TAM_NombreDocumento__c = null;     
        
        try{
            update detalleIncRecord;  
        }catch(Exception e){
            system.debug('Error'+e.getMessage());
        }
    }
}