public class CustomPaginationExt{

	public ApexPages.Standardsetcontroller acctSSC {get;set;}
	public Account Informacion_Cuentas{get;set;}
	public String Record {get; set;}
	public String Condicion {get; set;}
	public String traeCli{get; set;}
	public List<Cli__c>lineItems{get;set;}	
	public String Cuenta_id{public get;public set;}	
    public String Modificar{get;set;}
    public Integer alert{get;set;}
	public boolean displayPopup {get; set;}
	
	/*Constructor*/	
	public CustomPaginationExt(){
	
		//updateStandardSetController();
		Condicion = '';
		Record = 'SELECT Name, Correo_Electronico__c, Fecha_de_Nacimiento__c, Cuidad__c, Estado__c, Codigo_Postal__c, Calle__c  FROM Account ';
		traeCli = 'Select id, Name, Cuenta__c, Cuenta__r.Name, VIN__c, Vehiculo__r.Name, Vehiculo__r.Modelo__c, Vehiculo__r.Version__c From CLI__c';
		Informacion_Cuentas = new Account();
		
		lineItems =[Select id, Name, Cuenta__c, Cuenta__r.Name, VIN__c, Vehiculo__r.Name, Vehiculo__r.Modelo__c, Vehiculo__r.Version__c from CLI__c limit 10];
		
	
		acctSSC = new ApexPages.Standardsetcontroller(Database.query('SELECT Name, Correo_Electronico__c, Fecha_de_Nacimiento__c, Cuidad__c, Estado__c, Codigo_Postal__c, Calle__c FROM Account order by Name limit 2000'));
		
	
	}

	/*Metodo principal de la tabla*/
public void updateStandardSetController(){
 		alert = 0;
		Condicion = '';
     
/*Evaluo el nombre de la cuenta*/
if(Informacion_Cuentas.Name != null){
	   Integer resultName = Condicion.length();
		if(resultName == 0){
			Condicion += 'Name LIKE \'%'+ Informacion_Cuentas.Name +'%\'';
		}else{
			Condicion += 'AND Name LIKE \'%'+ Informacion_Cuentas.Name +'%\'';
			alert++;
		}
	}
/*Evaluo el correo electrónico de la cuenta*/
	if(Informacion_Cuentas.Correo_Electronico__c != null){
	   Integer resultEmail = Condicion.length();
		if(resultEmail == 0){
			Condicion += 'Correo_Electronico__c '+ Informacion_Cuentas.Correo_Electronico__c +'';
		}else{
			Condicion += 'AND Correo_Electronico__c LIKE \'%' + Informacion_Cuentas.Correo_Electronico__c +'%\'';
			alert++;
		}
	}	
/*Evaluo la fecha de nacimiento de la cuenta*/	
 	if(Informacion_Cuentas.Calle__c != null){
 		
 		
 	   Integer resultNato = Condicion.length();
 		if(resultNato == 0){
 			Condicion += ' Calle__c LIKE \'%' + Informacion_Cuentas.Calle__c + '%\'';
 		}else{
 			Condicion += ' AND Calle__c LIKE \'%' + Informacion_Cuentas.Calle__c + '%\'';
 			alert++;
 		}
 	}
/*Evaluo la ciudad de la cuenta*/ 	
 	if(Informacion_Cuentas.Cuidad__c != null){
 		Integer resultCiudad = Condicion.length();
 		if(resultCiudad == 0){
 			Condicion += 'Cuidad__c LIKE \'%' + Informacion_Cuentas.Cuidad__c + '%\'';
 		}else{
 			Condicion += 'AND Cuidad__c LIKE \'%' + Informacion_Cuentas.Cuidad__c + '%\'';
 			alert++;
 		}
 	}
/*Evaluo el estado de la cuenta*/ 	
 	if(Informacion_Cuentas.Estado__c != null){
 		Integer resultEstado = Condicion.length();
 		 if(resultEstado == 0){
 			Condicion += 'Estado__c LIKE \'%'+ Informacion_Cuentas.Estado__c + '%\'';
   		}else{
 		Condicion += 'AND Estado__c LIKE \'%' + Informacion_Cuentas.Estado__c + '%\'';
 		alert++;
 	}
}
/*Evaluo el código postal de la cuenta*/ 	
 	if(Informacion_Cuentas.Codigo_Postal__c != null){
 		Integer resultPostal = Condicion.length();
 		 if(resultPostal == 0){
 		  Condicion += ' Codigo_Postal__c = \'' + Informacion_Cuentas.Codigo_Postal__c + '\'';
   }else{
 	    Condicion += ' AND Codigo_Postal__c = \'' + Informacion_Cuentas.Codigo_Postal__c + '\'';
 	    alert++;
 	}
 } 
	
 	Integer resultTotal = Condicion.length();

     System.debug('resultTotal: ' + resultTotal);
 	  if(resultTotal != 0){
	 	   String query;
	 	   query = Record + ' WHERE ' + Condicion +' limit 2000'; 
	 	   System.debug('hugo '+Condicion);
	 	   System.debug(query);
	 	   acctSSC = new ApexPages.Standardsetcontroller(Database.query(query));
	 	   System.debug('Evaluo'+acctSSC);	
     }else{
		acctSSC = new ApexPages.Standardsetcontroller(Database.query('SELECT Name, Correo_Electronico__c, Fecha_de_Nacimiento__c, Cuidad__c, Estado__c, Codigo_Postal__c, Calle__c FROM Account order by Name limit 2000'));
	}
	
}	

	public Pagereference limpiar(){
		Pagereference p = new Pagereference('/apex/Toyota_Search');
		p.setRedirect(TRUE);
		return p;
	}
	
	public void showPopup(){
		displayPopup = true;
		System.debug('hola Salesforce ' + Cuenta_id);
		Integer Evaluo = Cuenta_id.length();
		
		if(Evaluo != 0){
			String query_dos;
			
			query_dos = traeCli + ' WHERE Cuenta__c = \'' + Cuenta_id + '\'';
			System.debug('Consulta dos ejecutada:'+ query_dos);
			lineItems = Database.query(query_dos);
				
		}else{
			lineItems =[Select id, 
							   Name, 
							   Cuenta__c, 
							   Cuenta__r.Name, 
							   VIN__c, 
							   Vehiculo__r.Name, 
							   Vehiculo__r.Modelo__c, 
							   Vehiculo__r.Version__c 
					      from CLI__c 
					      limit 10]; 
		}
	}
	public void closePopup(){
        displayPopup = false;
    }
	
	public List<Account> getAccounts(){
		return acctSSC.getRecords();
	}
	
    public Boolean hasNext{
        get{ 
        	return acctSSC.getHasNext();
        }
        set;
    }
 
    public Boolean hasPrevious{
        get{
            return acctSSC.getHasPrevious();
        }
        set;
    }
 
    public void previous(){
        acctSSC.previous();
    }

    public void next(){
        acctSSC.next();
    }   
    
    public Pagereference Vehiculos(){
		Pagereference T = new Pagereference('/apex/Panel_Search');
		T.setRedirect(TRUE);
		return T;
	}
 
}