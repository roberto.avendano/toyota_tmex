/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Samuel Galindo
Proyecto: Toyota-Incentivos
Descripción: Clase para el desencadenador(Trigger) IN_ListaNegra_tgr para el
proceso de lista negra en el cual realizará una busqueda en el objeto Cuenta,
en caso de encontrar al cliente cambiará el status a "Boletinado", en caso
contrario va a crear la cuenta

-------------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------
0.1    13-05-2019 Samuel Galindo             Creación
*******************************************************************************/

public with sharing class IN_AccountHandler_cls {

	String VaRtAccRegCteFisicoNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	String VaRtAccRegCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    
	String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	String VaRtSolCtePreauto = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Cliente Preautorizado').getRecordTypeId();
	String VaRtSolCteActivacion = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

	// Registros Nuevos
	final List<Account> lstNuevosRegistros;

	// Registros Antiguos
	final Map<Id, Account> mapViejosRegistros;

	// Grupo Listas Negras
	final Group objGroup = getGrupo();
	
	public Group getGrupo(){
		Group GroupoPaso;
		for (Group objGroup : [
			SELECT Id, DeveloperName
			FROM Group
			WHERE DeveloperName =:Label.IN_AccountHandler_cls_var_001
			LIMIT 1
		]){
			GroupoPaso = objGroup;
		}
		
		return GroupoPaso;
	}

	// Identificador tipo de registro cuenta fisica
	final RecordType objPersonaFisica = [
		SELECT Id, DeveloperName
		FROM RecordType
		WHERE SobjectType =:Label.IN_AccountHandler_cls_var_002 AND DeveloperName =:Label.IN_AccountHandler_cls_var_003
		LIMIT 1
	];

	// Identificador tipo de registro cuenta moral
	final RecordType objPersonaMoral = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SObjectType =:Label.IN_AccountHandler_cls_var_002 AND DeveloperName =:Label.IN_AccountHandler_cls_var_004
			LIMIT 1
	];

	
	// Constructor de la clase
	public IN_AccountHandler_cls(List<Account> lstNuevosRegistros, Map<Id, Account> mapViejosRegistros) {
		this.lstNuevosRegistros = lstNuevosRegistros;
		this.mapViejosRegistros = mapViejosRegistros;
	}

	public void actualizaCuenta(){
		// Creacion
		if (lstNuevosRegistros.size() > 0 && mapViejosRegistros == null) {
		// Actualizacion
		} else {
			// Id cuenta lista negra
			Set<String> setIdAccountLN = new Set<String>();
			for (Account objAccount : lstNuevosRegistros) {
				Account objAccountViejo = mapViejosRegistros.get(objAccount.Id);
			}
		}
	}
	
	//Actualiza el estatus y ponlo en no vigente
	public void actualizaEstatusVigente(){
		System.debug('EN IN_AccountHandler_cls.actualizaEstatusVigente..');
		Map <String, Boolean> mapTipocuentaPersonal = new Map <String, Boolean>();
		//Recorre los reg de las cuentas que se estan actualizando y ve de que tipo son
		for (Account objAccount :  [Select a.IsPersonAccount, a.Id From Account a
			where id IN :lstNuevosRegistros]){
			mapTipocuentaPersonal.put(objAccount.id, objAccount.IsPersonAccount);		
		}
		System.debug('EN IN_AccountHandler_cls.actualizaEstatusVigente mapTipocuentaPersonal: ' + mapTipocuentaPersonal);
		
		//Recorre la lista de lstNuevosRegistros
		for (Account objAccount : lstNuevosRegistros) {
			Account objAccountViejo = mapViejosRegistros.get(objAccount.Id);
			System.debug('EN IN_AccountHandler_cls.actualizaEstatusVigente objAccount.TAM_EstatusCliente__c: ' + objAccount.TAM_EstatusCliente__c + ' objAccountViejo.TAM_EstatusCliente__c: ' + objAccountViejo.TAM_EstatusCliente__c);
			//Ve si el estatus es no vigente en el campo de TAM_EstatusCliente__c
			if (objAccount.TAM_EstatusCliente__c == 'Vencido' && objAccountViejo.TAM_EstatusCliente__c ==  'Vigente'){
				if (mapTipocuentaPersonal.containsKey(objAccount.id)){
					if (mapTipocuentaPersonal.get(objAccount.id))
						objAccount.recordTypeId = VaRtAccRegCteFisicoNoVigente;
					if (!mapTipocuentaPersonal.get(objAccount.id))
						objAccount.recordTypeId = VaRtAccRegCteMoralNoVigente;	
					System.debug('EN IN_AccountHandler_cls.actualizaEstatusVigente objAccount.recordTypeId: ' + objAccount.recordTypeId);
				}//Fin si mapTipocuentaPersonal.containsKey(objAccount.id)	
			}//Fin si objAccount.TAM_EstatusCliente__c == 'No Vigente' && objAccountViejo.TAM_EstatusCliente__c ==  'Vigente'
			
		}	
	
	}

	//Actualiza el estatus y ponlo en no vigente
	public void creaPreautorizacionCliente(){
		System.debug('EN IN_AccountHandler_cls.creaPreautorizacionCliente..');
		
        System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls.creaPreautorizacionCliente');
        List<Account> ClienteUpd = new List<Account>();
        List<TAM_SolicitudesParaAprobar__c> lTAM_SolicitudesParaAprobarUps = new List<TAM_SolicitudesParaAprobar__c>();   
        Map<String, String> mapNomCandIdCand = new Map<String, String>();

		//this.lstNuevosRegistros = lstNuevosRegistros;
		//this.mapViejosRegistros = mapViejosRegistros;
        
        for(Account Cte: lstNuevosRegistros){
            
            //Ve si el campo de solicitar aprobación es verdadero
            if (Cte.TAM_EnviarPreautorizacion__c && !mapViejosRegistros.get(Cte.id).TAM_EnviarPreautorizacion__c){
                //Crea el registro en la tabla de TAM_SolicitudesParaAprobar 
                TAM_SolicitudesParaAprobar__c objTAM_SolicitudesParaAprobar = new TAM_SolicitudesParaAprobar__c();
                System.debug('EN IN_AccountHandler_cls.creaPreautorizacionCliente Tipo de Reg: ' + Cte.RecordTypeId + ' Cte.Name: ' + Cte.Name + ' ' +  Cte.FirstName + ' ' + Cte.LastName + ' ' + Cte.Apellido_Materno__c);
                String sNombreSolcitud = (Cte.RecordTypeId == VaRtAccRegCteMoralNoVigente || Cte.RecordTypeId == VaRtCteMoralPreAuto) ? Cte.Name : Cte.FirstName.toUpperCase() + ' ' + Cte.LastName.toUpperCase() + ' ' + (Cte.Apellido_Materno__c != null ? Cte.Apellido_Materno__c.toUpperCase() : '');
                //objTAM_SolicitudesParaAprobar.Name = Cte.FirstName.toUpperCase() + ' ' + Cte.LastName.toUpperCase() + ' ' + (Cte.FWY_ApellidoMaterno__c != null ? Cte.FWY_ApellidoMaterno__c.toUpperCase() : '');
                objTAM_SolicitudesParaAprobar.Name = sNombreSolcitud;
                //objTAM_SolicitudesParaAprobar.TAM_Candidato__c = Cte.id;
                objTAM_SolicitudesParaAprobar.TAM_ClienteCorporativoRef__c = Cte.id;
                objTAM_SolicitudesParaAprobar.TAM_Estatus__c = 'Proceso';
                objTAM_SolicitudesParaAprobar.TAM_RazonSocial__c = sNombreSolcitud;
                objTAM_SolicitudesParaAprobar.TAM_NombreComercial__c = Cte.TAM_NombreComercial__c;
                //objTAM_SolicitudesParaAprobar.TAM_RangoFinall__c = Cte.Rango__c;
                objTAM_SolicitudesParaAprobar.RecordTypeId = VaRtSolCtePreauto;
                //Mete a la lista de lTAM_SolicitudesParaAprobarUps el objeto de  objTAM_SolicitudesParaAprobar
                lTAM_SolicitudesParaAprobarUps.add(objTAM_SolicitudesParaAprobar);
                //Toma los archivos adjuntos que tiene asociado el Account y crealos ahora asociadoa a  la nueva objTAM_SolicitudesParaAprobar
                mapNomCandIdCand.put(objTAM_SolicitudesParaAprobar.Name, Cte.id);
                //Crea una lista para actualiza el contacto asociado
                ClienteUpd.add(New Account(ID = Cte.id, TAM_FechaEnvioPreautorizacion__c = Date.today()));
            }//Fin si Cte.TAM_EnviarPreautorizacion__c
            
            System.debug('EN IN_AccountHandler_cls lTAM_SolicitudesParaAprobarUps: ' + lTAM_SolicitudesParaAprobarUps);
            Integer iCnt = 0;
            Map<String, String> mapIdSolFinIdCand = new Map<String, String>();
            //Ve si tiene algo la lista de lTAM_SolicitudesParaAprobarUps
            if (!lTAM_SolicitudesParaAprobarUps.isEmpty()){
                List<Database.SaveResult> lSaveResultSol = Database.insert(lTAM_SolicitudesParaAprobarUps);
                //Recorre la lista de lSaveResultSol
                for (Database.SaveResult objSvResul : lSaveResultSol){
                    if (objSvResul.isSuccess()){
                        //Toma el nombre del rg que se esta actualizando
                        String sName = lTAM_SolicitudesParaAprobarUps.get(iCnt).Name;
                        String sIdCand = lTAM_SolicitudesParaAprobarUps.get(iCnt).TAM_ClienteCorporativoRef__c;
                        //Ve si existe en el mapa de mapNomCandIdCand
                        if (mapNomCandIdCand.containsKey(sName))
                            mapIdSolFinIdCand.put(sIdCand, objSvResul.getId());
                    }//Fin si objSvResul.isSuccess()
                    iCnt++;	
                }//Fin del for para lSaveResultSol
            }//Fin si lTAM_SolicitudesParaAprobarUps.isEmpty())
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls mapIdSolFinIdCand: ' + mapIdSolFinIdCand);
            List<ContentDocumentLink> lContentDocumentLinkNew = new List<ContentDocumentLink>();
            //Ya tienes los Id de las nuevas solicitudes entonces asocia los adjuntos
            if (!mapIdSolFinIdCand.isEmpty()){
                //Recorre la lista de Documentos asociados a mapIdSolFinIdCand.values()
                for(ContentDocumentLink objContentDocumentLink : [Select c.Visibility, c.LinkedEntityId, c.Id, c.ContentDocumentId, c.ShareType 
                	From ContentDocumentLink c where LinkedEntityId = :mapIdSolFinIdCand.KeySet()]){
                   	//Crea el reg en lContentDocumentLinkNew	
                    lContentDocumentLinkNew.add(new ContentDocumentLink(
                    	Visibility = objContentDocumentLink.Visibility,
                        LinkedEntityId = mapIdSolFinIdCand.get(objContentDocumentLink.LinkedEntityId),
                        ContentDocumentId = objContentDocumentLink.ContentDocumentId,
                        ShareType = objContentDocumentLink.ShareType
                        )
                    );
                }
            }//Fin si !mapIdSolFinIdCand.isEmpty()
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls lContentDocumentLinkNew: ' + lContentDocumentLinkNew); 
            //Ve si tiene algo la lista de lContentDocumentLinkNew y crea los reg
            if (!lContentDocumentLinkNew.isEmpty())
                List<Database.SaveResult> lSaveResultArchvCand = Database.insert(lContentDocumentLinkNew);
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls ClienteUpd: ' + ClienteUpd);           
            //Ve si tiene algo la lista de lTAM_SolicitudesParaAprobarUps
            if (!ClienteUpd.isEmpty()){
                List<Database.SaveResult> lSaveResultCand = Database.update(ClienteUpd);
                for (Database.SaveResult objDtbrs : lSaveResultCand){
					if (objDtbrs.isSuccess()){
       					System.debug('EN validateRecords objDtbUpsRes.getId() AccountContactRelation: ' + objDtbrs.getId());
					}                
				}
            }
        }//Fin del for para los reg de los lstNuevosRegistros
	}

	//Actualiza el estatus y ponlo en no vigente
	public void creaActivacionCliente(){
		System.debug('EN IN_AccountHandler_cls.creaActivacionCliente..');
		
        System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls.creaActivacionCliente');
        List<Account> ClienteUpd = new List<Account>();
        List<TAM_SolicitudesParaAprobar__c> lTAM_SolicitudesParaAprobarUps = new List<TAM_SolicitudesParaAprobar__c>();   
        Map<String, String> mapNomCandIdCand = new Map<String, String>();

		//this.lstNuevosRegistros = lstNuevosRegistros;
		//this.mapViejosRegistros = mapViejosRegistros;
        for(Account Cte: lstNuevosRegistros){
            //Ve si el campo de solicitar aprobación es verdadero
            if (Cte.TAM_SolcitaActivacion__c && !mapViejosRegistros.get(Cte.id).TAM_SolcitaActivacion__c){
                //Crea el registro en la tabla de TAM_SolicitudesParaAprobar 
                TAM_SolicitudesParaAprobar__c objTAM_SolicitudesParaAprobar = new TAM_SolicitudesParaAprobar__c();
                System.debug('EN IN_AccountHandler_cls.creaActivacionCliente Tipo de Reg: ' + Cte.RecordTypeId + ' Cte.Name: ' + Cte.Name + ' ' +  Cte.FirstName + ' ' + Cte.LastName + ' ' + Cte.Apellido_Materno__c);
                String sNombreSolcitud = (Cte.RecordTypeId == VaRtAccRegCteMoralNoVigente || Cte.RecordTypeId == VaRtCteMoralPreAuto) ? Cte.Name : Cte.FirstName.toUpperCase() + ' ' + Cte.LastName.toUpperCase() + ' ' + (Cte.Apellido_Materno__c != null ? Cte.Apellido_Materno__c.toUpperCase() : '');
                //objTAM_SolicitudesParaAprobar.Name = Cte.FirstName.toUpperCase() + ' ' + Cte.LastName.toUpperCase() + ' ' + (Cte.FWY_ApellidoMaterno__c != null ? Cte.FWY_ApellidoMaterno__c.toUpperCase() : '');
                objTAM_SolicitudesParaAprobar.Name = sNombreSolcitud;
                //objTAM_SolicitudesParaAprobar.TAM_Candidato__c = Cte.id;
                objTAM_SolicitudesParaAprobar.TAM_ClienteCorporativoRef__c = Cte.id;
                //objTAM_SolicitudesParaAprobar.TAM_Estatus__c = 'Proceso';
                objTAM_SolicitudesParaAprobar.TAM_RazonSocial__c = sNombreSolcitud;
                objTAM_SolicitudesParaAprobar.TAM_NombreComercial__c = Cte.TAM_NombreComercial__c;
                //objTAM_SolicitudesParaAprobar.TAM_RangoFinall__c = Cte.Rango__c;
                objTAM_SolicitudesParaAprobar.RecordTypeId = VaRtSolCteActivacion;
                //Mete a la lista de lTAM_SolicitudesParaAprobarUps el objeto de  objTAM_SolicitudesParaAprobar
                lTAM_SolicitudesParaAprobarUps.add(objTAM_SolicitudesParaAprobar);
                //Toma los archivos adjuntos que tiene asociado el Account y crealos ahora asociadoa a  la nueva objTAM_SolicitudesParaAprobar
                mapNomCandIdCand.put(objTAM_SolicitudesParaAprobar.Name, Cte.id);
                //Crea una lista para actualiza el contacto asociado
                ClienteUpd.add(New Account(ID = Cte.id, TAM_FechaEnvioPreautorizacion__c = Date.today()));
            }//Fin si Cte.TAM_EnviarPreautorizacion__c
            
            System.debug('EN IN_AccountHandler_cls lTAM_SolicitudesParaAprobarUps: ' + lTAM_SolicitudesParaAprobarUps);
            Integer iCnt = 0;
            Map<String, String> mapIdSolFinIdCand = new Map<String, String>();
            //Ve si tiene algo la lista de lTAM_SolicitudesParaAprobarUps
            if (!lTAM_SolicitudesParaAprobarUps.isEmpty()){
                List<Database.SaveResult> lSaveResultSol = Database.insert(lTAM_SolicitudesParaAprobarUps);
                //Recorre la lista de lSaveResultSol
                for (Database.SaveResult objSvResul : lSaveResultSol){
                    if (objSvResul.isSuccess()){
                        //Toma el nombre del rg que se esta actualizando
                        String sName = lTAM_SolicitudesParaAprobarUps.get(iCnt).Name;
                        String sIdCand = lTAM_SolicitudesParaAprobarUps.get(iCnt).TAM_ClienteCorporativoRef__c;
                        //Ve si existe en el mapa de mapNomCandIdCand
                        if (mapNomCandIdCand.containsKey(sName))
                            mapIdSolFinIdCand.put(sIdCand, objSvResul.getId());
                    }//Fin si objSvResul.isSuccess()
                    iCnt++;	
                }//Fin del for para lSaveResultSol
            }//Fin si lTAM_SolicitudesParaAprobarUps.isEmpty())
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls mapIdSolFinIdCand: ' + mapIdSolFinIdCand);
            List<ContentDocumentLink> lContentDocumentLinkNew = new List<ContentDocumentLink>();
            //Ya tienes los Id de las nuevas solicitudes entonces asocia los adjuntos
            if (!mapIdSolFinIdCand.isEmpty()){
                //Recorre la lista de Documentos asociados a mapIdSolFinIdCand.values()
                for(ContentDocumentLink objContentDocumentLink : [Select c.Visibility, c.LinkedEntityId, c.Id, c.ContentDocumentId, c.ShareType 
                	From ContentDocumentLink c where LinkedEntityId = :mapIdSolFinIdCand.KeySet()]){
                   	//Crea el reg en lContentDocumentLinkNew	
                    lContentDocumentLinkNew.add(new ContentDocumentLink(
                    	Visibility = objContentDocumentLink.Visibility,
                        LinkedEntityId = mapIdSolFinIdCand.get(objContentDocumentLink.LinkedEntityId),
                        ContentDocumentId = objContentDocumentLink.ContentDocumentId,
                        ShareType = objContentDocumentLink.ShareType
                        )
                    );
                }
            }//Fin si !mapIdSolFinIdCand.isEmpty()
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls lContentDocumentLinkNew: ' + lContentDocumentLinkNew); 
            //Ve si tiene algo la lista de lContentDocumentLinkNew y crea los reg
            if (!lContentDocumentLinkNew.isEmpty())
                List<Database.SaveResult> lSaveResultArchvCand = Database.insert(lContentDocumentLinkNew);
            
            System.debug(LoggingLevel.INFO,'EN IN_AccountHandler_cls ClienteUpd: ' + ClienteUpd);           
            //Ve si tiene algo la lista de lTAM_SolicitudesParaAprobarUps
            if (!ClienteUpd.isEmpty()){
                List<Database.SaveResult> lSaveResultCand = Database.update(ClienteUpd);
                for (Database.SaveResult objDtbrs : lSaveResultCand){
					if (objDtbrs.isSuccess()){
       					System.debug('EN validateRecords objDtbUpsRes.getId() AccountContactRelation: ' + objDtbrs.getId());
					}                
				}
            }
        }//Fin del for para los reg de los lstNuevosRegistros
	}
	
	
}