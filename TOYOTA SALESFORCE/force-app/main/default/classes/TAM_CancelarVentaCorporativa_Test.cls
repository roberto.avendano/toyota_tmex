@isTest
public class TAM_CancelarVentaCorporativa_Test {
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String srecordTypePrograma =  Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    @testSetup static void setup() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        
        
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordTypeId = recordTypeDistribuidor
        );
        insert a01;
        
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a01.id;
        insert contactRH1;
        
        Contact contactRH2 = new Contact();
        contactRH2.NombreUsuario__c = 'Contacto RH2';
        contactRH2.lastName = 'Contacto RH2';
        contactRH2.Email = 'test2@test.com';
        contactRH2.AccountId = a01.id;
        insert contactRH2;
        
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia 
            UserRole uR = [SELECT Id FROM UserRole WHERE Name='Consultor comonuevos'];
            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US',ContactId = contactRH2.id,ProfileId = p.Id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
            //Usuario 2
            User u2 = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',ContactId = contactRH1.id,ProfileId = p.Id,PortalRole  = 'Manager',
                               TimeZoneSidKey='America/Los_Angeles', UserName='test992@testorg.com');
            system.debug('usuario'+u2);
            insert u2;
            
            
            //Grupo 1
            Group grp = new Group();
            grp.name = 'Aprobador DTM (nivel 1)';
            grp.Type = 'Regular'; 
            Insert grp; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = u.Id;
            grpMem1.GroupId = grp.Id;
            Insert grpMem1;
            
            
            //Grupo 2
            Group grp2 = new Group();
            grp2.name = 'Aprobador DTM (nivel 2)';
            grp2.Type = 'Regular'; 
            Insert grp2; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = u2.Id;
            grpMem2.GroupId = grp2.Id;
            Insert grpMem2;
            
            
        }
        
        //Se crea un registro de prueba de tipo TAM_SolicitudesFlotillaPrograma__c
        Rangos__c rangoPrograma = new Rangos__c(
            Name = 'UBER',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'UBER | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoPrograma;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoFlotilla;
        
        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;
        
        
        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = srecordTypePrograma,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_NomDistribuidorPropietario__c = 'Es de Prueba',
            TAM_ComentarioDTM1__c = 'TEST',
            TAM_ComentarioDTM2__c = 'TEST',
            TAM_AprobacionDTM1__c = 'Aprobar Cancelación',
            TAM_AprobacionDTM2__c = 'Aprobar Cancelación',
            TAM_FechaCierreSolicitud__c = Date.today()-1
        );
        insert solVentaFlotillaPrograma;
        
        //Se crean lineas de tipo TAM_DODSolicitudesPedidoEspecial__c
        TAM_DODSolicitudesPedidoEspecial__c solicitudDOD = new TAM_DODSolicitudesPedidoEspecial__c();
        solicitudDOD.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        solicitudDOD.TAM_VIN__c = 'QWERTYUIO45670';
        solicitudDOD.TAM_Estatus__c = 'Autorizado';
        solicitudDOD.TAM_Modelo__c  = '2701';
        solicitudDOD.TAM_Codigo__c = '2701';
        solicitudDOD.TAM_Anio__c = '2020';
        solicitudDOD.TAM_DescripcionColor__c = 'Rojo';
        solicitudDOD.TAM_Pago__c  = 'Contado';
        solicitudDOD.TAM_DescuentoAutorizadoDtm__c  = 5;
        solicitudDOD.TAM_ComentarioDTM1__c = 'tEST';
        insert solicitudDOD;
        
        //Se crean lineas de tipo TAM_CheckOutDetalleSolicitudCompra__c
        TAM_CheckOutDetalleSolicitudCompra__c lineaCheckOut = new TAM_CheckOutDetalleSolicitudCompra__c();
        lineaCheckOut.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        lineaCheckOut.TAM_VIN__c = 'CAERQIOO1829101';
        lineaCheckOut.TAM_EstatusDealerSolicitud__c = 'En proceso de cancelación';
        lineaCheckOut.TAM_TipoPago__c = 'Contado';
        lineaCheckOut.TAM_IncPropuestoPorcentaje__c = 5;
        insert lineaCheckOut;
        
    }
    
    
    //Metodo que comprueba getCurrentUser
    @isTest static void testMethod1(){
        User usr = [Select Id From User where LastName = 'Testing1' limit 1];
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        TAM_CancelarVentaCorporativa.getCurrentUser(usr.Id);
        TAM_CancelarVentaCorporativa.checkPermissionsUser(usr.id,solFlotilla.id);
        
    }
    
    //Metodo que comprueba getSolicitud
    @isTest static void testMethod2(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        TAM_CancelarVentaCorporativa.getSolicitud(solFlotilla.Id);
        
    }
    
    //Metodo que comprueba getRegistrosPedidoEspecial
    @isTest static void testMethod3(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        TAM_CancelarVentaCorporativa.getRegistrosPedidoEspecial(solFlotilla.Id);
        
    }
    
    //Metodo que comprueba getRegistrosPedidoEspecial
    @isTest static void testMethod4(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;  
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusRegistro = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusCancelacion = DODSolicitud.TAM_Estatus__c;
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        
        TAM_CancelarVentaCorporativa.enviarSolicitudCancelacion('Es de prueba','Nivel1',solFlotilla.id,'Es de prueba',ListaRegistrosCancelar,solFlotilla);
        
    }
    
    
    //Metodo que comprueba nivel2
    @isTest static void testMethod5(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;  
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusRegistro = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusCancelacion = DODSolicitud.TAM_Estatus__c;
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        
        TAM_CancelarVentaCorporativa.enviarSolicitudCancelacion('Es de prueba','Nivel2',solFlotilla.id,'Es de prueba',ListaRegistrosCancelar,solFlotilla);
        
    }
    
    //Metodo que comprueba dealer
    @isTest static void testMethod6(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;  
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusRegistro = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusCancelacion = DODSolicitud.TAM_Estatus__c;
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        
        TAM_CancelarVentaCorporativa.enviarSolicitudCancelacion('Es de prueba','notificarDealer',solFlotilla.id,'Es de prueba',ListaRegistrosCancelar,solFlotilla);
        
    }
    
    //Metodo que guarda aprobaciónes
    @isTest static void testMethod7(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;  
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = 'En proceso de cancelación';
        registroCancelar.estatusRegistro = 'En proceso de cancelación';
        registroCancelar.estatusCancelacion = 'En proceso de cancelación';
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        TAM_CancelarVentaCorporativa.guardarAprobacionDTM(true,solFlotilla.id,'de prueba','de prueba','Aprobar Cancelación','Aprobar Cancelación',ListaRegistrosCancelar);
        
    }
    
    //Metodo que guarda aprobaciónes
    @isTest static void testMethod8(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;  
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = 'En proceso de cancelación';
        registroCancelar.estatusRegistro = 'En proceso de cancelación';
        registroCancelar.estatusCancelacion = 'En proceso de cancelación';
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        TAM_CancelarVentaCorporativa.guardarAprobacionDTM(false,solFlotilla.id,'de prueba','de prueba','Aprobar Cancelación','Aprobar Cancelación',ListaRegistrosCancelar);
        
    }
    
    
    //Metodo que guarda aprobaciónes
    @isTest static void testMethod9(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusRegistro = DODSolicitud.TAM_Estatus__c;
        registroCancelar.estatusCancelacion = DODSolicitud.TAM_Estatus__c;
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        TAM_CancelarVentaCorporativa.guardarAprobacionDTM(false,solFlotilla.id,'de prueba','de prueba','Rechazar Cancelación','Rechazar Cancelación',ListaRegistrosCancelar);
        
    }
    
    //Metodo que guarda aprobaciónes
    @isTest static void testMethod10(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta> ListaRegistrosCancelar = new List<TAM_CancelarVentaCorporativa.wrapperRegistroVenta>();
        
        //Se obtienen registros del objeto DOD
        TAM_DODSolicitudesPedidoEspecial__c DODSolicitud = [Select Id,TAM_VIN__c,TAM_Estatus__c,TAM_Modelo__c,
                                                            TAM_Codigo__c,TAM_Anio__c,TAM_DescripcionColor__c,
                                                            TAM_Pago__c,TAM_DescuentoAutorizadoDtm__c
                                                            From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_VIN__c = 'QWERTYUIO45670' ];
        
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelar = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelar.idRegistro = DODSolicitud.id;
        registroCancelar.tipoRegistro = 'PedidoEspecial';
        registroCancelar.VIN = DODSolicitud.TAM_VIN__c;
        registroCancelar.estatusDOD = 'En proceso de cancelación';
        registroCancelar.estatusRegistro = 'En proceso de cancelación';
        registroCancelar.estatusCancelacion = 'En proceso de cancelación';
        registroCancelar.serie = DODSolicitud.TAM_Codigo__c;
        registroCancelar.modelo = DODSolicitud.TAM_Codigo__c;
        registroCancelar.anioModelo = DODSolicitud.TAM_Anio__c;
        registroCancelar.color = DODSolicitud.TAM_DescripcionColor__c;
        registroCancelar.tipoPago = DODSolicitud.TAM_Pago__c;
        registroCancelar.descuentoAutorizado =   DODSolicitud.TAM_DescuentoAutorizadoDtm__c;
        registroCancelar.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelar);
        
        
        //Se obtienen registros del objeto DOD
        TAM_CheckOutDetalleSolicitudCompra__c checkRegistro = [Select Id,TAM_VIN__c
                                                               From TAM_CheckOutDetalleSolicitudCompra__c WHERE TAM_VIN__c = 'CAERQIOO1829101' ];
        
        
        //CheckOut Registro
        TAM_CancelarVentaCorporativa.wrapperRegistroVenta registroCancelarCh = new TAM_CancelarVentaCorporativa.wrapperRegistroVenta();
        registroCancelarCh.idRegistro = checkRegistro.id;
        registroCancelarCh.tipoRegistro = 'Inventario';
        registroCancelarCh.VIN = checkRegistro.TAM_VIN__c;
        registroCancelarCh.estatusDOD = 'En proceso de cancelación';
        registroCancelarCh.estatusRegistro = 'En proceso de cancelación';
        registroCancelarCh.estatusCancelacion = 'En proceso de cancelación';
        registroCancelarCh.nombreRegistroInventario = 'Test';
        
        ListaRegistrosCancelar.add(registroCancelarCh);
        
        
        TAM_CancelarVentaCorporativa.guardarAprobacionDTM(false,solFlotilla.id,'de prueba','de prueba','Rechazar Cancelación','Rechazar Cancelación',ListaRegistrosCancelar);
        //Si la cancelación es directa se ejecuta el siguiente metodo
        TAM_CancelarVentaCorporativa.guardarAutomaticamente(solFlotilla.Id,ListaRegistrosCancelar);
    }
    
    //Metodo que cancela la solicitud completa
    @isTest static void testMethod11(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        TAM_CancelarVentaCorporativa.cancelaSolicitudVC(solFlotilla.Id);
    }
    
    
    //Verificar si se necesita una aprobación o se cancela directa
    @isTest static void testMethod12(){
        TAM_SolicitudesFlotillaPrograma__c solFlotilla = [Select id,TAM_Cliente__c,TAM_Estatus__c,TAM_ProgramaRango__c From TAM_SolicitudesFlotillaPrograma__c Where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'];
        TAM_CancelarVentaCorporativa.validaAprobacion(solFlotilla.Id);

    }
}