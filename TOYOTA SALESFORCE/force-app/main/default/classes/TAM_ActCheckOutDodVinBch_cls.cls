/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DODSolicitudesPedidoEspecial__c y actualiza el status a Pendiente 
                        y despues a Asignada.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    31-May-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCheckOutDodVinBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActCheckOutDodVinBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActCheckOutDodVinBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_DODSolicitudesPedidoEspecial__c> scope){
        System.debug('EN TAM_ActCheckOutDodVinBch_cls.');
        
        //Crea la lista para poder eliminar los reg de ClientesPaso__c
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapInvDummyPasoUpd = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapIdDummyIdReg = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();
        Set<String> setVinExiste = new Set<String>();
        Set<String> setIdDodUpd = new Set<String>();
        Set<String> setVinVentas = new Set<String>();
        
        Map<String, String> mapInvDummyObjDod = new Map<String, String>();
        Set<String> setDodAsig = new Set<String>();
        Set<String> setDodAsigNoExiste = new Set<String>();
        Map<String, DateTime> mapVinesVtas = new map<String, DateTime>();
        
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_DODSolicitudesPedidoEspecial__c objDoD : scope){
            String sVinPaso;
            String sVinFinal;
            //Toma los datos del TAM_VIN__c y los ultimos digitos
            sVinPaso = objDoD.TAM_VIN__c != null ? objDoD.TAM_VIN__c.right(6) : objDoD.TAM_VIN__c;
            if (sVinPaso.isNumeric())
                mapIdDummyIdReg.put(objDoD.TAM_VIN__c, objDoD);
            //lInvDummyPaso.add(objDoD);
            System.debug('EN TAM_ActCheckOutDodVinBch_cls TAM_DummyVin__c: ' + sVinFinal + ' id: ' + objDoD.id);
        }
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapIdDummyIdReg: ' + mapIdDummyIdReg.KeySet());
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapIdDummyIdReg: ' + mapIdDummyIdReg.Values());

        //Ya tienes los Vines que vas a buscar en el objeto de ventas
        //Consulta los vines asociados a setVinesHist en Ventas__c
        for (Vehiculo__c ventas : [Select v.id, v.Fecha_de_Venta__c, v.Name From Vehiculo__c v
            Where Name IN :mapIdDummyIdReg.keySet()]){
            setVinVentas.add(ventas.Name);
        }
        System.debug('EN TAM_ActCheckOutDodVinBch_cls setVinVentas: ' + setVinVentas);         

        //Busca los vines mapIdDummyIdReg en checkout 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOut : [Select t.TAM_VIN__c From TAM_CheckOutDetalleSolicitudCompra__c t 
            Where TAM_VIN__c IN:mapIdDummyIdReg.keyset() ]){
            //Metelo al set de setVinExiste
            setVinExiste.add(objCheckOut.TAM_VIN__c);
        }
        System.debug('EN TAM_ActCheckOutDodVinBch_cls setVinExiste: ' + setVinExiste);

        //Recorre la lista de mapIdDummyIdReg y ve si el vin existe en setVinExiste
        for (String sVinPaso : mapIdDummyIdReg.KeySet()){
            String sIdDoD = mapIdDummyIdReg.get(sVinPaso).id;
            Boolean blnActRegCheckOut = mapIdDummyIdReg.get(sVinPaso).TAM_ActRegCheckOut__c ? false: true;
            //Ve si existe sVinPaso en setVinExiste
            if (!setVinExiste.contains(sVinPaso)){
                TAM_DODSolicitudesPedidoEspecial__c objPaso = new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_ActRegCheckOut__c = blnActRegCheckOut); 
                //Ve si ya esta vendido y existe en el set de setVinVentas 
                if (setVinVentas.contains(sVinPaso))
                    objPaso.TAM_TipoInventario__c = 'H - Reportado'; 
                //Metelo al mapa de mapInvDummyPasoUpd    
                mapInvDummyPasoUpd.put(sIdDoD, objPaso);
            }//Fin si !setVinExiste.contains(sVinPaso)
            if (setVinExiste.contains(sVinPaso)){
                TAM_DODSolicitudesPedidoEspecial__c objPaso = new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_TieneCheckout__c = true); 
                //Ve si ya esta vendido y existe en el set de setVinVentas 
                if (setVinVentas.contains(sVinPaso))
                    objPaso.TAM_TipoInventario__c = 'H - Reportado';                 
                //Metelo al mapa de mapInvDummyPasoUpd
                mapInvDummyPasoUpd.put(sIdDoD, objPaso);
            }//Fin si setVinExiste.contains(sVinPaso)
        }//Fin del for para mapIdDummyIdReg.,KeySet()
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd: ' + mapInvDummyPasoUpd.KeySet());
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd: ' + mapInvDummyPasoUpd.Values());

        //Un obneto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        List<TAM_LogsErrores__c> lLogError = new List<TAM_LogsErrores__c>();
        
        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!mapInvDummyPasoUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapInvDummyPasoUpd.values(), TAM_DODSolicitudesPedidoEspecial__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    lLogError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'Error Upd DOD Estatus', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage() ) );                
                if (objDtbUpsRes.isSuccess()){
                    setIdDodUpd.add(objDtbUpsRes.getId());
                    lLogError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'OK Upd DOD Estatus', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'OK: ' + objDtbUpsRes.getId() ) );                
                }
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
        System.debug('EN TAM_ActCheckOutDodVinBch_cls lLogError: ' + lLogError);
        System.debug('EN TAM_ActCheckOutDodVinBch_cls setIdDodUpd: ' + setIdDodUpd);
        
        //Limpia el mapa de paso
        mapInvDummyPasoUpd.clear();
        mapIdDummyIdReg.clear();
        setVinExiste.clear();
        //Busca los reg en DOD que se actualizaron
        for (TAM_DODSolicitudesPedidoEspecial__c objDod : [Select id, Name, TAM_VIN__c, TAM_ActRegCheckOut__c 
            From TAM_DODSolicitudesPedidoEspecial__c Where id IN :setIdDodUpd]){
            //Metelo al mapa de mapInvDummyPasoUpd
            mapIdDummyIdReg.put(objDoD.TAM_VIN__c, objDoD);
            //mapInvDummyPasoUpd.put(objDod.id, new TAM_DODSolicitudesPedidoEspecial__c(id = objDod.id, TAM_Estatus__c = 'Asignado'));            
        }//Fin del for para TAM_DODSolicitudesPedidoEspecial__c
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.KeySet());
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.Values());

        //Busca los vines mapIdDummyIdReg en checkout 
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOut : [Select t.TAM_VIN__c From TAM_CheckOutDetalleSolicitudCompra__c t 
            Where TAM_VIN__c IN:mapIdDummyIdReg.keyset()]){
            //Metelo al set de setVinExiste
            setVinExiste.add(objCheckOut.TAM_VIN__c);
        }
        System.debug('EN TAM_ActCheckOutDodVinBch_cls setVinExiste2: ' + setVinExiste);
        
        //Recorre la lista de mapIdDummyIdReg y ve si el vin existe en setVinExiste
        for (String sVinPaso : mapIdDummyIdReg.KeySet()){
            String sIdDoD = mapIdDummyIdReg.get(sVinPaso).id;
            Boolean blnActRegCheckOut = mapIdDummyIdReg.get(sVinPaso).TAM_ActRegCheckOut__c ? false: true;
            //Ve si existe sVinPaso en setVinExiste
            if (!setVinExiste.contains(sVinPaso)){
                TAM_DODSolicitudesPedidoEspecial__c objPaso = new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_ActRegCheckOut__c = blnActRegCheckOut); 
                //Ve si ya esta vendido y existe en el set de setVinVentas 
                if (setVinVentas.contains(sVinPaso))
                    objPaso.TAM_TipoInventario__c = 'H - Reportado'; 
                //Metelo al mapa de mapInvDummyPasoUpd    
                mapInvDummyPasoUpd.put(sIdDoD, objPaso);
            }//Fin si !setVinExiste.contains(sVinPaso)
            if (setVinExiste.contains(sVinPaso)){
                TAM_DODSolicitudesPedidoEspecial__c objPaso = new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_TieneCheckout__c = true); 
                //Ve si ya esta vendido y existe en el set de setVinVentas 
                if (setVinVentas.contains(sVinPaso))
                    objPaso.TAM_TipoInventario__c = 'H - Reportado';                 
                //Metelo al mapa de mapInvDummyPasoUpd
                mapInvDummyPasoUpd.put(sIdDoD, objPaso);
            }//Fin si setVinExiste.contains(sVinPaso)
            /*//Ve si existe sVinPaso en setVinExiste
            if (!setVinExiste.contains(sVinPaso))
                mapInvDummyPasoUpd.put(sIdDoD, new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_ActRegCheckOut__c = blnActRegCheckOut));
            if (setVinExiste.contains(sVinPaso))
                mapInvDummyPasoUpd.put(sIdDoD, new TAM_DODSolicitudesPedidoEspecial__c(id = sIdDoD, TAM_TieneCheckout__c = true));*/
        }//Fin del for para mapIdDummyIdReg.,KeySet()
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.KeySet());
        System.debug('EN TAM_ActCheckOutDodVinBch_cls mapInvDummyPasoUpd2: ' + mapInvDummyPasoUpd.Values());
        
        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!mapInvDummyPasoUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapInvDummyPasoUpd.values(), TAM_DODSolicitudesPedidoEspecial__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    lLogError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'Error Upd DOD Estatus', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ERROR2: ' + objDtbUpsRes.getErrors()[0].getMessage() ) );                                    
                if (objDtbUpsRes.isSuccess()){
                    setIdDodUpd.add(objDtbUpsRes.getId());
                    lLogError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'OK Upd DOD Estatus', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'OK2: ' + objDtbUpsRes.getId() ) );
                }
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
        System.debug('EN TAM_ActCheckOutDodVinBch_cls lLogError2: ' + lLogError);
        System.debug('EN TAM_ActCheckOutDodVinBch_cls setIdDodUpd2: ' + setIdDodUpd);

        //Hecha todo para atras
        //Database.rollback(sp);

        //Crea el reg en lErrores
        if (!lLogError.isEmpty())
            insert lLogError;
                
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActCheckOutDodVinBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}