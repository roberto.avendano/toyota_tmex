/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto Lead
                        y actueliza el nombre del propietario TAM_ActPropProsBch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    19-Feb-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActPropProsBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActPropProsBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActPropProsBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Lead> scope){
        System.debug('EN TAM_ActPropProsBch_cls.');

        Map<String, Lead> mapLeadConsFinal = new Map<String, Lead>();
        Map<String, Lead> mapLeadUpd = new Map<String, Lead>();
        Map<String, Lead> mapLeadUps = new Map<String, Lead>();
        Set<String> setIdLeadUpsSucc = new Set<String>();
        Set<String> setIdLeadUpsErr = new Set<String>();
        Set<String> setNomNvoProp = new Set<String>();
        Map<String, User> mapNomObjUsr = new Map<String, User>();
        Map<String, Lead> mapNomNvoProp = new Map<String, Lead>();
        Map<String, String> mapIdLeadIdProp = new Map<String, String>();
        
        Map<String, TAM_LogsErrores__c> mapLogsErroresUps = new Map<String, TAM_LogsErrores__c>();
        Set<String> setNomProp = new Set<String>();
        Map<String, String> mapNomProLeadCodDist = new Map<String, String>();
        
        //Recorre la lista de Leads que viene en el scope
        for (Lead cand : scope){
            System.debug('EN TAM_ActPropProsBch_cls cand:' + cand);
            setNomProp.add(cand.Owner.Name);
            if (cand.TAM_NuevoPropietario__c != null)
                setNomProp.add(cand.TAM_NuevoPropietario__c);
        }
        System.debug('EN TAM_ActPropProsBch_cls setNomProp:' + setNomProp);
        
        //Consulta el no de distrobuidor asociado a setNomProp
        for (User objUsr : [Select id, name, CodigoDistribuidor__c From User Where Name IN: setNomProp]){
            if (objUsr.CodigoDistribuidor__c != null)
                mapNomProLeadCodDist.put(objUsr.Name, objUsr.CodigoDistribuidor__c);
            if (objUsr.CodigoDistribuidor__c == null)
                System.debug('EN TAM_ActPropProsBch_cls NO TIENE CODIGO DE DISTRIBUIDOR objUsr:' + objUsr);
        }
        System.debug('EN TAM_ActPropProsBch_cls mapNomProLeadCodDist:' + mapNomProLeadCodDist.keyset());
        System.debug('EN TAM_ActPropProsBch_cls mapNomProLeadCodDist:' + mapNomProLeadCodDist.values());
        
        //Recorre la lista de Leads que viene en el scope
        for (Lead cand : scope){
            //ve si el campo de TAM_NuevoPropietario__c tiene un valor
            if (cand.TAM_NuevoPropietario__c != null){
                mapNomNvoProp.put(cand.id, cand);
                setNomNvoProp.add(cand.TAM_NuevoPropietario__c);
            }//Fin si cand.TAM_NuevoPropietario__c != null
            if (cand.TAM_NuevoPropietario__c == null || Test.isRunningTest()){
                String sCodDistFin;
                String sCodDist = mapNomProLeadCodDist.containsKey(cand.Owner.Name) ? mapNomProLeadCodDist.get(cand.Owner.Name) : null;
                //Ve si los codigos son iguales
                if (sCodDist != null){
                    if (sCodDist != cand.FWY_codigo_distribuidor__c)
                        sCodDistFin = sCodDist;
                    if (sCodDist == cand.FWY_codigo_distribuidor__c)
                        sCodDistFin = cand.FWY_codigo_distribuidor__c;
                }//Fin si sCodDist != null
                if (sCodDist == null)
                   sCodDistFin = cand.FWY_codigo_distribuidor__c;
                System.debug('EN TAM_ActPropProsBch_cls sCodDistFin: ' + sCodDistFin);                   
	            //Agregalo al mapa de mapLeadUps
	            Lead objLeadPaso = new Lead(
                        id = cand.id,
                        TAM_NomPropKban__c = cand.Owner.Name,
                        TAM_CambioPropietario__c = false,
                        TAM_PropietarioAanteriorTxt__c = cand.TAM_PropietarioAnterior__c != null ? cand.TAM_PropietarioAnterior__r.Name : null,
                        FWY_codigo_distribuidor__c = sCodDistFin
                );
                if (!Test.isRunningTest())
	               mapLeadUps.put(cand.id, objLeadPaso);
            }//Fin si cand.TAM_NuevoPropietario__c != null
        }
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUps0: ' + mapLeadUps.KeySet());
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUps0: ' + mapLeadUps.Values());

        System.debug('EN TAM_ActPropProsBch_cls mapNomNvoProp: ' + mapNomNvoProp.KeySet());
        System.debug('EN TAM_ActPropProsBch_cls mapNomNvoProp: ' + mapNomNvoProp.Values());

        System.debug('EN TAM_ActPropProsBch_cls setNomNvoProp: ' + setNomNvoProp);

        //Ve si tiene algo setNomNvoProp
        if (!setNomNvoProp.isEmpty()){
           //Consulta los datos
           for (User propFinal : [Select Id, Name, CodigoDistribuidor__c From User Where Name IN :setNomNvoProp]){
                mapNomObjUsr.put(propFinal.Name, propFinal);               
           }
           System.debug('EN TAM_ActPropProsBch_cls mapNomObjUsr: ' + mapNomObjUsr.KeySet());
           System.debug('EN TAM_ActPropProsBch_cls mapNomObjUsr: ' + mapNomObjUsr.Values());
           //Recorre la lista de setNomNvoProp y mete el lead al mapa de mapLeadUps
           for (String idLeadNomNvo : mapNomNvoProp.keySet()){
                System.debug('EN TAM_ActPropProsBch_cls idLeadNomNvo: ' + idLeadNomNvo);           
               if (!mapLeadUps.containsKey(idLeadNomNvo)){
                    Lead lPaso = mapNomNvoProp.get(idLeadNomNvo);
                    System.debug('EN TAM_ActPropProsBch_cls lPaso: ' + lPaso);
                    String sIdNvoPro = mapNomObjUsr.containsKey(lPaso.TAM_NuevoPropietario__c) ? mapNomObjUsr.get(lPaso.TAM_NuevoPropietario__c).id : null;
                    String sNomProPaso = mapNomObjUsr.containsKey(lPaso.TAM_NuevoPropietario__c) ? mapNomObjUsr.get(lPaso.TAM_NuevoPropietario__c).Name: null;
	                String sCodDistFin;
	                String sCodDist = mapNomProLeadCodDist.containsKey(sNomProPaso) ? mapNomProLeadCodDist.get(sNomProPaso) : null;
	                String sCodDistNvo = mapNomObjUsr.containsKey(lPaso.TAM_NuevoPropietario__c) ? mapNomObjUsr.get(lPaso.TAM_NuevoPropietario__c).CodigoDistribuidor__c : null;	                
	                //Ve si los codigos son iguales
	                if (sCodDist != null){
	                    if (sCodDist != sCodDistNvo)
	                        sCodDistFin = sCodDist;
	                    if (sCodDist == sCodDistNvo)
	                        sCodDistFin = sCodDistNvo;
	                }//Fin si sCodDist != null
	                if (sCodDist == null)
	                   sCodDistFin = sCodDistNvo;
                    System.debug('EN TAM_ActPropProsBch_cls lPaso: ' + lPaso + ' sIdNvoPro: ' + sIdNvoPro + ' sCodDistFin: ' + sCodDistFin);
	                //Agregalo al mapa de mapLeadUps
	                mapLeadUps.put(idLeadNomNvo, new Lead(
	                        id = idLeadNomNvo,
	                        TAM_NomPropKban__c = lPaso.TAM_NuevoPropietario__c,
	                        TAM_CambioPropietario__c = false,
	                        TAM_PropietarioAanteriorTxt__c = lPaso.TAM_PropietarioAnterior__c != null ? lPaso.TAM_PropietarioAnterior__r.Name : null,
	                        TAM_NuevoPropietario__c = null,
	                        OwnerId = sIdNvoPro,
	                        FWY_codigo_distribuidor__c = sCodDistFin
	                    )
	                );
	                //Agregalo al mapa de mapIdLeadIdProp
	                if (sIdNvoPro != null && sNomProPaso != null)
	                   mapIdLeadIdProp.put(idLeadNomNvo, sIdNvoPro);
               }//Fin si mapLeadUps.containsKey(sIdLead)
           } //Fin  del for para setNomNvoProp
        }//Fin si !setNomNvoProp.isEmpty()
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUps2: ' + mapLeadUps.KeySet());
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUps2: ' + mapLeadUps.Values());

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();
        
        //Ve si tiene algo la lista de mapLeadUps
        if (!mapLeadUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapLeadUps.values(), Lead.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                Boolean berror = false;
                String sdetalerror = '';
                if (!objDtbUpsRes.isSuccess()){
                    berror = true;
                    sdetalerror = 'Error en Lead Upd Nombre Prop: ' + objDtbUpsRes.getErrors()[0].getMessage() + ' ID Candidato: ' + objDtbUpsRes.getId();
                }//Fin si !objDtbUpsRes.isSuccess()
                if (objDtbUpsRes.isSuccess())
                    setIdLeadUpsSucc.Add(objDtbUpsRes.getId());
                //Hubo error    
                if (berror || Test.isRunningTest()){
                    mapLogsErroresUps.put(objDtbUpsRes.getId() + '-Lead', new TAM_LogsErrores__c( 
                            TAM_idExtReg__c = objDtbUpsRes.getId() + '-Lead',    
                            TAM_Proceso__c = 'Error Upd Nom Prop Lead',
                            TAM_Fecha__c = Date.today(),
                            TAM_DetalleError__c = sdetalerror
                        )
                    );                    
                }//Fin si berror
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapLeadUps.isEmpty()
        if (Test.isRunningTest())
           setIdLeadUpsErr.add('Error en Lead Upd Nomnre Prop PRUEBA...');
        System.debug('EN TAM_ActPropProsBch_cls setIdLeadUpsErr: ' + setIdLeadUpsErr);
        System.debug('EN TAM_ActPropProsBch_cls setIdLeadUpsSucc: ' + setIdLeadUpsSucc);         
        
        List<Task> lTareaUpd = new List<Task>();
        //Consulta las tareas asciadas a los lead que se estan reasignando
        for (Task objTarea : [Select Id, WhoId From Task Where WhoId IN:setIdLeadUpsSucc]){
            //Ve si el objTarea.WhoId existe en el mapIdLeadIdProp 
            if (mapIdLeadIdProp.containsKey(objTarea.WhoId))
                lTareaUpd.add(new Task(id = objTarea.id, OwnerId = mapIdLeadIdProp.get(objTarea.WhoId)));
        }//Fin del for para Task 
        System.debug('EN TAM_ActPropProsBch_cls lTareaUpd antes de enviarla a TAM_TareasPaso_Handler.actalizaRegistrosTareas: ' + lTareaUpd);        
        
        //Manda la lista lTareaUpd a la clase de TAM_TareasPaso_Handler
        TAM_TareasPaso_Handler.actalizaRegistrosTareas(lTareaUpd);
        
        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!mapLogsErroresUps.isEmpty()){
           //Recorre la lista de Opp y busca el VIN para asociar el id de la Cand en AccountId
           List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapLogsErroresUps.values(), TAM_LogsErrores__c.TAM_idExtReg__c, false);
           //Ve si hubo error
           for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
              if (!objDtbUpsRes.isSuccess())
                  System.debug('EN tamConfirmaReasignacion Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());         
          }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

        //Consulta los leads que se acaban de actualizar en setIdLeadUpsSucc
        for (Lead objLeadPaso : [Select id, TAM_CodDistribuidorUsr__c, TAM_NomPropUsr__c, FWY_Distribuidor_f__c 
            From Lead Where ID IN :setIdLeadUpsSucc ]){
            //Metelo al mapa de Map<String, Lead> mapLeadConsFinal
            mapLeadConsFinal.put(objLeadPaso.id, objLeadPaso);
        }//Fin del for para Lead
        System.debug('EN TAM_ActPropProsBch_cls mapLeadConsFinal:' + mapLeadConsFinal.keyset());
        System.debug('EN TAM_ActPropProsBch_cls mapLeadConsFinal:' + mapLeadConsFinal.values());

        //Recorre la lista de Leads del mapa mapLeadUps
        for (Lead objLeadPaso : mapLeadConsFinal.values()){
            Boolean blnActua = false;            
            Lead objLeadHist = new Lead(id = objLeadPaso.Id);
            //Ve si el campo Lead.TAM_CodDistribuidorUsr__c != FWY_codigo_distribuidor__c
            if (objLeadPaso.TAM_CodDistribuidorUsr__c != null){
                objLeadHist.FWY_codigo_distribuidor__c = objLeadPaso.TAM_CodDistribuidorUsr__c;
                blnActua = true;
            }//Fin si objLeadPaso.TAM_CodDistribuidorUsr__c != objLeadPaso.FWY_codigo_distribuidor__c
            //Ve si el campo Lead.TAM_CodDistribuidorUsr__c != FWY_codigo_distribuidor__c
            if (objLeadPaso.TAM_NomPropUsr__c != null){
                objLeadHist.TAM_NomPropKban__c = objLeadPaso.TAM_NomPropUsr__c;
                blnActua = true;            
            }//Fin si objLeadPaso.TAM_NomPropUsr__c != objLeadPaso.TAM_NomPropKban__c
            //Ve si el campo Lead.FWY_Distribuidor_f__c != Distribuidor_para_cuenta__c
            if (objLeadPaso.FWY_Distribuidor_f__c != null){
                objLeadHist.Distribuidor_para_cuenta__c = objLeadPaso.FWY_Distribuidor_f__c;
                blnActua = true;
            }//Fin si objLeadPaso.FWY_Distribuidor_f__c != objLeadPaso.Distribuidor_para_cuenta__c
            //Metelo al mapa de mapLeadUpd
            if (blnActua || Test.isRunningTest())
                mapLeadUpd.put(objLeadHist.id, objLeadHist);            
        }//Fin del for para mapLeadUps.values()
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUpd:' + mapLeadUpd.keyset());
        System.debug('EN TAM_ActPropProsBch_cls mapLeadUpd:' + mapLeadUpd.values());

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapLeadUpd.values(), Lead.id, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess())
               System.debug('EN TAM_ActNomKbanLeadBch_cls Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
        }//Fin del for para lDtbUpsRes
       
        //Roleback a todo
        //Database.rollback(sp);
        
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActPropProsBch_cls.finish Hora: ' + DateTime.now());  
        
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_ActNombrePropProsMinutos);
        DateTime dtHoraActual = DateTime.now();
        DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //5
        String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
        System.debug('EN TAM_ActPropProsBch_cls.finish CRON_EXP: ' + CRON_EXP);

        //Manda a llamar el proceso de TAM_ActPropProsSch_cls para actuelizar el campo Prop
        TAM_ActPropProsSch_cls objActPropProsSch = new TAM_ActPropProsSch_cls();
        System.debug('EN TAM_ActPropProsBch_cls.finish objActPropProsSch: ' + objActPropProsSch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('TAM_ActPropProsSch_cls: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objActPropProsSch);
        
    } 
    
}