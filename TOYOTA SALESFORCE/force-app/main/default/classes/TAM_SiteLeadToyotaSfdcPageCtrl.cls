/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros
                        que llegan a la pagina de: TAM_SiteLeadToyotaSfdcPage.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SiteLeadToyotaSfdcPageCtrl {
    
    //Un constructor por default
    public TAM_SiteLeadToyotaSfdcPageCtrl(){
        System.debug('EN TAM_SiteLeadToyotaSfdcPageCtrl...');
        //Vamos a tomar los parametros que vienen en la URL
        Map<String, String> getParametersMap = ApexPages.currentPage().getParameters();
        
        //Ya tienes la lista de mapas entonces toma el que trae la información del lead
        System.debug('EN TAM_SiteLeadToyotaSfdcPageCtrl getParametersMap: ' + getParametersMap);
    }
    
}