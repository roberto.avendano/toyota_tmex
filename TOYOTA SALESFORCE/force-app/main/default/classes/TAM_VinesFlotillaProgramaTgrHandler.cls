public with sharing class TAM_VinesFlotillaProgramaTgrHandler extends TriggerHandler{

    private List<TAM_VinesFlotillaPrograma__c > itemsOld;

    //Un constructor por default
    public TAM_VinesFlotillaProgramaTgrHandler() {
        this.itemsOld = (List<TAM_VinesFlotillaPrograma__c >) Trigger.Old;        
    }    

    //Sobreescribe el metodo de afterInsert
    public override void afterDelete() {
        System.debug('EN afterDelete....');
        validaEstadoSolicitudCerrada(this.itemsOld);
    }
    
    public static void validaEstadoSolicitudCerrada(List<TAM_VinesFlotillaPrograma__c> itemsOld){
        System.debug('ENTRO validaEstadoSolicitudCerrada TriggerHandler itemsOld: ' + itemsOld);           

        Set<String> setIdOwner = new Set<String>();
        Map<String, String> mapIdRegIdLead = new Map<String, String>();
        Set<String> setIdLeadFactEntre = new Set<String>();
        Boolean bErrorElimina = false;
        Set<String> setInv = new Set<String>();
        List<TAM_InventarioVehiculosToyota__c> lInvUpd = new List<TAM_InventarioVehiculosToyota__c>();
        Map<String, String> mapVinSolic = new Map<String, String>();
        
        for(TAM_VinesFlotillaPrograma__c  objLeadInv : itemsOld){
            //Ve si tiene algo el cammpo de OwnerId
            if (objLeadInv.TAM_SolicitudFlotillaPrograma__c != null)
                mapIdRegIdLead.put(objLeadInv.id, objLeadInv.TAM_SolicitudFlotillaPrograma__c);
        }
        System.debug('ENTRO validaEstadoSolicitudCerrada TriggerHandler mapIdRegIdLead: ' + mapIdRegIdLead);           
        
        //selecciona los lead asociados a mapIdRegIdLead
        for (TAM_SolicitudesFlotillaPrograma__c  objSolFlot : [Select id, Name From TAM_SolicitudesFlotillaPrograma__c  
            Where id IN: mapIdRegIdLead.Values() And (TAM_FechaCierreSolicitudForm__c != NULL
            OR TAM_FechaCierreSolicitud__c != NULL)]){
            mapVinSolic.put(objSolFlot.id, objSolFlot.Name);
        }
        System.debug('ENTRO validaEstadoSolicitudCerrada TriggerHandler mapVinSolic: ' + mapVinSolic);
        
        //Recorre al lista de TAM_VinesFlotillaPrograma__c y ve si algino de esos ya esta facturado o entregado y desplieda un error
        for(TAM_VinesFlotillaPrograma__c objLeadInv : itemsOld){
            //Ve si tiene algo el cammpo de OwnerId
            if (setIdLeadFactEntre.contains(objLeadInv.TAM_SolicitudFlotillaPrograma__c)){
                if (!Test.isRunningTest())
                    objLeadInv.addError('Este Vin: (' + objLeadInv.Name + ') ya existe en una solicitud folio :( ' + mapVinSolic.get(objLeadInv.TAM_SolicitudFlotillaPrograma__c) + '), vigente con un checkout. Si es necesario cancela la solicitud para que puedas usar el vin en otra.');
            }//Fin si setIdLeadFactEntre.contains(objLeadInv.TAM_Prospecto__c
        }
    
    }    
}