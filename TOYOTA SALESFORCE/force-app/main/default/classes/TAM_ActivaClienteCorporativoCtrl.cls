/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladora para el componente TAM_ActivaClienteCorporativo

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    20-Agosto-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class TAM_ActivaClienteCorporativoCtrl {

    public static String VaRtAccRegDist = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
    public static String VaRtAccRegCteCorp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();

    public static String VaRtAccRegCtePerFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    public static String VaRtAccRegCtePerMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();

    public static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    public static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();

	public static String VaRtCteCorp = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

    public static String VaRtCteRangoFlot = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    public static String VaRtCteRangoProg = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	public class wrpMapDatosCatalogos{
	    @AuraEnabled 
	    public String strCodigo {get;set;}    
	    @AuraEnabled 
	    public String strDescripcion {get;set;}    
	    @AuraEnabled 
	    public Boolean selected {get;set;}    
		
		//Un contructor por default
		public wrpMapDatosCatalogos(){
			this.strCodigo = '';
			this.strDescripcion = '';	
			this.selected = false;	
		}
		
		//Un contructor por default
		public wrpMapDatosCatalogos(String strCodigo, String strDescripcion, Boolean selected){
			this.strCodigo = strCodigo;
			this.strDescripcion = strDescripcion;	
			this.selected = selected;
		}
	}

	public class wrpDatosClienteCorporativo{
	    @AuraEnabled 
	    public TAM_ReactivarCliente__c objClienteCorporativo {get;set;}    
	    @AuraEnabled 
	    public Account objClienteActual {get;set;}    
	    @AuraEnabled 
	    public String strTipoClienteCorp {get;set;}    
	    @AuraEnabled 
	    public Boolean blnTieneSolProc {get;set;}    
	    @AuraEnabled 
	    public String strRango {get;set;}    
	    @AuraEnabled 
	    public String strGpoCorp {get;set;}    
        @AuraEnabled 
        public String strUrlSitioWeb {get;set;}    

        @AuraEnabled 
        public Boolean blnNoEsProp {get;set;}
        @AuraEnabled 
        public String strNoEsProp {get;set;}    
		
		//Un contructor por default
		public wrpDatosClienteCorporativo(){
			this.objClienteCorporativo = new TAM_ReactivarCliente__c();
			this.strTipoClienteCorp = '';	
			this.objClienteActual = new Account();		
			this.blnNoEsProp = false;
			this.strNoEsProp = '';
		}
		
		//Un contructor por default
		public wrpDatosClienteCorporativo(TAM_ReactivarCliente__c objClienteCorporativo, String strTipoClienteCorp,
		Account objClienteActual){
			this.objClienteCorporativo = objClienteCorporativo;
			this.strTipoClienteCorp = strTipoClienteCorp;	
			this.objClienteActual = objClienteActual;
		}
	}

	public class wrpArchichivosRequeridos{
	    @AuraEnabled 
	    public Boolean blnActaConstitutiva {get;set;}    
	    @AuraEnabled 
	    public Boolean blnCedulaFiscal {get;set;}    
	    @AuraEnabled 
	    public Boolean blnPdfDatosAdicionalesRepLegal {get;set;}    
	    @AuraEnabled 
	    public Boolean blnOrdenCompraCartaCompromiso {get;set;}
        @AuraEnabled
	    public Boolean blnIdentificacionOficial {get;set;}    
	    @AuraEnabled 
	    public Boolean blnComprobanteDomicilio {get;set;}    
	    @AuraEnabled 
	    public Boolean blnDocumentacionSoporte {get;set;}

        @AuraEnabled 
        public Boolean blnValidaRFC {get;set;}
        @AuraEnabled 
        public String strMsgValidaRfc {get;set;}

		//Un contructor por default
		public wrpArchichivosRequeridos(){
			this.blnActaConstitutiva = false;
			this.blnCedulaFiscal = false;	
			this.blnPdfDatosAdicionalesRepLegal = false;				
			this.blnOrdenCompraCartaCompromiso = false;
			this.blnIdentificacionOficial = false;	
			this.blnComprobanteDomicilio = false;
			this.blnDocumentacionSoporte = false;
		}
		
		//Un contructor por default
		public wrpArchichivosRequeridos(Boolean blnActaConstitutiva, Boolean blnCedulaFiscal, Boolean blnPdfDatosAdicionalesRepLegal,
			Boolean blnOrdenCompraCartaCompromiso, Boolean blnIdentificacionOficial, Boolean blnComprobanteDomicilio, Boolean blnDocumentacionSoporte){
			this.blnActaConstitutiva = false;
			this.blnCedulaFiscal = false;	
			this.blnPdfDatosAdicionalesRepLegal = false;				
			this.blnOrdenCompraCartaCompromiso = false;
			this.blnIdentificacionOficial = false;	
			this.blnComprobanteDomicilio = false;
			this.blnDocumentacionSoporte = false;
		}
	}
    
    //Obtener getDatosCliente.
    @AuraEnabled
    public static wrpArchichivosRequeridos getArchivosCargardos(String recordId) {
    	System.debug('EN getArchivosCargardos recordId: ' + recordId);
        
        String sRFCCte;
        
    	Set<String> setContentDocumentId = new Set<String>();    
    	wrpArchichivosRequeridos objwrpArchichivosRequeridosPaso = new wrpArchichivosRequeridos();
		objwrpArchichivosRequeridosPaso.blnActaConstitutiva = false;
		objwrpArchichivosRequeridosPaso.blnCedulaFiscal = false;	
		objwrpArchichivosRequeridosPaso.blnPdfDatosAdicionalesRepLegal = false;				
		objwrpArchichivosRequeridosPaso.blnOrdenCompraCartaCompromiso = false;
		objwrpArchichivosRequeridosPaso.blnIdentificacionOficial = false;	
		objwrpArchichivosRequeridosPaso.blnComprobanteDomicilio = false;
		objwrpArchichivosRequeridosPaso.blnDocumentacionSoporte = false;
    	    	
		//Consulta los archivos relaconados a recordId
		for (ContentDocumentLink objCdl : [SELECT  id, LinkedEntityId, ContentDocumentId, ContentDocument.Description 
			FROM ContentDocumentLink WHERE LinkedEntityId =: recordId]){
	    	System.debug('EN getArchivosCargardos Description: ' + objCdl.ContentDocument.Description);
        	if (objCdl.ContentDocument.Description == 'Acta Constitutiva') 
				objwrpArchichivosRequeridosPaso.blnActaConstitutiva = true;        		  
        	if (objCdl.ContentDocument.Description == 'Cedula Fiscal') 
				objwrpArchichivosRequeridosPaso.blnCedulaFiscal = true;
        	if (objCdl.ContentDocument.Description == 'PDF datos adicionales') 
				objwrpArchichivosRequeridosPaso.blnPdfDatosAdicionalesRepLegal = true;  
       		if (objCdl.ContentDocument.Description == 'Orden de compra') 
				objwrpArchichivosRequeridosPaso.blnOrdenCompraCartaCompromiso = true;
        	if (objCdl.ContentDocument.Description == 'Identificación oficial') 
				objwrpArchichivosRequeridosPaso.blnIdentificacionOficial = true;        		  
        	if (objCdl.ContentDocument.Description == 'Comprobante de domicilio') 
				objwrpArchichivosRequeridosPaso.blnComprobanteDomicilio = true;        		  
        	if (objCdl.ContentDocument.Description == 'Documentación de soporte') 
				objwrpArchichivosRequeridosPaso.blnDocumentacionSoporte = true;
		}
		
		//Consulta los datos de la cuenta asociada al ID para tomar el campo del RFC
		for (Account clienteReact : [Select Id, RecordTypeId, TAM_RFC_1__pc, RFC__c From Account 
		      Where (RecordTypeId =:VaRtAccRegCtePerFisica OR RecordTypeId =:VaRtAccRegCtePerMoral
		      OR RecordTypeId =:VaRtCteMoralNoVigente OR RecordTypeId =:VaRtCteFisicaNoVigente)
		      And id =:recordId ]){		          
            //Inicializa el campo de sRFCCte
            if (clienteReact.RecordTypeId == VaRtAccRegCtePerFisica || clienteReact.RecordTypeId == VaRtCteFisicaNoVigente)
                sRFCCte = clienteReact.TAM_RFC_1__pc;
            if (clienteReact.RecordTypeId == VaRtAccRegCtePerMoral || clienteReact.RecordTypeId == VaRtCteMoralNoVigente)
                sRFCCte = clienteReact.RFC__c;
		}
        System.debug('EN getArchivosCargardos sRFCCte: ' + sRFCCte);
        
        //Ve si tiene algo sRFCCte
        if (sRFCCte != null){
	        //Consulta los datos del RFC para ver si ya hay una solicitud asociada a ese cliente.
	        for (TAM_SolicitudesParaAprobar__c sol : [Select t.TAM_RFCRef__c, t.TAM_Estatus__c, t.RecordType.Name, 
	            t.OwnerId, t.Name, t.CreatedDate From TAM_SolicitudesParaAprobar__c t
	            where TAM_Estatus__c IN ('Proceso') And TAM_RFCRef__c = :sRFCCte 
	            And RecordTypeId =:VaRtCteCorp Order by CreatedDate DESC, TAM_Estatus__c LIMIT 1]){
	            //Inicializa los datos de objwrpArchichivosRequeridosPaso
	            objwrpArchichivosRequeridosPaso.blnValidaRFC = true;
	            objwrpArchichivosRequeridosPaso.strMsgValidaRfc = 'Ya existe una solicitud pendiente por atorizar para este Cliente';    
	        }//Fin del for para TAM_SolicitudesParaAprobar__c
        }//Fin si 
    	System.debug('EN getArchivosCargardos objwrpArchichivosRequeridosPaso: ' + objwrpArchichivosRequeridosPaso);

		//Regresa el objeto    	    	    	
    	return objwrpArchichivosRequeridosPaso;
    }
        
    //Obtener getDatosCliente.
    @AuraEnabled
    public static wrpDatosClienteCorporativo getDatosCliente(String recordId) {
    	System.debug('EN getDatosCliente recordId: ' + recordId);
    	TAM_ReactivarCliente__c nuevoClienteCorporativo;
    	wrpDatosClienteCorporativo objWrpCteCorp;
		String sRango;
		String sGpoCorp;
		
		//Toma el id de la cuenta del usuario que esta entrando
		String sIdAccountUsr = getDistribuidor(UserInfo.getUserId());
		
    	Boolean blnTieneSolProc = false;
		//Consulta en  TAM_SolicitudesParaAprobar__c y ve si hay una en estatus de en proceso
		for (TAM_SolicitudesParaAprobar__c solEnProc : [Select id From TAM_SolicitudesParaAprobar__c 
			Where TAM_ClienteCorporativoRef__c =: recordId And TAM_Estatus__c = 'Proceso']){
			blnTieneSolProc = true;
		}
		
 		//Consulta todos los reistros que estan en estapa de Pedido en Proceso
 		for (Account candidatoFacturar : [SELECT id, Name, Apellido_Materno__c, BillingCity, BillingCountry, 
 			BillingPostalCode, BillingState, BillingStreet, Colonia__c, Website, 
 			Compania__c, Correo_Electronico__c, DenominacionSocial__c, Phone, RecordType.Name, RFC__c,
			TAM_ApellidoMaternoRL__c, TAM_ApellidoPaternoRL__c, TAM_CorreoElectronicoRL__c, TAM_DireccionRL__c,
			TAM_EstatusCliente__c, TAM_FechaEnvioPreautorizacion__c, TAM_IdExternoNombre__c, TAM_NombreComercial__c,
			TAM_NombreRL__c, TAM_ProgramaRango__c, TAM_RFCRL__c, TAM_SolcitaActivacion__c, TAM_TelefonoRL__c, 
			TAM_TipoPersonaProspecto__c, Telefono_Oficina__c, FirstName, LastName, TAM_DistribuidorCC__c,
			TAM_ActaConstitutiva__c, TAM_CedulaFiscal__c, TAM_PdfDatosAdicionalesRepLegal__c, TAM_OrdenCompraCartaCompromiso__c,
			TAM_IdentificacionOficial__c, TAM_ComprobanteDomicilio__c, TAM_DocumentacionSoporte__c,
			(SELECT TAM_TipoCliente__c, TAM_Telefono__c, TAM_TelefonoRL__c, TAM_RFC__c, TAM_RFCRL__c, TAM_ProgramaRango__c, 
				TAM_Pais__c, TAM_NombreRL__c, TAM_NombreComercial__c, TAM_Estado__c, TAM_DireccionRL__c, TAM_CorreoElectronico__c, 
				TAM_CorreoElectronicoRL__c, TAM_Colonia__c, TAM_CodigoPostal__c, TAM_Cliente__c, TAM_Ciudad__c, TAM_CiudadMunicipio__c, 
				TAM_Calle__c, TAM_ApellidoPaterno__c, TAM_ApellidoPaternoRL__c, TAM_ApellidoMaterno__c, TAM_ApellidoMaternoRL__c, 
				Name, Id, TAM_GrupoCorporativo__c, TAM_GrupoCorporativo__r.Name, TAM_ProgramaRango__r.Name, TAM_SitioWeb__c  
				From TAM_ReactivarClientes__r) 
 			From Account Where id =: recordId]){
	    	System.debug('EN getDatosCliente candidatoFacturar: ' + candidatoFacturar);
	    	System.debug('EN getDatosCliente candidatoFacturar.RecordType.Name: ' + candidatoFacturar.RecordType.Name);
 			
 			//Ve si ya tiene un registro de cliente TAM_ReactivarClientes__r asociado al cliente principal
 			if (candidatoFacturar.TAM_ReactivarClientes__r.isEmpty())
 				nuevoClienteCorporativo = new TAM_ReactivarCliente__c(TAM_Cliente__c = recordId);
 			//Ve si ya tiene un registro de cliente TAM_ReactivarClientes__r asociado al cliente principal
 			if (!candidatoFacturar.TAM_ReactivarClientes__r.isEmpty())
 				nuevoClienteCorporativo = candidatoFacturar.TAM_ReactivarClientes__r.get(0);
	    	System.debug('EN getDatosCliente nuevoClienteCorporativo: ' + nuevoClienteCorporativo);
 				
			//Inicializa el objeto nuevoClienteCorporativo
			if (nuevoClienteCorporativo.Name == null){
				//Datos del cliente
				if (candidatoFacturar.RecordType.Name == 'Cliente - Persona Moral' || candidatoFacturar.RecordType.Name == 'Cliente - Persona Moral No Vigente')
					nuevoClienteCorporativo.Name = candidatoFacturar.Name;
				if (candidatoFacturar.RecordType.Name == 'Cliente - Persona Fisica' || candidatoFacturar.RecordType.Name == 'Cliente - Persona Fisica No Vigente'){
					nuevoClienteCorporativo.Name = candidatoFacturar.FirstName;				
					nuevoClienteCorporativo.TAM_ApellidoPaterno__c = candidatoFacturar.LastName;
					nuevoClienteCorporativo.TAM_ApellidoMaterno__c = candidatoFacturar.Apellido_Materno__c;
				}
				nuevoClienteCorporativo.TAM_NombreComercial__c = candidatoFacturar.TAM_NombreComercial__c;
				nuevoClienteCorporativo.TAM_CorreoElectronico__c = candidatoFacturar.Correo_Electronico__c;
				nuevoClienteCorporativo.TAM_RFC__c = candidatoFacturar.RFC__c;
				nuevoClienteCorporativo.TAM_Telefono__c = candidatoFacturar.Phone;
				nuevoClienteCorporativo.TAM_TipoCliente__c = candidatoFacturar.TAM_TipoPersonaProspecto__c != null ? candidatoFacturar.TAM_TipoPersonaProspecto__c : 'Persona Física';
                nuevoClienteCorporativo.TAM_SitioWeb__c = candidatoFacturar.Website != null ? candidatoFacturar.Website : null;
	
				//Datos de la dirección del cliente
				nuevoClienteCorporativo.TAM_Calle__c = candidatoFacturar.BillingStreet;
				nuevoClienteCorporativo.TAM_CodigoPostal__c = candidatoFacturar.BillingPostalCode;
				nuevoClienteCorporativo.TAM_Colonia__c = candidatoFacturar.Colonia__c;
				nuevoClienteCorporativo.TAM_CiudadMunicipio__c = candidatoFacturar.BillingCity;			
				nuevoClienteCorporativo.TAM_Ciudad__c = candidatoFacturar.BillingCity;
				nuevoClienteCorporativo.TAM_Estado__c = candidatoFacturar.BillingState;
				nuevoClienteCorporativo.TAM_Pais__c = candidatoFacturar.BillingCountry;
				
				//Datos del Representante Legal
				nuevoClienteCorporativo.TAM_NombreRL__c = candidatoFacturar.TAM_NombreRL__c;
				nuevoClienteCorporativo.TAM_ApellidoPaternoRL__c = candidatoFacturar.TAM_ApellidoPaternoRL__c;
				nuevoClienteCorporativo.TAM_ApellidoMaternoRL__c = candidatoFacturar.TAM_ApellidoMaternoRL__c;
				nuevoClienteCorporativo.TAM_RFCRL__c = candidatoFacturar.TAM_RFCRL__c;
				nuevoClienteCorporativo.TAM_TelefonoRL__c = candidatoFacturar.TAM_TelefonoRL__c;
				nuevoClienteCorporativo.TAM_CorreoElectronicoRL__c = candidatoFacturar.TAM_CorreoElectronicoRL__c;
			}//Fin si nuevoClienteCorporativo.Name == null
			sRango = nuevoClienteCorporativo.TAM_ProgramaRango__c != null ? nuevoClienteCorporativo.TAM_ProgramaRango__r.Name : null;
			sGpoCorp = nuevoClienteCorporativo.TAM_GrupoCorporativo__c != null ? nuevoClienteCorporativo.TAM_GrupoCorporativo__r.Name : null;
			System.debug('EN getDatosCliente sRango: ' + sRango + ' sGpoCorp: ' + sGpoCorp);
			
			//Inicializa el objeto del tipo objWrpCteCorp
			objWrpCteCorp = new wrpDatosClienteCorporativo(nuevoClienteCorporativo, candidatoFacturar.RecordType.Name,
				candidatoFacturar);
			objWrpCteCorp.blnTieneSolProc = blnTieneSolProc;
			objWrpCteCorp.strRango = sRango;
			objWrpCteCorp.strGpoCorp = sGpoCorp;
			objWrpCteCorp.blnNoEsProp = false;
            objWrpCteCorp.strUrlSitioWeb = nuevoClienteCorporativo.TAM_SitioWeb__c;
			
			//Ve si se trata de la misma cuenta del Distribuidor
            if (candidatoFacturar.TAM_DistribuidorCC__c != null){
			     /*if (sIdAccountUsr != candidatoFacturar.TAM_DistribuidorCC__c){
			         objWrpCteCorp.blnNoEsProp = true;
			         objWrpCteCorp.strNoEsProp = 'No puedes reactivar este cliente porque tu no eres el propietario, ponte en contacto con el area de DTM : Edoardo Sandoval Rivera, edoardo@dtmac.com.mx';
			     }//Fin si sIdAccountUsr != candidatoFacturar.TAM_DistribuidorCC__c*/
            }//Fin si candidatoFacturar.TAM_DistribuidorCC__c != null
 		}//Fin del for para candidatoFacturar
				
		System.debug('EN getDatosCliente objWrpCteCorp: ' + objWrpCteCorp);
        return objWrpCteCorp;
    }

    //Obtener upsDatosCliente.
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion upsDatosCliente(String recordId, wrpDatosClienteCorporativo datosClienteCorp,
    	String sRango, String sGpoCorp) {
    	System.debug('EN upsDatosCliente recordId: ' + recordId + ' sRango: ' + sRango + ' sGpoCorp: ' + sGpoCorp);
    	System.debug('EN upsDatosCliente datosClienteCorp: ' + datosClienteCorp);
    	System.debug('EN upsDatosCliente datosClienteCorp.objClienteCorporativo.id: ' + datosClienteCorp.objClienteCorporativo.id);
    			
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se actualizarón con exito.');		
    
        //Un SP para hacer el comit de toso en caso de alla habido errorres
        SavePoint sp = Database.setSavePoint();

    	//Toma el registro de wrpDatosClienteCorporativo.objClienteCorporativo y has un upsert sobre el campo de id
    	TAM_ReactivarCliente__c nuevoClienteCorporativo = datosClienteCorp.objClienteCorporativo;
    	if (nuevoClienteCorporativo.TAM_Pais__c == null)
    		nuevoClienteCorporativo.TAM_Pais__c = 'MEXICO';
        nuevoClienteCorporativo.TAM_ProgramaRango__c = (sRango != null && sRango != '') ? sRango : null;//nuevoClienteCorporativo.TAM_ProgramaRango__c;
        nuevoClienteCorporativo.TAM_GrupoCorporativo__c = (sGpoCorp != null && sGpoCorp != '') ? sGpoCorp : null; //nuevoClienteCorporativo.TAM_GrupoCorporativo__c;
    	System.debug('EN upsDatosCliente nuevoClienteCorporativo: ' + nuevoClienteCorporativo);    		
    		
    	//Actualiza el registro de 	nuevoClienteCorporativo
    	upsert nuevoClienteCorporativo TAM_ReactivarCliente__c.id; 
    	
    	//Actualiza los datos del contacto cliente recien creado en TAM_ReactivarCliente__c
    	wrpDatosClienteCorporativo objWrpCteCorp = new wrpDatosClienteCorporativo(nuevoClienteCorporativo, 
    		datosClienteCorp.strTipoClienteCorp, new Account());
    	
    	//Consulta el nombre del rango
        for (Rangos__c objTAMRango : [Select r.Name, r.Id, r.Activo__c, r.RecordTypeId 
            From Rangos__c r Where id =:sRango]){
            objRespoPaso.strParamOpcional2 = objTAMRango.Name;	    
    	}

        //Si es una prueba
        //Database.rollback(sp);
    	
    	objRespoPaso.objParamOpcional = objWrpCteCorp;
        objRespoPaso.objParamOpcional.strRango = objRespoPaso.strParamOpcional2;
        System.debug('EN upsDatosCliente objRespoPaso: ' + objRespoPaso);         
        System.debug('EN upsDatosCliente objWrpCteCorp: ' + objWrpCteCorp);
    	 
    	//Regresa el objeto del tipo TAM_WrpResultadoActualizacion objRespoPaso
    	return objRespoPaso;
    }
    
    
    //Obtener getDatosCliente.
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion creaSolCteCorp(String recordId, wrpDatosClienteCorporativo datosClienteCorp,
    	String sRango, String sGpoCorp) {
    	System.debug('EN creaSolCteCorp recordId: ' + recordId + ' sRango: ' + sRango + ' sGpoCorp: ' + sGpoCorp);
    	System.debug('EN creaSolCteCorp datosClienteCorp: ' + datosClienteCorp);
    	System.debug('EN creaSolCteCorp datosClienteCorp.objClienteCorporativo.id: ' + datosClienteCorp.objClienteCorporativo.id);

        List<Account> lClienteUpd = new List<Account>();
        List<TAM_SolicitudesParaAprobar__c> lTAM_SolicitudesParaAprobarUps = new List<TAM_SolicitudesParaAprobar__c>();    	
        Map<String, String> mapNomCandIdCand = new Map<String, String>();    	
    	TAM_ReactivarCliente__c nuevoClienteCorporativo = datosClienteCorp.objClienteCorporativo;    		
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se actualizarón con exito.');		

		//Un SP para hacer el comit de toso en caso de alla habido errorres
	    SavePoint sp = Database.setSavePoint();
		try{
		
		//Actualiza el objeto del nuevoClienteCorporativo
		upsert nuevoClienteCorporativo TAM_ReactivarCliente__c.id;
    	System.debug('EN creaSolCteCorp despues de act al nuevoClienteCorporativo: ' + nuevoClienteCorporativo);
		    	
    	String sIdProp = '';
        //Comsulta el propietario del reg de TAM_ReactivarCliente__c
        for (TAM_ReactivarCliente__c objReactCteCorp : [Select id, OwnerId From TAM_ReactivarCliente__c
        	Where id =:nuevoClienteCorporativo.id]){
        	sIdProp = objReactCteCorp.OwnerId;
        }
    	String sCodDist = '';        
        //Consulta el codigo del distribuidor asociado al usuario sIdProp
        for (User prop : [Select id, CodigoDistribuidor__c From User Where id =:sIdProp]){
        	sCodDist = prop.CodigoDistribuidor__c == null ? '57001' : prop.CodigoDistribuidor__c;
        }
        String sNameDist = '';
        //Consulta el nombre del dustribuidor
        for (Account cliente : [Select id, Name From Account Where Codigo_Distribuidor__c =:sCodDist]){
        	sNameDist = cliente.Name;
        }
        
        //Crea el registro en la tabla de TAM_SolicitudesParaAprobar 
        TAM_SolicitudesParaAprobar__c objTAMSolicitudesParaAprobar = new TAM_SolicitudesParaAprobar__c();
        String sNombreSolcitud = nuevoClienteCorporativo.TAM_TipoCliente__c == 'Persona moral' ? nuevoClienteCorporativo.Name : nuevoClienteCorporativo.Name.toUpperCase() + ' ' + nuevoClienteCorporativo.TAM_ApellidoPaterno__c.toUpperCase() + ' ' + (nuevoClienteCorporativo.TAM_ApellidoMaterno__c != null ? nuevoClienteCorporativo.TAM_ApellidoMaterno__c.toUpperCase() : '');
        //objTAMSolicitudesParaAprobar.Name = L.FirstName.toUpperCase() + ' ' + l.LastName.toUpperCase() + ' ' + (l.FWY_ApellidoMaterno__c != null ? l.FWY_ApellidoMaterno__c.toUpperCase() : '');
        objTAMSolicitudesParaAprobar.TAM_ClienteCorporativoRef__c = recordId;
        objTAMSolicitudesParaAprobar.TAM_ReactivarCliente__c = nuevoClienteCorporativo.id;         
        objTAMSolicitudesParaAprobar.TAM_Estatus__c = 'Proceso';

        objTAMSolicitudesParaAprobar.TAM_CodigoDistribuidorRef__c = sCodDist;
        objTAMSolicitudesParaAprobar.TAM_NombreDistribuidorRef__c = sNameDist;
        objTAMSolicitudesParaAprobar.TAM_ProgramaRandoRef__c = nuevoClienteCorporativo.TAM_ProgramaRango__c; //(sRango != null && sRango != '') ? sRango : null;//nuevoClienteCorporativo.TAM_ProgramaRango__c;
        objTAMSolicitudesParaAprobar.TAM_GrupoCorporativoRef__c = nuevoClienteCorporativo.TAM_GrupoCorporativo__c; //(sGpoCorp != null && sGpoCorp != '') ? sGpoCorp : null; //nuevoClienteCorporativo.TAM_GrupoCorporativo__c;
        objTAMSolicitudesParaAprobar.TAM_SinGrupoCorporativoRef__c = (sGpoCorp != null && sGpoCorp != '') ? true : false; // nuevoClienteCorporativo.TAM_GrupoCorporativo__c == null ? false : true;

        objTAMSolicitudesParaAprobar.Name = sNombreSolcitud;
        objTAMSolicitudesParaAprobar.TAM_RazonSocial__c = sNombreSolcitud;
        objTAMSolicitudesParaAprobar.TAM_NombreComercial__c = nuevoClienteCorporativo.TAM_NombreComercial__c;
        
        objTAMSolicitudesParaAprobar.TAM_RFCRef__c = nuevoClienteCorporativo.TAM_RFC__c;
        objTAMSolicitudesParaAprobar.TAM_CorreoCorporativoRef__c = nuevoClienteCorporativo.TAM_CorreoElectronico__c; 
        objTAMSolicitudesParaAprobar.TAM_TelefonoCorporativoRef__c = nuevoClienteCorporativo.TAM_Telefono__c;
        objTAMSolicitudesParaAprobar.TAM_SitioWeb__c = nuevoClienteCorporativo.TAM_SitioWeb__c != null ? nuevoClienteCorporativo.TAM_SitioWeb__c : '';

        objTAMSolicitudesParaAprobar.TAM_CalleRef__c = nuevoClienteCorporativo.TAM_Calle__c != null ? nuevoClienteCorporativo.TAM_Calle__c : '';
        objTAMSolicitudesParaAprobar.TAM_CodigoPostalRef__c = nuevoClienteCorporativo.TAM_CodigoPostal__c;
        objTAMSolicitudesParaAprobar.TAM_ColoniaRef__c = nuevoClienteCorporativo.TAM_Colonia__c;
        objTAMSolicitudesParaAprobar.TAM_CiudadRef__c = nuevoClienteCorporativo.TAM_Ciudad__c;
        objTAMSolicitudesParaAprobar.TAM_EstadoRef__c = nuevoClienteCorporativo.TAM_Estado__c ;
        
        objTAMSolicitudesParaAprobar.TAM_NombreRefRL__c = nuevoClienteCorporativo.TAM_NombreRL__c;
        objTAMSolicitudesParaAprobar.TAM_ApellidoPaternoRefRL__c = nuevoClienteCorporativo.TAM_ApellidoPaternoRL__c;
        objTAMSolicitudesParaAprobar.TAM_ApellidoMaternoRefRL__c = nuevoClienteCorporativo.TAM_ApellidoMaternoRL__c;
        objTAMSolicitudesParaAprobar.TAM_RFCRefRL__c = nuevoClienteCorporativo.TAM_RFCRL__c;
        objTAMSolicitudesParaAprobar.TAM_CorreoElectronicoRefRL__c = nuevoClienteCorporativo.TAM_CorreoElectronicoRL__c;
        objTAMSolicitudesParaAprobar.TAM_TelefonoRefRL__c = nuevoClienteCorporativo.TAM_TelefonoRL__c;
        objTAMSolicitudesParaAprobar.RecordTypeId = VaRtCteCorp;
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp objTAMSolicitudesParaAprobar: ' + objTAMSolicitudesParaAprobar);

        //Mete a la lista de lTAM_SolicitudesParaAprobarUps el objeto de  objTAMSolicitudesParaAprobar
        lTAM_SolicitudesParaAprobarUps.add(objTAMSolicitudesParaAprobar);
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp lTAM_SolicitudesParaAprobarUps: ' + lTAM_SolicitudesParaAprobarUps);

        //Toma los archivos adjuntos que tiene asociado el Lead y crealos ahora asociadoa a  la nueva objTAMSolicitudesParaAprobar
        mapNomCandIdCand.put(objTAMSolicitudesParaAprobar.Name, nuevoClienteCorporativo.id);
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp mapNomCandIdCand: ' + mapNomCandIdCand);
        
        //Crea una lista para actualiza el contacto asociado
        lClienteUpd.add(New Account(ID = recordId, TAM_EstatusCliente__c = 'En proceso', 
        	TAM_ProgramaRango__c = nuevoClienteCorporativo.TAM_ProgramaRango__c,
        	TAM_GrupoCorporativo__c = nuevoClienteCorporativo.TAM_GrupoCorporativo__c,
        	TAM_ComentariosAutorizacion__c = null));
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp lClienteUpd: ' + lClienteUpd);
        //Actualiza los datos del cliente para la solicitud
        //update lClienteUpd;
        
        Integer iCnt = 0;
        Map<String, String> mapIdSolFinIdCand = new Map<String, String>();
        //Ve si tiene algo la lista de lTAM_SolicitudesParaAprobarUps
        if (!lTAM_SolicitudesParaAprobarUps.isEmpty()){
           List<Database.SaveResult> lSaveResultSol = Database.insert(lTAM_SolicitudesParaAprobarUps);
           //Recorre la lista de lSaveResultSol
           for (Database.SaveResult objSvResul : lSaveResultSol){
               if (objSvResul.isSuccess()){
                  //Toma el nombre del rg que se esta actualizando
                  String sName = lTAM_SolicitudesParaAprobarUps.get(iCnt).Name;
                  String sIdCand = lTAM_SolicitudesParaAprobarUps.get(iCnt).TAM_ClienteCorporativoRef__c;
                  //Ve si existe en el mapa de mapNomCandIdCand
                  if (mapNomCandIdCand.containsKey(sName))
                     mapIdSolFinIdCand.put(sIdCand, objSvResul.getId());
               }//Fin si objSvResul.isSuccess()
               iCnt++;	
           }//Fin del for para lSaveResultSol
        }//Fin si lTAM_SolicitudesParaAprobarUps.isEmpty())
            
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp mapIdSolFinIdCand: ' + mapIdSolFinIdCand);
        List<ContentDocumentLink> lContentDocumentLinkNew = new List<ContentDocumentLink>();
        //Ya tienes los Id de las nuevas solicitudes entonces asocia los adjuntos
        if (!mapIdSolFinIdCand.isEmpty()){
           //Recorre la lista de Documentos asociados a mapIdSolFinIdCand.values()
           for(ContentDocumentLink objContentDocumentLink : [Select c.Visibility, c.LinkedEntityId, c.Id, c.ContentDocumentId, c.ShareType 
               From ContentDocumentLink c where LinkedEntityId = :mapIdSolFinIdCand.KeySet()]){
               //Crea el reg en lContentDocumentLinkNew	
               lContentDocumentLinkNew.add(new ContentDocumentLink(
                   Visibility = objContentDocumentLink.Visibility,
                   LinkedEntityId = mapIdSolFinIdCand.get(objContentDocumentLink.LinkedEntityId),
                   ContentDocumentId = objContentDocumentLink.ContentDocumentId,
                   ShareType = objContentDocumentLink.ShareType
               	   )
               );                                                                  
           }
        }//Fin si !mapIdSolFinIdCand.isEmpty()
            
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp lContentDocumentLinkNew: ' + lContentDocumentLinkNew); 
        //Ve si tiene algo la lista de lContentDocumentLinkNew y crea los reg
        if (!lContentDocumentLinkNew.isEmpty())
           List<Database.SaveResult> lSaveResultArchvCand = Database.insert(lContentDocumentLinkNew);
        System.debug(LoggingLevel.INFO,'EN creaSolCteCorp lClienteUpd: ' + lClienteUpd);           
		                	
    	//Actualiza los datos del contacto cliente recien creado en TAM_ReactivarCliente__c
    	wrpDatosClienteCorporativo objWrpCteCorp = new wrpDatosClienteCorporativo(nuevoClienteCorporativo, 
    		datosClienteCorp.strTipoClienteCorp, new Account());
    	
    	System.debug('EN creaSolCteCorp objWrpCteCorp: ' + objWrpCteCorp);    		
    	objRespoPaso.objParamOpcional = objWrpCteCorp; 

		}catch(Exception ex){
			System.debug(LoggingLevel.INFO,'EN creaSolCteCorp ERROR: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber());
			objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'EN creaSolCteCorp ERROR: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber());		
			//Alica el rollback
			Database.rollback(sp);
		}
    	
    	//Si es una prueba
    	//Database.rollback(sp);
    	 
    	//Regresa el objeto del tipo TAM_WrpResultadoActualizacion objRespoPaso
    	return objRespoPaso;
    }
    

    //Obtener getRangos.
    @AuraEnabled
    public static List<wrpMapDatosCatalogos> getRangos(String sRango, String sGpoCorp, String recordId,
        wrpDatosClienteCorporativo datosClienteCorp) {
    	System.debug('EN getRangos sRango: ' + sRango + ' sGpoCorp: ' + sGpoCorp + ' recordId: ' + recordId);
        System.debug('EN getRangos datosClienteCorp: ' + datosClienteCorp);
    	String sOk = '';
		
		Map<String, String> MapTipoOfertaFactPaso = new Map<String, String>();
		List<String> lTipoOfertaFactPaso = new List<String>();
		List<wrpMapDatosCatalogos> lSObjet = new List<wrpMapDatosCatalogos>();
		String sTipoCliente = '';
		
		//Comsulta los datos del cliente y ve que tipo es
		for (TAM_ReactivarCliente__c reacCliente : [Select id, TAM_TipoCliente__c 
		    From TAM_ReactivarCliente__c Where TAM_Cliente__c =:recordId]){
            sTipoCliente = 	reacCliente.TAM_TipoCliente__c;	    
		}
        System.debug('EN getRangos sTipoCliente: ' + sTipoCliente);
				
		if (sRango == ''){
			lSObjet.add(new wrpMapDatosCatalogos(
					'', '- Selecciona -', true
				)
			);		
    	}//Fin si sRango == ''

        //Cosnsulta los rangos y asigna los que corresponden    	
		for (Rangos__c objTAMRango : [Select r.Name, r.Id, r.Activo__c, r.RecordTypeId 
			From Rangos__c r Where r.Activo__c = true Order by Name]){
            //Inicializa los datos
            lTipoOfertaFactPaso.add(objTAMRango.Name);
            MapTipoOfertaFactPaso.put(objTAMRango.id, objTAMRango.Name);
			//Ve si se trata de 'Persona Física'
			if ((//sTipoCliente == 'Persona Física' || 
			     datosClienteCorp.objClienteCorporativo.TAM_TipoCliente__c == 'Persona Física'
			     || datosClienteCorp.objClienteCorporativo.TAM_TipoCliente__c == 'Person física') 
			     && objTAMRango.RecordTypeId == VaRtCteRangoProg
			    ){ //VaRtCteRangoFlot
                System.debug('EN getRangos datosClienteCorp Persona Física: ' + objTAMRango.Name);
                lSObjet.add(new wrpMapDatosCatalogos(
                        objTAMRango.id, objTAMRango.Name, objTAMRango.Name == sRango ? true : false
                    )
                );
			}//Fin si sTipoCliente == 'Persona Física' 
			//Ve si se trata de 'Persona Moral'
			if ( //sTipoCliente == 'Persona Moral' || sTipoCliente == 'Fisica con actividad empresarial' ||
			     datosClienteCorp.objClienteCorporativo.TAM_TipoCliente__c == 'Persona moral' 
			     || datosClienteCorp.objClienteCorporativo.TAM_TipoCliente__c == 'Fisica con actividad empresarial'
			    ){
                System.debug('EN getRangos datosClienteCorp Persona moral: ' + objTAMRango.Name);
                //Inicializa los datos
                lSObjet.add(new wrpMapDatosCatalogos(
                        objTAMRango.id, objTAMRango.Name, objTAMRango.Name == sRango ? true : false
                    )
                );			
			}//Fin si (sTipoCliente == 'Persona Moral'
		}
				
		System.debug('EN getRangos lSObjet: ' + lSObjet);
        return lSObjet;
    } 

    //Obtener updateDatosCandidato.
    @AuraEnabled
    public static List<wrpMapDatosCatalogos> getGruposCorporativos(String sRango, String sGpoCorp) {
    	System.debug('EN getGruposCorporativos sRango: ' + sRango + ' sGpoCorp: ' + sGpoCorp);
    	String sOk = '';
		
		Map<String, String> MapTipoOfertaFactPaso = new Map<String, String>();
		List<String> lTipoOfertaFactPaso = new List<String>();
		List<wrpMapDatosCatalogos> lSObjet = new List<wrpMapDatosCatalogos>();
		if (sGpoCorp == ''){
			lSObjet.add(new wrpMapDatosCatalogos(
					'', '- Selecciona -', true
				)
			);	
		}// fIN SI sGpoCorp == ''
		for (TAM_GrupoCorporativo__c objTAMCteCorp : [Select t.TAM_Estatus__c, t.Name, t.Id 
			From TAM_GrupoCorporativo__c t Order by Name]){
			lTipoOfertaFactPaso.add(objTAMCteCorp.Name);
			MapTipoOfertaFactPaso.put(objTAMCteCorp.id, objTAMCteCorp.Name);
			lSObjet.add(new wrpMapDatosCatalogos(
					objTAMCteCorp.id, objTAMCteCorp.Name, objTAMCteCorp.Name == sGpoCorp ? true : false
				)
			);	
		}
				
		System.debug('EN getGruposCorporativos lSObjet: ' + lSObjet);
        return lSObjet;
    }  

    private static String getDistribuidor(String sUserId){
        System.debug('EN TAM_ActivaClienteCorporativoCtrl.getDistribuidor sUserId '+ sUserId);

        String sDistId;
        String sCodigoDistribuidor;
        User usuarioActual = new User();
                
        //Los datos del distribuidor
        for (User usuario : [Select u.id, u.Name, u.CodigoDistribuidor__c, u.Email
                             From User u Where id = :sUserId and CodigoDistribuidor__c != null]){
                             usuarioActual = usuario;
        }//Fin del for para User
        sCodigoDistribuidor = usuarioActual.CodigoDistribuidor__c != null ? usuarioActual.CodigoDistribuidor__c : '57000'; 
        System.debug('EN TAM_ActivaClienteCorporativoCtrl.getDistribuidor sCodigoDistribuidor '+ sCodigoDistribuidor);
        
        //Ya tienes el No de Dist entonces busca la cuenta asociada a ese Dist
        for (Account dist : [Select id, a.RecordType.Name, a.RecordTypeId, a.Codigo_Distribuidor__c 
            From Account a Where a.Codigo_Distribuidor__c =: sCodigoDistribuidor 
            and RecordTypeId =: VaRtAccRegDist]){
            sDistId = dist.id;
        }
        
        System.debug('EN TAM_ActivaClienteCorporativoCtrl.getDistribuidor sDistId '+ sDistId);
        return sDistId;
    }
    
}