public without sharing class ConciliacionTriggerHandler extends TriggerHandler{
    public static boolean executeFutureMethod = true;
    private List<Conciliacion__c> conciliacionList;
    //private String IdMesAnnio{get;set;}
    /*
    class UniqueKey {
        UniqueKey(string anio, String mes, String vin) {
            this.anio = anio;
            this.mes = mes;
            this.vin = vin;
        }
        string anio, mes, vin;
        integer hashCode() {
            return toString().hashCode();
        }
        boolean equals(Object o) {
            return toString() == ((UniqueKey)o).toString();
        }
        public override string toString() {
            return String.format(
                'AnioConciliacion__c = \'\'{0}\'\' AND MesConciliacion__c = \'\'{1}\'\' AND VIN__c = \'\'{2}\'\'',
                new String[] {
                    anio==null?'':String.escapeSingleQuotes(anio.tolowercase()),
                        mes==null?'':String.escapeSingleQuotes(mes.tolowercase()),
                            vin==null?'':String.escapeSingleQuotes(vin.tolowercase())
                                }
            );
        }
    }*/
    
    public ConciliacionTriggerHandler() { 
        this.conciliacionList = (List<Conciliacion__c>) Trigger.new; 
    }
    
    public override void beforeInsert(){ 
        validaSolicitudDemo(conciliacionList);
        //validaCampos(conciliacionList);
    }
    
    public override void beforeUpdate(){  
        validaSolicitudDemo(conciliacionList);
        asignAP(conciliacionList);
        //validaCampos(conciliacionList);
    }
    
    public override void afterInsert(){ 
        actualizaTotalInteresesAutoDemo(conciliacionList);
        validateDuplicateRecords(conciliacionList);
        //validaCampos(conciliacionList);
    }
    
    public override void afterUpdate(){
        actualizaTotalInteresesAutoDemo(conciliacionList); System.debug('ConciliacionAntes de SummaryAP = '+ conciliacionList);
        summaryAP(conciliacionList);
        validateDuplicateRecords(conciliacionList);
    }
    
    private void validaSolicitudDemo(List<Conciliacion__c> conciliaciones){   
        System.debug('Entraste al metodo before update/insert de conciliacion VALIDA SOLICITUD DEMO'); 
        Map<String,SolicitudAutoDemo__c> sadMap = new Map<String,SolicitudAutoDemo__c>();
        Set<String> vines = new Set<String>();
        
        for(Conciliacion__c con: conciliaciones){
            con.RegistroDuplicado__c = ConciliacionTriggerHandler.executeFutureMethod ? false: con.RegistroDuplicado__c;
            
            if(con.VIN__c!=null && con.VIN__c!=''){
                vines.add(con.VIN__c);
            }else{
                con.addError('Es obligatorio asociar un VIN');
            }
        }
        
        for(SolicitudAutoDemo__c sad:[SELECT Id, Name, VIN__c, VIN__r.Name,EstatusAutoDemo__c,Estatus__c FROM SolicitudAutoDemo__c WHERE  VIN__r.Name=:vines]){ 
            sadMap.put(sad.VIN__r.Name,sad); 
        }
        System.debug('sadMap: ' +sadMap);
        
        for(Conciliacion__c con: conciliaciones){
            //       if(con.AnioConciliacion__c!=null && con.MesConciliacion__c=='' && con.AnioConciliacion__c!=null && con.MesConciliacion__c==''){
            con.AnioConciliacionGroup__c = con.AnioConciliacion__c;
            con.MesConciliacionGroup__c = con.MesConciliacion__c;
            con.IdMesAnio__c = con.AnioConciliacion__c+con.MesConciliacion__c;
            System.debug('IdMesAnio__c '+con.IdMesAnio__c);
            //        }
            if(con.VIN__c!=null && con.VIN__c!=''){
                if(sadMap.containsKey(con.VIN__c)){
                    System.debug('Si hay solicitud para esa conciliacion');
                    con.AutoDemoAsociado__c = true;
                    con.SolicitudAutoDemo__c = sadMap.get(con.VIN__c).Id; 
                    con.EstatusAutoDemo__c = con.EstatusAutoDemo__c == null || con.EstatusAutoDemo__c =='' ? sadMap.get(con.VIN__c).Estatus__c: con.EstatusAutoDemo__c;
                }else{
                    //con.addError('No existe solicitud asociada al VIN especificado');
                }
            }
        }
    }
    
    private void actualizaTotalInteresesAutoDemo(List<Conciliacion__c> conciliaciones){
        System.debug('Entra conciliacion INTERESES SUM');
        Map<String,List<SolicitudAutoDemo__c>> mapSadUpd = new Map<String,List<SolicitudAutoDemo__c>>(); 
        List<SolicitudAutoDemo__c> sadListUpd = new List<SolicitudAutoDemo__c>();
        Set<String> setConVIN = new Set<String>();
        String IdVin;
        
        for(Conciliacion__c con:conciliaciones){
            if(con.VIN__c!=null){
                setConVIN.add(con.VIN__c);
            }
        } 
        
        for(SolicitudAutoDemo__c sad:[SELECT Id, VIN__r.Name, TotalInteresesAutoDemo__c, VIN__c,Estatus__c
                                      FROM SolicitudAutoDemo__c WHERE VIN__r.Name IN:setConVIN]){
                                          if(sad.VIN__r.Name!=null && sad.VIN__r.Name!=''){
                                              if(mapSadUpd.containsKey(sad.VIN__r.Name)){
                                                  mapSadUpd.get(sad.VIN__r.Name).add(sad);
                                              }else{
                                                  mapSadUpd.put(sad.VIN__r.Name, new List<SolicitudAutoDemo__c>{sad});
                                              }
                                          }
                                      }
        System.debug('MAPA SAD CON' +mapSadUpd);
        
        for(AggregateResult conVIN : [SELECT VIN__c, SUM(InteresesVencidos__c) sumIntereses 
                                     FROM Conciliacion__c WHERE VIN__c IN: mapSadUpd.keySet() GROUP BY VIN__c]){
                                          IdVin  = (String)conVIN.get('VIN__c');
                                          System.debug('IdVin: ' +IdVin);
                                          System.debug('SUMA CON VIN: '+conVIN);
                                          if(mapSadUpd.containsKey(IdVin)){
                                              for(SolicitudAutoDemo__c sadUpd: mapSadUpd.get(IdVin)){
                                                  if(conVIN.get('sumIntereses')!=null){
                                                      sadUpd.TotalInteresesAutoDemo__c = (Decimal)conVIN.get('sumIntereses')*1.16;
                                                      System.debug('sadUpd: '+mapSadUpd);
                                                  }
                                                  sadListUpd.add(sadUpd);
                                              } 
                                          } 
                                      }
        
        try{
            update sadListUpd;
        }catch(Exception e){
            System.debug('ERROR UPDA SAD CON INTERESES: ' +e.getMessage());
        }    
    }
    
    private void asignAP(List<Conciliacion__c> conciliaciones){
        Set<String> dealersCode = new Set<String>();
        Map<String, List<AsignacionPresupuesto__c>> mapaAsignaciones;
        
        for(Conciliacion__c con: conciliaciones){
            if(con.ValidacionTMEX__c){
                if(con.CodigoDistribuidor__c != null && con.CodigoDistribuidor__c !=''){
                    dealersCode.add(con.CodigoDistribuidor__c);
                }
            }
        }
        System.debug('dealersCode'+dealersCode);
        mapaAsignaciones = this.getAsignacionesMap(dealersCode);
        for(Conciliacion__c con: conciliaciones){
            if(con.ValidacionTMEX__c){
                if(con.CodigoDistribuidor__c != null && con.CodigoDistribuidor__c !=''){
                    if(mapaAsignaciones.containsKey(con.CodigoDistribuidor__c)){
                        con.AsignacionPresupuesto__c = null;
                        
                        for(AsignacionPresupuesto__c ap: mapaAsignaciones.get(con.CodigoDistribuidor__c)){
                            if(ap.Estatus__c=='Vigente'){
                                con.AsignacionPresupuesto__c = ap.Id;
                                break;
                            }
                        } 
                        
                    } else {
                        con.AsignacionPresupuesto__c = null;
                    }
                }
            }
        }
    }
    
    private void summaryAP(List<Conciliacion__c> conciliaciones){
        Set<String> asignacionesIDS = new Set<String>();
        Map<Id, AsignacionPresupuesto__c> asignacionMap;
        Map<String, Set<String>> conteosVines;
        for(Conciliacion__c con: conciliaciones){
            if(con.ValidacionTMEX__c){
                if(con.AsignacionPresupuesto__c != null){
                    asignacionesIDS.add(con.AsignacionPresupuesto__c);
                }
            }
        }
        asignacionMap = this.loadAsignations(asignacionesIDS);
        
        if(asignacionMap.size() > 0){
            for(AsignacionPresupuesto__c ap : asignacionMap.values()){
                conteosVines = new Map<String, Set<String>>{
                    'Enero' => new Set<String>(),
                        'Febrero' => new Set<String>(),
                        'Marzo' => new Set<String>(),
                        'Abril' => new Set<String>(),
                        'Mayo' => new Set<String>(),
                        'Junio' => new Set<String>(),
                        'Julio' => new Set<String>(),
                        'Agosto' => new Set<String>(),
                        'Septiembre' => new Set<String>(),
                        'Octubre' => new Set<String>(),
                        'Noviembre' => new Set<String>(),
                        'Diciembre' => new Set<String>()
                        };
                            
                            ap.InteresesPagadosTFSEnero__c = 0; 
                ap.InteresesPagadosTFSFebrero__c = 0; 
                ap.InteresesPagadosTFSMarzo__c = 0; 
                ap.InteresesPagadosTFSAbril__c = 0;
                ap.InteresesPagadosTFSMayo__c = 0; 
                ap.InteresesPagadosTFSJunio__c = 0; 
                ap.InteresesPagadosTFSJulio__c = 0; 
                ap.InteresesPagadosTFSAgosto__c = 0;
                ap.InteresesPagadosTFSSeptiembre__c = 0; 
                ap.InteresesPagadosTFSOctubre__c = 0; 
                ap.InteresesPagadosTFSNoviembre__c = 0; 
                ap.InteresesPagadosTFSDiciembre__c = 0;                
                
                ap.AutosDemoPagadosTFSEnero__c = 0; 
                ap.AutosDemoPagadosTFSFebrero__c = 0; 
                ap.AutosDemoPagadosTFSMarzo__c = 0; 
                ap.AutosDemoPagadosTFSAbril__c = 0;
                ap.AutosDemoPagadosTFSMayo__c = 0;
                ap.AutosDemoPagadosTFSJunio__c = 0; 
                ap.AutosDemoPagadosTFSJulio__c = 0; 
                ap.AutosDemoPagadosTFSAgosto__c = 0; 
                ap.AutosDemoPagadosTFSSeptiembre__c = 0;
                ap.AutosDemoPagadosTFSOctubre__c = 0; 
                ap.AutosDemoPagadosTFSNoviembre__c = 0; 
                ap.AutosDemoPagadosTFSDiciembre__c = 0;
                
                for(Conciliacion__c con : ap.Conciliaci_n__r){
                    if(con.MesConciliacion__c == 'Enero'){
                        ap.InteresesPagadosTFSEnero__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSEnero__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Febrero'){
                        ap.InteresesPagadosTFSFebrero__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSFebrero__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Marzo'){
                        ap.InteresesPagadosTFSMarzo__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSMarzo__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Abril'){
                        ap.InteresesPagadosTFSAbril__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSAbril__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Mayo'){
                        ap.InteresesPagadosTFSMayo__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSMayo__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Junio'){
                        ap.InteresesPagadosTFSJunio__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSJunio__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Julio'){
                        ap.InteresesPagadosTFSJulio__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSJulio__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Agosto'){
                        ap.InteresesPagadosTFSAgosto__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSAgosto__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Septiembre'){
                        ap.InteresesPagadosTFSSeptiembre__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSSeptiembre__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Octubre'){
                        ap.InteresesPagadosTFSOctubre__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSOctubre__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Noviembre'){
                        ap.InteresesPagadosTFSNoviembre__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSNoviembre__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                        
                    } else if(con.MesConciliacion__c == 'Diciembre'){
                        ap.InteresesPagadosTFSDiciembre__c += this.validaNull(con.InteresesVencidos__c);
                        if(!conteosVines.get(con.MesConciliacion__c).contains(con.VIN__c)){
                            ap.AutosDemoPagadosTFSDiciembre__c += 1;
                            conteosVines.get(con.MesConciliacion__c).add(con.VIN__c); 
                        }
                    }
                }
            }
        }
        
        if(asignacionMap.size() > 0){
            try{
                update asignacionMap.values();
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }   
    }
    
    private void validateDuplicateRecords(List<Conciliacion__c> conciliaciones){
        Set<String> conciliacionKeys = new Set<String>();
        Set<String> conciliacionQueryKeys = new Set<String>();
        Set<String> duplicadosIDs = new Set<String>();

        for(Conciliacion__c con:conciliaciones) {
            conciliacionKeys.add(con.AnioConciliacion__c +'-'+con.MesConciliacion__c+'-'+con.VIN__c);
        }
        
        for(Conciliacion__c con: [SELECT Id, Name, ConciliacionUniqueKey__c FROM Conciliacion__c WHERE ConciliacionUniqueKey__c IN: conciliacionKeys AND Id NOT IN: Trigger.new]){
            duplicadosIDs.add(con.Id);
            conciliacionQueryKeys.add(con.ConciliacionUniqueKey__c);
        }

        for(Conciliacion__c con:conciliaciones) {
            if(conciliacionQueryKeys.contains(con.AnioConciliacion__c +'-'+con.MesConciliacion__c+'-'+con.VIN__c)){
                duplicadosIDs.add(con.Id);
            }
        }
        System.debug(JSON.serialize(duplicadosIDs));
        if(duplicadosIDs.size() > 0){
            System.enqueueJob(new QueueableCall(duplicadosIDs));
        }
        
        

        /*Map<UniqueKey, Conciliacion__c> mapCon = new Map<UniqueKey, Conciliacion__c>();
        for(Conciliacion__c con:conciliaciones) {
            UniqueKey key = new UniqueKey(con.AnioConciliacion__c, con.MesConciliacion__c, con.VIN__c);
            if(mapCon.containskey(key)) {
                con.RegistroDuplicado__c=true;
            } else {
                mapCon.put(key, con);
            }
        }
        String[] keys = new String[0];
        for(UniqueKey key:mapCon.keyset()) {
            keys.add(key.tostring());
        } 
        String query = string.format(
            'SELECT Id,AnioConciliacion__c,MesConciliacion__c,VIN__c FROM Conciliacion__c WHERE {0}',
            new String[] {
                String.join(keys, ' OR ')
                    }
        );
        
        System.debug('query:' +query);
        if(!mapCon.isEmpty()) {
            for(Conciliacion__c con:Database.query(query)) {
                UniqueKey key = new UniqueKey(con.AnioConciliacion__c, con.MesConciliacion__c, con.VIN__c);
                if(mapCon.containskey(key)) {
                    mapCon.get(key).RegistroDuplicado__c=true;
                }
            }
        }*/
    }
    
    //--------------------METHODS ---------------------------//
    public Map<String, List<AsignacionPresupuesto__c>> getAsignacionesMap(Set<String> dealersCode){
        Map<String, List<AsignacionPresupuesto__c>> retMap = new Map<String, List<AsignacionPresupuesto__c>>();
        for(Account acc: [SELECT Id, Name, Codigo_Distribuidor__c, 
                          (SELECT Id, Name, Estatus__c FROM AsignacionPresupuesto__r WHERE Estatus__c='Vigente') 
                          FROM Account WHERE Codigo_Distribuidor__c IN: dealersCode]){
                              
                              if(!retMap.containsKey(acc.Codigo_Distribuidor__c)){
                                  retMap.put(acc.Codigo_Distribuidor__c, new List<AsignacionPresupuesto__c>());
                              }
                              
                              retMap.get(acc.Codigo_Distribuidor__c).addAll(acc.AsignacionPresupuesto__r);
                          }
        System.debug('dealersCode'+dealersCode);
        System.debug('retMap'+retMap);
        return retMap;
    }
    
    public Map<Id, AsignacionPresupuesto__c> loadAsignations(Set<String> asignaciones){
        return new Map<Id, AsignacionPresupuesto__c>(
            [SELECT Id, 
             InteresesPagadosTFSEnero__c, InteresesPagadosTFSFebrero__c, InteresesPagadosTFSMarzo__c, InteresesPagadosTFSAbril__c,
             InteresesPagadosTFSMayo__c, InteresesPagadosTFSJunio__c, InteresesPagadosTFSJulio__c, InteresesPagadosTFSAgosto__c,
             InteresesPagadosTFSSeptiembre__c, InteresesPagadosTFSOctubre__c, InteresesPagadosTFSNoviembre__c, InteresesPagadosTFSDiciembre__c,
             (SELECT Id, InteresesVencidos__c, MesConciliacion__c, VIN__c FROM Conciliaci_n__r)  //Conciliacion__r
             FROM AsignacionPresupuesto__c WHERE Id IN: asignaciones]
        );
    }
    
    public Decimal validaNull(Decimal d){
        return d==null?0.0:d;
    }

    @future(callout=true)
    public static void futureMethodCall(Set<String> conciliacionIDS){
        ConciliacionTriggerHandler.executeFutureMethod = false;
        
        List<Conciliacion__c> conciliacionList = [SELECT Id, Name, RegistroDuplicado__c FROM Conciliacion__c WHERE Id IN: conciliacionIDS];
        for(Conciliacion__c con: conciliacionList){
            con.RegistroDuplicado__c = true;
        }

        update conciliacionList;
    }

    public class QueueableCall implements System.Queueable, Database.AllowsCallouts{
        private Set<String> conciliacionIDS;

        public QueueableCall(Set<String> conIDS){
            this.conciliacionIDS = conIDS;
        }

        public void execute(QueueableContext context) {
            if(ConciliacionTriggerHandler.executeFutureMethod){
                futureMethodCall(conciliacionIDS);
            }
        }
    }
}