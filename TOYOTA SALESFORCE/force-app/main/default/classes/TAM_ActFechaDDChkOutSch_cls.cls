/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_CheckOutDetalleSolicitudCompra__c
                        y toma los reg que ya tienen un TAM_Vin__c y no tienen valor en el campo de TAM_FechaVentaDD__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    20-Dic-2020          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActFechaDDChkOutSch_cls implements Schedulable {

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActFechaDDChkOutSch_cls.execute...');
        String strTipoPedido = 'Inventario';
        String strVinPrueba = '5TDGZRAH0MS049120';
        String sActualizaFechaDDCheckout = System.Label.TAM_FechaMesToyota;        
        Date dFechaConsulta = sActualizaFechaDDCheckout != 'null' ? Date.valueOf(sActualizaFechaDDCheckout) : Date.today().addDays(-1);
        System.debug('EN TAM_ActFechaDDChkOutSch_cls.execute dFechaConsulta: ' + dFechaConsulta);

        Date dFechaIniConVines = Date.today();
        Date dFechaFinConVines = Date.today();

        //Consulta el calendario de toyota y metelo al mapa de mapCalenToyota
        for (TAM_CalendarioToyota__c objCalToy : [Select id, TAM_FechaInicio__c, TAM_FechaFin__c 
            From TAM_CalendarioToyota__c Where TAM_FechaInicio__c <= :dFechaConsulta 
            And TAM_FechaFin__c >= :dFechaConsulta]){
            dFechaIniConVines = objCalToy.TAM_FechaInicio__c;
            dFechaFinConVines = objCalToy.TAM_FechaFin__c;
        }
        //Es una prueba 
        if (Test.isRunningTest()) dFechaIniConVines = Date.today();
        System.debug('EN TAM_ActFechaDDChkOutSch_cls.execute dFechaIniConVines: ' + dFechaIniConVines + ' dFechaFinConVines: ' + dFechaFinConVines);
        
        this.sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
        this.sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name ';
        this.sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
        this.sQuery += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
        //this.sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
        //this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= ' + String.valueOf(dFechaIniConVines); //2021-02-08';
        //this.sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c <= ' + String.valueOf(dFechaFinConVines); //2021-02-08';
        //this.sQuery += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';
        this.sQuery += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';
        //this.sQuery += ' Limit 5';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActFechaDDChkOutSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActFechaDDChkOutBch_cls objActFechaDDChkOutBch = new TAM_ActFechaDDChkOutBch_cls(sQuery);
        
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActFechaDDChkOutBch, 25);
                     
    }
    
}