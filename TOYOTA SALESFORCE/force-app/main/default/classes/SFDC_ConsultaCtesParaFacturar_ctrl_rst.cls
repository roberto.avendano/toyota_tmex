/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS SFDC_ConsultaClientesParaFacturar_ws_rst

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Junio-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

public with sharing class SFDC_ConsultaCtesParaFacturar_ctrl_rst {
    
    //Un constructor por default
    public SFDC_ConsultaCtesParaFacturar_ctrl_rst(){}
	
	//Ahora Crea el llamado para la serializacion JSON.deserialize
	public List<DatosProspecto> listaClientesSFDC;	
	
	public class DatosProspecto{	
		//Datos del cliente para facturación	
		public String IdProspectoSFDC {get;set;}	//Id
        public String ClaveVendedor {get;set;}      //Id
		public String numeroCliente {get;set;}		//FWY_ID_de_cliente__c
		public String nombre {get;set;}				//FistName
		public String apellidoPaterno {get;set;}	//LastName
		public String apellidoMaterno {get;set;}	//FWY_ApellidoMaterno__c
		public String tipoPersona {get;set;}		//FWY_Tipo_de_persona__c
		public String rfc {get;set;}				//TAM_RFC__c
		public String Telefono {get;set;}			//Phone
		public String celular {get;set;}			//MobilePhone
		public String direccion {get;set;}			//TAM_Calle__c
		public String noInt {get;set;}				//TAM_Estado__c
		public String noExt {get;set;}				//TAM_Pais__c
		public String colonia {get;set;}			//TAM_Colonia__c
		public String localidad {get;set;}			//TAM_Ciudad__c
		public String cp {get;set;}					//TAM_CodigoPostal__c
		public String delegacionMunicipio {get;set;}//
		public String estado {get;set;}				//TAM_Estado__c
		public String pais {get;set;}				//TAM_Pais__c
		public String email {get;set;}				//Email
        public String curp {get;set;}               //TAM_CurpFact__c
        public String fechaNacimiento {get;set;}    //TAM_FechaNacimientoFact__c
		//Daos del contacto
		public String nombreContacto {get;set;}				//FistName
		public String apellidoPaternoContacto {get;set;}	//LastName
		public String apellidoMaternoContacto {get;set;}	//FWY_ApellidoMaterno__c
		public String rfcContacto {get;set;}				//TAM_RFC__c
		public String TelefonoContacto {get;set;}			//Phone
		public String celularContacto {get;set;}			//MobilePhone
		public String emailContacto {get;set;}				//Email
		public String direccionContacto {get;set;}			//TAM_Calle__c
		public String noIntContacto {get;set;}				//TAM_Estado__c
		public String noExtContacto {get;set;}				//TAM_Pais__c
		public String cpContacto {get;set;}					//TAM_CodigoPostal__c
		public String localidadContacto {get;set;}			//TAM_Ciudad__c
		public String coloniaContacto {get;set;}			//TAM_Colonia__c
		public String delegacionMunicipioContacto {get;set;}//
		public String estadoContacto {get;set;}				//TAM_Estado__c
		public String paisContacto {get;set;}				//TAM_Pais__c
        //Daos del contacto servicio
        public String nombreContactoServ {get;set;}             //FistName
        public String apellidoPaternoContactoServ {get;set;}    //LastName
        public String apellidoMaternoContactoServ {get;set;}    //FWY_ApellidoMaterno__c
        public String TelefonoContactoServ {get;set;}           //Phone
        public String celularContactoServ {get;set;}            //MobilePhone
        public String emailContactoServ {get;set;}              //Email
		//Daos para Quiter
		public String TipoOferta {get;set;}					//TipoOferta
		public String AsesorVentas {get;set;}				//AsesorVentas
		public String Origen {get;set;}						//Origen
		public String Agencia {get;set;}					//Agencia
		public String EstadoPedidoInicial {get;set;}		//EstadoPedidoInicial
		public String TipoVenta {get;set;}					//TipoVenta
		public String TipoVentaDestino {get;set;}			//TipoVentaDestino
		//Lista de Vines
		public List<DatosVin> lstVines {get;set;}
	}

	public class DatosVin{
		public String Vin {get;set;}				//TAM_InventarioFyG__r.Name
		public String modelo {get;set;}				//TAM_InventarioFyG__r.Model_Number__c
		public String descripcion {get;set;}		//Description
		public String precioVenta {get;set;}		//TAM_PrecioVenta__c	NUEVO CAMPO
		public String colorExterior {get;set;}		//TAM_InventarioFyG__r.Exterior_Color_Description__c
		public String colorInterior {get;set;}		//TAM_InventarioFyG__r.Interior_Color_Description__c
		public String Catalogo {get;set;}			//TAM_Catalogo__c		NUEVO CAMPO
		public String AnioModelo {get;set;}			//TAM_InventarioFyG__r.Model_Year__c
		public String Carline {get;set;}			//TAM_Carline__c
		public String MonedaVenta {get;set;}		//TAM_MonedaVenta__c	NUEVO CAMPO
		public String TipoCambioPactado {get;set;}	//TAM_TipoCambioPactado__c	NUEVO CAMPO
        public String NoInventario {get;set;}        //TAM_TipoCambioPactado__c  NUEVO CAMPO
	}

	public class DatosEntrada{
		public String IdUnicoUserSFDC {get;set;}
		public String NumeroDealer {get;set;}
	}	
	
 	//El metodo que va a consultar los datos de los clientes que ya estan en la etapa de pedido en proceso y 
 	//ya estan listos para facturarrse
 	public static String getClientesParaFacturar(String sBody){ 
 		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar sBody: ' + sBody);
 		String sListaClientesFacturar = '';
 		
 		List<DatosProspecto> lDatosProspecto = new List<DatosProspecto>();
 		DatosEntrada objDatosEntradaPaso = (DatosEntrada)JSON.deserialize(sBody, DatosEntrada.class);
 		StrIng sCodDist = '';
 		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar objDatosEntradaPaso: ' + objDatosEntradaPaso);
 		
 		//Ve si existe la información de la cuenta
 		for (Account Dist : [Select id, Codigo_Distribuidor__c, TAM_TokenSFDC__c From Account
 			Where Codigo_Distribuidor__c = :objDatosEntradaPaso.NumeroDealer And
 			TAM_TokenSFDC__c = :objDatosEntradaPaso.IdUnicoUserSFDC]){
 			sCodDist = Dist.Codigo_Distribuidor__c;
 		}
 		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar sCodDist: ' + sCodDist);
 
         for (TAM_Integraciones__c onjIntegraCons : [Select t.TAM_username__c, t.TAM_password__c, t.TAM_UrlConsultaCtesDealer__c, 
                t.TAM_UrlAutenticacion__c, t.TAM_GrantType__c, t.TAM_Distribuidor__c, t.TAM_ApiSecret__c, t.TAM_ApiKey__c, 
                t.TAM_TokenSFDC__c, t.Name, t.TAM_TipoIntegreacion__c, TAM_Distribuidor__r.Codigo_Distribuidor__c 
                From TAM_Integraciones__c t where TAM_Distribuidor__r.Codigo_Distribuidor__c =: objDatosEntradaPaso.NumeroDealer
                And TAM_TokenSFDC__c = :objDatosEntradaPaso.IdUnicoUserSFDC ]){
            //Inicializa el obieto de datosIntegra
            sCodDist = onjIntegraCons.TAM_Distribuidor__r.Codigo_Distribuidor__c;
            System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar onjIntegraCons: ' + onjIntegraCons);
        }
 		
 		//Consulta todos los reistros que estan en estapa de Pedido en Proceso
 		for (Lead candidatoFacturar : [Select Id, 
 			//Datos del Cliente
 			FWY_ID_de_cliente__c, TAM_NombreRL__c, TAM_ApellidoPaternoRL__c, TAM_ApellidoMaternoRL__c, FWY_Tipo_de_persona__c,
 			TAM_TipoPersonaFact__c, TAM_RFCRL__c, TAM_TelefonoRL__c, TAM_TelefonoCelularRL__c, TAM_CorreoElectronicoRL__c,
 			TAM_CurpFact__c, TAM_FechaNacimientoFact__c,
 			//Dirección
 			TAM_Calle__c, TAM_CodigoPostal__c, TAM_Colonia__c, TAM_DelegacionMunicipio__c, TAM_Ciudad__c, 
 			TAM_Estado__c, TAM_Pais__c, TAM_NoExt__c, TAM_NoInt__c,
 			//Datos del Contacto
 			TAM_NombreContFact__c, TAM_ApellidoPaternoContFact__c, TAM_ApellidoMaternoContFact__c, TAM_RFCContFact__c,
 			TAM_TelefonoContFact__c, TAM_TelefonoCelularContFact__c, TAM_CorreoElectronicoContFact__c,
 			//Dirección contacto
 			TAM_CalleContFact__c, TAM_NoExtContacto__c, TAM_NoIntContacto__c, TAM_CodigoPostalContFact__c, TAM_ColoniaContFact__c, 
 			TAM_DelegacionMunicipioContFact__c, TAM_CiudadContFact__c, TAM_EstadoContFact__c, TAM_PaisContFact__c,
            //Datos del Contacto servicio
            TAM_NombreContactoServicio__c, TAM_ApellidoPaternoContactoServ__c, TAM_ApellidoMaternoContactoServ__c, 
            TAM_TelefonoContactoServicio__c, TAM_TelefonoCelularContactoServicio__c, TAM_CorreoElectronicoContactoServicio__c,
 			//Datos Generales QUITER
 			TAM_TipoOfertaFac__c, TAM_AsesorVentasFac__c, TAM_OrigenFac__c, TAM_AgenciaFac__c, TAM_EstadoPedidoInicialFac__c, 
 			TAM_TipoVentaFac__c, TAM_TipoVentaDestinoFac__c,
 			//Datos del inventario seleccionado
 			(Select TAM_InventarioVehiculosFyG__r.TAM_IdExterno__c, TAM_InventarioVehiculosFyG__r.Tipo_Inventario__c, 
				TAM_InventarioVehiculosFyG__r.TAM_DiasPiso__c, TAM_InventarioVehiculosFyG__r.Toms_Series_Name__c, 
				TAM_InventarioVehiculosFyG__r.Numero_de_Orden__c, TAM_InventarioVehiculosFyG__r.Model_Year__c, 
				TAM_InventarioVehiculosFyG__r.Model_Number__c, TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c, 
				TAM_InventarioVehiculosFyG__r.Interior_Color_Code__c, TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c, 
				TAM_InventarioVehiculosFyG__r.Exterior_Color_Code__c, TAM_InventarioVehiculosFyG__r.Dummy_VIN__c, 
				TAM_InventarioVehiculosFyG__r.Distributor_Invoice_Date_MM_DD_YYYY__c, TAM_InventarioVehiculosFyG__r.Dealer_Code__c, 
				TAM_InventarioVehiculosFyG__r.Name, TAM_InventarioVehiculosFyG__c, TAM_Prospecto__c,
				//Datos del modelo
				Name, TAM_Descripcion__c, TAM_Catalogo__c, TAM_Carline__c, TAM_PrecioVenta__c, TAM_MonedaVenta__c, 
				TAM_TipoCambioPactado__c, TAM_NoInventario__c 
 				From TAM_LeadInventProspecto__r ) 			
 			From Lead Where Status = 'Pedido en Proceso' And TAM_DatosValidosFacturacion__c = true
 			And FWY_codigo_distribuidor__c = :sCodDist
 			And (TAM_EstatusActualizacionDealer__c = 'Sin Consultrar' or TAM_EstatusActualizacionDealer__c = null 
 			    or TAM_PermiteConsultarTodosLeads__c = true)]){
 			
 			//Ya tienes los datos validos para facturación entonces metelos a la lista del tipo DatosProspecto
 			DatosProspecto objDatosProspectoPaso = new DatosProspecto();
			List<DatosVin> lDatosVin = new List<DatosVin>();
			
 			//Datos del Cliente 			
			objDatosProspectoPaso.IdProspectoSFDC = candidatoFacturar.Id;	//Id
            objDatosProspectoPaso.ClaveVendedor = candidatoFacturar.TAM_AsesorVentasFac__c != null ? candidatoFacturar.TAM_AsesorVentasFac__c : System.Label.TAM_AsesorVentasIntegraLeadMang;      //AsesorVentas 
			objDatosProspectoPaso.numeroCliente = candidatoFacturar.FWY_ID_de_cliente__c != null ? candidatoFacturar.FWY_ID_de_cliente__c : ''; 		//FWY_ID_de_cliente__c
			objDatosProspectoPaso.nombre = candidatoFacturar.TAM_NombreRL__c != null ? candidatoFacturar.TAM_NombreRL__c : '';			//TAM_NombreRL__c
			objDatosProspectoPaso.apellidoPaterno = candidatoFacturar.TAM_ApellidoPaternoRL__c != null ? candidatoFacturar.TAM_ApellidoPaternoRL__c : '';	//TAM_ApellidoPaternoRL__c
			objDatosProspectoPaso.apellidoMaterno = candidatoFacturar.TAM_ApellidoMaternoRL__c != null ? candidatoFacturar.TAM_ApellidoMaternoRL__c : '';	//TAM_ApellidoMaternoRL__c
            //String sTipoPersona = candidatoFacturar.FWY_Tipo_de_persona__c != null ? candidatoFacturar.FWY_Tipo_de_persona__c : '';
			String sTipoPersona = candidatoFacturar.TAM_TipoPersonaFact__c != null ? candidatoFacturar.TAM_TipoPersonaFact__c : '';
	 		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar sTipoPersona: ' + sTipoPersona);

			objDatosProspectoPaso.tipoPersona = (sTipoPersona == 'Person física' || sTipoPersona == 'Persona Física') ? 'PERSONA FISICA' : (sTipoPersona == 'Persona moral' ? 'PERSONA MORAL' : 'Fisica con actividad empresarial');		//FWY_Tipo_de_persona__c
			objDatosProspectoPaso.rfc = candidatoFacturar.TAM_RFCRL__c != null ? candidatoFacturar.TAM_RFCRL__c : ''; 						//TAM_RFCRL__c
			objDatosProspectoPaso.email = candidatoFacturar.TAM_CorreoElectronicoRL__c != null ? candidatoFacturar.TAM_CorreoElectronicoRL__c : '';			//TAM_CorreoElectronicoRL__c
			objDatosProspectoPaso.Telefono = candidatoFacturar.TAM_TelefonoRL__c != null ? candidatoFacturar.TAM_TelefonoRL__c : ''; 				//TAM_TelefonoRL__c
			objDatosProspectoPaso.celular = candidatoFacturar.TAM_TelefonoCelularRL__c != null ? candidatoFacturar.TAM_TelefonoCelularRL__c : ''; 		//TAM_TelefonoCelularRL   Teléfono móvil
            objDatosProspectoPaso.curp = candidatoFacturar.TAM_CurpFact__c != null ? candidatoFacturar.TAM_CurpFact__c : '';                //TAM_CurpFact__c
            if (candidatoFacturar.TAM_FechaNacimientoFact__c == null)
                objDatosProspectoPaso.fechaNacimiento = '';       //TAM_FechaNacimientoFact__c   Teléfono móvil                
            //Toma los datos de la fecha
            if (candidatoFacturar.TAM_FechaNacimientoFact__c != null){
                String sAnio = String.valueOf(candidatoFacturar.TAM_FechaNacimientoFact__c.year());
                String sMes = String.valueOf(candidatoFacturar.TAM_FechaNacimientoFact__c.month());
                String sDia = String.valueOf(candidatoFacturar.TAM_FechaNacimientoFact__c.day());
                String sFechaPaso = ( Integer.valueOf(sDia) < 10 ? '0' + sDia : sDia) + '/' + ( Integer.valueOf(sMes) < 10 ? '0' + sMes : sMes) + '/' + sAnio;
                objDatosProspectoPaso.fechaNacimiento = sFechaPaso;       //TAM_FechaNacimientoFact__c   Teléfono móvil
            }//Fin si candidatoFacturar.TAM_FechaNacimientoFact__c != null

 			//Dirección
			objDatosProspectoPaso.direccion = candidatoFacturar.TAM_Calle__c != null ? candidatoFacturar.TAM_Calle__c : ''; 					//TAM_Calle__c
			objDatosProspectoPaso.noExt = candidatoFacturar.TAM_NoExt__c != null ? candidatoFacturar.TAM_NoExt__c : '';	//TAM_Calle__c
			objDatosProspectoPaso.noInt = candidatoFacturar.TAM_NoInt__c != null ? candidatoFacturar.TAM_NoInt__c : '';	//TAM_Calle__c
			objDatosProspectoPaso.cp = candidatoFacturar.TAM_CodigoPostal__c != null ? candidatoFacturar.TAM_CodigoPostal__c : ''; 					//TAM_CodigoPostal__c
			objDatosProspectoPaso.colonia = candidatoFacturar.TAM_Colonia__c != null ? candidatoFacturar.TAM_Colonia__c : ''; 					//TAM_Colonia__c
			objDatosProspectoPaso.delegacionMunicipio = candidatoFacturar.TAM_DelegacionMunicipio__c != null ? candidatoFacturar.TAM_DelegacionMunicipio__c : '';	//TAM_DelegacionMunicipio__c NUEVO CAMPO
			objDatosProspectoPaso.localidad = candidatoFacturar.TAM_Ciudad__c != null ? candidatoFacturar.TAM_Ciudad__c : ''; 					//TAM_Ciudad__c
			objDatosProspectoPaso.estado = candidatoFacturar.TAM_Estado__c != null ? candidatoFacturar.TAM_Estado__c : ''; 					//TAM_Estado__c
			objDatosProspectoPaso.pais = candidatoFacturar.TAM_Pais__c != null ? candidatoFacturar.TAM_Pais__c : ''; 						//TAM_Pais__c

			//Daos del contacto adicional
			objDatosProspectoPaso.nombreContacto = candidatoFacturar.TAM_NombreContFact__c != null ? candidatoFacturar.TAM_NombreContFact__c : '';		//FistName
			objDatosProspectoPaso.apellidoPaternoContacto = candidatoFacturar.TAM_ApellidoPaternoContFact__c != null ? candidatoFacturar.TAM_ApellidoPaternoContFact__c : ''; //LastName
			objDatosProspectoPaso.apellidoMaternoContacto = candidatoFacturar.TAM_ApellidoMaternoContFact__c != null ? candidatoFacturar.TAM_ApellidoMaternoContFact__c : ''; //FWY_ApellidoMaterno__c
			objDatosProspectoPaso.rfcContacto = candidatoFacturar.TAM_RFCContFact__c != null ? candidatoFacturar.TAM_RFCContFact__c : '';		//TAM_RFC__c
			objDatosProspectoPaso.TelefonoContacto = candidatoFacturar.TAM_TelefonoContFact__c != null ? candidatoFacturar.TAM_TelefonoContFact__c : '';	//Phone
			objDatosProspectoPaso.celularContacto = candidatoFacturar.TAM_TelefonoCelularContFact__c != null ? candidatoFacturar.TAM_TelefonoContFact__c : '';	//MobilePhone
			objDatosProspectoPaso.emailContacto = candidatoFacturar.TAM_CorreoElectronicoContFact__c != null ? candidatoFacturar.TAM_CorreoElectronicoContFact__c : '';		//Email

			objDatosProspectoPaso.direccionContacto = candidatoFacturar.TAM_CalleContFact__c != null ? candidatoFacturar.TAM_CalleContFact__c : '';	//TAM_Calle__c
			objDatosProspectoPaso.noExtContacto = candidatoFacturar.TAM_NoExtContacto__c != null ? candidatoFacturar.TAM_NoExtContacto__c : '';	//TAM_Calle__c
			objDatosProspectoPaso.noIntContacto = candidatoFacturar.TAM_NoIntContacto__c != null ? candidatoFacturar.TAM_NoIntContacto__c : '';	//TAM_Calle__c
			objDatosProspectoPaso.cpContacto = candidatoFacturar.TAM_CodigoPostalContFact__c != null ? candidatoFacturar.TAM_CodigoPostalContFact__c : '';			//TAM_CodigoPostal__c
			objDatosProspectoPaso.localidadContacto = candidatoFacturar.TAM_CiudadContFact__c != null ? candidatoFacturar.TAM_CiudadContFact__c : '';	//TAM_Ciudad__c
			objDatosProspectoPaso.coloniaContacto = candidatoFacturar.TAM_ColoniaContFact__c != null ? candidatoFacturar.TAM_ColoniaContFact__c : '';	//TAM_Colonia__c
			objDatosProspectoPaso.delegacionMunicipioContacto = candidatoFacturar.TAM_DelegacionMunicipioContFact__c != null ? candidatoFacturar.TAM_DelegacionMunicipioContFact__c : ''; //
			objDatosProspectoPaso.estadoContacto = candidatoFacturar.TAM_EstadoContFact__c != null ? candidatoFacturar.TAM_EstadoContFact__c : '';		//TAM_Estado__c
			objDatosProspectoPaso.paisContacto = candidatoFacturar.TAM_PaisContFact__c != null ? candidatoFacturar.TAM_PaisContFact__c : '';		//TAM_Pais__c

            //Daos del contacto servicio
            objDatosProspectoPaso.nombreContactoServ = candidatoFacturar.TAM_NombreContactoServicio__c != null ? candidatoFacturar.TAM_NombreContactoServicio__c : '';      //FistName
            objDatosProspectoPaso.apellidoPaternoContactoServ = candidatoFacturar.TAM_ApellidoPaternoContactoServ__c != null ? candidatoFacturar.TAM_ApellidoPaternoContactoServ__c : ''; //LastName
            objDatosProspectoPaso.apellidoMaternoContactoServ = candidatoFacturar.TAM_ApellidoMaternoContactoServ__c != null ? candidatoFacturar.TAM_ApellidoMaternoContactoServ__c : ''; //FWY_ApellidoMaterno__c
            objDatosProspectoPaso.TelefonoContactoServ = candidatoFacturar.TAM_TelefonoContactoServicio__c != null ? candidatoFacturar.TAM_TelefonoContactoServicio__c : '';    //Phone
            objDatosProspectoPaso.celularContactoServ = candidatoFacturar.TAM_TelefonoCelularContactoServicio__c != null ? candidatoFacturar.TAM_TelefonoCelularContactoServicio__c : '';  //MobilePhone
            objDatosProspectoPaso.emailContactoServ = candidatoFacturar.TAM_CorreoElectronicoContactoServicio__c != null ? candidatoFacturar.TAM_CorreoElectronicoContactoServicio__c : '';     //Email

            //Daos para Quiter
            objDatosProspectoPaso.TipoOferta = candidatoFacturar.TAM_TipoOfertaFac__c != null ? candidatoFacturar.TAM_TipoOfertaFac__c : '';            //TipoOferta
            objDatosProspectoPaso.AsesorVentas = candidatoFacturar.TAM_AsesorVentasFac__c != null ? candidatoFacturar.TAM_AsesorVentasFac__c : System.Label.TAM_AsesorVentasIntegraLeadMang;      //AsesorVentas
            objDatosProspectoPaso.Origen = candidatoFacturar.TAM_OrigenFac__c != null ? candidatoFacturar.TAM_OrigenFac__c : '';                    //Origen
            objDatosProspectoPaso.Agencia = candidatoFacturar.TAM_AgenciaFac__c != null ? candidatoFacturar.TAM_AgenciaFac__c : '';             //Agencia
            objDatosProspectoPaso.EstadoPedidoInicial = candidatoFacturar.TAM_EstadoPedidoInicialFac__c != null ? candidatoFacturar.TAM_EstadoPedidoInicialFac__c : ''; //EstadoPedidoInicial
            objDatosProspectoPaso.TipoVenta = candidatoFacturar.TAM_TipoVentaFac__c != null ? candidatoFacturar.TAM_TipoVentaFac__c : '';               //TipoVenta
            objDatosProspectoPaso.TipoVentaDestino = candidatoFacturar.TAM_TipoVentaDestinoFac__c != null ? candidatoFacturar.TAM_TipoVentaDestinoFac__c : '';  //TipoVentaDestino

			//Ve si tiene Vines asociados a candidatoFacturar y agregalos al objeto de objDatosProspectoPaso
			if (!candidatoFacturar.TAM_LeadInventProspecto__r.isEmpty()){
				for ( TAM_LeadInventarios__c objLeadInventarios : candidatoFacturar.TAM_LeadInventProspecto__r){
					DatosVin objDatosVinPaso = new DatosVin();
					//objDatosVinPaso.Vin = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Name != null ? objLeadInventarios.TAM_InventarioVehiculosFyG__r.Name : '';	//TAM_InventarioFyG__r.Name
					objDatosVinPaso.Vin = objLeadInventarios.Name != null ? objLeadInventarios.Name : '';	//TAM_InventarioFyG__r.Name
					objDatosVinPaso.modelo = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Number__c != null ? objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Number__c : '';	//TAM_InventarioFyG__r.Model_Number__c
					objDatosVinPaso.descripcion = objLeadInventarios.TAM_Descripcion__c != null ? objLeadInventarios.TAM_Descripcion__c : '';	//Description
					objDatosVinPaso.precioVenta = objLeadInventarios.TAM_PrecioVenta__c != null ? String.valueOf(objLeadInventarios.TAM_PrecioVenta__c) : '0.00';	//TAM_PrecioVenta__c	NUEVO CAMPO
					objDatosVinPaso.colorExterior = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c != null ? objLeadInventarios.TAM_InventarioVehiculosFyG__r.Exterior_Color_Description__c : '';	//TAM_InventarioFyG__r.Exterior_Color_Description__c
					objDatosVinPaso.colorInterior = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c != null ? objLeadInventarios.TAM_InventarioVehiculosFyG__r.Interior_Color_Description__c : '';	//TAM_InventarioFyG__r.Interior_Color_Description__c
					objDatosVinPaso.Catalogo = objLeadInventarios.TAM_Catalogo__c != null ? objLeadInventarios.TAM_Catalogo__c : '';			//TAM_Catalogo__c		NUEVO CAMPO
					objDatosVinPaso.AnioModelo = objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Year__c != null ? objLeadInventarios.TAM_InventarioVehiculosFyG__r.Model_Year__c : '';		//TAM_InventarioFyG__r.Model_Year__c
					objDatosVinPaso.Carline = objLeadInventarios.TAM_Carline__c != null ? objLeadInventarios.TAM_Carline__c : '';			//TAM_Carline__c
					objDatosVinPaso.MonedaVenta = objLeadInventarios.TAM_MonedaVenta__c != null ? objLeadInventarios.TAM_MonedaVenta__c : '';	//TAM_MonedaVenta__c	NUEVO CAMPO
					objDatosVinPaso.TipoCambioPactado = objLeadInventarios.TAM_TipoCambioPactado__c != null ? String.valueOf(objLeadInventarios.TAM_TipoCambioPactado__c) : '0.00';	//TAM_TipoCambioPactado__c	NUEVO CAMPO
                    objDatosVinPaso.NoInventario = objLeadInventarios.TAM_NoInventario__c != null ? String.valueOf(objLeadInventarios.TAM_NoInventario__c) : ''; //TAM_TipoCambioPactado__c  NUEVO CAMPO
					//Agrega el objeto a la lista de lDatosVin
					lDatosVin.add(objDatosVinPaso);					
				}//Fin del for para candidatoFacturar.TAM_LeadInventProspecto__r				
			}//Fin si candidatoFacturar.TAM_LeadInventProspecto__r.isEmpty()
			
			//Agrega la lista de vines al objDatosProspectoPaso
			objDatosProspectoPaso.lstVines = lDatosVin;
			
 			//Agregaro a la lista de lDatosProspecto
 			lDatosProspecto.add(objDatosProspectoPaso);
 		}
 		
 		sListaClientesFacturar = '{"listaClientesSFDC":';
 		
 		//Ahora si serializa la lista de lDatosProspecto
 		sListaClientesFacturar += JSON.serialize(lDatosProspecto);
 		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar sListaClientesFacturar: ' + sListaClientesFacturar);
 		
 		//Pon unos datos de prueba nada mas para que responda
 		/*sListaClientesFacturar = 	
'{' +
	'"listaClientesSFDC": [{' +
			'"IdProspectoSFDC": "00Qe000000BcgiYEAR",' + 
			'"numeroCliente": "",' + 
			'"nombre": "JORGE",' +
			'"apellidoPaterno": "LOZANO",' +
			'"apellidoMaterno": "LOZANO",' +
			'"tipoPersona": "PERSONA FISICA",' +
			'"rfc": "LOLJ780101cg4",' +
			'"Telefono": "5534567898",' +
			'"celular": "5523456543",' +
			'"direccion": "DE LA ROSA 51",' +
			'"colonia": "LAS ROSAS",' +
			'"cp": "34567",' +
			'"localidad": "CDMX",' +
			'"delegacionMunicipio": "IZTACALCO",' +
			'"estado": "CDMX",' +
			'"pais": "MEXICO",' +
			'"email": "jorgell@hotmail.com",' +
			'"nombreContacto": "",' +
			'"apellidoPaternoContacto": "",' +
			'"apellidoMaternoContacto": "",' +
			'"rfcContacto": "",' +
			'"TelefonoContacto": "",' +
			'"celularContacto": "",' +
			'"direccionContacto": "",' +
			'"cpContacto": "",' +
			'"localidadContacto": "",' +
			'"coloniaContacto": "",' +
			'"delegacionMunicipioContacto": "",' +
			'"estadoContacto": "",' +
			'"paisContacto": "",' +
			'"emailContacto": "",' +
			'"lstVines": [{' +
				'"Vin": "JTDBARDEXMJ000166",' +
				'"modelo": "1780",' + 
				'"descripción": "Corolla HV",' +
				'"precioVenta": "200000.00",' +
				'"colorExterior": "Gris Metpalico",' +
				'"colorInterior": "Negro",' +
				'"Catalogo": "",' +
				'"AnioModelo": "2021",' +
				'"Carline": "",' +
				'"MonedaVenta": "MXP",' +
				'"TipoCambioPactado": "20.50"' +
			'},' +
			'{' +
				'"Vin": "3MYDLBYV1LY711075",' +
				'"modelo": "1062",' + 
				'"descripción": "YARIS R",' +
				'"precioVenta": "210000.00",' +
				'"colorExterior": "Gris Metpalico",' +
				'"colorInterior": "Negro",' +
				'"Catalogo": "",' +
				'"AnioModelo": "2020",' +
				'"Carline": "",' +
				'"MonedaVenta": "MXP",' +
				'"TipoCambioPactado": "22.50"' +
			'}]' +
		'},' +
		'{' +
			'"IdProspectoSFDC": "00Qe000000BdFgOEAV",' + 
			'"numeroCliente": "",' + 
			'"nombre": "SOFIA",' +
			'"apellidoPaterno": "LOPEZ",' +
			'"apellidoMaterno": "LOPEZ",' +
			'"tipoPersona": "PERSONA FISICA",' +
			'"rfc": "LOLS780101GH5",' +
			'"Telefono": "5534567898",' +
			'"celular": "5523456543",' +
			'"direccion": "CARDENAR 100",' +
			'"colonia": "GPE DEL MOIRAL",' +
			'"cp": "45678",' +
			'"localidad": "CDMX",' +
			'"delegacionMunicipio": "IZTAPALAPA",' +
			'"estado": "CDMX",' +
			'"pais": "MEXICO",' +
			'"email": "sofiall@hotmail.com",' +
			'"nombreContacto": "",' +
			'"apellidoPaternoContacto": "",' +
			'"apellidoMaternoContacto": "",' +
			'"rfcContacto": "",' +
			'"TelefonoContacto": "",' +
			'"celularContacto": "",' +
			'"direccionContacto": "",' +
			'"cpContacto": "",' +
			'"localidadContacto": "",' +
			'"coloniaContacto": "",' +
			'"delegacionMunicipioContacto": "",' +
			'"estadoContacto": "",' +
			'"paisContacto": "",' +
			'"emailContacto": "",' +
			'"lstVines": [{' +
				'"Vin": "JTDBARDE9MJ001924",' +
				'"modelo": "1780",' + 
				'"descripción": "Corolla HV",' +
				'"precioVenta": "230000.00",' +
				'"colorExterior": "Blanco plata",' +
				'"colorInterior": "Negro",' +
				'"Catalogo": "",' +
				'"AnioModelo": "2021",' +
				'"Carline": "",' +
				'"MonedaVenta": "MXP",' +
				'"TipoCambioPactado": "20.50"' +
			'},' +
			'{' +
				'"Vin": "JTDBARDE9MJ001213",' +
				'"modelo": "1780",' + 
				'"descripción": "YARIS R",' +
				'"precioVenta": "220000.00",' +
				'"colorExterior": "Azul Marino",' +
				'"colorInterior": "Negro",' +
				'"Catalogo": "",' +
				'"AnioModelo": "2021",' +
				'"Carline": "",' +
				'"MonedaVenta": "MXP",' +
				'"TipoCambioPactado": "22.50"' +
			'}]' +
		'}' +
	']' +
'}'; 		
*/
 
 		sListaClientesFacturar += '}'; 		
 		
  		System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar sListaClientesFacturar: ' + sListaClientesFacturar);		
 		return sListaClientesFacturar;
 	}
    
        //ya estan listos para facturarrse
    public static String updProspCons(String sResUpdInves){
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.updProspCons sResUpdInves: ' + sResUpdInves);        
        
        String sOk = '';
        Map<String, Lead> mapLeadUpd = new Map<String, Lead>();
        
        //Desrailiza los datos de sResUpdInves con la clase SFDC_ConsultaCtesParaFacturar_ctrl_rst
        SFDC_ConsultaCtesParaFacturar_ctrl_rst objConsultaCtes = (SFDC_ConsultaCtesParaFacturar_ctrl_rst) JSON.deserialize(sResUpdInves, SFDC_ConsultaCtesParaFacturar_ctrl_rst.class);
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.updProspCons objConsultaCtes: ' + objConsultaCtes);        

        //Recorre la lista de reg del tipo listaClientesSFDC
        if (!objConsultaCtes.listaClientesSFDC.isEmpty()){
            for (DatosProspecto objDatosProspecto : objConsultaCtes.listaClientesSFDC){
                mapLeadUpd.put(objDatosProspecto.IdProspectoSFDC, new Lead(
                        id = objDatosProspecto.IdProspectoSFDC,
                        TAM_EstatusActualizacionDealer__c = 'Consultado'
                    )
                );
                System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.updProspCons objDatosProspecto: ' + objDatosProspecto);
            }//Fin del for para objDatosProspecto
        }//Fin si !objConsultaCtes.listaClientesSFDC.isEmpty()
        System.debug('EN SFDC_ConsultaCtesParaFacturar_ctrl_rst.updProspCons mapLeadUpd: ' + mapLeadUpd);
        
        //Ve si tiene algo el mapa de mapLeadUpd y actualiza
        if (!mapLeadUpd.isEmpty())
            if (!Test.isRunningTest())
                update mapLeadUpd.values();
                
        //Regresa el resultado de la actuelización
        return sOk;
    }
    
    
    
    
}