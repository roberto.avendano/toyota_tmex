/** 
    Descripción General: Pantalla de provición de incetivos a distribuidores.
    ________________________________________________________________
    	Autor                   Fecha               	Descripción
    ________________________________________________________________
    	Cecilia Cruz            18/Junio/2020         	Versión Inicial
    ________________________________________________________________
**/
public without sharing class TAM_ProvisionReversasControllerClass {

	/***	ANALISIS DE REVERSAS	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getReversa(String recordId){
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  TAM_VIN__r.Name,
                                                                      		  TAM_VIN__r.Id,
                                                                              TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                              TAM_CodigoDealer__c,
                                                                              TAM_NombreDealer__c,
                                                                              TAM_CodigoVenta__c,
                                                                              TAM_Serie__c,
                                                                              TAM_Modelo__c,
                                                                              TAM_AnioModelo__c,
                                                                              TAM_FechaEnvio__c,
                                                                              TAM_TipoMovimiento__c,
                                                                              TAM_FirstName__c,
                                                                              TAM_LastName__c,
                                                                      		  TAM_Version__c,
                                                                      		  TAM_FechaVenta__c,
                                                                              TAM_ProvicionarEfectivo__c,
                                                                              TAM_TransmitirDealer__c,
                                                                      		  TAM_IdExterno__c,
                                                                      		  TAM_Tipo__c,
                                                                              TAM_Fleet__c,
                                                                              TAM_DetalleEstadoCuenta__r.Id,
                                                                              TAM_DetalleEstadoCuenta__r.Name,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Estatus_Finanzas__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Monto_sin_IVA__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Fecha_Pago__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_CodigoDealer__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_FolioFactura__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Movimiento__c,
                                                                              TAM_DetalleEstadoCuenta__r.RecordType.Name,
                                                                              TAM_ProvisionarProdFinanciero__c,
                                                                              TAM_IncentivoTMEX_Efectivo__c,
                                                                              RecordType.Name,
                                                                      		  TAM_PoliticaIncentivos__r.TAM_FinVigencia__c,
                                                                              Bonos__c,
                                                                              Bonos__r.Id,
                                                                              Bonos__r.Name,
                                                                              TAM_BonoTMEX_Efectivo__c,
                                                                              TAM_ProductoFinanciero__c,
                                                                              TAM_BonoTMEX_PF__c,
                                                                              TAM_TipoRegEdoCuenta__c
                                                                              
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Con Reversa'
                                                                      AND	  TAM_ReversaPendienteProvision__c = false
                                                                      ORDER BY RecordType.Name DESC, TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        
        return lstDetalleProvision;
    }

     /*GUARDADO DE REGISTROS*/
     @AuraEnabled
     public static List<TAM_DetalleProvisionIncentivo__c> setRecordsReversas(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
         
         List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
         for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
             objDetalle.TAM_Guardado__c = true;
             lstDetalleUpsert.add(objDetalle);
         }
          
         
         try {
             Database.update(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
         } catch (DmlException e) {
             System.debug(e.getMessage());
         }        
         return lstDetalleUpsert;
     } 
 
     /*CERRAR PROVISION*/
     @AuraEnabled
     public static Boolean cerrarProvision(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        TAM_CerrarProvisionQueueable updateJob_Reversa = new TAM_CerrarProvisionQueueable(lstDetalleProvision, recordId, 'Reversa');
        ID jobID_reversa = System.enqueueJob(updateJob_Reversa);
         System.debug('ID REVERSA JOB:*** ' + jobID_reversa);
        if(jobID_reversa != null){
            return true;
        } else{
            return false;
        }
     }
    
    /***	PENDIENTES DE PROVISION	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getPendientes(String recordId){
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  TAM_VIN__r.Name,
                                                                      		  TAM_VIN__r.Id,
                                                                              TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                              TAM_CodigoDealer__c,
                                                                              TAM_NombreDealer__c,
                                                                              TAM_CodigoVenta__c,
                                                                              TAM_Serie__c,
                                                                              TAM_Modelo__c,
                                                                              TAM_AnioModelo__c,
                                                                              TAM_FechaEnvio__c,
                                                                              TAM_TipoMovimiento__c,
                                                                              TAM_FirstName__c,
                                                                              TAM_LastName__c,
                                                                      		  TAM_Version__c,
                                                                      		  TAM_FechaVenta__c,
                                                                              TAM_ProvicionarEfectivo__c,
                                                                              TAM_TransmitirDealer__c,
                                                                      		  TAM_IdExterno__c,
                                                                      		  TAM_Tipo__c,
                                                                              TAM_Fleet__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Estatus_Finanzas__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Monto_sin_IVA__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_Fecha_Pago__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_NombreDealer__c,
                                                                              TAM_DetalleEstadoCuenta__r.TAM_FolioFactura__c,
                                                                              TAM_DetalleEstadoCuenta__r.RecordType.Name,
                                                                              TAM_ProvisionarProdFinanciero__c,
                                                                              TAM_IncentivoTMEX_Efectivo__c,
                                                                              RecordType.Name,

                                                                              Bonos__c,
                                                                              Bonos__r.Id,
                                                                              Bonos__r.Name,
                                                                              TAM_BonoTMEX_Efectivo__c,
                                                                              TAM_ProductoFinanciero__c,
                                                                              TAM_BonoTMEX_PF__c
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Con Reversa'
                                                                      AND	  TAM_ReversaPendienteProvision__c = true
                                                                      ORDER BY RecordType.Name DESC, TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        
        System.debug('DETALLE PENDIENTE PROVISION: ' +lstDetalleProvision);
        return lstDetalleProvision;
    }

     /*GUARDADO DE REGISTROS*/
     @AuraEnabled
     public static List<TAM_DetalleProvisionIncentivo__c> setRecordsPendientes(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
         
         List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
         for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
             objDetalle.TAM_Guardado__c = true;
             lstDetalleUpsert.add(objDetalle);
         }
                 
         try {
             Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
         } catch (DmlException e) {
             System.debug(e.getMessage());
         }        
         return lstDetalleUpsert;
     } 
 
     /*CERRAR PROVISION*/
     @AuraEnabled
     public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvisionPendientes(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
         Date dateToday = Date.today();
         List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
         for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
             
             objDetalle.TAM_FechaCierre__c = dateToday;
             objDetalle.TAM_ProvisionCerrada__c = true;
             lstDetalleUpsert.add(objDetalle);
         }
 
         try {
             /**ACTUALIZAR DETALLE PROVISION */
             Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
             
         } catch (DmlException e) {
             System.debug(e.getMessage());
         }
         return lstDetalleUpsert;
     }
}