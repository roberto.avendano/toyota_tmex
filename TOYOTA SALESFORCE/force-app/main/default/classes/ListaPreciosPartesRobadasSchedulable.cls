global class ListaPreciosPartesRobadasSchedulable implements Schedulable {
	global void execute(SchedulableContext sc) {
		ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('PRCambioPreciosDealer');
		Date ahora = Date.today();
		if(Integer.valueOf(pc.Valor__c)==ahora.day()){
			ListaPreciosPartesRobadasBatch b = new ListaPreciosPartesRobadasBatch();
			database.executebatch(b);
		}else{
			System.debug('Solo se ejecuta los días '+pc.Valor__c);
		}
	}
}