/**
*  Descripción General: Clase de prueba para Nuevas Solicitudes de venta corporativa
*  ________________________________________________________________
*      Autor                   Fecha               Descripción
*  ________________________________________________________________
*      Cecilia Cruz            15/Octubre/2020      Versión Inicial
*  ________________________________________________________________
*/
@isTest(SeeAllData=false)
public class TAM_SolicitudCorpTestClass {

    @isTest
    public static void testAccount(){
        //Crear Programa
        String strRecordTypeId_Programa = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
        Rangos__c objRango = new Rangos__c();
        objRango.Name = 'UBER';
        objRango.Activo__c = true;
        objRango.RecordTypeId = strRecordTypeId_Programa;
        insert objRango;
        
        //Crear cuenta
        String strRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();
        Account objAccount = new Account();
        objAccount.Name = 'Test';
        objAccount.RecordTypeId = strRecordTypeId;
        objAccount.TAM_ProgramaRango__c = objRango.Id;
        insert objAccount;
        
        Test.startTest();
        Account objPruebaAccount = TAM_SolicitudCorpController.getDatosCuenta(objAccount.Id);
        String strRecordTypeIdSolicitud = TAM_SolicitudCorpController.getRecordId(objAccount.Id);
        Test.stopTest();
        System.assertEquals(objRango.Id, objPruebaAccount.TAM_ProgramaRango__c);
    }
}