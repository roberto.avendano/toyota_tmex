@isTest
private class OrderTriggerHandlerTest {
	@testSetup static void setup() {

        Id standardPricebookId = Test.getStandardPricebookId();
        ParametrosConfiguracionToyota__c pc4 = new  ParametrosConfiguracionToyota__c(
            Name='PRNumDocumentos',
            Valor__c='1',
            Consecutivo__c = 4	
        );
        insert pc4;


       	//Creacion de Account para el pedido
       	Account acct = new Account(
        	    Name = 'Toyota Aeropuerto Test2'
            	);
            insert acct;
     
        String idExterno='A1B2C34567891g';
        
        //Creacion del vehiculo para el numero de serie en pedido
    	Vehiculo__c vhl = new Vehiculo__c(
			Name = idExterno,
			Id_Externo__c = idExterno 
		);
        insert vhl;
		
		Product2 producto = new Product2(
			IdExternoProducto__c=idExterno,
			Name='Producto Prueba',
			Description= 'Test product2',
			CantidadMaxima__c=5,
			PartesRobadas__c= false
			);
		insert producto;		


    	Pricebook2 pb2Custom = new Pricebook2(
				IdExternoListaPrecios__c=idExterno,
				Name='Test',
				IsActive=true
				);
			insert pb2Custom;

		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id=standardPricebookId,
			UnitPrice=0.0,
			Product2Id=producto.Id,			
			IsActive=true,
			IdExterno__c='Test1'
			);
		insert pbeStandard;

		PricebookEntry pbeCustom = new PricebookEntry(
			Pricebook2Id=pb2Custom.Id,
			UnitPrice=100,
			Product2Id=producto.Id,			
			IsActive=true,
			IdExterno__c='Test2'
			);
		insert pbeCustom;


    		//Creacion del pedido
    	Order pedido = new Order(
			Status='En proceso',
			Estatus__c='Solicitud capturada',
            EffectiveDate = Date.today(),
            FechaAutorizacion__c = Date.today(),
            Mes_Ano__c =  Date.today(),
			AccountId = acct.Id,
			Vehiculo__c=vhl.Id,
			Pricebook2Id=pb2Custom.Id
			/*
			CartaRobo__c=true,
	        CartaMembretadaRazonSocial__c=true,
	        CopiaFacturaOriginal__c=true,
	        FormatoSolicitudPartesRobadas__c=true,
	        FotoParte__c=true,
	        FotoPlacaVIN__c=true,
	        FotoVehiculo__c=true,
	        IFELicencia__c=true,
	        Otros__c=true,
	        TarjetaCirculacion__c=true*/
    	);
		insert pedido;
        
		OrderItem ordItem = new OrderItem(
                OrderId = pedido.id, 
				PricebookEntryId=pbeCustom.Id,
				ProductoFinal__c = producto.id,
            	UnitPrice=pbeCustom.UnitPrice,
				Quantity=3
			); 
		
        insert ordItem;
    }
    
    
	  public static List<sObject> getStatusOrder(){
	  	Id standardPricebookId = Test.getStandardPricebookId();
        ParametrosConfiguracionToyota__c pc4 = new  ParametrosConfiguracionToyota__c(
            Name='PRNumDocumentos',
            Valor__c='1',
            Consecutivo__c = 4
        );
        insert pc4;


       	//Creacion de Account para el pedido
       	Account acct = new Account(
        	    Name = 'Toyota Aeropuerto Test'
            	);
            insert acct;
     
        String idExterno='A1B2C345678910';
        
        //Creacion del vehiculo para el numero de serie en pedido
    	Vehiculo__c vhl = new Vehiculo__c(
			Name = idExterno,
			Id_Externo__c = idExterno 
		);
        insert vhl;
		
		Product2 producto = new Product2(
			IdExternoProducto__c=idExterno,
			Name='Producto Prueba',
			Description= 'Test product2',
			CantidadMaxima__c=5,
			PartesRobadas__c= false
			);
		insert producto;		


    	Pricebook2 pb2Custom = new Pricebook2(
				IdExternoListaPrecios__c=idExterno,
				Name='Test',
				IsActive=true
				);
			insert pb2Custom;

		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id=standardPricebookId,
			UnitPrice=0.0,
			Product2Id=producto.Id,			
			IsActive=true,
			IdExterno__c='Test1'
			);
		insert pbeStandard;

		PricebookEntry pbeCustom = new PricebookEntry(
			Pricebook2Id=pb2Custom.Id,
			UnitPrice=100,
			Product2Id=producto.Id,			
			IsActive=true,
			IdExterno__c='Test2'
			);
		insert pbeCustom;


    		//Creacion del pedido
    	Order pedido = new Order(
			Status='En proceso',
			Estatus__c='Solicitud capturada',
			EffectiveDate = Date.today(),
            Mes_Ano__c = Date.today(),
			AccountId = acct.Id,
			Vehiculo__c=vhl.Id,
			Pricebook2Id=pb2Custom.Id
			/*
			CartaRobo__c=true,
	        CartaMembretadaRazonSocial__c=true,
	        CopiaFacturaOriginal__c=true,
	        FormatoSolicitudPartesRobadas__c=true,
	        FotoParte__c=true,
	        FotoPlacaVIN__c=true,
	        FotoVehiculo__c=true,
	        IFELicencia__c=true,
	        Otros__c=true,
	        TarjetaCirculacion__c=true*/
    	);
		
		OrderItem ordItem = new OrderItem(
				PricebookEntryId=pbeCustom.Id,
				UnitPrice=pbeCustom.UnitPrice,
				Quantity=3
			); 
		
		List<sObject> resp = new List<sObject>();
		resp.add(pedido);
		resp.add(ordItem);
		return resp;
	}
	
	/*
	@isTest static void OrderTriggerHandlerTest(){
		List<sObject> orders = getStatusOrder();
		Test.startTest();
			Order order = (Order)orders[0];
			OrderItem ordItem = (OrderItem)orders[1];

			try{
				order.Estatus__c='Autorizada';
				insert order;
				ordItem.OrderId=order.Id;
			    insert ordItem;
			}catch(Exception e){
				System.debug(e.getMessage() + ' ' + e.getLineNumber());
				//Se valida que se adjunten los archivos
			}

			try{
				order.Estatus__c='Solicitud capturada';
				insert order;
				ordItem.OrderId=order.Id;
			    insert ordItem;

			} catch(Exception e){
				System.debug(e.getMessage() + ' ' + e.getLineNumber());
			}

			//ordItem.OrderId=order.Id;
			//insert ordItem;

			try{
				order.Estatus__c='Solicitud nueva';
				update order;
			}catch(Exception e){
				//Se valida que se adjunten los archivos
			}
			List<Attachment> atts = new List<Attachment>();
			Blob bodyBlob = Blob.valueOf('Test Attachment Body');
			for(Integer i=0 ; i<10 ; i++){
				atts.add(new Attachment(Name='Test '+1,ParentId=order.Id, Body=bodyBlob));
			}
			insert atts;

			try{
				update order;
			}catch(Exception e){
				
			}			


			order.CartaRobo__c = true;
			order.CartaMembretadaRazonSocial__c = true;
			order.CopiaFacturaOriginal__c = true;
			order.FormatoSolicitudPartesRobadas__c = true;
			order.FotoParte__c = true;
			order.FotoPlacaVIN__c = true;
			order.FotoVehiculo__c = true;
			order.IFELicencia__c = true;
			order.Otros__c = true;
			order.TarjetaCirculacion__c = true;

			OrderTriggerHandler.documentosPorValidar(order);

		Test.stopTest();
		}*/

		@isTest static void OrderTriggerHandlerTest2(){
			Account accName = [select id from account where name = 'Toyota Aeropuerto Test2' limit 1];
            Vehiculo__c vinId = [Select id from Vehiculo__c WHERE Name = 'A1B2C34567891g' limit 1];
           
             
            Order ordUpd = [Select id,estatus__c from order where Vehiculo__c =: vinId.id limit 1];
            
            ordUpd.estatus__c = 'Autorizada';
            update ordUpd;
            
        }
}