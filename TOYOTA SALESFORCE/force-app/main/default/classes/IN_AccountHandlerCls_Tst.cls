/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase IN_AccountHandler_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    25-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IN_AccountHandlerCls_Tst {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	String VaRtSolCtePreauto = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Cliente Preautorizado').getRecordTypeId();
	String VaRtSolCteActivacion = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();

	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;

		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_EstatusCliente__c = 'Vigente',
			TAM_EnviarPreautorizacion__c = false,
			TAM_SolcitaActivacion__c = false
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			RecordTypeId = sRectorTypePasoPersonaFisica,
			TAM_EstatusCliente__c = 'Vigente',
			TAM_EnviarPreautorizacion__c = false,
			TAM_SolcitaActivacion__c = false
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  		
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void IN_AccountHandlerClsOK() {
		//Consulta los datos del cliente
		list<Account> lCuentas =  [Select Id, Name, RecordTypeId, RecordType.Name From Account LIMIT 3];
		List<Account> LcuetasUps = new List<Account>();
		for (Account cuenta : lCuentas){
			System.debug('EN IN_AccountHandlerClsOK RecordType.Name: ' + cuenta.RecordType.Name);	
			if (cuenta.RecordType.Name == 'Cliente - Persona Moral'){
				LcuetasUps.add(new Account(	
						id = cuenta.id, 
						TAM_EstatusCliente__c = 'Vencido',
						TAM_EnviarPreautorizacion__c = true
					)
				);
			}//Fin si cuenta.RecordType.Name == 'Cliente - Persona Moral'
		}//Fin del for para lCuentas
		
		System.debug('EN IN_AccountHandlerClsOK LcuetasUps0: ' + LcuetasUps.get(0));	
		update LcuetasUps.get(0);
		
		//Toma el regsitro de la lista LcuetasUps y cambia los vcalores
		LcuetasUps.get(0).TAM_EnviarPreautorizacion__c = false;
		LcuetasUps.get(0).TAM_SolcitaActivacion__c = true;
		System.debug('EN IN_AccountHandlerClsOK LcuetasUps1: ' + LcuetasUps.get(0));	
		update LcuetasUps.get(0);
		
		/*RecordType.Name: Distribuidor
		RecordType.Name: Cliente - Persona Moral
		RecordType.Name: Cliente - Persona Fisica
		*/
		
    }

}