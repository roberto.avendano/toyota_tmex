public with sharing class  EntradaListaTriggerHandler extends TriggerHandler{
    private List<EntradaListaPrecios__c> entradasList;
    private static final String FIELD_INTEGRITY_EXCEPTION = 'INVALID_FIELD_FOR_INSERT_UPDATE';
    
    public  EntradaListaTriggerHandler() {
        this.entradasList = Trigger.new;	
    }
    
    public override void afterInsert(){
        createEntries(entradasList);
    }
    
    private void createEntries(List<EntradaListaPrecios__c> entradas){
        Pricebook2 pbPartes = [SELECT Id, Name, IdExternoListaPrecios__c FROM Pricebook2 WHERE IdExternoListaPrecios__c='LPRPartesRobadas'];
        Pricebook2 pbStandard = [SELECT Id, Name, IdExternoListaPrecios__c FROM Pricebook2 WHERE IdExternoListaPrecios__c='StandardPriceBook'];
        
        Map<String, List<Decimal>> pbeDates = new Map<String, List<Decimal>>();		  
        
        List<PricebookEntry> prices = new List<PricebookEntry>();
        List<PricebookEntry> pricesUpdate = new List<PricebookEntry>();
        List<PricebookEntry> standardPrices = new List<PricebookEntry>();
        List<PricebookEntry> standardPricesUpdate = new List<PricebookEntry>();		
        List<Product2> productos = new List<Product2>();
        Map<Id, Product2> productosMap = new Map<Id, Product2>(); 
        List<Sustituto__c> Sustitutos = new List<Sustituto__c>();
        
        Schema.SObjectField idExternoPBE = PricebookEntry.Fields.IdExterno__c;
        
        
        for(EntradaListaPrecios__c elp: entradas){
            String lineaCSV = elp.Parte__c;
            if(lineaCSV != null && lineaCSV!=''){
                System.debug('EN createEntries lineaCSV: ' + lineaCSV);
                
                if(lineaCSV.startsWith('2')){
                    System.debug(lineaCSV);
                    System.debug(lineaCSV.length());
                    String parte = lineaCSV.substring(1, 16);
                    parte = parte.trim();
                    System.debug(parte);
                    System.debug(parte.length());
                    String descripcion = lineaCSV.substring(31, 55);
                    Decimal dealer = Integer.valueOf(lineaCSV.substring(55,68));
                    Decimal mayorista = Integer.valueOf(lineaCSV.substring(69,82));
                    Decimal sugerido = Integer.valueOf(lineaCSV.substring(84,96));
                    
                    if(!pbeDates.containsKey(parte)){
                        pbeDates.put(parte, new List<Decimal>{dealer, mayorista, sugerido});
                    }
                    
                    System.debug('EN createEntries parte2: ' + parte);
                    Product2 obProduct2Paso = new Product2(
                        IdExternoProducto__c = parte,
                        Name= parte,
                        Description = descripcion,                          
                        IsActive = true
                    );
                    System.debug('EN createEntries parte: ' + parte);                    
                    productos.add(obProduct2Paso);
                    
                }//Fin si lineaCSV.startsWith('2')
                
                if(lineaCSV.startsWith('6')){ 
                    System.debug('EN createEntries lineaCSV6: ' + lineaCSV);                    
                    String parteOriginal = lineaCSV.substring(1, 16);
                    parteOriginal = parteOriginal.trim();
                    String parteSustituto = lineaCSV.substring(16,71);                    
                    Sustituto__c sustitutoRecord = new Sustituto__c();
					sustitutoRecord.putSObject('Parte__r', new Product2(IdExternoProducto__c = parteOriginal));
					sustitutoRecord.putSObject('Sustituto__r', new Product2(IdExternoProducto__c = parteSustituto));
					sustitutoRecord.Name = parteOriginal;
                    Sustitutos.add(sustitutoRecord);
               }//Fin si lineaCSV.startsWith('6')
               
            }
        }      

        System.debug('EN createEntries productos: ' + productos);        
        if(productos.size() > 0){
            try{		
                
                //upsert productos IdExternoProducto__c;
                Database.UpsertResult[] result = Database.upsert(productos,Product2.IdExternoProducto__c, false);                
                //Ve si hubo errores
                for (Database.UpsertResult objDtbUpsRes : result){
                    if (!objDtbUpsRes.isSuccess())
                        System.debug('Error a la hora de actualizar el producto: ' + objDtbUpsRes.getErrors()[0].getMessage());
                }//Fin del for para lDtbUpsRes          
                
                //Mete todos los productos al mapa productosMap               
                productosMap.putAll(productos);
                
                //Recorre la lista de productos
                for(Product2 p: productos){						
                    standardPrices.add(new PricebookEntry(
                        UnitPrice = 0.0,
                        Pricebook2Id = pbStandard.Id,
                        Product2Id = p.Id,
                        IsActive = true,
                        UseStandardPrice = false,
                        IdExterno__c = p.IdExternoProducto__c+'-'+pbStandard.IdExternoListaPrecios__c
                    ));
                    
                    prices.add(
                        new PricebookEntry(
                            UnitPrice = (pbeDates.get(p.IdExternoProducto__c).get(2))/100,
                            Pricebook2Id = pbPartes.Id,
                            Product2Id = p.Id,								
                            PrecioDealer__c = (pbeDates.get(p.IdExternoProducto__c).get(0))/100,
                            PrecioMayorista__c = (pbeDates.get(p.IdExternoProducto__c).get(1))/100,
                            IsActive = true,
                            IdExterno__c = p.IdExternoProducto__c+'-'+pbPartes.IdExternoListaPrecios__c
                        )
                    );
                }
                
            } catch(Exception ex){
                System.debug('Error de ejecucion'+ex.getMessage()+'Linea de error'+ex.getLineNumber());
            }
        }
        
        
        if(standardPrices.size() > 0){
            Database.UpsertResult [] result = Database.upsert(standardPrices,idExternoPBE, false);
            for(Integer i=0; i< result.size(); i++){
                if(!result[i].isSuccess()){
                    PricebookEntry pbeFail = standardPrices[i];
                    
                    for(Database.Error err: result[i].getErrors()){
                        if(String.valueOf(err.getStatusCode())==FIELD_INTEGRITY_EXCEPTION){
                            standardPricesUpdate.add(new PricebookEntry(
                                UnitPrice = 0.0,
                                Pricebook2Id = pbStandard.Id,								
                                IsActive = true,
                                UseStandardPrice = false,
                                IdExterno__c = productosMap.get(pbeFail.Product2Id).IdExternoProducto__c+'-'+pbStandard.IdExternoListaPrecios__c
                            ));
                        }	
                    }
                }
            }
        }
        
        if(standardPricesUpdate.size() > 0){
            try{
                upsert standardPricesUpdate IdExterno__c;
                
            }catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
        
        if(prices.size() > 0){			
            Database.UpsertResult [] result = Database.upsert(prices,idExternoPBE, false);			
            for(Integer i=0; i< result.size(); i++){
                if(!result[i].isSuccess()){
                    PricebookEntry pbeFail = prices[i];
                    
                    for(Database.Error err: result[i].getErrors()){
                        if(String.valueOf(err.getStatusCode())==FIELD_INTEGRITY_EXCEPTION){								
                            pricesUpdate.add(
                                new PricebookEntry(
                                    UnitPrice = (pbeDates.get(productosMap.get(pbeFail.Product2Id).IdExternoProducto__c).get(2))/100,
                                    //Pricebook2Id = pbPartes.Id,
                                    PrecioDealer__c = (pbeDates.get(productosMap.get(pbeFail.Product2Id).IdExternoProducto__c).get(0))/100,
                                    PrecioMayorista__c = (pbeDates.get(productosMap.get(pbeFail.Product2Id).IdExternoProducto__c).get(1))/100,
                                    IsActive = true,
                                    IdExterno__c = productosMap.get(pbeFail.Product2Id).IdExternoProducto__c+'-'+pbPartes.IdExternoListaPrecios__c
                                )
                            );
                        }							
                    }
                }
            }
            
            if(pricesUpdate.size() > 0){
                try{
                    system.debug('objActualizar'+pricesUpdate);
                    upsert pricesUpdate IdExterno__c;
                } catch(Exception ex){
                    System.debug('Error en el proceso linea 162'+ex.getMessage()+'Linea de error'+ ex.getLineNumber());
                }
            }	
        }
        
        if(!sustitutos.isEmpty()){
           insert sustitutos;
           system.debug('Sustitutos'+ Sustitutos);
        }
        
    }
    
    /*
    public static void relacionarSustitutos(Map<String,String> mapSustitutos){
        Map<Product2,Product2> mapProducto = new Map<Product2,Product2>();
        List<Sustituto__c> listaSustitutos = new  List<Sustituto__c>();
        List<Product2> prodOriginal = new 	List<Product2>();
        List<Product2> prodSustituto = new  List<Product2>();
        for (String parteR : mapSustitutos.keySet()){
            if(mapSustitutos.get(parteR).trim() != null){
                prodOriginal = [Select name,id From Product2 WHERE name =: mapSustitutos.get(parteR).trim()];    
            }
            if(parteR != null){ 
                prodSustituto = [Select name,id From Product2 WHERE name =: parteR.trim()];   
            }

            if(prodSustituto.size() > 0 && prodOriginal.size() > 0){
            	mapProducto.put(prodSustituto[0],prodOriginal[0]);    
            }
            
        }
        
        if(!mapProducto.isEmpty()){
            for(Product2 productDet : mapProducto.keySet()){
                Sustituto__c sustitoRelated = new Sustituto__c();
                sustitoRelated.Name =  mapProducto.get(productDet).Name;
                sustitoRelated.Parte__c =  mapProducto.get(productDet).id;
                sustitoRelated.Sustituto__c = productDet.Id;
                listaSustitutos.add(sustitoRelated);    
            }
        }
        
        if(!listaSustitutos.isEmpty()){
            try{
                insert listaSustitutos;
            }
            catch(Exception e){
                system.debug('error de proceso'+e.getMessage()+'En la linea'+e.getLineNumber());
            }
        }   
    }*/
}