/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Refere more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ClienteVehiculoClass_tst {

   	static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();    	
	
	@TestSetup static void loadData(){
		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 1
		);
		insert cuenta;
		
   		//Crea el objeto del tipo Opportunity
		Opportunity OpportunidadPaso = new Opportunity( 
			Name = 'TestAccount',
			CloseDate = Date.today(),
			StageName = 'Closed Won',
			Amount = 0.00, //dPrecio,
			recordTypeId = VaRtOppRegVCrm,
			AccountId = cuenta.id,
			TAM_Vin__c = '3MYDLBYV0LY711312',
			TAM_Distribuidor__c = cuenta.ID
		);						 
		insert OpportunidadPaso;
		
		TAM_ClienteVehiculo__c objClienteVehiculo = new TAM_ClienteVehiculo__c(
			Name = 'TestAccount - 3MYDLBYV0LY711312',
			TAM_Cliente__c = cuenta.id
		);
		insert objClienteVehiculo;
		
	}

    static testMethod void TAM_ClienteVehiculoClassOK() {
     	
     	TAM_ClienteVehiculo__c objClienteVehiculo = [Select id, OwnerId From TAM_ClienteVehiculo__c LIMIT 1];
     	update objClienteVehiculo;
     	 
    }
}