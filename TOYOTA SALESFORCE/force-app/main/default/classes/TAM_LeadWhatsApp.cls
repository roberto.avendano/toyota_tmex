/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño  roberto.avendano@globant.com
Proyecto:           TOYOTA TAM

Infomación de cambios (versiones)
===============================================================================
No.    Fecha             		Autor                       Descripcion
1.0    15-Septiembre-2020     	Roberto Carlos Avendaño     Creación
	   22-Septiembre-2020		Cecilia Cruz Moran			Llenado de campos para Primer Contacto
	   14-Julio-2021			Cecilia Cruz Moran			Guardado de interacciones mediante WhatsApp
******************************************************************************* */
public without sharing class TAM_LeadWhatsApp {
    
    static final String STRING_WHATSAPP = 'WhatsApp';
    
    @AuraEnabled
    public static Lead getInformationByLead(String recordId){
        
        Lead leadInformation = new Lead ();
        leadInformation = [Select id,phone,name From Lead WHERE id =: recordId];
        
        return leadInformation; 
        
    }
    
    @AuraEnabled 
    public static void saveLogRecord(String recordId){
        
        Lead leadInformation = new Lead ();
        leadInformation = [SELECT Id,
                                  Name,
                                  Phone,
                                  CreatedDate,
                                  TAM_ContactadoWhatsApp__c,
                                  TAM_NumeroContactoPorWhatsApp__c,
                                  TAM_FechaPrimerContacto__c,
                                  TAM_MedioPrimerContacto__c,
                                  TAM_Contactado__c
                                FROM Lead 
                                WHERE id =: recordId];
        
        if(leadInformation != null){
            //Insertar interaccion
            setInteraccion(recordId);

            //Primer Contacto mediante WhatsApp
            leadInformation.TAM_ContactadoWhatsApp__c = true;
            if(leadInformation.TAM_Contactado__c == false){
                leadInformation.TAM_FechaPrimerContacto__c = Datetime.now();
                leadInformation.TAM_MedioPrimerContacto__c = STRING_WHATSAPP;
                leadInformation.TAM_Contactado__c = true;
            }

            //Contador de intentos de contactos
            if(leadInformation.TAM_NumeroContactoPorWhatsApp__c == null){
                leadInformation.TAM_NumeroContactoPorWhatsApp__c = 1;
            }else{
                leadInformation.TAM_NumeroContactoPorWhatsApp__c += 1;
            }
            try{
                update leadInformation;
                
            }catch(Exception e){
                system.debug('error general al actualizar'+e.getMessage());
                
            }
        }
    }
    
    //Interacciones
    public static void setInteraccion(String recordId){
        Interacciones_Lead__c objInteraccion = new Interacciones_Lead__c();
        objInteraccion.TAM_Candidato__c = recordId;
        objInteraccion.TAM_MedioContacto__c = STRING_WHATSAPP;
        objInteraccion.TAM_FechaContacto__c = Datetime.now();
        objInteraccion.TAM_Detalle__c = Label.TAM_DETALLE_INTERACCION_WHATSAPP + Datetime.now();
        objInteraccion.TAM_IdExterno__c = recordId + '-' + STRING_WHATSAPP + '-' + Datetime.now();
        try{
            insert objInteraccion;
        }catch(Exception e){
            system.debug('error general al insertar interaccion: '+e.getMessage());
        }
    }
}