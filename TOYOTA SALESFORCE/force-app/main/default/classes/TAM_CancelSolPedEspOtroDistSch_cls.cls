/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DistribuidoresFlotillaPrograma__c y cancelar las soicitudes 
                        que el usuario final no ha enviado a autorizar.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    07-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CancelSolPedEspOtroDistSch_cls implements Schedulable{

    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute...');
        
        String sHoraZonaHoraria = TAM_ActCteVehicSch_cls.getHoraZonaHoraria();
        System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute sHoraZonaHoraria: ' + sHoraZonaHoraria);

        //Para la fecha Ini        
        DateTime dtFechaCancelacionIni = DateTime.now().addMinutes(-3);
        DateTime dtFechaCancelacionIni2 = dtFechaCancelacionIni.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCancelacionIni;
        String sdtFechaCancelacionIni = String.valueOf(dtFechaCancelacionIni2);
        String sdtFechaCancelacionIniFinal = sdtFechaCancelacionIni.replace(' ', 'T') + 'Z';
        //Para la fecha Fin
        DateTime dtFechaCancelacionFin = DateTime.now().addMinutes(3);
        DateTime dtFechaCancelacionFin2 = dtFechaCancelacionFin.addHours(Integer.valueOf(sHoraZonaHoraria)); //dtFechaCancelacionFin;
        String sdtFechaCancelacionFin = String.valueOf(dtFechaCancelacionFin2);
        String sdtFechaCancelacionFinFinal = sdtFechaCancelacionFin.replace(' ', 'T') + 'Z';
        System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute sdtFechaCancelacionIniFinal: ' + sdtFechaCancelacionIniFinal + ' sdtFechaCancelacionFinFinal: ' + sdtFechaCancelacionFinFinal);
            
        /*String sQuery = 'Select t.TAM_SolicitudFlotillaPrograma__c, t.TAM_SolicitudFlotillaPrograma__r.Name, t.Name,';
        sQuery += ' t.TAM_NombreDistribuidor__c, t.TAM_IdExterno__c, t.TAM_FechaCancelacion__c, t.TAM_Estatus__c, t.OwnerId ';
        sQuery += ' From TAM_DistribuidoresFlotillaPrograma__c t';
        sQuery += ' Where TAM_FechaCancelacion__c >= ' + String.valueOf(sdtFechaCancelacionIniFinal);
        sQuery += ' And TAM_FechaCancelacion__c <= ' + String.valueOf(sdtFechaCancelacionFinFinal);
        sQuery += ' Order by t.TAM_SolicitudFlotillaPrograma__r.Name';*/

        String sQuery = 'Select t.Name, t.TAM_FechaCheckOut__c, t.TAM_FechaCancelacion__c, t.TAM_Estatus__c,';
        sQuery += ' t.TAM_DealerOrigen__c, t.TAM_DealerDestino__c, t.TAM_IdExterno__c';
        sQuery += ' From TAM_SolicitudDealerDestino__c t';
        if (!Test.isRunningTest()){
            sQuery += ' Where TAM_FechaCancelacion__c >= ' + String.valueOf(sdtFechaCancelacionIniFinal);
            sQuery += ' And TAM_FechaCancelacion__c <= ' + String.valueOf(sdtFechaCancelacionFinFinal);
        }//Fin si !Test.isRunningTest()
        sQuery += ' Order by t.Name';
        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 1';
        System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  objActCancelSolInvBch      
        TAM_CancelSolPedEspOtroDistBch_cls objCancelSolPedEspOtroDistBch = new TAM_CancelSolPedEspOtroDistBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCancelSolPedEspOtroDistBch, 25);

    }
        
}