/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para crear las Excepciones para las solicitudes de
    					Venta Espeacial de Flotilla y Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    13-Mayo-2020      Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitFlotProgExcepcionVinesCmpCtrl {

	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    //Mostrar distribuidor seleccionado en modal
    @AuraEnabled
    public static String consTipoSolicitud(String sRecordId){
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMesesSolicitud sRecordId: ' + sRecordId);	
		String sTipoSol = '';

		//Consulta los datos de la solicitud en el objeto de TAM_SolicitudExpecionIncentivo__c
		for (TAM_DetalleOrdenCompra__c objSolExcepUIncent : [Select id, RecordType.Name 
			From TAM_DetalleOrdenCompra__c Where TAM_SolicitudFlotillaPrograma__c = :sRecordId]){
			//Inicializa sTipoSol
			sTipoSol = objSolExcepUIncent.RecordType.Name;
    	}				
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMesesSolicitud sTipoSol: ' + sTipoSol);	

		//Regresa la lista de sTipoSol
		return sTipoSol;
	}

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static Map<String, Map<String, String>> getSolicitudExcepcionVigente(TAM_WrpSolicitudFlotillaProgramaModelos objModeloSelExcepCte) {    	
    	System.debug('EN getSolicitudExcepcionVigente...');
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente objModeloSelExcepCte: ' + objModeloSelExcepCte);	
		
		Map<String, Map<String, String>> mapNotDistVines = new Map<String, Map<String, String>>();
		String strFolioSolicitdPaso = objModeloSelExcepCte.strFolioSolicitd;
		String strIdExterno = objModeloSelExcepCte.lModelosSeleccionados[0].strIdExterno + '%';
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente strFolioSolicitdPaso: ' + strFolioSolicitdPaso + ' strIdExterno: ' + strIdExterno);	
		
		//Consulta los datos de la solicitud en el objeto de TAM_SolicitudExpecionIncentivo__c
		for (TAM_SolicitudExpecionIncentivo__c objSolExcepUIncent : [Select ID, Name, TAMVIN__c, 
		    TAM_listaVINES__c From TAM_SolicitudExpecionIncentivo__c 
			Where TAM_FolioVentaCorporativa__c =:strFolioSolicitdPaso And TAM_IDExterno__c LIKE :strIdExterno
			And TAM_AprobacionDM__c != 'Rechazado' And TAM_AprobacionFinanzas__c != 'Rechazado' And TAM_VoBo_Aprobacio__c != 'Rechazado'			
			Order by CreatedDate DESC]){
			//3MYDLBYV5KY525117:57064, 3MYDLBYV5KY525118:57064, 3MYDLBYV5KY525119:57064
			String sTAMlistaVINES = objSolExcepUIncent.TAM_listaVINES__c;
			System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente sTAMlistaVINES: ' + sTAMlistaVINES);
			String sTAMlistaVINESFinal = sTAMlistaVINES.replace('(', '').replace(')','');
			String[] sTAMlistaVinesNoDist = sTAMlistaVINESFinal.split(',');
			System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente sTAMlistaVinesNoDist: ' + sTAMlistaVinesNoDist);	
			//Recorre la lista de VINES Y obten el No de Dist al que pertenece
			for (String sVinDistPaso : sTAMlistaVinesNoDist){
				System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente sVinDistPaso: ' + sVinDistPaso);	
				String[] strVinDistSep = sVinDistPaso.contains(':') ? sVinDistPaso.split(':') : new List<String>();
				if (strVinDistSep.size() > 0){
					System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente VIN: ' + strVinDistSep[0].trim() + ' Dist: ' + strVinDistSep[1]);	
					//Ya tiens los datos de la excepcion de TAM_SolicitudExpecionIncentivo__c metelos al mapa de mapNotDistVines
					if (mapNotDistVines.containsKey(strVinDistSep[1]))
						mapNotDistVines.get(strVinDistSep[1]).put(strVinDistSep[0].trim(), objSolExcepUIncent.Name);
					if (!mapNotDistVines.containsKey(strVinDistSep[1]))
						mapNotDistVines.put(strVinDistSep[1], new Map<String, String>{strVinDistSep[0].trim() => objSolExcepUIncent.Name});
					if (strVinDistSep[0] == objSolExcepUIncent.TAMVIN__c)
						break;
				}//Fin si strVinDistSep.size() > 0
			}//Fin del for para sTAMlistaVinesNoDist	
		}
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getSolicitudExcepcionVigente mapNotDistVines: ' + mapNotDistVines);	

		//Reregresa el mapa final 
		return mapNotDistVines;
    }

    //Obtener un Set de las series disponibles
    /*@AuraEnabled
    public static List<TAM_SolicitFlotProgExcepcionVinesCmWrp> consListaInventarioEntrega(String recordId,  String strIdCatCentMod,
    	TAM_WrpSolicitudFlotillaProgramaModelos objModeloSelExcepCte, Map<String, Map<String, String>> mapNoDistVines) {    	
    	System.debug('EN consListaInventarioEntrega...');
    	System.debug('EN consListaInventarioEntrega recordId: ' + recordId);
		System.debug('EN consListaInventarioEntrega strIdCatCentMod: ' + strIdCatCentMod);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.consListaInventarioEntrega objModeloSelExcepCte: ' + objModeloSelExcepCte);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.consListaInventarioEntrega mapNoDistVines: ' + mapNoDistVines);	

    	String sCadenaPaso = '';  
    	String strIdCatCentModFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdCatCentrModelos;
    	String strIdSolicitudFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdSolicitud;
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.consListaInventarioEntrega strIdCatCentMod: ' + strIdCatCentMod);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.consListaInventarioEntrega strIdCatCentModFinal: ' + strIdCatCentModFinal);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.consListaInventarioEntrega strIdSolicitudFinal: ' + strIdSolicitudFinal);	
    	
		Map<String, Set<String>> mapDistSetVines = new Map<String, Set<String>>();
		List<String> listDistr = new List<String>();			
		List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lWrpExecpVines = new List<TAM_SolicitFlotProgExcepcionVinesCmWrp>();
		List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lWrpExecpVinesFinal = new List<TAM_SolicitFlotProgExcepcionVinesCmWrp>();
		Set<String> setVinesAsignados = new Set<String>();
						
		//Agrega toda la info listDistr
		listDistr.addAll(mapDistSetVines.KeySet());
		listDistr.sort();
		System.debug('EN consListaInventarioEntrega listDistr: ' + listDistr);
		
		//Recorre la lista de reg y ordena los vines
		for(String sDistPaso : listDistr){
			//Recorre la lista de los vines asociados a sDistPaso
 			for (TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso : lWrpExecpVines){
				//System.debug('EN consListaInventarioEntrega0 objPaso.strVin: ' + objPaso.strVin + ' objPaso.strFechaRecibo: ' + objPaso.strFechaRecibo);
				if (objPaso.strDistribuidor == sDistPaso && objPaso.strVin != ''){	
					if (objPaso.strFolioExcepcion != '' && objPaso.strFolioExcepcion != null){
						objPaso.bolSeleccionar = true;		
						objPaso.bolVinBloqueado = true;
						objPaso.bolSelBloqueado = true;						
					}
					if (objPaso.strFolioExcepcion == '' || objPaso.strFolioExcepcion == null)
						objPaso.bolSeleccionado = true;
					lWrpExecpVinesFinal.add(objPaso);
				}//Fin si objPaso.strDistribuidor == sDistPaso && objPaso.strVin == ''
			}//Fin drl for para lWrpExecpVines
			for (TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso : lWrpExecpVines){
				//System.debug('EN consListaInventarioEntrega2 objPaso.strVin: ' + objPaso.strVin + ' objPaso.strFechaRecibo: ' + objPaso.strFechaRecibo);
				if (objPaso.strDistribuidor == sDistPaso && objPaso.strVin == '')
					lWrpExecpVinesFinal.add(objPaso);
			}//Fin drl for para lWrpExecpVines
		}//Fin del for para mapDistSetVines.keySet()
		
		System.debug('EN consListaInventarioEntrega lWrpExecpVinesFinal: ' + lWrpExecpVinesFinal);
		//Regresa la lista del tipo TAM_DODSolicitudesPedidoEspecial__c
		return lWrpExecpVinesFinal;
    }*/
    
    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static List<TAM_SolicitFlotProgExcepcionVinesCmWrp> getDatosDistribuidores(String recordId,  String strIdCatCentMod,
    	TAM_WrpSolicitudFlotillaProgramaModelos objModeloSelExcepCte, Map<String, Map<String, String>> mapNoDistVines) {    	
    	System.debug('EN getDatosDistribuidores...');
    	System.debug('EN getDatosDistribuidores recordId: ' + recordId);
		System.debug('EN getDatosDistribuidores strIdCatCentMod: ' + strIdCatCentMod);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores objModeloSelExcepCte: ' + objModeloSelExcepCte);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores mapNoDistVines: ' + mapNoDistVines);	

    	String sCadenaPaso = '';  
    	String strIdCatCentModFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdCatCentrModelos;
    	String strIdSolicitudFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdSolicitud;
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdCatCentMod: ' + strIdCatCentMod);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdCatCentModFinal: ' + strIdCatCentModFinal);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdSolicitudFinal: ' + strIdSolicitudFinal);	
    	
		Map<String, Set<String>> mapDistSetVines = new Map<String, Set<String>>();
		List<String> listDistr = new List<String>();			
		List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lWrpExecpVines = new List<TAM_SolicitFlotProgExcepcionVinesCmWrp>();
		List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lWrpExecpVinesFinal = new List<TAM_SolicitFlotProgExcepcionVinesCmWrp>();
		Set<String> setVinesAsignados = new Set<String>();
		Map<String, String> mapIdOwnerNoDist = new  Map<String, String>();
		Map<String, String> mapNoDistNombre = new  Map<String, String>();
		Set<String> setIdOwner = new Set<String>();
        Set<String> setVinesDod = new Set<String>();
						
		//Se trata de una solicitud de tipo Pedido especial 
		for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspe : [Select id, Name, 
			TAM_SolicitudFlotillaPrograma__c, TAM_CatalogoCentralizadoModelos__c, TAM_VINDistribuidor__c,
			TAM_Entrega__c, TAM_VIN__c, TAM_Estatus__c, TAM_NoOrden__c,	TAM_FechaHoraRecibo__c
			From TAM_DODSolicitudesPedidoEspecial__c Where 
			TAM_SolicitudFlotillaPrograma__c =: strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c =: strIdCatCentModFinal
			Order by TAM_Entrega__c, TAM_Estatus__c, TAM_VIN__c]){
				
			Boolean bTieneVIN = objDodSolPedEspe.TAM_VIN__c != null ? true : false;
			String sVIN = objDodSolPedEspe.TAM_VIN__c != NULL ? objDodSolPedEspe.TAM_VIN__c : '';
			String[] sTAM_Entrega = objDodSolPedEspe.TAM_Entrega__c.split('-');
			System.debug('EN getDatosDistribuidores sVIN: ' + sVIN + ' sTAM_Entrega: ' + sTAM_Entrega + ' bTieneVIN: ' + bTieneVIN + ' sTAM_Entrega[0]: ' + sTAM_Entrega[0]);
			Map<String, String> mapPasoVinesFolExecp = new Map<String, String>();

			if (bTieneVIN){
				//Mete al mapa el lugar de la entrega
				mapDistSetVines.put(objDodSolPedEspe.TAM_Entrega__c.toUpperCase(), new Set<String>{});
				//Busca sTAM_Entrega[0] en el mapa de mapNoDistVines
				if (mapNoDistVines.containsKey(sTAM_Entrega[0])){
					mapPasoVinesFolExecp = mapNoDistVines.get(sTAM_Entrega[0]);
					System.debug('EN getDatosDistribuidores mapPasoVinesFolExecp.KeySet(): ' + mapPasoVinesFolExecp.KeySet());
					System.debug('EN getDatosDistribuidores mapPasoVinesFolExecp.Values(): ' + mapPasoVinesFolExecp.Values());					
					//Busca en el mapa de mapPasoVinesFolExecp sVIN	si tiene algo sVIN
					if (mapPasoVinesFolExecp.containsKey(sVIN)){
						String sIdFolExcep = mapPasoVinesFolExecp.get(sVIN); 
						System.debug('EN getDatosDistribuidores sVIN2: ' + sVIN + ' sIdFolExcep: ' + sIdFolExcep);						
						//Crea el objeto del tipo TAM_SolicitFlotProgExcepcionVinesCmWrp
						TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso = new TAM_SolicitFlotProgExcepcionVinesCmWrp(
							objDodSolPedEspe.TAM_Entrega__c, sVIN, '', false, false
						);
						objPaso.strIdRegistroDOD = objDodSolPedEspe.id;
						objPaso.strFolioExcepcion = sIdFolExcep; 
						objPaso.strTipoSolicitud = 'PEDIDO ESPECIAL';
						//Agregas a la lista de lWrpExecpVines el objPaso
						lWrpExecpVines.add(objPaso);
						//Agrregalo a setVinesDod
						setVinesDod.add(objDodSolPedEspe.TAM_VIN__c);
						System.debug('EN getDatosDistribuidores objPaso1: ' + objPaso);
						System.debug('EN getDatosDistribuidores objDodSolPedEspe1: ' + objDodSolPedEspe);
					}//Fin si mapPasoVinesFolExecp.containsKey(sVIN)
					if (!mapPasoVinesFolExecp.containsKey(sVIN) || Test.isRunningTest()){
						System.debug('EN getDatosDistribuidores sVIN3: ' + sVIN);						
						//Crea el objeto del tipo TAM_SolicitFlotProgExcepcionVinesCmWrp
						TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso = new TAM_SolicitFlotProgExcepcionVinesCmWrp(
							objDodSolPedEspe.TAM_Entrega__c, sVIN, '', false, false
						);
						objPaso.strIdRegistroDOD = objDodSolPedEspe.id;
						objPaso.strTipoSolicitud = 'PEDIDO ESPECIAL';					
						//Agregas a la lista de lWrpExecpVines el objPaso
						lWrpExecpVines.add(objPaso);
                        //Agrregalo a setVinesDod
                        setVinesDod.add(objDodSolPedEspe.TAM_VIN__c);						
						//System.debug('EN getDatosDistribuidores objPaso2: ' + objPaso);
						//System.debug('EN getDatosDistribuidores objDodSolPedEspe2: ' + objDodSolPedEspe);					
					}//Fin si mapPasoVinesFolExecp.containsKey(sVIN) && setVinesAsignados.contains(sVIN)
				}//Fin si mapNoDistVines.containsKey(sTAM_Entrega[0]
				//No existe en el mapa de mapNoDistVines				
				if (!mapNoDistVines.containsKey(sTAM_Entrega[0])  || Test.isRunningTest()){
					//Crea el objeto del tipo TAM_SolicitFlotProgExcepcionVinesCmWrp
					TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso = new TAM_SolicitFlotProgExcepcionVinesCmWrp(
						objDodSolPedEspe.TAM_Entrega__c, sVIN, '', false, false
					);
					objPaso.strIdRegistroDOD = objDodSolPedEspe.id;
					objPaso.strTipoSolicitud = 'PEDIDO ESPECIAL';			
					//Agregas a la lista de lWrpExecpVines el objPaso
					lWrpExecpVines.add(objPaso);
                    //Agrregalo a setVinesDod
                    setVinesDod.add(objDodSolPedEspe.TAM_VIN__c);					
					//System.debug('EN getDatosDistribuidores objPaso3: ' + objPaso);
				    //mapNoDistVines.put(sTAM_Entrega[0], new Map<String, String>{objDodSolPedEspe.TAM_VIN__c => ''});
				}//Fin si !mapNoDistVines.containsKey(sTAM_Entrega[0])
                System.debug('EN getDatosDistribuidores lWrpExecpVines: ' + lWrpExecpVines);
			}//Fin si bTieneVIN
		} //Fin del for para TAM_DODSolicitudesPedidoEspecial__c

		//Toma el propietario del registro
		for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVines : [Select id, OwnerId
			From TAM_CheckOutDetalleSolicitudCompra__c t
			Where TAM_SolicitudFlotillaPrograma__c = :strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c = :strIdCatCentModFinal
			And TAM_VIN__c != null ]){		
			setIdOwner.add(objCheckOutVines.OwnerId);
		}
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla setIdOwner: ' + setIdOwner);
		
		//Consulta los datos del usuario asociado a setIdOwner
		for (User propietario : [Select u.id, u.CodigoDistribuidor__c From User u where id IN:setIdOwner]){
			String sCodigoDistribuidor = propietario.CodigoDistribuidor__c != null ? propietario.CodigoDistribuidor__c : '57000';
			mapIdOwnerNoDist.put(propietario.id, sCodigoDistribuidor);
		}
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdOwnerNoDist: ' + mapIdOwnerNoDist);
			
		//Finalmente conslta el nombre del dostribuidor
		for (Account Distrib : [Select id, a.Name, a.Codigo_Distribuidor__c From Account a
			where a.Codigo_Distribuidor__c IN :mapIdOwnerNoDist.values() And RecordTypeId =:sRectorTypePasoDistribuidor]){
			mapNoDistNombre.put(Distrib.Codigo_Distribuidor__c, Distrib.Name);	
		}
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapNoDistNombre: ' + mapNoDistNombre);
			
		//Consulta los datos de los vines asociados a TAM_SolicitudFlotillaPrograma__c
		for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVines : [Select t.TAM_VIN__c, t.TAM_SolicitudFlotillaPrograma__c, 
			t.TAM_IdExterno__c, t.TAM_FechaEntrega__c, t.TAM_FechaEntregaDealer__c, t.TAM_EstatusEntrega__c, t.OwnerId,
			t.TAM_EntregadoDealer__c, t.TAM_CatalogoCentralizadoModelos__c, t.TAM_MotivoRechazoSolicitudDealer__c,
			t.TAM_TipoVenta__c From TAM_CheckOutDetalleSolicitudCompra__c t
			Where TAM_SolicitudFlotillaPrograma__c = :strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c = :strIdCatCentModFinal
			And TAM_VIN__c != null Order by OwnerId, TAM_EstatusEntrega__c, TAM_VIN__c ]){
	    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla objCheckOutVines: ' + objCheckOutVines);
			
			String sCodDist = mapIdOwnerNoDist.containsKey(objCheckOutVines.OwnerId) ? mapIdOwnerNoDist.get(objCheckOutVines.OwnerId) : null;
			String sEntrega = sCodDist != null ? (mapNoDistNombre.containsKey(sCodDist) ? mapNoDistNombre.get(sCodDist) : '') : '';
			mapDistSetVines.put(sCodDist + '-' + sEntrega.toUpperCase(), new Set<String>{});
	    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sCodDist: ' + sCodDist + ' sEntrega: ' + sEntrega);
	    	
			Boolean bTieneVIN = objCheckOutVines.TAM_VIN__c != null ? true : false;
			String sVIN = objCheckOutVines.TAM_VIN__c != NULL ? objCheckOutVines.TAM_VIN__c : '';
			String sTamFechaHoraRecibo = objCheckOutVines.TAM_FechaEntrega__c != null ? String.valueOf( objCheckOutVines.TAM_FechaEntrega__c.format() ) : '';  //format('dd/MM/yyyy','America/Mexico_City')
			Boolean bConfirmado = objCheckOutVines.TAM_EntregadoDealer__c;
			Map<String, String> mapPasoVinesFolExecp = new Map<String, String>();			
			System.debug('EN getDatosDistribuidoresInv sTamFechaHoraRecibo: ' + sTamFechaHoraRecibo + ' bConfirmado: ' + bConfirmado);			
	    	
			//Crea el objeto del tipo TAM_SolicitFlotProgExcepcionVinesCmWrp
			TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso = new TAM_SolicitFlotProgExcepcionVinesCmWrp(
				sCodDist + '-' + sEntrega.toUpperCase(), objCheckOutVines.TAM_VIN__c, '', false, false
			);
			objPaso.bolConfirmaEntrega = objCheckOutVines.TAM_EntregadoDealer__c;
			objPaso.bolConfirmado = objCheckOutVines.TAM_EntregadoDealer__c;
			objPaso.dtFechaEntrega = objCheckOutVines.TAM_FechaEntregaDealer__c;
			objPaso.strCancelacion = objCheckOutVines.TAM_EstatusEntrega__c;
			objPaso.bolCancelada = objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado' ? true : false;
			objPaso.bolVinBloqueado = objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado' ? true : false;
			objPaso.strStyleCancelacion = 'position: absolute; top: -8px;'; //position: absolute; top: 8px;
			
			//Ve si sCodDist exixte en mapNoDistVines
			if (mapNoDistVines.containsKey(sCodDist)){
				mapPasoVinesFolExecp = mapNoDistVines.get(sCodDist);
				System.debug('EN getDatosDistribuidores mapPasoVinesFolExecp.KeySet()2: ' + mapPasoVinesFolExecp.KeySet());
				System.debug('EN getDatosDistribuidores mapPasoVinesFolExecp.Values()2: ' + mapPasoVinesFolExecp.Values());					
				//Busca en el mapa de mapPasoVinesFolExecp sVIN	si tiene algo sVIN
				if (mapPasoVinesFolExecp.containsKey(sVIN)){
					String sIdFolExcep = mapPasoVinesFolExecp.get(sVIN); 
					//Ve si ya tiene una excepción asociada
					objPaso.strFolioExcepcion = sIdFolExcep;
					System.debug('EN getDatosDistribuidores sIdFolExcep4: ' + sIdFolExcep);

		            //Ve si es cancelada
		            if (objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'){
		                objPaso.strStyleCancelacion = 'position: absolute; top: 8px;';
		                objPaso.bolCancelComent = true;
		            }//Fin si objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'
		            objPaso.strMotivoCancelacion = objCheckOutVines.TAM_MotivoRechazoSolicitudDealer__c;    
		            objPaso.bolSelBloqueado = false;
		            objPaso.blnFlotillaPrograma = false;
		            objPaso.strTipoSolicitud = objCheckOutVines.TAM_TipoVenta__c; //'Inventario';
		            
		            //Agregalos a la lista de lWrpExecpVines
                    if (!setVinesDod.contains(objCheckOutVines.TAM_VIN__c))
		                  lWrpExecpVines.add(objPaso);
                    System.debug('EN getDatosDistribuidoresInv YA ESTA EN EL MAPA DE mapNoDistVines0 lWrpExecpVines: ' + lWrpExecpVines);
				}//Fin si mapPasoVinesFolExecp.containsKey(sVIN)
                if (!mapPasoVinesFolExecp.containsKey(sVIN)){
		            //Ve si es cancelada
		            if (objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'){
		                objPaso.strStyleCancelacion = 'position: absolute; top: 8px;';
		                objPaso.bolCancelComent = true;
		            }//Fin si objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'
		            objPaso.strMotivoCancelacion = objCheckOutVines.TAM_MotivoRechazoSolicitudDealer__c;    
		            objPaso.bolSelBloqueado = false;
		            objPaso.blnFlotillaPrograma = false;
		            objPaso.strTipoSolicitud = objCheckOutVines.TAM_TipoVenta__c; //'Inventario';
		            
		            //Agregalos a la lista de lWrpExecpVines
		            if (!setVinesDod.contains(objCheckOutVines.TAM_VIN__c))
		              lWrpExecpVines.add(objPaso);                
                    System.debug('EN getDatosDistribuidoresInv YA ESTA EN EL MAPA DE mapNoDistVines1 lWrpExecpVines: ' + lWrpExecpVines);
                }//Fin si !mapPasoVinesFolExecp.containsKey(sVIN)
			}//Fin si mapNoDistVines.containsKey(sCodDist)
            
            //No esta en mapNoDistVines crealo y ponglo de tipo INVENTARIO
            if (!mapNoDistVines.containsKey(sCodDist)){
	            //Ve si es cancelada
	            if (objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'){
	                objPaso.strStyleCancelacion = 'position: absolute; top: 8px;';
	                objPaso.bolCancelComent = true;
	            }//Fin si objCheckOutVines.TAM_EstatusEntrega__c == 'Cancelado'
	            objPaso.strMotivoCancelacion = objCheckOutVines.TAM_MotivoRechazoSolicitudDealer__c;    
	            objPaso.bolSelBloqueado = false;
	            objPaso.blnFlotillaPrograma = false;
	            objPaso.strTipoSolicitud = 'INVENTARIO';
	            
	            //Agregalos a la lista de lWrpExecpVines
                if (!setVinesDod.contains(objCheckOutVines.TAM_VIN__c))
	               lWrpExecpVines.add(objPaso);
	            System.debug('EN getDatosDistribuidoresInv NO ESTA EN EL MAPA DE mapNoDistVines lWrpExecpVines: ' + lWrpExecpVines);
            }//Fin si !mapNoDistVines.containsKey(sCodDist)
            
		}//Fin del for para TAM_CheckOutDetalleSolicitudCompra__c
		
		//Agrega toda la info listDistr
		listDistr.addAll(mapDistSetVines.KeySet());
		listDistr.sort();
		System.debug('EN getDatosDistribuidores listDistr: ' + listDistr);
		
		//Recorre la lista de reg y ordena los vines
		for(String sDistPaso : listDistr){
			//Recorre la lista de los vines asociados a sDistPaso
 			for (TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso : lWrpExecpVines){
				System.debug('EN getDatosDistribuidores0 objPaso.strVin: ' + objPaso.strVin + ' objPaso.strFechaRecibo: ' + objPaso.strFechaRecibo);
				if (objPaso.strDistribuidor == sDistPaso && objPaso.strVin != ''){	
					if (objPaso.strFolioExcepcion != '' && objPaso.strFolioExcepcion != null){
						objPaso.bolSeleccionar = true;		
						objPaso.bolVinBloqueado = true;
						objPaso.bolSelBloqueado = true;						
					}
					if (objPaso.strFolioExcepcion == '' || objPaso.strFolioExcepcion == null)
						objPaso.bolSeleccionado = true;
					lWrpExecpVinesFinal.add(objPaso);
				}//Fin si objPaso.strDistribuidor == sDistPaso && objPaso.strVin == ''
			}//Fin drl for para lWrpExecpVines
			for (TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso : lWrpExecpVines){
				System.debug('EN getDatosDistribuidores2 objPaso.strDistribuidor: ' + objPaso.strDistribuidor.toUpperCase() + ' sDistPaso: ' + sDistPaso);
				if (objPaso.strDistribuidor.toUpperCase() == sDistPaso && objPaso.strVin == '')
					lWrpExecpVinesFinal.add(objPaso);
			}//Fin drl for para lWrpExecpVines
		}//Fin del for para mapDistSetVines.keySet()
		
		System.debug('EN getDatosDistribuidores lWrpExecpVinesFinal: ' + lWrpExecpVinesFinal);
		//Regresa la lista del tipo TAM_DODSolicitudesPedidoEspecial__c
		return lWrpExecpVinesFinal;
    }
    
    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpSolicitudFlotillaProgramaModelos updateVinesLista(TAM_WrpSolicitudFlotillaProgramaModelos objModeloSelExcepCte, 
    	List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lstDistriEntrega ) {    	
    	System.debug('EN updateVinesLista objModeloSelExcepCte: ' + objModeloSelExcepCte);
		System.debug('EN updateVinesLista lstDistriEntrega: ' + lstDistriEntrega);
		
		//un objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos
		TAM_WrpSolicitudFlotillaProgramaModelos objPasoWrpSolProgMod = objModeloSelExcepCte;	
		objPasoWrpSolProgMod.lVinesSeleccionados = new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion>();
		
		//Ahora si dale la vuelta a la lista de lstDistriEntrega y toma los vines capturados y que no estan bolSeleccionado
		for (TAM_SolicitFlotProgExcepcionVinesCmWrp objWrpVinSelc : lstDistriEntrega){
			if (objWrpVinSelc.bolSeleccionar && (objWrpVinSelc.strFolioExcepcion == NULL || objWrpVinSelc.strFolioExcepcion == '')){
				//Separa el nombre del distribuidor y ponlo toma la clave
				String[] strDistribuidorPaso = objWrpVinSelc.strDistribuidor.split('-');
				String strVinPaso = objWrpVinSelc.strVin + ':' + strDistribuidorPaso[0]; 
				System.debug('EN updateVinesLista objWrpVinSelc.strVin: ' + objWrpVinSelc.strVin + ' strVinPaso: ' + strVinPaso);
				objPasoWrpSolProgMod.lVinesSeleccionados.add(new TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion(
						strVinPaso
					)
				);
			}//Fin si objWrpVinSelc.bolSeleccionar && (objWrpVinSelc.strFolioExcepcion == NULL || objWrpVinSelc.strFolioExcepcion == ''
		}//Fin del for para lstDistriEntrega
    	
    	//Regresa el objeto al componente
    	System.debug('EN updateVinesLista objPasoWrpSolProgMod: ' + objPasoWrpSolProgMod);
    	return objPasoWrpSolProgMod;
    }    
    
}