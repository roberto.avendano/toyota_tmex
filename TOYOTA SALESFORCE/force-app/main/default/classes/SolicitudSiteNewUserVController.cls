public class SolicitudSiteNewUserVController {
    public String sivId {get;set;}
    public String retURL {get;set;}
    public boolean showAllFields{get;set;}
    public boolean showNuevoUsuario{get;set;}
    public boolean showExistenteUsuario{get;set;}
    public boolean showRegresarGuardado{get;set;}
    public boolean showCancelar{get;set;}
    public String picklistValue{get;set;}
    public SolicitudInternaVehiculos__c solicitud{get;set;}
    public ApexPages.StandardController sivController;
    
    public SolicitudSiteNewUserVController(ApexPages.StandardController stdcontroller){
        retURL = ApexPages.currentPage().getParameters().get('prev');
        sivId =  ApexPages.currentPage().getParameters().get('idsolicitud');
        this.sivController = stdcontroller;
        solicitud = [SELECT Id,  Name, Solicitante__c FROM SolicitudInternaVehiculos__c  WHERE Id =:sivId];
        System.debug('Solicitud: ' +solicitud.Solicitante__c);   
        System.debug(sivId);
        UsuarioVehiculo__c uv = (UsuarioVehiculo__c)sivController.getRecord();
        uv.Nombre__c = solicitud.Solicitante__c;
        showRegresarGuardado = false;
    }
    
    public PageReference mostrarCampos(){
        // Contacto Existente Nuevo Usuario
        showAllFields = true;
        showNuevoUsuario = picklistValue=='Nuevo Usuario'? true: false;
        showExistenteUsuario = pickListValue=='Contacto Existente'? true:false;
        return null;
    }
    
    public PageReference guardar(){
        PageReference page;        
        UsuarioVehiculo__c uv = (UsuarioVehiculo__c)sivController.getRecord();
        uv.SolicitudInternaVehiculos__c = sivId;
        try{
            insert uv;  
            showAllFields = false;
            showRegresarGuardado = true;
            showCancelar = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Usuario creado/registrado')); 
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        } 
        return null; 
    }
    
    public PageReference cerrar(){
        PageReference page;
        page = new PageReference(retURL);
        page.setRedirect(true);               
        return page;
    }
}