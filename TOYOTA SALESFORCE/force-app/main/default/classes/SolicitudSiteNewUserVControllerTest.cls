@isTest
public class SolicitudSiteNewUserVControllerTest {
    
    @isTest
    static void test_one(){
        Date inicio = date.parse('01/03/2018');
        Date fin = date.parse('31/03/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='ToyotaGroup'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='ToyotaGroup'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Departamento__c = 'Human Resources',
                                Codigo__c = '456RTY',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                RecordTypeId = conRT.Id);
        insert c;
        
        RecordType sivRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='SolicitudInternaVehiculos__c' 
                            AND DeveloperName='PoolAsignacion'];
        
        SolicitudInternaVehiculos__c siv = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                            RecordTypeId = sivRT.Id,
                                                                            Estatus__c='Nuevo');
        insert siv;
        
        RecordType uvRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='UsuarioVehiculo__c' 
                            AND DeveloperName='NuevoUsuario'];
        
        UsuarioVehiculo__c uv = new UsuarioVehiculo__c(SolicitudInternaVehiculos__c=siv.Id,
                                                      NombreContacto__c='Usuario vehiculo 1 Test',
                                                      Apellidos__c = 'Site',
                                                      RelacionEdit__c = 'Padre',
                                                      recordTypeId = uvRT.Id);
        
        try{
        insert uv;
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
        test.startTest();
        String prev = '%2Fapex%2FSolicitudSiteEPoolSolicitante%3Fid%3Da0v5B000002KVHyQAO';
        PageReference page = System.Page.SolicitudSiteNewUserV;
        page.getParameters().put('idsolicitud', String.valueOf(siv.Id));
        page.getParameters().put('prev',prev);
        Test.setCurrentPage(page);
        SolicitudSiteNewUserVController ssnuv = new SolicitudSiteNewUserVController(new ApexPages.standardController(uv));
        ssnuv.mostrarCampos();  
        ssnuv.guardar();  
        ssnuv.cerrar();  
        test.stopTest();
    }
}