public class OrderTriggerHandler extends TriggerHandler{
    public static boolean firstRun = true;
    private Map<Id, Order> oldOrderMap;
    private Map<Id, Order> newOrderMap;
    private List<Order> newOrderList;
    
    public OrderTriggerHandler(){
        this.oldOrderMap = (Map<Id, Order>) Trigger.oldMap;
        this.newOrderMap = (Map<Id, Order>) Trigger.newMap;
        this.newOrderList = (List<Order>) Trigger.new;
        System.debug('OrderTriggerHandler');//Una solicitud debe tener al meno
    }
    
    public override void beforeInsert(){
        system.debug('evento before insert');
        validaEstatusNuevaBeforeInsert(newOrderList);
        validaEventosPartesRobadasAnio(newOrderList,true);
    }
    
    public override void beforeUpdate(){
        system.debug('evento before update');
        validateEstatusBeforeUpdate(newOrderMap,oldOrderMap);
    }
    
    
    public override void afterUpdate() {
        system.debug('evento after update');
        crearResumen(newOrderList);
        validaEventosPartesRobadasAnio(newOrderList,false);
        //Cuenta solicitudes
        cuentaSolicitudesAutorizadas(newOrderMap,oldOrderMap);
        
    }
    
    
    private static void validaEventosPartesRobadasAnio(List<Order> pedidos, Boolean actualizaVIN){
        System.debug('validaEventosPartesRobadasAnio');
        Set<Id> setVehiculos = new Set<Id>();
        
        Map<Id, RecordType> mapRTCases = new Map<Id, RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE SobjectType='Order']);
        
        //Obtenemos los id unicos de VIN asociados a los casos
        for(Order o : pedidos){
            if(o.RecordTypeId!=null && mapRTCases.get(o.RecordTypeId).DeveloperName=='Solicitud_partes_robadas')
                if(o.Vehiculo__c!=null){
                    setVehiculos.add(o.Vehiculo__c);
                }
        }
        
        //Validar si se seleccionaron setVehiculos
        if(setVehiculos.size()>0){
            //ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('PREventosAnuales');
            Map<Id, Set<Id>> conteoAutorizados = new Map<Id, Set<Id>>();
            for(Order o : [SELECT Vehiculo__c, Id FROM Order WHERE Estatus__c='Autorizada' and Vehiculo__c IN:setVehiculos and EffectiveDate=THIS_YEAR]){
                if(!conteoAutorizados.containsKey(o.Vehiculo__c)){
                    conteoAutorizados.put(o.Vehiculo__c, new Set<Id>());
                }
                conteoAutorizados.get(o.Vehiculo__c).add(o.Id);
            }
            
            Map<Id,Vehiculo__c> updVehiculos = new Map<Id,Vehiculo__c>();
            for(Id idVehiculo : setVehiculos){
                updVehiculos.put(idVehiculo, new Vehiculo__c(
                    Id = idVehiculo,
                    ContadorEventosAnio__c = conteoAutorizados.containsKey(idVehiculo)?conteoAutorizados.get(idVehiculo).size():0
                )
                                );
            }
            
            if(updVehiculos.size()>0){
                update updVehiculos.values();
            }
        }
    }
    
    private static void validaEstatusNuevaBeforeInsert(List<Order> pedidos){
        System.debug('validaEstatusNuevaBeforeInsert');
        Set<String> setEstatusError = new Set<String>{'Solicitud nueva','Autorizada'};
            for(Order odr : pedidos){
                if(setEstatusError.contains(odr.Estatus__c)){
                    odr.Estatus__c.addError('No se puede crear el registro con Estatus ' + odr.Estatus__c);
                }
                
                OrderTriggerHandler.validaSeccionDocumentosSolicitadosCliente(odr);
            }
    }
    
    private static void validateEstatusBeforeUpdate(Map<Id, Order> newOrderMap, Map<Id, Order> oldOrderMap){
        System.debug('validateEstatusBeforeUpdate');
        Map<Id, Integer> countmap= new Map<Id, Integer>();
        Map<Id, Order> ordmap = new Map<Id, Order>();
        
        AggregateResult[] groupedResults=[SELECT ParentId, COUNT(Id) contador FROM Attachment WHERE ParentId IN :newOrderMap.keySet() GROUP BY ParentId];
        for(AggregateResult ar : groupedResults){
            countmap.put((Id)ar.get('ParentId'), (Integer)ar.get('contador'));
        }
        
        for(Order o: newOrderMap.values()){
            if(o.Estatus__c=='Solicitud nueva'){
                ParametrosConfiguracionToyota__c pc = ParametrosConfiguracionToyota__c.getInstance('PRNumDocumentos');
                //Funcionalidad de conteo de documentos por banderas en orden
                OrderTriggerHandler.documentosPorValidar(o);
                Integer conteoAtt = Integer.valueOf(pc.Valor__c);
                if(conteoAtt > 0){
                    if(countmap.get(o.Id) == null || countmap.get(o.Id) < conteoAtt){
                        o.addError('Falta adjuntar archivos requeridos a la solicitud');
                    }
                }
            }
        }
    }
    
    public static void validaSeccionDocumentosSolicitadosCliente(Order o){
        if(o.IFELicencia__c != o.IFELicenciaFormula__c){o.IFELicencia__c.addError('No puede modificar este campo');}
        if(o.TarjetaCirculacion__c != o.TarjetaCirculacionFormula__c){o.TarjetaCirculacion__c.addError('No puede modificar este campo');}
        if(o.FormatoSolicitudPartesRobadas__c != o.FormatoSolicitudPartesRobadasFormula__c){o.FormatoSolicitudPartesRobadas__c.addError('No puede modificar este campo');}
        if(o.CartaMembretadaRazonSocial__c != o.CartaMembretadaRazonSocialFormula__c){o.CartaMembretadaRazonSocial__c.addError('No puede modificar este campo');}
        if(o.CopiaFacturaOriginal__c != o.CopiaFacturaOriginalFormula__c){o.CopiaFacturaOriginal__c.addError('No puede modificar este campo');}
        if(o.CartaRobo__c != o.CartaRoboFormula__c){o.CartaRobo__c.addError('No puede modificar este campo');}
        if(o.FotoParte__c != o.FotoParteFormula__c){o.FotoParte__c.addError('No puede modificar este campo');}
        if(o.FotoPlacaVIN__c != o.FotoPlacaVINFormula__c){o.FotoPlacaVIN__c.addError('No puede modificar este campo');}
        if(o.FotoVehiculo__c != o.FotoVehiculoFormula__c){o.FotoVehiculo__c.addError('No puede modificar este campo');}
        if(o.Otros__c != o.OtrosFormula__c){o.Otros__c.addError('No puede modificar este campo');}
    }
    
    
    public static void documentosPorValidar(Order o){
        if(o.IFELicencia__c != o.IFELicenciaDealer__c){o.IFELicenciaDealer__c.addError(Label.MensajeErrorIFE);}
        if(o.TarjetaCirculacion__c != o.TarjetaCirculacionDealer__c){o.TarjetaCirculacionDealer__c.addError(Label.MensajeErrorTarjetaCirculacion);}
        if(o.FormatoSolicitudPartesRobadas__c != o.FormatoSolicitudPartesDealer__c){o.FormatoSolicitudPartesDealer__c.addError(Label.MensajeErrorFormatoSolicitudPartesRobadas);}
        if(o.CartaMembretadaRazonSocial__c != o.CartaMembretadaDealer__c){o.CartaMembretadaDealer__c.addError(Label.MensajeErrorCartaMembretadaRazonSocial);}
        if(o.CopiaFacturaOriginal__c != o.CopiaFacturaOriginalDealer__c){o.CopiaFacturaOriginalDealer__c.addError(Label.MensajeErrorCopiaFacturaOriginal);}
        if(o.CartaRobo__c != o.CartaRoboDealer__c){o.CartaRoboDealer__c.addError(Label.MensajeErrorCartaRobo);}
        if(o.FotoParte__c != o.FotoParteDealer__c){o.FotoParteDealer__c.addError(Label.MensajeErrorFotoParte);}
        if(o.FotoPlacaVIN__c != o.FotoPlacaVINDealer__c){o.FotoPlacaVINDealer__c.addError(Label.MensajeErrorFotoPlacaVIN);}
        if(o.FotoVehiculo__c != o.FotoVehiculoDealer__c){o.FotoVehiculoDealer__c.addError(Label.MensajeErrorFotoVehiculo);}
        if(o.Otros__c != o.OtrosDealer__c){o.OtrosDealer__c.addError(Label.MensajeErrorOtros);}
    }
    
    
    private static void cuentaSolicitudesAutorizadas(Map<Id, Order> newOrderMap, Map<Id, Order> oldOrderMap){
        System.debug('En cuentaSolicitudesAutorizadas...');
        Map<Id, Integer> countmap= new Map<Id, Integer>();
        Map<Id, Order> ordmap = new Map<Id, Order>();
        Map<String, Integer> mapIdAccTot = new Map<String, Integer>();		
        Map<String, Account> mapAccountUpd = new Map<String, Account>();
        
        //Recorre la lista de reg que se estan actualizando y mete las cuentas asociadas en setAccount();
        Set<String> setAccount = new Set<String>();
        
        for (Order solicitud : newOrderMap.values()){
            if(solicitud.AccountId != null)
                setAccount.add(solicitud.AccountId);
        }
        System.debug('En cuentaSolicitudesAutorizadas setAccount: ' + setAccount);
        
        for (AggregateResult ar : [SELECT AccountId, COUNT(Id) contador FROM Order  
                                   WHERE AccountId IN :setAccount And Estatus__c = 'Autorizada' 
                                   GROUP BY AccountId ORDER BY AccountId]){
                                       
                                       Integer iContador = (Integer) ar.get('contador');
                                       String sAccountId = (String) ar.get('AccountId');
                                       System.debug('En cuentaSolicitudesAutorizadas iContador: ' + iContador);
                                       System.debug('En cuentaSolicitudesAutorizadas sAccountId: ' + sAccountId);
                                       
                                       //Ve si tiene algo la variable de sAccountId                        	
                                       if(sAccountId != null )
                                           mapIdAccTot.put(sAccountId, iContador);
                                   }//Fin del for para Order
        System.debug('En cuentaSolicitudesAutorizadas mapIdAccTot: ' + mapIdAccTot);
        
        //Ya tienes a los clientes que tienen solicitudes termoinadas
        for(String sIdAccount : setAccount){
            //Ve si existe en mapIdAccTot.KeySet() 
            if (!mapIdAccTot.containsKey(sIdAccount))
                mapIdAccTot.put(sIdAccount, 0);        		
        }
        System.debug('En cuentaSolicitudesAutorizadas mapIdAccTot2: ' + mapIdAccTot);
        
        //Ya tienes las cuentas que vas a actualizar, crea los objetos 
        for (String sIdAccount : mapIdAccTot.KeySet()){
            mapAccountUpd.put(sIdAccount, new Account(
                ID = sIdAccount, TAM_NoSolicitudesAutorizadas__c = String.valueOf(mapIdAccTot.get(sIdAccount)) 
            )
                             );
        } //Fin del for para mapIdAccTot
        System.debug('En cuentaSolicitudesAutorizadas mapAccountUpd: ' + mapAccountUpd);
        
        //Actualiza
        update mapAccountUpd.values();
        
    }
    
    public static void crearResumen(List<Order> pedidos){
        List<String> dealer = new  List<String>();
        List<String> mes    = new  List<String>();
        List<String> anio 	= new  List<String>();
        List<ResumenMensualPR__c> resumenDealer = new  List<ResumenMensualPR__c>();
        List<Order> orderCount = new List<Order>();
        for(Order orderAutorizadas : pedidos){
            if(orderAutorizadas.isCounted__c == false && orderAutorizadas.Estatus__c == 'Autorizada' && orderAutorizadas.FechaAutorizacion__c != null){
                Order orderCountUpd = new Order();
                orderCountUpd.id = orderAutorizadas.id;
                orderCountUpd.isCounted__c = true;
                string fechaAutorizacion = string.valueOfGmt(orderAutorizadas.FechaAutorizacion__c);
                list<String> splitDate = fechaAutorizacion.split('-');
                dealer.add(orderAutorizadas.AccountId); 
                mes.add(splitDate[1]);
                anio.add(splitDate[0]);
                orderCount.add(orderCountUpd); 
            }
        }
        
        system.debug('delaer'+dealer);
        if(!dealer.isEmpty() && !mes.isEmpty() && !anio.isEmpty()){
            resumenDealer = [select id,Distribuidor__c,FechaInicio__c,MesSolicitud__c,Anio__c,ContadorAutorizadas__c  FROM ResumenMensualPR__c WHERE Distribuidor__c IN : dealer AND MesSolicitud__c IN : mes AND Anio__c IN : anio];	    
            
        }
        List<ResumenMensualPR__c> listResumenMensual =  new List<ResumenMensualPR__c>(); 
        system.debug('registro de resumen'+resumenDealer);
        if(!resumenDealer.isEmpty()){
            for(ResumenMensualPR__c resumenDealerPR : resumenDealer){
                resumenDealerPR.ContadorAutorizadas__c += 1;     
                listResumenMensual.add(resumenDealerPR);                            
                
            }
        }
        boolean isSuccess = false;
        if(!listResumenMensual.isEmpty()){
            Database.SaveResult [] updateResult = Database.update(listResumenMensual,false);
            for(Database.SaveResult r : updateResult){
                if(r.isSuccess()){
                    isSuccess = true;
                }
                
            }
            if(isSuccess == true){
                update orderCount;
                
            }
            
        }
        
    }
    
}