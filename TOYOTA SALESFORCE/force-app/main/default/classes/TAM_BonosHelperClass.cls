public class TAM_BonosHelperClass {
    
    //Variables Públicas
    public static Set<String> setIdExternoModelos {get;set;}
    public static Map<String, CatalogoCentralizadoModelos__c> mapCatalogoCentralizado {get;set;}
    public static Map<String, InventarioPiso__c> mapInventarioPiso {get;set;}

    /*After Insert*/
    public static void afterInsert(List<TAM_Bonos__c> lstNewBono){
        
        //Obtener información de Catálogo Centralizado de Modelos.
        getCatalogoCentralizado();
        
        for(TAM_Bonos__c objBono : lstNewBono){
            
            //Obtener información de lista de precios e inventario G
            getInventario(objBono.TAM_InicioVigencia__c);
            
            //Guardar información
            setDetalleBono(objBono.Id);
        }
    }
    
    /*Insertar registros de Detalle Bono*/
    public static void setDetalleBono(String recordId){
        
        String strCodigoModelo = '';
        Double intMargenTMEX_Porcentaje;
        Integer intMargenTMEX_Pesos;
        Integer intMSRP;
        Integer intDiasInventario;
        Integer intCantidadInventario;
        Date datFechaInventario;
        String incentivoAnterior;
        
        List<TAM_DetalleBonos__c> lstDetalleBono = new List<TAM_DetalleBonos__c>();
        
        for(String strIdModelos : setIdExternoModelos){
            strIdModelos= strIdModelos.toUpperCase();
            TAM_DetalleBonos__c objDetalleBono = new TAM_DetalleBonos__c();
            
            //Validación de mapa de inventarios
            if(mapInventarioPiso.containsKey(strIdModelos)){
                intCantidadInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).Cantidad__c);
                intDiasInventario = Integer.valueOf(mapInventarioPiso.get(strIdModelos).DiasVenta__c);
                datFechaInventario = Date.valueOf(mapInventarioPiso.get(strIdModelos).FechaInventario__c);
            } else {
                intCantidadInventario = 0;
                intDiasInventario = 0;
                datFechaInventario = null;
            }
            

            //Llenado de lista.
            /** Nombre     */	objDetalleBono.Name = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + '-'+ String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + '-'+ String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c)+ '-'+ recordId;
            /** Bono	   */	objDetalleBono.Bono__c = recordId;
            /** Año Modelo */   objDetalleBono.TAM_AnioModelo__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c);
           	/** Serie	   */	objDetalleBono.TAM_Serie__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c);
            /** Modelo     */   objDetalleBono.TAM_Clave__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c);
            /** Versión    */   objDetalleBono.TAM_Version__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Version__c);
            /** Inventario */	objDetalleBono.TAM_InventarioDealer__c = intCantidadInventario;
            /** Dias Vta.  */	objDetalleBono.TAM_DiasVenta__c = intDiasInventario;
            /** Id Externo */   objDetalleBono.TAM_IdExterno__c = String.valueOf(mapCatalogoCentralizado.get(strIdModelos).AnioModelo__c) + '-'+ String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Serie__c) + '-'+ String.valueOf(mapCatalogoCentralizado.get(strIdModelos).Modelo__c)+ '-'+ recordId;
            
            lstDetalleBono.add(objDetalleBono);
        }
        
        try {
            Database.insert(lstDetalleBono, false);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        
    }
    
    /*Obtener Modelos del Catalogo Centralizado*/
    public static void getCatalogoCentralizado(){
        String idExterno = '';
        setIdExternoModelos = new Set<String>();
        mapCatalogoCentralizado = new Map<String, CatalogoCentralizadoModelos__c>();
        List<CatalogoCentralizadoModelos__c> lstCatalogoCentralizado = [SELECT  Modelo__c,
                                                                                AnioModelo__c,
                                                                                Serie__c,
                                                                                Disponible__c,
                                                                                Version__c,
                                                                                Serie_AnioModelo__c
                                                                        FROM    CatalogoCentralizadoModelos__c];
        
        for(CatalogoCentralizadoModelos__c objCatalogo : lstCatalogoCentralizado){
            idExterno = objCatalogo.Serie__c + objCatalogo.Modelo__c +  objCatalogo.AnioModelo__c;
            idExterno = idExterno.toUpperCase();
            setIdExternoModelos.add(idExterno);
            mapCatalogoCentralizado.put(idExterno,objCatalogo);
        }
        
    }
    
    /*Obtener Inventario G*/
    public static void getInventario(Date dateFechaInventario){
        
        Boolean boolCargaHistorica = false;
        String idExternoInventario ='';
        String idExternoInventario_CatalogoModelos ='';
        Decimal intCantidadTotal;
        Decimal intDiasInventarioAux;
        Integer intDiasInventarioTotal;
        mapInventarioPiso = new Map<String, InventarioPiso__c>();
            
        List<InventarioPiso__c> lstInventarioPiso = [SELECT	 AnioModelo__c,
                                                             Modelo__c,
                                                             Serie__c,
                                                             Version__c,
                                                             Cantidad__c,
                                                             DiasVenta__c,
                                                             Serie_AnioModelo__c,
                                                             FechaInventario__c
                                                     FROM    InventarioPiso__c
                                                     WHERE   FechaInventario__c =:dateFechaInventario];
            
            
            if(!lstInventarioPiso.isEmpty()){
                for(InventarioPiso__c objInventario : lstInventarioPiso){
                    
                    //Llenado de Inventario
                    idExternoInventario = objInventario.Serie__c + objInventario.Modelo__c +  objInventario.AnioModelo__c;
                    idExternoInventario = idExternoInventario.toUpperCase();
    
                    if(mapInventarioPiso.containsKey(idExternoInventario)){
                        intCantidadTotal =  mapInventarioPiso.get(idExternoInventario).Cantidad__c + objInventario.Cantidad__c;
                        objInventario.Cantidad__c = intCantidadTotal;
    
                        intDiasInventarioAux = mapInventarioPiso.get(idExternoInventario).DiasVenta__c;
                        intDiasInventarioTotal = Integer.valueOf(intDiasInventarioAux.round(System.RoundingMode.HALF_UP));
    
                        if(objInventario.DiasVenta__c < intDiasInventarioTotal){
                            objInventario.DiasVenta__c = intDiasInventarioTotal;
                        }
                    }
                    mapInventarioPiso.put(idExternoInventario,objInventario);
                }
            }
    }  

}