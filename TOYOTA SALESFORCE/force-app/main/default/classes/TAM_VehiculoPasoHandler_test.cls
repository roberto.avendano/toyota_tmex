/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba que contiene la logica para probar la clase: TAM_VehiculoPasoHandler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    21-Agosto-2019    Héctor Figueroa             Creación
******************************************************************************* */

@isTest
private class TAM_VehiculoPasoHandler_test {

	static	Account clientePrueba = new Account();
	static	List<Opportunity> opp = new List<Opportunity>();	
	static	List<Venta__c> ventasH = new List<Venta__c>();
	static	List<TAM_VehiculoPaso__c> vehiculos = new List<TAM_VehiculoPaso__c>();
  	static  String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId(); 	
	static  String VaRtStdPriceBookIdReg = Test.getStandardPricebookId();   
		    
	@TestSetup static void loadData(){
		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cuenta;
	}

	public static void LoadVentasH(){
		System.debug('EN test_one LoadVentasH...');
		clientePrueba = [Select Id, Name, Codigo_Distribuidor__c, UnidadesAutosDemoAutorizadas__c From Account LIMIT 1];
		System.debug('EN test_one LoadVentasH clientePrueba: ' + clientePrueba);
				
		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};

		String sTAMTipoCliente = 'Primero';
		for(Integer i=0; i < 10; i++){			
		 	String sufix = i < 10 ? '0'+String.valueOf(i):String.valueOf(i);
		
	       	//Crea el objeto del tipo Opportunity
			opp.add(new Opportunity( 
				Name = 'JTDKBRFUXH30310'+sufix,
				TAM_Vin__c = 'JTDKBRFUXH30310'+sufix,			
				CloseDate = Date.today(),
				StageName = 'Closed Won',
				Pricebook2Id = VaRtStdPriceBookIdReg,
				Amount = 1.0,
				TAM_IdExterno__c = 'JTDKBRFUXH30310'+sufix,
				recordTypeId = VaRtOppRegVCrm,
				AccountId = clientePrueba.id
				)
			);
			
			//Crea el objeto para la venta					  
		 	ventasH.add(new Venta__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),            		
        		IdModelo__c = modelos.get(aleatorio(modelos.size())),
        		YearModelo__c = '2018',
        		IdColorExt__c = colores.get(aleatorio(colores.size())),
        		DescrColorExt__c = 'External color description',
        		IdColorInt__c = colores.get(aleatorio(colores.size())),
        		DescrColorInt__c = 'Internal color description',
    			Serial__c = seriales.get(aleatorio(seriales.size())),
    			SaleCode__c = sufix,
    			SubmittedDate__c = Datetime.newInstance(2017, 10, 13),
        		IdExterno__c = 'JTDKBRFUXH30310'+sufix+'-'+String.valueOf(Date.today()),
        		TAM_TipoCliente__c = sTAMTipoCliente,
        		FirstName__c = 'PruebaFN',
        		LastName__c = 'PruebaLN'
	 		));
	 		//Cambia el estatus
	 		if (sTAMTipoCliente == 'Primero')
				sTAMTipoCliente = 'Segundo';
	 		else if (sTAMTipoCliente == 'Segundo')
				sTAMTipoCliente = 'Primero';

			//Crea los registros del tipo TAM_VehiculoPaso__c
			vehiculos.add(new TAM_VehiculoPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),
					TAM_IdModelo__c = modelos.get(aleatorio(modelos.size())),
					TAM_Serial__c = seriales.get(aleatorio(seriales.size())),
					TAM_YearModelo__c = '2018',
					TAM_TransType__c = 'RDR',
					TAM_Factura__c = '123456789',
					TAM_SaleCode__c = '1',
					TAM_IdColorExt__c = '03R3',
					TAM_DescrColorExt__c = 'BARCELONA RED MET',
					TAM_IdColorInt__c = 'EA20',
					TAM_DescrColorInt__c = 'EA20',
					TAM_SaleDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedTime__c = '19.05.30.077173',
					TAM_SalesManager__c = '169621364',
					TAM_lsAssesor__c = '',
					TAM_FLEET__c = 'N',
					TAM_Year__c = '2018',
					TAM_Month__c = '6'
				)
			);				
		}		
				
		System.debug(JSON.serialize(ventasH));		
		System.debug(JSON.serialize(vehiculos));		
		//return ventasH;
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

	@isTest static void test_one() {
		
		//Crea la Data para las pruebas
		TAM_VehiculoPasoHandler_test.LoadVentasH();

		List<Opportunity> oppPaso = opp;		
		List<Venta__c> ventasPaso = ventasH;
		List<TAM_VehiculoPaso__c> vehiculosPaso = vehiculos;

    	System.debug('EN test_one oppPaso: ' + oppPaso);
		System.debug('EN test_one ventasPaso: ' + ventasPaso);
		System.debug('EN test_one vehiculosPaso: ' + vehiculosPaso);		

		insert oppPaso;		
		insert ventasPaso;
		insert vehiculosPaso;
				
		//Llama al metodo de ConvierteFecha
		TAM_VehiculoPasoHandler objTAM_VehiculoPasoHandler = new TAM_VehiculoPasoHandler();
		Time horaAct = Time.newInstance(4,26,18,0);
		objTAM_VehiculoPasoHandler.ConvierteFecha(String.valueOf('12-10-2020'));
	}
    
    
}