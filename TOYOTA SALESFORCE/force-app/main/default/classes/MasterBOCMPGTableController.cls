public with sharing class MasterBOCMPGTableController {

	public transient MasterBO__c master;
	public transient List<PartNoCMPG__c> listaCampanas{get;set;}
	
	public MasterBOCMPGTableController(ApexPages.StandardController controlador){
		this.master= (MasterBO__c)controlador.getRecord();
		listaCampanas=[SELECT Campaign__r.Name, Campaign__r.CodigoAccionServicio__c, Name, Id, PartDescription__c, PartNumber__c, ReleaseActionPRA__c, ReleaseActionDealer__c, ServiceActionDescription__c 
		FROM PartNoCMPG__c 
		WHERE PartNumber__c =: master.PartNameId__c];	
	}
}