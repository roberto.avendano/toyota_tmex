/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para colaborar los registris que vienen de la clases
                        TAM_AdminVisibInventarioToyota.bpCreaColaboracion.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    02-Junio-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CreaColaborInventDefDealerBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sDealerColabora;
    
    //Un constructor por default
    global TAM_CreaColaborInventDefDealerBch_cls(string query, String sDealerColaboraParam){
        this.query = query;
        this.sDealerColabora = sDealerColaboraParam;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaborInventDefDealerBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_CreaColaborInventDefDealerBch_cls sDealerColabora: ' + this.sDealerColabora);

        Set<String> setVodDealer = new Set<String>();
        Map<String, String> mapIdUsrNomDis = new Map<String, String>();
        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        Map<String, String> mapNoDealerNomGrupo = new Map<String, String>();
        List<TAM_InventarioVehiculosToyota__Share >  lInvenVehiculoShare = new List<TAM_InventarioVehiculosToyota__Share >();
        Map<String, set<String>> mapNoDelSelNomDeaDest = new Map<String, set<String>>();
         
        Set<String> setNomDealerOrigen = new Set<String>();
        Set<String> setNomDealer = new Set<String>();
        
        String sFleterosColaboracionEspecial = System.Label.TAM_FleterosColaboracionEspecial;
        String sCuernavacaColaboracionEspecial = System.Label.TAM_CuernavacaColaboracionEspecial;
                
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvVhToy : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvVhToy.Dealer_Code__c != null)
                setVodDealer.add(objInvVhToy.Dealer_Code__c);
        }//Fin del for para scope
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls setVodDealer: ' + setVodDealer + ' sDealerColabora: ' + this.sDealerColabora);           

        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Codigo_Distribuidor__c =:sDealerColabora ORDER BY NAME]){
            String sName = Dealer.Name;    
            mapNoDealerNomGrupo.put(Dealer.Codigo_Distribuidor__c, sName.toUpperCase());
        }
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.KeySet());           
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.Values());           
                
        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group WHERE (NAME LIKE '%TOYOTA%' OR NAME LIKE '%TMEX%') ORDER BY NAME]){
            String sNombreGrupo = Gropo.Name;
            mapNomGrupoIdGrupo.put(sNombreGrupo.toUpperCase(), Gropo.id);
            sNombreGrupo = Gropo.Name.toUpperCase() + ' ASESOR';
            mapNomGrupoIdGrupo.put(sNombreGrupo, Gropo.id);
        }
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.KeySet());           
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           

        //Crea la colaboracion para el reg 
        for(TAM_InventarioVehiculosToyota__c objInvVehToy : scope){
            //System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls Dealer_Code__c: ' + objInvenFyG.Dealer_Code__c);            
            String sNombreGrupo = mapNoDealerNomGrupo.containsKey(objInvVehToy.Dealer_Code__c) ? mapNoDealerNomGrupo.get(objInvVehToy.Dealer_Code__c) : ' ';
            String sDealerPaso = sNombreGrupo.toUpperCase();
            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
            TAM_InventarioVehiculosToyota__Share objPasoShare = new TAM_InventarioVehiculosToyota__Share (
                ParentId = objInvVehToy.id, UserOrGroupId = mapNomGrupoIdGrupo.get(sDealerPaso), AccessLevel = 'Edit');
            //System.debug('ENTRO TAM_InventarioVehiculosToyotaTgrHandler sDealerPaso3 : ' + sDealerPaso + ' objPasoShare: ' + objPasoShare);
            //Agregalo a la lista de lInvenVehiculoShare
            lInvenVehiculoShare.add(objPasoShare);                                 
    
            sDealerPaso = sNombreGrupo.toUpperCase() + ' ASESOR';
            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
            TAM_InventarioVehiculosToyota__Share objPasoShareAsor = new TAM_InventarioVehiculosToyota__Share (
            ParentId = objInvVehToy.id, UserOrGroupId = mapNomGrupoIdGrupo.get(sDealerPaso), AccessLevel = 'Edit');                     
            //System.debug('ENTRO TAM_InventarioVehiculosToyotaTgrHandler sDealerPaso4 : ' + sDealerPaso + ' objPasoShareAsor: ' + objPasoShareAsor);
            //Agregalo a la lista de lInvenVehiculoShare
            lInvenVehiculoShare.add(objPasoShareAsor);        
        }//Fin del for para el scope
        System.debug('ENTRO TAM_CreaColaborInventDefDealerBch_cls lInvenVehiculoShare IdNomdis: ' + lInvenVehiculoShare);           
                
        Boolean blnError = false;
        Set<String> setIdOkCol = new Set<String>();
        
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();
        
        if (!lInvenVehiculoShare.isEmpty()){
            //Actualiza las Opp 
            List<Database.Saveresult> lDtbUpsRes = Database.insert(lInvenVehiculoShare, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()

        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);

    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaborInventDefDealerBch_cls.finish Hora: ' + DateTime.now());
        List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerDel = new List<TAM_AdminInventarioDealer__c>();
        
        //Toma el no de dis que viene en this.sDealerColabora y consulta todo lo que haya en TAM_AdminInventarioDealer__c 
        for (TAM_AdminInventarioDealer__c objAdminInvDealer : [Select t.TAM_IdExterno__c, t.TAM_DistribuidorAsociado__c, t.TAM_CodigoDistribuidor__c, 
            t.TAM_AplicaColarobarionDefault__c, t.CreatedDate From TAM_AdminInventarioDealer__c t 
            Where TAM_CodigoDistribuidor__c =: this.sDealerColabora ]){
            //Llena la lista de lAdminInventarioDealerDel
            lAdminInventarioDealerDel.add(objAdminInvDealer);
        }
        System.debug('EN TAM_CreaColaborInventDefDealerBch_cls.finish lAdminInventarioDealerDel: ' + lAdminInventarioDealerDel);
        
        //Elimina lAdminInventarioDealerDel
        if (!lAdminInventarioDealerDel.isEmpty())
            delete lAdminInventarioDealerDel;
        
    } 
    
}