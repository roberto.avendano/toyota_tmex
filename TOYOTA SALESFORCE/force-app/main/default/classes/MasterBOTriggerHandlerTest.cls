@isTest
private class MasterBOTriggerHandlerTest{
	
	@isTest
	static void itShould(){
		//Datos para prueba
		MasterBO__c mbo1 = new MasterBO__c();
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
			User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;


        Contact contacto= new Contact();
        contacto.IdExterno__c='ANGELALYLE-ANGELA.D.LYLE@TOYOTA.COM';
        contacto.Codigo__c='5J';
        contacto.FirstName='ANGELA';
        contacto.LastName='LYLE';
        contacto.Email='ANGELA.D.LYLE@TOYOTA.COM';
        contacto.Supervisor__c='JAY THOMPSON';
        contacto.Gerente__c='GINA FANELLI';
        contacto.UsuarioPRAId__c= u.Id;
        insert contacto;


        Account cuenta = new Account();
        cuenta.Name='Toyota López Mateos';
        cuenta.Codigo_Distribuidor__c='57001';
        cuenta.Telefono_Oficina_1__c='3338846061';
        cuenta.District_Manager__c='Leonel Miranda';
        cuenta.Dia__c=1;
        cuenta.Grupo__c='GM';
        cuenta.PRAId__c=contacto.Id;
        cuenta.DSPMId__c=contacto.Id;
        cuenta.FTSId__c=contacto.Id;
        cuenta.PRAId__c=contacto.Id;
        cuenta.Promotor_Kaizen__c=u.Id;	
        insert cuenta;

        Modelo_PDC__c modelo= new Modelo_PDC__c();
        modelo.MdlCd__c='541W';
        modelo.Name='541W';
        modelo.Model__c='RAV4';
        modelo.MdlDesc__c='RAV4 (USA, CANADA, P.RICO)';
        insert modelo;

        Product2 prod= new Product2();
        prod.Name='676100R090C2';
        prod.Description='PANEL ASSY, FR DOOR TRIM, RH';
        prod.ProductCode='676100R090C2';
        prod.IdExternoProducto__c='676100R090C2';
        prod.IsActive=!true;
        insert prod;




        PDCXsInt__c newPDCXsInt = new PDCXsInt__c();
        newPDCXsInt.EtaDte__c = Date.newInstance(2016, 3, 15);
        newPDCXsInt.IdExternoPDCXsInt__c='174100V050XS8548USXX401464';
        newPDCXsInt.PartNameId__c = prod.Id;
        insert newPDCXsInt;

		BOPCPivot__c pivot = new BOPCPivot__c();
		pivot.IdExterno__c='676100R090C2-XS614112, XS614415, XS614516, XS614610, XS614709-01/06/2016';
		pivot.Name='676100R090C2';
		pivot.PartNameId__c=prod.Id;
		pivot.OrdPCNum__c=5;
		pivot.OrdPCNum2__c='XS614112, XS614415, XS614516, XS614610, XS614709';
		pivot.ItemNum__c='0703, 0732, 0746, 0761, 0971';
		pivot.EtaDte__c= Date.newInstance(2016, 5, 31);
		pivot.EtaDte2__c= Date.newInstance(2016, 6, 3);
		pivot.PlanShpQty__c=1;
		pivot.PlanShpQty2__c=3;
		pivot.PlanShpQty3__c=10;
		pivot.FPDD__c= Date.newInstance(2016, 6, 1);
        insert pivot;

		RecordType recordMasterBOID= [SELECT ID from RecordType WHERE DeveloperName='MasterBO'];

		mbo1.SalidaTentativaPDC__c= Date.newInstance(2016, 6, 13);
		mbo1.Categoria__c='Sin categoría';
		mbo1.AnalystNameId__c=contacto.Id;
		mbo1.PRAbyAnalystId__c=contacto.Id;
		mbo1.DSPMId__c=contacto.Id;
		mbo1.FTSId__c=contacto.Id;
		mbo1.PRAId__c=contacto.Id;
		mbo1.DealerId__c=cuenta.Id;
		mbo1.OrderReference__c='XS7824RP';

		mbo1.TipoOrdenBO__c='S';
		mbo1.MdlCdId__c=modelo.Id;
		mbo1.OrderDate__c= Date.newInstance(2016, 5, 26);
		mbo1.BOQTYDealerOrder__c=1;
		mbo1.CountOrder__c=2;
		mbo1.TotalBOQty__c=2;

		mbo1.Origin__c='CA';
		mbo1.Warehouse__c='T';
		mbo1.Status__c='Active';

		mbo1.PDCOnHandQty__c=0;
		mbo1.PDCInTransitQty__c=0;
		mbo1.PDCOIL__c=5;
		mbo1.PDCDmdMonth__c=1.60;
		mbo1.PC_OH_Qty__c=0.000;
		mbo1.PCInTransitQty__c=2;
		mbo1.PCOilQty__c=2;
		mbo1.PCDmdMonthQty__c=11.20;
		mbo1.BOPCPivotId__c=pivot.Id;
		mbo1.PartNameId__c=prod.Id;

		
		Test.startTest();
        insert mbo1;

        mbo1.Status__c='Inactive';
        update mbo1;
   
		Test.stopTest();

	}
}