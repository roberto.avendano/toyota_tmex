/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_Movimientos__c
                        del la fecha actual.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Jun-2021          Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_ConsMovDDSolInvBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    global Date dtFechaIniConsPrm;
    global Date dtFechaFinConsPrm;
    
    //Un constructor por default
    global TAM_ConsMovDDSolInvBch_cls(string query, Date dtFechaIniConsPrm, Date dtFechaFinConsPrm){
        this.query = query;
        this.dtFechaIniConsPrm = dtFechaIniConsPrm;
        this.dtFechaFinConsPrm = dtFechaFinConsPrm;        
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ConsMovDDSolInvBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Movimiento__c> scope){ //List<Movimiento__c>
        System.debug('EN TAM_ConsMovDDSolInvBch_cls.');

        System.debug('EN TAM_ConsMovDDSolInvBch_cls this.dtFechaIniConsPrm: ' + this.dtFechaIniConsPrm);
        System.debug('EN TAM_ConsMovDDSolInvBch_cls this.dtFechaFinConsPrm: ' + this.dtFechaFinConsPrm);

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        String sFleet = 'N';

        String strTipoPedido = 'Inventario';
        String strVinPrueba = '5TDGZRAH0MS049120';
        String EstatusDOD = 'Cancelado';
        String EstDealerSol = 'Cancelada';
        String EstDealerSol2 = 'Rechazada';
                      
        //Un Objero para el error en caso de exista
        Map<String, TAM_LogsErrores__c> mapLogErrores = new Map<String, TAM_LogsErrores__c>();
        String sHabilitaMergeAccounts = System.Label.TAM_FechaMesToyota;                
        Set<String> setVinMov = new Set<String>();
        List<Movimiento__c> lCheckOut = new List<Movimiento__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdExtObjCheckoutSolCons = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_MovimientosSolicitudes__c> mapIdExtObjMovSolUps = new Map<String, TAM_MovimientosSolicitudes__c>();
        Map<String, Map<Date, TAM_MovimientosSolicitudes__c>> MapVinFechaMovObjMovSol = new Map<String, Map<Date, TAM_MovimientosSolicitudes__c>>();
        Map<String, Map<Date, List<Movimiento__c>>> MapVinFechaMovObjMov = new Map<String, Map<Date, List<Movimiento__c>>>();
        Set<Date> setFechaSubmite = new Set<Date>();

        Date dtFechaActual = Date.today();
                
        //Recorre el mapa de mapIdVinReg 
        for (Movimiento__c objMovConsFin : scope){
        
	        String queryCheckout = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
	        queryCheckout += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_NombreDistribuidor__c ';
	        queryCheckout += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
	        queryCheckout += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
	        queryCheckout += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(objMovConsFin.VIN__r.Name) + '\'';
	        queryCheckout += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';

            queryCheckout += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c != null';
            queryCheckout += ' And TAM_EstatusDOD__c != \'' + String.escapeSingleQuotes(EstatusDOD) + '\'';
            queryCheckout += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol) + '\'';
            queryCheckout += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol2) + '\'';

	        queryCheckout += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';
	        //queryCheckout += ' Limit 5';
            
            //Ees una prueba
            if (Test.isRunningTest()){    
	            queryCheckout = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
	            queryCheckout += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_NombreDistribuidor__c ';
	            queryCheckout += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
	            queryCheckout += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
	            queryCheckout += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';
                queryCheckout += ' Limit 1';
            }
            System.debug('EN TAM_ConsMovDDSolInvBch_cls queryCheckout: ' + queryCheckout);
            
            //Busca los movimientos    
            List<sObject> sObjLstMov = Database.query(queryCheckout);
            
            //Metelo al mapa de 
            //Recorre la lista de los mov por vin y metelos al mapa de mapIdExtObjCheckoutSolCons
            for (sObject objsObjectPaso : sObjLstMov){
                TAM_CheckOutDetalleSolicitudCompra__c objMovPaso = (TAM_CheckOutDetalleSolicitudCompra__c) objsObjectPaso;
                mapIdExtObjCheckoutSolCons.put(objMovPaso.TAM_VIN__c, objMovPaso);            
            }//Fin del for para sObject 
            System.debug('EN TAM_ConsMovDDSolInvBch_cls mapIdExtObjCheckoutSolCons: ' + mapIdExtObjCheckoutSolCons.keyset());
            System.debug('EN TAM_ConsMovDDSolInvBch_cls mapIdExtObjCheckoutSolCons: ' + mapIdExtObjCheckoutSolCons.values());
            
            for (Movimiento__c objMovPaso : [Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c,
                    Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, 
                    Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c
                    From Movimiento__c Where RecordTypeId =:strRecordTypeId 
                    And TAM_FechaEnvForm__c >=: this.dtFechaIniConsPrm
                    And TAM_FechaEnvForm__c <=: this.dtFechaFinConsPrm                    
                    //And Fleet__c =:sFleet 
                    And VIN__r.Name IN: mapIdExtObjCheckoutSolCons.keyset() Order by Submitted_Date__c DESC]){
                System.debug('EN TAM_ConsMovDDSolInvBch_cls objMovPaso: ' + objMovPaso);
                //Agrega la fecha de envio del mov al set de setFechaSubmite 
                setFechaSubmite.add(objMovPaso.TAM_FechaEnvForm__c);
                //Ve si existe en MapVinFechaMovObjMov
                if (MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name)){
                    if (MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c)){
                        List<Movimiento__c> lMovPaso = MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).get(objMovPaso.TAM_FechaEnvForm__c);
                        //Agrega a la lista el nuevo mov
                        lMovPaso.add(objMovPaso);
                        //Actualiza el mapa de la posición MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name)
                        MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).put(objMovPaso.TAM_FechaEnvForm__c, lMovPaso);
                    }//Fin si MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c)
                    if (!MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).containsKey(objMovPaso.TAM_FechaEnvForm__c))
                        MapVinFechaMovObjMov.get(objMovPaso.VIN__r.Name).put(objMovPaso.TAM_FechaEnvForm__c, new List<Movimiento__c>{objMovPaso});
                }//Fin si MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name)
                //No exite el objMovPaso.VIN__r.Name en MapVinFechaMovObjMov
                if (!MapVinFechaMovObjMov.containsKey(objMovPaso.VIN__r.Name))
                   MapVinFechaMovObjMov.put(objMovPaso.VIN__r.Name, new Map<Date, List<Movimiento__c>>{objMovPaso.TAM_FechaEnvForm__c => new List<Movimiento__c>{objMovPaso}});
            }//Fin del for para Movimiento__c
            System.debug('EN TAM_ConsMovDDSolInvBch_cls setFechaSubmite: ' + setFechaSubmite);
            System.debug('EN TAM_ConsMovDDSolInvBch_cls MapVinFechaMovObjMov: ' + MapVinFechaMovObjMov.KeySet());
            System.debug('EN TAM_ConsMovDDSolInvBch_cls MapVinFechaMovObjMov: ' + MapVinFechaMovObjMov.Values());            
        }//Fin del for para la lista de TAM_CheckOutDetalleSolicitudCompra__c

        //Recorre la lista del mapa mapIdExtObjCheckoutSolCons
        for (String sVinCheckoutPaso : mapIdExtObjCheckoutSolCons.keyset()){
            System.debug('EN TAM_ConsMovDDSolInvBch_cls sVinCheckoutPaso: ' + sVinCheckoutPaso);
            TAM_CheckOutDetalleSolicitudCompra__c objCheckoutPasoCons = mapIdExtObjCheckoutSolCons.get(sVinCheckoutPaso);
            System.debug('EN TAM_ConsMovDDSolInvBch_cls objCheckoutPasoCons: ' + objCheckoutPasoCons);

            //Ya tienes todos los mov del objMovConsFin.TAM_VIN__c busca los registros en la table de TAM_MovimientosSolicitudes__c
            for (TAM_MovimientosSolicitudes__c objMovSolPaso : [Select t.TAM_VIN__c, 
                t.TAM_UltimoMovimientoDD__c, 
                t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
                From TAM_MovimientosSolicitudes__c t Where TAM_VIN__c =:objCheckoutPasoCons.TAM_VIN__c ]){
                //Mete los mov al mapa de MapVinFechaMovObjMovSol
                if (MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)){
                    //Agregarlo al mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                    MapVinFechaMovObjMovSol.get(objMovSolPaso.TAM_VIN__c).put(objMovSolPaso.TAM_FechaMovimiento__c, objMovSolPaso);
                }//Fin si MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c)
                //No exite el objMovSolPaso.TAM_VIN__c en MapVinFechaMovObjMovSol
                if (!MapVinFechaMovObjMovSol.containsKey(objMovSolPaso.TAM_VIN__c))
                   MapVinFechaMovObjMovSol.put(objMovSolPaso.TAM_VIN__c, new Map<Date, TAM_MovimientosSolicitudes__c>{objMovSolPaso.TAM_FechaMovimiento__c => objMovSolPaso});                
            }//Fin del for para TAM_MovimientosSolicitudes__c
            System.debug('EN TAM_ConsMovDDSolInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.KeySet());
            System.debug('EN TAM_ConsMovDDSolInvBch_cls MapVinFechaMovObjMovSol: ' + MapVinFechaMovObjMovSol.Values());
            
            //Ve si el vin tiene mov en el mapa de MapVinFechaMovObjMov
            if (MapVinFechaMovObjMov.containsKey(objCheckoutPasoCons.TAM_VIN__c)){
                System.debug('EN TAM_ConsMovDDSolInvBch_cls CONTIENE EL VIN TAM_VIN__c: ' + objCheckoutPasoCons.TAM_VIN__c);
                Map<Date, List<Movimiento__c>> mapVinPasoMov = MapVinFechaMovObjMov.get(objCheckoutPasoCons.TAM_VIN__c);  
                //Muy bien ahora recorre la lista de fechas del mapa MapVinFechaMovObjMov
                for (Date objFechaMovPaso : mapVinPasoMov.KeySet()){
                    System.debug('EN TAM_ConsMovDDSolInvBch_cls objFechaMovPaso: ' + objFechaMovPaso);
                    //Busca la fecha objFechaMovPaso en el mapa de MapVinFechaMovObjMovSol
                    if (MapVinFechaMovObjMovSol.containsKey(objCheckoutPasoCons.TAM_VIN__c)){
                        //Toma la lista de mov 
                        if (mapVinPasoMov.containsKey(objFechaMovPaso)){
                            List<Movimiento__c> lMovCons = mapVinPasoMov.get(objFechaMovPaso);
                            System.debug('EN TAM_ConsMovDDSolInvBch_cls lMovCons: ' + lMovCons);
                            //Ya tienes la fecha adecuada entonces busca sus mov y obten el ultimo
                            Movimiento__c objMovInv = TAM_ActTotVtaInvBch_cls.getUltMov(lMovCons);
                            System.debug('EN TAM_ConsMovDDSolInvBch_cls objMovInv: ' + objMovInv);
                            //Solo tiene un mov
                            if (lMovCons.size() == 1)
                                objMovInv = lMovCons.get(0);
                            System.debug('EN TAM_ConsMovDDSolInvBch_cls objMovInv: ' + objMovInv);
                            Integer dSuma = 0;
                            //Ya tienes el ultimo mov ahora haz la suma aritmetica de los mov
                            for (Movimiento__c obMovPasoSum : lMovCons)
                                dSuma += Integer.valueOf(obMovPasoSum.Qty__c != null && obMovPasoSum.Qty__c != '' ? Integer.valueOf(obMovPasoSum.Qty__c) : 0);
                            System.debug('EN TAM_ConsMovDDSolInvBch_cls dSuma: ' + dSuma);
                            //Ve si existe la fecha en el mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                            if (MapVinFechaMovObjMovSol.get(objCheckoutPasoCons.TAM_VIN__c).containsKey(objFechaMovPaso)){
                                //Busca el objeto de tipo TAM_MovimientosSolicitudes__c y Actualuzalo
                                TAM_MovimientosSolicitudes__c objMovSolPaso = MapVinFechaMovObjMovSol.get(objCheckoutPasoCons.TAM_VIN__c).get(objFechaMovPaso);
                                TAM_MovimientosSolicitudes__c objMovSolUpd = new TAM_MovimientosSolicitudes__c(
                                    TAM_IdExternoSFDC__c = objMovSolPaso.TAM_IdExternoSFDC__c,
                                    TAM_CheckOutSolicitud__c = objCheckoutPasoCons.id,
                                    TAM_UltimoMovimientoDD__c = objMovInv.id
                                );
                                System.debug('EN TAM_ConsMovDDSolInvBch_cls objMovInv.TAM_CodigoDistribuidorFrm__c: ' + objMovInv.TAM_CodigoDistribuidorFrm__c + ' objCheckoutPasoCons.TAM_NombreDistribuidor__c: ' + objCheckoutPasoCons.TAM_NombreDistribuidor__c);                                
                                //Solo aplica si la solicitud esta autorizada 
                                if (objMovSolPaso.TAM_EstatusSolicitud__c == 'Autorizada'){
                                    if ( objMovSolPaso.TAM_Total__c == 0 || objMovSolPaso.TAM_Total__c == null)
                                        objMovSolUpd.TAM_Total__c = objMovInv.TAM_CodigoDistribuidorFrm__c == objCheckoutPasoCons.TAM_NombreDistribuidor__c ? dSuma : 0;
                                }//Fin si objMovSolPaso.TAM_EstatusSolicitud__c == 'Autorizada'                                
                                System.debug('EN TAM_ConsMovDDSolInvBch_cls existe en Mov Sol objMovSolUpd: ' + objMovSolUpd);                               
                                //Agregalo al mapa de mapIdExtObjMovSolUps
                               mapIdExtObjMovSolUps.put(objMovSolPaso.TAM_IdExternoSFDC__c, objMovSolUpd);
                               mapLogErrores.put(objMovSolPaso.TAM_IdExternoSFDC__c, new TAM_LogsErrores__c( TAM_Proceso__c = 'Tipo Venta Inventario DD Fecha Exist',
                                    TAM_Fecha__c = Date.today(), TAM_idExtReg__c = objMovSolPaso.TAM_IdExternoSFDC__c,
                                    TAM_DetalleError__c = 'En DD Ceckout Obj: ' + String.valueOf(objMovSolUpd) + ''
                                    )
                               );                               
                            }//Fin si MapVinFechaMovObjMovSol.get(objCheckoutPasoCons.TAM_VIN__c).containsKey(objFechaMovPaso)
                            //Ve si existe la fecha en el mapa de los Map<Date, TAM_MovimientosSolicitudes__c>
                            if (!MapVinFechaMovObjMovSol.get(objCheckoutPasoCons.TAM_VIN__c).containsKey(objFechaMovPaso) || Test.isRunningTest()){
                                Date dtFechaCierreSolSfdc = objCheckoutPasoCons.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c;
                                String strDtFechaCheckOut = String.valueOf(objFechaMovPaso);
                                String sIdExterno = objCheckoutPasoCons.TAM_VIN__c + '-' + objCheckoutPasoCons.TAM_SolicitudFlotillaPrograma__r.Name  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                                                
                                System.debug('EN TAM_ConsMovDDSolInvBch_cls existe en Mov Sol sIdExterno: ' + sIdExterno);                               
                                //Busca el objeto de tipo TAM_MovimientosSolicitudes__c y Actualuzalo
                                TAM_MovimientosSolicitudes__c objMovSolUpd = new TAM_MovimientosSolicitudes__c(
                                    Name = objCheckoutPasoCons.TAM_SolicitudFlotillaPrograma__r.Name + ' - ' + strDtFechaCheckOut,
                                    TAM_IdExternoSFDC__c = sIdExterno,
                                    TAM_CheckOutSolicitud__c = objCheckoutPasoCons.id,
                                    TAM_UltimoMovimientoDD__c = objMovInv.id,
                                    TAM_FechaMovimiento__c = objFechaMovPaso,
                                    TAM_VIN__c = objCheckoutPasoCons.TAM_VIN__c                                        
                                );
                                System.debug('EN TAM_ConsMovDDSolInvBch_cls objMovInv.TAM_CodigoDistribuidorFrm__c: ' + objMovInv.TAM_CodigoDistribuidorFrm__c + ' objCheckoutPasoCons.TAM_NombreDistribuidor__c: ' + objCheckoutPasoCons.TAM_NombreDistribuidor__c);                                
                                //Solo aplica si la solicitud esta autorizada 
                                if (objCheckoutPasoCons.TAM_EstatusDealerSolicitud__c == 'Cerrada'){
                                    objMovSolUpd.TAM_Total__c = objMovInv.TAM_CodigoDistribuidorFrm__c == objCheckoutPasoCons.TAM_NombreDistribuidor__c ? dSuma : 0;
			                        objMovSolUpd.TAM_EstatusSolicitud__c = 'Autorizada';
                                }
                                //Ve si la fecha del movimiento es menor a la fecha de cirre de la solicutud
                                if (dtFechaCierreSolSfdc <= objFechaMovPaso){ 
                                    System.debug('EN TAM_ConsMovDDSolInvBch_cls No Existe Fecha y es Post Sol dtFechaCierreSolSfdc:  ' + dtFechaCierreSolSfdc + ' objFechaMovPaso:  ' + objFechaMovPaso + ' objMovSolUpd: ' + objMovSolUpd); 
                                    mapIdExtObjMovSolUps.put(sIdExterno, objMovSolUpd);
                                    mapLogErrores.put(sIdExterno, new TAM_LogsErrores__c( TAM_Proceso__c = 'Tipo Venta Inventario DD No Existe Fecha y es Post',
                                        TAM_Fecha__c = Date.today(), TAM_idExtReg__c = sIdExterno,
                                        TAM_DetalleError__c = 'En DD Ceckout Obj: ' + String.valueOf(objMovSolUpd) + ''
                                        )
                                    );                                
                                }//Fin si dtFechaCierreSolSfdc <= objFechaMovPaso
                                //Si la fecha del mov es menor a la fecha de cierre de la sol
                                if (dtFechaCierreSolSfdc > objFechaMovPaso){
                                    System.debug('EN TAM_ConsMovDDSolInvBch_cls No Existe Fecha y es Ant Sol dtFechaCierreSolSfdc:  ' + dtFechaCierreSolSfdc + ' objFechaMovPaso:  ' + objFechaMovPaso + ' objMovSolUpd: ' + objMovSolUpd); 
				                    mapLogErrores.put(sIdExterno, new TAM_LogsErrores__c( TAM_Proceso__c = 'Tipo Venta Inventario DD No Existe Fecha y es Ant',
				                        TAM_Fecha__c = Date.today(), TAM_idExtReg__c = sIdExterno,
				                        TAM_DetalleError__c = 'En DD Ceckout Obj: ' + String.valueOf(objMovSolUpd) + ''
				                        )
				                    );
                                } //Fin si dtFechaCierreSolSfdc > objFechaMovPaso
                            }//Fin si MapVinFechaMovObjMovSol.get(objCheckoutPasoCons.TAM_VIN__c).containsKey(objFechaMovPaso)
                        }//Fin si mapVinPasoMov.contains(objFechaMovPaso)
                    }//Fin si MapVinFechaMovObjMovSol.containsKey(objCheckoutPasoCons.TAM_VIN__c)
                }//Fin del for para la mapVinPasoMov.KeySet()
            }//Fin si MapVinFechaMovObjMov.containsKey(objCheckoutPasoCons.TAM_VIN__c)

        }//Fin del for para mapIdExtObjCheckoutSolCons.keyset()
        System.debug('EN TAM_ConsMovDDSolInvBch_cls mapCheckOutDDUpsFinal: ' + mapIdExtObjMovSolUps.keySet());
        System.debug('EN TAM_ConsMovDDSolInvBch_cls mapCheckOutDDUpsFinal: ' + mapIdExtObjMovSolUps.values());
        
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdExtObjMovSolUps.isEmpty()){
            Integer cntMov = 0;
            List<TAM_MovimientosSolicitudes__c> lMovSol = mapIdExtObjMovSolUps.values();
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lMovSol, TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ConsMovDDSolInvBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess()){
                    /*mapLogErrores.add(new TAM_LogsErrores__c( TAM_Proceso__c = 'Tipo Venta Inventario DD',
                        TAM_Fecha__c = Date.today(), 
                        TAM_DetalleError__c = 'En DD Ceckout : ' + objDtbUpsRes.getId() + ' Obj: ' + String.valueOf(lMovSol.get(cntMov)) + ''
                        )
                    );*/
                }//Fin si objDtbUpsRes.isSuccess()
                cntMov++;                        
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()
        
        //Roleback a todo
        //Database.rollback(sp);
        
        //Ve si hay errores
        if (!mapLogErrores.isEmpty())
            upsert mapLogErrores.values() TAM_LogsErrores__c.TAM_idExtReg__c;
            
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActTotVtaInvBch_cls.finish Hora: ' + DateTime.now());      
    } 

}