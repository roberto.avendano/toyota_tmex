/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para el servicio de SFDC_ConsultaCtesParaFacturar_ctrl_rst

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Sep-2020	    	Héctor Figueroa            	Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SFDC_ConsultaCtesParaFacturarCtrlRstTest {

	static String VaRtLeadRetailNuevos = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Nuevos').getRecordTypeId();
	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
	static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();

	@TestSetup static void loadData(){

		Account clienteMoral = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57039',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			TAM_TokenSFDC__c = '4cb2035d-4be2-66f6-af82-57981714bcc0'
			
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'TestAccount',
			LastName = 'TestAccountLastName',
			Codigo_Distribuidor__c = '570551',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  
		
        Lead cand = new Lead(
	        RecordTypeId = VaRtLeadRetailNuevos,
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '5554565432',
	        Status='Pedido en Proceso',
	        LastName = 'Test3',
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Retail Nuevos',
	        TAM_DatosValidosFacturacion__c = true,
	        FWY_codigo_distribuidor__c = '57039'      
        );
	    insert cand;

        TAM_InventarioVehiculosToyota__c objInvAFG = new TAM_InventarioVehiculosToyota__c(
       		  Name = 'XXXXXXXXXX1',
              Dealer_Code__c = '57039',
              Interior_Color_Description__c = 'Azul', 
              Exterior_Color_Description__c = 'Gris',
              Model_Number__c = '22060',
              Model_Year__c = '2020', 
              Toms_Series_Name__c = 'AVANZA',
              Exterior_Color_Code__c = '00B79', 
              Interior_Color_Code__c =  '010',
              Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
	    insert objInvAFG;

        TAM_LeadInventarios__c objLeadInvAFG = new TAM_LeadInventarios__c(
       		  Name = 'XXXXXXXXXX1',
              TAM_InventarioVehiculosFyG__c = objInvAFG.id,
              TAM_Prospecto__c = cand.id, 
              TAM_Idexterno__c = cand.id + ' XXXXXXXXXX1'
        );
	    insert objLeadInvAFG;
	             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void SFDC_ConsultaCtesParaFacturarCtrlRstOK() {

		//Consulta los datos del cliente
		candidatoPrueba = [Select Id, Name, TAM_DatosValidosFacturacion__c, FWY_codigo_distribuidor__c,  Status,
		TAM_TipoCandidato__c, FWY_Tipo_de_persona__c From Lead LIMIT 1];
		candidatoPrueba.Status = 'Pedido en Proceso';
		update candidatoPrueba;
   		System.debug('EN SFDC_ConsultaCtesParaFacturarCtrlRstOK candidatoPrueba: ' + candidatoPrueba);
        
        String sBody = '{"IdUnicoUserSFDC":"4cb2035d-4be2-66f6-af82-57981714bcc0","NumeroDealer":"57039"}';
        //Llama la clase de TAM_CargaArchivosCtrl y metodo consultDatosCand
        SFDC_ConsultaCtesParaFacturar_ctrl_rst.getClientesParaFacturar(sBody);
       
        
    }
}