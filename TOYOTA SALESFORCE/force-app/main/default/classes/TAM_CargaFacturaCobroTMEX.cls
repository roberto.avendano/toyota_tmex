/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento del TAM_CargaFacturaCobroTMEX estado de cuenta (INCENTIVOS)
*/
public class TAM_CargaFacturaCobroTMEX {
     
    @AuraEnabled
    public static List<wrapperEstadoCta>  verifyChecksVIN(List <wrapperEstadoCta> edoCtaIN){        
        List <wrapperEstadoCta> outListVINES = new   List <wrapperEstadoCta>();
        decimal totalReceived = 0;
        if(!edoCtaIN.isEmpty()){
            for(wrapperEstadoCta i : edoCtaIN){
                if(i.enviarFacturar == true && i.estatusVINEdoCta == 'Revisado y listo para cobro'){
                    wrapperEstadoCta lineaLista = new wrapperEstadoCta();
                    lineaLista.Id = i.id;
                    lineaLista.VIN = i.VIN;
                    lineaLista.comentarioDealer = i.comentarioDealer;
                    lineaLista.facturaVenta	 = i.facturaVenta;
                    lineaLista.incentivoSolicitadoDealer = i.incentivoSolicitadoDealer;
                    lineaLista.VINEntregado = i.VINEntregado;
                    lineaLista.estatusVINEdoCta = i.estatusVINEdoCta;
                    lineaLista.incentivoSolicitadoDealer = i.incentivoSolicitadoDealer;
                    outListVINES.add(lineaLista);
                    
                }
            }      
        }
        
        
        if(!outListVINES.isEmpty()){
            return outListVINES;
        }else{
            return null;
        }
        
    }
    
    
    @AuraEnabled
    public static decimal  getSUMofList(List <wrapperEstadoCta> edoCtaIN){        
        List <wrapperEstadoCta> outListVINES = new   List <wrapperEstadoCta>();
        decimal totalReceived = 0;
        if(!edoCtaIN.isEmpty()){
            for(wrapperEstadoCta i : edoCtaIN){
                if(i.enviarFacturar == true){
                    totalReceived +=i.incentivoSolicitadoDealer;
                } 
            }       
        }
        
        
        if(totalReceived != null){
            return totalReceived;
        }else{
            return null;
        }
        
    }
    
    @AuraEnabled
    public static TAM_EstadoCuenta__c getDocumentByParentId(String recordId){
        //TAM_EstadoCuenta__c detalleIncRecord   = [SELECT id,TAM_IdDocFactRetail__c,TAM_NombreFacturaRetail__c FROM TAM_EstadoCuenta__c WHERE id =: recordId];
        
        // if(detalleIncRecord.TAM_IdDocFactRetail__c != null){
        //   return detalleIncRecord;
        //}else{
        //return detalleIncRecord;
        //}
        return null;
    }   
    
    
    @AuraEnabled
    public static Boolean saveDocumentIdDetalleEdoCta(String idRecord, String idDocument, String nombreDocumento){
        
        // TAM_EstadoCuenta__c detalleIncRecord  = [SELECT id,TAM_IdDocFactRetail__c,TAM_NombreFacturaRetail__c FROM TAM_EstadoCuenta__c WHERE Id =: idRecord];
        // detalleIncRecord.TAM_IdDocFactRetail__c 				    = idDocument;
        // detalleIncRecord.TAM_NombreFacturaRetail__c					= nombreDocumento;
        
        return true;
        
    }
    
    
    
    @AuraEnabled
    public static void deleteDocument (String documentId, String idRecordSolicitud){
        ContentDocument[] documentRecord;
        documentRecord = [Select id from ContentDocument WHERE id =: documentId];
        if(documentRecord.size() >0){
            delete documentRecord[0];   
        }
        
    }    
    
    
    @AuraEnabled
    public static boolean guardaFacturaDetalleEdoCta (String documentId, String facturaCobroTMEX,List<wrapperEstadoCta> objIN,decimal subtotalFactura,String tipoProvision){
        List <TAM_DetalleEstadoCuenta__c> updListEdoCta = new List <TAM_DetalleEstadoCuenta__c> ();
        List<TAM_DetalleEstadoCuenta__c> listaLineasEdoCta = new List<TAM_DetalleEstadoCuenta__c>();
        List<String> listaVINES = new List<String>();
        String idDetalleEdoCta;
        system.debug('obIN'+objIN);
        //Se crea la factura en el objeto TAM_Factura_Dealer__c 
        if(!objIn.isEmpty()){
            for(wrapperEstadoCta detalle : objIn){
                listaVINES.add(detalle.VIN);
                idDetalleEdoCta = detalle.Id;
            }               
        }
        
        String IdEstadoCuenta;
        if(idDetalleEdoCta != null){
            IdEstadoCuenta = [Select TAM_EstadoCuenta__c From TAM_DetalleEstadoCuenta__c WHERE id =: idDetalleEdoCta].TAM_EstadoCuenta__c ;
            
            
        }
        
        String tipoDeEstadoCuenta;
        if(tipoProvision == 'VentaCorporativa'){
            tipoDeEstadoCuenta = 'Venta Corporativa';
        }
        if(tipoProvision == 'AutoDemo'){
            tipoDeEstadoCuenta = 'Auto Demo';
        }
        if(tipoProvision == 'Bono'){
            tipoDeEstadoCuenta = 'Bono';
        }
        if(tipoProvision == 'Retail'){
            tipoDeEstadoCuenta = 'Retail';
        }
        
        TAM_Factura_Dealer__c factEdoCtaDelaer= new TAM_Factura_Dealer__c();
        factEdoCtaDelaer.TAM_Id_Documento__c = documentId;
        factEdoCtaDelaer.TAM_FolioFactura__c = facturaCobroTMEX;
        factEdoCtaDelaer.TAM_VIN_Factura_Dealer__c = listaVINES.toString();
        factEdoCtaDelaer.TAM_SubTotal_Factura__c  = SubtotalFactura;
        factEdoCtaDelaer.TAM_EstadoFactura__c = 'Pendiente';
        factEdoCtaDelaer.TAM_EstadoCuenta__c = IdEstadoCuenta;
        factEdoCtaDelaer.TAM_TipoIncentivo__c = tipoDeEstadoCuenta;
        factEdoCtaDelaer.Id_Externo__c = facturaCobroTMEX+'-'+IdEstadoCuenta;
        
        try{
            system.debug('registro a insertar'+factEdoCtaDelaer);
            upsert factEdoCtaDelaer Id_Externo__c;
            
        }catch(Exception e){
            system.debug('Error en upsert de TAM_Factura_Dealer__c'+e.getMessage());  
        }
        
        
        if(!objIN.isEmpty()){
            for(wrapperEstadoCta lineaEdoCta : objIN){
                TAM_DetalleEstadoCuenta__c nuevoDetalleEdoCtaCobro = new TAM_DetalleEstadoCuenta__c();
                nuevoDetalleEdoCtaCobro.id = lineaEdoCta.id;
                nuevoDetalleEdoCtaCobro.TAM_ComentarioDealer__c = lineaEdoCta.comentarioDealer;
                nuevoDetalleEdoCtaCobro.TAM_FolioFactura__c	 = lineaEdoCta.facturaVenta;
                nuevoDetalleEdoCtaCobro.TAM_IncentivoPropuestoDealer__c = lineaEdoCta.incentivoSolicitadoDealer;
                nuevoDetalleEdoCtaCobro.TAM_VIN__c = lineaEdoCta.VIN;
                nuevoDetalleEdoCtaCobro.TAM_VINEntregado__c = lineaEdoCta.VINEntregado;
                nuevoDetalleEdoCtaCobro.TAM_EstatusVINEdoCta__c = lineaEdoCta.estatusVINEdoCta;
                nuevoDetalleEdoCtaCobro.TAM_IdDocFactRetail__c = documentId; 
                nuevoDetalleEdoCtaCobro.TAM_FactCbroTMRetail__c = facturaCobroTMEX;
                nuevoDetalleEdoCtaCobro.TAM_EstatusVINEdoCta__c = 'Enviado a Facturar';
                nuevoDetalleEdoCtaCobro.TAM_Factura_Dealer__c = factEdoCtaDelaer.id;
                nuevoDetalleEdoCtaCobro.TAM_Estatus_Finanzas__c = 'Pendiente';
                
                listaLineasEdoCta.add(nuevoDetalleEdoCtaCobro);
                
                
            }
            
        }
        
        if(!listaLineasEdoCta.isEmpty()){
            update listaLineasEdoCta;
            return true;
        }
        
        
        return false;
        
    }
    
    
    
    
    public class wrapperEstadoCta {
        @AuraEnabled public String Id {get; set;}
        @AuraEnabled public String nombrePropietario {get; set;}
        @AuraEnabled public String apellidoPropietario {get; set;}
        @AuraEnabled public String IdDetalleProvision {get; set;}
        @AuraEnabled public String IdDetalleEdoCta {get; set;}
        @AuraEnabled public String IdEstadoCuenta {get; set;}
        @AuraEnabled public String VIN {get; set;}
        @AuraEnabled public String serie {get; set;}
        @AuraEnabled public String codigoModelo {get; set;}
        @AuraEnabled public String anioModelo {get; set;}
        @AuraEnabled public decimal incentivoProvisionadoDealer {get; set;}
        @AuraEnabled public decimal incentivoSolicitadoDealer {get; set;}
        @AuraEnabled public String nombreProvision {get; set;}
        @AuraEnabled public String idProvision {get; set;}
        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String nombreDealer {get; set;}        
        @AuraEnabled public String facturaVenta {get; set;}
        @AuraEnabled public String IdfacturaVenta {get; set;}
        @AuraEnabled public String comentarioDealer {get; set;}
        @AuraEnabled public Boolean VINEntregado {get; set;} 
        @AuraEnabled public Boolean EdoCtaCerrado {get; set;} 
        @AuraEnabled public String estatusVINEdoCta {get; set;}
        @AuraEnabled public String tipoTransaction {get; set;}
        @AuraEnabled public String fechaVenta {get; set;}
        @AuraEnabled public String mesPeriodoVenta {get; set;}
        @AuraEnabled public String anioPeriodoVenta {get; set;}
        @AuraEnabled public String facturaCobroTMEX {get; set;}
        @AuraEnabled public Boolean ventaTFS {get; set;}
        @AuraEnabled public Boolean enviarFacturar {get; set;}
    }
    
    
}