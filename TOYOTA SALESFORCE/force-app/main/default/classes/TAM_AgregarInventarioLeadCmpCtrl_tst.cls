/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_AgregarInventarioLeadCmpCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    16-Julio-2020	    Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_AgregarInventarioLeadCmpCtrl_tst {

	static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
	static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
	static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Lead candidatoPrueba = new Lead();
	static  TAM_LeadInventarios__c leadInventa = new TAM_LeadInventarios__c();
	static	TAM_InventarioVehiculosToyota__c objInvVehToy = new TAM_InventarioVehiculosToyota__c();
	static  TAM_CatalogoCodigosModelo__c objCatCodMod = new TAM_CatalogoCodigosModelo__c();

	@TestSetup static void loadData(){
		
		Account clienteMoral = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'TestAccount',
			LastName = 'TestAccountLastName',
			Codigo_Distribuidor__c = '570551',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización'
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  
		
        Lead cand = new Lead(
	        RecordTypeId = VaRtLeadFlotilla,
	        FirstName = 'Test12',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'aw@a.com',
	        phone = '224',
	        Status='Nuevo Lead',
	        LastName = 'Test3',
	        FWY_Tipo_de_persona__c = 'Person física',
	        TAM_TipoCandidato__c = 'Programa'      
        );
	    insert cand;

		TAM_CatalogoCodigosModelo__c catCenMod = new TAM_CatalogoCodigosModelo__c(
        	TAM_Descripcion__c = 'AVANZA-22060-2020-B79-10-LE AT',
        	TAM_Codigo__c = '220602020',
        	TAM_Activo__c = true
	    );
	    insert catCenMod;  

        TAM_InventarioVehiculosToyota__c inventVehiToyota = new TAM_InventarioVehiculosToyota__c(
			Name = 'MR2B29F38M1222999',
			Model_Number__c = '22060',
			Model_Year__c = '2020'
        );
	    insert inventVehiToyota;

        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        Vehiculo__c vin = new Vehiculo__c();
        vin.Name ='MR2B29F38M1222999';
        insert vin;
        
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = clienteMoral.Id,
            Submitted_Date__c = Datetime.now(),
            VIN__c = vin.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            First_Name__c = 'PRUEBA',
            Last_Name__c = 'PRUEBA',
            First_Name_Add__c = 'PRUEBA',
            Last_Name_Add__c = 'PUREBA'                      
        );
        insert m01;
        
        TAM_LeadInventarios__c leadInv = new TAM_LeadInventarios__c(
			TAM_Prospecto__c = cand.id,
			TAM_InventarioVehiculosFyG__c = inventVehiToyota.id
        );
	    insert leadInv;
 				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
	//El metodo de prueba
    static testMethod void TAM_AgregarInventarioLeadCmpCtrlOK() {
		
		//Inician las pruebas
		Test.startTest();
		
			//Consulta los datos del cliente
			candidatoPrueba = [Select Id, Name, TAM_EnviarAutorizacion__c, RecordTypeId, FWY_Tipo_de_persona__c, 
			TAM_TipoCandidato__c From Lead LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk candidatoPrueba: ' + candidatoPrueba);
			
			//Consulta los datos del cliente
			leadInventa = [Select Id, TAM_Prospecto__c, TAM_InventarioVehiculosFyG__c 
			From TAM_LeadInventarios__c LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk leadInventa: ' + leadInventa);
	
			//Consulta los datos del cliente
			objInvVehToy = [Select Id, Name, Model_Number__c, Model_Year__c 
			From TAM_InventarioVehiculosToyota__c LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk objInvVehToy: ' + objInvVehToy);
	
			objCatCodMod = [Select Id, TAM_Descripcion__c, TAM_Codigo__c, TAM_Activo__c 
			From TAM_CatalogoCodigosModelo__c LIMIT 1];        
	   		System.debug('EN TAM_CargaArchivosCtrlTstOk objCatCodMod: ' + objCatCodMod);
	
			//Llama a la función de getLeadInventario
			TAM_AgregarInventarioLeadCmpCtrl.getLeadInventario(candidatoPrueba.id); 
			//Llama a la función de getLeadInventario		
			TAM_AgregarInventarioLeadCmpCtrl.actualizaInventario(candidatoPrueba.id, leadInventa.id);  
			//Llama a la función de updateLeadInventario		
			TAM_AgregarInventarioLeadCmpCtrl.updateLeadInventario(candidatoPrueba.id, leadInventa.id);  
			
			List<TAM_AgregarInventarioLeadCmpCtrl.wrpInventarioFyG> lVinesSeleProspFactDel = 
				new List<TAM_AgregarInventarioLeadCmpCtrl.wrpInventarioFyG>();
			lVinesSeleProspFactDel.add(new TAM_AgregarInventarioLeadCmpCtrl.wrpInventarioFyG(objInvVehToy, objCatCodMod.TAM_Descripcion__c, false));
			
			//Llama a la función de consultaLeadInventario   	
	   		TAM_AgregarInventarioLeadCmpCtrl.consultaLeadInventario(candidatoPrueba.id, leadInventa.id);

            //Llama a la función de deleteLeadInventario        
            TAM_AgregarInventarioLeadCmpCtrl.deleteLeadInventario(candidatoPrueba.id, leadInventa.id);
            //Llama a la función de deleteLeadInventario
            TAM_AgregarInventarioLeadCmpCtrl.deleteLeadInventarioSel(candidatoPrueba.id, lVinesSeleProspFactDel);
	
	        //Llama a la función de buscaVinDD      
	        TAM_AgregarInventarioLeadCmpCtrl.buscaVinDD(candidatoPrueba.id, 'MR2B29F38M1222999');
	        //Llama a la función de getEstatusLead      
	        TAM_AgregarInventarioLeadCmpCtrl.getEstatusLead(candidatoPrueba.id);
	        //Llama a la función de getProfileuser      
	        TAM_AgregarInventarioLeadCmpCtrl.getProfileuser();   		
        
        //Fin de las pruebas   		
   		Test.stopTest();
    }

}