/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_Integraciones__c y toma los reg que ya tienen un Mov en DD .

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    27-Jul-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_SirenaWebServiceBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String slstIdLead;
    
    //Un constructor por default
    global TAM_SirenaWebServiceBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_SirenaWebServiceBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_Integraciones__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_SirenaWebServiceBch.');

        TAM_CallRestWebService.datosWebService objDatosWebServiceFinal = 
            new TAM_CallRestWebService.datosWebService();
        
        //Toma los minutos que debes de contar para la consulta de leads en Sirena
        Integer intGetLeadsSirenaMinutos = Integer.valueOf(System.Label.TAM_GetLeadsSirenaMinutos);
        DateTime dtFechaActual = DateTime.now();
        DateTime dtFechaActualFinal = dtFechaActual.addMinutes(intGetLeadsSirenaMinutos);
        String strFechaActual = String.valueOf(dtFechaActualFinal);
        String strFechaActualFinal = strFechaActual.replace(' ', 'T') + '.000Z';
        System.debug('EN TAM_SirenaWebServiceBch.getLeadsSirena strFechaActual: ' + strFechaActual + ' strFechaActualFinal: ' + strFechaActualFinal);

        //Recorre el mapa de mapIdVinReg 
        for (TAM_Integraciones__c objIntegraCons : scope){
            System.debug('EN TAM_SirenaWebServiceBch objIntegraCons: ' + objIntegraCons);
            String strUrlPaso = objIntegraCons.TAM_UrlConsultaCtesDealer__c;
            if (Test.isRunningTest())
                strUrlPaso += 'prospect/60fda1356e4b97000864dab6?api-key=pFm8HEczABluOuN2YjhOyWaba2f5c75H';            
            //Vamos a consultar todos los leads de los ultimos 5 minutos
            if (!Test.isRunningTest()){
                strUrlPaso += 'prospects?api-key=pFm8HEczABluOuN2YjhOyWaba2f5c75H';
                strUrlPaso += '&createdAfter=' + strFechaActualFinal;
            }//Fin si !Test.isRunningTest()
            System.debug('EN TAM_SirenaWebService.getLeadsSirena strUrlPaso: ' + strUrlPaso);            
            //Inicializa el objeto de objIntegra
            objDatosWebServiceFinal = new TAM_CallRestWebService.datosWebService(strUrlPaso, 'GET',
                '', new Map<String, String>());        
        }
        System.debug('EN TAM_SirenaWebServiceBch.getLeadsSirena objDatosWebServiceFinal: ' + objDatosWebServiceFinal);
        
        //Llama al webservice de TAM_RespCallWebService con el objeto objDatosWebServiceFinal
        TAM_RespCallWebService objRespCallWebService = TAM_SirenaWebService.getLeadsSirena(objDatosWebServiceFinal, intGetLeadsSirenaMinutos);        
        System.debug('EN TAM_SirenaWebService.getLeadsSirena objRespCallWebService: ' + objRespCallWebService);            
        //Inicaliza la variable de slstIdLead
        this.slstIdLead = objRespCallWebService.JsonRes;
        System.debug('EN TAM_SirenaWebService.getLeadsSirena this.slstIdLead: ' + this.slstIdLead);            
           
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_SirenaWebServiceBch.finish Hora: ' + DateTime.now() + ' this.slstIdLead: ' + this.slstIdLead);    
        
        //Manda llamar al siguinte proceso para leer los nuevos leads en 5 minutos
        Integer intProcessLeadsSirenaMinutos = Integer.valueOf(System.Label.TAM_ProcessLeadsSirenaMinutos);
        System.debug('EN TAM_SirenaWebServiceBch.finish intProcessLeadsSirenaMinutos: ' + intProcessLeadsSirenaMinutos);
        DateTime dtHoraActual1 = DateTime.now();
        System.debug('EN TAM_SirenaWebServiceBch.finish dtHoraActual1: ' + dtHoraActual1);
        DateTime dtHoraFinal1 = dtHoraActual1.addMinutes(intProcessLeadsSirenaMinutos); //5
        String CRON_EXP1 = dtHoraFinal1.second() + ' ' + dtHoraFinal1.minute() + ' ' + dtHoraFinal1.hour() + ' ' + dtHoraFinal1.day() + ' ' + dtHoraFinal1.month() + ' ? ' + dtHoraFinal1.year();
        System.debug('EN TAM_ActNomDistBch_cls.finish CRON_EXP1: ' + CRON_EXP1);

        //Manda a llamar el proceso de TAM_ActPropProsSch_cls para actuelizar el campo Prop
        TAM_SirenaWebServiceSch objSirenaWebServiceSch = new TAM_SirenaWebServiceSch();
        System.debug('EN TAM_SirenaWebServiceBch.finish objSirenaWebServiceSch: ' + objSirenaWebServiceSch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('Ejecuta_TAM_SirenaWebServiceSch_cls: ' + CRON_EXP1 + '-' + UserInfo.getUserId() + ': ' + CRON_EXP1, CRON_EXP1, objSirenaWebServiceSch);
        

        //Manda llamar al siguinte proceso para leer las interacciones de los nuevos leads en 3 minutos
        Integer intGetInteracionesMinutos = Integer.valueOf(System.Label.TAM_LeadsInteraccSirenaMinutos);
        DateTime dtHoraActual2 = DateTime.now();
        DateTime dtHoraFinal2 = dtHoraActual2.addMinutes(intGetInteracionesMinutos); //3
        String CRON_EXP2 = dtHoraFinal2.second() + ' ' + dtHoraFinal2.minute() + ' ' + dtHoraFinal2.hour() + ' ' + dtHoraFinal2.day() + ' ' + dtHoraFinal2.month() + ' ? ' + dtHoraFinal2.year();
        System.debug('EN TAM_ActNomDistBch_cls.finish CRON_EXP2: ' + CRON_EXP2);

        //Manda a llamar el proceso de TAM_ActPropProsSch_cls para actuelizar el campo Prop
        TAM_SirenaInteracWebServicesch_cls objSirenaInteracWebServicesch = new TAM_SirenaInteracWebServicesch_cls();
        objSirenaInteracWebServicesch.slstIdLead = this.slstIdLead;
        System.debug('EN TAM_SirenaWebServiceBch.finish objSirenaInteracWebServicesch: ' + objSirenaInteracWebServicesch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('Ejecuta_TAM_SirenaInteracWebServicesch_cls: ' + CRON_EXP2 + '-' + UserInfo.getUserId() + ': ' + CRON_EXP2, CRON_EXP2, objSirenaInteracWebServicesch);
                 
    } 
    
}