public without sharing class AsignacionPresupuestoTriggerHandler extends TriggerHandler{
  	private List<AsignacionPresupuesto__c> oldAsignacionList;
    private List<AsignacionPresupuesto__c> asignacionList;
    private Map<String,Schema.RecordTypeInfo> asignacionRecordTypes;

    	public AsignacionPresupuestoTriggerHandler() {
    		this.asignacionList = (List<AsignacionPresupuesto__c>) Trigger.new;
        this.oldAsignacionList = (List<AsignacionPresupuesto__c>) Trigger.old;
        loadRecordTypes();
    	}

      public override void beforeInsert(){
          validateWorkflows(asignacionList);
      }

      public override void afterInsert(){
          loadDataToPresupuesto(asignacionList);
      }

    	public override void afterUpdate(){
          loadDataToPresupuesto(asignacionList);
    	}

      public override void afterDelete(){
          loadDataToPresupuesto(oldAsignacionList);
      }


      private void validateWorkflows(List<AsignacionPresupuesto__c> asignaciones){
          Map<Id, Presupuesto__c> presupuestos = presupuestosInsert(asignaciones);
          Map<Id, Account> dealers = loadAccountMap(asignaciones);
          
          for(AsignacionPresupuesto__c ap: asignaciones){
              Presupuesto__c p =  presupuestos.get(ap.Presupuesto__c);            
              if(p!=null){
                  //Workflow: AsignacionAbril
                  ap.InicioAsignacionAbril__c = p.InicioCortePresupuestoAbril__c;
                  ap.FinAsignacionAbril__c = p.FinCortePresupuestoAbril__c;

                  //Workflow: AsignacionMayo
                  ap.InicioAsignacionMayo__c = p.InicioCortePresupuestoMayo__c;
                  ap.FinAsignacionMayo__c = p.FinCortePresupuestoMayo__c;

                  //Workflow: AsignacionJunio
                  ap.InicioAsignacionJunio__c = p.InicioCortePresupuestoJunio__c;
                  ap.FinAsignacionJunio__c = p.FinCortePresupuestoJunio__c;

                  //Workflow: AsignacionJulio
                  ap.InicioAsignacionJulio__c = p.InicioCortePresupuestoJulio__c;
                  ap.FinAsignacionJulio__c = p.FinCortePresupuestoJulio__c;

                  //Workflow: AsignacionAgosto
                  ap.InicioAsignacionAgosto__c = p.InicioCortePresupuestoAgosto__c;
                  ap.FinAsignacionAgosto__c = p.FinCortePresupuestoAgosto__c;

                  //Workflow: AsignacionSeptiembre
                  ap.InicioAsignacionSeptiembre__c = p.InicioCortePresupuestoSeptiembre__c;
                  ap.FinAsignacionSeptiembre__c = p.FinCortePresupuestoSeptiembre__c;

                  //Workflow: AsignacionOctubre
                  ap.InicioAsignacionOctubrel__c = p.InicioCortePresupuestoOctubre__c;
                  ap.FinAsignacionOctubre__c = p.FinCortePresupuestoOctubre__c;

                  //Workflow: AsignacionNoviembre
                  ap.InicioAsignacionNoviembre__c = p.InicioCortePresupuestoNoviembre__c;
                  ap.FinAsignacionNoviembre__c = p.FinCortePresupuestoNoviembre__c;

                  //Workflow: AsignacionDiciembre
                  ap.InicioAsignacionDiciembre__c = p.InicioCortePresupuestoDiciembre__c;
                  ap.FinAsignacionDiciembre__c = p.FinCortePresupuestoDiciembre__c;

                  //Workflow: AsignacionEnero
                  ap.InicioAsignacionEnero__c = p.InicioCortePresupuestoEnero__c;
                  ap.FinAsignacionEnero__c = p.FinCortePresupuestoEnero__c;
                  
                  //Workflow: AsignacionFebrero
                  ap.InicioAsignacionFebrero__c = p.InicioCortePresupuestoFebrero__c;
                  ap.FinAsignacionFebrero__c = p.FinCortePresupuestoFebrero__c;

                  //Workflow: AsignacionMarzo
                  ap.InicioAsignacionMarzo__c = p.InicioCortePresupuestoMarzo__c;
                  ap.FinAsignacionMarzo__c = p.FinCortePresupuestoMarzo__c;

                  //Workflow: AsignacionPresupuestoCreada
                  ap.RecordTypeId = asignacionRecordTypes.get('Asignación de presupuesto').recordTypeId;

                  if(dealers.get(ap.Distribuidor__c)!=null){
                      //Workflow: Id asignación de presupuestos
                      ap.IdAsignacionPresupuesto__c = p.PeriodoFiscal__r.Name+''+dealers.get(ap.Distribuidor__c).Codigo_Distribuidor__c;
                      ap.GrupoCorporativo__c = dealers.get(ap.Distribuidor__c).Grupo_Coorporativo__c !=null ? dealers.get(ap.Distribuidor__c).Grupo_Coorporativo__c: '';
                      ap.NombreDistribuidorT__c = dealers.get(ap.Distribuidor__c).Name !=null ? dealers.get(ap.Distribuidor__c).Name : '';
                  }                  
                
                } else{
                    ap.addError('La asignación no tiene un Presupuesto asociado');
                }
          }
      }//FIN validateWorkflows

    	private void loadDataToPresupuesto(List<AsignacionPresupuesto__c> asignacionList){
    		Map<Id, Presupuesto__c> presupuestos = loadPresupuestos(asignacionList);    

        for(Presupuesto__c p: presupuestos.values()){
    			p = montosToZero(p);  			
    			for(AsignacionPresupuesto__c ap : p.Presupuestos_Autos_Demo__r){
      				p.PresupuestoAsignadoEnero__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoEnero__c);
      				p.PresupuestoAsignadoFebrero__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoFebrero__c);
      				p.PresupuestoAsignadoMarzo__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoMarzo__c);
      				p.PresupuestoAsignadoAbril__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoAbril__c);
      				p.PresupuestoAsignadoMayo__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoMayo__c);
      				p.PresupuestoAsignadoJunio__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoJunio__c);
      				p.PresupuestoAsignadoJulio__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoJulio__c);
      				p.PresupuestoAsignadoAgosto__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoAgosto__c);
      				p.PresupuestoAsignadoSeptiembre__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoSeptiembre__c);
      				p.PresupuestoAsignadoOctubre__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoOctubre__c);
      				p.PresupuestoAsignadoNoviembre__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoNoviembre__c);
      				p.PresupuestoAsignadoDiciembre__c += AutoDemoTriggerMethods.validaNull(ap.AsignadoDiciembre__c);

              p.UnidadesUtilizadasEnero__c += ap.AutosDemoActivosEnero__c; 
              p.UnidadesUtilizadasFebrero__c += ap.AutosDemoActivosFebrero__c; 
              p.UnidadesUtilizadasMarzo__c += ap.AutosDemoActivosMarzo__c; 
              p.UnidadesUtilizadasAbril__c += ap.AutosDemoActivosAbril__c;
              p.UnidadesUtilizadasMayo__c += ap.AutosDemoActivosMayo__c; 
              p.UnidadesUtilizadasJunio__c += ap.AutosDemoActivosJunio__c; 
              p.UnidadesUtilizadasJulio__c += ap.AutosDemoActivosJulio__c; 
              p.UnidadesUtilizadasAgosto__c += ap.AutosDemoActivosAgosto__c; 
              p.UnidadesUtilizadasSeptiembre__c += ap.AutosDemoActivosSeptiembre__c; 
              p.UnidadesUtilizadasOctubre__c += ap.AutosDemoActivosOctubre__c; 
              p.UnidadesUtilizadasNoviembre__c += ap.AutosDemoActivosNoviembre__c; 
              p.UnidadesUtilizadasDiciembre__c += ap.AutosDemoActivosDiciembre__c;

              p.PresupuestoUtilizadoEnero__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoEnero__c); 
              p.PresupuestoUtilizadoFebrero__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoFebrero__c); 
              p.PresupuestoUtilizadoMarzo__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoMarzo__c); 
              p.PresupuestoUtilizadoAbril__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoAbril__c);
              p.PresupuestoUtilizadoMayo__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoMayo__c); 
              p.PresupuestoUtilizadoJunio__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoJunio__c); 
              p.PresupuestoUtilizadoJulio__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoJulio__c); 
              p.PresupuestoUtilizadoAgosto__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoAgosto__c);
              p.PresupuestoUtilizadoSeptiembre__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoSeptiembre__c); 
              p.PresupuestoUtilizadoOctubre__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoOctubre__c); 
              p.PresupuestoUtilizadoNoviembre__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoNoviembre__c); 
              p.PresupuestoUtilizadoDicembre__c += AutoDemoTriggerMethods.validaNull(ap.MontoAsignadoUtilizadoDiciembre__c);

              p.UnidadesPagadasEnero__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSEnero__c); 
              p.UnidadesPagadasFebrero__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSFebrero__c); 
              p.UnidadesPagadasMarzo__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSMarzo__c); 
              p.UnidadesPagadasAbril__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSAbril__c); 
              p.UnidadesPagadasMayo__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSMayo__c);
              p.UnidadesPagadasJunio__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSJunio__c); 
              p.UnidadesPagadasJulio__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSJulio__c); 
              p.UnidadesPagadasAgosto__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSAgosto__c); 
              p.UnidadesPagadasSeptiembre__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSSeptiembre__c); 
              p.UnidadesPagadasOctubre__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSOctubre__c);
              p.UnidadesPagadasNoviembre__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSNoviembre__c); 
              p.UnidadesPagadasDiciembre__c += AutoDemoTriggerMethods.validaNull(ap.AutosDemoPagadosTFSDiciembre__c); 

              p.PresupuestoPagadoEnero__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSEnero__c); 
              p.PresupuestoPagadoFebrero__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSFebrero__c); 
              p.PresupuestoPagadoMarzo__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSMarzo__c); 
              p.PresupuestoPagadoAbril__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSAbril__c); 
              p.PresupuestoPagadoMayo__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSMayo__c);
              p.PresupuestoPagadoJunio__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSJunio__c); 
              p.PresupuestoPagadoJulio__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSJulio__c); 
              p.PresupuestoPagadoAgosto__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSAgosto__c); 
              p.PresupuestoPagadoSeptiembre__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSSeptiembre__c); 
              p.PresupuestoPagadoOctubre__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSOctubre__c);
              p.PresupuestoPagadoNoviembre__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSNoviembre__c); 
              p.PresupuestoPagadoDiciembre__c += AutoDemoTriggerMethods.validaNull(ap.InteresesPagadosTFSDiciembre__c);              
    			}
    		}

    		update presupuestos.values();
    	}


    	private Map<Id, Presupuesto__c> loadPresupuestos(List<AsignacionPresupuesto__c> asignacionList){
  		  Set<Id> presupuestoID = new Set<Id>();
    		for(AsignacionPresupuesto__c ap: asignacionList){
      		presupuestoID.add(ap.Presupuesto__c);
      	}

        	return new Map<Id, Presupuesto__c>(
              [SELECT Id, PresupuestoAsignadoEnero__c, PresupuestoAsignadoFebrero__c, PresupuestoAsignadoMarzo__c, PresupuestoAsignadoAbril__c, 
              PresupuestoAsignadoMayo__c, PresupuestoAsignadoJunio__c, PresupuestoAsignadoJulio__c, PresupuestoAsignadoAgosto__c, 
              PresupuestoAsignadoSeptiembre__c, PresupuestoAsignadoOctubre__c, PresupuestoAsignadoNoviembre__c, PresupuestoAsignadoDiciembre__c,
              UnidadesUtilizadasEnero__c, UnidadesUtilizadasFebrero__c, UnidadesUtilizadasMarzo__c, UnidadesUtilizadasAbril__c,
              UnidadesUtilizadasMayo__c, UnidadesUtilizadasJunio__c, UnidadesUtilizadasJulio__c, UnidadesUtilizadasAgosto__c, 
              UnidadesUtilizadasSeptiembre__c, UnidadesUtilizadasOctubre__c, UnidadesUtilizadasNoviembre__c, UnidadesUtilizadasDiciembre__c,	
              PresupuestoUtilizadoEnero__c, PresupuestoUtilizadoFebrero__c, PresupuestoUtilizadoMarzo__c, PresupuestoUtilizadoAbril__c,
              PresupuestoUtilizadoMayo__c, PresupuestoUtilizadoJunio__c, PresupuestoUtilizadoJulio__c, PresupuestoUtilizadoAgosto__c,
              PresupuestoUtilizadoSeptiembre__c, PresupuestoUtilizadoOctubre__c, PresupuestoUtilizadoNoviembre__c, PresupuestoUtilizadoDicembre__c,
              
              UnidadesPagadasEnero__c, UnidadesPagadasFebrero__c, UnidadesPagadasMarzo__c, UnidadesPagadasAbril__c, UnidadesPagadasMayo__c,
              UnidadesPagadasJunio__c, UnidadesPagadasJulio__c, UnidadesPagadasAgosto__c, UnidadesPagadasSeptiembre__c, UnidadesPagadasOctubre__c,
              UnidadesPagadasNoviembre__c, UnidadesPagadasDiciembre__c, 

              PresupuestoPagadoEnero__c, PresupuestoPagadoFebrero__c, PresupuestoPagadoMarzo__c, PresupuestoPagadoAbril__c, PresupuestoPagadoMayo__c,
              PresupuestoPagadoJunio__c, PresupuestoPagadoJulio__c, PresupuestoPagadoAgosto__c, PresupuestoPagadoSeptiembre__c, PresupuestoPagadoOctubre__c,
              PresupuestoPagadoNoviembre__c, PresupuestoPagadoDiciembre__c,

                (SELECT Id, AsignadoEnero__c, AsignadoFebrero__c, AsignadoMarzo__c, AsignadoAbril__c, AsignadoMayo__c, 
              	AsignadoJunio__c, AsignadoJulio__c, AsignadoAgosto__c, AsignadoSeptiembre__c, AsignadoOctubre__c, 
              	AsignadoNoviembre__c, AsignadoDiciembre__c,
                AutosDemoActivosEnero__c, AutosDemoActivosFebrero__c, AutosDemoActivosMarzo__c, AutosDemoActivosAbril__c,
                AutosDemoActivosMayo__c, AutosDemoActivosJunio__c, AutosDemoActivosJulio__c, AutosDemoActivosAgosto__c,
                AutosDemoActivosSeptiembre__c, AutosDemoActivosOctubre__c, AutosDemoActivosNoviembre__c, AutosDemoActivosDiciembre__c,

                MontoAsignadoUtilizadoEnero__c, MontoAsignadoUtilizadoFebrero__c, MontoAsignadoUtilizadoMarzo__c, MontoAsignadoUtilizadoAbril__c, MontoAsignadoUtilizadoMayo__c,
                MontoAsignadoUtilizadoJunio__c, MontoAsignadoUtilizadoJulio__c, MontoAsignadoUtilizadoAgosto__c, MontoAsignadoUtilizadoSeptiembre__c, MontoAsignadoUtilizadoOctubre__c,
                MontoAsignadoUtilizadoNoviembre__c, MontoAsignadoUtilizadoDiciembre__c,  
                
                InteresesPagadosTFSEnero__c, InteresesPagadosTFSFebrero__c, InteresesPagadosTFSMarzo__c, InteresesPagadosTFSAbril__c,
                InteresesPagadosTFSMayo__c, InteresesPagadosTFSJunio__c, InteresesPagadosTFSJulio__c, InteresesPagadosTFSAgosto__c,
                InteresesPagadosTFSSeptiembre__c, InteresesPagadosTFSOctubre__c, InteresesPagadosTFSNoviembre__c, InteresesPagadosTFSDiciembre__c,
                
                AutosDemoPagadosTFSEnero__c, AutosDemoPagadosTFSFebrero__c, AutosDemoPagadosTFSMarzo__c, AutosDemoPagadosTFSAbril__c,
                AutosDemoPagadosTFSMayo__c, AutosDemoPagadosTFSJunio__c, AutosDemoPagadosTFSJulio__c, AutosDemoPagadosTFSAgosto__c, AutosDemoPagadosTFSSeptiembre__c,
                AutosDemoPagadosTFSOctubre__c, AutosDemoPagadosTFSNoviembre__c, AutosDemoPagadosTFSDiciembre__c 

                FROM Presupuestos_Autos_Demo__r)
              FROM Presupuesto__c WHERE Id IN:presupuestoID]);
    	}

      private Map<Id, Presupuesto__c> presupuestosInsert(List<AsignacionPresupuesto__c> asignacionList){
        Set<Id> presupuestoID = new Set<Id>();
        for(AsignacionPresupuesto__c ap: asignacionList){
          presupuestoID.add(ap.Presupuesto__c);
        }

        return new Map<Id, Presupuesto__c>(
          [SELECT Id, InicioCortePresupuestoAbril__c, FinCortePresupuestoAbril__c, InicioCortePresupuestoMayo__c, FinCortePresupuestoMayo__c,
            InicioCortePresupuestoJunio__c, FinCortePresupuestoJunio__c, InicioCortePresupuestoJulio__c, FinCortePresupuestoJulio__c, 
            InicioCortePresupuestoAgosto__c, FinCortePresupuestoAgosto__c, InicioCortePresupuestoSeptiembre__c, FinCortePresupuestoSeptiembre__c,
            InicioCortePresupuestoOctubre__c, FinCortePresupuestoOctubre__c, InicioCortePresupuestoNoviembre__c, FinCortePresupuestoNoviembre__c,
            InicioCortePresupuestoDiciembre__c, FinCortePresupuestoDiciembre__c, InicioCortePresupuestoEnero__c, FinCortePresupuestoEnero__c,
            InicioCortePresupuestoFebrero__c, FinCortePresupuestoFebrero__c, InicioCortePresupuestoMarzo__c, FinCortePresupuestoMarzo__c,
            PeriodoFiscal__r.Name
          FROM Presupuesto__c WHERE Id IN:presupuestoID]);
      }

    	private Presupuesto__c montosToZero(Presupuesto__c p){
    		p.PresupuestoAsignadoEnero__c = 0; 
    		p.PresupuestoAsignadoFebrero__c = 0; 
    		p.PresupuestoAsignadoMarzo__c = 0; 
    		p.PresupuestoAsignadoAbril__c = 0; 
        p.PresupuestoAsignadoMayo__c = 0; 
        p.PresupuestoAsignadoJunio__c = 0; 
        p.PresupuestoAsignadoJulio__c = 0; 
        p.PresupuestoAsignadoAgosto__c = 0; 
        p.PresupuestoAsignadoSeptiembre__c = 0; 
        p.PresupuestoAsignadoOctubre__c = 0; 
        p.PresupuestoAsignadoNoviembre__c = 0; 
        p.PresupuestoAsignadoDiciembre__c = 0;

        p.UnidadesUtilizadasEnero__c = 0; 
        p.UnidadesUtilizadasFebrero__c = 0; 
        p.UnidadesUtilizadasMarzo__c = 0; 
        p.UnidadesUtilizadasAbril__c = 0;
        p.UnidadesUtilizadasMayo__c = 0; 
        p.UnidadesUtilizadasJunio__c = 0; 
        p.UnidadesUtilizadasJulio__c = 0; 
        p.UnidadesUtilizadasAgosto__c = 0; 
        p.UnidadesUtilizadasSeptiembre__c = 0; 
        p.UnidadesUtilizadasOctubre__c = 0; 
        p.UnidadesUtilizadasNoviembre__c = 0; 
        p.UnidadesUtilizadasDiciembre__c = 0;

        p.PresupuestoUtilizadoEnero__c= 0; 
        p.PresupuestoUtilizadoFebrero__c= 0; 
        p.PresupuestoUtilizadoMarzo__c= 0; 
        p.PresupuestoUtilizadoAbril__c= 0;
        p.PresupuestoUtilizadoMayo__c= 0; 
        p.PresupuestoUtilizadoJunio__c= 0; 
        p.PresupuestoUtilizadoJulio__c= 0; 
        p.PresupuestoUtilizadoAgosto__c= 0;
        p.PresupuestoUtilizadoSeptiembre__c= 0; 
        p.PresupuestoUtilizadoOctubre__c= 0; 
        p.PresupuestoUtilizadoNoviembre__c= 0; 
        p.PresupuestoUtilizadoDicembre__c= 0;

        p.UnidadesPagadasEnero__c = 0; 
        p.UnidadesPagadasFebrero__c = 0; 
        p.UnidadesPagadasMarzo__c = 0; 
        p.UnidadesPagadasAbril__c = 0; 
        p.UnidadesPagadasMayo__c = 0;
        p.UnidadesPagadasJunio__c = 0; 
        p.UnidadesPagadasJulio__c = 0; 
        p.UnidadesPagadasAgosto__c = 0; 
        p.UnidadesPagadasSeptiembre__c = 0; 
        p.UnidadesPagadasOctubre__c = 0;
        p.UnidadesPagadasNoviembre__c = 0; 
        p.UnidadesPagadasDiciembre__c = 0; 

        p.PresupuestoPagadoEnero__c = 0; 
        p.PresupuestoPagadoFebrero__c = 0; 
        p.PresupuestoPagadoMarzo__c = 0; 
        p.PresupuestoPagadoAbril__c = 0; 
        p.PresupuestoPagadoMayo__c = 0;
        p.PresupuestoPagadoJunio__c = 0; 
        p.PresupuestoPagadoJulio__c = 0; 
        p.PresupuestoPagadoAgosto__c = 0; 
        p.PresupuestoPagadoSeptiembre__c = 0; 
        p.PresupuestoPagadoOctubre__c = 0;
        p.PresupuestoPagadoNoviembre__c = 0; 
        p.PresupuestoPagadoDiciembre__c = 0;

        return p;
    	}

      private void loadRecordTypes(){
          Schema.DescribeSObjectResult asignacionSchema = Schema.SObjectType.AsignacionPresupuesto__c;
          asignacionRecordTypes = asignacionSchema.getRecordTypeInfosByName();
      }

      private Map<Id, Account> loadAccountMap(List<AsignacionPresupuesto__c> asignaciones){
          Set<Id> distribuidorID = new Set<Id>();
          for(AsignacionPresupuesto__c ap: asignaciones){
              distribuidorID.add(ap.Distribuidor__c);
          }

          return new Map<Id, Account>([
              SELECT Id, Name, Codigo_Distribuidor__c, Grupo_Coorporativo__c FROM Account WHERE Id IN:distribuidorID 
          ]);
      }



}