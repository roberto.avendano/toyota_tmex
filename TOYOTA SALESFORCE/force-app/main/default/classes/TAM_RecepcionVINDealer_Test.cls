@isTest
public class TAM_RecepcionVINDealer_Test {
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String srecordTypePrograma =  Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    @testSetup static void setup() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        
        
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordTypeId = recordTypeDistribuidor
        );
        insert a01;
        
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a01.id;
        insert contactRH1;
        
        Contact contactRH2 = new Contact();
        contactRH2.NombreUsuario__c = 'Contacto RH2';
        contactRH2.lastName = 'Contacto RH2';
        contactRH2.Email = 'test2@test.com';
        contactRH2.AccountId = a01.id;
        insert contactRH2;
        
        Contact contactRH3 = new Contact();
        contactRH3.NombreUsuario__c = 'Contacto RH3';
        contactRH3.lastName = 'Contacto RH3';
        contactRH3.Email = 'test3@test.com';
        contactRH3.AccountId = a01.id;
        insert contactRH3;
        
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia 
            UserRole uR = [SELECT Id FROM UserRole WHERE Name='TOYOTA MONTERREY Socio Ejecutivo'];
            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US',ContactId = contactRH2.id,ProfileId = p.Id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
            //Usuario 2
            User u2 = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',ContactId = contactRH1.id,ProfileId = p.Id,PortalRole  = 'Manager',
                               TimeZoneSidKey='America/Los_Angeles', UserName='test992@testorg.com');
            system.debug('usuario'+u2);
            insert u2;
            
            // Usuario 3
            
            User u3 = new User(Alias = 'standt3', Email='standarduser23@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing13', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',ContactId = contactRH3.id,ProfileId = p.Id,PortalRole  = 'Manager',
                               TimeZoneSidKey='America/Los_Angeles', UserName='test9923@testorg.com');
            system.debug('usuario'+u3);
            insert u3;
            
            //Grupo 1
            Group grp = new Group();
            grp.name = 'Aprobador DTM (nivel 1)';
            grp.Type = 'Regular'; 
            Insert grp; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = u.Id;
            grpMem1.GroupId = grp.Id;
            Insert grpMem1;
            
            
            //Grupo 2
            Group grp2 = new Group();
            grp2.name = 'Aprobador DTM (nivel 2)';
            grp2.Type = 'Regular'; 
            Insert grp2; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = u2.Id;
            grpMem2.GroupId = grp2.Id;
            Insert grpMem2;
            
            
        }
        
        //Se crea un registro de prueba de tipo TAM_SolicitudesFlotillaPrograma__c
        Rangos__c rangoPrograma = new Rangos__c(
            Name = 'UBER',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'UBER | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoPrograma;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoFlotilla;
        
        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;
        
        
        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = srecordTypePrograma,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_NomDistribuidorPropietario__c = 'Es de Prueba',
            TAM_ComentarioDTM1__c = 'TEST',
            TAM_ComentarioDTM2__c = 'TEST',
            TAM_AprobacionDTM1__c = 'Aprobar Cancelación',
            TAM_AprobacionDTM2__c = 'Aprobar Cancelación'
        );
        insert solVentaFlotillaPrograma;

        
        //Se crean lineas de tipo TAM_DODSolicitudesPedidoEspecial__c
        TAM_DODSolicitudesPedidoEspecial__c solicitudDOD = new TAM_DODSolicitudesPedidoEspecial__c();
        solicitudDOD.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        solicitudDOD.TAM_VIN__c = 'QWERTYUIO45670';
        solicitudDOD.TAM_Estatus__c = 'Autorizado';
        solicitudDOD.TAM_Modelo__c  = '2701';
        solicitudDOD.TAM_Codigo__c = '2701';
        solicitudDOD.TAM_Anio__c = '2020';
        solicitudDOD.TAM_DescripcionColor__c = 'Rojo';
        solicitudDOD.TAM_Pago__c  = 'Contado';
        solicitudDOD.TAM_DescuentoAutorizadoDtm__c  = 5;
        solicitudDOD.TAM_ComentarioDTM1__c = 'tEST';
        solicitudDOD.TAM_Entrega__c = 'Test';
        insert solicitudDOD;
        
        //Se crean lineas de tipo TAM_CheckOutDetalleSolicitudCompra__c
        TAM_CheckOutDetalleSolicitudCompra__c lineaCheckOut = new TAM_CheckOutDetalleSolicitudCompra__c();
        lineaCheckOut.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        lineaCheckOut.TAM_VIN__c = 'CAERQIOO1829101';
        lineaCheckOut.TAM_EstatusDealerSolicitud__c = 'En proceso de cancelación';
        lineaCheckOut.TAM_TipoPago__c = 'Contado';
        lineaCheckOut.TAM_IncPropuestoPorcentaje__c = 5;
        insert lineaCheckOut;
        
        //Distribidores flotilla programa
        TAM_DistribuidoresFlotillaPrograma__c distFlotillaProgr = new TAM_DistribuidoresFlotillaPrograma__c();
        distFlotillaProgr.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        distFlotillaProgr.TAM_Cantidad__c = 9;
        distFlotillaProgr.TAM_ComentarioDealerRecepcion__c = 'test';
        distFlotillaProgr.TAM_Cuenta__c =  a01.id;
        distFlotillaProgr.TAM_Estatus__c = 'Pendiente';
        distFlotillaProgr.Name = 'Test-2020-2701';
        distFlotillaProgr.TAM_NombreDistribuidor__c = 'De prueba';
        insert distFlotillaProgr;
        
        //Solicitud desitino dealer
        String solName = [select name from TAM_SolicitudesFlotillaPrograma__c  where TAM_NomDistribuidorPropietario__c = 'Es de Prueba'].name;

        TAM_SolicitudDealerDestino__c destinoDealer = new TAM_SolicitudDealerDestino__c();
        destinoDealer.name = solName;
        destinoDealer.TAM_DealerOrigen__c = 'Country';
        destinoDealer.TAM_DealerDestino__c = 'Monterrey';
        system.debug('destinodealer'+destinodealer);
        insert destinoDealer;
        
        
        
    }
    
    //Metodo que comprueba getTypeOfUser
    @isTest static void testMethod1(){
        User usr = [Select Id From User where LastName = 'Testing1' limit 1];
        
        
        TAM_SolicitudDealerDestino__c destinoDealer = [Select id from TAM_SolicitudDealerDestino__c Where TAM_DealerOrigen__c  = 'Country' ];
        
        Boolean respuestaMetodo =  TAM_RecepcionVINDealer.getTypeOfUser(destinoDealer.id,usr.Id);
        system.debug('assert'+respuestaMetodo);
       	System.assertEquals(respuestaMetodo,false);
        
        
    }
    
    //Metodo que comprueba getTypeOfUser
    @isTest static void testMethod2(){
        User usr = [Select Id From User where Email='standarduser23@testorg.com' limit 1];
        
        
       TAM_SolicitudDealerDestino__c destinoDealer = [Select id from TAM_SolicitudDealerDestino__c Where  TAM_DealerOrigen__c  = 'Country' ];
        
        Boolean respuestaMetodo = TAM_RecepcionVINDealer.getTypeOfUser(destinoDealer.id,usr.Id);
        System.assertEquals(respuestaMetodo,false);
        
    }
    
    //Metodo que comprueba getLineasDealerFlotillaPrograma
    @isTest static void testMethod3(){             
        TAM_SolicitudDealerDestino__c destinoDealer = [Select id from TAM_SolicitudDealerDestino__c Where  TAM_DealerOrigen__c  = 'Country' ];
        
         list<TAM_DistribuidoresFlotillaPrograma__c> listaDistribuidores = TAM_RecepcionVINDealer.getLineasDealerFlotillaPrograma(destinoDealer.id);
          System.assertEquals(!listaDistribuidores.isEmpty(),true);
    }
    
    //Metodo que comprueba guardarAprobacion
    @isTest static void testMethod4(){             
        TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = [Select id from TAM_SolicitudesFlotillaPrograma__c Where TAM_ComentarioDTM1__c = 'TEST' ];
        List<TAM_DistribuidoresFlotillaPrograma__c> listFlotillaPrograma = [Select id,TAM_IdExterno__c,TAM_IdDistribuidor__c,
                                                                            TAM_NombreDistribuidor__c,TAM_SolicitudFlotillaPrograma__c,TAM_Estatus__c,TAM_Cuenta__c,
                                                                            Name,TAM_ComentarioDealerRecepcion__c From TAM_DistribuidoresFlotillaPrograma__c];
        
        TAM_SolicitudDealerDestino__c destinoDealer = [Select id from TAM_SolicitudDealerDestino__c Where  TAM_DealerOrigen__c  = 'Country' ];
        
        
        if(!listFlotillaPrograma.isEmpty()){
            for(TAM_DistribuidoresFlotillaPrograma__c reg : listFlotillaPrograma){
                reg.TAM_Estatus__c = 'Aceptar Recepción';
            }
            update listFlotillaPrograma;
        }
        TAM_RecepcionVINDealer.guardarAprobacion(listFlotillaPrograma,destinoDealer.id);
        TAM_DistribuidoresFlotillaPrograma__c distribuidorFlotillas = [Select id,TAM_Estatus__c from TAM_DistribuidoresFlotillaPrograma__c where TAM_Estatus__c = 'Aceptar Recepción' limit 1];
        System.assertEquals('Aceptar Recepción',distribuidorFlotillas.TAM_Estatus__c);
        
    }
    
    //Metodo que comprueba valoresAprobacionDealer
    @isTest static void testMethod5(){             
         List<String> pickListValues = TAM_RecepcionVINDealer.valoresAprobacionDealer();
        System.assertEquals(!pickListValues.isEmpty(),true);
        
    }
    
}