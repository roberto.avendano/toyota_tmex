public  without sharing class SolicitudSiteAttachmentController{
    public String sivId{get;set;}
    public String nameFile{get;set;}
    public ContentVersion file { get; set; }
    public List<ContentVersion> contentVersionsList{ get; set; }
    
    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
    
    
    public SolicitudSiteAttachmentController(){    
        sivId =  ApexPages.currentPage().getParameters().get('id');
        file = new ContentVersion();
        getContentVersions();
    }
    
    public List<Attachment> getAttachments(){
        return [SELECT Id, Name, ContentType,  LastModifiedBy.Name, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentId=:sivId];
    } 
    public void getContentVersions(){             
        system.debug('entro a contne');
        
        Set<String> ListaId = new Set<String>();
        List<ContentDocumentLink> conDocu = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :sivId];
        for(ContentDocumentLink doc : conDocu){
            ListaId.add(doc.ContentDocumentId);
            
        }
        
        
        contentVersionsList =  [SELECT Id, Title, ContentSize, FirstPublishLocationId,LastModifiedBy.Name, CreatedBy.Name, CreatedDate
                                FROM ContentVersion where ContentDocumentId IN : ListaId];
        
        
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> id '+ sivId +'>>>>>>>  '+contentVersionsList);
    } 
    
    public PageReference cargarArchivo() {
        //PageReference ret;
        //ret = new PageReference('/SolicitudSiteAttachments?id='+sivId); //Dirigir a la pagina previa tal vez o solo cerrar ventana
        system.debug('entro');
        try {
            if(nameFile != null || nameFile != ''){
                file.title = nameFile;
                file.FirstPublishLocationId = sivId;
                insert file;	        	
            }
            
            nameFile = '';
            file = new ContentVersion();
            getContentVersions();
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error al guardar archivo'));
        }         
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Archivo guardado'));
        //return ret;
        return null;
    }
    
    public PageReference adjuntarArchivo(){
        PageReference ret;
        ret = new PageReference('/SolicitudSiteAttachments?id='+sivId);
        return ret;
    }
}