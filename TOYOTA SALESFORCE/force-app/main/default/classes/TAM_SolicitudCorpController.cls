/**
*  Descripción General: Nuevas Solicitudes de venta corporativa
*  ________________________________________________________________
*      Autor                   Fecha               Descripción
*  ________________________________________________________________
*      Cecilia Cruz            15/Octubre/2020      Versión Inicial
*  ________________________________________________________________
*/
public without sharing class TAM_SolicitudCorpController {

    @AuraEnabled
    public static Account getDatosCuenta(String recordId){
        Account objAccount = [SELECT Id, TAM_ProgramaRango__c, TAM_ProgramaRango__r.Name, IN_Estatus__c FROM Account WHERE Id =:recordId];
        return objAccount;
    }
    
    @AuraEnabled
    public static String getRecordId(String recordId){
        System.debug('Entro getRecordId Apex');
        Account objAccount  = [SELECT TAM_ProgramaRango__r.RecordType.Name, IN_Estatus__c FROM Account WHERE Id =:recordId LIMIT 1];
        System.debug('Account: ' + objAccount);
        String recordTypeNameAccount;
        System.debug('RecordType Account Name: ' + objAccount.TAM_ProgramaRango__r.RecordType.Name);
        if(objAccount.TAM_ProgramaRango__r.RecordType.Name == 'Flotillas'){
            recordTypeNameAccount = 'Venta Corporativa';
        } else if(objAccount.TAM_ProgramaRango__r.RecordType.Name == 'Programas'){
            recordTypeNameAccount = 'Programa';
        }
        String strRecordTypeName = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get(recordTypeNameAccount).getRecordTypeId();
        System.debug('Record Id' + strRecordTypeName);
        return strRecordTypeName;
    }
}