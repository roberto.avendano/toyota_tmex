/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para enviar los datos del Prospecto a validaxión por parte del usuario
    					para el aera de DTM.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    22-Nov-2019       Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_AutorizacionProspectoCtrl {

	public static String VaRtLeadlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
	public static String VaRtLeadPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();

	public static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	public static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

    public static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    public static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();

	public static String VaRtAccRegPMCC = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();
	public static String VaRtAccRegPFCC = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo CP').getRecordTypeId();

    public static String VaRtCteRangoFlot = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    public static String VaRtCteRangoProg = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
    
   @AuraEnabled  
   public static String consultDatosCand(string recordId){ 
    	System.debug('EN TAM_CargaArchivosCtrl.consultDatosCand recordId:  ' + recordId);
    	String sTipoPersona = '';
    	//Es un candidato
        if (recordId.startsWith('00Q')){
	    	for (Lead candidato : [Select id, TAM_TipoCandidato__c, FWY_Tipo_de_persona__c From Lead Where id = :recordId]){
	    		sTipoPersona = candidato.TAM_TipoCandidato__c;
	    		if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Programa')
	    			sTipoPersona = 'ProgramaFisica';
	    		if ( (candidato.FWY_Tipo_de_persona__c == 'Persona moral' || candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') && candidato.TAM_TipoCandidato__c == 'Programa')
					sTipoPersona = 'ProgramaMoral';
				if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Flotilla')
					sTipoPersona = 'FlotillaFisica';
				if ((candidato.FWY_Tipo_de_persona__c == 'Persona moral' || candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') && candidato.TAM_TipoCandidato__c == 'Flotilla')
					sTipoPersona = 'FlotillaMoral';	    		
	    	}
       }//Fin si recordId.startsWith('00Q')
        
       //Si se trata de Account
       if (recordId.startsWith('001')){
        	TAM_SolicitudesParaAprobar__c objSolAprobar = new TAM_SolicitudesParaAprobar__c();
        	for (Account cliente : [Select id, Name, RecordTypeId, TAM_EstatusCliente__c From Account Where 
        		id =:recordId ]){
        		//Si se trata de un cilente  No Viegnte de tipo Moral	
        		if (cliente.RecordTypeId == VaRtCteMoralNoVigente)	
        			sTipoPersona = 'ClienteNoVigenteMoral';	  	
        		//Si se trata de un cilente  No Viegnte de tipo Moral	
        		if (cliente.RecordTypeId == VaRtCteFisicaNoVigente)	
        			sTipoPersona = 'ClienteNoVigenteFisico';
        		//Si se trata de un cilente  No Viegnte de tipo Moral	
        		if (cliente.RecordTypeId == VaRtCteMoralPreAuto || cliente.RecordTypeId == VaRtCteFisicaPreAuto)	
        			sTipoPersona = 'ClientePreautorizado';
        		if (cliente.RecordTypeId == VaRtCteMoralPreAuto || cliente.RecordTypeId == VaRtCteFisicaPreAuto
        			&& cliente.TAM_EstatusCliente__c == 'Activación')	
        			sTipoPersona = 'ClienteActivación';        			
        	}	
       }//Fin si recordId.startsWith('001')

		System.debug('EN TAM_CargaArchivosCtrl.consultDatosCand sTipoPersona:  ' + sTipoPersona);        
       //Regersa el tipo de candidato o cliente. 
        return sTipoPersona;  
    } 
        

   @AuraEnabled  
   public static wrpDatosCndidato getDatosCandidato(string recordId){ 
    	System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato recordId:  ' + recordId);
    	
		String VaRtSolFlotilla = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
		String VaRtSolPrograma = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	    	
	    String sRFC;
        String sCodigoDistribuidor;
        User usuarioActual = new User();
        
        //Los datos del distribuidor
        for (User usuario : [Select u.id, u.Name, u.CodigoDistribuidor__c, u.Email
                             From User u Where id = :UserInfo.getUserId()]){
                             sCodigoDistribuidor = usuario.CodigoDistribuidor__c;
                             usuarioActual = usuario;
        }//Fin del for para User
        sCodigoDistribuidor = usuarioActual.CodigoDistribuidor__c != null ? usuarioActual.CodigoDistribuidor__c : '57000'; 
        System.debug('EN TAM_CheckOutController.getDatosCandidato sCodigoDistribuidor '+ sCodigoDistribuidor);
        
        Map<String, String> mapRangos = new Map<String, String>();
        for (Rangos__c rangosRT : [Select r.RecordTypeId, r.Id From Rangos__c r]){
            mapRangos.put(rangosRT.id, rangosRT.RecordTypeId);
        }
        System.debug('EN TAM_CheckOutController.getDatosCandidato mapRangos '+ mapRangos);

    	wrpDatosCndidato objWrpDatosCndidato;
    	List<wrpDatosFaltantes> lwrpDatosFaltantes = new List<wrpDatosFaltantes>();
    	
    	//Es un candidato
        if (recordId.startsWith('00Q')){
        	
    	for (Lead candidato : [SELECT ID, Address,City,Description,Email,FirstName,LastName,LeadSource,Name,OwnerId,Phone,PostalCode,RecordTypeId,State,Status,Street,
			TAM_ActaConstitutiva__c,TAM_ActividadPedidoProceso__c,TAM_ComentariosActCot__c,TAM_ComentariosAutorizacionCliente__c,
			TAM_ApellidoPaternoRL__c, TAM_ApellidoMaternoRL__c, TAM_CatalogoProgramas__c, TAM_CatalogoProgramas__r.Name,
			TAM_ArrendadoraFormPagoFinal__c,TAM_AutoDemo__c,TAM_BancoForPagoFinal__c,TAM_Banco__c,TAM_Calle__c,TAM_Cantidad2__c,
			TAM_Cantidad3__c,TAM_Cantidad__c,TAM_CedulaFiscal__c,TAM_Ciudad__c,TAM_ClienteCorporativo__c,TAM_CodigoPostal__c,TAM_Colonia__c,
			TAM_ComentariosActPedFac__c,TAM_ComentariosActPedProc__c,TAM_ComentariosActPedRep__c,TAM_ComentariosActPruMan__c, TAM_ClienteCorporativo__r.Name,
			TAM_ComentariosCteCal__c,TAM_ConfirmaDatos__c,TAM_CorreoElectronicoRL__c,TAM_CorreoElectronico__c,TAM_CuentaInventarioDisponible__c,
			TAM_DejoApartadoEnganche__c,TAM_DireccionCAN__c,TAM_DireccionRL__c,TAM_EntregoDocumentos__c,TAM_EnviarAutorizacion__c,TAM_Estado__c,
			TAM_EstatusAutorizacion__c,TAM_EstatusPedido__c,TAM_FechaActividadCot__c,TAM_FechaActividadCteCal__c,TAM_FechaActividadPedFact__c,
			TAM_FechaActividadPedProc__c,TAM_FechaActividadPedRep__c,TAM_FechaActividadPrueMan__c,TAM_FechaEnvioAprobacion__c,TAM_FechaVigenciaFin__c,
			TAM_FechaVigenciaInicio__c,TAM_FormaAdquisicionOtro__c,TAM_FormaPagoDefinitiva__c,TAM_NombreComercial__c,TAM_NombreNuevoClienteCorporativo__c,
			TAM_NombreRL__c,TAM_Observaciones__c,TAM_OrdenCompraCartaCompromiso__c,TAM_Pais__c,TAM_PdfDatosAdicionalesRepLegal__c,TAM_PedidoFacturado__c,
			TAM_PedidoReportado__c,TAM_PorcentajeAvance__c,TAM_ProspectoCaliente__c,TAM_PruebaManejo__c,TAM_Rango2__c,
			TAM_Rango3__c,TAM_Rango__c,TAM_RazonSocial__c,TAM_Recompra__c,TAM_RFCRL__c,TAM_RFC__c,TAM_SeEntregoCotizacion__c,TAM_SolicitaPrivacidadDatosCAN__c,
			TAM_SolicitaPrivacidadDatosRL__c,TAM_TelefonoRL__c,TAM_TipoAutomovilCompra__c,TAM_TipoAutomovil__c,TAM_TipoCandidato__c,TAM_TipoCompra__c,
			TAM_Vehiculo2Rango__c,TAM_VehIculo2__c,TAM_Vehiculo3Rango__c,TAM_VehIculo3__c, FWY_codigo_distribuidor__c,
			TAM_Version2Opcional__c,TAM_Version3Opcional__c,TAM_Version__c,Telefono_cuenta__c,Title, FWY_ApellidoMaterno__c,
			FWY_Tipo_de_persona__c, TAM_IdentificacionOficial__c, TAM_ComprobanteDomicilio__c, TAM_DocumentacionSoporte__c, TAM_CatalogoRangos__c,
			TAM_CatalogoRangos__r.Name, TAM_GrupoCorporativo__c, TAM_GrupoCorporativo__r.Name, TAM_RFCC__c, Website
			//, TAM_VehiculoFact__c, TAM_VehiculoReport__c,TAM_VehiculoRango__c, TAM_VinVehiculo__c
			,(Select Id, RecordTypeId, RecordType.Name, TAM_Estatus__c, TAM_FechaEnvioAprobacion__c, TAM_NombreRL__c, TAM_Comentarios__c,
				Name From SolicitudesAprobar__r Where (RecordTypeId =: VaRtSolFlotilla or RecordTypeId =: VaRtSolPrograma) 
				And TAM_Estatus__c = 'Proceso' Order by CreatedDate DESC )
			FROM Lead Where id = :recordId]){
			
			//Valida los datos para prpgrama
			//candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Programa'
			if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Programa'){		
				//Valida la lista de campos que son obligatorios para los datos del prospecto
				if (candidato.FirstName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Nombre(s)'));	
				if (candidato.LastName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Paterno'));	
				if (candidato.FWY_ApellidoMaterno__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Materno'));					
				if (candidato.TAM_RFCC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'RFC'));
				if (candidato.Email == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Correo electrónico'));
				if (candidato.Phone == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Teléfono'));
				if (candidato.TAM_CatalogoProgramas__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Programa'));
	
				//Valida la lista de campos que son obligatorios para la Dirección del Prospecto
				if (candidato.TAM_Calle__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Calle'));				
				if (candidato.TAM_Colonia__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Colonia'));		
				if (candidato.TAM_CodigoPostal__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Código postal'));
				if (candidato.TAM_Ciudad__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Ciudad'));
				if (candidato.TAM_Estado__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Estado'));	
					
				//Valida la lista de campos que son obligatorios para los archivos adjuntos del Prospecto
				if (!candidato.TAM_IdentificacionOficial__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Identificación oficial'));				
				if (!candidato.TAM_ComprobanteDomicilio__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Comprobante de domicilio'));	
				if (!candidato.TAM_OrdenCompraCartaCompromiso__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Orden de compra'));

				if (candidato.TAM_RFCC__c != null)				
					sRFC = candidato.TAM_RFCC__c;
			}//Fin si candidato.TAM_TipoCandidato__c == 'Programa'
			
			//(candidato.FWY_Tipo_de_persona__c == 'Persona moral' || candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') && candidato.TAM_TipoCandidato__c == 'Programa'
			if ( (candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') 
				&& candidato.TAM_TipoCandidato__c == 'Programa'){		
				//Valida la lista de campos que son obligatorios para los datos del prospecto
				if (candidato.FirstName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Nombre(s)'));	
				if (candidato.LastName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Paterno'));	
				if (candidato.FWY_ApellidoMaterno__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Materno'));					
				if (candidato.TAM_RFCC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'RFC'));
				if (candidato.Email == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Correo electrónico'));
				if (candidato.Phone == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Teléfono'));
				if (candidato.TAM_CatalogoProgramas__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Programa'));

				if (candidato.TAM_RazonSocial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Razón Social'));
				if (candidato.TAM_NombreComercial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Nombre Comercial'));
				if (candidato.TAM_RFC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'RFC'));			
				//if (candidato.TAM_CorreoElectronico__c == null)
				//	lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Correo electrónico'));
				//if (candidato.Telefono_cuenta__c == null)
				//	lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Teléfono'));
		
				//Valida la lista de campos que son obligatorios para la Dirección del Prospecto
				if (candidato.TAM_Calle__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Calle'));				
				if (candidato.TAM_Colonia__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Colonia'));		
				if (candidato.TAM_CodigoPostal__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Código postal'));
				if (candidato.TAM_Ciudad__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Ciudad'));
				if (candidato.TAM_Estado__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Prospecto', 'Estado'));	
					
				//Valida la lista de campos que son obligatorios para los archivos adjuntos del Prospecto
				if (!candidato.TAM_CedulaFiscal__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Cedula Fiscal'));	
				if (!candidato.TAM_OrdenCompraCartaCompromiso__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Orden de compra'));
				if (!candidato.TAM_IdentificacionOficial__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Identificación oficial'));				
				if (!candidato.TAM_ComprobanteDomicilio__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Comprobante de domicilio'));	
				
				//Ve si tiene algo el RFC
				if (candidato.TAM_RFC__c != null)				
					sRFC = candidato.TAM_RFC__c;
			}//Fin si (candidato.FWY_Tipo_de_persona__c == 'Persona moral' || candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') && candidato.TAM_TipoCandidato__c == 'Programa'
			
			//candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Flotilla'	
			if (candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Flotilla'){		
				//Valida la lista de campos que son obligatorios para los datos del prospecto
				if (candidato.FirstName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Nombre(s)'));	
				if (candidato.LastName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Paterno'));	
				if (candidato.FWY_ApellidoMaterno__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Materno'));					
				if (candidato.Email == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Correo electrónico'));
				if (candidato.Phone == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Teléfono'));
				if (candidato.TAM_RFCC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'RFC'));
				if (candidato.TAM_CatalogoRangos__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Rango'));

				//Valida la lista de campos que son obligatorios para la Dirección del Cliente Corporativo
				if (candidato.TAM_Calle__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Calle'));				
				if (candidato.TAM_Colonia__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Colonia'));		
				if (candidato.TAM_CodigoPostal__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Código postal'));
				if (candidato.TAM_Ciudad__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Ciudad'));
				if (candidato.TAM_Estado__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Estado'));

				//Valida la lista de campos que son obligatorios para los archivos adjuntos del Prospecto
				if (!candidato.TAM_IdentificacionOficial__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Identificación oficial'));				
				if (!candidato.TAM_ComprobanteDomicilio__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Comprobante de domicilio'));	
				if (!candidato.TAM_OrdenCompraCartaCompromiso__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Orden de compra'));

				//Ve si tiene algo el RFC
				if (candidato.TAM_RFCC__c != null)				
					sRFC = candidato.TAM_RFCC__c;
			}// Fin si candidato.FWY_Tipo_de_persona__c == 'Person física' && candidato.TAM_TipoCandidato__c == 'Flotilla'
					
			//candidato.FWY_Tipo_de_persona__c == 'Persona moral' && (candidato.TAM_TipoCandidato__c == 'Flotilla' || candidato.TAM_TipoCandidato__c == 'Programa')
			if (candidato.FWY_Tipo_de_persona__c == 'Persona moral' && (candidato.TAM_TipoCandidato__c == 'Flotilla' || candidato.TAM_TipoCandidato__c == 'Programa')){	

				//Valida la lista de campos que son obligatorios para los datos del prospecto
				if (candidato.FirstName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Nombre(s)'));	
				if (candidato.LastName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Paterno'));	
				if (candidato.FWY_ApellidoMaterno__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Materno'));					
				if (candidato.Email == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Correo electrónico'));
				if (candidato.Phone == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Teléfono'));
					
				if (candidato.TAM_CatalogoRangos__c == null && candidato.TAM_TipoCandidato__c == 'Flotilla')
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Rango'));
				if (candidato.TAM_CatalogoProgramas__c == null && candidato.TAM_TipoCandidato__c == 'Programa')
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Programa'));
												
				if (candidato.TAM_RazonSocial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Razón Social'));
				if (candidato.TAM_NombreComercial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Nombre Comercial'));
				if (candidato.TAM_RFC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'RFC'));			
				if (candidato.TAM_CorreoElectronico__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Correo electrónico'));
				if (candidato.Telefono_cuenta__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Teléfono'));
				
				//Valida la lista de campos que son obligatorios para la Dirección del Cliente Corporativo
				if (candidato.TAM_Calle__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Calle'));				
				if (candidato.TAM_Colonia__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Colonia'));		
				if (candidato.TAM_CodigoPostal__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Código postal'));
				if (candidato.TAM_Ciudad__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Ciudad'));
				if (candidato.TAM_Estado__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Estado'));	
					
				//Valida la lista de campos que son obligatorios para los datos del RL
				if (candidato.TAM_NombreRL__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Representante Legal', 'Nombre(s)'));	
				if (candidato.TAM_ApellidoPaternoRL__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Representante Legal', 'Apellido Paterno'));	
				if (candidato.TAM_ApellidoMaternoRL__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Representante Legal', 'Apellido Materno'));	
				if (candidato.TAM_RFCRL__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Representante Legal', 'RFC'));
				if (candidato.TAM_CorreoElectronicoRL__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Representante Legal', 'Correo electrónico'));
					
				//Valida la lista de campos que son obligatorios para los archivos adjuntos del Prospecto
				if (!candidato.TAM_ActaConstitutiva__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Acta Constitutiva'));				
				if (!candidato.TAM_CedulaFiscal__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Cedula Fiscal'));	
				if (!candidato.TAM_OrdenCompraCartaCompromiso__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Orden de compra'));
				//if (!candidato.TAM_PdfDatosAdicionalesRepLegal__c)
				//	lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'PDF datos adicionales'));						

				//Ve si tiene algo el RFC
				if (candidato.TAM_RFC__c != null)				
					sRFC = candidato.TAM_RFC__c;
			}//Fin si candidato.TAM_TipoCandidato__c == 'Programa'

			if ((candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial') && candidato.TAM_TipoCandidato__c == 'Flotilla'){	

				//Valida la lista de campos que son obligatorios para los datos del prospecto
				if (candidato.FirstName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Nombre(s)'));	
				if (candidato.LastName == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Paterno'));	
				if (candidato.FWY_ApellidoMaterno__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Apellido Materno'));					
				if (candidato.Email == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Correo electrónico'));
				if (candidato.Phone == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Teléfono'));
				if (candidato.TAM_CatalogoRangos__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Prospecto', 'Rango'));
									
				if (candidato.TAM_RazonSocial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Razón Social'));
				if (candidato.TAM_NombreComercial__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Nombre Comercial'));
				if (candidato.TAM_RFC__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'RFC'));			
				//if (candidato.TAM_CorreoElectronico__c == null)
				//	lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Correo electrónico'));
				//if (candidato.Telefono_cuenta__c == null)
				//	lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'Teléfono'));
				
				//Valida la lista de campos que son obligatorios para la Dirección del Cliente Corporativo
				if (candidato.TAM_Calle__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Calle'));				
				if (candidato.TAM_Colonia__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Colonia'));		
				if (candidato.TAM_CodigoPostal__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Código postal'));
				if (candidato.TAM_Ciudad__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Ciudad'));
				if (candidato.TAM_Estado__c == null)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Dirección del Cliente Corporativo', 'Estado'));	
										
				if (!candidato.TAM_CedulaFiscal__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Cedula Fiscal'));	
				if (!candidato.TAM_OrdenCompraCartaCompromiso__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Orden de compra'));
				if (!candidato.TAM_IdentificacionOficial__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Identificación oficial'));				
				if (!candidato.TAM_ComprobanteDomicilio__c)
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Archivos Adjuntos del Prospecto', 'Comprobante de domicilio'));	

                //Ve si tiene algo el RFC
                if (candidato.TAM_RFC__c != null)               
                    sRFC = candidato.TAM_RFC__c;

			}//Fin si candidato.TAM_TipoCandidato__c == 'Programa'

	    	System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato candidato.SolicitudesAprobar__r:  ' + candidato.SolicitudesAprobar__r);			
			TAM_SolicitudesParaAprobar__c objSolAprobar = new TAM_SolicitudesParaAprobar__c();
			Boolean bSolicitudesAprobar = false;
			//Consulta las solicitudes en proceso
			if (!candidato.SolicitudesAprobar__r.isEmpty()){
				for (TAM_SolicitudesParaAprobar__c objSol : candidato.SolicitudesAprobar__r){
					if (candidato.TAM_TipoCandidato__c == 'Programa' && objSol.RecordTypeId == VaRtSolPrograma){
						objSolAprobar = objSol;
						bSolicitudesAprobar = true;
						break;	
					}//Fin si candidato.TAM_TipoCandidato__c == 'Programa'
					if (candidato.TAM_TipoCandidato__c == 'Flotilla' && objSol.RecordTypeId == VaRtSolFlotilla){
						objSolAprobar = objSol;
						bSolicitudesAprobar = true;
						break;							
					}//Fin si candidato.TAM_TipoCandidato__c == 'Programa'
				}//Fin del for para candidato.SolicitudesAprobar__r
	    		System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato objSolAprobar:  ' + objSolAprobar);
			}//Fin si !candidato.SolicitudesAprobar__r.isEmpty()
			
			//Ve si existen un cliente corporativo con el mismo REF
			if (sRFC != null){
				String ssRFCPaso = sRFC.toUpperCase();
				for(Account CteCorp : [Select id, TAM_IdExternoNombre__c From Account Where RFC__c =:ssRFCPaso 
					And (recordTypeId = :VaRtAccRegPMCC OR recordTypeId = :VaRtAccRegPFCC OR recordTypeId = :VaRtCteMoralNoVigente OR recordTypeId = :VaRtCteFisicaNoVigente)]){
                    if (CteCorp.recordTypeId == VaRtAccRegPMCC || CteCorp.recordTypeId == VaRtAccRegPFCC)					    
					   lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'El RFC: ' + ssRFCPaso + ' ya existe con el Cliente Corporativo: ' + CteCorp.TAM_IdExternoNombre__c));			
                    if (CteCorp.recordTypeId == VaRtCteMoralNoVigente || CteCorp.recordTypeId == VaRtCteFisicaNoVigente)                                             
                        lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'El RFC: ' + ssRFCPaso + ' ya existe con el Cliente: ' + CteCorp.TAM_IdExternoNombre__c + ', <br> debes solicitar una reactivación'));
				}
			
	            /*//Consulta las solicitudes de flotilla de Edoardo y ve si hay un cancelda con el RFC que se esta dando de alta
	            for (TAM_SolicitudesParaAprobar__c sol : [Select t.TAM_RFCRef__c, t.TAM_Estatus__c, t.RecordType.Name, t.OwnerId, t.Name, t.CreatedDate  
	                    From TAM_SolicitudesParaAprobar__c t
	                    where TAM_Estatus__c IN ('Rechazado', 'Activación', 'Autorizado')
	                    and TAM_RFCRef__c = :sRFC and OwnerId != :usuarioActual.id
	                    Order by CreatedDate DESC, TAM_Estatus__c LIMIT 1]){
	                //Ve si el estatus de la solicitud es Rechazado
	                if (sol.TAM_Estatus__c == 'Rechazado'){
                        lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', 'El RFC: ' + ssRFCPaso + ' ya existe con el Cliente: ' + sol.Name));	                   
                        lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', ' y tiene una solicitud rechazada, ponte en contacto con el area de DTM : '));                      
                        lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion del Cliente Corporativo', ' Edoardo Sandoval Rivera, edoardo@dtmac.com.mx'));                      
	                }//Fin si sol.TAM_Estatus__c == 'Rechazado'
	            }//Fin del for para TAM_SolicitudesParaAprobar__c*/
				
			}//fin si sRFC != null

            System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato FWY_Tipo_de_persona__c:  ' + candidato.FWY_Tipo_de_persona__c + ' TAM_CatalogoRangos__c: ' + candidato.TAM_CatalogoRangos__c + ' VaRtCteRangoFlot: ' + VaRtCteRangoFlot);
            //Si se trata de una persona fisica y selecciono un rango que aplica para Flotillas marca eun error			
            if (candidato.FWY_Tipo_de_persona__c == 'Person física'){
                if (mapRangos.get(candidato.TAM_CatalogoRangos__c) == VaRtCteRangoFlot)
                   lwrpDatosFaltantes.add(new wrpDatosFaltantes('Informacion Venta Corporativa: Tipo de persona.', 'Solo puedes seleccionar rangos que aplican para programas, si eres una Persona Física o Física con actividad empresarial.'));                
            }//Fin si candidato.FWY_Tipo_de_persona__c == 'Fisica con actividad empresarial' || candidato.FWY_Tipo_de_persona__c == 'Person física'
									
			//Inicializa el objeto del tipo wrpDatosCndidato
    		objWrpDatosCndidato = new wrpDatosCndidato(candidato.TAM_TipoCandidato__c, candidato, lwrpDatosFaltantes, objSolAprobar
    			, new Account(), bSolicitudesAprobar);
    	}
    	
        }//Fin si recordId.startsWith('00Q')
        
        //Si se trata de Account
        if (recordId.startsWith('001')){
        	Account ClienteCons;
        	TAM_SolicitudesParaAprobar__c objSolAprobar = new TAM_SolicitudesParaAprobar__c();
        	for (Account cliente : [Select id, Name, FirstName, LastName, TAM_ProgramaRango__c,
        		TAM_IdExternoNombre__pc, TAM_IdExternoNombre__c, TAM_FechaVigenciaInicio__c, Apellido_Materno__c,
        		TAM_FechaVigenciaFin__c, TAM_EstatusCliente__c, TAM_CartaCompromiso__c, TAM_OrdenCompra__c, 
        		TAM_ProgramaRango__r.Name, TAM_EnviarPreautorizacion__c, BillingStreet, BillingState, BillingPostalCode, 
        		BillingLongitude, BillingLatitude, BillingGeocodeAccuracy, BillingCountry, BillingCity, Colonia__c,
        		BillingAddress, TAM_NombreRL__c, TAM_ApellidoPaternoRL__c, TAM_ApellidoMaternoRL__c, TAM_RFCRL__c, 
        		TAM_TelefonoRL__c, TAM_CorreoElectronicoRL__c, TAM_DireccionRL__c, RFC__c, TAM_NombreComercial__c,
        		Correo_Electronico__c, Phone, DenominacionSocial__c, Website  
        		From Account Where id =:recordId ]){
        		//Inicializa el objeto del cliente
        		ClienteCons = cliente;	
        		ClienteCons.TAM_EnviarPreautorizacion__c = cliente.TAM_EnviarPreautorizacion__c != null ? cliente.TAM_EnviarPreautorizacion__c : false;
        		System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato TAM_EnviarPreautorizacion__c:  ' + ClienteCons.TAM_EnviarPreautorizacion__c);
        		//Ve si cargo alguno de los dos archivos.
        		if (!cliente.TAM_CartaCompromiso__c && !cliente.TAM_OrdenCompra__c) 
					lwrpDatosFaltantes.add(new wrpDatosFaltantes('Debes cargar uno de los dos archivos ', 'Orden de compra Carca Compromiso'));        		
        	}	

			//Inicializa el objeto del tipo wrpDatosCndidato
    		objWrpDatosCndidato = new wrpDatosCndidato('ClienteVencido', new Lead(), lwrpDatosFaltantes, objSolAprobar
    			, ClienteCons, false);
        	
        }//Fin si recordId.startsWith('001')
        
    	System.debug('EN TAM_CargaArchivosCtrl.getDatosCandidato objWrpDatosCndidato:  ' + objWrpDatosCndidato);
    	//Regresa el objeto del tipp wrpDatosCndidato
        return objWrpDatosCndidato;  
    } 
        	
	public class wrpDatosCndidato {
		@AuraEnabled public String sTipoPersona {get;set;}		
		@AuraEnabled public Lead Candidato {get;set;}
		@AuraEnabled public Account Cliente {get;set;}				
		@AuraEnabled public List<wrpDatosFaltantes> lWrpDatosFaltantes {get;set;}	
		@AuraEnabled public TAM_SolicitudesParaAprobar__c SolicitudAprobar {get;set;}		
		@AuraEnabled public Boolean bSolicitudesAprobar {get;set;}		
						
		public wrpDatosCndidato(String sTipoPersona, Lead Candidato, List<wrpDatosFaltantes> lWrpDatosFaltantes,
			TAM_SolicitudesParaAprobar__c SolicitudAprobar, Account Cliente, Boolean bSolicitudesAprobar){
			this.sTipoPersona = sTipoPersona;
			this.Candidato = Candidato;
			this.lWrpDatosFaltantes = lWrpDatosFaltantes;
			this.SolicitudAprobar = SolicitudAprobar;
			this.Cliente = Cliente;
			this.bSolicitudesAprobar = bSolicitudesAprobar;
		}
	}

	public class wrpDatosFaltantes {
		@AuraEnabled public String sSeccion {get;set;}
		@AuraEnabled public String sNombreCampo {get;set;}		
					
		public wrpDatosFaltantes(String sSeccion, String sNombreCampo){
			this.sSeccion = sSeccion;
			this.sNombreCampo = sNombreCampo;
		}
	}

    @AuraEnabled  
    public static void enviaSolicitudAproba(string recordId){
   		System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudAproba recordId:  ' + recordId);
    	Lead objLeadUpd;
    	for (Lead candidato : [Select id, TAM_EnviarAutorizacion__c, TAM_TipoCandidato__c From Lead Where id = :recordId]){
    		candidato.TAM_EnviarAutorizacion__c = true;
			if (candidato.TAM_TipoCandidato__c == 'Programa')
				candidato.recordTypeId = VaRtLeadPrograma;	
			if (candidato.TAM_TipoCandidato__c == 'Flotilla')
				candidato.recordTypeId = VaRtLeadlotilla;
			candidato.Status = 'Autorizacion de Cliente';	
    		objLeadUpd = candidato;
    	}
        
        //Ve si selecciono un Programa/Rango diferente 
        
    	System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudAproba objLeadUpd:  ' + objLeadUpd);
    	//Actualiza el registro del Lead
    	if (objLeadUpd != null){
    		Database.SaveResult dtSvr = Database.update(objLeadUpd);
    		if (!dtSvr.isSuccess())
    			System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudAproba ERROR:  ' + dtSvr.getErrors()[0].getMessage());
    	}    	 
    }      


    @AuraEnabled  
    public static void enviaSolicitudPreAuto(string recordId){
   		System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudPreAuto recordId:  ' + recordId);
    	Account objAccountUpd;
    	for (Account cliente : [Select id, TAM_EnviarPreautorizacion__c, RecordTypeId 
    		From Account Where id = :recordId]){
    		cliente.TAM_EnviarPreautorizacion__c = true;    		
			cliente.TAM_EstatusCliente__c = 'Preautorización';	
    		objAccountUpd = cliente;
    	}
    	
    	System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudPreAuto objAccountUpd:  ' + objAccountUpd);
    	//Actualiza el registro del Lead
    	if (objAccountUpd != null){
    		Database.SaveResult dtSvr = Database.update(objAccountUpd);
    		if (!dtSvr.isSuccess())
    			System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudPreAuto ERROR:  ' + dtSvr.getErrors()[0].getMessage());
    	}    	 
    }      

    @AuraEnabled  
    public static void enviaSolicitudActiva(string recordId){
   		System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudActiva recordId:  ' + recordId);
    	Account objAccountUpd;
    	for (Account cliente : [Select id, TAM_EnviarPreautorizacion__c, RecordTypeId 
    		From Account Where id = :recordId]){
    		cliente.TAM_SolcitaActivacion__c = true;    		
			cliente.TAM_EstatusCliente__c = 'Activación';	
    		objAccountUpd = cliente;
    	}
    	
    	System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudActiva objAccountUpd:  ' + objAccountUpd);
    	//Actualiza el registro del Lead
    	if (objAccountUpd != null){
    		Database.SaveResult dtSvr = Database.update(objAccountUpd);
    		if (!dtSvr.isSuccess())
    			System.debug('EN TAM_CargaArchivosCtrl.enviaSolicitudActiva ERROR:  ' + dtSvr.getErrors()[0].getMessage());
    	}    	 
    }      

    
}