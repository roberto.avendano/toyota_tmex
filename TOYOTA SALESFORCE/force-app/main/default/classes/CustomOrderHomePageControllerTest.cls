@isTest
private class CustomOrderHomePageControllerTest {
    
    public static Order getCurrentOrder(){
        RecordType rtParte = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Parte' and IsActive=true];
        RecordType rtProductoFinal = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='ProductoFinal' and IsActive=true];
        RecordType rtDealer = [Select Id FROM RecordType WHERE SobjectType='Account' and DeveloperName='Dealer' and IsActive=true];
        Id standardPricebookId = Test.getStandardPricebookId();
        
        Account cliente = new Account();
            cliente.Name='TOYOTA GUANAJUATO';
            cliente.RecordTypeId = rtDealer.Id;
        insert cliente;

        Vehiculo__c vehicle= new Vehiculo__c(
            Celular__c= '123456789',
            WorkId__c = '123456789'
        );
        insert vehicle;

        ParametrosConfiguracionToyota__c pc = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosDestino',
            Valor__c='PRListaPreciosDestino',
            Consecutivo__c = 999
        );
        insert pc;

        Product2 miProd= new Product2();
            miProd.Name='Test001';
            miProd.IdExternoProducto__c='Test001';
            miProd.ProductCode='Test001';
            miProd.Description= 'Test product2';
            miProd.CantidadMaxima__c = 5;
            miProd.PartesRobadas__c = true;
            miProd.RecordTypeId = rtParte.Id;
            miProd.IsActive = true;
        insert miProd;

        Product2 miProdFinal = new Product2();
            miProdFinal.Name='Test02';
            miProdFinal.IdExternoProducto__c='Test02';
            miProdFinal.ProductCode='Test02';
            miProdFinal.Description= 'Test product2';
            miProdFinal.CantidadMaxima__c = 5;
            miProdFinal.PartesRobadas__c = true;
            miProdFinal.RecordTypeId = rtProductoFinal.Id;
            miProdFinal.IsActive = true;

        insert miProdFinal;
        
        ProductoFinalComponente__c pcFinal = new ProductoFinalComponente__c(
            Producto__c = miProd.Id,
            ProductoFinal__c = miProdFinal.Id,
            Name='Test'
        );
        insert pcFinal;

        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IdExternoListaPrecios__c='testId3', 
            isActive=true
        );
        insert customPB;

        PricebookEntry pbePro1Estandard = new PricebookEntry();
            pbePro1Estandard.Pricebook2Id= standardPricebookId;
            pbePro1Estandard.Product2Id= miProd.Id;
            pbePro1Estandard.UnitPrice= 0.0;
            pbePro1Estandard.IdExterno__c = 'expbe1';
            pbePro1Estandard.IsActive = true;
        insert pbePro1Estandard; 

        PricebookEntry pbePro1CPB = new PricebookEntry(
                Pricebook2Id= customPB.Id,
                Product2Id= miProd.Id,
                UnitPrice= 0.0,
                IdExterno__c = 'expbe2',
                IsActive = true
            );
        insert pbePro1CPB; 

        PricebookEntry pbePro2Estandard= new PricebookEntry();
            pbePro2Estandard.Pricebook2Id= standardPricebookId;
            pbePro2Estandard.Product2Id= miProdFinal.Id;
            pbePro2Estandard.UnitPrice= 0.0;
            pbePro2Estandard.IdExterno__c = 'expbe3';
        insert pbePro2Estandard;  

        PricebookEntry pbePro2CPB= new PricebookEntry();
            pbePro2CPB.Pricebook2Id= customPB.Id;
            pbePro2CPB.Product2Id= miProdFinal.Id;
            pbePro2CPB.UnitPrice= 0.0;
            pbePro2CPB.IdExterno__c = 'expbe4';
        insert pbePro2CPB;    

        Order miOrden = new Order();
            miOrden.AccountId= cliente.Id;
            miOrden.EffectiveDate= Date.today();
            miOrden.Status='En proceso';
            miOrden.Vehiculo__c=vehicle.Id;
            miOrden.Pricebook2Id= customPB.Id;
        insert miOrden;

        OrderItem prod_pedido = new OrderItem();
            prod_pedido.UnitPrice = 1200;
            prod_pedido.Quantity = 3;
            prod_pedido.OrderId = miOrden.Id;
            prod_pedido.PricebookEntryId = pbePro1CPB.Id;
        insert prod_pedido;

        return miOrden;
    }

    @isTest static void test_method_one() {
        Order currentOrder = CustomOrderHomePageControllerTest.getCurrentOrder();        
        ApexPages.StandardController controller = new ApexPages.StandardController(currentOrder);
        CustomOrderHomePageController currentPage = new CustomOrderHomePageController(controller);
        
        currentPage.filter='Test';
        currentPage.buscar();
        for(Integer i=0; i<currentPage.returnProds.size();i++){
            currentPage.returnProds[i].selected = true;
        }

        currentPage.seleccionar();

        currentPage.indexUnSelect = currentPage.returnProds[0].priceEntry.Id;
        currentPage.unSelect();
        currentPage.indexUnSelect = currentPage.returnProds[0].priceEntry.Id;
        currentPage.unSelect();
        currentPage.filter='Test';
        currentPage.buscar();
        for(Integer i=0; i<currentPage.returnProds.size();i++){
            currentPage.returnProds[i].selected = true;
        }
        
        currentPage.seleccionar();
        currentPage.guardar();
        currentPage.iniciarVariables();
        currentPage.agregarRegresar();
        currentPage.getProdRepetidosSize();
        currentPage.validaProdRepetido();

        currentPage.orderItemID = [SELECT Id FROM OrderItem WHERE OrderId=: controller.getId() LIMIT 1].Id;
        currentPage.deleteOrderItem();

        
        Pricebook2 pb = [SELECT Id, Name FROM Pricebook2 WHERE Id=:currentOrder.Pricebook2Id];
        if(pb!=null){
            List<OrderItem> items = [SELECT Id, PricebookEntry.Pricebook2Id FROM OrderItem WHERE PricebookEntry.Pricebook2Id =: pb.Id];
            
            try{
                delete items;
                delete pb;
                currentOrder = CustomOrderHomePageControllerTest.getCurrentOrder();
                controller = new ApexPages.StandardController(currentOrder);
                currentPage = new CustomOrderHomePageController(controller);

            } catch(DmlException e){
                System.debug(e.getMessage());
            }                        
        }

    }
    
}