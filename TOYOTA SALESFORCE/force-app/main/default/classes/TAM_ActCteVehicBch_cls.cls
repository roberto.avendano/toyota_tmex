/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        Vehiculo__c y verificar que exista el cliente y su relacion con
                        Opp, Opp-Factura, Opp-Vehic, Cte-Vehiv.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCteVehicBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global String sQuery {get;set;}

    //Un constructor por default
    global TAM_ActCteVehicBch_cls(string query){
        this.sQuery = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActCteVehicBch_cls.start sQuery: ' + this.sQuery);
        return Database.getQueryLocator(this.sQuery);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Vehiculo__c> scope){ 
        System.debug('EN TAM_ActCteVehicBch_cls.');

        Map<String, List<Vehiculo__c>> mapDistrEntrega = new Map<String, List<Vehiculo__c>>();
        Map<String, Vehiculo__c> mapVinObjVehicPaso = new Map<String, Vehiculo__c>();
        Map<String, Vehiculo__c> mapVinObjVehicPasoCancel = new Map<String, Vehiculo__c>();
        Map<String, Set<String>> mapSolDealer = new Map<String, Set<String>>();
        Set<String> setDealers = new Set<String>();        
        Map<String, TAM_ConsiltaDatosClienteVIN__c> mapVinObjConsDatCteIns = new Map<String, TAM_ConsiltaDatosClienteVIN__c>();
        
        //Recorre los registros del objeto  Vehiculo__c y ve cuales no estan autorizados o cancelados
        for (Vehiculo__c objVehicCons : scope){
            //Ve si el estatus es Pendiente
            if (objVehicCons.Name != null)
                mapVinObjVehicPaso.put(objVehicCons.Name, objVehicCons);
        }//Fin del for para scope
        System.debug('EN TAM_ActCteVehicBch_cls mapVinObjVehicPaso: ' + mapVinObjVehicPaso.keyset());
        System.debug('EN TAM_ActCteVehicBch_cls mapVinObjVehicPaso: ' + mapVinObjVehicPaso.values());

        //Recorre la lista de vines del mapa mapVinObjVehicPaso y metelos al objeto de paso TAM_ConsiltaDatosClienteVIN__c
        for (String sVinPaso : mapVinObjVehicPaso.keyset()){
            mapVinObjConsDatCteIns.put(sVinPaso, new TAM_ConsiltaDatosClienteVIN__c(
                    Name = sVinPaso, TAM_SinCliente__c = false
                )
            );
        }//Fin del for para mapVinObjVehicPaso.keyset()
        System.debug('EN TAM_ActCteVehicBch_cls mapVinObjConsDatCteIns: ' + mapVinObjConsDatCteIns.keyset());
        System.debug('EN TAM_ActCteVehicBch_cls mapVinObjConsDatCteIns: ' + mapVinObjConsDatCteIns.values());
        
        //Ya tienes todos los registros que tienes que cancelar 
        Savepoint sp = Database.setSavepoint();        
        
        for (TAM_ConsiltaDatosClienteVIN__c objPasoIns : mapVinObjConsDatCteIns.values()){
            System.debug('EN TAM_ActCteVehicBch_cls objPasoIns: ' + objPasoIns);
            //Crea el registro
            insert objPasoIns;
        }
        
        //Rollback por pruebas            
        //Database.rollback(sp);
                
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActCteVehicBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}