@isTest
public class SolicitudSiteAttachmentControllerTest {
    
    @isTest
    static void test_one(){
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        Date inicio = date.parse('01/03/2018');
        Date fin = date.parse('31/03/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND DeveloperName='Dealer'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                             VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='ToyotaGroup'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Departamento__c = 'Human Resources',
                                Codigo__c = '456RTY',
                                Estatus__c = 'Activo',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                RecordTypeId = conRT.Id);
        insert c;
        
        RecordType sivRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='SolicitudInternaVehiculos__c' 
                            AND DeveloperName='PoolAsignacion'];
        
        SolicitudInternaVehiculos__c siv = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                            RecordTypeId = sivRT.Id,
                                                                            Estatus__c='Nuevo');
        insert siv;
        
        Attachment attach=new Attachment(
            Name = 'Solicitud Site Attachment TEST', 
            body = bodyBlob,
            parentId = siv.Id);   	
        insert attach;
        
        
        //Se comentan lineas del insert de content version
        /*
        Contentversion cv = new Contentversion();
		cv.Title = 'ABC';
		cv.PathOnClient = 'test';
		cv.VersionData = EncodingUtil.base64Decode(EncodingUtil.urlEncode('Unit Test Attachment Body','UTF-8'));
		cv.FirstPublishLocationId = siv.Id;
		insert cv;*/
        
        test.startTest();
		
        PageReference page = System.Page.SolicitudSiteAttachments;
        page.getParameters().put('id', String.valueOf(siv.Id));
        Test.setCurrentPage(page);
        SolicitudSiteAttachmentController ssac = new SolicitudSiteAttachmentController();
        ssac.getAttachments();  
        ssac.cargarArchivo();  
        ssac.adjuntarArchivo();
        Attachment att = new Attachment();
        att = ssac.attachment;
        
        test.stopTest();
    }
}