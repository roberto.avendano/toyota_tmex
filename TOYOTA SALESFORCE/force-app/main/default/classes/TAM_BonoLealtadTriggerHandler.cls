public class TAM_BonoLealtadTriggerHandler {

    /***** INSERT *****/
    public static void getProducts(List<Series_Bono_Lealtad__c> lstNewBonoLealtad){
        
        for(Series_Bono_Lealtad__c objSerieBonoLealtad : lstNewBonoLealtad){
            getModelos(objSerieBonoLealtad);
        }
    }
    
    /*Obtener Todos los Productos Relacionados a la Serie*/
    public static void getModelos(Series_Bono_Lealtad__c objSerieBonoLealtad){
        String strSerie = objSerieBonoLealtad.SerieFormula__c;
        String strAnio = objSerieBonoLealtad.Anio_Modelo__c;
        
        List<Product2> lstModelos = [SELECT	Id FROM	Product2 WHERE  Serie__r.Name =:strSerie AND Anio__c =:strAnio];
        List<Modelos_Bono_Lealtad__c> lstModelosLealtad = new List<Modelos_Bono_Lealtad__c>();
        
        for(Product2 objProducto : lstModelos){
            Modelos_Bono_Lealtad__c objModelosBonoLealtad = new Modelos_Bono_Lealtad__c();
            objModelosBonoLealtad.Modelo__c = objProducto.Id;
            objModelosBonoLealtad.Serie_Bono_Lealtad__c = objSerieBonoLealtad.Id;
            if(objSerieBonoLealtad.Aplica_Todos_los_Modelos__c == 'SI'){
                objModelosBonoLealtad.Bono_TMEX__c = objSerieBonoLealtad.Bono_TMEX__c;
                objModelosBonoLealtad.Bono_Distribuidor__c = objSerieBonoLealtad.Bono_Distribuidor__c;
                objModelosBonoLealtad.Bono_TFS__c = objSerieBonoLealtad.Bono_TFS__c;
                objModelosBonoLealtad.Comentarios__c = objSerieBonoLealtad.Comentarios__c;
            }
            lstModelosLealtad.add(objModelosBonoLealtad);
        }
        
        try {
            Database.insert(lstModelosLealtad, false);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
    }
}