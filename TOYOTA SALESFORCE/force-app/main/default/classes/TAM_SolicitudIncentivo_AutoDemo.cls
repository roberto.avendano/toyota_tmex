/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Controlador Solicitud de Autos Demo.
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		27/Agosto/2020          Versión Inicial
Cecilia Cruz Morán        		23/Septiembre/2021      Se agrega reembolso de intereses
__________________________________________________________________________________
*********************************************************************************/
public without sharing  class TAM_SolicitudIncentivo_AutoDemo {

    @AuraEnabled
    public static Boolean saveRecord(String objRecord){
        TAM_SolicitudAutoDemo__c objSolicitudAutoDemo = new TAM_SolicitudAutoDemo__c();
        TAM_SearchAutoDemo.wrapVIN objWrapper = new TAM_SearchAutoDemo.wrapVIN();
        
        if(objRecord != null){
            objWrapper = (TAM_SearchAutoDemo.wrapVIN) JSON.deserialize(objRecord,TAM_SearchAutoDemo.wrapVIN.class);
            
            objSolicitudAutoDemo.TAM_Serie__c = objWrapper.Serie;
            objSolicitudAutoDemo.TAM_AnioModelo__c = objWrapper.anioModelo;
            objSolicitudAutoDemo.TAM_Modelo__c = objWrapper.codigoModelo;
            objSolicitudAutoDemo.TAM_Factura__c = objWrapper.idFacturaCarga;
            objSolicitudAutoDemo.TAM_Movimiento__c = objWrapper.idMovimiento;
            objSolicitudAutoDemo.TAM_VIN__c = objWrapper.VINID;
            objSolicitudAutoDemo.TAM_NombreDistribuidor__c = objWrapper.nombreDealer;
            objSolicitudAutoDemo.TAM_CodigoDistribuidor__c = objWrapper.codigoDealer;
            objSolicitudAutoDemo.TAM_FechaVenta__c = objWrapper.fechaVentaVIN;
            objSolicitudAutoDemo.TAM_CodigoVenta__c = objWrapper.codigoVenta;
            objSolicitudAutoDemo.TAM_ValorFactura__c = objWrapper.dblValorFactura;
            objSolicitudAutoDemo.TAM_PrecioLista__c = objWrapper.dblPrecioLista;
            objSolicitudAutoDemo.TAM_PrecioMenosIncentivo__c = objWrapper.dblPrecioMenosIncentivo;
            objSolicitudAutoDemo.TAM_IncentivoDefinidoPoliticas__c = objWrapper.dblIncentivoPropuesto;
            objSolicitudAutoDemo.TAM_IncentivoTMEXDefinido__c = objWrapper.dblIncentivoTMEX;
            objSolicitudAutoDemo.TAM_IncentivoDealerDefinido__c = objWrapper.dblIncentivoDealer;
            objSolicitudAutoDemo.TAM_ParticipacionTMEX__c = objWrapper.intPorcentajeParticipacionTMEX;
            objSolicitudAutoDemo.TAM_IncentivoOtorgado__c = objWrapper.dblIncentivoOtorgado;
            objSolicitudAutoDemo.TAM_CostoTMEXSinIVA__c = objWrapper.dblIncentivoProporcionalTMEXSinIVA;
            objSolicitudAutoDemo.TAM_DetallePoliticaIncentivos__c =  objWrapper.idDetallePolitica;
            objSolicitudAutoDemo.TAM_AutoDemo__c = objWrapper.idAutoDemo;
            objSolicitudAutoDemo.TAM_FechaSolicitud__c = date.today();
            objSolicitudAutoDemo.TAM_ReembolsoInteres__c = objWrapper.intereses_totalInteres;
            objSolicitudAutoDemo.TAM_IdExterno__c =  objWrapper.VINID +'-'+ objWrapper.Serie + '-' + objWrapper.anioModelo + '-' + objWrapper.codigoModelo;
  
        }    
        try {
            Database.insert(objSolicitudAutoDemo);
            return true;
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return false;
        }
    }
}