/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_ReasignaMasivaLeadsCtlr.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Mayo-202          Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ReasignaMasivaLeadsCtlr_tst {

    static  Account Distrib = new Account();
    
    @TestSetup static void loadData(){
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '57000',
                TAM_GestorLeads__c = UserInfo.getUserId()
            );              
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id
            );
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive = true,
                ContactId = con.Id,
                timezonesidkey ='America/Los_Angeles', 
                username ='testOwnertfsproduc@noemail.com.test',
                TAM_ReasignacionMasivaLeads__c = true
            );
            insert user;

            RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'TAM_NuevoCandidato' and SobjectType = 'Lead'];    
                        
            Lead l2vin = new Lead();
            l2vin.RecordTypeId = rectype.id;
            l2vin.LeadSource = 'DISTRIBUIDOR Tráfico de piso'; //'Landing Page';
            l2vin.FirstName = 'Hector';
            l2vin.Email = 'hectfa@test.com';
            l2vin.phone = '5571123922';
            l2vin.Status='Nuevo Prospecto';
            l2vin.LastName = 'Hector';
            l2vin.TAM_Inventario__c  = null;
            l2vin.FWY_codigo_distribuidor__c = '57000';
            l2vin.TAM_EnviarAutorizacion__c = false;
            insert l2vin;

            TAM_UsuariosSeguimientoLead__c objUsrSeg = new TAM_UsuariosSeguimientoLead__c(
                Name = 'Hector Hector',
                TAM_IdExterno__c = 'Hector Hector-57000',
                TAM_NoDistribuidor__c = '57000',
                TAM_NombreCompleto__c = 'Hector Hector',
                TAM_NombreDistribuidor__c = 'TOYOTA POLANCO',
                TAM_ActivoSFDC__c = true
            );
            insert objUsrSeg;
            
            TAM_MaximoNumeroLeads__c testCustum = new TAM_MaximoNumeroLeads__c();
            testCustum.TAM_CodigoDealer__c = '57000';
            testCustum.Id_Externo__c = '57000';
            testCustum.TAM_MaximoLeads__c = '20';
            insert testCustum;
            
        Test.stopTest();

    }

    static testMethod void TAM_ReasignaMasivaLeadsCtlrOK() {

        //Inicia las pruebas        
        Test.startTest();
            Lead candPaso = [Select id, Name, FirstName, LastName, LeadSource, Email, phone, Status, FWY_codigo_distribuidor__c
                From Lead LIMIT 1];

            TAM_UsuariosSeguimientoLead__c objUsrSeg = [Select ID, Name, TAM_IdExterno__c, TAM_NoDistribuidor__c, 
                TAM_NombreCompleto__c, TAM_NombreDistribuidor__c, TAM_Activo__c 
                From TAM_UsuariosSeguimientoLead__c LIMIT 1];
            
            String IdClienteActual = '';
            String sPropietario = 'test123p';
            String sDistribuidor = '57000';
            String strCadenaDeBusqueda = '57000'; 
            String defaultPrm = 'true';
            String totCandConsPrm = System.Label.TAM_TotRegConsReasigLeads; 
            String sPropietarioFinalPrm = 'test123p';
            List<TAM_ReasignaMasivaLeadsCtlr.wrpCandidatos> lCandidSelecPrm = 
                new List<TAM_ReasignaMasivaLeadsCtlr.wrpCandidatos>(); 
            //Agregale un objeto del tipo 
            lCandidSelecPrm.add(new TAM_ReasignaMasivaLeadsCtlr.wrpCandidatos(
                    candPaso.ID, candPaso.Name, 'Avanxa', 'test123p', DateTime.now(), 'TMEX', 'Cotización', 'Cotización.', '57000', '5534545678'
                )
            );
            //Llama al metodo getPerfilUserActual
            TAM_ReasignaMasivaLeadsCtlr.getPerfilUserActual();
            TAM_ReasignaMasivaLeadsCtlr.getUsuariosActivos(IdClienteActual);
            TAM_ReasignaMasivaLeadsCtlr.buscaCandidatos(sPropietario, sDistribuidor, strCadenaDeBusqueda,
              defaultPrm, totCandConsPrm);
            TAM_ReasignaMasivaLeadsCtlr.tamConfirmaReasignacion(candPaso.Name, lCandidSelecPrm);
        //Terminan las pruebas
        Test.stopTest();
        
    }
}