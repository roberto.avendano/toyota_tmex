global class BatchRepo12m_Schedulable implements Schedulable {
    global static String scheduleit(){
        string strSeconds = '0';
        string strMinutes = '0';
        string strHours = '1';
        string strDay_of_month = 'L';
        string strMonth = '12';
        string strDay_of_week = '?';
        string strYear = '2019-2200';
        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;         
        BatchRepo12m_Schedulable job = new BatchRepo12m_Schedulable();
        return System.schedule('Ejecuta_BatchRepo12m',sch,job);
    }
    
    global void execute(SchedulableContext sc) {
        BatchRepo12m uc = new BatchRepo12m();
        Database.executeBatch(uc);
        
    }
}