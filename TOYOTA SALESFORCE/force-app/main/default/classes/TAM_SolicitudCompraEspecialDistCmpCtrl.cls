/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para asociar los distribuidores cuando se trata de una Venta Especial
    					Para Flotilla o Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    09-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_SolicitudCompraEspecialDistCmpCtrl {

	static String sRectorTypePasoPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();

	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();
	
    //Un constructor por default
    public TAM_SolicitudCompraEspecialDistCmpCtrl(){}
    
   //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_OrdenDeCompraWrapperClass getWrpModelo(String sWrpModelo) {
    	System.debug('EN getWrpModelo: ' + sWrpModelo);
    	String sCadenaPaso = '';
    	
    	//Serializa el JSON sobre la clase de TAM_OrdenDeCompraWrapperClass
		TAM_OrdenDeCompraWrapperClass objTAM_OrdenDeCompraWrapperClassPaso = new TAM_OrdenDeCompraWrapperClass();
		objTAM_OrdenDeCompraWrapperClassPaso = (TAM_OrdenDeCompraWrapperClass) JSON.deserialize(sWrpModelo, TAM_OrdenDeCompraWrapperClass.class);
		System.debug('EN getWrpModelo objTAM_OrdenDeCompraWrapperClassPaso: ' + objTAM_OrdenDeCompraWrapperClassPaso);
       
        //Regresa la cadena
        return objTAM_OrdenDeCompraWrapperClassPaso;
    }

   //Obtener un Set de las series disponibles
    @AuraEnabled
    public static string addWrpDist(String sWrpDist, String recordId, Integer intIndex, String strAnioModelo,
    	String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strSolRecortTypeId) {    	
    	System.debug('EN TAM_SolicitudCompraEspecialDistCmpCtrl getWrpDist: ' + sWrpDist + ' recordId: ' + recordId + ' intIndex: ' + intIndex + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt);
    	String sCadenaPaso = '';  
    	
		if (intIndex > 0)
			intIndex++;				
		TAM_WrpDistSolFlotillaPrograma objPasoWrpDist = new TAM_WrpDistSolFlotillaPrograma(intIndex, false, '' , strAnioModelo, strSerie
			, strModelo, strVersion, '', 0, ' NINGUNO ', new List<TAM_WrpDistribuidores>());
		objPasoWrpDist.strIdColExt = strIdColExt;
		objPasoWrpDist.strIdColInt = strIdColInt;	
		objPasoWrpDist.bolDisable = false;
        
        //serializa la lista y crea el JSON
		sCadenaPaso = JSON.serialize(objPasoWrpDist);              
        System.debug('EN TAM_SolicitudCompraEspecialDistCmpCtrl addWrpDist sCadenaPaso: ' + sCadenaPaso);
        
        //Regresa la cadena
        return sCadenaPaso;
    }

    //Obtener distribuidores.
    @AuraEnabled
    public static List<Account> getDistribuidores() {
    	System.debug('EN getDistribuidores...');
        List<Account> lstDistribuidores = [ SELECT Id, Name, Codigo_Distribuidor__c 
                                            FROM Account 
                                            WHERE RecordType.Name = 'Distribuidor'
                                            AND Venta_Corporativa__c = 'Si'
                                            AND EstatusDealer__c = 'Activo'
                                            AND Asignado_a_autos_Pool__c = true
                                            ORDER BY Name ASC ];
		System.debug('EN getDistribuidores lstDistribuidores: ' + lstDistribuidores);
        return lstDistribuidores;
    }   

    //Obtener distribuidores.
    @AuraEnabled
    public static List<TAM_CatalogoDistribuidores__c> getCatalogoDistribuidores() {
    	System.debug('EN getCatalogoDistribuidores...');
        List<TAM_CatalogoDistribuidores__c> lstDistribuidores = [ SELECT Id, Name, TAM_IdExterno__c, TAM_IdDistribuidor__c
					FROM TAM_CatalogoDistribuidores__c
					ORDER BY Name ASC ];
		System.debug('EN getCatalogoDistribuidores lstDistribuidores: ' + lstDistribuidores);
        return lstDistribuidores;
    }   

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion saveDatos(String sWrpDist, String sWrpDistElim, String recordId, Integer intIndex, 
    	String strAnioModelo, String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strSolRecortTypeId) {    	
    	System.debug('EN saveDatos sWrpDist: ' + sWrpDist );
    	System.debug('EN saveDatos sWrpDistElim: ' + sWrpDistElim );
    	System.debug('EN saveDatos recordId: ' + recordId + ' intIndex: ' + intIndex + ' strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma + ' strSolRecortTypeId: ' + strSolRecortTypeId);
    	String sMensaejeSalidaUpd = '';  
		
		String sRectorTypePaso = getTipoRegSol(strSolRecortTypeId);
		System.debug('EN saveDatos sRectorTypePaso: ' + sRectorTypePaso );
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDist = new List<TAM_WrpDistSolFlotillaPrograma>();		
		List<TAM_DistribuidoresFlotillaPrograma__c>	ListDistProgUps = new List<TAM_DistribuidoresFlotillaPrograma__c>();
		Set<String> setCaveDistr = new Set<String>();
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDistElim = new List<TAM_WrpDistSolFlotillaPrograma>();
		Set<String> setCaveDistrElim = new Set<String>();
		List<TAM_DistribuidoresFlotillaPrograma__c>	ListDistProgDel = new List<TAM_DistribuidoresFlotillaPrograma__c>();
        Map<String, String> mapSolFlotProg = new Map<String, String>();
        User Propietario = new User();
        
        //Los datos del distribuidor
        for (User usuario : [Select u.Name, u.CodigoDistribuidor__c 
            From User u Where id = :UserInfo.getUserId()]){
            Propietario = usuario;
        }//Fin del for para User
        System.debug('EN saveDatos Propietario '+ Propietario);
		
		for (TAM_SolicitudesFlotillaPrograma__c  objSolFlotProg : [Select id, Name From TAM_SolicitudesFlotillaPrograma__c  Where id = :recordId]){
		    //Metelo al mapa de mapSolFlotProg
		    mapSolFlotProg.put(objSolFlotProg.id, objSolFlotProg.Name);
		}
        System.debug('EN saveDatos mapSolFlotProg '+ mapSolFlotProg.keyset());
        System.debug('EN saveDatos mapSolFlotProg '+ mapSolFlotProg.values());
		
        SavePoint sp = Database.setSavepoint();
        try{
            
	        //Ve si tiene algo la lista de sWrpDistElim entonces deserealizala
	        if (sWrpDistElim != null && sWrpDistElim.length() > 0){
	            lObjWrpDistElim = JSONParserSFDC(sWrpDistElim);
	            //Recorre los reg y toma los que tienen algo en el campo de strIdExterno
	            for (TAM_WrpDistSolFlotillaPrograma objDistFlotProgElim : lObjWrpDistElim){
	                //Ve si tiene algo en elñ campo de strIdExterno
	                if (objDistFlotProgElim.strIdExterno != null && objDistFlotProgElim.strIdExterno != '')
	                    setCaveDistrElim.add(objDistFlotProgElim.strIdExterno); 
	            }//Fin del for para lObjWrpDistElim
	            
	            //ya que tienes los ID esternos ahora consultalos en SFDC para que loselimines
	            for (TAM_DistribuidoresFlotillaPrograma__c objDistElim : [Select id From TAM_DistribuidoresFlotillaPrograma__c
	                Where TAM_IdExterno__c IN : setCaveDistrElim]){
	                ListDistProgDel.add(objDistElim);
	            }
	            
	            System.debug('EN saveDatos ListDistProgDel: ' + ListDistProgDel );
	            //Tiene algo la lista de ListDistProgDel
	            if (!ListDistProgDel.isEmpty()) 
	                if (!Test.isRunningTest())
	                    delete ListDistProgDel;
	            
	        }//Fin si sWrpDistElim != null && sWrpDistElim.length() > 0
	        
	        //Ve si tiene algo la lista 
	        if (intIndex > 0){
	            lObjWrpDist = JSONParserSFDC(sWrpDist);
	            //Obten los datos del distribuidor en este caso la clave
	            for (TAM_WrpDistSolFlotillaPrograma objWrpDist : lObjWrpDist){
	                String sCaveDist = objWrpDist.strIdDistrib.substring(0, objWrpDist.strIdDistrib.indexOf(' - '));
	                setCaveDistr.add(sCaveDist);
	            }
	            //Ya tienes las claves de los distribuidores buscalos
	            Map<String, Account> mapClaveDistIdSfdc = getDistribuidores(setCaveDistr);
		        String sNombreDist = mapClaveDistIdSfdc.containsKey(Propietario.CodigoDistribuidor__c) ? mapClaveDistIdSfdc.get(Propietario.CodigoDistribuidor__c).Name : Propietario.Name;
		        String sTAMEntrega = Propietario.CodigoDistribuidor__c != null ? Propietario.CodigoDistribuidor__c : '57000';
		        sTAMEntrega += '-' + sNombreDist;
		        System.debug('EN saveDatos sNombreDist: '+ sNombreDist + ' sTAMEntrega: ' + sTAMEntrega);
                	
	            //Recorre la lista de y obten el id del distribuidor
	            for (TAM_WrpDistSolFlotillaPrograma objWrpDist : lObjWrpDist){
	                System.debug('EN saveDatos objWrpDist: ' + objWrpDist);             
	                //Solo los que son mayor a 0
	                if (objWrpDist.intCantidad > 0){
	                    String sCaveDist = objWrpDist.strIdDistrib.substring(0, objWrpDist.strIdDistrib.indexOf(' - '));
	                    //String sRecordName = sCaveDist + '-' + (mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).Name : '') + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strVersion;
	                    String sRecordName = sCaveDist + '-' + (mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).Name : '') + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie;
	                    //String sIdExterno = recordId + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strModelo + '-' + objWrpDist.strVersion + '-' + strIdColExt + '-' + strIdColInt + '-' + sCaveDist + '-' + sRectorTypePasoPrograma;
	                    String sIdExterno = recordId + '-' + objWrpDist.strAnioModelo + '-' +  objWrpDist.strSerie + '-' + objWrpDist.strModelo + '-' + strIdColExt + '-' + strIdColInt + '-' + sCaveDist + '-' + sRectorTypePasoPrograma;
	                    if (objWrpDist.strIdExterno != null && objWrpDist.strIdExterno != '')
	                        sIdExterno = objWrpDist.strIdExterno;
	                    System.debug('EN saveDatos sIdExterno: ' + sIdExterno); 
	                    //Id, Name, TAM_IdExterno__c, TAM_IdDistribuidor__c FROM TAM_CatalogoDistribuidores__c
	                    //Crea el registro y agregalo a la lista de ListDistProgUps
	                    ListDistProgUps.add(new TAM_DistribuidoresFlotillaPrograma__c(
	                            Name = sRecordName,
	                            //TAM_DetalleSolicitudCompra_FLOTILLA__c = recordId,
	                            TAM_SolicitudFlotillaPrograma__c = recordId,
	                            RecordTypeid = sRectorTypePaso, //sRectorTypePasoPrograma, 
	                            TAM_Cuenta__c = mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).id : null, //mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).TAM_IdDistribuidor__c : null,
	                            TAM_IdDistribuidor__c = mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).Codigo_Distribuidor__c : null, //mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).TAM_IdExterno__c : null
	                            TAM_NombreDistribuidor__c = mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).Name : null,  
	                            TAM_Cantidad__c = objWrpDist.intCantidad,
	                            TAM_Estatus__c = 'Pendiente',
	                            TAM_IdExterno__c = sIdExterno,
	                            TAM_DistribuidorOrigen__c = sTAMEntrega,
	                            TAM_FolioSolicitud__c = mapSolFlotProg.get(recordId)
	                        )
	                    );
	                }//Fin si objWrpDist.strIdDistrib != null && objWrpDist.strIdDistrib != ''              
	            }//Fin del for para lObjWrpDist
	        }//Fin si intIndex > 0
	        System.debug('EN saveDatos ListDistProgUps: ' + ListDistProgUps);
	        
	        //Ve si tiene algo la lista de ListDistProgUps
	        if (!ListDistProgUps.isEmpty()){
	            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
	            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(ListDistProgUps, TAM_DistribuidoresFlotillaPrograma__c.TAM_IdExterno__c, false);
	            //Ve si hubo error
	            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
	                if (!objDtbUpsRes.isSuccess())
	                    objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Hubo un error a la hora de crear/Actualizar los registros de los Distribuidores ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
	                if (objDtbUpsRes.isSuccess())
	                    objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos de los distribuidores se crear/Actualizar con exito');
	            }//Fin del for para lDtbUpsRes
	        } //Fin si ListDistProgUps.isEmpty()
	        System.debug('EN saveDatos objRespoPaso.strDetalle: ' + objRespoPaso.strDetalle);
	                    
	        //serializa la lista y crea el JSON
	        System.debug('EN saveDatos objRespoPaso: ' + objRespoPaso);            
           
        }catch(Exception ex){
            System.debug('EN saveDatos ERROR: ' + ex.getMessage() + ' Número de Línea: ' + ex.getLineNumber());
            Database.rollback(sp);            
        }
        
        //Regresa todo a su lugar		
        //Database.rollback(sp);
                 
        //Regresa la cadena
        return objRespoPaso;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static string getDatosDistribuidores(String recordId, Integer intIndex, String strAnioModelo,
    	String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strSolRecortTypeId) {    	
    	System.debug('EN getDatosDistribuidores...');
    	System.debug('EN getDatosDistribuidores recordId: ' + recordId + ' intIndex: ' + intIndex + ' strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt + ' sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma);
    	String sCadenaPaso = '';  
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDist = new List<TAM_WrpDistSolFlotillaPrograma>();	
		List<TAM_DistribuidoresFlotillaPrograma__c>	ListDistProgUps = new List<TAM_DistribuidoresFlotillaPrograma__c>();
		Set<String> setCaveDistr = new Set<String>();
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		
		//Toma los datos de los paremtros y forma la llave
		//String sIdExternoIni = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strVersion + '-' + strIdColExt + '-' + strIdColInt + '%';
		String sIdExternoIni = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt + '%';
		System.debug('EN getDatosDistribuidores sIdExternoIni: ' + sIdExternoIni);
		
		Integer intCnt = 0;
		//Ya tienes la llave consulta los datos en TAM_DistribuidoresFlotillaPrograma__c
		for (TAM_DistribuidoresFlotillaPrograma__c objDistrib : [Select ID, TAM_Cantidad__c, TAM_Cuenta__r.Codigo_Distribuidor__c,
			TAM_Cuenta__r.Name, TAM_IdExterno__c From TAM_DistribuidoresFlotillaPrograma__c
			Where TAM_SolicitudFlotillaPrograma__c = :recordId 
			//TAM_SolicitudCompraPROGRAMA__c =:recordId 
			And TAM_IdExterno__c Like :sIdExternoIni 
			]){
				
			TAM_WrpDistSolFlotillaPrograma objPaso = new TAM_WrpDistSolFlotillaPrograma(
				intCnt, false, objDistrib.TAM_Cuenta__r.Codigo_Distribuidor__c, strAnioModelo, strSerie, strModelo, strVersion,
				objDistrib.TAM_IdExterno__c, Integer.valueOf(objDistrib.TAM_Cantidad__c), objDistrib.TAM_Cuenta__r.Codigo_Distribuidor__c + ' - ' + objDistrib.TAM_Cuenta__r.Name,
				new List<TAM_WrpDistribuidores>()	
			);
			objPaso.strIdColExt = strIdColExt;
			objPaso.strIdColInt = strIdColInt;
			objPaso.bolDisable = true;
			//Crea los reg del tipo TAM_WrpDistSolFlotillaPrograma
			lObjWrpDist.add(objPaso);
			intCnt++;
		}//fin del for para TAM_DistribuidoresFlotillaPrograma__c
		    	        
        //serializa la lista y crea el JSON
		sCadenaPaso = JSON.serialize(lObjWrpDist);              
        System.debug('EN getWrpModelo sCadenaPaso: ' + sCadenaPaso);
                 
        //Regresa la cadena
        return sCadenaPaso;
    }

    //Obtener distribuidores.
    public static Map<String, Account> getDistribuidores(Set<String> setCaveDistr) {
    	System.debug('EN getDistribuidores...');
    	Map<String, Account> mapClaveDistIdSfdc = new Map<String, Account>();
            	
        for (Account objAccount : [SELECT Id, Name, Codigo_Distribuidor__c 
                                            FROM Account 
                                            WHERE RecordType.Name = 'Distribuidor'
                                            AND Venta_Corporativa__c = 'Si'
                                            AND EstatusDealer__c = 'Activo'
                                            AND Asignado_a_autos_Pool__c = true
                                            ORDER BY Name ASC]){
            //Mete los datos en el mapa de mapClaveDistIdSfdc
            mapClaveDistIdSfdc.put(objAccount.Codigo_Distribuidor__c, objAccount);
        }
        
		System.debug('EN getDistribuidores lstDistribuidores: ' + mapClaveDistIdSfdc);
        return mapClaveDistIdSfdc;
    }   

    //Obtener distribuidores.
    public static Map<String, TAM_CatalogoDistribuidores__c> getCatalogoDistribuidores(Set<String> setCaveDistr) {
    	System.debug('EN getDistribuidores...');
    	Map<String, TAM_CatalogoDistribuidores__c> mapClaveDistIdSfdc = new Map<String, TAM_CatalogoDistribuidores__c>();
    	    	
    	for (TAM_CatalogoDistribuidores__c objAccount : [SELECT Id, Name, TAM_IdExterno__c, TAM_IdDistribuidor__c
					FROM TAM_CatalogoDistribuidores__c
					ORDER BY Name ASC]){
    		//Mete los datos en el mapa de mapClaveDistIdSfdc
    		mapClaveDistIdSfdc.put(objAccount.TAM_IdExterno__c, objAccount);
    	}
        
		System.debug('EN getCatalogoDistribuidores lstDistribuidores: ' + mapClaveDistIdSfdc);
        return mapClaveDistIdSfdc;
    }   

    //Obtener distribuidores.
    @AuraEnabled
    public static string getTipRegPrograma(String strTipRegNombre) {
    	System.debug('EN getTipRegPrograma...');
    	String sRectorTypePasoProgramaPaso = sRectorTypePasoPrograma; //Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get(strTipRegNombre).getRecordTypeId();
    	
		System.debug('EN getDistribuidores sRectorTypePasoProgramaPaso: ' + sRectorTypePasoProgramaPaso);
        return sRectorTypePasoProgramaPaso;
    }   

	//Regresa un objeto del tipo AddTelephoneDto si no hubo error a la hora de registrar el cliente		
	public static List<TAM_WrpDistSolFlotillaPrograma> JSONParserSFDC(String sJsonResp){
		System.debug('EN JSONParserSFDC: sJsonResp: ' + sJsonResp);
		
		List<TAM_WrpDistSolFlotillaPrograma> listObjWrp = new List<TAM_WrpDistSolFlotillaPrograma>();
		try{
            JSONParser parser = JSON.createParser(sJsonResp);
            //Ve si tiene algo el objeto de parser  
            while (parser.nextToken() != null) {//[{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":10,"intInventario":8,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2201","strSerie":"AVANZA","strVersion":"LE MT","strYear_Serie":"2019-AVANZA"},'{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":2,"intInventario":1,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2202","strSerie":"AVANZA","strVersion":"LE AT","strYear_Serie":"2019-AVANZA"} '']'
				//Inicia el detalle del objeto: sNombreObj
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					//Toma el contenido del Json y conviertelo a SignInCls.class 
					TAM_WrpDistSolFlotillaPrograma objTAM_WrpDistSolFlotillaPrograma = (TAM_WrpDistSolFlotillaPrograma)parser.readValueAs(TAM_WrpDistSolFlotillaPrograma.class);
					listObjWrp.add(objTAM_WrpDistSolFlotillaPrograma);
				}//Fin si parser.getCurrentToken() == JSONToken.START_OBJECT
            }//Fin mientras parser.nextToken() != null			
		}catch(Exception ex){
			System.debug('ERROR EN JSONParserSFDC: sJsonResp: ' + ex.getMessage());
	 	}
		System.debug('ANTES DE SALIR DE JSONParserSFDC: listObjWrp: ' + listObjWrp);
			 	
		//Regresa el objeto objSignInClsPaso
		return listObjWrp;
	}    
   

    public static string getTipoRegSol(String sTipoRegSol) {
    	System.debug('EN getTipoRegSol sTipoRegSol: ' + sTipoRegSol);
    	
    	String sTipoRegSolPaso = '';
    	
    	sTipoRegSolPaso = sTipoRegSol == sRectorTypePasoSolPrograma ? sRectorTypePasoPrograma :
    		(sTipoRegSol == sRectorTypePasoSolProgramaCerrada ? sRectorTypePasoPrograma :
    			( sTipoRegSol == sRectorTypePasoSolVentaCorporativa ? sRectorTypePasoFlotilla : 
    		 		( sTipoRegSol == sRectorTypePasoSolVentaCorporativaCerrada ? sRectorTypePasoFlotilla : sRectorTypePasoPrograma)
    		 	) 
    		);

		System.debug('EN getTipoRegSol sTipoRegSolPaso: ' + sTipoRegSolPaso);
        return sTipoRegSolPaso;
    }
      
}