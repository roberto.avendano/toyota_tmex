public without sharing class  TAM_ExcepcionAutoDemo {
    
    @AuraEnabled
    public static Id saveRecord(String objRecord,String comentarioDealer){
        TAM_SolicitudAutoDemo__c objSolicitudAutoDemo = new TAM_SolicitudAutoDemo__c();
        TAM_SearchAutoDemoExcepcion.wrapVIN objWrapper = new TAM_SearchAutoDemoExcepcion.wrapVIN();
        
        //RecordId de Solicitu de tipo AutoDemo
        String recordTypeId =  Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Auto_Demo').getRecordTypeId(); 
        TAM_SolicitudExpecionIncentivo__c solicitudExcepcion = new TAM_SolicitudExpecionIncentivo__c();
        String recordTypeIdDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId(); 
        
        objWrapper = (TAM_SearchAutoDemoExcepcion.wrapVIN) JSON.deserialize(objRecord,TAM_SearchAutoDemoExcepcion.wrapVIN.class);
        
        Account distribuidor = [Select id from account where Codigo_Distribuidor__c =: objWrapper.codigoDealer and recordtypeId =: recordTypeIdDistribuidor limit 1];        
        
        //Se obtienen datos de movimiento
        Movimiento__c movObj = [Select id,NombreCliente__c From Movimiento__c Where Id =: objWrapper.idMovimiento limit 1];
        
        if(objRecord != null){
            solicitudExcepcion.TAM_NombreCliente__c = movObj.NombreCliente__c;
            solicitudExcepcion.TAM_Serie__c = objWrapper.Serie;
            solicitudExcepcion.TAM_AnioModelo__c = objWrapper.anioModelo;
            solicitudExcepcion.TAM_Modelo__c  = objWrapper.codigoModelo;
            solicitudExcepcion.TAMVIN__c     = objWrapper.VIN;
            solicitudExcepcion.TAM_SubmittedDate__c = objWrapper.fechaVentaVIN;
            solicitudExcepcion.TAM_IncentivoCorrespondienteRetail__c  = objWrapper.idDetallePolitica;
            solicitudExcepcion.TAM_PoliticaSeleccionadaRetail__c = objWrapper.idDetallePolitica;
            solicitudExcepcion.TAM_Comentario__c = comentarioDealer;
            solicitudExcepcion.TAM_Distribuidor__c = distribuidor.id;
            solicitudExcepcion.TAM_AprobacionDM__c = 'Pendiente';
            solicitudExcepcion.TAM_AprobacionFinanzas__c = 'Pendiente';
            solicitudExcepcion.RecordTypeId = recordTypeId;
            
        }
        
        
        try {
            system.debug('solicitud a insertar'+solicitudExcepcion);
            Database.insert(solicitudExcepcion);
            return solicitudExcepcion.id;
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return null;
        }
        
    }
    
}