public without sharing class PresupuestoTriggerHandler extends TriggerHandler{
	private List<Presupuesto__c> presupuestosList;
	private List<String> meses;

	public PresupuestoTriggerHandler() {
		this.presupuestosList = (List<Presupuesto__c>) Trigger.new;
		meses = new List<String>{'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'};
	}

	public override void beforeInsert(){
		calculoFechas(presupuestosList);
	}

    private void calculoFechas(List<Presupuesto__c> presupuestos){    	        
    	Map<Id, PeriodoFiscal__c> periodos = loadPeriodos(presupuestos);
        
    	for(Presupuesto__c p: presupuestos){
            PeriodoFiscal__c pf = periodos.get(p.PeriodoFiscal__c);
            //p.IdPresupuesto__c = String.valueOf(p.TipoPresupuesto__c)+' '+String.valueOf(p.FechaInicioPresupuesto__c)+' '+String.valueOf(p.FechaFinPresupuesto__c);

            Date startPF= pf.FechaInicioPeriodoFiscal__c; 
            Date endPF= pf.FechaFinPeriodoFiscal__c; 
            
            Integer daysOfMonth;     
            Date lastDayOfMonth;              

            for(Integer j=0; j<=startPF.monthsBetween(endPF); j++){
                Date nextMonth =startPF.addMonths(j);
                daysOfMonth = Date.daysInMonth(nextMonth.year(), nextMonth.month());
                lastDayOfMonth= Date.newInstance(nextMonth.year(), nextMonth.month(), daysOfMonth);            
                
                if(meses.get(nextMonth.month()-1)=='Enero'){
                    p.InicioCortePresupuestoEnero__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoEnero__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Febrero'){
                    p.InicioCortePresupuestoFebrero__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoFebrero__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Marzo'){
                    p.InicioCortePresupuestoMarzo__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoMarzo__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Abril'){
                    p.InicioCortePresupuestoAbril__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoAbril__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Mayo'){
                    p.InicioCortePresupuestoMayo__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoMayo__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Junio'){
                    p.InicioCortePresupuestoJunio__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoJunio__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Julio'){
                    p.InicioCortePresupuestoJulio__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoJulio__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Agosto'){
                    p.InicioCortePresupuestoAgosto__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoAgosto__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Septiembre'){
                    p.InicioCortePresupuestoSeptiembre__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoSeptiembre__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Octubre'){
                    p.InicioCortePresupuestoOctubre__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoOctubre__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Noviembre'){
                    p.InicioCortePresupuestoNoviembre__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoNoviembre__c= lastDayOfMonth;
                } else if(meses.get(nextMonth.month()-1)=='Diciembre'){
                    p.InicioCortePresupuestoDiciembre__c= nextMonth.toStartOfMonth();
                    p.FinCortePresupuestoDiciembre__c= lastDayOfMonth;
                }
                
            }

    	}    	
    }//Fin calculoFechas


    private Map<Id, PeriodoFiscal__c> loadPeriodos(List<Presupuesto__c> presupuestos){
    	Set<Id> pf = new Set<Id>();    	
    	for(Presupuesto__c p: presupuestos){
    		pf.add(p.PeriodoFiscal__c);
    	}
        
    	return new Map<Id, PeriodoFiscal__c>([
    		SELECT Id, FechaInicioPeriodoFiscal__c, FechaFinPeriodoFiscal__c FROM PeriodoFiscal__c WHERE Id IN: pf
		]);
    }
}