/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para ejecutar de TAM_CatalogoProductos_tgr_handler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    01-Junio-2019     Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CatalogoProductos_tgr_handler_Test {

	public static TAM_CatalogoProductos__c getDatosListaPrecios(){
		System.debug('EN TAM_CatalogoProductos_tgr_handler_Test....');
		
		Id standardPricebookId = Test.getStandardPricebookId();
		
		TAM_CatalogoProductos_tgr_handler.wrpPricebook objwrpPricebook = new TAM_CatalogoProductos_tgr_handler.wrpPricebook();
		TAM_CatalogoProductos_tgr_handler.wrpPricebookEntry objwrpPricebookEntry = new TAM_CatalogoProductos_tgr_handler.wrpPricebookEntry();
		TAM_CatalogoProductos_tgr_handler.wrpProducto objwrpProducto = new TAM_CatalogoProductos_tgr_handler.wrpProducto();
		
		//Crea el registro para el objeto de TAM_CatalogoProductos__c
		TAM_CatalogoProductos__c objTAM_CatalogoProductos = new TAM_CatalogoProductos__c(); 
		objTAM_CatalogoProductos.IdExterno__c = '22012019';
		objTAM_CatalogoProductos.Name = '2201';		
		objTAM_CatalogoProductos.AnioModelo__c = '2019';
		objTAM_CatalogoProductos.Version__c = 'LE 5MT L4 RWD';
		objTAM_CatalogoProductos.Familia__c = 'Toyota';
		objTAM_CatalogoProductos.Serie__c = 'Avanza';		
		objTAM_CatalogoProductos.PrecioPublico__c = '242100.00';
		objTAM_CatalogoProductos.PrecioSinIva__c = '242100.00';
		objTAM_CatalogoProductos.PrecioTotalEmpleado__c = '242100.00';
		objTAM_CatalogoProductos.IdExternoListaPrecios__c = 'VH-Mayo-2019';		
		objTAM_CatalogoProductos.InicioVigencia__c = '01/05/2019'; //Date.newInstance(2019, 05, 01); //'2019-05-01';
		objTAM_CatalogoProductos.FinVigencia__c = '31/05/2019'; //Date.newInstance(2019, 05, 30); //'2019-05-30';		
		objTAM_CatalogoProductos.DivisaPrecio__c = 'Pesos';
		
		objTAM_CatalogoProductos.TAM_PhaseCodeVersion__c = ''; 	
		objTAM_CatalogoProductos.TAM_TMSMarginP__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_InLineAccessoryCostUSD__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_VehicleType__c =  '';
		objTAM_CatalogoProductos.TAM_TMSCostUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_LandedCostUSD__c = 0.00;
		//objWrpPricebookEntry.PrecioSinIva = objTAM_CatalogoProductos.PrecioSinIva__c = 
		objTAM_CatalogoProductos.TAM_OceanFreightUSD__c  = '0.00'; //VALIDAR
		objTAM_CatalogoProductos.TAM_ISANTaxTotalEmployeeMXP__c  =  '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_IVAVATMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_IVAP__c = 0.00;
		objTAM_CatalogoProductos.TAM_VehicleExFactoryPriceUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_LandedCostMXP__c = 0.00;
		//objTAM_CatalogoProductos.TAM_InlandFreightUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_OneExFactPriceAdjustmentFactorMXP__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_ISANTaxTotalMXP__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_IVAVATEmployeeMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_MixPercentage__c = 0.00;
		objTAM_CatalogoProductos.TAM_FreightInsuranceUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_MexicoLocalCostsMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_MarineInsuranceP__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_PDIPDSMXP__c = '0.00';
		objTAM_CatalogoProductos.TAM_RetailPriceUSD__c = 0.00;
		//Para el registro relaciopado
		objTAM_CatalogoProductos.TAM_ModId__c = '';
		objTAM_CatalogoProductos.TAM_MarineInsuranceUSD__c = '0.00'; //VALIDAR
		objTAM_CatalogoProductos.TAM_DealerCostUSD__c = 0.00; 										
		objTAM_CatalogoProductos.TAM_ExFactorywFastPricingUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_MSRPwoIVAEmployeeMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_ApplicableAmountMXP__c = '0.00';	
		objTAM_CatalogoProductos.TAM_PDIPDSEmployeeMXP__c = '0.00';	
		objTAM_CatalogoProductos.TAM_MSRPwoIVAISANUSD__c = 	0.00;
		objTAM_CatalogoProductos.TAM_FixedAmountMXP__c = '0.00';	//vALIDAR
		objTAM_CatalogoProductos.TAM_MSRPwoIVAISANEmployeeMXP__c = 	0.00;
		objTAM_CatalogoProductos.TAM_ProdMth__c = '';	
		objTAM_CatalogoProductos.TAM_FileName__c = 	'';
		objTAM_CatalogoProductos.TAM_M3USD__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_FreightInsuranceMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_IVAPEmployeeMXP__c = '0'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_CIFtoTMEXMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_ModName__c = '';
		objTAM_CatalogoProductos.TAM_MSRPwoIVAMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_CIFtoTMEXUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_DealerCostEmployeeMXP__c = 0.00; 
		objTAM_CatalogoProductos.TAM_MSRPwoIVAISANMXP__c = 0.00;
		//objTAM_CatalogoProductos.TAM_WharfageANDHandlingUSD__c = 0.00; 
		objTAM_CatalogoProductos.TAM_DTAP__c = '#Missing'; //VALIDAR
		objTAM_CatalogoProductos.TAM_DealerCostMXP__c = 0.00; 
		objTAM_CatalogoProductos.TAM_AllinrateUSD__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_PricingFXRatesMXP__c = 0.00;
		objTAM_CatalogoProductos.TAM_Source__c = '';
		objTAM_CatalogoProductos.TAM_ExFactoryPricewithoptionsUSD__c = 0.00; 
		//objTAM_CatalogoProductos.TAM_DocksidePreparationUSD__c = 0.00;
		objTAM_CatalogoProductos.TAM_DTAUSD__c = '0.00'; //VALIDAR 
		objTAM_CatalogoProductos.TAM_MSRPUSD__c = 0.00; 
		objTAM_CatalogoProductos.TAM_FreightInsuranceEmployeeMXP__c = 0.00; 
		objTAM_CatalogoProductos.TAM_X100POptionUSD__c = '#Missing';
		System.debug('EN beforeInsert.creaMapasDatos objWrpPricebookEntry: ' + objWrpPricebookEntry);	
		System.debug('EN TAM_CatalogoProductos_tgr_handler_Test objTAM_CatalogoProductos: ' + objTAM_CatalogoProductos);
	
		//Crea el reg						
		insert objTAM_CatalogoProductos;
		System.debug('EN TAM_CatalogoProductos_tgr_handler_Test objTAM_CatalogoProductos1: ' + objTAM_CatalogoProductos);
		//Regresa el objeto objTAM_CatalogoProductos 				
		return objTAM_CatalogoProductos;

	}

	@isTest
    static void TAM_CatalogoProductosTest() {
        // TO DO: implement unit test
        TAM_CatalogoProductos__c objTAM_CatalogoProductos = TAM_CatalogoProductos_tgr_handler_Test.getDatosListaPrecios();
        
    }
}