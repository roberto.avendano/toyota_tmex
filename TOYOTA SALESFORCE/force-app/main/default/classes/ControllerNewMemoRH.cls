/*******************************************************************************
Desarrollado por: Globant México
Autor: Roberto Carlos Avendaño Quintana 
Proyecto: Assestment Center TMEX
*********************************************************************************/
public without sharing  class ControllerNewMemoRH {
    
    //Metodo utilizado en el componente para obtener la información relacionada a la evaluación del candidato
    @AuraEnabled
    public static Contact getEvalInfo(Id UserId){
        Contact evalCandidato = new Contact();
        //SOQL que obtiene el contactid relacionado a USER SFDC
        User[] usContact = [Select id,contactId from User where id =: UserId];  
        //Validación que verifica que el arreglo usContact tenga mas de 0 elements
        if(usContact.size() > 0){
            //Se guarda en la variable idContact el id del contacto relacionado con el usuario
            String idContact = String.valueOf(usContact[0].contactId).substring(0, 15);
            //SOQL que retorna el metodo con la información relacionada a la evaluación RH
            evalCandidato= [Select id,RH_FechaAssestment__c,RH_DuracionEvalucion__c,Inicio_Evaluacion__c,FechaActivacionEval__c,FechaEvaluacionRH__c from Contact WHERE id =: idContact limit 1];
            
            
        }
        return evalCandidato;  
    }
    
    //Metodo que recibe desde el componente los parametros (RH_EvaluacionCandidato__c nuevoCorreo, id userId,List<String> deRespuesta,List<String> opcionesPara, List<String> opcionesCC,List<String> opcionesCCO)
    //Para apartir de estos parametros crear un nuevo correo.
    @AuraEnabled
    public static List<String> createNewEmail(RH_EvaluacionCandidato__c nuevoCorreo, id userId,List<String> deRespuesta,List<String> opcionesPara, List<String> opcionesCC,List<String> opcionesCCO){
        //Incializaciín de estructuras de datos para usarlas en el flujo
        RH_Contacto__c contactoRH = new RH_Contacto__c();
		RH_EvaluacionCandidato__c nuevoEmailIn = new RH_EvaluacionCandidato__c();
        List<RH_EvaluacionCandidato__c> listNuevoEmail = new List<RH_EvaluacionCandidato__c>();
        //Se recibe como lista de String los campos de la evaluación estas listas se transforman a String para poder guardarlas en el campo
        string deRespuestaValue = string.join(deRespuesta,',');
        string paraRespuestaValue = string.join(opcionesPara,',');
        string ccRespuestaValue = string.join(opcionesCC,',');
        string ccoRespuestaValue = string.join(opcionesCCO,',');
        //Obtenemos el contactid relacionado a el Usuario
        User[] contactId = [Select contactId From User where id =: userId]; 
        //Validación que verifica que la lista contactId sea mayor a 0
        if(contactId.size() > 0){
            //SOQL que obtiene la información del candidato que esta realizando al evaluación
        	 contactoRH = [Select id,name,RH_ApellidoPaterno__c,RH_ApellidoMaterno__c From RH_Contacto__c WHERE RH_Contacto__c =: contactId[0].contactId AND RH_evaluacionFinalizada__c = false];    
        }
        //Obtenemos el recordType que utilizaremos para guardar los "New memo"
        RecordType recordTypeNuevoC  = [select Id from RecordType where sObjectType='RH_EvaluacionCandidato__c' and NAME = 'NUEVO CORREO'];

        //Validacion que verifica que nuevoCorreo != null && recordTypeNuevoC != null
        if(nuevoCorreo != null && recordTypeNuevoC != null){
            nuevoEmailIn.Name = 'New memo';
            nuevoEmailIn.RecordTypeId = recordTypeNuevoC.id;
            nuevoEmailIn.Para_Respuesta__c = paraRespuestaValue;
            nuevoEmailIn.De_Respuesta__c = deRespuestaValue;
            nuevoEmailIn.CC_Respuesta__c = ccRespuestaValue;
            nuevoEmailIn.CCO_Respuesta__c = ccoRespuestaValue;
            nuevoEmailIn.Asunto_Respuesta__c = nuevoCorreo.Asunto_Respuesta__c;
            nuevoEmailIn.Cuerpo_Respuesta__c = nuevoCorreo.Cuerpo_Respuesta__c;
            nuevoEmailIn.RH_candidatoEvaluacion__c = contactoRH.id;
            nuevoEmailIn.RH_NombreCandidato__c = contactoRH.name+' '+contactoRH.RH_ApellidoPaterno__c+' '+contactoRH.RH_ApellidoMaterno__c;
            listNuevoEmail.add(nuevoEmailIn);
        }
        
        if(!listNuevoEmail.isEmpty()){
            try{
                //Insertamos en el objeto RH_EvaluacionCandidato__c los nuevos registros
            	insert listNuevoEmail;    
            }catch(Exception e){
                system.debug('Error al tratar de insetar la lista listNuevoEmail'+e.getMessage());
                
            }
            
        }
        
        return opcionesCCO;
    }
    
    //Metodo que funciona para realizar una llamada REST a la api de salesforce para obtener los valores de pick list , dependiendo el recordaType
    @AuraEnabled
    public static List<String>  getPickListValuesOrg(Id userId){
        Contact contactoEval = new Contact();
        User contactId = new User();
        String contact;
        
        //Se valida que userId sea diferente nulo
        if(userId != null){
        	 contactId = [Select contactid From User where id  =: userId];   
        }
        
        //Se valida que contactId sea diferente nulo
        if(contactId != null){
             contactoEval = [Select id,CasoUsoRH__c from Contact WHERE id =: contactid.contactid];
        }
        
        //Se instancia el obejto http , para realizar una llamada rest de metodo GET, esta llamada nos regresa como response un json con los valores del pickList , dependiendo el recordtype
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String host = System.Url.getSalesforceBaseURL().toExternalForm();
        String url = host+'/services/data/v45.0/ui-api/object-info/RH_EvaluacionCandidato__c/picklist-values/'+contactoEval.CasoUsoRH__c+'/De_Respuesta__c';
        request.setEndpoint(url);
        request.setMethod('GET');  
        request.setHeader('Authorization', 'OAuth '+UserInfo.getSessionId());
        HttpResponse response;        
        response = http.send(request);
        List<String> result = new List<String>();
        //Se utiliza response.getBody() para deserializarlo en un objeto de tipo Mapa
        Map<String,Object> root = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
        if(!root.containsKey('values')){ 
            return result; 
        }
        List<Object> pValues = (List<Object>)root.get('values');
        for(Object pValue : pValues){
            Map<String,Object> pValueMap = (Map<String,Object>)pValue;
            result.add((String)pValueMap.get('value'));
            
        }

        //Se retorna una lista de String con las opciónes en un picklist
        return result;

    }

}