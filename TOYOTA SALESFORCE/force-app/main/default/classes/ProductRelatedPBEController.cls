public with sharing class ProductRelatedPBEController {
	public Product2 producto{get;set;}
	private static String LISTA_PRECIOS='Lista Precios Partes Robadas';

	public ProductRelatedPBEController(ApexPages.StandardController controller) {
		this.producto = getProducto(controller.getId());
	}

	private Product2 getProducto(String productID){
		return [SELECT Id, Name, 
				(SELECT Pricebook2.Name, UnitPrice, PrecioDealer__c, PrecioMayorista__c FROM PricebookEntries WHERE Pricebook2.Name=:LISTA_PRECIOS)  
			FROM Product2 WHERE Id=:productID];
	}

	public Integer getSize(){
		List<PricebookEntry> pbe = producto.PricebookEntries;
		return pbe.size();
	}
}