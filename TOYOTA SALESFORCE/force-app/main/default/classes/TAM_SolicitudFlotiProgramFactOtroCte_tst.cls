/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase 
                        TAM_SolicitudFlotiProgramFactOtroCteCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-10-2021           Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_SolicitudFlotiProgramFactOtroCte_tst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    
    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
    
    static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

    static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
        
    static String sListaPreciosCutom = getCustomPriceBookList();
    
    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
    static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
    static  Rangos__c rango = new Rangos__c();
    static  TAM_CheckOutDetalleSolicitudCompra__c CheckOutDetalleSolicitudCompra = new TAM_CheckOutDetalleSolicitudCompra__c();
    static  TAM_SolicitudExpecionIncentivo__c SoldExpInc = new TAM_SolicitudExpecionIncentivo__c();
    
    @TestSetup static void loadData(){

        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;

        Account clienteDealer = new Account(
            Name = 'TOYOTA PRUEBA',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoDistribuidor
        );
        insert clienteDealer;

        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_IdExternoNombre__c = 'CARSON'
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
            LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaFisica
        );
        insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  

        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod;  
        
        Id standardPricebookId = Test.getStandardPricebookId();
        Product2 ProducStdPriceBook = new Product2(
                Name = '22060',
                Anio__c = '2020', 
                NombreVersion__c = 'LE MT',
                IdExternoProducto__c = '220602020',
                ProductCode = '220602020',
                Description= 'AVANZA-22060-2020-B79-10-LE AT', 
                RecordTypeId = sRectorTypePasoProductoUnidad,
                Family = 'Toyota',
                IsActive = true
        );
        insert ProducStdPriceBook;
        
        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id = standardPricebookId,
            UnitPrice = 0.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true,
            IdExterno__c = '220602020'
        );
        insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
        
        PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
            Pricebook2Id = customPB.id,
            UnitPrice = 1.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true
        );
        insert pbeCustomProceBookEntry;
        
        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'Cerrada',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaCierreSolicitud__c = Date.today()
        );
        insert solVentaFlotillaPrograma;
                
        TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoPedidoEspecial,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = false,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Cantidad__c = 10
        );
        insert detSolVentaFlotillaPrograma;
                
        TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra = new TAM_DistribuidoresFlotillaPrograma__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoDistFlotilla,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_Estatus__c = 'Pendiente',
            TAM_Cantidad__c = 10,
            TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma.id,
            TAM_Cuenta__c = clienteMoral.id
        );
        insert detSolDistFltoProgra;
        
        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Pedido Especial',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_VIN__c = 'XXXXXXXXX10'
        );
        insert objCheckOutDetSolComp;
        
        TAM_SolicitudExpecionIncentivo__c SoldExpInc = new TAM_SolicitudExpecionIncentivo__c(
            //Name = solVentaFlotillaPrograma.Name,
            TAM_listaVINES__c = 'XXXXXXXXX10:570550,XXXXXXXXX11:570550',
            TAM_FolioVentaCorporativa__c = solVentaFlotillaPrograma.Name,
            TAM_IDExterno__c = solVentaFlotillaPrograma.Name        
        );
        insert SoldExpInc;
        
        TAM_DODSolicitudesPedidoEspecial__c DODSolPedidoEspecial = new TAM_DODSolicitudesPedidoEspecial__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id, 
            TAM_VINDistribuidor__c = 'XXXXXXXXX10',
            TAM_Entrega__c = '570550-TOYOTA POLANCO',
            TAM_VIN__c = 'XXXXXXXXX10',
            TAM_FechaHoraRecibo__c = Date.today()
        );
        insert DODSolPedidoEspecial;
        
        TAM_DODSolicitudesPedidoEspecial__c DODSolPedidoEspecial2 = new TAM_DODSolicitudesPedidoEspecial__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id, 
            TAM_Entrega__c = '570550-TOYOTA POLANCO',
            TAM_FechaHoraRecibo__c = Date.today()
        );
        insert DODSolPedidoEspecial2;
        
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;        
    }

    public static TAM_WrpSolicitudFlotillaProgramaModelos getWrpSolicitudFlotillaProgramaModelos(
        TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma, String strIdCatCentrModelos){
        System.debug('EN getWrpSolicitudFlotillaProgramaModelos...');
        
        //Crea la lista de modelos para TAM_WrpSolicitudFlotillaProgramaModelos.lModelosSeleccionados
        List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> lWrpDatModSel = 
            new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>();

        List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> lWrpDatModSelInv = 
            new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>();

        //Un objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objPasoModelos = 
            new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
        //Inicializa los datos del objeto objPasoModelos
        objPasoModelos.strIdSolicitud = solFlotillaPrograma.id;
        objPasoModelos.strNombre = solFlotillaPrograma.Name;
        objPasoModelos.strIdExterno = solFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial; //solFlotillaPrograma.Name;
        objPasoModelos.strIdCatCentrModelos = strIdCatCentrModelos;

        //Metelo a la lista de lWrpDatModSel
        lWrpDatModSel.add(objPasoModelos);

        //Un objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objPasoModelosInv = 
            new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
        //Inicializa los datos del objeto objPasoModelos
        objPasoModelosInv.strIdSolicitud = solFlotillaPrograma.id;
        objPasoModelosInv.strNombre = solFlotillaPrograma.Name;
        objPasoModelosInv.strIdExterno = solFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial; //solFlotillaPrograma.Name;
        objPasoModelosInv.strIdCatCentrModelos = strIdCatCentrModelos;

        //Metelo a la lista de lWrpDatModSel
        lWrpDatModSelInv.add(objPasoModelosInv);
        
        //Crea el objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos
        TAM_WrpSolicitudFlotillaProgramaModelos objWrpSolFlotProgMod = new TAM_WrpSolicitudFlotillaProgramaModelos(
                solFlotillaPrograma.Name, solFlotillaPrograma.Name, solFlotillaPrograma.Name, 
                String.valueOf(solFlotillaPrograma.TAM_FechaCierreSolicitud__c),
                'Cliente prueba',
                'Cerrada Pedido Especial',
                'UBER',
                'Usuario Prueba',
                String.valueOf(solFlotillaPrograma.TAM_FechaCierreSolicitud__c),
                lWrpDatModSel
        );
        objWrpSolFlotProgMod.lModelosSeleccionadosVin = lWrpDatModSelInv;
        System.debug('EN getWrpSolicitudFlotillaProgramaModelos objWrpSolFlotProgMod: ' + objWrpSolFlotProgMod);
        
        //Regresa al objeto
        return objWrpSolFlotProgMod;
    }

    public static List<TAM_SolicitFlotProgExcepcionVinesCmWrp> getWrpSolFlotProgExcepVinesCom(
        TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma, String strIdCatCentrModelos,
        String sIdDoD){
            
        List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lSolFlotProgExcepVinesCmWrp 
            = new List<TAM_SolicitFlotProgExcepcionVinesCmWrp>();
        TAM_SolicitFlotProgExcepcionVinesCmWrp SolicitFlotProgExcepcionVinesCmWrpPaso 
            = new TAM_SolicitFlotProgExcepcionVinesCmWrp(
                '570550-TOYOTA POLANCO', 
                'XXXXXXXXX10', 
                solFlotillaPrograma.Name, 
                true, 
                false           
            );
            
        //Inicializa el resto de los campos 
        SolicitFlotProgExcepcionVinesCmWrpPaso.strDistribuidor = '';
        SolicitFlotProgExcepcionVinesCmWrpPaso.strVin = 'XXXXXXXXX10';
        SolicitFlotProgExcepcionVinesCmWrpPaso.strFolioExcepcion = '';
        SolicitFlotProgExcepcionVinesCmWrpPaso.bolSeleccionar = false;
        SolicitFlotProgExcepcionVinesCmWrpPaso.bolSeleccionado = false;
        SolicitFlotProgExcepcionVinesCmWrpPaso.strFechaRecibo = '';
        SolicitFlotProgExcepcionVinesCmWrpPaso.strIdRegistroDOD = sIdDoD;
        SolicitFlotProgExcepcionVinesCmWrpPaso.bolVinBloqueado = false;
        SolicitFlotProgExcepcionVinesCmWrpPaso.bolSelBloqueado = false;
        SolicitFlotProgExcepcionVinesCmWrpPaso.bolConfirmaEntrega = true;
        SolicitFlotProgExcepcionVinesCmWrpPaso.strCancelacion = 'Cancelado';
        SolicitFlotProgExcepcionVinesCmWrpPaso.blnConfirmaFactura = true;
        
        //Agrega un objeto a la lista de lSolFlotProgExcepVinesCmWrp
        lSolFlotProgExcepVinesCmWrp.add(SolicitFlotProgExcepcionVinesCmWrpPaso);
        //Regresa lalista
        return lSolFlotProgExcepVinesCmWrp;         
    }


    static testMethod void TAM_SolicitudFlotiProgramFactOtroCteOK() {
        //Inician las pruebas
        Test.startTest();

	        //Consulta los datos del cliente
	        solFlotillaPrograma = [Select Id, Name, TAM_FechaCierreSolicitud__c From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];        
	        System.debug('EN TAM_CargaArchivosCtrlTstOk solFlotillaPrograma: ' + solFlotillaPrograma);
	
	        CatalogoCentMod = [Select id, name From CatalogoCentralizadoModelos__c LIMIT 1];
	        System.debug('EN TAM_CargaArchivosCtrlTstOk CatalogoCentMod: ' + CatalogoCentMod);                      
	
	        CheckOutDetalleSolicitudCompra = [Select id, Name, TAM_IdExterno__c, TAM_TipoPago__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_CatalogoCentralizadoModelos__r.Name From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 1];        
	        System.debug('EN TAM_CargaArchivosCtrlTstOk CheckOutDetalleSolicitudCompra: ' + CheckOutDetalleSolicitudCompra);
	
	        TAM_DODSolicitudesPedidoEspecial__c DODSolPedidoEspecial = [Select id From TAM_DODSolicitudesPedidoEspecial__c Limit 1];
	        SoldExpInc = [Select ID, Name, TAM_listaVINES__c, TAM_FolioVentaCorporativa__c, TAM_IDExterno__c From TAM_SolicitudExpecionIncentivo__c LIMIT 1];
	        SoldExpInc.TAM_IDExterno__c = solFlotillaPrograma.Name;
	        SoldExpInc.TAM_FolioVentaCorporativa__c = solFlotillaPrograma.Name;
	        update SoldExpInc;
	        System.debug('EN TAM_CargaArchivosCtrlTstOk SoldExpInc: ' + SoldExpInc);
	
	        //TAM_listaVINES__c = 'XXXXXXXXX10:570550,XXXXXXXXX11:570550',
	        Map<String, Map<String, String>> mapNoDistVines = new Map<String, Map<String, String>>{'570550' => new Map<String, String>{'XXXXXXXXX10' => 'XXXXXXXXX10', 'XXXXXXXXX11' => 'XXXXXXXXX11'}};
	        //Crea un objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos
	        TAM_WrpSolicitudFlotillaProgramaModelos objWrpSolFlotProgMod = getWrpSolicitudFlotillaProgramaModelos(solFlotillaPrograma, CatalogoCentMod.id);        

	        //Llama al metodo de getMesesSolicitud
	        TAM_SolicitudFlotiProgramFactOtroCteCtrl.getDatosDistribuidores(solFlotillaPrograma.id, CatalogoCentMod.ID, 
	        objWrpSolFlotProgMod);

	        //Llama al metodo de getDatosDistribuidoresInv
	        TAM_SolicitudFlotiProgramFactOtroCteCtrl.getDatosDistribuidoresInv(solFlotillaPrograma.id, CatalogoCentMod.ID, 
	        objWrpSolFlotProgMod);

	        //Crea un objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos
	        List<TAM_SolicitFlotProgExcepcionVinesCmWrp> lSolFlotProgExcepVinesCmWrp = getWrpSolFlotProgExcepVinesCom(
	        solFlotillaPrograma, CatalogoCentMod.id, DODSolPedidoEspecial.id);
	        
	        TAM_SolicitFlotProgExcepcionVinesCmWrp objPaso = new TAM_SolicitFlotProgExcepcionVinesCmWrp();
	        //Inicializa el resto de los campos 
	        objPaso.strDistribuidor = '';
	        objPaso.strVin = '';
	        objPaso.strFolioExcepcion = '';
	        objPaso.strFechaRecibo = '';
	        objPaso.strIdRegistroDOD = DODSolPedidoEspecial.id;
	
	        objPaso.bolSeleccionar = false;
	        objPaso.bolSeleccionado = false;
	        
	        objPaso.bolVinBloqueado = false;
	        objPaso.bolSelBloqueado = false;
	        objPaso.dtFechaEntrega = Date.today(); 
	        objPaso.strTipoPago = '';
	        objPaso.strCancelacion = 'Cancelado';
	        objPaso.bolCancelada = true;
	        objPaso.strMotivoCancelacion = '';
	        objPaso.strStyleCancelacion = '';   
	        objPaso.bolCancelComent = false;
	                
	        objPaso.bolConfirmaEntrega = true;
	        objPaso.bolConfirmado = true;
	                
	        //Llama al metodo de getMesesSolicitud
	        TAM_SolicitudFlotiProgramFactOtroCteCtrl.updateFechaRecibo(lSolFlotProgExcepVinesCmWrp, 'true', 
	            solFlotillaPrograma.id);

	        //Una instancia de TAM_WrpSolicitudFlotillaProgramaModelos
	        TAM_WrpSolicitudFlotillaProgramaModelos objWrpSolFlotProgMod2 = new TAM_WrpSolicitudFlotillaProgramaModelos();
	        TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objWrpDatosModSel = 
	            new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
	        objWrpDatosModSel.strVersion = '';
	        objWrpDatosModSel.strDescripcionColorExterior = '';
	        objWrpDatosModSel.strDescripcionColorInterior = '';
	        objWrpDatosModSel.strCantidadSolicitada = '';
	        objWrpDatosModSel.strTipoVenta = '';
	        objWrpDatosModSel.bolMismoDistribuidor = false;
	        objWrpDatosModSel.bolCreaExcepcion = false;
	        objWrpDatosModSel.strRtTipoSolicitud = '';
	        
	        //Manda llamar la funcion de enviaCorreoInfo
	        TAM_SolicitudFlotiProgramFactOtroCteCtrl.enviaCorreoInfo(solFlotillaPrograma.id, lSolFlotProgExcepVinesCmWrp, 'prueba12345@hotmail.com');
            //Manda llamar la funcion de enviaCorreoInfo
            TAM_SolicitudFlotiProgramFactOtroCteCtrl.convierteFechaDT(DateTime.now());
            //Manda llamar la funcion de getFilialesGpoCorp
            TAM_SolicitudFlotiProgramFactOtroCteCtrl.getFilialesGpoCorp(solFlotillaPrograma.id);
            
            System.assertEquals(1,lSolFlotProgExcepVinesCmWrp.size(),'1 reistro debe ser regresado');
            //System.assertEquals(lSolFlotProgExcepVinesCmWrp, null);
            
        //Fin del las pruebas
        Test.stopTest();            
    }
}