public class ControllerClonarFicha {
    @AuraEnabled
    //Metodo utilizado para obtener las versiones disponibles en base a el nuevo año de la ficha tecnica, agrupando por la serie del Vehiculo
    public static ClonacionWrap obtenerVeriones (String anoFichaTecnica , String serieModelo, String idFichaProd){
        //Query para buscar los modelos en el objeto de Product2 , apartir de la serie del producto y del año capturado en el componentes de la clonación
        List<Product2> listProductos = [Select id,Anio__c,ProductCode,Name,NombreVersion__c FROM Product2 WHERE Serie__c=: serieModelo AND Anio__c =: anoFichaTecnica];
        //Query para obtener los modelos que contiene la ficha de producto base(Ficha a clonar), para mostrar en el componentes las versiones para hacer el match
        List<CodigoModeloFichaTecnica__c> listCodigosModelo = [SELECT CodigoModeloVersion__r.Name,CodigoModeloVersion__c,Estatus__c,FichaTecnica__c,Id,Name,NombreVersion__c,Orden__c FROM CodigoModeloFichaTecnica__c WHERE FichaTecnica__c =: idFichaProd];
        //Se valida que las listas no esten vacias , para posteriormente ingresar los datos a un objeto wrapper. los datos de este objeto wrapper se mostraran en el componente 
        if(!listProductos.isEmpty() && !listCodigosModelo.isEmpty()){
            ClonacionWrap wrapClon = new ClonacionWrap();
            wrapClon.prod2 = listProductos;
            wrapClon.codigoModelo = listCodigosModelo;
            return wrapClon; 
        }else{
            return null;
        } 
        
    }   
    
    @AuraEnabled
    //Metodo utilizado para realizar la clonación de la ficha tecnica de producto , en base a lo que selecciono el usuario en el componente
    public static Id clonacionFichaTP(String fichaOriginal,String anioNuevaFicha,Map<String,String> jsonVersiones){
        Savepoint sp = Database.setSavepoint();
        FichaTecnicaProducto__c ftpClon = new FichaTecnicaProducto__c(); 
        List<String> listModeloNuevo = new List<String>();
        List<ObjValoresClon> listValoresClon = new  List<ObjValoresClon>();
        //Se valida que la entida jsonVersiones no este vacia
        if(!jsonVersiones.isEmpty()){
            for(String code: jsonVersiones.keyset()){
                //Posteriormente se guardan los datos de la entidad jsonVersiones en el objeto ObjValoresClon , en este objeto viene la seleccion del usuario que realizo en el componente
                ObjValoresClon objClon = new ObjValoresClon();
                objClon.modeloNuevo = code;
                objClon.modeloSeleccionado = jsonVersiones.get(code);
                listValoresClon.add(objClon);   
                listModeloNuevo.add(code);
            }  
            
        }
        
        try{
            //Se obtiene los datos de la ficha de producto original , para poder usarlos en el proceso siguiente  
            System.debug('Id que se pasa'+fichaOriginal);          
            //FichaTecnicaProducto__c ftpOriginal = FichaTecnicaController.getFTProducto(fichaOriginal);
            FichaTecnicaProducto__c ftpOriginal = [Select id,Serie__c,Version__c from FichaTecnicaProducto__c where id =: fichaOriginal];
            List<EspecificacionFichaTecnica__c> efts = FichaTecnicaController.getEFT(fichaOriginal);
            //Se crea la base de la nueva ficha de producto correspodiente al año capturado
            ftpClon = new FichaTecnicaProducto__c(
                Anio__c = anioNuevaFicha,
                Serie__c = ftpOriginal.Serie__c, 
                Version__c = ftpOriginal.Version__c
            );
            //Se valida que el objeto a insetar contenga datos , para realizar una operación DML
            if(ftpClon != null){
                insert ftpClon;
            }
            List<CodigoModeloFichaTecnica__c> modelos = new List<CodigoModeloFichaTecnica__c>(); 
            //Se consultan los modelos disponibles en base a la seleccion del usario . 
            List<Product2> versionesNuevas = [Select id,Anio__c,name from product2 WHERE serie__c =: ftpOriginal.Serie__c AND Anio__c =: anioNuevaFicha AND name IN : listModeloNuevo];
            //Validamos que la lista sea diferente a vacia
            if(!versionesNuevas.isEmpty()){
                integer orden = 1;
                //Se recorre a lista de versiones nuevas para crear registros de tipo CodigoModeloFichaTecnica__c que seran los modelos asociados a la nueva ficha de producto
                for(Product2 versionesFicha : versionesNuevas){
                    modelos.add(new CodigoModeloFichaTecnica__c(
                        FichaTecnica__c = ftpClon.Id,
                        Estatus__c = 'Preliminar',
                        Orden__c = orden++,
                        CodigoModeloVersion__c = versionesFicha.id
                    )); 
                }
                //Se valida que la lista modelos sea diferente a vacio , para poder realizar la operción DML
                if(!modelos.isEmpty()){
                    insert modelos;    
                }
            }
            
            Set<id> idVersionesFd = new Set<id>();
            //En la lista idVersionesFd se agregan los id's de las versiones creadas , para despues utilizarlos en el proceso
            if(!modelos.isEmpty()){
                for(CodigoModeloFichaTecnica__c versionId : modelos){
                    idVersionesFd.add(versionId.id);
                }
                
            }
            //En esta consulta se obtienen los atributos y especificaciónes asociadas a la ficha de producto quu es la base para la clonacion
            List<EspecificacionFichaTecnica__c> eftslist = [Select id,TAM_CampoCritico__c,TAM_EstatusCriticalField__c,CodigoModeloFichaTecnica__c,EspecificacionAtributo__c,Atributo__c,NoAplica__c,codeModel__c  from EspecificacionFichaTecnica__c WHERE FichaTecnicaProducto__c =: fichaOriginal] ; 
            List<CodigoModeloFichaTecnica__c> codigoList = new List<CodigoModeloFichaTecnica__c>();
            //Se crea la validacion para la lista idVersionesFd
            if(!idVersionesFd.isEmpty()){
                codigoList = [Select id,FichaTecnica__c,Estatus__c,CodigoModeloVersion__r.Name From CodigoModeloFichaTecnica__c WHERE ID IN : idVersionesFd]; 
                
            }
            
            List<EspecificacionFichaTecnica__c> eftsNueva = new List<EspecificacionFichaTecnica__c>(); 
            //Se recorre la lista con los modelos seleccionados por el usuario
            for(ObjValoresClon modelSelect : listValoresClon){
                //Se recorre la lista con los atributos y especificación existentes en la ficha base 
                for(EspecificacionFichaTecnica__c eft : eftslist){
                    //Se recorre las versiones creadas para la nueva ficha de producto
                    for(CodigoModeloFichaTecnica__c prodLista : codigoList ){
                        //Se crea una validación para poder asociar correctamente las atributos de las ficha base , a la ficha clon en base a la seleccion que realizo el usuario para hacer el match de veriones
                        if(eft.codeModel__c == modelSelect.modeloSeleccionado && prodLista.CodigoModeloVersion__r.name == modelSelect.modeloNuevo){
                            //Se crean registros de tipo EspecificacionFichaTecnica__c para asociarlos a la ficha de producto clon
                            EspecificacionFichaTecnica__c espFichaTecnica = new EspecificacionFichaTecnica__c();
                            espFichaTecnica.CodigoModeloFichaTecnica__c = prodLista.id;
                            espFichaTecnica.EspecificacionAtributo__c =  eft.EspecificacionAtributo__c;
                            espFichaTecnica.FichaTecnicaProducto__c = ftpClon.Id;
                            espFichaTecnica.Atributo__c = eft.Atributo__c;
                            espFichaTecnica.NoAplica__c = eft.NoAplica__c;

                            if(eft.TAM_CampoCritico__c == true){
                                espFichaTecnica.TAM_CampoCritico__c = true;
                                espFichaTecnica.TAM_EstatusCriticalField__c = 'Pendiente Revisión (Planner)';
                            }

                            eftsNueva.add(espFichaTecnica);
 	
                        }
                    }
                }
            }
            //Se valida que la lista eftsNueva sea diferente de vacia, para realizar la operación DML
            if(!eftsNueva.isEmpty()){
                insert eftsNueva;
            }
            
            //Duplicar la ficha de colores     
            //controllerClonarFicha.duplicarFichaColores(listValoresClon, versionesNuevas, ftpClon, fichaOriginal);
            
        }catch(Exception e){
            System.debug('error al clonar ficha'+e.getMessage());
            System.debug('error al clonar linea'+e.getLineNumber());
            Database.rollback(sp); 
        } 
        return ftpClon.id;
        
    }    
    
   /*
    public static void duplicarFichaColores( List<ObjValoresClon> listValoresClon,List<Product2> versionesNuevas,FichaTecnicaProducto__c ftpClon,String fichaOriginal){
        FichaTecnicaProducto__c ftpOriginal = FichaTecnicaController.getFTProducto(fichaOriginal);
        List<EspecificacionFichaTecnica__c> efts = FichaTecnicaController.getEFT(fichaOriginal);
        FichaColores__c fichaColoresOrg = new FichaColores__c();
        FichaColores__c fichaColores = new FichaColores__c();
        List<Producto_Version__c> versionAcabadoIntClon = new List<Producto_Version__c>();
        List<Versiones_Ficha_Colores__c> versionesFichaColorClon = new List<Versiones_Ficha_Colores__c> ();
        List<Versiones_Ficha_Colores__c> versionesFichaColor  = new List<Versiones_Ficha_Colores__c>();
        List<Producto_Version__c> versionAcabadoIntOrg = new List<Producto_Version__c>();
        if(ftpOriginal != null){
            fichaColoresOrg = [Select id,ColoresAgregados__c FROM FichaColores__c WHERE FichaTcnicaProducto__c =: fichaOriginal];     
        }
        if(ftpClon.id != null){
            fichaColores.Activo__c = true;
            fichaColores.FichaTcnicaProducto__c = ftpClon.id;
            fichaColores.ColoresAgregados__c = fichaColoresOrg.ColoresAgregados__c;
            insert  fichaColores;
        }
        
        if(fichaOriginal != null){
            versionesFichaColor =   [SELECT AcabadoInterior__c,AcabadoInterior__r.name,Ficha_Colores__c,Id,Name,Orden__c FROM Versiones_Ficha_Colores__c WHERE Ficha_Colores__r.FichaTcnicaProducto__c =: fichaOriginal];   
        }
        
        if(!versionesFichaColor.isEmpty()){
            for(Versiones_Ficha_Colores__c vFichaOriginal : versionesFichaColor){
                Versiones_Ficha_Colores__c nuevaVersionFichaClon = new Versiones_Ficha_Colores__c();
                nuevaVersionFichaClon.AcabadoInterior__c = vFichaOriginal.AcabadoInterior__c;
                nuevaVersionFichaClon.Ficha_Colores__c = fichaColores.id;
                nuevaVersionFichaClon.orden__c = vFichaOriginal.Orden__c;
                versionesFichaColorClon.add(nuevaVersionFichaClon);
            }   
        }
        if(!versionesFichaColorClon.isEmpty()){
            insert versionesFichaColorClon;
        }	
        
        //Relacionar versiones a los acabados interiores 
        if(fichaOriginal != null){
            versionAcabadoIntOrg = [SELECT Id,Version__r.AcabadoInterior__r.id,Version__r.AcabadoInterior__r.Name,Name,Producto__r.name,Orden__c,Producto__c,Version__c FROM Producto_Version__c WHERE Version__r.Ficha_Colores__r.FichaTcnicaProducto__c =: fichaOriginal];     
        }
        
        Integer countProd = 0;        
        for(Producto_Version__c versionOrigin : versionAcabadoIntOrg) {
            for(Product2 modelosProd : versionesNuevas){
                for(ObjValoresClon modelSelect : listValoresClon){
                    for(Versiones_Ficha_Colores__c versionesClon : versionesFichaColorClon){
                        if(modelSelect.modeloSeleccionado == versionOrigin.Producto__r.name && modelosProd.name == modelSelect.modeloNuevo){	
                            Producto_Version__c prodAcabadoOrg = new Producto_Version__c(); 
                            prodAcabadoOrg.Orden__c = countProd++;
                            prodAcabadoOrg.Producto__c = modelosProd.id;
                            prodAcabadoOrg.Version__c = versionesClon.id;
                            versionAcabadoIntClon.add(prodAcabadoOrg); 
                            
                        }
                    }
                }	
            }
        }         
        if(!versionAcabadoIntClon.isEmpty()){
            insert versionAcabadoIntClon;
        }
        
        List<Color_version__c> colorVersionOrg = [SELECT Id,orden__c,ColorInterior__c,Version__r.AcabadoInterior__r.id,version__r.AcabadoInterior__r.name,Name,Version__r.name,Version__c,Version__r.Ficha_Colores__r.name FROM Color_version__c where Version__r.Ficha_Colores__r.FichaTcnicaProducto__c =: fichaOriginal];
        List<Color_version__c> listNew =  new  List<Color_version__c>();
        
        for(Color_version__c versionesClon : colorVersionOrg){
            Color_version__c clonedRecord  = new Color_version__c();
            clonedRecord.orden__c = versionesClon.Orden__c;
            listNew.add(clonedRecord);
        }
        List<Color_version__c> colVersion = new List<Color_version__c>();
            for(Versiones_Ficha_Colores__c versionesClon : versionesFichaColorClon){ 
                for(Color_version__c colorList : listNew){
                    if(versionesClon.AcabadoInterior__r.id == colorList.Version__c){
                        Color_version__c coloresVer = new Color_version__c();
                        coloresVer.ColorInterior__c = colorList.ColorInterior__c; 
                        coloresVer.orden__c = colorList.orden__c;
                        coloresVer.Version__c = versionesClon.id;
                        colVersion.add(coloresVer);
                    }
                }
            }   
        insert colVersion;
       
    }*/
    
    public class ClonacionWrap {	
        @AuraEnabled public List<Product2> prod2 {get; set;}
        @AuraEnabled public List<CodigoModeloFichaTecnica__c> codigoModelo {get; set;}    
        
    }
    
    public class ObjValoresClon {	
        @AuraEnabled public String modeloNuevo {get; set;}
        @AuraEnabled public String modeloSeleccionado {get; set;}
        @AuraEnabled public Id versionId {get; set;}  
        
    } 
    
}