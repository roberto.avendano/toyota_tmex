/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Batch para procesar estados de cuenta y provisiones de incentivos para obtener variaciones.
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		09/Junio/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
public class TAM_CalculoVariacionBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    //Constantes
	static final Boolean BOOLEAN_TRUE = TRUE;
    static final Boolean BOOLEAN_FALSE = FALSE;
    static final String STRING_AUTORIZADO_TMEX = 'Pago Autorizado TMEX'; 
    static final Double DOUBLE_CERO = 0.0; 
    
    //Variables
    public final String AllVinesDelMes;
    public static Set<String> setAllVinesDelMes;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapDetalleEstadoCuenta {get;set;}

    //Constructor
    public TAM_CalculoVariacionBatch(){
        AllVinesDelMes = 	'SELECT		Id, TAM_VIN__c, TAM_Monto_sin_IVA__c  '+
                            'FROM 		TAM_DetalleEstadoCuenta__c '+
                            'WHERE 		TAM_Historico_Estado_Cuenta__c =: BOOLEAN_FALSE '+
                            'AND		TAM_Monto_sin_IVA__c != NULL '+
                            'AND		TAM_VIN__c != NULL '+
                            'AND		TAM_VariacionValidada__c =: BOOLEAN_FALSE '+
                            'AND 		TAM_Estatus_Finanzas__c =: STRING_AUTORIZADO_TMEX ';
    }
    
    //Start
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(AllVinesDelMes);
    }
    
    //Excecute
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        List<TAM_DetalleEstadoCuenta__c> lstUpdateEdoCuenta = new List<TAM_DetalleEstadoCuenta__c>();
        mapDetalleEstadoCuenta = new Map<String, TAM_DetalleEstadoCuenta__c>();
        Set<String> setVIN = new Set<String>();
        if(!scope.isEmpty()){
            for(sObject objScope : scope){
                TAM_DetalleEstadoCuenta__c objDetalleEdoCta = (TAM_DetalleEstadoCuenta__c)objScope;
                setVIN.add(objDetalleEdoCta.TAM_VIN__c);
                mapDetalleEstadoCuenta.put(objDetalleEdoCta.TAM_VIN__c,objDetalleEdoCta);
                objDetalleEdoCta.TAM_VariacionValidada__c = true;
                lstUpdateEdoCuenta.add(objDetalleEdoCta);
            }
            if(!lstUpdateEdoCuenta.isEmpty()){
                try {
                    Database.update(lstUpdateEdoCuenta,TAM_DetalleEstadoCuenta__c.Id);
                }catch (DmlException e) {
                    System.debug(e.getMessage());
                } 
            }
            
            //Consultar provisiones
            TAM_CalculoVariacionProvisionesClass.getDetalleCuentaPagados(mapDetalleEstadoCuenta);
        }
    }
    
    //Finish
    public void finish(Database.BatchableContext BC){} 
}