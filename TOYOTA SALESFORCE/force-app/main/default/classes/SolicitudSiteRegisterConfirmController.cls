public class SolicitudSiteRegisterConfirmController {
    public String csId{get;set;}
    
    public SolicitudSiteRegisterConfirmController(ApexPages.StandardController stdcontroller){
        csId =stdcontroller.getId();
    }
    
    public PageReference goLogin(){
        PageReference page = System.Page.SolicitudSiteLogin;
        //PageReference page = new PageReference('/SiteRegisterConfirm');
        page.setRedirect(true);
        return page;
    }
    
}