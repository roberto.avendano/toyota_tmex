/******************************************************************************** 
    Desarrollado por:   Globant de México
    Autor:              Roberto Carlos Avendaño Quintana
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para crear el estado de cuenta a partir de la provisión de incentivos
******************************************************************************* */
public class TAM_EstadoCuentaIncentivos {
    
    
    public static void generaEstadoCuenta(Id provisionCerrada,Date fechaCierreProvision,String tipoProvision, Date fechaCierrePoliticaIncentivo){
        
        decimal diasVigencia = [Select TAM_DiasVigencia__c From TAM_DetalleEdoCta__c Limit 1].TAM_DiasVigencia__c;
        Integer diasVigenciaInt = diasVigencia.intValue();
        
        //Generación de el objeto Master del Estado de Cuenta
        Map<String, TAM_EstadoCuenta__c> mapaSolicitud = new Map<String, TAM_EstadoCuenta__c>();
        List<TAM_DetalleEstadoCuenta__c> lineasEstadoCuenta = new List<TAM_DetalleEstadoCuenta__c>();
        List<TAM_EstadoCuenta__c> listadealerEstadoCuenta = new  List<TAM_EstadoCuenta__c>();
        String fechaTexto;
        
        //Se obtien las cuenta con tipo de registro "Distribuidor"
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        List<Account> listaDistribuidores = [Select id,TAM_Gestor_Estado_Cuenta__r.id,name,Codigo_Distribuidor__c From Account WHERE recordtypeId =: recordTypeDistribuidor
                                            AND TAM_ActivarEdoCta__c = true];
        
        //Validamos la fecha de cierre de la provisión sea diferente de nulo 
        if(fechaCierreProvision != null){
            fechaTexto = obtieneFechaTexto(fechaCierreProvision); 
        }
        
        //Validamos que la lista listaDistribuidores sea diferente de vacia
        if(!listaDistribuidores.isEmpty()){
            for(Account acc : listaDistribuidores){
                TAM_EstadoCuenta__c dealerEstadoCuenta = new TAM_EstadoCuenta__c();
                dealerEstadoCuenta.TAM_CodigoDealer__c = acc.Codigo_Distribuidor__c;
                dealerEstadoCuenta.TAM_NombreDealer__c = acc.name;
                system.debug('fechaCierrePoliticaIncentivo'+fechaCierrePoliticaIncentivo);
                if(fechaCierrePoliticaIncentivo != null){
                	dealerEstadoCuenta.TAM_FechaInicio__c = fechaCierrePoliticaIncentivo;
                	dealerEstadoCuenta.TAM_FechaCierre__c = fechaCierrePoliticaIncentivo+diasVigenciaInt;
                }
                dealerEstadoCuenta.TAM_FechaEstadoCuenta__c = fechaTexto;
                dealerEstadoCuenta.TAM_EstadoRegistro__c = 'Abierto';
                dealerEstadoCuenta.Id_Externo__c      = acc.Codigo_Distribuidor__c+'-'+fechaTexto;
                dealerEstadoCuenta.name = acc.Codigo_Distribuidor__c+'-'+fechaTexto;
                if(acc.TAM_Gestor_Estado_Cuenta__r.id != null){
                    dealerEstadoCuenta.ownerId = acc.TAM_Gestor_Estado_Cuenta__r.id;
                }
                listadealerEstadoCuenta.add(dealerEstadoCuenta);
                
            }
        }
        
        //Si la lista es diferente de vacia se insertan los registros
        if(!listadealerEstadoCuenta.isEmpty()){
            Schema.SObjectField idExterno = TAM_EstadoCuenta__c.Fields.Id_Externo__c;
            try{
                Database.UpsertResult [] cr = Database.upsert(listadealerEstadoCuenta,idExterno, false);
                for (Database.UpsertResult objDtSvr : cr){
                    if(objDtSvr.isSuccess())
                        System.debug('Regitros de estado de cuenta creados exitosamente ' + objDtSvr.id);
                } 
            }catch(Exception e){
                system.debug('Error en upsert de la lista listadealerEstadoCuenta'+e.getMessage());  
            } 
        }
    
        //Se verifica que la lista sea diferente de vacia y se agrega a un mapa de datos
        Set<String> edoCtaInsertados = new Set<String>();
        if(!listadealerEstadoCuenta.isEmpty()){
            for(TAM_EstadoCuenta__c i : listadealerEstadoCuenta){
                mapaSolicitud.put(i.TAM_CodigoDealer__c,i);
                edoCtaInsertados.add(i.Id);
            }
        }
        
        //Llamada a metodo 'actualizarRegistrosPendientes para actualizar los registros pendientes al nuevo estado de cuenta
        actualizarRegistrosPendientes(fechaCierreProvision,mapaSolicitud);
        
        //Obtener el tipo de registro de la provisión cerrada
        String recordIdProvision; 
        if(tipoProvision == 'Retail'){
            recordIdProvision  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        }
        
        if(tipoProvision == 'VentaCorporativa'){
            recordIdProvision  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId();
        }
        
        if(tipoProvision == 'Bono'){
            recordIdProvision  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId();
        }        
        
        
        
        List<TAM_DetalleProvisionIncentivo__c> lineasProvisionCerrada = new  List<TAM_DetalleProvisionIncentivo__c>();
        if(tipoProvision == 'Retail' || tipoProvision == 'VentaCorporativa'){
            //Se realiza una query para obtener los detallas de la provisión a partir de el id de provisión

            lineasProvisionCerrada = [SELECT Id,TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c,TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c,TAM_TMEXEfectivoMenor__c,TAM_VIN__r.name,Incentivo_Total_c_TFS__c,Incentivo_Total_s_TFS__c,Name,RecordTypeId,
                                      RetailSinReversa_Cerrado__c,TAM_AnioModelo__c,TAM_BonoLealtad__c,TAM_Clasificacion__c,
                                      TAM_CodigoDealer__c,TAM_CodigoVenta__c,TAM_ExepcionIncentivo__c,TAM_Factura__c,TAM_FechaEnvio2__c,
                                      TAM_FechaEnvio__c,TAM_FechaVenta__c,TAM_FirstName__c,TAM_Guardado__c,TAM_IdExterno__c,TAM_IncentivoConExcepcion__c,
                                      TAM_IncentivoDealer__c,TAM_IncentivoProvisionado__c,TAM_IncentivoTMEX_Efectivo__c,TAM_IncentivoTMEX_PF__c,TAM_LastName__c,
                                      TAM_Lealtad__c,TAM_Modelo__c,TAM_NombreDealer__c,TAM_PagadoConTFS__c,TAM_PagadoSinTFS__c,TAM_PoliticaIncentivos__c,TAM_PrecioVehiculo__c,
                                      TAM_ProductoFinanciero__c,TAM_Propietario__c,TAM_ProvicionarEfectivo__c,TAM_Provisionado__c,TAM_ProvisionarLealtad__c,TAM_ProvisionarProdFinanciero__c,
                                      TAM_ProvisionIncentivos__c,TAM_Serie__c,TAM_SolicitadoDealer__c,TAM_SolicitudExcepcionIncentivo__c,TAM_TipoMovimiento__c,TAM_TransmitirDealer__c,TAM_TransmitirLealtad__c,
                                      TAM_Version__c,TAM_VIN__c FROM TAM_DetalleProvisionIncentivo__c WHERE TAM_ProvisionIncentivos__c =: provisionCerrada AND TAM_TransmitirDealer__c = true AND RecordtypeId =: recordIdProvision];
        

         }
        
        if(tipoProvision == 'Bono'){
            //Se realiza una query para obtener los detallas de la provisión a partir de el id de provisión
            lineasProvisionCerrada = [SELECT Id,TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c,TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c,TAM_TMEXEfectivoMenor__c,TAM_VIN__r.name,Incentivo_Total_c_TFS__c,Incentivo_Total_s_TFS__c,Name,RecordTypeId,
                                      RetailSinReversa_Cerrado__c,TAM_AnioModelo__c,TAM_BonoLealtad__c,TAM_Clasificacion__c,
                                      TAM_CodigoDealer__c,TAM_CodigoVenta__c,TAM_ExepcionIncentivo__c,TAM_Factura__c,TAM_FechaEnvio2__c,
                                      TAM_FechaEnvio__c,TAM_FechaVenta__c,TAM_FirstName__c,TAM_Guardado__c,TAM_IdExterno__c,TAM_IncentivoConExcepcion__c,
                                      TAM_IncentivoDealer__c,TAM_IncentivoProvisionado__c,TAM_IncentivoTMEX_Efectivo__c,TAM_IncentivoTMEX_PF__c,TAM_LastName__c,
                                      TAM_Lealtad__c,TAM_Modelo__c,TAM_NombreDealer__c,TAM_PagadoConTFS__c,TAM_PagadoSinTFS__c,TAM_PoliticaIncentivos__c,TAM_PrecioVehiculo__c,
                                      TAM_ProductoFinanciero__c,TAM_Propietario__c,TAM_ProvicionarEfectivo__c,TAM_Provisionado__c,TAM_ProvisionarLealtad__c,TAM_ProvisionarProdFinanciero__c,
                                      TAM_ProvisionIncentivos__c,TAM_Serie__c,TAM_SolicitadoDealer__c,TAM_SolicitudExcepcionIncentivo__c,TAM_TipoMovimiento__c,TAM_TransmitirDealer__c,TAM_TransmitirLealtad__c,
                                      TAM_Version__c,TAM_VIN__c FROM TAM_DetalleProvisionIncentivo__c WHERE TAM_ProvisionIncentivos__c =: provisionCerrada AND TAM_TransmitirBonoEfectivo__c  = true AND RecordtypeId =: recordIdProvision];
            
            
        }
        
        //Si la provisión cerrada es de Reversas se invoca el metodo generaEstadoCuentaReversa
        if(tipoProvision == 'Reversa'){
            generaEstadoCuentaReversa(provisionCerrada,mapaSolicitud,fechaCierrePoliticaIncentivo);
            
        }
        
        //Si es de tipo retail se busca si existen registros de excepciónes en los estados de cuenta insertados
        Set<String> listVINTransmitidos = new Set<String>();
        if (tipoProvision == 'Retail' || tipoProvision == 'VentaCorporativa') {
            for (TAM_DetalleProvisionIncentivo__c detalleVIN : lineasProvisionCerrada) {
                 listVINTransmitidos.add(detalleVIN.TAM_VIN__r.name);

            }

        }

        //Apartir de la lista de VINES Transmitidos se busca si existen como excepciónes en los estados de cuenta creados
        List<TAM_DetalleEstadoCuenta__c> detalleExistentes = New List<TAM_DetalleEstadoCuenta__c>();
        detalleExistentes = [Select id,TAM_IDExterno__c,TAM_VIN__c,TAM_Tipo_Movimiento__c FROM
                                                        TAM_DetalleEstadoCuenta__c WHERE TAM_EstadoCuenta__c IN: edoCtaInsertados AND TAM_VIN__c  IN: listVINTransmitidos
                                                        AND TAM_Movimiento__c = 'Excepción'];
                
        //Si el query es diferente de vacio, se recorre y se guardo en un mapa (VIN,DetalleEstadoCuenta)
        Map<String,TAM_DetalleEstadoCuenta__c> mapVINByDetalle = new Map<String,TAM_DetalleEstadoCuenta__c> ();
        if(!detalleExistentes.isEmpty()){
            for(TAM_DetalleEstadoCuenta__c detalleRegistro : detalleExistentes){
                mapVINByDetalle.put(detalleRegistro.TAM_VIN__c,detalleRegistro);
            }
        }

        //Se recorre la lista y con el codigo dealer se recorre el mapa para obtener los dealers que tengan lineas del estado de cuenta
        for(TAM_DetalleProvisionIncentivo__c detallesP : lineasProvisionCerrada){
            TAM_DetalleEstadoCuenta__c registroExistente = new TAM_DetalleEstadoCuenta__c();
            if(mapVINByDetalle.containsKey(detallesP.TAM_VIN__r.name)){
                registroExistente = mapVINByDetalle.get(detallesP.TAM_VIN__r.name); 
            }
            TAM_EstadoCuenta__c keyDealer = mapaSolicitud.get(detallesP.TAM_CodigoDealer__c);
            if(keyDealer != null && (detallesP.TAM_VIN__r.name != registroExistente.TAM_VIN__c)){
                if(detallesP.TAM_CodigoDealer__c == keyDealer.TAM_CodigoDealer__c){
                    String recordIdEdoCta;
                    //Retail
                    if(detallesP.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId()){
                        recordIdEdoCta = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                    }
                    //Venta Corporativa 
                    if(detallesP.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId()){
                        recordIdEdoCta = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Flotilla').getRecordTypeId();
                    }
                    
                    //Bonos
                    if(detallesP.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId()){
                        recordIdEdoCta = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId();
                    }
                    
                    TAM_DetalleEstadoCuenta__c lineasEdoCuenta = new TAM_DetalleEstadoCuenta__c();
                    lineasEdoCuenta.TAM_DetalleProvision__c = detallesP.id;
                    lineasEdoCuenta.TAM_FechaCaducidad__c = fechaCierrePoliticaIncentivo+diasVigenciaInt;
                    lineasEdoCuenta.TAM_RegistroProvisionado__c = true;
                    lineasEdoCuenta.TAM_EstadoCuenta__c = keyDealer.Id;
                    lineasEdoCuenta.TAM_VIN__c  = detallesP.TAM_VIN__r.name;
                    lineasEdoCuenta.TAM_IDExterno__c  = detallesP.id+'-'+detallesP.TAM_VIN__r.name;
                    lineasEdoCuenta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
                    lineasEdoCuenta.TAM_pagoPorTFS__c = detallesP.TAM_PagadoConTFS__c;
                    lineasEdoCuenta.TAM_IntercambioEdoCta__c =  false;
                    lineasEdoCuenta.TAM_Estatus_Finanzas__c = 'Pendiente';
                    //Mapeo Nuevo
					lineasEdoCuenta.TAM_CodigoDealer__c = detallesP.TAM_CodigoDealer__c;
                    if(detallesP.TAM_FechaVenta__c != null){
                    	lineasEdoCuenta.TAM_FechaVenta__c = date.valueOf(detallesP.TAM_FechaVenta__c);
                    }
                    lineasEdoCuenta.TAM_Anio_Incentivo__c = detallesP.TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c;   
                    lineasEdoCuenta.TAM_Periodo__c = detallesP.TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c;    
                    lineasEdoCuenta.TAM_Movimiento__c = detallesP.TAM_TipoMovimiento__c;
                    lineasEdoCuenta.TAM_Codigo_Producto__c = detallesP.TAM_Modelo__c;
                    lineasEdoCuenta.TAM_NombreDealer__c = detallesP.TAM_NombreDealer__c;
                    lineasEdoCuenta.TAM_Serie__c = detallesP.TAM_Serie__c;
                    lineasEdoCuenta.TAM_AnioModelo__c = detallesP.TAM_AnioModelo__c;

                    
                    //Retail
                    if(detallesP.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId()){
                        lineasEdoCuenta.TAM_IncentivoPropuestoDealer__c = detallesP.TAM_TMEXEfectivoMenor__c;    
                    }
                    //Venta Corporativa
                    if(detallesP.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId()){
                        lineasEdoCuenta.TAM_IncentivoPropuestoDealer__c = detallesP.TAM_IncentivoTMEX_Efectivo__c;    
                    }
                    
                    lineasEdoCuenta.recordtypeId = recordIdEdoCta;
                    lineasEstadoCuenta.add(lineasEdoCuenta);
                    
                    
                } 
            }
        }  
        
        if(!lineasEstadoCuenta.isEmpty()){
            Schema.SObjectField idExterno = TAM_DetalleEstadoCuenta__c.Fields.TAM_IDExterno__c;
            try{
                system.debug('lineas a crear'+lineasEstadoCuenta);
                List<Database.UpsertResult> lcr = Database.upsert(lineasEstadoCuenta,idExterno, false);
                for (Database.UpsertResult objDtSvr : lcr){
                    if(!objDtSvr.isSuccess())
                        System.debug('Regitros de detalle de cuenta creados exitosamente ' + objDtSvr.getErrors()[0].getMessage());
                }      
                
            }catch(Exception e){
                system.debug('Error en upsert de la lista lineasEstadoCuenta'+ e.getMessage());
            }
        }
          
    }
    
    //Metodo que busca los detalles del estado de cuenta "Pendientes" y los va jalando al estado de cuenta actual
    public static void actualizarRegistrosPendientes(Date fechaCierreProvision,Map<String,TAM_EstadoCuenta__c> mapaSolicitud){
        List<TAM_DetalleEstadoCuenta__c> listaDetalles = new List<TAM_DetalleEstadoCuenta__c>();
        String estadoCuentaAnterior;
        if(fechaCierreProvision != null){
            estadoCuentaAnterior = obtieneFechaTexto(fechaCierreProvision.addMonths(-1));
            system.debug('estadoCuenta anterior'+estadoCuentaAnterior);
        }
        //Se obtien los detalles de estado de cuenta que se encuentre en estatus de pendiente 
        List<TAM_DetalleEstadoCuenta__c> detalleEstadoCuenta = [Select id,TAM_EstadoCuenta__r.TAM_CodigoDealer__c from TAM_DetalleEstadoCuenta__c WHERE TAM_EstadoCuenta__r.TAM_FechaEstadoCuenta__c  =: estadoCuentaAnterior 
                                                                AND (TAM_Estatus_Finanzas__c = 'Pendiente' or TAM_Estatus_Finanzas__c = 'Autorizado para pago') 
                                                                AND (TAM_EstatusVINEdoCta__c = 'Pendiente de cobro' or TAM_EstatusVINEdoCta__c = 'Pendiente de Revisión' or TAM_EstatusVINEdoCta__c = '' ) 
                                                                ];
        
        system.debug('lineas pendientes'+detalleEstadoCuenta); 
        //Validamos la lista detalleEstadoCuenta sea diferente de vacia
        if(!detalleEstadoCuenta.isEmpty()){
            for(TAM_DetalleEstadoCuenta__c detallesE : detalleEstadoCuenta){
                if(mapaSolicitud.containsKey(detallesE.TAM_EstadoCuenta__r.TAM_CodigoDealer__c)){
                    TAM_EstadoCuenta__c keyDealer = mapaSolicitud.get(detallesE.TAM_EstadoCuenta__r.TAM_CodigoDealer__c);
                    TAM_DetalleEstadoCuenta__c nuevoDetalle = new TAM_DetalleEstadoCuenta__c();
                    nuevoDetalle.id		=   detallesE.Id;
                    nuevoDetalle.TAM_EstadoCuenta__c = keyDealer.id;
                    listaDetalles.add(nuevoDetalle);
                }
            }
        }
        
        
        if(!listaDetalles.isEmpty()){	
            system.debug('lineas a insertar'+listaDetalles);
            update listaDetalles;
            
        }
        
        
    }
    
    
    public static void generaEstadoCuentaReversa(Id provisionCerrada,Map<String, TAM_EstadoCuenta__c> mapEdoCta,Date fechaCierrePoliticaIncentivo){

        decimal diasVigencia = [Select TAM_DiasVigencia__c From TAM_DetalleEdoCta__c Limit 1].TAM_DiasVigencia__c;
        Integer diasVigenciaInt = diasVigencia.intValue();

        List<TAM_DetalleProvisionIncentivo__c>lineasProvisionCerrada = new List<TAM_DetalleProvisionIncentivo__c>();
        List<TAM_DetalleEstadoCuenta__c> listaDetalleEdoCta = new List<TAM_DetalleEstadoCuenta__c>();
        
        //Se realiza una query para obtener los detalles de la provisión a partir de el id de provisión
        lineasProvisionCerrada = [SELECT Id,TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c,TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c,TAM_BonoTMEX_Efectivo__c,TAM_TMEXEfectivoMenor__c,TAM_VIN__r.name,Incentivo_Total_c_TFS__c,Incentivo_Total_s_TFS__c,Name,RecordTypeId,
                                  RetailSinReversa_Cerrado__c,TAM_AnioModelo__c,TAM_BonoLealtad__c,TAM_Clasificacion__c,
                                  TAM_CodigoDealer__c,TAM_CodigoVenta__c,TAM_ExepcionIncentivo__c,TAM_Factura__c,TAM_FechaEnvio2__c,
                                  TAM_FechaEnvio__c,TAM_FechaVenta__c,TAM_FirstName__c,TAM_Guardado__c,TAM_IdExterno__c,TAM_IncentivoConExcepcion__c,
                                  TAM_IncentivoDealer__c,TAM_IncentivoProvisionado__c,TAM_IncentivoTMEX_Efectivo__c,TAM_IncentivoTMEX_PF__c,TAM_LastName__c,
                                  TAM_Lealtad__c,TAM_Modelo__c,TAM_NombreDealer__c,TAM_PagadoConTFS__c,TAM_PagadoSinTFS__c,TAM_PoliticaIncentivos__c,TAM_PrecioVehiculo__c,
                                  TAM_ProductoFinanciero__c,TAM_Propietario__c,TAM_ProvicionarEfectivo__c,TAM_Provisionado__c,TAM_ProvisionarLealtad__c,TAM_ProvisionarProdFinanciero__c,
                                  TAM_ProvisionIncentivos__c,TAM_Serie__c,TAM_SolicitadoDealer__c,TAM_SolicitudExcepcionIncentivo__c,TAM_TipoMovimiento__c,TAM_TransmitirDealer__c,TAM_TransmitirLealtad__c,
                                  TAM_Version__c,TAM_VIN__c FROM TAM_DetalleProvisionIncentivo__c WHERE TAM_ProvisionIncentivos__c =: provisionCerrada AND TAM_TransmitirDealer__c  = true AND TAM_Clasificacion__c =: 'Con Reversa'];
        
        if(!lineasProvisionCerrada.isEmpty()){
            for(TAM_DetalleProvisionIncentivo__c detalleProv : lineasProvisionCerrada){
                TAM_EstadoCuenta__c keyDealer = mapEdoCta.get(detalleProv.TAM_CodigoDealer__c);
                String tipoRegistoEdoCta;
                if(detalleProv.TAM_CodigoDealer__c == keyDealer.TAM_CodigoDealer__c){
                    //Reversa tipo Retail
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId()){
                        tipoRegistoEdoCta  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                        
                    }
                    //Reversa tipo Venta Corporativa
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId()){
                        tipoRegistoEdoCta  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Flotilla').getRecordTypeId();
                        
                    }
                    //Reversa tipo Bono
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId()){
                        tipoRegistoEdoCta  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                        
                    }
                    
                    TAM_DetalleEstadoCuenta__c lineasEdoCuenta = new TAM_DetalleEstadoCuenta__c();
                    lineasEdoCuenta.TAM_DetalleProvision__c = detalleProv.id;
                    if(fechaCierrePoliticaIncentivo != null){
                    	lineasEdoCuenta.TAM_FechaCaducidad__c = fechaCierrePoliticaIncentivo+diasVigenciaInt;
                    }
                    lineasEdoCuenta.TAM_RegistroProvisionado__c = true;
                    lineasEdoCuenta.TAM_EstadoCuenta__c = keyDealer.Id;
                    lineasEdoCuenta.TAM_VIN__c  = detalleProv.TAM_VIN__r.name;
                    lineasEdoCuenta.TAM_IDExterno__c  = detalleProv.id+'-'+detalleProv.TAM_VIN__r.name;
                    lineasEdoCuenta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
                    lineasEdoCuenta.TAM_pagoPorTFS__c = detalleProv.TAM_PagadoConTFS__c;
                    lineasEdoCuenta.TAM_Tipo_Movimiento__c = 'RVRSL';
                    lineasEdoCuenta.TAM_IntercambioEdoCta__c =  false;
                    //Mapeo Nuevo
					lineasEdoCuenta.TAM_CodigoDealer__c = detalleProv.TAM_CodigoDealer__c;
                    if(detalleProv.TAM_FechaVenta__c != null){
                    	lineasEdoCuenta.TAM_FechaVenta__c = date.valueOf(detalleProv.TAM_FechaVenta__c);
                    }
                    lineasEdoCuenta.TAM_Anio_Incentivo__c = detalleProv.TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c;   
                    lineasEdoCuenta.TAM_Periodo__c = detalleProv.TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c; 
                    lineasEdoCuenta.TAM_Movimiento__c = detalleProv.TAM_TipoMovimiento__c;
                    lineasEdoCuenta.TAM_Codigo_Producto__c = detalleProv.TAM_Modelo__c;
                    lineasEdoCuenta.TAM_NombreDealer__c = detalleProv.TAM_NombreDealer__c;
                    lineasEdoCuenta.TAM_Serie__c = detalleProv.TAM_Serie__c;
                    lineasEdoCuenta.TAM_AnioModelo__c = detalleProv.TAM_AnioModelo__c;
                    
                    //Bono
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId()){
                        lineasEdoCuenta.TAM_IncentivoPropuestoDealer__c = detalleProv.TAM_BonoTMEX_Efectivo__c; 
                         lineasEdoCuenta.RecordTypeId = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                    }
                    //Retail
                    system.debug('record Prov'+detalleProv.RecordTypeId+'=='+ Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId());
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId()){
                        system.debug('entro a RVSL Retail'+ detalleProv.TAM_IncentivoTMEX_Efectivo__c);
                        lineasEdoCuenta.TAM_IncentivoPropuestoDealer__c = detalleProv.TAM_IncentivoTMEX_Efectivo__c; 
                         lineasEdoCuenta.RecordTypeId = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                    }
                    //Venta Corporativa
                    if(detalleProv.RecordTypeId == Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId()){
                        lineasEdoCuenta.TAM_IncentivoPropuestoDealer__c = detalleProv.TAM_IncentivoTMEX_Efectivo__c;
                        lineasEdoCuenta.RecordTypeId =  Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Flotilla').getRecordTypeId();
                    }
                    lineasEdoCuenta.TAM_Estatus_Finanzas__c = 'Pendiente';
                    listaDetalleEdoCta.add(lineasEdoCuenta);
                    
                }
                
            }   
            
        }
        
        if(!listaDetalleEdoCta.isEmpty()){
            Schema.SObjectField idExterno = TAM_DetalleEstadoCuenta__c.Fields.TAM_IDExterno__c;
            try{
                system.debug('lineas a crear'+listaDetalleEdoCta);
                List<Database.UpsertResult> lcr = Database.upsert(listaDetalleEdoCta,idExterno, false);
                system.debug('lCR'+lcr);
                for (Database.UpsertResult objDtSvr : lcr){
                    if(objDtSvr.isSuccess())
                        System.debug('Regitros de detalle de cuenta creados exitosamente ' + objDtSvr.getErrors()[0].getMessage());
                }      
                
            }catch(Exception e){
                system.debug('Error en upsert de la lista lineasEstadoCuenta'+ e.getMessage());
            }
        } 
    }  
    
    
    public static String obtieneFechaTexto(Date fechaIn){
        Map<Integer,String> mapFecha = new Map<Integer,String>();
        String fechaTexto;
        
        Date fecha = fechaIn;
        Integer mes = fecha.month();
        Integer anio = fecha.year();
        
        mapFecha.put(1,'ENERO');
        mapFecha.put(2,'FEBRERO'); 
        mapFecha.put(3,'MARZO');
        mapFecha.put(4,'ABRIL');
        mapFecha.put(5,'MAYO');
        mapFecha.put(6,'JUNIO');
        mapFecha.put(7,'JULIO');
        mapFecha.put(8,'AGOSTO');
        mapFecha.put(9,'SEPTIEMBRE');
        mapFecha.put(10,'OCTUBRE');
        mapFecha.put(11,'NOVIEMBRE');
        mapFecha.put(12,'DICIEMBRE');
        
        fechaTexto = mapFecha.get(mes)+' '+anio;
        
        return fechaTexto;
        
    }
    
}