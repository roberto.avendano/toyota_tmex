public with sharing class EvaluacionDealerReportSf1Controller {
	public String evalName{get;set;}
	
	public EvaluacionDealerReportSf1Controller(ApexPages.StandardController controller) {
		evalName = ApexPages.currentPage().getParameters().get('evalName');
	}

	public List<Actividad_Plan_Integral__c> getApis(){
		return [
			SELECT Name, Condicion_Observada__c, Celula_Area__c, Actividad_Mejora__c, 
				Referencia__c, CreatedById, CreatedBy.Name, OwnerId, Owner.Name, Fecha_Compromiso__c, 
				Fecha_Realizacion__c, Validado_TMEX__c, Acciones__c, Observaciones__c, 
				Respuestas_Preguntas_TSM__r.Pregunta_Relacionada__r.Reactivo__c, 
				RespuestasObjetosTSM__r.Objeto_Evaluacion_Relacionado__r.Objeto_de_Evaluacion__c, 
				Respuestas_Preguntas_TSM__r.Pregunta_Relacionada__r.Metodo_de_Evaluacion__c,
				RespuestasObjetosTSM__r.Objeto_Evaluacion_Relacionado__r.Metodos_de_Evaluacion__c,
				Respuestas_Preguntas_TSM__r.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,
				Respuestas_Preguntas_TSM__r.Respuesta_Pregunta__c, Responsable_TMEX__r.Name, 
				Responsable_Comonuevos__c, Responsable_Dealer__c, EvaluacionDealer__r.Name    
			FROM Actividad_Plan_Integral__c 
			WHERE EvaluacionDealer__r.Name =: evalName
		];
	}

	public List<String> getCabeceras(){
		return new List<String>{
			'# Actividad Plan Integral',
			'Condición Observada',
			'Área',
			'Sugerencia/Actividad de Mejora',
			'Referencia',
			'Creado por: Nombre completo',
			'Propietario: Nombre completo',
			'Fecha Compromiso',
			'Fecha Realización',
			'Validado TMEX',
			'Acciones de Mejora Distribuidor',
			'Observaciones',
			'Pregunta',
			'Objeto de Evaluación',
			'Metodo de Evaluación',
			'Métodos de Evaluación',
			'Sección',
			'TSM: Respuesta Pregunta',
			'Responsable TMEX: Nombre completo',
			'Responsable Comonuevos',
			'Responsable Dealer'
		};
	}	
}