//Es una prueba roberto carlos avendaño quintana
public without sharing class AltaAutoDemoDistribuidorController {
    public ApexPages.StandardSetController setController{get;set;}
    public SolicitudAutoDemo__c help{get;set;}
    
    private User currentUser;
    private Contact contactDealer;
    public Filtros camposBusqueda{get;set;} 
    private Map<String,Map<String,RecordType>> recordTypesMap;
    
    public AltaAutoDemoDistribuidorController(ApexPages.StandardController controller) {
        help = new SolicitudAutoDemo__c();
        camposBusqueda = new Filtros();
        currentUser = [SELECT Id, ContactId FROM User WHERE Id = : UserInfo.getUserId()];		
        
        recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        
        if(currentUser.ContactId!=null){
            contactDealer = [SELECT Id, AccountId, Account.Codigo_Distribuidor__c FROM Contact WHERE Id =:currentUser.ContactId];
            
            /*
setController = new ApexPages.StandardSetController(Database.getQueryLocator([
SELECT Name, Dealer_Code__c, Toms_Series_Name__c, Exterior_Color_Description__c, Model_Year__c
FROM InventarioWholesale__c 
WHERE Dealer_Code__c=:contactDealer.Account.Codigo_Distribuidor__c
]));
setController.setPageSize(10);*/
            //} else{			
            //	setController = null;
        }
        
        setController = null;
    }
    
    
    public List<InventarioWholesale__c> getVines(){
        return (List<InventarioWholesale__c>) setController.getRecords();
    }
    
    public Map<Id, InventarioWholesale__c> getVinesMap(){
        return new Map<Id, InventarioWholesale__c>(this.getVines());
    }
    
    public PageReference anterior(){
        setController.previous();
        return null;
    }
    
    public PageReference siguiente(){
        setController.next();
        return null;
    }
    
    
    public PageReference guardar(){		
        help.Estatus__c = Constantes.AUTO_DEMO_SOLICITUD_ALTA;        
        try{					
            
            this.mapeaSerie(getVinesMap().get(camposBusqueda.selectedWholeSale));
            insert help;			
            
            this.actualizaSAD(getVinesMap().get(camposBusqueda.selectedWholeSale), help);
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Registro Creado'));
            help = new SolicitudAutoDemo__c();
            camposBusqueda = new Filtros();
            //setController = null;
            
            
        } catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage());			
        }
        
        return null;
    }
    
    public void mapeaSerie(InventarioWholesale__c wholeSale){
        List<Product2> infoProducto = new List<Product2>();
        Vehiculo__c vin = new Vehiculo__c ();
        if(wholeSale != null){
            String cadenaFactura = wholeSale.Distributor_Invoice_Date_MM_DD_YYYY__c;
            String cadenaCodigoAnioModelo = wholeSale.Model_Number__c+wholeSale.Model_Year__c;
            
            
            system.debug('cadena prodcuto'+cadenaCodigoAnioModelo);
            if(cadenaCodigoAnioModelo != null){
                infoProducto = [Select id,Serie__c From Product2 Where IdExternoProducto__c =: cadenaCodigoAnioModelo];
                
            }
            
            //Se utiliza la información de la consulta infoProducto2
            if(!infoProducto.isEmpty()){
                vin = new Vehiculo__c(
                    Name = wholeSale.Name,
                    AnioModel__c = wholeSale.Model_Year__c,
                    Id_Externo__c = wholeSale.Name,
                    Distribuidor__c = contactDealer != null ? contactDealer.AccountId : null,
                    RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id,
                    Producto__c = infoProducto[0].Id
                );
                upsert vin Id_Externo__c;	
                
            }
            
            if(infoProducto.isEmpty()){
                vin = new Vehiculo__c(
                    Name = wholeSale.Name,
                    AnioModel__c = wholeSale.Model_Year__c,
                    Id_Externo__c = wholeSale.Name,
                    Distribuidor__c = contactDealer != null ? contactDealer.AccountId : null,
                    RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id
                );
            }
            
            if(cadenaFactura!= null ){
                if(Pattern.matches('[0-9]{2}[-|/]{1}[0-9]{2}[-|/]{1}[0-9]{4}[\\s]{1}[0-9]{2}[:]{1}[0-9]{2}', cadenaFactura)){
                    cadenaFactura = cadenaFactura.split(' ').get(0);
                    List<String> splits = cadenaFactura.split('/');
                    Date fechaFactura = Date.newInstance(Integer.valueOf(splits[2]), Integer.valueOf(splits[1]), Integer.valueOf(splits[0]));	
                    vin.FechaFactura__c = fechaFactura;
                }
            }
            
            Product2 producto = new Product2( 
                Name = wholeSale.Model_Number__c,
                Anio__c	 = wholeSale.Model_Year__c,
                //IdUnidad__c = wholeSale.Model_Number__c,
                IdExternoProducto__c = wholeSale.Model_Number__c+wholeSale.Model_Year__c,	
                IsActive = true,
                RecordTypeId = recordTypesMap.get('Product2').get('Unidad').Id
            );
            
            Serie__c serie = new Serie__c(
                Id_Externo_Serie__c= wholeSale.Toms_Series_Name__c,
                Name = wholeSale.Toms_Series_Name__c
            );
            
            
            try{
                upsert serie Id_Externo_Serie__c;
                producto.Serie__c = serie.Id;				
                
            } catch(Exception ex){
                System.debug('Error upsert: '+ ex.getMessage());
            }			
            
            
            try{
                upsert producto IdExternoProducto__c;
                vin.Producto__c = producto.Id;
                
            } catch(Exception ex){
                System.debug('Error upsert Producto: '+ ex.getMessage());
            }
            
            if(infoProducto.isEmpty()){
                try{
                    upsert vin Id_Externo__c;	
                } catch(Exception ex){
                    System.debug('Error upsert VIN: '+ ex.getMessage());
                }
            }
        }
    }
    
    public void actualizaSAD(InventarioWholesale__c wholeSale, SolicitudAutoDemo__c sad){
        /*Vehiculo__c vin = [SELECT Producto__c FROM Vehiculo__c WHERE Id_Externo__c=:wholeSale.Name];
String cadenaFactura = wholeSale.Distributor_Invoice_Date_MM_DD_YYYY__c;

if(cadenaFactura!= null ){
if(Pattern.matches('[0-9]{2}[-|/]{1}[0-9]{2}[-|/]{1}[0-9]{4}[\\s]{1}[0-9]{2}[:]{1}[0-9]{2}', cadenaFactura)){
cadenaFactura = cadenaFactura.split(' ').get(0);
List<String> splits = cadenaFactura.split('/');
Date fechaFactura = Date.newInstance(Integer.valueOf(splits[2]), Integer.valueOf(splits[1]), Integer.valueOf(splits[0]));	
vin.FechaFactura__c = fechaFactura;
// sad.FacturaAutoDemo__c =  fechaFactura;
}
}*/
        
        sad.AnioModeloAutoDemo__c = wholeSale.Model_Year__c;
        sad.CodigoColorInterior__c = wholeSale.Interior_Color_Code__c;
        sad.CodigoColorExterior__c = wholeSale.Exterior_Color_Code__c;
        sad.DescripcionColorInterior__c = wholeSale.Interior_Color_Description__c;
        sad.DescripcionColorExterior__c = wholeSale.Exterior_Color_Description__c;
        
        /*
ColorInterno__c colorInt = new ColorInterno__c(
CodigoColor__c = wholeSale.Interior_Color_Code__c,
Name = wholeSale.Interior_Color_Description__c
);

ColorExterno__c colorExt = new ColorExterno__c(
CodigoColor__c = wholeSale.Exterior_Color_Code__c,
Name = wholeSale.Exterior_Color_Description__c
);*/
        
        
        /*
try{
upsert colorInt CodigoColor__c;
sad.ColorInteriorAutoDemo__c = sad != null? colorInt.Id: null;

} catch(Exception ex){
System.debug('Error upsert Color Interno: '+ ex.getMessage());
}

try{
upsert colorExt CodigoColor__c;
sad.ColorExteriorAutoDemo__c = sad != null? colorExt.Id: null;

} catch(Exception ex){
System.debug('Error upsert Color Externo: '+ ex.getMessage());
}*/
        
        
        
        /*
Product2 producto = new Product2(
Name = wholeSale.Model_Number__c,
IdUnidad__c = wholeSale.Model_Number__c,
IdExternoProducto__c = wholeSale.Model_Number__c,
IsActive = true,
RecordTypeId = recordTypesMap.get('Product2').get('Unidad').Id
);

Serie__c serie = new Serie__c(
Id_Externo_Serie__c= wholeSale.Toms_Series_Name__c,
Name = wholeSale.Toms_Series_Name__c
);

try{
upsert serie Id_Externo_Serie__c;
producto.Serie__c = serie.Id;				
} catch(Exception ex){
System.debug('Error upsert Serie: '+ ex.getMessage());
}			

try{
upsert producto IdExternoProducto__c;
vin.Producto__c = producto.Id;
} catch(Exception ex){
System.debug('Error upsert Producto: '+ ex.getMessage());
}

try{
update vin;				
} catch(Exception ex){
System.debug('Error update VIN: '+ ex.getMessage());
}

*/
        
        try{
            update sad;				
        } catch(Exception ex){
            System.debug('Error update Auto demo: '+ ex.getMessage());
        }
        
    }
    
    public PageReference buscarFiltros(){
        if(contactDealer!=null){
            setController = camposBusqueda.getQueryFilter(contactDealer.Account.Codigo_Distribuidor__c);
            setController.setPageSize(5);	
        }
        
        return null;
    }
    
    public class Filtros{
        public String vin{get;set;}
        public String modelo{get;set;}
        public String colorExt{get;set;}
        public String anio{get;set;}
        public String selectedWholeSale{get;set;}
        
        public Filtros(){
            this.vin = '';
            this.modelo = '';
            this.colorExt = '';
            this.anio = '';
            this.selectedWholeSale = '';
        }
        
        private ApexPages.StandardSetController getQueryFilter(String dealerCode){
            String QUERY = 'SELECT Id, Name, Dealer_Code__c, Toms_Series_Name__c, Exterior_Color_Description__c, Interior_Color_Description__c, Model_Year__c, FechaFactura__c, Interior_Color_Code__c, Exterior_Color_Code__c, Model_Number__c, Distributor_Invoice_Date_MM_DD_YYYY__c FROM InventarioWholesale__c WHERE Dealer_Code__c =: dealerCode';			
            String VIN = vin!=''? ' AND Name LIKE \'%'+vin+'%\'': '';
            String MODEL = modelo !='' ? ' AND Toms_Series_Name__c LIKE \'%'+modelo+'%\'': '';
            String COLOR = colorExt != '' ? ' AND Exterior_Color_Description__c LIKE \'%'+colorExt+'%\'': '';
            String ANIO = anio != '' ? ' AND Model_Year__c LIKE \'%'+anio+'%\'': '';			
            String CONDITIONALS = VIN+ MODEL+ COLOR+ ANIO;
            QUERY += CONDITIONALS;
            return new ApexPages.StandardSetController(Database.getQueryLocator(QUERY));
        }
    } 
    
}