/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_MergeContactosBch_cls y
    					actualizar el cliente en AccountContactRelation

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Agosto-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_MergeContactosBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global string query;
    global String sClienteFinal;
    
    //Un constructor por default
    global TAM_MergeContactosBch_cls(string query, String sClienteFinal){
        this.query = query;
        this.sClienteFinal = sClienteFinal;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_MergeContactosBch_cls.start query: ' + this.query + ' sClienteFinal: ' + this.sClienteFinal );
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<AccountContactRelation> scope){
        System.debug('EN TAM_MergeContactosBch_cls.');
		List<AccountContactRelation> lOppUps = new List<AccountContactRelation>();

		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (AccountContactRelation objOpp : scope){
        	String sIdExterno = objOpp.TAM_IdExterno__c; 
        	System.debug('EN TAM_MergeContactosBch_cls sIdExterno: ' + sIdExterno);        	
        	//Remplaza el id de la cuenta en sIdExterno por sClienteFinal
        	String sIdExternoFinal = sIdExterno != null ? sIdExterno.replace(objOpp.AccountId, this.sClienteFinal) : '';
        	System.debug('EN TAM_MergeContactosBch_cls sIdExternoFinal: ' + sIdExternoFinal);        	
        	//Crea el objeto del tipo AccountContactRelation
        	AccountContactRelation OppPaso = new AccountContactRelation(id = objOpp.id, AccountId = this.sClienteFinal,
        		TAM_IdExterno__c = sIdExternoFinal, TAM_MergeCuentaAnterior__c = objOpp.AccountId); 
        	lOppUps.add(OppPaso);
        	System.debug('EN TAM_MergeContactosBch_cls OppPaso: ' + OppPaso);
        }

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lOppUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lOppUps, AccountContactRelation.id, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeContactosBch_cls Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_MergeContactosBch_cls Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

		//Ve si la etiqueta de TAM_HabilitaMergeAccounts
		String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
		Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
   		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
   		if (!bHabilitaMergeAccounts){
    		System.debug('EN TAM_MergeContactosBch_cls bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);	
       		Database.rollback(sp);
   		}
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_MergeContactosBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}