@isTest
private class CCHeaderControllerTest{	
    
    @testSetup
    public static void insertUser(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Administrador del sistema'];    
        User u = new User(
            Alias = 'ConsTSM', 
            Email='ConsultorTSM@testorgtoyota.com',
            LastName='TSM',
            ProfileId = p.Id,
            UserName='standarduser@testorgtoyota.com',
            EmailEncodingKey='UTF-8',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            NombreMenuComunidad__c = 'AdministradorPrincipal',
            IsActive = true
        );        
    
        Database.SaveResult status= Database.insert(u, false);
        if(!status.isSuccess()){
            for(Database.Error error: status.getErrors()){
                System.debug('Error al insertar: '+error.getFields());
            }
        }

        MenuComunidad__c mc1 = new MenuComunidad__c(
            Name = 'AdministradorPrincipal',
            Orden__c = 0,
            TipoElemento__c = 'Submenu'
        );
        insert mc1;

        MenuComunidad__c mc2 = new MenuComunidad__c(
            Name = 'SubMenuMenu',
            Parent__c = mc1.Id,
            Orden__c = 0,
            TipoElemento__c = 'Submenu'
        );
        insert mc2;

        MenuComunidad__c mc3 = new MenuComunidad__c(
            Name = 'SubSubMenuMenu',
            Parent__c = mc2.Id,
            Orden__c = 0,
            TipoElemento__c = 'Submenu'
        );
        insert mc3;

	}

    @isTest
    private static void executeTest(){
    	User myTestUser= [SELECT Id, UserName, NombreMenuComunidad__c FROM User where NombreMenuComunidad__c = 'AdministradorPrincipal' limit 1];
    	System.debug(myTestUser);
    	Test.startTest();
            System.runAs(myTestUser){
        		CCHeaderController obj = new CCHeaderController();
        		CCHeaderController.saveApp('Kodawari');
        		obj = new CCHeaderController();
            }
    	Test.stopTest();
    }
}