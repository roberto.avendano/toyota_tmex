/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de los incentivos para 
    					Venta Corporativa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    19-JUNIO-2020    	Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_ProvisionIncentivosVentaCorporCtrl {
    
    //Un constructor por default
    public TAM_ProvisionIncentivosVentaCorporCtrl(){}

    /** SIN REVERSA */
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getRetailFlotilla(String recordId){
    	System.debug('ENTRO A getRetailFlotilla...');
    	
        Id RecordTypeId = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
	    String RecordTypeId_Flotilla = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = new List<TAM_DetalleProvisionIncentivo__c>();
        
        System.debug('ENTRO A getRetailFlotilla recordId: ' + recordId + ' RecordTypeId_Flotilla: ' + RecordTypeId_Flotilla);
        
        for (TAM_DetalleProvisionIncentivo__c objDetaProvIncen : [SELECT  TAM_VIN__r.Name,
                                                                      		  TAM_VIN__r.Id,
                                                                              TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                              TAM_NombreDealer__c,
                                                                              TAM_CodigoDealer__c,
                                                                              TAM_AnioModelo__c,
                                                                              TAM_Modelo__c,
                                                                              TAM_Serie__c,
                                                                              TAM_Version__c,
                                                                              TAM_FechaEnvio__c,
                                                                              TAM_FechaVenta__c,
                                                                              TAM_TipoProgramaRango__c,
                                                                              TAM_TipoMovimiento__c,
                                                                              TAM_FirstName__c,
                                                                              TAM_LastName__c,
                                                                              TAM_DescuentoAutorizadoDTM__c,
                                                                              TAM_DescuentoAutorizadoDtmTxt__c,
                                                                              TAM_IncPropuestoCash__c,
                                                                              TAM_CodigoVenta__c,
                                                                              TAM_NombreClienteCRM__c,
                                                                              TAM_NombreClienteCorporativoCRM__c,
                                                                              TAM_NombreClienteFactura__c,
                                                                              TAM_CodigoContable__c,
                                                                              TAM_Factura__c,
                                                                              TAM_PrecioVehiculo__c,
                                                                              TAM_IncentivoProvisionado__c,
                                                                              TAM_IncentivoTMEX_Efectivo__c,
                                                                              TAM_PorcentParticipacionTMEX__c,
                                                                              TAM_ParticipacionTmexTxt__c,
                                                                              TAM_IncTMEXCash__c,
                                                                              TAM_Cerrada__c,
                                                                              TAM_Fleet__c,
                                                                              TAM_ProvisionIncentivos__c,
                                                                              TAM_Clasificacion__c,
                                                                              Name,
                                                                              RecordTypeId,
                                                                              TAM_Tipo__c,
                                                                              TAM_IdExterno__c,
                                                                              TAM_ProvicionarEfectivo__c,
                                                                              TAM_TransmitirDealer__c,
                                                                              TAM_PoliticaIncentivos__r.Id,
                                                                  			  TAM_PoliticaIncentivos__r.Name,
                                                                  			  TAM_PoliticaIncentivos__r.TAM_FinVigencia__c,
                                                                              TAM_ProvisionIncentivos__r.TAM_MesDeProvision__c, 
                                                                              TAM_ProvisionIncentivos__r.TAM_AnioDeProvision__c,
                                                                              TAM_FinVigPoliticaRetail_AUX__c
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Sin Reversa'
                                                                      AND     RecordTypeId =: RecordTypeId_Flotilla
                                                                      ORDER BY TAM_TipoProgramaRango__c ASC, TAM_Modelo__c ASC,  TAM_FechaEnvio__c ASC]){                                                        	
        	//Agregalo a la lista de lstDetalleProvision
        	lstDetalleProvision.add(objDetaProvIncen);        	
        }
                
        //METE EL REGISTRO A LA LISTA DE lstDetalleProvision
        return lstDetalleProvision;
    }

    /*GUARDADO DE REGISTROS*/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> saveProvision(String recordId, List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision){
    	
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
                
        try {
            List<Database.UpsertResult> LdtUpsrv = Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c, false);
			for (Database.UpsertResult objDtSvr : LdtUpsrv){
				if(!objDtSvr.isSuccess())
					System.debug('EN saveProvision Error a la hora de crear los reg en TAM_DetalleProvisionIncentivo: ' + objDtSvr.getErrors()[0].getMessage());
			}//Fin del for para lDtSvr            
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        //Regresa la lista de lstDetalleUpsert
        return lstDetalleUpsert;
    } 

    /*CERRAR PROVISION*/
    @AuraEnabled
    public static Boolean saveCerrarProvision(String recordId, List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision){
        TAM_CerrarProvisionQueueable updateJob = new TAM_CerrarProvisionQueueable(lstDetalleProvision, recordId, 'VentaCorporativa');
        ID jobID = System.enqueueJob(updateJob);
        System.debug('ID CORP JOB:*** ' + jobID);
        if(jobID != null){
            return true;
        } else{
            return false;
        }
        
    } 
        
}