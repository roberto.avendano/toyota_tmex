public with sharing class BajaAutoDemoEditController {
	private ApexPages.StandardController myController;
	private SolicitudAutoDemo__c help;
	public boolean showName{get;set;}
	public boolean showRazonSocial{get;set;}
	public boolean showAllFields{get;set;}
	public String picklistValue{get;set;}

	public BajaAutoDemoEditController(ApexPages.StandardController controller) {
		this.myController = controller;
		picklistValue ='';
		showRazonSocial = false;
		showAllFields = false;
	}

	public PageReference mostrarCampo(){
		showAllFields = true;
		showRazonSocial = picklistValue=='Cliente persona moral'? true: false;
		showName = pickListValue=='Cliente persona moral'?false:true;
		return null;
	}

	public PageReference guardar(){
		PageReference ret;
			try{
				help = (SolicitudAutoDemo__c) myController.getRecord();
				help.Estatus__c = Constantes.AUTO_DEMO_SOLICITUD_BAJA;	
                help.SolicitudAutoDemo1__c = true;//HELP!!! ACTUALIZAR EL VALOR CORRECTO DEL PICKLIST ESTATUS__C
               // System.debug('Valor de Solicitud Auto demo:' +  help.SolicitudAutoDemo__c);	
				update help;
				ret = new PageReference('/apex/BajaAutoDemoDistribuidor');
				ret.setRedirect(true);

			} catch(Exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		return ret;
	}
}