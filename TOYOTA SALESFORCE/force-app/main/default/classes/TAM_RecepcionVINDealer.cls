/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_SolicitudDealerDestino__c y cancelar las soicitudes 
                        que el usuario final no ha enviado a autorizar.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Septiembre-2021   Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_RecepcionVINDealer {
    
    
    @AuraEnabled
    public static Boolean getTypeOfUser (String recordIdSolicitud,String userId){
        System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser recordIdSolicitud '+ recordIdSolicitud + ' userId: ' + userId);
        
        // true = usuario arpobador. false = usuario del mismo dealer o de DTM.
        Boolean tipoUsuario = false;

        //Se obtiene el nombre de la solicitud flotilla programa
        String nameSolicitudFlotilla = [Select name From TAM_SolicitudDealerDestino__c WHERE Id =: recordIdSolicitud].name;
        System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser nameSolicitudFlotilla '+ nameSolicitudFlotilla);

        //Validamos que el usuario en sesión no sea (DTM)
        List<GroupMember> usariosDTM =  new List<GroupMember>();
        usariosDTM   = [SELECT UserOrGroupId,Group.Name FROM GroupMember WHERE (Group.Name = 'Aprobador DTM (nivel 1)' OR Group.Name = 'Aprobador DTM (Nivel 2)' )
                        AND UserOrGroupId =: userId];
        
        //Tiene algo usariosDTM
        if(!usariosDTM.isEmpty()){
            tipoUsuario = false;
        }
        
        //No tiene nada usariosDTM
        if(usariosDTM.isEmpty()){
            User[] usrInfo = [Select id,Distribuidor__c,CodigoDistribuidor__c From User WHERE Id =: userId];

            //TAM_SolicitudesFlotillaPrograma__c[] solicitudInfo = [Select id,TAM_NombreCompletoDistribuidor__c,TAM_NomDistribuidorPropietario__c From TAM_SolicitudesFlotillaPrograma__c WHERE name =: nameSolicitudFlotilla];
            TAM_DistribuidoresFlotillaPrograma__c[] solicitudInfo = [Select id, TAM_DistribuidorOrigen__c, TAM_NombreDistribuidor__c, TAM_IdDistribuidor__c From TAM_DistribuidoresFlotillaPrograma__c  WHERE TAM_FolioSolicitud__c =: nameSolicitudFlotilla];
            System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser solicitudInfo '+ solicitudInfo);
            //Si usrInfo.size() > 0 && solicitudInfo.size() > 0
            if(usrInfo.size() > 0 && solicitudInfo.size() > 0){
                System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser VAL 1 Distribuidor__c: ' + usrInfo[0].Distribuidor__c + ' TAM_NombreDistribuidor__c: ' + solicitudInfo[0].TAM_NombreDistribuidor__c);
                String sNombreDistPaso = solicitudInfo[0].TAM_DistribuidorOrigen__c;
                String sNombreDist = sNombreDistPaso.substring(sNombreDistPaso.indexOf('-'), sNombreDistPaso.length());
                System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser VAL 1 sNombreDist: ' + sNombreDist);
                if(usrInfo[0].Distribuidor__c == sNombreDist){
                    tipoUsuario = false;
                    System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser VAL 2...');
                }//Fin si usrInfo[0].Distribuidor__c == sNombreDist
                if(usrInfo[0].Distribuidor__c != sNombreDist){
                    tipoUsuario = true;
                    System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser VAL 3...');
                }//Fin si usrInfo[0].Distribuidor__c != sNombreDist
            }//Fin si usrInfo.size() > 0 && solicitudInfo.size() > 0
        }//Fin si usariosDTM.isEmpty()

        System.debug('EN TAM_RecepcionVINDealer.getTypeOfUser tipoUsuario: ' + tipoUsuario);        
        //Se retorna el tipo de usuario
        return tipoUsuario;
    }
    
    @AuraEnabled
    public static list<TAM_DistribuidoresFlotillaPrograma__c> getLineasDealerFlotillaPrograma (String recordIdSolicitud){
        System.debug('EN TAM_RecepcionVINDealer.getLineasDealerFlotillaPrograma recordIdSolicitud '+ recordIdSolicitud);
        
        List<TAM_DistribuidoresFlotillaPrograma__c> registrosFlotillaProgramaDealer = new List<TAM_DistribuidoresFlotillaPrograma__c>();

        //Se obtiene el nombre de la solicitud flotilla programa
        String nameSolicitudFlotilla = [Select name From TAM_SolicitudDealerDestino__c WHERE Id =: recordIdSolicitud].name;
        System.debug('EN TAM_RecepcionVINDealer.getLineasDealerFlotillaPrograma nameSolicitudFlotilla '+ nameSolicitudFlotilla);
        
        registrosFlotillaProgramaDealer = [Select id,TAM_RegistroRevisadoDealer__c,TAM_ComentarioDealerRecepcion__c,TAM_Cuenta__r.Name,TAM_DetalleSolicitudCompra_FLOTILLA__r.TAM_CatalogoCentralizadoModelos__r.Serie__c,
                                           TAM_DetalleSolicitudCompra_FLOTILLA__r.TAM_CatalogoCentralizadoModelos__r.Modelo__c,
                                           TAM_DetalleSolicitudCompra_FLOTILLA__r.TAM_CatalogoCentralizadoModelos__r.AnioModelo__c,
                                           TAM_DetalleSolicitudCompra_FLOTILLA__r.TAM_CatalogoCentralizadoModelos__r.DescripcionColorExterior__c,
                                           TAM_DetalleSolicitudCompra_FLOTILLA__r.TAM_CatalogoCentralizadoModelos__r.DescripcionColorInterior__c,
                                           name,TAM_Cantidad__c,TAM_Cuenta__c,TAM_DetalleSolicitudCompra_FLOTILLA__c,
                                           TAM_SolicitudFlotillaPrograma__r.TAM_NombreCompletoDistribuidor__c, TAM_DistribuidorOrigen__c,
                                           TAM_Estatus__c,TAM_IdDistribuidor__c,TAM_IdExterno__c,TAM_NombreDistribuidor__c,TAM_SolicitudFlotillaPrograma__c
                                           FROM TAM_DistribuidoresFlotillaPrograma__c WHERE TAM_SolicitudFlotillaPrograma__r.name =: nameSolicitudFlotilla];
        System.debug('EN TAM_RecepcionVINDealer.getLineasDealerFlotillaPrograma registrosFlotillaProgramaDealer '+ registrosFlotillaProgramaDealer);
        
        if(!registrosFlotillaProgramaDealer.isEmpty()){
            return registrosFlotillaProgramaDealer;
            
        }else{
            return null;
            
        }
        
    }
    
    
    @AuraEnabled
    public static void guardarAprobacion (List<TAM_DistribuidoresFlotillaPrograma__c> listaIn,String recordIdSolicitud){
        List<TAM_DistribuidoresFlotillaPrograma__c> listaActualizada = new List<TAM_DistribuidoresFlotillaPrograma__c>(listaIn);
        List<TAM_DODSolicitudesPedidoEspecial__c> registrosDOD = new List<TAM_DODSolicitudesPedidoEspecial__c>();
        List<TAM_DODSolicitudesPedidoEspecial__c> registrosDODUpd = new List<TAM_DODSolicitudesPedidoEspecial__c>();
        List<TAM_CheckOutDetalleSolicitudCompra__c > registrosCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c >();
        Map<String,TAM_DistribuidoresFlotillaPrograma__c> mapEntregaVIN = new   Map<String,TAM_DistribuidoresFlotillaPrograma__c>();
        Map<String,TAM_DistribuidoresFlotillaPrograma__c> mapProcess = new Map<String,TAM_DistribuidoresFlotillaPrograma__c>();
        Map<String, String> mapSolDealDest = new Map<String, String>();
        Map<String,TAM_SolicitudDealerDestino__c> mapSolDealDedtUps = new Map<String,TAM_SolicitudDealerDestino__c>();

        //Se obtiene el nombre de la solicitud flotilla programa
        String nameSolicitudFlotilla = [Select name From TAM_SolicitudDealerDestino__c WHERE Id =: recordIdSolicitud].name;
        
        //Consulta los datos de los modelos que se estan modificando
        for (TAM_SolicitudDealerDestino__c objPaso : [Select t.id, t.TAM_IdExterno__c, t.TAM_DealerOrigen__c, t.TAM_DealerDestino__c 
            From TAM_SolicitudDealerDestino__c t Where id =: recordIdSolicitud]){
            //Metelo al mapa de mapSolDealDest 
            mapSolDealDest.put(objPaso.TAM_DealerDestino__c, objPaso.id);
        }
        System.debug('En TAM_RecepcionVINDealer mapSolDealDest: ' + mapSolDealDest.keyset());
        System.debug('En TAM_RecepcionVINDealer mapSolDealDest: ' + mapSolDealDest.values());
        
        if(!listaActualizada.isEmpty()){
            system.debug('lista a actualiozar'+listaActualizada);
            for(TAM_DistribuidoresFlotillaPrograma__c regFlot : listaActualizada){
                if(regFlot.TAM_Estatus__c == 'Pendiente'){
                    regFlot.TAM_RegistroRevisadoDealer__c = false;
                }
                if(regFlot.TAM_Estatus__c != 'Pendiente'){
                    regFlot.TAM_RegistroRevisadoDealer__C = true;
                    mapProcess.put(regFlot.TAM_IdExterno__c, regFlot);
                }//Fin si regFlot.TAM_Estatus__c != 'Pendiente'
            }
        }//Fin si !listaActualizada.isEmpty()
        System.debug('En TAM_RecepcionVINDealer mapProcess: ' + mapProcess.keyset());
        System.debug('En TAM_RecepcionVINDealer mapProcess: ' + mapProcess.values());
                
        //Ve si tiene algo listaActualizada
        if(!listaActualizada.isEmpty()){
            for(TAM_DistribuidoresFlotillaPrograma__c regEntrega : listaActualizada){
                mapEntregaVIN.put(regEntrega.name,regEntrega);
            }
        }
        System.debug('En TAM_RecepcionVINDealer mapEntregaVIN: ' + mapEntregaVIN.keyset());
        System.debug('En TAM_RecepcionVINDealer mapEntregaVIN: ' + mapEntregaVIN.values());
        
        //Consulta el catalogo de productos
        Map<String, String> mapIdExtProdSerie = new Map<String, String>();
        //Consulta los productos
        for (Product2 objProduct2 : [SELECT NombreVersion__c, IdExternoProducto__c, Serie__r.Id_Externo_Serie__c
            FROM Product2 WHERE IsActive = true AND IdExternoProducto__c != null
            AND NombreVersion__c != null  Order by IdExternoProducto__c]){
            //Agregalo al mapa de 
            mapIdExtProdSerie.put(objProduct2.IdExternoProducto__c, objProduct2.Serie__r.Id_Externo_Serie__c);
        }           
        System.debug('EN TAM_RecepcionVINDealer guardarAprobacion mapIdExtProdSerie0: ' + mapIdExtProdSerie.keySet());
        System.debug('EN TAM_RecepcionVINDealer guardarAprobacion mapIdExtProdSerie0: ' + mapIdExtProdSerie.Values());      
                
        //Se actualizan las lineas de DOD con el comentario del dealaer aprobador
        registrosDOD = [Select id, TAM_ComentarioDealerRecepcion__c, TAM_Entrega__c, TAM_Codigo__c, TAM_Anio__c, TAM_Serie__c, TAM_Modelo__c From TAM_DODSolicitudesPedidoEspecial__c WHERE TAM_SolicitudFlotillaPrograma__r.name =: nameSolicitudFlotilla];
        System.debug('En TAM_RecepcionVINDealer registrosDOD: ' + registrosDOD);

        registrosCheckOut = [Select id, TAM_DistribuidorEntrega__c, TAM_Modelo__c, TAM_Anio_Modelo__c From TAM_CheckOutDetalleSolicitudCompra__c WHERE TAM_SolicitudFlotillaPrograma__r.name =: nameSolicitudFlotilla];
        System.debug('En TAM_RecepcionVINDealer registrosCheckOut: ' + registrosCheckOut);
        
        //Para hacer el rollback
        Savepoint sp = Database.setSavepoint();        

        if(!listaActualizada.isEmpty())
            //Actualiza los reg
            update listaActualizada;

        //Recorre la lista de mapProcess
        for (TAM_DistribuidoresFlotillaPrograma__c objPaso : mapProcess.values()){
            //Crea la clave del dealer
            String sCvaDealer = objPaso.TAM_IdDistribuidor__c +'-'+ objPaso.TAM_NombreDistribuidor__c;
            System.debug('En TAM_RecepcionVINDealer sCvaDealer: ' + sCvaDealer);
            //Busca sCvaDealer en mapSolDealDest
            if (mapSolDealDest.containsKey(sCvaDealer)){
                //Metelo al mapa de mapSolDealDedtUps
                mapSolDealDedtUps.put(mapSolDealDest.get(sCvaDealer), new TAM_SolicitudDealerDestino__c(
                        id = mapSolDealDest.get(sCvaDealer),
                        TAM_Estatus__c = 'Procesada'
                    )
                );
            }//Fin si mapSolDealDest.containsKey(sCvaDealer)
        }//Fin del for para mapProcess.values()
        System.debug('En TAM_RecepcionVINDealer mapSolDealDedtUps: ' + mapSolDealDedtUps.keyset());
        System.debug('En TAM_RecepcionVINDealer mapSolDealDedtUps: ' + mapSolDealDedtUps.values());
        
        //Ve si tiene algo mapSolDealDedtUps
        if (!mapSolDealDedtUps.isEmpty())
            upsert mapSolDealDedtUps.values() TAM_SolicitudDealerDestino__c.id;

        //ve si la lista registrosDOD tiene algo 
        if(!registrosDOD.isEmpty()){
            for(TAM_DODSolicitudesPedidoEspecial__c regDOD : registrosDOD){
                String sModelo = mapIdExtProdSerie.get(regDOD.TAM_Codigo__c + '' + regDOD.TAM_Anio__c);
                String sIdExt = regDOD.TAM_Entrega__c + '-' + regDOD.TAM_Anio__c + '-' + sModelo; //regDOD.TAM_Modelo__c;
                System.debug('En TAM_RecepcionVINDealer sIdExt: ' + sIdExt);
                //Metelo al mapa de mapEntregaVIN
                if (Test.isRunningTest()){
                    mapEntregaVIN.put(sIdExt, new TAM_DistribuidoresFlotillaPrograma__c(
                            TAM_Estatus__c = 'Rechazar Recepción'
                        )
                    );                    
                } //Fin si Test.isRunningTest()
                //Ve si existe mapEntregaVIN.containsKey(sIdExt)
                if(mapEntregaVIN.containsKey(sIdExt)){
                    TAM_DistribuidoresFlotillaPrograma__c dealerEntrega = mapEntregaVIN.get(sIdExt); //regDOD.TAM_Entrega__c+'-'+regDOD.TAM_Anio__c+'-'+regDOD.TAM_Modelo__c
                    System.debug('En TAM_RecepcionVINDealer dealerEntrega: ' + dealerEntrega);
                    TAM_DODSolicitudesPedidoEspecial__c regNuevoDOD = new TAM_DODSolicitudesPedidoEspecial__c();
                    if(dealerEntrega.TAM_Estatus__c == 'Aceptar Recepción' || Test.isRunningTest())
                        regNuevoDOD.TAM_Estatus__c = 'Autorizado Dealer';
                    if(dealerEntrega.TAM_Estatus__c == 'Rechazar Recepción' || Test.isRunningTest()){
                        regNuevoDOD.TAM_Estatus__c = 'Rechazado Dealer';
                        regNuevoDOD.TAM_FechaCancelacion__c = Date.today();
                    }
                    if(dealerEntrega.TAM_Estatus__c == 'Pendiente' || Test.isRunningTest())
                    	regNuevoDOD.TAM_RegistroRevisadoDealer__c = false;
                    if(dealerEntrega.TAM_Estatus__c != 'Pendiente' || Test.isRunningTest())
                    	regNuevoDOD.TAM_RegistroRevisadoDealer__c = true;
                    //EL RESTO DE LOS CAMPOS
                    regNuevoDOD.id = regDOD.Id;
                    regNuevoDOD.TAM_ComentarioDealerRecepcion__c = dealerEntrega.TAM_ComentarioDealerRecepcion__c;
                    System.debug('En TAM_RecepcionVINDealer regNuevoDOD: ' + regNuevoDOD);
                    registrosDODUpd.add(regNuevoDOD);
                }//Fin si mapEntregaVIN.containsKey(regDOD.TAM_Entrega__c+'-'+regDOD.TAM_Anio__c+'-'+regDOD.TAM_Modelo__c)
            }//Fin del for para 
        }
        
        //Ve si tiene algo registrosDODUpd        
        if(!registrosDODUpd.isEmpty()){
            try{
                update registrosDODUpd;    
            }catch(Exception e){
                system.debug('Error al actualizar la lista registrosDODUpd'+e.getMessage());    
            }
        }//Fin si !registrosDODUpd.isEmpty()
        
        //Crea la colaboración
        TAM_ColaboracionNivel1DOD.shareRecordsDODNivel1(mapEntregaVIN, registrosDOD, mapIdExtProdSerie);
        
        //PARA PRUEBAS DESPUES LO QUITAS
        //Database.rollback(sp);        
        
    }
    
    
    @AuraEnabled 
    public static List<String> valoresAprobacionDealer(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = TAM_DistribuidoresFlotillaPrograma__c.TAM_Estatus__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.getValue() == 'Aceptar Recepción' || pickListVal.getValue() == 'Rechazar Recepción' || pickListVal.getValue() == 'Pendiente'){
                pickListValuesList.add(pickListVal.getLabel());
            }
        }   
        
        return pickListValuesList;
        
    }    
    
}