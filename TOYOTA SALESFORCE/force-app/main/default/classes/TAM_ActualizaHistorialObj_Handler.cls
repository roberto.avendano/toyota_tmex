public with sharing class TAM_ActualizaHistorialObj_Handler extends TriggerHandler{

    private List<TAM_ActualizaHistorialObj__c> CteServList;
    private Map<Id,TAM_ActualizaHistorialObj__c> mapCtesServ;
    
    public TAM_ActualizaHistorialObj_Handler() {
        this.CteServList = Trigger.new;
        this.mapCtesServ = (Map<Id,TAM_ActualizaHistorialObj__c>)Trigger.newMap;
    }

    public override void afterInsert(){
        actalizaRegistrosObjFinal(CteServList);
    }

    public override void afterUpdate(){
        actalizaRegistrosObjFinal(CteServList);
    }

    public void actalizaRegistrosObjFinal(List<TAM_ActualizaHistorialObj__c> lActHistoObjFinal){
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal...');

        Map<String, TAM_ActualizaHistorialObjFinal__c> mapIdExtObjFinal = new Map<String, TAM_ActualizaHistorialObjFinal__c>();
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapIdDodObjDodUpd = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();
        Set<String> setIdDod = new Set<String>();
        Map<String, Boolean> mapIdDodActCampos = new Map<String, Boolean>();
                
        //Recorre la lista de reg del tipo lActHistoObjFinal
        for (TAM_ActualizaHistorialObj__c objPaso : lActHistoObjFinal){
            setIdDod.add(objPaso.TAM_PARENTID__c);
        }
        System.debug('EN TAM_ActualizaHistorialObj_Handler.setIdDod: ' + setIdDod);
        
        //Consulta los reg asociados a TAM_DODSolicitudesPedidoEspecial__c de setIdDod
        for (TAM_DODSolicitudesPedidoEspecial__c objDodSel : [Select id, TAM_ActRegCheckOut__c 
            From TAM_DODSolicitudesPedidoEspecial__c Where ID IN :setIdDod]){
            Boolean blnActRegCheckOut = objDodSel.TAM_ActRegCheckOut__c ? false : true;
            //Metelo al mapa de mapIdDodActCampos
            mapIdDodActCampos.put(objDodSel.id, blnActRegCheckOut);
        }
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal mapIdDodActCampos: ' + mapIdDodActCampos);
        
        //Recorre la lista de reg del tipo lActHistoObjFinal
        for (TAM_ActualizaHistorialObj__c objPaso : lActHistoObjFinal){
            
            String sIdExtPaso = objPaso.TAM_PARENTID__c + '-' + Date.today();
            String sNEWVALUE;
            
            /*if (objPaso.TAM_NEWVALUE__c == 'Asignado' && objPaso.TAM_OLDVALUE__c == 'Cancelado')
               sNEWVALUE =  'Cancelado';
            else if (objPaso.TAM_NEWVALUE__c == 'Asignado' && objPaso.TAM_OLDVALUE__c == 'Pendiente')
               sNEWVALUE =  'Asignado';
            else if (objPaso.TAM_NEWVALUE__c == 'Pendiente' && objPaso.TAM_OLDVALUE__c == 'Asignado')
               sNEWVALUE =  'Asignado';
            else if (objPaso.TAM_NEWVALUE__c == 'Pendiente' && objPaso.TAM_OLDVALUE__c == 'Cancelado')
               sNEWVALUE =  'Cancelado';*/

            if (objPaso.TAM_NEWVALUE__c == 'Asignado' && objPaso.TAM_OLDVALUE__c == 'Cancelado')
               sNEWVALUE =  'Cancelado';
            //else if (objPaso.TAM_NEWVALUE__c == 'Pendiente' && objPaso.TAM_OLDVALUE__c == 'Cancelado')
            //   sNEWVALUE =  'Cancelado';

            System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal sNEWVALUE: ' + sNEWVALUE + ' TAM_NEWVALUE__c: ' + objPaso.TAM_NEWVALUE__c  + ' TAM_OLDVALUE__c: ' + objPaso.TAM_OLDVALUE__c);
                   
            //Agrega al mapa de mapIdExtObjFinal un nuevo objeto del tipo TAM_ActualizaHistorialObjFinal__c
            TAM_ActualizaHistorialObjFinal__c objPasoUpd = new TAM_ActualizaHistorialObjFinal__c(
                    TAM_ID_Historial__c = objPaso.TAM_ID_Historial__c,
                    TAM_IdExterno__c = sIdExtPaso,
                    CREATEDDATE__c = objPaso.CREATEDDATE__c,
                    TAM_DUMMYVINFORMULA__c = objPaso.TAM_DUMMYVINFORMULA__c,
                    TAM_NOM_OBJ__c = objPaso.TAM_NOM_OBJ__c,
                    TAM_OLDVALUE__c = objPaso.TAM_OLDVALUE__c,
                    TAM_NEWVALUE__c = sNEWVALUE,
                    TAM_PARENTID__c = objPaso.TAM_PARENTID__c,
                    TAM_SOLICITUDFLOTILLAPROGRAMAID__c = objPaso.TAM_SOLICITUDFLOTILLAPROGRAMAID__c, 
                    TAM_SOLICITUDFLOTILLAPROGRAMANAME__c = objPaso.TAM_SOLICITUDFLOTILLAPROGRAMANAME__c,
                    TAM_TAM_VIN__c = objPaso.TAM_TAM_VIN__c
            );
            //Metelo al mapa de mapIdExtObjFinal si objPaso.TAM_Proceso__c == null
            if (objPaso.TAM_Proceso__c == null && sNEWVALUE != null)
                mapIdExtObjFinal.put(sIdExtPaso, objPasoUpd);
            if (objPaso.TAM_Proceso__c == 'Update Vin DOD y CheckOut'){
                mapIdDodObjDodUpd.put(objPaso.TAM_PARENTID__c, new TAM_DODSolicitudesPedidoEspecial__c(
                        id = objPaso.TAM_PARENTID__c,
                        TAM_VIN__c = objPaso.TAM_TAM_VIN__c,
                        TAM_ActRegCheckOut__c = mapIdDodActCampos.get(objPaso.TAM_PARENTID__c),
                        TAM_Estatus__c = 'Asignado'
                    )
                );
            }//Fin si objPaso.TAM_Proceso__c == 'Update Vin DOD y CheckOut'
        }
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal mapIdExtObjFinal: ' + mapIdExtObjFinal.keySet());
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal mapIdExtObjFinal: ' + mapIdExtObjFinal.values());

        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal mapIdExtObjFinal: ' + mapIdDodObjDodUpd.keySet());
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal mapIdExtObjFinal: ' + mapIdDodObjDodUpd.values());
        
        //Ya tienes el registro en el mapa entonces haz el ups en TAM_ActualizaHistorialObjFinal__c
        if (!mapIdExtObjFinal.isEmpty())
            upsert mapIdExtObjFinal.values() TAM_ActualizaHistorialObjFinal__c.TAM_IdExterno__c;

        List<TAM_LogsErrores__c> lerrorShare = new List<TAM_LogsErrores__c>();
        Savepoint sp = Database.setSavepoint();        
        
        //Ya tienes el registro en el mapa entonces haz el ups en TAM_ActualizaHistorialObjFinal__c
        if (!mapIdDodObjDodUpd.isEmpty()){
            Integer cntRegDpd = 0;
            List<TAM_DODSolicitudesPedidoEspecial__c> lDodUpd = mapIdDodObjDodUpd.values();
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.UpsertResult> lDtbUpsRes = Database.upsert(lDodUpd, TAM_DODSolicitudesPedidoEspecial__c.id, false);
            //Ve si hubo error         
            for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
                TAM_LogsErrores__c objLogErro = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd DOD Vin y CheckOut', TAM_Fecha__c = Date.today());
                if (!objDtbUpsRes.isSuccess()){
                    objLogErro.TAM_DetalleError__c = 'Error en TAM_ActualizaHistorialObj_Handler ID DOD : ' + lDodUpd.get(cntRegDpd) + ' ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage();
                    lerrorShare.add(objLogErro);                
                }//Fin si !objDtbUpsRes.isSuccess()
                cntRegDpd++;
            }//Fin del for para lDtbUpsRes        
        }//Fin si !mapIdDodObjDodUpd.isEmpty()
        System.debug('EN TAM_ActualizaHistorialObj_Handler.actalizaRegistrosObjFinal lerrorShare: ' + lerrorShare);
        
        //Crea el registro en lerrorShare
        if (!lerrorShare.isEmpty())
            insert lerrorShare;

        //rollback para pruebas nada nas
        //Database.rollback(sp);
        
    }    
}