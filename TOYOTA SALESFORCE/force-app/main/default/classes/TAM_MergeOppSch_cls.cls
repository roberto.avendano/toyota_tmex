/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar los registros de la Opp a otra diferente

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    17-Agosto-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeOppSch_cls implements Schedulable{

	global String sQuery {get;set;}
	global List<String> lClientes {get;set;}
	global String strClienteFinal {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeOppSch_cls.execute...');
		String sLClientes = '(';
		
		//Recorre la lista de lClientes
		for (String sIdCliente : lClientes){
			sLClientes += '\'' + sIdCliente + '\',';
		}
		sLClientes = sLClientes.substring(0, sLClientes.lastIndexOf(','));
		sLClientes += ')';
		System.debug('EN TAM_MergeOppSch_cls.execute sLClientes: ' + sLClientes);
		
		this.sQuery = 'Select Id, AccountId, TAM_IdExterno__c, TAM_Distribuidor__c, TAM_Distribuidor__r.Codigo_Distribuidor__c, ';
		this.sQuery += ' TAM_Distribuidor__r.Name ';
		this.sQuery += ' From Opportunity Where AccountId IN ' + sLClientes;
		this.sQuery += ' Order by AccountId ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeOppSch_cls.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_MergeOppBch_cls objUpdOppCls = new TAM_MergeOppBch_cls(sQuery, strClienteFinal);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objUpdOppCls, 100);
			    	 
    }


    
}