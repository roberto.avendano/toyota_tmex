/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de InventarioWholesaleTrigger
						04-Mayo-2020 se agrega el metodo insertaRegistros
						
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    04-Mayo-2020     Héctor Figueroa             Modificación
******************************************************************************* */

public with sharing class InventarioWholesaleTriggerHandler extends TriggerHandler{
	
	private List<InventarioWholesale__c> inventariosWList;
	private Map<Id,InventarioWholesale__c> inventariosWMap;
	
	public InventarioWholesaleTriggerHandler() {
		this.inventariosWList = Trigger.new;	
		this.inventariosWMap = (Map<Id,InventarioWholesale__c>)Trigger.newMap;
	}

	public override void afterInsert(){
		insertaRegistrosObjetoInvFyG(inventariosWList);
		eliminaRecords(inventariosWMap);
	}

	public override void afterUpdate(){
		insertaRegistrosObjetoInvFyG(inventariosWList);
	}

	private void eliminaRecords(Map<Id,InventarioWholesale__c> mapIW){
		List<InventarioWholesale__c> vinesPiso = new List<InventarioWholesale__c>();
		Map<String, string> mapVinPaso = new map<String, String>();
		set<String> setVinLeadPaso = new set<String>();	
		
		String squery = 'SELECT Id FROM InventarioWholesale__c WHERE CreatedDate < TODAY Order by CreatedDate ASC LIMIT 200';
		if (Test.isRunningTest())
			squery = 'SELECT Id FROM InventarioWholesale__c Order by CreatedDate ASC LIMIT 1';
		System.debug('EN InventarioWholesaleTriggerHandler squery: ' + squery);			
		List<SObject> lSoInvePiso = Database.query(squery);
		if (!lSoInvePiso.isEmpty()){
			for (SObject objInvPiso : lSoInvePiso){
				vinesPiso.add( (InventarioWholesale__c) objInvPiso );
			}
		}//Fin si !vinesPiso.isEmpty()
		System.debug('EN InventarioWholesaleTriggerHandler vinesPiso: ' + vinesPiso);		
		//Eliminalos
		if (!Test.isRunningTest()){
			//delete vinesPiso;
			List<Database.Deleteresult> lDtbUpsRes = Database.delete(vinesPiso, false);
			//Ve si hubo error
			for (Database.Deleteresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('En el Inventario Wholesaler hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
				/*if (objDtbUpsRes.isSuccess()){
					setIdContactCreados.add(objDtbUpsRes.getId());
       				System.debug('EN TAM_FacturaCarga_tgr_handler.actualizaDatosOportunidad objDtbUpsRes.getId() Contacts: ' + objDtbUpsRes.getId());							
				}*/
			}//Fin del for para lDtbUpsRes
		}
		
	}

	private void insertaRegistrosObjetoInvFyG(List<InventarioWholesale__c> inventariosWList){
		System.debug('');
		Map<String, TAM_InventarioVehiculosToyota__c> maplInventarioVehiculosToyotaUps = new Map<String, TAM_InventarioVehiculosToyota__c>();
				
		//Recorre la lista de los reg que vienen en inventariosWList
		for (InventarioWholesale__c invWholesale : inventariosWList){
			String sTAMIdExterno = invWholesale.Name + '-Piso-' + Date.today();
			maplInventarioVehiculosToyotaUps.put(sTAMIdExterno, new TAM_InventarioVehiculosToyota__c(
				Name = invWholesale.Name,
				Dealer_Code__c = invWholesale.Dealer_Code__c,
				Distributor_Invoice_Date_MM_DD_YYYY__c = invWholesale.Distributor_Invoice_Date_MM_DD_YYYY__c,
				Dummy_VIN__c = invWholesale.Dummy_VIN__c,
				Exterior_Color_Code__c = invWholesale.Exterior_Color_Code__c,
				Exterior_Color_Description__c = invWholesale.Exterior_Color_Description__c, 
				Interior_Color_Code__c = invWholesale.Interior_Color_Code__c,
				Interior_Color_Description__c = invWholesale.Interior_Color_Description__c,
				Model_Number__c = invWholesale.Model_Number__c,
				Model_Year__c = invWholesale.Model_Year__c,
				Tipo_Inventario__c = 'Piso',
				Toms_Series_Name__c = invWholesale.Toms_Series_Name__c,
				TAM_IdExterno__c = sTAMIdExterno,
				TAM_DiasPiso__c = String.valueOf(invWholesale.TAM_Dias_Piso__c),
				TAM_Assigned_Dealer__c = invWholesale.TAM_Assigned_Dealer__c,
				TAM_Fleet__c = invWholesale.TAM_Fleet__c,
                TAM_Dummy_VIN_Temp__c = invWholesale.TAM_Dummy_VIN_Temp__c,
                TAM_ModelPhaseCode__c = invWholesale.TAM_ModelPhaseCode__c,
                TAM_SerialNumberCDig__c = invWholesale.TAM_SerialNumberCDig__c                				
				)
			);
		}//Fin del for para inventariosWList

		System.debug('EN insertaRegistrosObjetoInvFyG maplInventarioVehiculosToyotaUps.KeySet(): ' + maplInventarioVehiculosToyotaUps.KeySet());		
		System.debug('EN insertaRegistrosObjetoInvFyG maplInventarioVehiculosToyotaUps.Values(): ' + maplInventarioVehiculosToyotaUps.Values());
		//Ve si tiene algo el mapa de maplInventarioVehiculosToyotaUps
		if (!maplInventarioVehiculosToyotaUps.isEmpty()){
			List<Database.UpsertResult> lDtbUpsRes = Database.upsert(maplInventarioVehiculosToyotaUps.values(), TAM_InventarioVehiculosToyota__c.TAM_IdExterno__c, false);
    		//Ve si hubo errores
    		for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
    			if (!objDtbUpsRes.isSuccess())
    				System.debug('Error a la hora de actualizar el Inventario Final: ' + objDtbUpsRes.getErrors()[0].getMessage());
    		}//Fin del for para lDtbUpsRes
		}//Fin si !maplInventarioVehiculosToyotaUps.isEmpty()
		
	}

}