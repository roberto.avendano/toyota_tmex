/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
 /******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba que contiene la logica para la cobertura de la clase de TAM_GridClass.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    13-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */
 
@isTest
private class TAM_GridClass_tst {

	static	Account clientePrueba = new Account();
	static	List<Venta__c> ventasH = new List<Venta__c>();
	static	List<Opportunity> opp = new List<Opportunity>();	
	static	List<TAM_VehiculoPaso__c> vehiculos = new List<TAM_VehiculoPaso__c>();	
	static	List<TAM_ClienteServicioPaso__c> clienteServicio = new List<TAM_ClienteServicioPaso__c>();
	static	List<TAM_PoliticasIncentivos__c> CataPoliticasIncentivos = new List<TAM_PoliticasIncentivos__c>();
	static	List<TAM_DetallePolitica__c> CataTAMDetallePolitica = new List<TAM_DetallePolitica__c>();
	
	static	List<CatalogoCentralizadoModelos__c> CataSeries = new List<CatalogoCentralizadoModelos__c>();
	static	List<Rangos__c> CataRangos = new List<Rangos__c>();
	static	List<InventarioPiso__c> CataInventarioPiso = new List<InventarioPiso__c>();
	
	static	String VaRtPolFlot = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static	String VaRtPolRetail = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Retail').getRecordTypeId();
	
   	static  String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId(); 	
	static  String VaRtStdPriceBookIdReg = Test.getStandardPricebookId();   
	
	@TestSetup 
	static void loadData(){
		Account cliente = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cliente;
	}

	public static void LoadVentasH(){
		System.debug('EN test_one LoadVentasH...');
		clientePrueba = [Select Id, Name, Codigo_Distribuidor__c, UnidadesAutosDemoAutorizadas__c From Account LIMIT 1];
		System.debug('EN test_one LoadVentasH clientePrueba: ' + clientePrueba);
		
		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};

		String sTAMTipoCliente = 'Primero';
		for(Integer i=0; i < 2; i++){
			
			String sufix = i < 10 ? '0' + String.valueOf(i):String.valueOf(i);  
			
	       	//Crea el objeto del tipo Opportunity
			opp.add(new Opportunity( 
				Name = 'JTDKBRFUXH30310'+sufix,
				TAM_Vin__c = 'JTDKBRFUXH30310'+sufix,			
				CloseDate = Date.today(),
				StageName = 'Closed Won',
				Pricebook2Id = VaRtStdPriceBookIdReg,
				Amount = 1.0,
				TAM_IdExterno__c = 'JTDKBRFUXH30310'+sufix,
				recordTypeId = VaRtOppRegVCrm,
				AccountId = clientePrueba.id
				)
			);						 			
						
			//Crea el objeto para la venta		 	
		 	ventasH.add(new Venta__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),            		
        		IdModelo__c = modelos.get(aleatorio(modelos.size())),
        		YearModelo__c = '2017',
        		IdColorExt__c = colores.get(aleatorio(colores.size())),
        		DescrColorExt__c = 'External color description',
        		IdColorInt__c = colores.get(aleatorio(colores.size())),
        		DescrColorInt__c = 'Internal color description',
    			Serial__c = seriales.get(aleatorio(seriales.size())),
    			SaleCode__c = sufix,
    			SubmittedDate__c = Datetime.newInstance(2017, 10, 13),
        		IdExterno__c = 'JTDKBRFUXH30310'+sufix+'-'+String.valueOf(Date.today()),
        		TAM_TipoCliente__c = sTAMTipoCliente,
        		FirstName__c = 'PruebaFN',
        		LastName__c = 'PruebaLN'
	 		));
	 		//Cambia el estatus
	 		if (sTAMTipoCliente == 'Primero')
				sTAMTipoCliente = 'Segundo';
	 		else if (sTAMTipoCliente == 'Segundo')
				sTAMTipoCliente = 'Primero';

			//Crea los registros del tipo TAM_VehiculoPaso__c
			vehiculos.add(new TAM_VehiculoPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),
					TAM_IdModelo__c = modelos.get(aleatorio(modelos.size())),
					TAM_Serial__c = seriales.get(aleatorio(seriales.size())),
					TAM_YearModelo__c = '2018',
					TAM_TransType__c = 'RDR',
					TAM_Factura__c = '123456789',
					TAM_SaleCode__c = '1',
					TAM_IdColorExt__c = '03R3',
					TAM_DescrColorExt__c = 'BARCELONA RED MET',
					TAM_IdColorInt__c = 'EA20',
					TAM_DescrColorInt__c = 'EA20',
					TAM_SaleDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedDate__c = '2018-07-02 00:00:00',
					TAM_SubmittedTime__c = '19.05.30.077173',
					TAM_SalesManager__c = '169621364',
					TAM_lsAssesor__c = '',
					TAM_FLEET__c = 'N',
					TAM_Year__c = '2018',
					TAM_Month__c = '6'
				)
			);
			
			//Crea los registros del tipo TAM_ClienteServicioPaso__c
			clienteServicio.add(new TAM_ClienteServicioPaso__c(
					Name = 'JTDKBRFUXH30310'+sufix,
					TAM_RFC__c = 'HUVN6210097G1',
					TAM_FirstName__c = 'NORMA ANGELICA',
					TAM_LastName__c = 'HUITRON',
					TAM_LastName2__c = 'VAZQUEZ',
					TAM_Address1__c = 'GRANADA',
					TAM_Address2__c = '',
					TAM_Numero__c = '5989',
					TAM_Colonia__c = 'CUMBRES DE SANTA CLARA 2 SEC',
					TAM_DelMunc__c = 'MONTERREY',
					TAM_Zip__c = '64349',
					TAM_City__c = 'MONTERREY',
					TAM_State__c = 'NUEVO LEÓN',
					TAM_country__c = 'MEXICO',
					TAM_TelCel__c = '448116362676',
					TAM_Telhome__c = '8116362676',
					TAM_TelWork__c = '',
					TAM_Email__c = 'NORMAHUITRON9@HOTMAIL.COM',
					TAM_MetodoPreferidoContacto__c = 'EMAIL',
					TAM_UsoDatosPersonales__c = '',
					TAM_Year__c = '2018',
					TAM_Month__c = '9',
					TAM_Day__c = '10',
					TAM_TipoCliente__c = 'Primero'
				)
			);
		

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataSeries.add(new CatalogoCentralizadoModelos__c(
					Name = 'YARIS R-1062-2020-41G-80-XLE AT' + sufix,
					Serie__c = 'YARIS R' + sufix,
					AnioModelo__c = '2020',
					DescripcionColorExterior__c = 'Rojo',
					DescripcionColorInterior__c = 'Azul',
					TAM_Diposnible_Programas__c = 'No',
					Marca__c = 'Toyota',
					Modelo__c = '1062',
					Version__c = 'XLE AT',
					CodigoColorExterior__c = '41G',
					CodigoColorInterior__c = '80',
					Disponible__c = 'SI'
				)
			);

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataRangos.add(new Rangos__c(
					Name = 'A' + sufix,
					NumUnidadesMinimas__c = 5,
					NumUnidadesMaximas__c = 30,
					DiasVigentes__c = 180,
					PerGraciaRango__c = 15,
					Activo__c = true
				)
			);

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataInventarioPiso.add(new InventarioPiso__c(
					Name = 'Yaris R-1062-2020-41G-2019-11-19' + sufix,
					FechaInventario__c = Date.today().addDays(-1),
					Modelo__c = '1062',
					AnioModelo__c = '2020',
					CodigoColorExterior__c = '41G',
					ColorExterior__c = 'Rojo',
					Categoria__c = 'G',
					Serie__c = 'Yaris R' + sufix,
					Version__c = 'Yaris R XLE AT',
					Cantidad__c = 73,
					DiasVenta__c = 19
				)
			);

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataPoliticasIncentivos.add(new TAM_PoliticasIncentivos__c(
					Name = 'TOYOTATÓN' + sufix,
					TAM_InicioVigencia__c = Date.today(),
					TAM_FinVigencia__c = Date.today().addDays(50),
					TAM_PeriodoDeGracia__c = 10,
					TAM_EstatusIncentivos__c = 'En proceso',
					RecordTypeId = VaRtPolFlot
				)
			);

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataTAMDetallePolitica.add(new TAM_DetallePolitica__c(
					Name = '2019-AVANZA-2203-Cargo0' + sufix,
					TAM_AnioModelo__c = '2020',
					TAM_Serie__c = 'AVANZA',
					TAM_Version__c = 'Cargo'
				)
			);

		}//Fin el for para los registros que se va a crear		
				
				
		for(Integer i=0; i < 2; i++){
			
			String sufix = i < 10 ? '0' + String.valueOf(i):String.valueOf(i);  

			//Crea los registros del tipo CatalogoCentralizadoModelos__c
			CataInventarioPiso.add(new InventarioPiso__c(
					Name = 'Yaris R-1062-2020-41G-2019-11-19' + sufix,
					FechaInventario__c = Date.today().addDays(-1),
					Modelo__c = '1062',
					AnioModelo__c = '2020',
					CodigoColorExterior__c = '41G',
					ColorExterior__c = 'Rojo',
					Categoria__c = 'G',
					Serie__c = 'Yaris R' + sufix,
					Version__c = 'Yaris R XLE AT',
					Cantidad__c = 73,
					DiasVenta__c = 19
				)
			);

		}//Fin el for para los registros que se va a crear
				
				
		System.debug(JSON.serialize(ventasH));		
		System.debug(JSON.serialize(vehiculos));		
		System.debug(JSON.serialize(clienteServicio));
		
		System.debug(JSON.serialize(CataSeries));
		System.debug(JSON.serialize(CataRangos));
		System.debug(JSON.serialize(CataInventarioPiso));
		System.debug(JSON.serialize(CataPoliticasIncentivos));
		System.debug(JSON.serialize(CataTAMDetallePolitica));
		//return ventasH;
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    @isTest 
    static void TAM_GridClass_OK() {
		//Crea la Data para las pruebas
		TAM_GridClass_tst.LoadVentasH();
		
		List<Opportunity> oppPaso = opp;
		List<Venta__c> ventasPaso = ventasH;
		List<TAM_VehiculoPaso__c> vehiculosPaso = vehiculos;
		List<TAM_ClienteServicioPaso__c> clienteServicioPaso = clienteServicio;
		
		List<CatalogoCentralizadoModelos__c> CataSeriesPaso = CataSeries;
    	List<Rangos__c> CataRangosPaso = CataRangos;
    	List<InventarioPiso__c> CataInventarioPisoPaso = CataInventarioPiso;
    	List<TAM_PoliticasIncentivos__c> CataPoliticasIncentivosPaso = CataPoliticasIncentivos;
    	List<TAM_DetallePolitica__c> CataTAMDetallePoliticaPaso = CataTAMDetallePolitica;
    	
    	System.debug('EN test_one oppPaso: ' + oppPaso);
		System.debug('EN test_one ventasPaso: ' + ventasPaso);
		System.debug('EN test_one vehiculosPaso: ' + vehiculosPaso);		
		System.debug('EN test_one clienteServicioPaso: ' + clienteServicioPaso);

		System.debug('EN test_one CataSeriesPaso: ' + CataSeriesPaso);
		System.debug('EN test_one CataRangosPaso: ' + CataRangosPaso);
		System.debug('EN test_one CataInventarioPisoPaso: ' + CataInventarioPisoPaso);
		System.debug('EN test_one CataPoliticasIncentivosPaso: ' + CataPoliticasIncentivosPaso);
		System.debug('EN test_one CataTAMDetallePoliticaPaso: ' + CataTAMDetallePoliticaPaso);
		
		//Crea los registros
		/*insert oppPaso;
		insert ventasPaso;
		insert vehiculosPaso;
		insert clienteServicioPaso;*/
		
		insert CataSeriesPaso;
		insert CataRangosPaso;
		insert CataInventarioPisoPaso;
		System.debug('EN test_one CataInventarioPisoPaso.get(0).id: ' + CataInventarioPisoPaso.get(0));
		
		insert CataPoliticasIncentivosPaso;
		System.debug('EN test_one CataPoliticasIncentivosPaso: ' + CataPoliticasIncentivosPaso);
		System.debug('EN test_one CataPoliticasIncentivosPaso.get(0).id: ' + CataPoliticasIncentivosPaso.get(0).id);
		
		CatalogoCentralizadoModelos__c objCatCentMod = CataSeries.get(0);
		String strIdDetalles = VaRtPolFlot + String.valueOf(objCatCentMod.AnioModelo__c) + String.valueOf(objCatCentMod.Serie__c) + String.valueOf(objCatCentMod.Modelo__c) + String.valueOf(objCatCentMod.Version__c);
		
		Integer intCmtRhg = 0;
		//Recorre la CataInventarioPisoPaso
		for (TAM_DetallePolitica__c objDetPol : CataTAMDetallePoliticaPaso){
			String sufix = intCmtRhg < 10 ? '0' + String.valueOf(intCmtRhg) : String.valueOf(intCmtRhg);  
			objDetPol.TAM_PoliticaIncentivos__c = CataPoliticasIncentivosPaso.get(0).id;
			objDetPol.TAM_IdExterno__c = strIdDetalles + '' + sufix;
			intCmtRhg++;
		}
		System.debug('EN test_one CataTAMDetallePoliticaPaso: ' + CataTAMDetallePoliticaPaso);
		//Crea las politicas
		insert CataTAMDetallePoliticaPaso;
				
		System.debug('EN test_one VaRtPolFlot: ' + VaRtPolFlot);
		System.debug('EN test_one VaRtPolRetail: ' + VaRtPolRetail);
		
		//Ahora si manda a llamar la Clase de TAM_GridClass
		//El metodo de getSeries
		Map<String, List<String>> mapPasoSeries = TAM_GridClass.getSeries();
		//El metodo de getRangos
		List<Rangos__c> ListRangos= TAM_GridClass.getRangos();
		//El metodo de TipoRegistroPolitica
		String sIdRectdTypePol = TAM_GridClass.TipoRegistroPolitica(CataPoliticasIncentivos.get(0).id);
		
		List<String> lopenSections = new List<String>();
		//Recorre la lista de CataSeries y obten los datos del campo
		for (CatalogoCentralizadoModelos__c objCatCentModCon :  [Select id, Serie_AnioModelo__c  From CatalogoCentralizadoModelos__c
			Where ID IN : CataSeriesPaso ]){
			lopenSections.add(objCatCentModCon.Serie_AnioModelo__c);
		}
		System.debug('EN test_one lopenSections: ' + lopenSections);
		
		//El metodo de setWrapper
		List<TAM_GridWrapperClass> LsitGridWrapperClass = TAM_GridClass.setWrapper(lopenSections, CataPoliticasIncentivos.get(0).id);
		for (TAM_GridWrapperClass objWrpListaPaso : LsitGridWrapperClass){
			objWrpListaPaso.intIncentivoPropuesto_Pesos = 100.00;
			objWrpListaPaso.intIncentivoPropuesto_Porcentaje = 2.00;
		}
		
		//Serializa la lsta dw LsitGridWrapperClass con JSON.serialize()
		String sLsitGridWrapperClass = JSON.serialize(LsitGridWrapperClass);
		//El metodo de guardarPolitica
		List<TAM_DetallePolitica__c> LsitTAM_DetallePolitica = TAM_GridClass.guardarPolitica(sLsitGridWrapperClass, VaRtPolFlot);
        
        //Crea un objeto del tipo TAM_GridTripePayWrapperClass
        TAM_GridTripePayWrapperClass objTAMGridTripePayWrapperClass = new TAM_GridTripePayWrapperClass();   
        
    }

}