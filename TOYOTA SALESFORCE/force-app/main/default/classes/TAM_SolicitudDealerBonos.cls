public  without sharing  class TAM_SolicitudDealerBonos {
    
    @AuraEnabled
    public static List<TAM_DetalleBonos__c> consultarBonosVIN (String serie,String codigoModelo,String anioModelo,String VIN){
        List<TAM_DetalleBonos__c> consultaFiltrada = new  List<TAM_DetalleBonos__c>();
        Date fechaHoy = system.today();
        Date fechaMenos2Meses = date.today().adddays(-100);
        
        List<TAM_DetalleBonos__c> consultaDetalleBono = [SELECT Bono__c,TAM_AplicaAmbosBonos__c,Bono__r.name,Id,Name,TAM_AnioModelo__c,TAM_BonoDealerEfectivo__c
                                                         ,TAM_BonoDealerProdFinanciero__c,TAM_BonoTFSProdFinanciero__c,TAM_BonoTMEXEfectivo__c,
                                                         TAM_BonoTMEXProdFinanciero__c,TAM_BonoTotalEfectivo__c,TAM_BonoTotalProductoFinanciero__c,
                                                         TAM_Clave__c,TAM_ComentariosEfectivo__c,TAM_DescripcionProdFinanciero__c,TAM_DiasVenta__c,
                                                         TAM_InventarioDealer__c,TAM_ProductoFinanciero__c,TAM_Serie__c,TAM_Version__c FROM TAM_DetalleBonos__c
                                                         WHERE  Bono__r.TAM_FinVigencia__c >=: fechaMenos2Meses AND
                                                         TAM_AnioModelo__c =: anioModelo AND TAM_Clave__c =: codigoModelo AND TAM_BonoTotalEfectivo__c != null ];    
        
        
        for(TAM_DetalleBonos__c consultaBonos : consultaDetalleBono ){
            //If que verifica que exita la condición Aplica Ambos bonos
            if(consultaBonos.TAM_BonoTotalEfectivo__c != null || consultaBonos.TAM_BonoTotalEfectivo__c != null  ){
                //If que verifica que exita la condición No Aplica ambos y validar si existe valor en Bono en efectivo 
                
                consultaFiltrada.add(consultaBonos);
                
                
            }
            
        }
        
        return consultaFiltrada;   
        
    }    
    
    
    @AuraEnabled
    public static wrapDeBonos getIncentivoCorrespondienteVIN(String serie, String codigoModelo, String anioModelo, String VIN ){
        Date fechaHoy = system.today();
        List<TAM_DetalleBonos__c> incentivoCorrespondiente = new List<TAM_DetalleBonos__c>();
        TAM_DetalleBonos__c politicaMenorValor = new TAM_DetalleBonos__c();
        wrapDeBonos resumenBono = new wrapDeBonos();
        incentivoCorrespondiente = [SELECT Bono__c,TAM_DetalleBonos__c.TAM_CondicionesProdFinanciero__c,TAM_DetalleBonos__c.TAM_CondicionesBonoEfectivo__c,TAM_DetalleBonos__c.TAM_BonoTFSEfectivo__c,TAM_AplicaAmbosBonos__c,Bono__r.name,Id,Name,TAM_AnioModelo__c,TAM_BonoDealerEfectivo__c
                                    ,TAM_BonoDealerProdFinanciero__c,TAM_BonoTFSProdFinanciero__c,TAM_BonoTMEXEfectivo__c,
                                    TAM_BonoTMEXProdFinanciero__c,TAM_BonoTotalEfectivo__c,TAM_BonoTotalProductoFinanciero__c,
                                    TAM_Clave__c,TAM_ComentariosEfectivo__c,TAM_DescripcionProdFinanciero__c,TAM_DiasVenta__c,
                                    TAM_InventarioDealer__c,TAM_ProductoFinanciero__c,TAM_Serie__c,TAM_Version__c FROM TAM_DetalleBonos__c 
                                    WHERE  (Bono__r.TAM_InicioVigencia__c <=: fechaHoy AND Bono__r.TAM_FinVigencia__c >=: fechaHoy) 
                                    AND TAM_AnioModelo__c =: anioModelo AND TAM_Clave__c =: codigoModelo AND TAM_BonoTotalEfectivo__c != null   order by TAM_BonoTotalEfectivo__c asc limit 1]; 
        
        if(!incentivoCorrespondiente.isEmpty()){
            for(TAM_DetalleBonos__c i: incentivoCorrespondiente){
                if(i.TAM_BonoTotalEfectivo__c != null){
                    resumenBono.bono = i.Bono__c;
                    resumenBono.TAM_BonoTotalProductoFinancieroTMEX = i.TAM_BonoTMEXProdFinanciero__c;
                    resumenBono.aplicaAmbosBonos = i.TAM_AplicaAmbosBonos__c;
                    resumenBono.NombreDelBono = i.Bono__r.name;
                    String nombreClean  =  resumenBono.NombreDelBono.replace(' ','\n');
                    resumenBono.NombreDelBono = i.Bono__r.name;
                    resumenBono.NombreDetalleBono = i.name;
                    resumenBono.idBono = i.id;
                    resumenBono.TAM_AnioModelo = i.TAM_AnioModelo__c;
                    resumenBono.TAM_BonoTMEXEfectivo = i.TAM_BonoTMEXEfectivo__c;
                    resumenBono.TAM_BonoTotalProductoFinancieroTFS = i.TAM_BonoTFSProdFinanciero__c;
                    resumenBono.TAM_BonoDealerEfectivo = i.TAM_BonoDealerEfectivo__c;
                    resumenBono.TAM_BonoTFSEfectivo = i.TAM_BonoTFSEfectivo__c;
                    resumenBono.TAM_BonoDealerProdFinanciero = i.TAM_BonoDealerProdFinanciero__c;
                    resumenBono.TAM_BonoTotalEfectivo = i.TAM_BonoTotalEfectivo__c;
                    resumenBono.TAM_BonoTotalProductoFinanciero = i.TAM_BonoTotalProductoFinanciero__c;
                    resumenBono.TAM_Clave = i.TAM_Clave__c;
                    resumenBono.TAM_ComentariosEfectivo = i.TAM_ComentariosEfectivo__c;
                    resumenBono.TAM_DescripcionProdFinanciero = i.TAM_DescripcionProdFinanciero__c;
                    resumenBono.TAM_DiasVenta  = i.TAM_DiasVenta__c;
                    resumenBono.TAM_InventarioDealer = i.TAM_InventarioDealer__c;
                    resumenBono.TAM_Serie  = i.TAM_Serie__c;
                    resumenBono.TAM_Version = i.TAM_Version__c;
                    resumenBono.condicionesBonoEfectivo = i.TAM_CondicionesBonoEfectivo__c;
                    resumenBono.condicionesBonoProductoFinaciero = i.TAM_CondicionesProdFinanciero__c;
                    if(i.TAM_BonoTotalEfectivo__c != null && i.TAM_BonoTMEXEfectivo__c != null && i.TAM_BonoDealerEfectivo__c != null){
                        resumenBono.TAM_CantidadDineroEfectivoTMEX  = (i.TAM_BonoTMEXEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                        resumenBono.TAM_CantidadDineroEfectivoDealer =  (i.TAM_BonoDealerEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                        //Se valida que TAM_BonoTFSEfectivo__c sea diferente de nulo
                        if(i.TAM_BonoTFSEfectivo__c != null){
                           resumenBono.TAM_CantidadDineroEfectivoTFS =   (i.TAM_BonoTFSEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                        }
                        resumenBono.TAM_PorcentajeTotalEfectivo = 100;
                    }
                    if(i.TAM_BonoTotalProductoFinanciero__c != null && i.TAM_BonoTMEXProdFinanciero__c != null && i.TAM_BonoDealerProdFinanciero__c != null){
                        resumenBono.TAM_ProductoFinanciero = i.TAM_ProductoFinanciero__c;
                        String nombreCleanProductoFinanciero  =  i.TAM_ProductoFinanciero__c.replace(' ','\n');
                        resumenBono.TAM_ProductoFinanciero = nombreCleanProductoFinanciero;
                        resumenBono.TAM_CantidadDineroProductoFinancieroTMEX = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTMEXProdFinanciero__c)/100;
                        resumenBono.TAM_CantidadDineroProductoFinancieroDealer =  (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoDealerProdFinanciero__c)/100;
                        //Se valida que TAM_BonoTFSProdFinanciero__c sea diferente de nulo
                        if(i.TAM_BonoTFSProdFinanciero__c != null){
                        	resumenBono.TAM_CantidadDineroProductoFinancieroTFS = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTFSProdFinanciero__c)/100;
                        }
                    } 
                }
            }
        }
        
        if(!incentivoCorrespondiente.isEmpty()){ 
            return resumenBono;
        }
        
        else{
            return null;
            
        }
        
    }    
    
    
    @AuraEnabled
    public static wrapDeBonos getIncentivoCorrespondienteFechaVenta(String serie, String codigoModelo, String anioModelo, String VIN, String fechaVenta){
        List<TAM_DetalleBonos__c> incentivoCorrespondiente = new List<TAM_DetalleBonos__c>();
        TAM_DetalleBonos__c politicaMenorValor = new TAM_DetalleBonos__c();
        wrapDeBonos resumenBono = new wrapDeBonos();
        Date fechaDeVenta = Date.valueOf(fechaVenta); 
        system.debug('fechaVenta'+fechaDeVenta);
        incentivoCorrespondiente = [SELECT Bono__c,TAM_DetalleBonos__c.TAM_CondicionesProdFinanciero__c,TAM_DetalleBonos__c.TAM_CondicionesBonoEfectivo__c,TAM_DetalleBonos__c.TAM_BonoTFSEfectivo__c,TAM_AplicaAmbosBonos__c,Bono__r.name,Id,Name,TAM_AnioModelo__c,TAM_BonoDealerEfectivo__c
                                    ,TAM_BonoDealerProdFinanciero__c,TAM_BonoTFSProdFinanciero__c,TAM_BonoTMEXEfectivo__c,
                                    TAM_BonoTMEXProdFinanciero__c,TAM_BonoTotalEfectivo__c,TAM_BonoTotalProductoFinanciero__c,
                                    TAM_Clave__c,TAM_ComentariosEfectivo__c,TAM_DescripcionProdFinanciero__c,TAM_DiasVenta__c,
                                    TAM_InventarioDealer__c,TAM_ProductoFinanciero__c,TAM_Serie__c,TAM_Version__c FROM TAM_DetalleBonos__c 
                                    WHERE (Bono__r.TAM_InicioVigencia__c <=: fechaDeVenta AND Bono__r.TAM_FinVigencia__c >=: fechaDeVenta) AND  TAM_AnioModelo__c =: anioModelo AND TAM_Clave__c =: codigoModelo AND TAM_BonoTotalEfectivo__c != null  order by TAM_BonoTotalEfectivo__c asc limit 1]; 
        
        if(!incentivoCorrespondiente.isEmpty()){
            for(TAM_DetalleBonos__c i: incentivoCorrespondiente){
                if(i.TAM_BonoTotalEfectivo__c != null){
                    resumenBono.bono = i.Bono__c;
                    resumenBono.TAM_BonoTotalProductoFinancieroTMEX = i.TAM_BonoTMEXProdFinanciero__c;
                    resumenBono.aplicaAmbosBonos = i.TAM_AplicaAmbosBonos__c;
                    resumenBono.NombreDelBono = i.Bono__r.name;
                    String nombreClean  =  resumenBono.NombreDelBono.replace(' ','\n');
                    resumenBono.NombreDelBono = i.Bono__r.name;
                    resumenBono.NombreDetalleBono = i.name;
                    resumenBono.idBono = i.id;
                    resumenBono.TAM_AnioModelo = i.TAM_AnioModelo__c;
                    resumenBono.TAM_BonoTMEXEfectivo = i.TAM_BonoTMEXEfectivo__c;
                    resumenBono.TAM_BonoTotalProductoFinancieroTFS = i.TAM_BonoTFSProdFinanciero__c;
                    resumenBono.TAM_BonoDealerEfectivo = i.TAM_BonoDealerEfectivo__c;
                    //resumenBono.TAM_BonoTFSEfectivo = i.TAM_BonoTFSEfectivo__c;
                    resumenBono.TAM_BonoDealerProdFinanciero = i.TAM_BonoDealerProdFinanciero__c;
                    resumenBono.TAM_BonoTotalEfectivo = i.TAM_BonoTotalEfectivo__c;
                    resumenBono.TAM_BonoTotalProductoFinanciero = i.TAM_BonoTotalProductoFinanciero__c;
                    resumenBono.TAM_Clave = i.TAM_Clave__c;
                    resumenBono.TAM_ComentariosEfectivo = i.TAM_ComentariosEfectivo__c;
                    resumenBono.TAM_DescripcionProdFinanciero = i.TAM_DescripcionProdFinanciero__c;
                    resumenBono.TAM_DiasVenta  = i.TAM_DiasVenta__c;
                    resumenBono.TAM_InventarioDealer = i.TAM_InventarioDealer__c;
                    resumenBono.TAM_Serie  = i.TAM_Serie__c;
                    resumenBono.TAM_Version = i.TAM_Version__c;
                    resumenBono.condicionesBonoEfectivo = i.TAM_CondicionesBonoEfectivo__c;
                    resumenBono.condicionesBonoProductoFinaciero = i.TAM_CondicionesProdFinanciero__c;
                    if(i.TAM_BonoTotalEfectivo__c != null && i.TAM_BonoTMEXEfectivo__c != null && i.TAM_BonoDealerEfectivo__c != null){
                        resumenBono.TAM_CantidadDineroEfectivoTMEX  = (i.TAM_BonoTMEXEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                        resumenBono.TAM_CantidadDineroEfectivoDealer =  (i.TAM_BonoDealerEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                        //Se valida que TAM_BonoTFSEfectivo__c sea diferente de nulo
                        if(i.TAM_BonoTFSEfectivo__c != null){
                           resumenBono.TAM_CantidadDineroEfectivoTFS =   (i.TAM_BonoTFSEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c; 
                        }
                        resumenBono.TAM_PorcentajeTotalEfectivo = 100;
                    }
                    if(i.TAM_BonoTotalProductoFinanciero__c != null && i.TAM_BonoTMEXProdFinanciero__c != null && i.TAM_BonoDealerProdFinanciero__c != null){
                        resumenBono.TAM_ProductoFinanciero = i.TAM_ProductoFinanciero__c;
                        String nombreCleanProductoFinanciero  =  i.TAM_ProductoFinanciero__c.replace(' ','\n');
                        resumenBono.TAM_ProductoFinanciero = nombreCleanProductoFinanciero;
                        resumenBono.TAM_CantidadDineroProductoFinancieroTMEX = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTMEXProdFinanciero__c)/100;
                        resumenBono.TAM_CantidadDineroProductoFinancieroDealer =  (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoDealerProdFinanciero__c)/100;
                        //Se valida que TAM_BonoTFSProdFinanciero__c sea diferente de nulo
                         if(i.TAM_BonoTFSProdFinanciero__c != null){
                        	resumenBono.TAM_CantidadDineroProductoFinancieroTFS = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTFSProdFinanciero__c)/100;
                         }
                    }
                }
            }
        }
        
        if(!incentivoCorrespondiente.isEmpty()){ 
            system.debug('Bono retornado'+resumenBono);
            return resumenBono;
        }
        
        else{
            return null;
            
        }
        
    }    
    
    
    @AuraEnabled
    public static wrapDeBonos obtieneResumenBonoSeleccionado(String idDetalleBonoSeleccionado){
        wrapDeBonos resumenBono = new wrapDeBonos();
        
        List<TAM_DetalleBonos__c> consultaDetalleBono = [SELECT Bono__c,TAM_DetalleBonos__c.TAM_CondicionesProdFinanciero__c,TAM_CondicionesBonoEfectivo__c,TAM_BonoTFSEfectivo__c,TAM_AplicaAmbosBonos__c,Bono__r.name,Id,Name,TAM_AnioModelo__c,TAM_BonoDealerEfectivo__c
                                                         ,TAM_BonoDealerProdFinanciero__c,TAM_BonoTFSProdFinanciero__c,TAM_BonoTMEXEfectivo__c,
                                                         TAM_BonoTMEXProdFinanciero__c,TAM_BonoTotalEfectivo__c,TAM_BonoTotalProductoFinanciero__c,
                                                         TAM_Clave__c,TAM_ComentariosEfectivo__c,TAM_DescripcionProdFinanciero__c,TAM_DiasVenta__c,
                                                         TAM_InventarioDealer__c,TAM_ProductoFinanciero__c,TAM_Serie__c,TAM_Version__c FROM TAM_DetalleBonos__c WHERE id =:idDetalleBonoSeleccionado];    
        
        
        if(!consultaDetalleBono.isEmpty()){
            for(TAM_DetalleBonos__c i : consultaDetalleBono){
                resumenBono.bono = i.Bono__c;
                resumenBono.TAM_BonoTotalProductoFinancieroTMEX = i.TAM_BonoTMEXProdFinanciero__c;
                resumenBono.aplicaAmbosBonos = i.TAM_AplicaAmbosBonos__c;
                resumenBono.NombreDelBono = i.Bono__r.name;
                String nombreClean  =  resumenBono.NombreDelBono.replace(' ','\n');
                resumenBono.NombreDelBono = i.Bono__r.name;
                resumenBono.NombreDetalleBono = i.name;
                resumenBono.idBono = i.id;
                resumenBono.TAM_AnioModelo = i.TAM_AnioModelo__c;
                resumenBono.TAM_BonoTMEXEfectivo = i.TAM_BonoTMEXEfectivo__c;
                resumenBono.TAM_BonoTotalProductoFinancieroTFS = i.TAM_BonoTFSProdFinanciero__c;
                resumenBono.TAM_BonoDealerEfectivo = i.TAM_BonoDealerEfectivo__c;
                resumenBono.TAM_BonoTFSEfectivo = i.TAM_BonoTFSEfectivo__c;
                resumenBono.TAM_BonoDealerProdFinanciero = i.TAM_BonoDealerProdFinanciero__c;
                resumenBono.TAM_BonoTotalEfectivo = i.TAM_BonoTotalEfectivo__c;
                resumenBono.TAM_BonoTotalProductoFinanciero = i.TAM_BonoTotalProductoFinanciero__c;
                resumenBono.TAM_Clave = i.TAM_Clave__c;
                resumenBono.TAM_ComentariosEfectivo = i.TAM_ComentariosEfectivo__c;
                resumenBono.TAM_DescripcionProdFinanciero = i.TAM_DescripcionProdFinanciero__c;
                resumenBono.TAM_DiasVenta  = i.TAM_DiasVenta__c;
                resumenBono.TAM_InventarioDealer = i.TAM_InventarioDealer__c;
                resumenBono.TAM_Serie  = i.TAM_Serie__c;
                resumenBono.TAM_Version = i.TAM_Version__c;
                resumenBono.condicionesBonoEfectivo = i.TAM_CondicionesBonoEfectivo__c;
                resumenBono.condicionesBonoProductoFinaciero = i.TAM_CondicionesProdFinanciero__c;
                if(i.TAM_BonoTotalEfectivo__c != null && i.TAM_BonoTMEXEfectivo__c != null && i.TAM_BonoDealerEfectivo__c != null){
                    resumenBono.TAM_CantidadDineroEfectivoTMEX  = (i.TAM_BonoTMEXEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                    resumenBono.TAM_CantidadDineroEfectivoDealer =  (i.TAM_BonoDealerEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                    //Se valida que TAM_BonoTFSEfectivo__c sea diferente de nulo
                    if(i.TAM_BonoTFSEfectivo__c != null){
                    	resumenBono.TAM_CantidadDineroEfectivoTFS =   (i.TAM_BonoTFSEfectivo__c * 100)/i.TAM_BonoTotalEfectivo__c;
                    }
                    resumenBono.TAM_PorcentajeTotalEfectivo = 100;
                }
                if(i.TAM_BonoTotalProductoFinanciero__c != null && i.TAM_BonoTMEXProdFinanciero__c != null && i.TAM_BonoDealerProdFinanciero__c != null && i.TAM_BonoTFSProdFinanciero__c != null){
                    resumenBono.TAM_ProductoFinanciero = i.TAM_ProductoFinanciero__c;
                    String nombreCleanProductoFinanciero  =  i.TAM_ProductoFinanciero__c.replace(' ','\n');
                    resumenBono.TAM_ProductoFinanciero = nombreCleanProductoFinanciero;
                    resumenBono.TAM_CantidadDineroProductoFinancieroTMEX = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTMEXProdFinanciero__c)/100;
                    resumenBono.TAM_CantidadDineroProductoFinancieroDealer =  (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoDealerProdFinanciero__c)/100;
                    //Se valida que TAM_BonoTFSProdFinanciero__c sea diferente de nulo
                    if(i.TAM_BonoTFSProdFinanciero__c != null){
                    	resumenBono.TAM_CantidadDineroProductoFinancieroTFS = (i.TAM_BonoTotalProductoFinanciero__c * i.TAM_BonoTFSProdFinanciero__c)/100;
                    }
                }
            }
        }
        
        return resumenBono;    
        
    }
    
    
    @AuraEnabled 
    public static Id guardaSolicitudBono(String objSolicitudBono, String asignacionVIN, String tipoMovimiento){
        Id idDistribuidor;
        Id recordTypeIdAcc =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        wrapDeBonos solicitud;
        
        if(objSolicitudBono != null){
            solicitud = (wrapDeBonos) JSON.deserialize(objSolicitudBono,wrapDeBonos.class);
        }
        
        
        if(recordTypeIdAcc != null && solicitud.TAM_codigoDealer != null){
            idDistribuidor = [Select id FROM Account WHERE Codigo_Distribuidor__c =: solicitud.TAM_codigoDealer AND recordtypeId =: recordTypeIdAcc limit 1].id;
        }
        
        //Se crea un objeto de tipo Solicitud de Bono para realizar el mapeo del objeto wrapDeBonos
        TAM_SolicitudDeBono__c solicitudBono = new TAM_SolicitudDeBono__c();
        solicitudBono.TAM_EstatusSolicitudBono__c = asignacionVIN;
        solicitudBono.TAM_TipoMovimiento__c    = tipoMovimiento; 
        solicitudBono.TAM_VIN__c = solicitud.VIN;
        solicitudBono.TAM_Serie__c = solicitud.TAM_Serie;
        solicitudBono.TAM_CodigoModelo__c = solicitud.TAM_codigoModelo;
        solicitudBono.TAM_AnioModelo__c =  solicitud.TAM_AnioModelo;
        solicitudBono.TAM_Distribuidor__c = idDistribuidor;
        solicitudBono.TAM_cumpleConCondiciones__c = solicitud.TAM_cumpleConCondiciones;
        Date dateSale;
        if(solicitud.TAM_fechaVenta != null){
            dateSale = Date.valueOf(solicitud.TAM_fechaVenta);           
            solicitudBono.TAM_FechaVenta__c = dateSale;
        }
        solicitudBono.TAM_ComentarioDealer__c = solicitud.TAM_comentarioDelaer;
        solicitudBono.TAM_BonoSeleccionado__c  = solicitud.TAM_bonoSolicitado;
        solicitudBono.TAM_EstatusVIN__c = solicitud.TAM_estatusVIN;
        solicitudBono.TAM_BonoElejido__c = solicitud.TAM_bonoElegido;
        solicitudBono.TAM_CodigoVenta__c = solicitud.TAM_codigoVenta;
        solicitudBono.TAM_BonoCorrespondiente__c = solicitud.TAM_BonoCorrespondiente;
        
        try{
            insert solicitudBono;   
        }catch(Exception e){
            system.debug('Error al insertar el registro de la solicitud'+e.getMessage());
        }
        
        return solicitudBono.id;
    }
    
    
    public class wrapDeBonos {
        
        @AuraEnabled public String VIN {get; set;}
        @AuraEnabled public String bono {get; set;}
        @AuraEnabled public boolean aplicaAmbosBonos {get; set;}
        @AuraEnabled public String NombreDelBono {get; set;}
        @AuraEnabled public String idBono {get; set;}
        @AuraEnabled public String TAM_AnioModelo {get; set;}
        @AuraEnabled public String TAM_codigoModelo {get; set;}
        @AuraEnabled public String NombreDetalleBono {get; set;}
        @AuraEnabled public decimal  TAM_BonoDealerEfectivo {get; set;}
        @AuraEnabled public decimal  TAM_BonoTFSEfectivo {get; set;}
        @AuraEnabled public decimal  TAM_BonoTMEXEfectivo {get; set;}
        @AuraEnabled public decimal TAM_BonoDealerProdFinanciero {get; set;}
        @AuraEnabled public decimal TAM_BonoTotalEfectivo {get; set;}
        @AuraEnabled public decimal TAM_BonoTotalProductoFinanciero {get; set;}
        @AuraEnabled public decimal TAM_BonoTotalProductoFinancieroTMEX {get; set;}
        @AuraEnabled public decimal TAM_BonoTotalProductoFinancieroTFS {get; set;}
        @AuraEnabled public String TAM_Clave {get; set;}
        @AuraEnabled public String TAM_ComentariosEfectivo {get; set;}
        @AuraEnabled public String TAM_DescripcionProdFinanciero {get; set;}
        @AuraEnabled public decimal TAM_DiasVenta {get; set;}
        @AuraEnabled public decimal TAM_InventarioDealer {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroEfectivoTMEX {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroEfectivoDealer {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroEfectivoTFS {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroProductoFinancieroTMEX {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroProductoFinancieroDealer {get; set;}
        @AuraEnabled public decimal TAM_CantidadDineroProductoFinancieroTFS {get; set;}  
        @AuraEnabled public decimal TAM_PorcentajeTotalEfectivo{get; set;}  
        @AuraEnabled public String TAM_ProductoFinanciero {get; set;}
        @AuraEnabled public String TAM_Serie {get; set;}
        @AuraEnabled public String TAM_Version {get; set;}
        @AuraEnabled public String TAM_codigoDealer {get; set;} 
        @AuraEnabled public String TAM_nombreDealer {get; set;}
        @AuraEnabled public Boolean TAM_cumpleConCondiciones {get; set;} 
        @AuraEnabled public String TAM_fechaVenta {get; set;}
        @AuraEnabled public String TAM_comentarioDelaer {get; set;}
        @AuraEnabled public String TAM_bonoSolicitado {get; set;}
        @AuraEnabled public String TAM_BonoCorrespondiente {get; set;}
        @AuraEnabled public String TAM_estatusVIN {get; set;}
        @AuraEnabled public String TAM_bonoElegido {get; set;}
        @AuraEnabled public String TAM_codigoVenta {get; set;}
        @AuraEnabled public String condicionesBonoEfectivo {get; set;}
        @AuraEnabled public String condicionesBonoProductoFinaciero {get; set;}
        
        
    }
    
}