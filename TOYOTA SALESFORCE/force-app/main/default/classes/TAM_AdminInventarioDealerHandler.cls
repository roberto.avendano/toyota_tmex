/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para prender el proceso TAM_ActCancelSolInvSch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    14-Julio-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_AdminInventarioDealerHandler extends TriggerHandler{
    
    private List<TAM_AdminInventarioDealer__c> adminInvList;
    private Map<Id,TAM_AdminInventarioDealer__c> mapAdminInvent;
    
    public TAM_AdminInventarioDealerHandler() {
        this.adminInvList = Trigger.new;
        this.mapAdminInvent = (Map<Id,TAM_AdminInventarioDealer__c>) Trigger.oldMap;
    }

    public override void afterUpdate(){
        activaProceso(adminInvList, mapAdminInvent);
    }
    
    public void activaProceso(List<TAM_AdminInventarioDealer__c> adminInvList,
        Map<Id,TAM_AdminInventarioDealer__c> mapAdminInvent){
        System.debug('EN TAM_AdminInventarioDealerHandler.activaProceso...');
        
        //Recorre la lista de adminInvList
        for (TAM_AdminInventarioDealer__c objAdminInvPaso : adminInvList){
            System.debug('EN TAM_AdminInventarioDealerHandler.activaProceso objAdminInvPaso: ' + objAdminInvPaso.TAM_ActivaProceso__c + ' mapAdminInvent.get(objAdminInvPaso.id).TAM_ActivaProceso__c: ' + mapAdminInvent.get(objAdminInvPaso.id).TAM_ActivaProceso__c);
            if (objAdminInvPaso.TAM_ActivaProceso__c && !mapAdminInvent.get(objAdminInvPaso.id).TAM_ActivaProceso__c){
                //Crea el prioceso para 

	            string strSeconds2 = '0';
	            string strMinutes2 = objAdminInvPaso.TAM_Minutos__c; //'30';
	            string strHours2 = objAdminInvPaso.TAM_Hora__c; //'8';
	            string strDay_of_month2 = '?';
	            string strMonth2 = '*';
	            string strDay_of_week2 = '*';
	            string strYear2 = '*';
	            String sch2 = strSeconds2 + ' ' + strMinutes2 + ' ' + strHours2 + ' ' + strDay_of_month2 + ' ' + strMonth2 + ' ' + strDay_of_week2 + ' ' + strYear2;

                //Llama al proceso solo si no es una prueba
                TAM_ActTotVtaInvSch_cls ActTotVtaInvSch_Jobh = new TAM_ActTotVtaInvSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_ActTotVtaInvSch_cls' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_ActTotVtaInvSch_cls' + strHours2 + '-' + strMinutes2, sch2, ActTotVtaInvSch_Jobh);	            
	            //Llama al proceso solo si no es una prueba
                TAM_ActCancelSolInvSch_cls ActCancelSolInvSch_Jobh = new TAM_ActCancelSolInvSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_ActCancelSolInvSch_cls' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_ActCancelSolInvSch_cls:' + strHours2 + '-' + strMinutes2, sch2, ActCancelSolInvSch_Jobh);          
                TAM_ConsMovDDSolInvSch_cls ConsMovDDSolInvSch_Jobh = new TAM_ConsMovDDSolInvSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_ConsMovDDSolInvSch_cls' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_ConsMovDDSolInvSch_cls' + strHours2 + '-' + strMinutes2, sch2, ConsMovDDSolInvSch_Jobh);          
                TAM_SirenaWebServiceSch SirenaWebServiceSch_Jobh = new TAM_SirenaWebServiceSch();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_SirenaWebServiceSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_SirenaWebServiceSch' + strHours2 + '-' + strMinutes2, sch2, SirenaWebServiceSch_Jobh);          
                TAM_ActFechaDDChkOutSch_cls ActFechaDDChkOutSch_Jobh = new TAM_ActFechaDDChkOutSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_ActFechaDDChkOutSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_ActFechaDDChkOutSch' + strHours2 + '-' + strMinutes2, sch2, ActFechaDDChkOutSch_Jobh);          
                TAM_CancelSolPedEspOtroDistSch_cls CancelSolPedEspOtroDistSch_Jobh = new TAM_CancelSolPedEspOtroDistSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_CancelSolPedEspOtroDistSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_CancelSolPedEspOtroDistSch' + strHours2 + '-' + strMinutes2, sch2, CancelSolPedEspOtroDistSch_Jobh);          
                TAM_ActCteVehicSch_cls ActCteVehicSch_Jobh = new TAM_ActCteVehicSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_ActCteVehicSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_ActCteVehicSch' + strHours2 + '-' + strMinutes2, sch2, ActCteVehicSch_Jobh);          
                
                //Para los procesos que hacen el MERGE de los clientes
                TAM_MergeShareAccountsSch MergeShareAccountsSch_Jobh = new TAM_MergeShareAccountsSch();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_MergeShareAccountsSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_MergeShareAccountsSch' + strHours2 + '-' + strMinutes2, sch2, MergeShareAccountsSch_Jobh);          
                TAM_MergeShareOppSch MergeShareOppSch_Jobh = new TAM_MergeShareOppSch();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_MergeShareOppSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_MergeShareOppSch' + strHours2 + '-' + strMinutes2, sch2, MergeShareOppSch_Jobh);    

	            //Los otros objetos
                TAM_MergeContactosSch_cls TAM_MergeContactosSch_Jobh = new TAM_MergeContactosSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_MergeContactosSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_MergeContactosSch' + strHours2 + '-' + strMinutes2, sch2, TAM_MergeContactosSch_Jobh);
                TAM_MergeVehiculosSch_cls MergeVehiculosSch_Jobh = new TAM_MergeVehiculosSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_MergeVehiculosSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_MergeVehiculosSch' + strHours2 + '-' + strMinutes2, sch2, MergeVehiculosSch_Jobh);
                TAM_MergePropietarioSch_cls MergePropietarioSch_Jobh = new TAM_MergePropietarioSch_cls();
                if (objAdminInvPaso.TAM_Proceso__c == 'TAM_MergePropietarioSch' || Test.isRunningTest())
                    if (!Test.isRunningTest())
                        System.schedule('Ejecuta_TAM_MergePropietarioSch' + strHours2 + '-' + strMinutes2, sch2, MergePropietarioSch_Jobh);
	            
            }//Fin si objAdminInvPaso.TAM_ActivaProceso__c && !mapAdminInvent.get(objAdminInvPaso.id).TAM_ActivaProceso__c
        }//Fin del for para adminInvList
        
    }
    
}