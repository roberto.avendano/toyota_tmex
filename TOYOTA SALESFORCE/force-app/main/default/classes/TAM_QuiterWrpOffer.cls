/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de offer para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_QuiterWrpOffer {

    public class QuiterOffer{
        @AuraEnabled public String id {get;set;}        
        @AuraEnabled public String IDV {get;set;}    
        @AuraEnabled public String model {get;set;}
        @AuraEnabled public String codColor {get;set;}
        @AuraEnabled public String upholstery {get;set;}

        @AuraEnabled public String client {get;set;}    
        @AuraEnabled public String subOrigin {get;set;}        
        @AuraEnabled public String externalId {get;set;}
        @AuraEnabled public Date dateFinal {get;set;}
        @AuraEnabled public Date dateOfferClient {get;set;}
        @AuraEnabled public String offerNumber {get;set;}
        @AuraEnabled public String offerObservat {get;set;}

        @AuraEnabled public Decimal totalOffer {get;set;}   
        @AuraEnabled public Decimal totalValue {get;set;}   

        @AuraEnabled public String dealer {get;set;}     //sAgenciaFact
        @AuraEnabled public String origin {get;set;}     //sOrigemFact
        @AuraEnabled public String seller {get;set;}   //sAsesorVentasFact
        @AuraEnabled public String type {get;set;}   //sTipoOfertaFact     
        @AuraEnabled public String typeSale {get;set;}   //sTipoVentaFact     
        @AuraEnabled public String status {get;set;}   //sEdoPedInicialFact
        
        //Un contructor por default
        public QuiterOffer(){
            this.id = '';
            this.IDV = '';
            this.codColor = ''; 
            this.upholstery = '';
            this.model = '';
            this.client = '';

            this.externalId = '';
            this.dateOfferClient = Date.today(); 
            this.offerNumber = '';
            this.offerObservat = '';

            this.totalOffer = 0.00;
            this.totalValue = 0.00; 

            this.dealer = '';
            this.origin = '';
            this.seller = '';
            this.type = '';
            this.typeSale = '';
            this.status = '';
        }        
    }
    
}