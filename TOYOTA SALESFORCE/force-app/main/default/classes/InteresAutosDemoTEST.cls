/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class InteresAutosDemoTEST {

   @isTest static  void test() {
        // TO DO: implement unit test
        RecordType rtParte = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='Parte' and IsActive=true];
        RecordType rtProductoFinal = [Select Id FROM RecordType WHERE SobjectType='Product2' and DeveloperName='ProductoFinal' and IsActive=true];
        RecordType rtDealer = [Select Id FROM RecordType WHERE SobjectType='Account' and DeveloperName='Dealer' and IsActive=true];
        
        Id standardPricebookId = Test.getStandardPricebookId();

        
         ParametrosConfiguracionToyota__c pc = new  ParametrosConfiguracionToyota__c(
            Name='PRListaPreciosDestino',
            Valor__c='PRListaPreciosDestino',
            Consecutivo__c = 999
        );
        insert pc;

        Product2 miProd= new Product2();
            miProd.Name='Test001';
            miProd.IdExternoProducto__c='Test001';
            miProd.ProductCode='Test001';
            miProd.Description= 'Test product2';
            miProd.CantidadMaxima__c = 5;
            miProd.PartesRobadas__c = true;
            miProd.RecordTypeId = rtParte.Id;
            miProd.IsActive = true;
        insert miProd;

        Product2 miProdFinal = new Product2();
            miProdFinal.Name='Test02';
            miProdFinal.IdExternoProducto__c='Test02';
            miProdFinal.ProductCode='Test02';
            miProdFinal.Description= 'Test product2';
            miProdFinal.CantidadMaxima__c = 5;
            miProdFinal.PartesRobadas__c = true;
            miProdFinal.RecordTypeId = rtProductoFinal.Id;
            miProdFinal.IsActive = true;

        insert miProdFinal;
        
        ProductoFinalComponente__c pcFinal = new ProductoFinalComponente__c(
            Producto__c = miProd.Id,
            ProductoFinal__c = miProdFinal.Id,
            Name='Test'
        );
        insert pcFinal;

        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IdExternoListaPrecios__c='testId3', 
            isActive=true
        );
        insert customPB;

        PricebookEntry pbePro1Estandard = new PricebookEntry();
            pbePro1Estandard.Pricebook2Id= standardPricebookId;
            pbePro1Estandard.Product2Id= miProd.Id;
            pbePro1Estandard.UnitPrice= 0.0;
            pbePro1Estandard.IdExterno__c = 'expbe1';
            pbePro1Estandard.IsActive = true;
        insert pbePro1Estandard; 

        PricebookEntry pbePro1CPB = new PricebookEntry(
                Pricebook2Id= customPB.Id,
                Product2Id= miProd.Id,
                UnitPrice= 0.0,
                IdExterno__c = 'expbe2',
                IsActive = true
            );
        insert pbePro1CPB; 

        PricebookEntry pbePro2Estandard= new PricebookEntry();
            pbePro2Estandard.Pricebook2Id= standardPricebookId;
            pbePro2Estandard.Product2Id= miProdFinal.Id;
            pbePro2Estandard.UnitPrice= 0.0;
            pbePro2Estandard.IdExterno__c = 'expbe3';
        insert pbePro2Estandard;  

        PricebookEntry pbePro2CPB= new PricebookEntry();
            pbePro2CPB.Pricebook2Id= customPB.Id;
            pbePro2CPB.Product2Id= miProdFinal.Id;
            pbePro2CPB.UnitPrice= 0.0;
            pbePro2CPB.IdExterno__c = 'expbe4';
        insert pbePro2CPB;    
        
        
        
        
        Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        
        MotivoRechazo__c motivo = new MotivoRechazo__c(
            Tipo__c= Constantes.CASE_STATUS_AUTORIZACION_BAJA_TMEX
        );
        insert motivo;
        
        Account daltonDealer = new Account(
            Name='Toyota Dalton López Mateos',
            Codigo_Distribuidor__c = '57055',
            UnidadesAutosDemoAutorizadas__c = 15
        );
        insert daltonDealer;
  		
        Serie__c serie = new Serie__c(
            Name = 'YARIS SD'
         );
        insert serie;
        Modelo__c modelo = new Modelo__c(
            Name = '1091 - Yaris Sedan',
            Tipo_de_vehiculo__c = 'Camioneta',
            ID_Modelo__c = '1091',
            Serie__c = serie.Id
        );
        insert modelo;
        
        Vehiculo__c vin1= new Vehiculo__c(
            Name='MHKMF53EXHK007709',          
            Distribuidor__c=daltonDealer.Id,
            Modelo_b__c = modelo.Id,
            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id            
        );
        insert vin1;
        
        Vehiculo__c vin2= new Vehiculo__c(
            Name='3MYDLAYV0HY167430',          
            Distribuidor__c=daltonDealer.Id,
            Modelo_b__c = modelo.Id,
            Producto__c = miProdFinal.Id,
            RecordTypeId= recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id     
        );
        insert vin2;
        
        PeriodoFiscal__c pf2017 = new PeriodoFiscal__c(
            Name = '2017',
            IdPeriodoFiscal__c = '2018',
            FechaInicioPeriodoFiscal__c = System.today() - 30,
            FechaFinPeriodoFiscal__c = System.today() +30 			
        );
        insert pf2017;
        
        PeriodoFiscal__c pf2018 = new PeriodoFiscal__c(
            Name = '2018',
            IdPeriodoFiscal__c = '2018',
            FechaInicioPeriodoFiscal__c = Date.newInstance(2018, 7, 1),
            FechaFinPeriodoFiscal__c = Date.newInstance(2018, 8, 20)			
        );
        insert pf2018;

        CostoSerie__c costoSerie = new CostoSerie__c(  			 
  			Serie__c = serie.Id,
  			FY__c = pf2018.Id,
  			Enero__c = 1000, 
  			Febrero__c = 2000,
  			Marzo__c = 800,
  			Abril__c = 900,
  			Mayo__c = 1200,
  			Junio__c = 300,  
  			Julio__c = 200, 
		 	Agosto__c = 20000,
  			Septiembre__c = 500, 
  			Octubre__c = 700,
  			Noviembre__c = 900, 
  			Diciembre__c = 700 
		);
		insert costoSerie;
		

        serie.InteresSerieActiva__c = costoSerie.Id;
        serie.InteresSerieProximoFY__c = costoSerie.Id;
        update serie;
        miProdFinal.Serie__c = serie.Id;
        update miProdFinal;
        
         System.debug(' ##### miProdFinal '+miProdFinal.Serie__c);
          System.debug(' ##### miProdFinal2 '+miProdFinal.Serie__r.InteresSerieActiva__c);
        Presupuesto__c p2017 = new Presupuesto__c(
            PeriodoFiscal__c = pf2017.Id,
            RecordTypeId= recordTypesMap.get('Presupuesto__c').get('AutosDemo').Id,
            PresupuestoAutorizadoFinanzas__c = true,
            PresupuestoAutorizadoEnero__c = 10000, 
            PresupuestoAutorizadoFebrero__c = 2000, 
            PresupuestoAutorizadoMarzo__c = 200, 
            PresupuestoAutorizadoAbril__c = 10000, 
            PresupuestoAutorizadoMayo__c = 10000, 
            PresupuestoAutorizadoJunio__c = 10000, 
            PresupuestoAutorizadoJulio__c = 10000,
            PresupuestoAutorizadoAgosto__c = 10000, 
            PresupuestoAutorizadoSeptiembre__c = 10000, 
            PresupuestoAutorizadoOctubre__c = 10000,
            PresupuestoAutorizadoNoviembre__c = 10000, 
            PresupuestoAutorizadoDiciembre__c = 10000            
        );
        insert p2017;
        
        Presupuesto__c p2018 = new Presupuesto__c(
            PeriodoFiscal__c = pf2018.Id,
            RecordTypeId= recordTypesMap.get('Presupuesto__c').get('AutosDemo').Id,
            PresupuestoAutorizadoFinanzas__c = true,
            Activo__c=true,
            PresupuestoAutorizadoEnero__c = 20000, 
            PresupuestoAutorizadoFebrero__c = 2000000, 
            PresupuestoAutorizadoMarzo__c = 20000, 
            PresupuestoAutorizadoAbril__c = 20000, 
            PresupuestoAutorizadoMayo__c = 20000, 
            PresupuestoAutorizadoJunio__c = 20000, 
            PresupuestoAutorizadoJulio__c = 20000,
            PresupuestoAutorizadoAgosto__c = 20000, 
            PresupuestoAutorizadoSeptiembre__c = 20000, 
            PresupuestoAutorizadoOctubre__c = 20000,
            PresupuestoAutorizadoNoviembre__c = 20000, 
            PresupuestoAutorizadoDiciembre__c = 20000            
        );
        insert p2018;	
        System.debug(JSON.serialize(p2018));
        AsignacionPresupuesto__c ap = new AsignacionPresupuesto__c(
            Distribuidor__c = daltonDealer.Id,
            Presupuesto__c = p2017.Id,			
            AsignadoEnero__c = 2000, 
            AsignadoFebrero__c = 1500, 
            AsignadoMarzo__c = 1200, 
            AsignadoAbril__c = 1100,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 1500,
            AsignadoJulio__c = 1400, 
            AsignadoAgosto__c = 2300,
            AsignadoSeptiembre__c = 4500,
            AsignadoOctubre__c = 3400,
            AsignadoNoviembre__c = 7800,
            AsignadoDiciembre__c = 4500
        );
        insert ap;
        
        
        AsignacionPresupuesto__c ap2 = new AsignacionPresupuesto__c(
            Distribuidor__c = daltonDealer.Id,
            Presupuesto__c = p2018.Id,						
            AsignadoEnero__c = 2000, 
            AsignadoFebrero__c = 1500, 
            AsignadoMarzo__c = 1200, 
            AsignadoAbril__c = 1100,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 1500,
            AsignadoJulio__c = 1400, 
            AsignadoAgosto__c = 2300,
            AsignadoSeptiembre__c = 4500,
            AsignadoOctubre__c = 3400,
            AsignadoNoviembre__c = 7800,
            AsignadoDiciembre__c = 4500
        );
        insert ap2;
         System.debug(' ##### asfigancion1 '+ap2.Presupuesto__c);
        System.debug(' ##### asfigancion '+ap2.Presupuesto__r.PeriodoFiscal__r.FechaInicioPeriodoFiscal__c);
        SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c(
            Estatus__c = 'Alta',
            VIN__c = vin2.Id,
            FechaAltaAutoDemo__c = System.today() - 30,
            FechaBajaPagoCompletado__c = System.today()+40,
            Distribuidor__c = daltonDealer.Id
        );

        insert sad;

		List<CortePresupuesto__c> lst =  new List<CortePresupuesto__c> ();      
       CortePresupuesto__c c = new CortePresupuesto__c(
            AsignacionPresupuesto__c = ap2.Id,
            SolicitudAutoDemo__c = sad.Id,
            InicioCortePresupuesto__c = System.today() - 32,
            FinCortePresupuesto__c = System.today() -3, 
            IDExterno__c = '- Julio',
            Mes__c = 'Julio',
            MontoAsignado__c = null,
               CostoPromedio__c = 100.00
        );  
       CortePresupuesto__c c2 = new CortePresupuesto__c(
            AsignacionPresupuesto__c = ap2.Id,
            SolicitudAutoDemo__c = sad.Id,
            InicioCortePresupuesto__c = System.today() - 2,
            FinCortePresupuesto__c = System.today() + 29, 
            IDExterno__c = '- Agosto',
            Mes__c = 'Agosto',
            MontoAsignado__c = 0.0,
            CostoPromedio__c = 100.00
        );
       CortePresupuesto__c c3 = new CortePresupuesto__c(
            AsignacionPresupuesto__c = ap2.Id,
            SolicitudAutoDemo__c = sad.Id,
            InicioCortePresupuesto__c = System.today() +30,
            FinCortePresupuesto__c = System.today() + 59, 
            IDExterno__c = '- Septiembre',
            Mes__c = 'Septiembre',
            MontoAsignado__c = 0.0,
            CostoPromedio__c = 100.00
        );  
        
        lst.add(c);
        lst.add(c2);
        lst.add(c3);
        insert lst;
      Test.startTest();
        
        Set<String>idsCorte =  new Set<String>();
        for (CortePresupuesto__c con: lst) {
        	idsCorte.add(con.Id);
        }
        
        List<CortePresupuesto__c> lstCortes = [Select SolicitudAutoDemo__c,MontoAsignado__c,SolicitudAutoDemo__r.VIN__r.Producto__c,InicioCortePresupuesto__c,FinCortePresupuesto__c,SolicitudAutoDemo__r.FechaAltaAutoDemo__c,AsignacionPresupuesto__r.InicioFY__c,
						SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieActiva__c,SolicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c,SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieActiva__r.InteresPromedio__c, MontoMensual__c,
						SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__r.InteresPromedio__c , AsignacionPresupuesto__r.FinFY__c,
						SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__c,   CostoPromedio__c
					From CortePresupuesto__c where Id in:idsCorte];
        
         InteresAutosDemo.montosAutoDFYA(lstCortes);
         
         List<SolicitudAutoDemo__c> lstSol =  new List<SolicitudAutoDemo__c>();
         sad.Estatus__c = 'Baja por pago completado sin solicitud';
         sad.Distribuidor__c = daltonDealer.Id;
         update sad;
        	lstSol.add(sad);
         
         Set<String>idsSolictud =  new Set<String>();
        for (solicitudAutoDemo__c sol: lstSol) {
        	idsSolictud.add(sol.Id);
        }
          
 		List<CortePresupuesto__c> lstCortess = [Select InicioCortePresupuesto__c,FinCortePresupuesto__c, MontoAsignado__c,
			 solicitudAutoDemo__r.FechaBajaPagoCompletado__c,solicitudAutoDemo__r.EstatusAutoDemo__c,MontoMensual__c
			From CortePresupuesto__c where solicitudAutoDemo__c in:idsSolictud And solicitudAutoDemo__r.EstatusAutoDemo__c = 'Inactivo'];
        InteresAutosDemo.montosAutoPAGOCompletoSolicitudes(lstSol);
    Test.StopTest();

    }
}