/**
    Descripción General: Controlador para el componente "TAM_Bono"
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        19/Mayo/2020         Versión Inicial
        Cecilia Cruz        23/Agosto/2021       Modo solo lectura para usuarios de auditoria
    ________________________________________________________________
**/
public without sharing class TAM_BonoControllerClass {
    
 /*Obtener información de Detalle de Bonos*/
    @AuraEnabled
    public static Map<String, List<TAM_DetalleBonos__c>> getDetalleBonos(String recordId) {
        Map<String, List<TAM_DetalleBonos__c>>  mapDetalle = new Map<String, List<TAM_DetalleBonos__c>>();
        List<TAM_DetalleBonos__c> lstDetalleAux = new List<TAM_DetalleBonos__c>();
        
    	List<TAM_DetalleBonos__c> lstDetalle = [SELECT 	Id,
                                                		TAM_AnioModelo__c,
                                                		TAM_BonoDealerEfectivo__c,
                                                		TAM_BonoDealerProdFinanciero__c,
                                                		TAM_BonoTFSProdFinanciero__c,
                                                		TAM_BonoTMEXEfectivo__c,
                                                		TAM_BonoTMEXProdFinanciero__c,
                                                		TAM_BonoTotalProductoFinanciero__c,
                                                		TAM_BonoTotalEfectivo__c,
                                                		TAM_Clave__c,
                                                		TAM_ComentariosEfectivo__c,
                                                		TAM_DescripcionProdFinanciero__c,
                                                		TAM_DiasVenta__c,
                                                		TAM_InventarioDealer__c,
                                                		TAM_ProductoFinanciero__c,
                                                		TAM_Serie__c,
                                                		TAM_Version__c,
                                                        TAM_VentasMY__c,
                                                        TAM_BalanceOut__c,
                                                        TAM_AplicaAmbosBonos__c,
                                                        TAM_BonoTFSEfectivo__c,
                                                        TAM_CondicionesBonoEfectivo__c,
                                                        TAM_CondicionesProdFinanciero__c
                                               	FROM	TAM_DetalleBonos__c
                                               	WHERE	Bono__c =:recordId
                                               	ORDER BY TAM_Serie__c ASC, TAM_Clave__c ASC];
        
        //Mapa Detalle
        for(TAM_DetalleBonos__c objDetalle : lstDetalle){
            String strAnioandModelo = objDetalle.TAM_Serie__c + objDetalle.TAM_AnioModelo__c;
            lstDetalleAux = mapDetalle.get(strAnioandModelo);

            if(mapDetalle.containsKey(strAnioandModelo)){
               
                if(!lstDetalleAux.contains(objDetalle)){
                    
                    lstDetalleAux.add(objDetalle);
                    mapDetalle.put(strAnioandModelo, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<TAM_DetalleBonos__c>();
                lstDetalleAux.add(objDetalle);
                mapDetalle.put(strAnioandModelo, lstDetalleAux);
            }
        }
        return mapDetalle;
    }
    
    /*Obtener mapa de series disponibles*/
	@AuraEnabled
    public static Map<String, List<String>> getSeries(String recordId) {
        Map<String, List<String>>  mapSeries = new Map<String, List<String>>();
        List<String> lstDetalleAux = new List<String>();
        List<TAM_DetalleBonos__c> lstDetalle = [SELECT 	TAM_AnioModelo__c,
                                                		TAM_Serie__c
                                                FROM	TAM_DetalleBonos__c
                                                WHERE 	Bono__c =:recordId
                                                ORDER BY	TAM_Serie__c ASC];
        
        //Mapa Detalle
        for(TAM_DetalleBonos__c objDetalle : lstDetalle){
            lstDetalleAux = mapSeries.get(objDetalle.TAM_AnioModelo__c);

            if(mapSeries.containsKey(objDetalle.TAM_AnioModelo__c)){
               
                if(!lstDetalleAux.contains(objDetalle.TAM_Serie__c)){
                    
                    lstDetalleAux.add(objDetalle.TAM_Serie__c);
                    mapSeries.put(objDetalle.TAM_AnioModelo__c, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<String>();
                lstDetalleAux.add(objDetalle.TAM_Serie__c);
                mapSeries.put(objDetalle.TAM_AnioModelo__c, lstDetalleAux);
            }
        }
        return mapSeries;
    }

    /*Guardar Detalle de Bonos*/
    @AuraEnabled
    public static boolean guardarDetalleBonos(List<TAM_DetalleBonos__c> lstDetalleBonos, String recordId){
        List<TAM_DetalleBonos__c> lstAUX = new List<TAM_DetalleBonos__c>();

        for (TAM_DetalleBonos__c objDetalleBonos  : lstDetalleBonos){
            TAM_DetalleBonos__c objDetalleAux = new TAM_DetalleBonos__c();

            objDetalleAux.TAM_VentasMY__c = objDetalleBonos.TAM_VentasMY__c;
            objDetalleAux.TAM_BonoTotalEfectivo__c = objDetalleBonos.TAM_BonoTotalEfectivo__c;
            objDetalleAux.TAM_BonoTMEXEfectivo__c = objDetalleBonos.TAM_BonoTMEXEfectivo__c;
            objDetalleAux.TAM_BonoTFSEfectivo__c = objDetalleBonos.TAM_BonoTFSEfectivo__c;
            objDetalleAux.TAM_BonoDealerEfectivo__c = objDetalleBonos.TAM_BonoDealerEfectivo__c;
            objDetalleAux.TAM_ComentariosEfectivo__c = objDetalleBonos.TAM_ComentariosEfectivo__c;
            objDetalleAux.TAM_ProductoFinanciero__c = objDetalleBonos.TAM_ProductoFinanciero__c;
            objDetalleAux.TAM_DescripcionProdFinanciero__c = objDetalleBonos.TAM_DescripcionProdFinanciero__c;
            objDetalleAux.TAM_BonoTotalProductoFinanciero__c = objDetalleBonos.TAM_BonoTotalProductoFinanciero__c;
            objDetalleAux.TAM_BonoTMEXProdFinanciero__c = objDetalleBonos.TAM_BonoTMEXProdFinanciero__c;
            objDetalleAux.TAM_BonoTFSProdFinanciero__c = objDetalleBonos.TAM_BonoTFSProdFinanciero__c;
            objDetalleAux.TAM_BonoDealerProdFinanciero__c = objDetalleBonos.TAM_BonoDealerProdFinanciero__c;
            objDetalleAux.TAM_AplicaAmbosBonos__c = objDetalleBonos.TAM_AplicaAmbosBonos__c;
            objDetalleAux.TAM_BalanceOut__c = objDetalleBonos.TAM_BalanceOut__c;
            objDetalleAux.TAM_IdExterno__c = objDetalleBonos.TAM_AnioModelo__c +'-'+ objDetalleBonos.TAM_Serie__c +'-'+ objDetalleBonos.TAM_Clave__c +'-'+ recordId;
            objDetalleAux.TAM_CondicionesBonoEfectivo__c = objDetalleBonos.TAM_CondicionesBonoEfectivo__c;
            objDetalleAux.TAM_CondicionesProdFinanciero__c = objDetalleBonos.TAM_CondicionesProdFinanciero__c;

            lstAUX.add(objDetalleAux);
        }
        try {
            Database.upsert(lstAUX, TAM_DetalleBonos__c.TAM_IdExterno__c, false);
            return true; 
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return false; 
        }

    }

    /*Validar usuario de Auditoria*/
    @AuraEnabled 
    public static Boolean getCurrentUser (String userId){
        Boolean boolUsuarioLectura = false;
        List<Profile> profileId = [SELECT Id FROM Profile WHERE Name='Auditor Finanzas Platform' LIMIT 1];
        User objUser = [SELECT id, ProfileId  FROM User WHERE Id =: userId];
        if(objUser.ProfileId == profileId[0].Id){
            boolUsuarioLectura = true;
        }
        return boolUsuarioLectura;
    }
}