public class VehiculoSIVTriggerHandler extends TriggerHandler {
    private List<VehiculoSIV__c> vehiculosSIVList;    


    public VehiculoSIVTriggerHandler(){
        this.vehiculosSIVList = (List<VehiculoSIV__c>) Trigger.new;
    }
    
    public override void afterInsert(){
        validaPoliticasPool(vehiculosSIVList);
        //cancelaVentaEspacial(vehiculosSIVList);
    }
    public override void afterUpdate(){
        validaPoliticasPool(vehiculosSIVList);
        //cancelaVentaEspacial(vehiculosSIVList);
    }

    /*//Metodo para cancelar las ventas espaciales que cambiaron de tipo de venta y de cliente
    private void cancelaVentaEspacial(List<VehiculoSIV__c> ventasHList){
        System.debug('EM cancelaVentaEspacial...');     
        
        Date dFechaCancelaFinal = Date.today();
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();        
        Map<String, VehiculoSIV__c> vines = new Map<String, VehiculoSIV__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVinCheckout = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new Map<String, TAM_LeadInventarios__c>();
        Map<String, List<Movimiento__c>> mapVinLstMov = new Map<String, List<Movimiento__c>>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdCheckoutObjUps = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        for(VehiculoSIV__c v: ventasHList){
            if (v.Name != null)
                vines.put(v.Name, v);
        }//Fin del for para los vines
        System.debug('EM cancelaVentaEspacial vines: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial vines: ' + vines.values());     
        
        //Consulta los mov asociados a los vines vines.keySet()
        for (Movimiento__c objMovPaso : [Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c,  
                Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, 
                Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c
                From Movimiento__c Where RecordTypeId =: strRecordTypeId
                And VIN__r.Name IN :vines.keySet()]){
            //Mete los mov en el mapa de mapVinLstMov
            if (mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
                mapVinLstMov.get(objMovPaso.VIN__r.Name).add(objMovPaso);            
            if (!mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
                mapVinLstMov.put(objMovPaso.VIN__r.Name, new List<Movimiento__c>{objMovPaso});
        }
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.values());     
        
        //Consulta todas las solicitudes de Pedido especial asociada a estos vines
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso : [Select t.id, t.TAM_VIN__c, 
            t.TAM_SolicitudFlotillaPrograma__c, t.TAM_NombreDistribuidor__c 
            From TAM_CheckOutDetalleSolicitudCompra__c t
            Where TAM_TipoVenta__c != 'Inventario' And TAM_EstatusDOD__c != 'Cancelado' 
            And TAM_EstatusDealerSolicitud__c != 'Cancelada'And TAM_EstatusDealerSolicitud__c != 'Rechazada']){
            //Metelo al mapa de mapVinCheckout
            mapVinCheckout.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso);
        }
        System.debug('EM cancelaVentaEspacial mapVinCheckout: ' + mapVinCheckout.keySet());     
        System.debug('EM cancelaVentaEspacial mapVinCheckout: ' + mapVinCheckout.values());     
        
        //Reorre la lista de vines y obten su ultimo mov
        for (String sVinPaso : vines.keySet()){
            //Busca el vin en mapVinLstMov
            if (mapVinLstMov.containsKey(sVinPaso)){
                Movimiento__c objMvPaso = new Movimiento__c();
                List<Movimiento__c> lstMov = mapVinLstMov.get(sVinPaso);
                //Busca su ultimo mov en TAM_ActTotVtaInvBch_cls.getUltMov();
                if (lstMov.size() > 1)
                    objMvPaso =  TAM_ActTotVtaInvBch_cls.getUltMov(lstMov);
                if (lstMov.size() == 1)
                    objMvPaso =  lstMov.get(1);
                //Ve si el VIN esta en el para de mapVinCheckout asociado a una solicutude de pedido especial
                if (mapVinCheckout.containsKey(sVinPaso)){
                    TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = mapVinCheckout.get(sVinPaso);
                    //Ve si el ultimo mov es un pedido especial y cambio a INVENTARIO y ademas esta rechazado
                    if (objMvPaso.Fleet__c == 'N' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c){
                        //Agregalo al mapa de mapIdCheckoutObjUps 
                        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPasoUpd = new TAM_CheckOutDetalleSolicitudCompra__c(
                                Id = objCheckOutPaso.id, TAM_ListaParaCancelar__c = true,
                                TAM_EstatusDealerSolicitud__c = 'Cancelada',
                                TAM_FechaCancelacion__c = dFechaCancelaFinal,
                                TAM_VIN__c = objCheckOutPaso.TAM_VIN__c
                        );
                        //Agregar el objeto al mapa de mapIdCheckoutObjUps
                        mapIdCheckoutObjUps.put(objCheckOutPaso.id, objCheckOutPasoUpd);
                        //Crea el registro en el log de errores                
                        lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutPaso.id + '-' + objCheckOutPaso.TAM_VIN__c, TAM_VIN__c = objCheckOutPaso.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Pedio Especial Cambio Fleet', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutPaso) );            
                    }//Fin si  objMvPaso.Fleet__c == 'N' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c
                }//Fin si mapVinCheckout.containsKey(sVinPaso)
            }//Fin si mapVinLstMov.containsKey(sVinPaso)
        }//Fin del for para vines.keySet()
        System.debug('EN cancelaVentaEspacial mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.keyset());
        System.debug('EN cancelaVentaEspacial mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.values());
        System.debug('EN cancelaVentaEspacial lError: ' + lError);
    
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdCheckoutObjUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdCheckoutObjUps.values(), TAM_CheckOutDetalleSolicitudCompra__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN VehiculoSIVTriggerHandler Hubo un error a la hora de crear/Actualizar los registros en Checkout ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Crea los reg en el objeto de Log
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);

        //Roleback a todo
        Database.rollback(sp);
        
    }*/
    
    public void validaPoliticasPool(List<VehiculoSIV__c> vinesSIV){
    	System.debug('Entro a valida politicas');
    	Map<Id,VehiculoSIV__c> mapSIV = new Map<Id, VehiculoSIV__c>();
    	Map<Id,List<String>> mapVinPuestos = new Map<Id, List<String>>();
    	Map<String,PoliticaAutosPoolAsignados__c> mapPoliticasUpd = new Map<String,PoliticaAutosPoolAsignados__c>();

    	for(VehiculoSIV__c vinSIV : vinesSIV){
    		if(vinSIV.VehiculoDisponiblePuestos__c !=null&& vinSIV.VehiculoDisponiblePuestos__c!=''){
    			mapSIV.put(vinSIV.Id, vinSIV);
    			System.debug('Antes de error: '+mapVinPuestos);
    			mapVinPuestos.put(vinSIV.Id,vinSIV.VehiculoDisponiblePuestos__c.split(';'));
    			System.debug('Despues de error: '+mapVinPuestos);
    		}
    	}
    	System.debug('Mapa SIV: ' +mapSIV);
    	System.debug('Mapa SIV y puestos: ' +mapVinPuestos);
    	
		Map<String,PoliticaAutosPoolAsignados__c> mapPoliticas = new Map<String, PoliticaAutosPoolAsignados__c>();
		List<PoliticaAutosPoolAsignados__c> politicas = new List<PoliticaAutosPoolAsignados__c>();
    	for(PoliticaAutosPoolAsignados__c politica :[SELECT Id, Name, IdExternoPuesto__c, VehiculoSIV__c FROM PoliticaAutosPoolAsignados__c]){
    		mapPoliticas.put(politica.Name,politica);
    	}
    	System.debug('Lista Politicas: ' +mapPoliticas);
    	
    	for(Id vinId : mapVinPuestos.keySet()){
    		System.debug('Map KeySet: ' +mapVinPuestos.keySet());
    		for(String puesto : mapVinPuestos.get(vinId)){
    			System.debug('vinId del puesto: ' +vinId);
        		System.debug('Puesto por VIN: ' +mapVinPuestos.get(vinId));
        		if(mapPoliticas.containsKey(puesto)){
        			System.debug('Ya existe Politica , '+'Puesto: '+puesto);
    				PoliticaAutosPoolAsignados__c politicaUpd = new PoliticaAutosPoolAsignados__c(
    				Name = puesto,
    				IdExternoPuesto__c = puesto,
    				VehiculoSIV__c = mapSIV.get(vinId).Id);
    				System.debug('Politica UPDATE: ' +politicaUpd);
    				//politicas.add(politicaUpd);  
    				mapPoliticasUpd.put(politicaUpd.IdExternoPuesto__c,politicaUpd);      			
        		}else{
        			System.debug('NO existe Politica , '+'Puesto: '+puesto);
    				PoliticaAutosPoolAsignados__c politicaNew = new PoliticaAutosPoolAsignados__c(
    				Name = puesto,
    				IdExternoPuesto__c = puesto,
    				VehiculoSIV__c = mapSIV.get(vinId).Id);
    				System.debug('Politica NUEVA: ' +politicaNew);
    				//politicas.add(politicaNew); 
    				mapPoliticasUpd.put(politicaNew.IdExternoPuesto__c,politicaNew);       					
        		}
   			}
		}

		try{
			System.debug(mapPoliticasUpd);
    		//upsert politicas IdExternoPuesto__c;
    		upsert mapPoliticasUpd.values() IdExternoPuesto__c;
    	}catch(Exception ex){
    		System.debug('ERROR: ' +ex.getMessage());
    	}		
    }
}