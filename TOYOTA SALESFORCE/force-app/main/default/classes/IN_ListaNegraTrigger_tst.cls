/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Samuel Galindo Rodríguez
Proyecto: Toyota Incentivos
Descripción: Clase de prueba para el trigger IN_ListaNegra_tgr
-------------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------
0.1    07-05-2018 Samuel Galindo Rodríguez   Creación
*******************************************************************************/
@isTest
private class IN_ListaNegraTrigger_tst {

	// Caso 18: Reporte de Lista begra boletinado previamente moral
	@isTest static void testBoletinadoPrevMoral() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];
		IN_TestData.crearCuentaMoral();
		Account objAccount = [
			SELECT Id, IN_Estatus__pc
			FROM Account
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objAccount.Id AND IN_Estatus__pc = 'No Boletinado'
		];

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			insert objLN;

			objLN.IN_Estatus__c = 'Boletinado Previamente';
			update objLN;
		Test.stopTest();

		List<Account> lstAccountU = [
			SELECT Id, IN_Estatus__pc, IN_Estatus__c, IN_Salvado_por_DTM__pc
			FROM Account
		];
		System.assertNotEquals(lstAccount.size(), lstAccountU.size());

	}

	// Caso 17: Reporte de lista negra boletinado previamente fisico
	@isTest static void testBoletinadoPrevFisico() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];
		IN_TestData.crearCuentaFisica();
		Account objAccount = [
			SELECT Id, IN_Estatus__pc
			FROM Account
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objAccount.Id AND IN_Estatus__pc = 'No Boletinado'
		];

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			insert objLN;

			objLN.IN_Estatus__c = 'Boletinado Previamente';
			update objLN;
		Test.stopTest();

		List<Account> lstAccountU = [
			SELECT Id, IN_Estatus__pc, IN_Estatus__c, IN_Salvado_por_DTM__pc
			FROM Account
		];
		System.assertNotEquals(lstAccount.size(), lstAccountU.size());

	}

	// Caso 16: Reporte de lista negra salvado moral
	@isTest static void testSalvadoPersonaMoral() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];
		IN_TestData.crearCuentaMoral();
		Account objAccount = [
			SELECT Id, IN_Estatus__pc
			FROM Account
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objAccount.Id AND IN_Estatus__pc = 'No Boletinado'
		];

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Moral';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			insert objLN;

			objLN.IN_Estatus__c = 'Boletinado Previamente';
			objLN.IN_Salvado__c = true;
			update objLN;
		Test.stopTest();

		List<Account> lstAccountU = [
			SELECT Id, IN_Estatus__pc, IN_Estatus__c, IN_Salvado_por_DTM__pc
			FROM Account
		];
		System.assertNotEquals(lstAccount.size(), lstAccountU.size());
	}

	// Caso 15: Reporte de lista negra salvado fisico
	@isTest static void testSalvadoPersonaFisica() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];
		IN_TestData.crearCuentaFisica();
		Account objAccount = [
			SELECT Id, IN_Estatus__pc
			FROM Account
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objAccount.Id AND IN_Estatus__pc = 'No Boletinado'
		];

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			insert objLN;

			objLN.IN_Estatus__c = 'Boletinado Previamente';
			objLN.IN_Salvado__c = true;
			update objLN;
		Test.stopTest();

		List<Account> lstAccountU = [
			SELECT Id, IN_Estatus__pc, IN_Estatus__c, IN_Salvado_por_DTM__pc
			FROM Account
		];
		System.assertNotEquals(lstAccount.size(), lstAccountU.size());
	}

	// Caso: 14 Creacion de multiples reportes de lista negra persona moral
	@isTest static void testMultipleLNPersonaMoral() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		Test.startTest();
			List<IN_Reporte_de_Listas_Negras__c> lstLN = new List<IN_Reporte_de_Listas_Negras__c>();

			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Moral';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Gamesa S.A. de C.V.';
			lstLN.add(objLN);

			IN_Reporte_de_Listas_Negras__c objLN2 = new IN_Reporte_de_Listas_Negras__c();
			objLN2.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN2.IN_Estatus__c = 'Boletinado';
			objLN2.IN_Modelo_1__c = objModelo.Id;
			objLN2.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN2.IN_Razon__c = 'Exportador';
			objLN2.IN_Salvado__c = false;
			objLN2.IN_Tipo_de_Persona__c	= 'Persona Moral';
			objLN2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN2.RecordTypeId = objRecordType.Id;
			objLN2.IN_Nombre_de_la_cuenta_text__c = 'Yoplait S.A. de C.V.';
			lstLN.add(objLN2);
			insert lstLN;
		Test.stopTest();

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
		];

		System.assertEquals(lstAccount.size(), 0);
	}

	// Caso: 13 Creacion de multiples reportes de lista negra persona fisica
	@isTest static void testMultipleLNPersonaFisica() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		Test.startTest();
			List<IN_Reporte_de_Listas_Negras__c> lstLN = new List<IN_Reporte_de_Listas_Negras__c>();

			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Samuel Galindo';
			lstLN.add(objLN);

			IN_Reporte_de_Listas_Negras__c objLN2 = new IN_Reporte_de_Listas_Negras__c();
			objLN2.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN2.IN_Estatus__c = 'Boletinado';
			objLN2.IN_Modelo_1__c = objModelo.Id;
			objLN2.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN2.IN_Razon__c = 'Exportador';
			objLN2.IN_Salvado__c = false;
			objLN2.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN2.RecordTypeId = objRecordType.Id;
			objLN2.IN_Nombre_de_la_cuenta_text__c = 'Samuel Galindo';
			lstLN.add(objLN2);
			insert lstLN;
		Test.stopTest();

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
		];

		System.assertEquals(lstAccount.size(), 0);
	}


	// Caso: 12 Reporte de lista negra sin cuenta fisica
	@isTest static void testLNCuentaPersonaFisica() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_006;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Samuel Galindo';
			insert objLN;
		Test.stopTest();

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
		];

		System.assertEquals(lstAccount.size(), 0);

	}

	// Caso 11: reporte de lista negra sin cuenta moral
	@isTest static void testLNCuentaPersonaMoral() {

		// Producto
		IN_TestData.crearModelo();
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_006;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Moral';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Gamesa S.A. de C.V.';
			insert objLN;
		Test.stopTest();

		List<Account> lstAccount = [
			SELECT Id
			FROM Account
		];

		System.assertEquals(lstAccount.size(), 0);

	}

	// Caso 10: Ya tenga reportes de lista negra guardados previamente
	@isTest static void testLNGuardadosPrev() {

		IN_TestData.crearCuentaMoral();
		IN_TestData.crearCuentaFisica();

		RecordType objPersonaMoral = [
	    SELECT Id, DeveloperName
	    FROM RecordType
	    WHERE SObjectType = 'Account' AND DeveloperName = 'ClientePersonaMoral'
	    LIMIT 1
	  ];

		Account objAccountM = [
			SELECT Id
			FROM Account
			WHERE RecordTypeId =:objPersonaMoral.Id
			LIMIT 1
		];

		IN_TestData.crearModelo();
		IN_TestData.crearRegistrosLN();

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		List<IN_Reporte_de_Listas_Negras__c> lstLN = [
			SELECT Id
			FROM IN_Reporte_de_Listas_Negras__c
		];

		Integer intTamOriginal = lstLN.size();

		Test.startTest();

			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_Cuenta__c = objAccountM.Id;
			insert objLN;

		Test.stopTest();

		List<IN_Reporte_de_Listas_Negras__c> lstNuevoLN = [
			SELECT Id
			FROM IN_Reporte_de_Listas_Negras__c
		];

		System.assertEquals(intTamOriginal+1, lstNuevoLN.size());
	}

	// Caso 9: Ya se encuentra posiblemente creada la cuenta
	@isTest static void testCuentaExistenteError() {

		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();
		String strAccount = 'SELECT '+String.join(new List<String>(SObjectType.Account.Fields.getMap().keySet()),', ')+'  FROM Account LIMIT 1';
    	Account objAccount = Database.query(strAccount);
		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_002;

		Test.startTest();

			List<IN_Reporte_de_Listas_Negras__c> lstLN = new List<IN_Reporte_de_Listas_Negras__c>();

			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Tipo_de_Persona__c = 'Persona Fisica';
			objLN.IN_RFC__c = 'GARS841215';
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Valerie Duncan';
			lstLN.add(objLN);

			IN_Reporte_de_Listas_Negras__c objLN2 = new IN_Reporte_de_Listas_Negras__c();
			objLN2.IN_Comentario_de_Listas_Negras__c = 'Fraude Doble';
			objLN2.IN_Estatus__c = 'Boletinado';
			objLN2.IN_Modelo_1__c = objModelo.Id;
			objLN2.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN2.IN_Razon__c = 'Exportador';
			objLN2.IN_Salvado__c = false;
			objLN2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN2.RecordTypeId = objRecordType.Id;
			objLN2.IN_Tipo_de_Persona__c = 'Persona Fisica';
			objLN2.IN_RFC__c = 'GARS841215';
			objLN2.IN_Nombre_de_la_cuenta_text__c = 'Valerie Duncan';
			lstLN.add(objLN2);

			try {
		  	insert lstLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}

	// Caso 7: Nombre Cuenta y Nombre Cuenta Texto no fue capturado
	@isTest static void testCuentasVacioError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();
		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_007;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Tipo_de_Persona__c = 'Persona Fisica';
			objLN.IN_RFC__c = 'GARS841215';
			try {
		  	insert objLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}

	// Caso 6: Tipo de persona es fisica y no tiene apellidos
	@isTest static void testFisicaApellidosError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();
		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_005;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Jose';
			objLN.IN_Tipo_de_Persona__c = 'Persona Fisica';
			objLN.IN_RFC__c = 'GARS841215';
			try {
		  	insert objLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}

	// Caso 5: RFC requerido cuando el campo de nombre cuenta(texto) tiene valor
	@isTest static void testCuentaRFCError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();
		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_008;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Jose Salvador';
			objLN.IN_Tipo_de_Persona__c = 'Persona Fisica';
		try {
		  	insert objLN;
	    }catch(Exception error) {
	    	System.debug('ERROR EN EL METODO testCuentaRFCError getMessage(): ' + error.getMessage() + ' strErrorMensaje: ' + strErrorMensaje);
			System.assert(error.getMessage().contains(strErrorMensaje));
		}
		Test.stopTest();
	}

	// Caso 4: Tipo de persona requerido cuando el campo de nombre de cuenta(texto) tiene valor
	@isTest static void testCuentaTipoPersonaError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();

		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_004;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Jose Salvador';
			try {
		  	insert objLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}


	// Caso 3: No se puede Boletinar y Salvar al mismo tiempo
	@isTest static void testBoletinarSalvarError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();

		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_001;

		Test.startTest();

			List<IN_Reporte_de_Listas_Negras__c> lstLN = new List<IN_Reporte_de_Listas_Negras__c>();

			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = true;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			lstLN.add(objLN);

			IN_Reporte_de_Listas_Negras__c objLN2 = new IN_Reporte_de_Listas_Negras__c();
			objLN2.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN2.IN_Estatus__c = 'Boletinado';
			objLN2.IN_Modelo_1__c = objModelo.Id;
			objLN2.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN2.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			objLN2.IN_Razon__c = 'Exportador';
			objLN2.IN_Salvado__c = true;
			objLN2.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN2.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN2.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN2.RecordTypeId = objRecordType.Id;
			lstLN.add(objLN2);

			try {
		  	insert lstLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}

	// Caso 2: Nombre Cuenta y Nombre Cuenta Texto con datos
	@isTest static void testCuentaCuentatxtError() {
		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();

		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_006;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			objLN.IN_Nombre_de_la_cuenta_text__c = 'Jose Salvador';
			try {
		  	insert objLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();

	}

	// Caso 1: No se puede crear reporte de lista negra con status Boletinado Previamente
	@isTest static void testBoletinadoPrevError() {

		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();

		Account objAccount = [
			SELECT Id
			FROM Account
			LIMIT 1
		];

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_000;

		Test.startTest();
			IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
			objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
			objLN.IN_Estatus__c = 'Boletinado Previamente';
			objLN.IN_Modelo_1__c = objModelo.Id;
			objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
			objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			objLN.IN_Razon__c = 'Exportador';
			objLN.IN_Salvado__c = false;
			objLN.IN_Tipo_de_Persona__c	= 'Persona Fisica';
			objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
			objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
			objLN.RecordTypeId = objRecordType.Id;
			try {
		  	insert objLN;
	    }catch(Exception error) {
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
		Test.stopTest();
	}

	// Caso 8: Que el perfil sea diferente de Usuario de DTM, que no sea dueño de la cuenta y que la cuenta este activa
	@isTest static void testPerfilOwnerError() {

		IN_TestData.crearCuentaFisica();
		IN_TestData.crearModelo();

		String strAccount = 'SELECT '+String.join(new List<String>(SObjectType.Account.Fields.getMap().keySet()),', ')+'  FROM Account LIMIT 1';
    	Account objAccount = Database.query(strAccount);

		Product2 objModelo = [
			SELECT Id
			FROM Product2
			LIMIT 1
		];

		// RecordType LN
		RecordType objRecordType = [
			SELECT Id, DeveloperName
			FROM RecordType
			WHERE SobjectType = 'IN_Reporte_de_Listas_Negras__c' AND DeveloperName = 'Persona_Moral'
			LIMIT 1
		];

		Profile objProfile = [
			SELECT Id
			FROM Profile
			WHERE Name='Gerente Operaciones'
		];

		User objUser = new User(
			Alias = 'standt1',
			Country='United Kingdom',
			Email='demo1@randomdemodomain.com',
			EmailEncodingKey='UTF-8',
			LastName='Testing',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			ProfileId = objProfile.Id,
			TimeZoneSidKey='America/Los_Angeles',
			UserName='testToyota@toyotatest.com'
		);
    	insert objUser;

		String strErrorMensaje = Label.IN_ListaNegraHandler_cls_msg_003;

		System.runAs(objUser) {

			Test.startTest();
				IN_Reporte_de_Listas_Negras__c objLN = new IN_Reporte_de_Listas_Negras__c();
				objLN.IN_Comentario_de_Listas_Negras__c = 'Fraude';
				objLN.IN_Estatus__c = 'Boletinado';
				objLN.IN_Modelo_1__c = objModelo.Id;
				objLN.IN_Nombre_del_Distribuidor_text_del_del__c = 'Distribuidor La Perla';
				objLN.IN_Razon__c = 'Exportador';
				objLN.IN_Salvado__c = false;
				objLN.IN_Tipo_de_Vehiculo_1_T__c = 'Nuevo';
				objLN.IN_Tipo_de_Vehiculo_Obligatorio__c = 'Nuevo';
				objLN.RecordTypeId = objRecordType.Id;
				objLN.IN_Tipo_de_Persona__c = 'Persona Fisica';
				objLN.IN_RFC__c = 'GARS841215';
				objLN.IN_Nombre_de_la_Cuenta__c = objAccount.Id;
			try {
			  	insert objLN;
		    }catch(Exception error) {
		    	System.debug('ERROR EN testPerfilOwnerError: ' + error.getMessage());
				System.assert(error.getMessage().contains(strErrorMensaje));
			}
			Test.stopTest();
    	}

	}

}