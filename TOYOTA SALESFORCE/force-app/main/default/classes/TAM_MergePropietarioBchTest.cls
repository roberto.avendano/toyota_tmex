/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_MergePropietarioSch_cls y TAM_MergePropietarioBch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    25-Agosto-2020       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_MergePropietarioBchTest {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
		
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();
	static String VaRtVehiculo = Schema.SObjectType.Vehiculo__c.getRecordTypeInfosByName().get('Vehículo').getRecordTypeId();
		
	static	Account clienteDealer = new Account();
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static	Rangos__c rango = new Rangos__c();
	static  TAM_Propietario__c objPropietario = new TAM_Propietario__c();
	
	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
			FirstName = 'testCon',
	    	LastName = 'testCon',            
	        AccountId = Label.TAM_IdCuentaPrincipalContactosCRM,
	        TAM_IdExternoNombre__c = 'testCon' + '-' + Label.TAM_IdCuentaPrincipalContactosCRM 
	    );
	    insert cont;  

		Contact cont2 = new Contact(
			FirstName = 'testCon2',
	    	LastName = 'testCon2',            
	        AccountId = clienteMoral.Id,
	        TAM_IdExternoNombre__c = 'testCon2' + '-' + clienteMoral.Id
	    );
	    insert cont2;  

		Vehiculo__c objVehiculo = new Vehiculo__c(
            Name = 'XXXXXXXX1',
            AnioModel__c = '2020',
            Id_Externo__c = 'XXXXXXXX1',
            EstatusVINAutoDemo__c = 'H',                        
            RecordTypeId = VaRtVehiculo                
        );
		insert objVehiculo;
		
		TAM_Propietario__c objProp = new TAM_Propietario__c(
			Name = 'XXXXXXXX1' + '-' + clienteMoral.Name, 
			TAM_IdExterno__c = clienteMoral.id + '-' + clienteMoral.id,
			TAM_Vin__c ='XXXXXXXX1',
			TAM_Cliente__c = clienteMoral.id,
			TAM_Actual__c = true,
			TAM_Vehiculo__c = objVehiculo.id  
		);
	    insert objProp;
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_MergePropietarioBchOK() {

		//Consulta los datos del cliente
		objPropietario = [Select Id, TAM_VIN__c, TAM_Cliente__c, TAM_IdExterno__c From TAM_Propietario__c LIMIT 1];        
   		System.debug('EN TAM_MergePropietarioBchTest objPropietario: ' + objPropietario);

		List<String> lClientes = new List<String>();
		lClientes.add(objPropietario.TAM_Cliente__c);
    	System.debug('EN TAM_MergePropietarioBchTest lClientes: ' + lClientes);

		String sLClientes = '(';		
		//Recorre la lista de lClientes
		for (String sIdCliente : lClientes){
			sLClientes += '\'' + sIdCliente + '\',';
		}
		sLClientes = sLClientes.substring(0, sLClientes.lastIndexOf(','));
		sLClientes += ')';
		System.debug('EN TAM_MergeOppSch_cls.execute sLClientes: ' + sLClientes);

		//Inicia las pruebas    	
        Test.startTest();

			String sQuery = 'Select Id, TAM_Cliente__c, TAM_IdExterno__c ';
			sQuery += ' From TAM_Propietario__c ';
			sQuery += ' Where TAM_Cliente__c IN ' + sLClientes;
			sQuery += ' Order by TAM_Cliente__c ';
			sQuery += ' Limit 1';
			System.debug('EN TAM_MergePropietarioBchTest sQuery: ' + sQuery);
			
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	    	    
			TAM_MergePropietarioBch_cls objTAMMergeOppBch = new TAM_MergePropietarioBch_cls(sQuery, objPropietario.TAM_Cliente__c);
    	    
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objTAMMergeOppBch, 5);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_MergePropietarioSch_cls objMergePropietarioSch = new TAM_MergePropietarioSch_cls();
			objMergePropietarioSch.lClientes = lClientes;
			objMergePropietarioSch.strClienteFinal = objPropietario.TAM_Cliente__c;
       		
       		System.schedule('Ejecuta_TAM_MergePropietarioSch_cls:', sch, objMergePropietarioSch);
       		
        Test.stopTest();
        
    }
}