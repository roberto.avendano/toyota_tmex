public class AttachmentKodawariController {
    public static String imageId { get; set; }
    public Evaluaciones_Dealer__c evalDealer{get;set;}
    public List<Respuestas_Preguntas_TSM__c> preguntasList{get;set;}
    private List<Attachment> archivos{get;set;}
    public Map<Id, List<Attachment>> mapAtt{get;set;}     
    private Set<Id> claves{get;set;}   
    public Set<Id> idsMapAtt{get;set;}
    public Map<Id, IfExistROEAPI> isHaveAttch{get;set;}
    public Document isNotIMGIcon{get;set;}
    public AttachmentKodawariController(ApexPages.StandardController controller){
        evalDealer= (Evaluaciones_Dealer__c)controller.getRecord();        
        claves= new Set<Id>();
        archivos= new List<Attachment>();
        mapAtt = new Map<Id, List<Attachment>>();
        isHaveAttch = new Map<Id, IfExistROEAPI>();
        
        preguntasList= [SELECT Id, Name, Evaluacion_Dealer__c, Pregunta_Relacionada__r.Reactivo__c,
                        (SELECT Id, Name FROM Respuestas_ObjetosTSM_del__r),
                        (SELECT Id, Name FROM Actividades_Planes_Integrales__r) 
                        FROM Respuestas_Preguntas_TSM__c 
                        WHERE Evaluacion_Dealer__c=:evalDealer.Id];                                
        System.debug(JSON.serialize(preguntasList));
        evalDealer= [SELECT Id, Name, RecordType.DeveloperName, Evaluacion_Cerrada__c, Consultor_TSM_Titular__r.Name, Nombre_Dealer__r.Codigo_Distribuidor__c, Nombre_Dealer__r.Promotor_Kaizen__r.Name, Tipo_de_Evaluacion_Kodawari__c, Nombre_Dealer__r.Name, (SELECT Id, Name, ContentType  from attachments) FROM Evaluaciones_Dealer__c WHERE Id=:evalDealer.Id];        
        
        List<Document> listDocs = [SELECT Id, Name, DeveloperName from Document WHERE DeveloperName='withoutTypeImg'];
        if(listDocs.size()>0){
            isNotIMGIcon= listDocs.get(0);
        }
        for(Respuestas_Preguntas_TSM__c rp: preguntasList){
            claves.add(rp.Id);
            for(Respuestas_ObjetosTSM__c roTSM: rp.Respuestas_ObjetosTSM_del__r){
                claves.add(roTSM.Id);
            }
            for(Actividad_Plan_Integral__c api: rp.Actividades_Planes_Integrales__r){
                claves.add(api.Id);
            }
        }
        System.debug(claves);
        archivos= [SELECT Id, Name, ParentId, Parent.Name, ContentType FROM Attachment WHERE ParentId IN:claves];                        
        System.debug('Adjuntos: '+ JSON.serialize(archivos));
        
        for(Attachment at: archivos){            
            if(mapAtt.containsKey(at.ParentId)){                
                mapAtt.get(at.ParentId).add(at);                
            }else{                            
                mapAtt.put(at.ParentId, new list<Attachment>{at});                
            }
        }
        
        
        for(Respuestas_Preguntas_TSM__c rp: preguntasList){
            if(!mapAtt.containsKey(rp.Id)){
                mapAtt.put(rp.Id, new List<Attachment>());                
            }else{}
            
            for(Respuestas_ObjetosTSM__c roTSM: rp.Respuestas_ObjetosTSM_del__r){
                if(!mapAtt.containsKey(roTSM.Id)){
                    mapAtt.put(roTSM.Id, new List<Attachment>());
                }else{
                    
                }
            }
            
            for(Actividad_Plan_Integral__c api: rp.Actividades_Planes_Integrales__r){
                if(!mapAtt.containsKey(api.Id)){
                    mapAtt.put(api.Id, new List<Attachment>());					
                }else{
                    
                }
            }
        } 
        
        
        
        
        for(Respuestas_Preguntas_TSM__c rp: preguntasList){
            boolean varTemporalROE= true; //Coloque "true" por que por defecto esta vacia la lista
            boolean varTemporalAPI= true;
            IfExistROEAPI objROEAPI = new IfExistROEAPI();
            
            for(Respuestas_ObjetosTSM__c roTSM: rp.Respuestas_ObjetosTSM_del__r){
                if(mapAtt.get(roTSM.Id).isEmpty()){                    
                }else{
                    varTemporalROE= false;
                    break;                    
                }                                                
            }
            
            objROEAPI.setROE(varTemporalROE);
            if(!isHaveAttch.containsKey(rp.Id)){
                isHaveAttch.put(rp.Id, objROEAPI); 
            }else{
                isHaveAttch.get(rp.Id).setROE(varTemporalROE);
            }
            
            
            for(Actividad_Plan_Integral__c api: rp.Actividades_Planes_Integrales__r){
                if(mapAtt.get(api.Id).isEmpty()){                    
                }else{
                    varTemporalAPI= false;
                    break;                    
                }                              
            }
            objROEAPI.setAPI(varTemporalAPI); 
            if(!isHaveAttch.containsKey(rp.Id)){
                isHaveAttch.put(rp.Id, objROEAPI);	  
            }else{
                isHaveAttch.get(rp.Id).setAPI(varTemporalAPI);
            } 
            
        }
        System.debug(JSON.serialize(isHaveAttch));
    }
    
    public class IfExistROEAPI{
        public boolean ROE{get;set;}
        public boolean API{get;set;}
        
        public IfExistROEAPI(){}
        
        public void setROE(boolean roe){
            this.ROE= roe;
        }
        
        public void setAPI(boolean api){
            this.API= api;
        }
    }
    
    public static void eliminarArchivo (){
        Attachment[] attEval = [Select id from  Attachment where id =: imageId];
            try {
                delete attEval;
            } catch (DmlException e) {
                // Process exception here
            }
    }
    
    
    
}