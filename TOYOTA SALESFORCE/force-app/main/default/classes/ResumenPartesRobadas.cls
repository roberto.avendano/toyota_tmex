global class ResumenPartesRobadas implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Account> accDealers = [Select id from Account WHERE recordTypeId = '012i00000002BxyAAE'];
        List<ResumenMensualPR__c> listMesResumen = new List<ResumenMensualPR__c>();
        Datetime myDT = Datetime.now();
        if(!accDealers.isEmpty()){
            for(Account accInfo  : accDealers){ 
                ResumenMensualPR__c mesResumen = new ResumenMensualPR__c();
                mesResumen.Distribuidor__c = accInfo.id;
                mesResumen.ContadorAutorizadas__c = 0;
                mesResumen.InicoFechaResumen__c = myDT.date();
                mesResumen.MesSolicitud__c = myDT.format('MM');
                mesResumen.Anio__c = myDT.format('YYYY');
                listMesResumen.add(mesResumen);
                
            }    
            
        }
        if(!listMesResumen.isEmpty()){
            insert listMesResumen;
        }
    }
}