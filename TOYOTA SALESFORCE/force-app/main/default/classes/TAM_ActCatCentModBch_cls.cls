/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DetalleOrdenCompra__c, TAM_CheckOutDetalleSolicitudCompra__c,
                        TAM_DODSolicitudesPedidoEspecial__c, TAM_VinesFlotillaPrograma__c
                        y actuelizar el campo que se llama TAM_CatalogoCentralizadoModelos__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCatCentModBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActCatCentModBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActCatCentModBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    //global void execute(Database.BatchableContext BC, List<Movimiento__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
    global void execute(Database.BatchableContext BC, List<TAM_DetalleOrdenCompra__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_ActCatCentModBch_cls.');

        //Un Objero para el error en caso de exista
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        Map<String, CatalogoCentralizadoModelos__c> mapIdCatCentModObj = new Map<String, CatalogoCentralizadoModelos__c>();
        Set<String> setIdSol = new Set<String>();
        List<TAM_DetalleOrdenCompra__c> lDetalleOrdenCompraUpd = new List<TAM_DetalleOrdenCompra__c>();
        Map<String, TAM_DetalleOrdenCompra__c> mapIdDetOrdCompraObj = new Map<String, TAM_DetalleOrdenCompra__c>();

        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdObjCheckOut = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutUpd = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
        List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPedEspUpd = new List<TAM_DODSolicitudesPedidoEspecial__c>();
        
        //Consulta el catalogo centralizado de modelos
        for (CatalogoCentralizadoModelos__c objCatCentMod : [Select c.id, c.Id_Externo__c 
            From CatalogoCentralizadoModelos__c c Order by c.Id_Externo__c ]){
            //Mete al mapa los datos de la llave externa
            String sIdExterno = objCatCentMod.Id_Externo__c; //Avanza-2201-2018-W09-10
            String sIdExternoFinal = sIdExterno.contains('-') ? sIdExterno.substring(sIdExterno.indexOf('-') + 1, sIdExterno.length()) : null;  
            //Agregalo al mapa de mapIdCatCentModObj
            if (sIdExternoFinal != null)
                mapIdCatCentModObj.put(sIdExternoFinal, objCatCentMod);
        }
        System.debug('EN TAM_ActCatCentModBch_cls mapIdCatCentModObj: ' + mapIdCatCentModObj.keyset());
        System.debug('EN TAM_ActCatCentModBch_cls mapIdCatCentModObj: ' + mapIdCatCentModObj.values());

        //Recorre el mapa de mapIdVinReg 
        for (TAM_DetalleOrdenCompra__c objDetOrdCompraPaso : scope){
            //Toma los reg y metelos al mapa de mapIdDetOrdCompraObj
            mapIdDetOrdCompraObj.put(objDetOrdCompraPaso.id, objDetOrdCompraPaso);
            //Mete el id de la sol en el set de setIdSol
            if (objDetOrdCompraPaso.TAM_SolicitudFlotillaPrograma__c != null)
                setIdSol.add(objDetOrdCompraPaso.TAM_SolicitudFlotillaPrograma__c);
        }//Fin del for para TAM_CheckOutDetalleSolicitudCompra__c
        System.debug('EN TAM_ActCatCentModBch_cls mapIdDetOrdCompraObj: ' + mapIdDetOrdCompraObj.keyset());
        System.debug('EN TAM_ActCatCentModBch_cls mapIdDetOrdCompraObj: ' + mapIdDetOrdCompraObj.values());
        System.debug('EN TAM_ActCatCentModBch_cls setIdSol: ' + setIdSol);

        //Recorre la lista de valores del mapa mapIdDetOrdCompraObj
        for (TAM_DetalleOrdenCompra__c objDetOrdCom : mapIdDetOrdCompraObj.Values()){
            //Toma el id externo del objeto TAM_DetalleOrdenCompra__c
            String sIdExterno = objDetOrdCom.TAM_IdExterno__c; //a241Y00001Nao58QAB-2021-HILUX-7490-040-20-0121Y000001J4rRQAS
            String[] arrIdExterno = sIdExterno.contains('-') ? sIdExterno.split('-') : new List<String>();  
            //Construye el Id externo para poder busccarlo en el mapa de mapIdCatCentModObj
            String sIdExternoFinal = arrIdExterno[3] + '-' + arrIdExterno[1] + '-' + arrIdExterno[4] + '-' + arrIdExterno[5]; 
            System.debug('EN TAM_ActCatCentModBch_cls DetOrdCompra sIdExternoFinal: ' + sIdExternoFinal);
            //Busca el sIdExternoFinal en el mapa de  mapIdCatCentModObj
            if (mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               lDetalleOrdenCompraUpd.add(new TAM_DetalleOrdenCompra__c(TAM_IdExterno__c = objDetOrdCom.TAM_IdExterno__c, TAM_CatalogoCentralizadoModelos__c = mapIdCatCentModObj.get(sIdExternoFinal).id)); 
            }//Fin si mapIdCatCentModObj.containsKey(sIdExternoFinal)
            if (!mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               //Crea el registro en el log de errores                
               lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objDetOrdCom.TAM_IdExterno__c, TAM_Proceso__c = 'Act Cat Cent Mod', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'objDetOrdComp' + objDetOrdCom) );                
            }//Fin si !mapIdCatCentModObj.containsKey(sIdExternoFinal)
        }//Fin del for para mapIdDetOrdCompraObj.Values()
        System.debug('EN TAM_ActCatCentModBch_cls lDetalleOrdenCompraUpd: ' + lDetalleOrdenCompraUpd);

        //Busca los reg asociddos al TAM_CheckOutDetalleSolicitudCompra__c sin rela con cat centra de Modelos
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutCons : [Select t.Id , t.TAM_IdExterno__c, t.TAM_CatalogoCentralizadoModelos__c
            From TAM_CheckOutDetalleSolicitudCompra__c t Where (TAM_CatalogoCentralizadoModelos__c = null
            OR TAM_SolicitudFlotillaPrograma__c IN :setIdSol) LIMIT 25]){
            //Toma el TAM_IdExterno__c y destripalo para buscarlo en el mapa de mapIdCatCentModObj
            String sIdExterno = objCheckOutCons.TAM_IdExterno__c; //a241Y00001Nao58QAB-2021-HILUX-7490-040-20-0121Y000001J4rRQAS
            String[] arrIdExterno = sIdExterno.contains('-') ? sIdExterno.split('-') : new List<String>();  
            //Construye el Id externo para poder busccarlo en el mapa de mapIdCatCentModObj
            String sIdExternoFinal = arrIdExterno[3] + '-' + arrIdExterno[1] + '-' + arrIdExterno[4] + '-' + arrIdExterno[5]; 
            System.debug('EN TAM_ActCatCentModBch_cls CheckOutDetSol sIdExternoFinal: ' + sIdExternoFinal);
            //Busca el sIdExternoFinal en el mapa de  mapIdCatCentModObj
            if (mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               if (objCheckOutCons.TAM_CatalogoCentralizadoModelos__c == null) 
                    lCheckOutUpd.add(new TAM_CheckOutDetalleSolicitudCompra__c(TAM_IdExterno__c = objCheckOutCons.TAM_IdExterno__c, TAM_CatalogoCentralizadoModelos__c = mapIdCatCentModObj.get(sIdExternoFinal).id)); 
            }//Fin si mapIdCatCentModObj.containsKey(sIdExternoFinal)
            if (!mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               //Crea el registro en el log de errores                
               if (objCheckOutCons.TAM_CatalogoCentralizadoModelos__c == null) 
                    lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutCons.TAM_IdExterno__c, TAM_Proceso__c = 'Act Cat Cent Mod', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'objCheckOutCons' + objCheckOutCons) );                
            }//Fin si !mapIdCatCentModObj.containsKey(sIdExternoFinal)            
        }//Fin del for para TAM_CheckOutDetalleSolicitudCompra__c
        System.debug('EN TAM_ActCatCentModBch_cls lCheckOutUpd: ' + lCheckOutUpd);

        //Busca los reg asociddos al TAM_DODSolicitudesPedidoEspecial__c sin rela con cat centra de Modelos
        for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspCons : [Select t.Id , t.TAM_IdExterno__c, t.TAM_CatalogoCentralizadoModelos__c
            From TAM_DODSolicitudesPedidoEspecial__c t Where (TAM_CatalogoCentralizadoModelos__c = null
            OR TAM_SolicitudFlotillaPrograma__c IN :setIdSol) LIMIT 25]){
            //Toma el TAM_IdExterno__c y destripalo para buscarlo en el mapa de mapIdCatCentModObj
            String sIdExterno = objDodSolPedEspCons.TAM_IdExterno__c; //a241Y00001Nao58QAB-2021-HILUX-7490-040-20-0121Y000001J4rRQAS
            String[] arrIdExterno = sIdExterno.contains('-') ? sIdExterno.split('-') : new List<String>();  
            //Construye el Id externo para poder busccarlo en el mapa de mapIdCatCentModObj
            String sIdExternoFinal = arrIdExterno[3] + '-' + arrIdExterno[1] + '-' + arrIdExterno[4] + '-' + arrIdExterno[5]; 
            System.debug('EN TAM_ActCatCentModBch_cls DodSolPedEsp sIdExternoFinal: ' + sIdExternoFinal);
            //Busca el sIdExternoFinal en el mapa de  mapIdCatCentModObj
            if (mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               if (objDodSolPedEspCons.TAM_CatalogoCentralizadoModelos__c == null) 
                    lDodSolPedEspUpd.add(new TAM_DODSolicitudesPedidoEspecial__c(TAM_IdExterno__c = objDodSolPedEspCons.TAM_IdExterno__c, TAM_CatalogoCentralizadoModelos__c = mapIdCatCentModObj.get(sIdExternoFinal).id)); 
            }//Fin si mapIdCatCentModObj.containsKey(sIdExternoFinal)
            if (!mapIdCatCentModObj.containsKey(sIdExternoFinal)){
               //Crea el registro en el log de errores                
               if (objDodSolPedEspCons.TAM_CatalogoCentralizadoModelos__c == null) 
                    lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objDodSolPedEspCons.TAM_IdExterno__c, TAM_Proceso__c = 'Act Cat Cent Mod', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'objDodSolPedEspCons' + objDodSolPedEspCons) );                
            }//Fin si !mapIdCatCentModObj.containsKey(sIdExternoFinal)            
        }//Fin del for para TAM_DODSolicitudesPedidoEspecial__c
        System.debug('EN TAM_ActCatCentModBch_cls lDodSolPedEspUpd: ' + lDodSolPedEspUpd);

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lDetalleOrdenCompraUpd, TAM_DetalleOrdenCompra__c.TAM_IdExterno__c, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess())
               System.debug('EN TAM_ActCatCentModBch_cls Hubo un error a la hora de crear/Actualizar los registros en TAM_DetalleOrdenCompra__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            //if (objDtbUpsRes.isSuccess())
            //   setDodAsig.add(objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes
        //System.debug('EN TAM_ActCatCentModBch_cls setDodAsig: ' + setDodAsig);         
        
        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.Upsertresult> lDtbUpsRes2 = Database.upsert(lCheckOutUpd, TAM_CheckOutDetalleSolicitudCompra__c.TAM_IdExterno__c, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes2){
            if (!objDtbUpsRes.isSuccess())
               System.debug('EN TAM_ActCatCentModBch_cls Hubo un error a la hora de crear/Actualizar los registros en TAM_CheckOutDetalleSolicitudCompra__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            //if (objDtbUpsRes.isSuccess())
            //   setDodAsig.add(objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes2
        //System.debug('EN TAM_ActCatCentModBch_cls setDodAsig: ' + setDodAsig);         
        
        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.Upsertresult> lDtbUpsRes3 = Database.upsert(lDodSolPedEspUpd, TAM_DODSolicitudesPedidoEspecial__c.TAM_IdExterno__c, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes3){
            if (!objDtbUpsRes.isSuccess())
               System.debug('EN TAM_ActCatCentModBch_cls Hubo un error a la hora de crear/Actualizar los registros en TAM_DODSolicitudesPedidoEspecial__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            //if (objDtbUpsRes.isSuccess())
            //   setDodAsig.add(objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes3
        //System.debug('EN TAM_ActCatCentModBch_cls setDodAsig: ' + setDodAsig);         

        System.debug('EN TAM_ActCatCentModBch_cls lError: ' + lError);                
        //Ve si hay errores          
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);

        //Roleback a todo
        //Database.rollback(sp);
            
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActCatCentModBch_cls.finish Hora: ' + DateTime.now());  
        
        //Manda llamar al siguinte proceso para leer los nuevos leads en 5 minutos
        Integer intProcessDetOrdCompraMinutos = Integer.valueOf(System.Label.TAM_ActDatosUsrSegLeadMinut);
        System.debug('EN TAM_SirenaWebServiceBch.finish intProcessDetOrdCompraMinutos: ' + intProcessDetOrdCompraMinutos);
        DateTime dtHoraActual1 = DateTime.now();
        System.debug('EN TAM_ActCatCentModBch_cls.finish dtHoraActual1: ' + dtHoraActual1);
        DateTime dtHoraFinal1 = dtHoraActual1.addMinutes(intProcessDetOrdCompraMinutos); //5
        String CRON_EXP1 = dtHoraFinal1.second() + ' ' + dtHoraFinal1.minute() + ' ' + dtHoraFinal1.hour() + ' ' + dtHoraFinal1.day() + ' ' + dtHoraFinal1.month() + ' ? ' + dtHoraFinal1.year();
        System.debug('EN TAM_ActCatCentModBch_cls.finish CRON_EXP1: ' + CRON_EXP1);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActCatCentModSch_cls objActCatCentModSch = new TAM_ActCatCentModSch_cls();
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('Ejecuta_TAM_ActCatCentModSch_cls: ' + CRON_EXP1 + '-' + UserInfo.getUserId() + ': ' + CRON_EXP1, CRON_EXP1, objActCatCentModSch);        
            
    } 

    
}