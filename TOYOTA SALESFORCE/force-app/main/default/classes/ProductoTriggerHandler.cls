public class ProductoTriggerHandler extends TriggerHandler {

private List<Product2> itemsNew;    
    
    public ProductoTriggerHandler(){
		this.itemsNew = (List<Product2>) Trigger.new;      

    }

    public override void beforeInsert(){
        ProductUpdate.updateExternalId(itemsNew);
        
    }    
    
    public override void beforeUpdate(){
        ProductUpdate.updateExternalId(itemsNew);
        
    } 
    
}