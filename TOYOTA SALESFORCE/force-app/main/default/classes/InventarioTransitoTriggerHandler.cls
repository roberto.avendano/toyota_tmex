/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de Inventario_en_Transito__c
						04-Mayo-2020 se agrega el metodo insertaRegistros
						
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    04-Mayo-2020     Héctor Figueroa              Modificación
******************************************************************************* */

public with sharing class InventarioTransitoTriggerHandler extends TriggerHandler{
	
	private List<Inventario_en_Transito__c> inventariosWList;
	private Map<Id,Inventario_en_Transito__c> inventariosWMap;
	
	public InventarioTransitoTriggerHandler() {
		this.inventariosWList = Trigger.new;	
		this.inventariosWMap = (Map<Id,Inventario_en_Transito__c>)Trigger.newMap;
	}

	public override void afterInsert(){
		insertaRegistrosObjetoInvFyG(inventariosWList);
		eliminaRecords(inventariosWMap);
	}

	public override void afterUpdate(){
		//insertaRegistrosObjetoInvFyG(inventariosWList);
	}

	private void eliminaRecords(Map<Id,Inventario_en_Transito__c> mapIW){
		List<Inventario_en_Transito__c> vinesTransito = new List<Inventario_en_Transito__c>();
		
		String squery = 'SELECT Id FROM Inventario_en_Transito__c WHERE CreatedDate < TODAY Order by CreatedDate ASC LIMIT 200';
		if (Test.isRunningTest())
			squery = 'SELECT Id FROM Inventario_en_Transito__c Order by CreatedDate ASC LIMIT 1';
		System.debug('EN insertaRegistrosObjetoInvFyG squery: ' + squery);			
		List<SObject> lSoInveTransito = Database.query(squery);
		if (!lSoInveTransito.isEmpty()){
			for (SObject objInvTrans : lSoInveTransito){
				vinesTransito.add( (Inventario_en_Transito__c) objInvTrans );
			}
		}//Fin si !lSoInveTransito.isEmpty()
		System.debug('EN insertaRegistrosObjetoInvFyG vinesTransito: ' + vinesTransito);		
		//Eliminalos
        if (!Test.isRunningTest()){
            List<Database.DeleteResult> ldtSvr = Database.delete(vinesTransito, false);
            for(Database.DeleteResult objSvr : ldtSvr){
               if (!objSvr.isSuccess())
                    System.debug('Hubo un error a la hora de eliminar el inventario en Inventario_en_Transito__c: ' + objSvr.getErrors()[0].getMessage());
            }           
        }
        //Database.delete()		
	}

	private void insertaRegistrosObjetoInvFyG(List<Inventario_en_Transito__c> inventariosWList){
		System.debug('');
		Map<String, TAM_InventarioVehiculosToyota__c> maplInventarioVehiculosToyotaUps = new Map<String, TAM_InventarioVehiculosToyota__c>();
				
		//Recorre la lista de los reg que vienen en inventariosWList
		for (Inventario_en_Transito__c invWholesale : inventariosWList){
			String sTAMIdExterno = invWholesale.Name + '-Tráfico-' + Date.today();
			maplInventarioVehiculosToyotaUps.put(sTAMIdExterno, new TAM_InventarioVehiculosToyota__c(
				Name = invWholesale.Name,
				Dealer_Code__c = invWholesale.Dealer_Code__c,
				Distributor_Invoice_Date_MM_DD_YYYY__c = invWholesale.Distributor_Invoice_Date_MM_DD_YYYY__c,
				Dummy_VIN__c = invWholesale.Dummy_VIN__c,
				Exterior_Color_Code__c = invWholesale.Exterior_Color_Code__c,
				Exterior_Color_Description__c = invWholesale.Exterior_Color_Description__c, 
				Interior_Color_Code__c = invWholesale.Interior_Color_Code__c,
				Interior_Color_Description__c = invWholesale.Interior_Color_Description__c,
				Model_Number__c = invWholesale.Model_Number__c,
				Model_Year__c = invWholesale.Model_Year__c,
				Numero_de_Orden__c = invWholesale.Dummy_VIN__c,
				Tipo_Inventario__c = 'Tráfico',
				Toms_Series_Name__c = invWholesale.Toms_Series_Name__c,
				TAM_IdExterno__c = sTAMIdExterno,
				TAM_Fleet__c = invWholesale.TAM_Fleet__c,
				TAM_Dummy_VIN_Temp__c = invWholesale.TAM_Dummy_VIN_Temp__c,
                TAM_ModelPhaseCode__c = invWholesale.TAM_ModelPhaseCode__c,
                TAM_SerialNumberCDig__c = invWholesale.TAM_SerialNumberCDig__c				
				)
			);
		}//Fin del for para inventariosWList

		System.debug('EN insertaRegistrosObjetoInvFyG maplInventarioVehiculosToyotaUps.KeySet(): ' + maplInventarioVehiculosToyotaUps.KeySet());		
		System.debug('EN insertaRegistrosObjetoInvFyG maplInventarioVehiculosToyotaUps.Values(): ' + maplInventarioVehiculosToyotaUps.Values());
		//Ve si tiene algo el mapa de maplInventarioVehiculosToyotaUps
		if (!maplInventarioVehiculosToyotaUps.isEmpty()){
			List<Database.UpsertResult> lDtbUpsRes = Database.upsert(maplInventarioVehiculosToyotaUps.values(), TAM_InventarioVehiculosToyota__c.TAM_IdExterno__c, false);
    		//Ve si hubo errores
    		for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
    			if (!objDtbUpsRes.isSuccess())
    				System.debug('Error a la hora de actualizar el Inventario Final: ' + objDtbUpsRes.getErrors()[0].getMessage());
    		}//Fin del for para lDtbUpsRes
		}//Fin si !maplInventarioVehiculosToyotaUps.isEmpty()
		
	}
    
}