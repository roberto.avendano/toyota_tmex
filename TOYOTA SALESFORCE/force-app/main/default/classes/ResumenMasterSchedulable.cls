global class ResumenMasterSchedulable implements Schedulable {
    
    global void execute(SchedulableContext SC){
        String query='Select PartNameId__c, Id, BOQTYDealerOrder__c from MasterBO__c where StatusByAsDate__c=\'Yes\' and RecordType.DeveloperName= \'MasterBO\'';
        ResumenMasterBOBatch rmBO = new ResumenMasterBOBatch(query);
        Database.executeBatch(rmBO);
    }
}