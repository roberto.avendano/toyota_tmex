/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladira para el componente TAM_CargaMasivaVinesDod

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    03-Junio-2020        Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CargaMasivaVinesDodCtrl_tst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypeFlotillaRep = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Pedido Especial Rep').getRecordTypeId();    
    static String sRectorTypePasoFlotillaPE = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();

    @TestSetup static void loadData(){

        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;

        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id
        );
        insert solVentaFlotillaPrograma;

        TAM_DODSolicitudesPedidoEspecial__c  objDodSolPedEsp = new TAM_DODSolicitudesPedidoEspecial__c (
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10'  + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoFlotillaPE + '-Arrendadora-0',
            //TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_Pago__c = 'Arrendadora',
            TAM_TipoSolicitud__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_FechaPeticion__c = Date.today(),
            TAM_ClaveDealer__c = '57000',
            TAM_Anio__c = '2021', 
            TAM_Codigo__c = '4501', 
            TAM_Serie__c = 'AVANXA', 
            TAM_Modelo__c = 'LT',
            TAM_Color__c = '20/50',
            //TAM_DummyVin__c = 'XXXXXXXXXX1',
            TAM_Estatus__c = 'Pendiente'
        );
        insert objDodSolPedEsp;

    }

    static testMethod void TAM_CargaMasivaVinesDodCtrlOK() {

        Test.startTest();
            List<TAM_DODSolicitudesPedidoEspecial__c> lDodSol = new List<TAM_DODSolicitudesPedidoEspecial__c>();
            //Consulta los datos de los vineas creados para DOD
            for (TAM_DODSolicitudesPedidoEspecial__c objDod : [Select ID, Name, TAM_FechaPeticion__c,
                TAM_ClaveDealer__c, TAM_Modelo__c, TAM_Anio__c, TAM_Codigo__c, TAM_Serie__c, TAM_Color__c,
                TAM_DummyVin__c, TAM_IdExterno__c, TAM_Estatus__c From TAM_DODSolicitudesPedidoEspecial__c LIMIT 2]){
                lDodSol.add(objDod);
            }
            Test.setCurrentPage(Page.TAM_CargaMasivaVinesDodPage);
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lDodSol);
            stdSetController.setSelected(lDodSol);
            TAM_CargaMasivaVinesDodCtrl edc = new TAM_CargaMasivaVinesDodCtrl(stdSetController);
            
            //Inicializa el VIN que se va aprobar
            edc.sVinesSelecc = 'XXXXXXXXXX1';
            //Llama al metodo de seleccionarVines
            edc.seleccionarVines();
            //Llama al metodo de guardar
            edc.guardar();
            //Llama al metodo de regresar
            edc.regresar();
            
        Test.stopTest();
                
    }
}