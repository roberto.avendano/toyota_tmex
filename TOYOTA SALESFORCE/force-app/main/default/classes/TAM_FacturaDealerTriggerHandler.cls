/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de la clase de TAM_FacturaDealerTriggerHandler
******************************************************************************* */
public with sharing class TAM_FacturaDealerTriggerHandler extends TriggerHandler{
private List<TAM_Factura_Dealer__c> itemsNew;

//Un constructor por default
public TAM_FacturaDealerTriggerHandler() {
    this.itemsNew = (List<TAM_Factura_Dealer__c>) Trigger.new;
    
}

//Sobreescribe el metodo de afterInsert
public override void afterUpdate() {
    actualizaFactura(this.itemsNew);
}

//Evento que actualiza los detalles de estado de cuenta apartir de la factura de dealer
private void actualizaFactura(List<TAM_Factura_Dealer__c> itemsNew){
    Set<String> folioFactura = new Set<String>();
    Set<Id> idEstadoCuenta = new Set<Id>();
    Map<String,TAM_Factura_Dealer__c> mapByFolioFactura = new Map<String,TAM_Factura_Dealer__c>();

    //Se recorre los nuevos elementos del trigger y se guarda en diferentes elementos 
    if(!itemsNew.isEmpty()){
        for(TAM_Factura_Dealer__c fact : itemsNew){
            folioFactura.add(fact.TAM_FolioFactura__c);
            idEstadoCuenta.add(fact.TAM_EstadoCuenta__c);
            mapByFolioFactura.put(fact.TAM_FolioFactura__c,fact);
            
        }

    }

    List<TAM_DetalleEstadoCuenta__c> listaDetalleEdoCta = new List<TAM_DetalleEstadoCuenta__c>();
    
    //Se valida que el folio de la factura y el id del estado de cuenta sean diferentes de nulo
    if(!folioFactura.isEmpty() && !idEstadoCuenta.isEmpty()){
        //Query al detalle del estado de cuenta
        listaDetalleEdoCta = [Select id,TAM_EstatusVINEdoCta__c,recordtype.name,TAM_FolioFacturaCobroTMEX__c,TAM_EstadoCuenta__c,TAM_VIN__c,TAM_Fecha_Pago__c
                                FROM TAM_DetalleEstadoCuenta__c WHERE TAM_FolioFacturaCobroTMEX__c IN : folioFactura AND TAM_EstadoCuenta__c IN : idEstadoCuenta];


    }


    List<TAM_DetalleEstadoCuenta__c> listaDelleEdoCtaUpdate = new List<TAM_DetalleEstadoCuenta__c>();
    if(!listaDetalleEdoCta.isEmpty()){
        //Se recorren los detalles de estado de cuenta
        for(TAM_DetalleEstadoCuenta__c detalleEdoCta : listaDetalleEdoCta){
            //Convesion del tipo de registro
            String tipoRegistroEdoCta;
            if(detalleEdoCta.recordtype.name == 'Retail'){
                tipoRegistroEdoCta = 'retail';
            }
            else if(detalleEdoCta.recordtype.name == 'Flotilla'){
                tipoRegistroEdoCta = 'venta corporativa';
 
            }
            else if(detalleEdoCta.recordtype.name == 'Bono'){
                tipoRegistroEdoCta = 'bono';

            }
            else if(detalleEdoCta.recordtype.name == 'AutoDemo'){
                tipoRegistroEdoCta = 'Auto Demo';
  
            }
            
            //Se revisa si el mapa de facturas con tiene la misma llava que los detalles del estado de cuenta
            if(mapByFolioFactura.containsKey(detalleEdoCta.TAM_FolioFacturaCobroTMEX__c)){
                //Se guarda en un objeto la factura que conicida con los detalles estado de cuenta
                TAM_Factura_Dealer__c facturaByFolio = mapByFolioFactura.get(detalleEdoCta.TAM_FolioFacturaCobroTMEX__c);
                //Comparación para validar que corresponda el detalle de estadod e cuenta a la factura que se actualizo
                if (tipoRegistroEdoCta == facturaByFolio.TAM_TipoIncentivo__c && facturaByFolio.TAM_FolioFactura__c == detalleEdoCta.TAM_FolioFacturaCobroTMEX__c && facturaByFolio.TAM_EstadoCuenta__c == detalleEdoCta.TAM_EstadoCuenta__c) {
                    TAM_DetalleEstadoCuenta__c newdDetalleEdoCta = new TAM_DetalleEstadoCuenta__c();
                    newdDetalleEdoCta.Id = detalleEdoCta.Id;
                    newdDetalleEdoCta.TAM_Fecha_Pago__c = facturaByFolio.TAM_FechaTentativaPago__c;
                    newdDetalleEdoCta.TAM_Fecha_Pago_Autorizado_TMEX__c = Date.today();
                    //Se setea el estatus de la factura
                    //Factura autorizada
                    if(facturaByFolio.TAM_EstadoFactura__c == 'Autorizada'){
                        newdDetalleEdoCta.TAM_Estatus_Finanzas__c = 'Pago Autorizado TMEX';
                    }
                    //Rechazada
                    system.debug('estatus trigger vin'+detalleEdoCta.TAM_EstatusVINEdoCta__c);
                    if(facturaByFolio.TAM_EstadoFactura__c == 'Rechazada' && detalleEdoCta.TAM_EstatusVINEdoCta__c == 'Enviado a Facturar'){
                        newdDetalleEdoCta.TAM_Estatus_Finanzas__c = 'Rechazado por Finanzas';
                        newdDetalleEdoCta.TAM_EstatusVINEdoCta__c = 'Pendiente de cobro';

                    }
                    //Pendiente
                    if(facturaByFolio .TAM_EstadoFactura__c == 'Pendiente'){
                        newdDetalleEdoCta.TAM_Estatus_Finanzas__c = 'Pendiente';

                    }
                    //Pendiente
                    if(facturaByFolio.TAM_EstadoFactura__c == 'Pago Cancelado Finanzas'){
                        newdDetalleEdoCta.TAM_Estatus_Finanzas__c = 'Pago Cancelado Finanzas';

                    }

                    listaDelleEdoCtaUpdate.add(newdDetalleEdoCta);


                }

            }

        }

    }

    if(!listaDelleEdoCtaUpdate.isEmpty()){
        try{
            update listaDelleEdoCtaUpdate;
        }catch(Exception e){
            System.debug('eror de procesamiento'+e.getMessage());

            }
        

        }      


    }


}