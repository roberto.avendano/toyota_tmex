public class SolicitudSiteHeaderHomeController {
    public String csId{get;set;}
    
    public SolicitudSiteHeaderHomeController(){  
        csId =  ApexPages.currentPage().getParameters().get('id');
        System.debug(csId);
    }
       
    public PageReference inicio(){
        PageReference page;
        page = new PageReference('/SolicitudSiteHome?id='+csId);
        return page;
    } 
}