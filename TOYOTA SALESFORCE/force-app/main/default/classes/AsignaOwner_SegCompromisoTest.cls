//Clase de prueba de AsignaOwner_SegCompromiso
@IsTest
public class AsignaOwner_SegCompromisoTest {
static testmethod void test(){
        List<Seguimiento_de_compromiso_fwy__c> ej = new List<Seguimiento_de_compromiso_fwy__c>();               
        Id p = [select id from profile where name='Partner Community User'].id;

       	Account ac = new Account(
            name ='Grazitti',
        	Codigo_Distribuidor__c = '57031');
        insert ac; 
        
        Contact con = new Contact(
            LastName ='testCon',            
            AccountId = ac.Id);
        insert con;  
        
        User user = new User(
            alias = 'test123p', 
            email='test123tfsproduc@noemail.com',
            emailencodingkey='UTF-8', 
            lastname='Testingproductfs', 
            Owner_De_Registros__c = true,
            languagelocalekey='en_US',
            localesidkey='en_US', 
            profileid = p, 
            country='United States',
            IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', 
            username='testOwnertfsproduc@noemail.com.test');
        
        insert user;
        
        
        
        for(integer x=0; x<10; x++){
            Seguimiento_de_compromiso_fwy__c y = new Seguimiento_de_compromiso_fwy__c();
            y.name= 'Ejemplo '+x;
            y.fwy_Dealer_Code__c = '57031';        
            ej.add(y);
        }    
        insert ej;
        
    }    
}