/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_CheckOutDetalleSolicitudCompra__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CancelaVinCheckoutDupBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_CancelaVinCheckoutDupBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_CheckOutDetalleSolicitudCompra__c> scope){
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls.');

        Set<String> setVinCeckout = new Set<String>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutVinDup = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapCheckOutVinDupUps 
            = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, List<TAM_CheckOutDetalleSolicitudCompra__c>> mapCheckOutVinDupCons 
            = new Map<String, List<TAM_CheckOutDetalleSolicitudCompra__c>>();
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_CheckOutDetalleSolicitudCompra__c objChkOut : scope){
            //Agrega el vin a setVinCeckout
            setVinCeckout.add(objChkOut.TAM_VIN__c);    
        }
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls setVinCeckout: ' + setVinCeckout);

        //Consulta los vines asociados a setVinCeckout y ve si ya estan cancelados
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVinDup : [Select id, TAM_VIN__c, TAM_EstatusDOD__c, 
            TAM_Historico__c, TAM_EstatusDealerSolicitud__c, TAM_FECHACANCELACION__C, TAM_TipoVenta__c 
            From TAM_CheckOutDetalleSolicitudCompra__c 
            Where TAM_VIN__c IN :setVinCeckout Order by TAM_VIN__c]){
            //Metela a la lista de lCheckOutVinDup
            lCheckOutVinDup.add(objCheckOutVinDup);
        } 
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls lCheckOutVinDup: ' + lCheckOutVinDup);
        
        //Recorre la lista de lCheckOutVinDup
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVinDupCons : lCheckOutVinDup){
            //Metelo al mapa de mapCheckOutVinDupCons
            if (mapCheckOutVinDupCons.containsKey(objCheckOutVinDupCons.TAM_VIN__c))
                mapCheckOutVinDupCons.get(objCheckOutVinDupCons.TAM_VIN__c).add(objCheckOutVinDupCons);
            if (!mapCheckOutVinDupCons.containsKey(objCheckOutVinDupCons.TAM_VIN__c))
                mapCheckOutVinDupCons.put(objCheckOutVinDupCons.TAM_VIN__c, new List<TAM_CheckOutDetalleSolicitudCompra__c>{objCheckOutVinDupCons});
        }//Fin del for para 
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls mapCheckOutVinDupCons: ' + mapCheckOutVinDupCons.KeySet());
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls mapCheckOutVinDupCons: ' + mapCheckOutVinDupCons.values());

        //Recorre el mapa de mapCheckOutVinDupCons y ve si los dos reg estan asignados
        for (String sCheckouVinPaso : mapCheckOutVinDupCons.KeySet()){
            Boolean blnHistCancel = true;
            Boolean blnSolCancel = true;
            Boolean blnSolCancelInv = false;
            TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVinDupPasoUpd;
            //Recorre la lista de  TAM_CheckOutDetalleSolicitudCompra__c
            for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutVinDupPaso : mapCheckOutVinDupCons.get(sCheckouVinPaso)){
                //Ve si no esta cancelado y es del historico
                if (objCheckOutVinDupPaso.TAM_Historico__c && objCheckOutVinDupPaso.TAM_EstatusDOD__c != 'Cancelado'
                    && objCheckOutVinDupPaso.TAM_EstatusDealerSolicitud__c != 'Rechazada'){
                    blnHistCancel = false;
                    objCheckOutVinDupPasoUpd = new TAM_CheckOutDetalleSolicitudCompra__c(
                        id = objCheckOutVinDupPaso.id,
                        TAM_EstatusDealerSolicitud__c = 'Rechazada',
                        TAM_VIN__c = objCheckOutVinDupPaso.TAM_VIN__c
                    ); 
                }//Fin si objCheckOutVinDupPaso.TAM_Historico__c && objCheckOutVinDupPaso.TAM_EstatusDOD__c != 'Cancelado' && objCheckOutVinDupPaso.TAM_EstatusDealerSolicitud__c != 'Rechazada'
                if (!objCheckOutVinDupPaso.TAM_Historico__c && objCheckOutVinDupPaso.TAM_EstatusDOD__c != 'Cancelado')
                    blnSolCancel = false;
                if (!objCheckOutVinDupPaso.TAM_Historico__c && objCheckOutVinDupPaso.TAM_EstatusDOD__c == 'Cancelado' && objCheckOutVinDupPaso.TAM_TipoVenta__c == 'Inventario'){
                    objCheckOutVinDupPasoUpd = new TAM_CheckOutDetalleSolicitudCompra__c(
                        id = objCheckOutVinDupPaso.id,
                        TAM_EstatusDealerSolicitud__c = 'Cancelada',
                        TAM_VIN__c = objCheckOutVinDupPaso.TAM_VIN__c
                    );
                    blnSolCancelInv = true;
                }//Fin si !objCheckOutVinDupPaso.TAM_Historico__c && objCheckOutVinDupPaso.TAM_EstatusDOD__c == 'Cancelado' && objCheckOutVinDupPaso.TAM_TipoVenta__c == 'Inventario' 
            }//Fin del for para mapCheckOutVinDupCons.get(sCheckouVinPaso)
            //Ve si tiene historico y una solicitud aporbada
            if (!blnHistCancel && !blnSolCancel && objCheckOutVinDupPasoUpd.id != null){
                mapCheckOutVinDupUps.put(objCheckOutVinDupPasoUpd.id, objCheckOutVinDupPasoUpd);
                lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutVinDupPasoUpd.id + '-' + objCheckOutVinDupPasoUpd.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Historico ', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutVinDupPasoUpd ) );            
            }//Fin si !blnHistCancel && !blnSolCancel && objCheckOutVinDupPasoUpd.id != null
            if (blnSolCancelInv && objCheckOutVinDupPasoUpd.id != null){
                mapCheckOutVinDupUps.put(objCheckOutVinDupPasoUpd.id, objCheckOutVinDupPasoUpd);
                lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutVinDupPasoUpd.id + '-' + objCheckOutVinDupPasoUpd.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Inventario', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutVinDupPasoUpd ) );            
            }//Fin si !blnHistCancel && !blnSolCancel && objCheckOutVinDupPasoUpd.id != null
        }//Fin del for para mapCheckOutVinDupCons.KeySet()
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls mapCheckOutVinDupUps: ' + mapCheckOutVinDupUps.KeySet());
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls mapCheckOutVinDupUps: ' + mapCheckOutVinDupUps.values());

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();

        //Ve si tiene algo la lista de mapCheckOutVinDupUps
        if (!mapCheckOutVinDupUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapCheckOutVinDupUps.values(), TAM_CheckOutDetalleSolicitudCompra__c.Id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('EN TAM_CancelaVinCheckoutDupBch_cls Hubo un error a la hora de crear/Actualizar el registro ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    lError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'Cancelada CheckOut Historico Error', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = objDtbUpsRes.getErrors()[0].getMessage() ) );            
                }//Fin si!objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutVinDupUps.isEmpty()

        //Roleback a todo
        //Database.rollback(sp);

        //Crea los reg en el objeto de Log
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);

    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CancelaVinCheckoutDupBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}