public class SolicitudSiteOlvidoPasswordController {
    public String csId{get;set;}
    public String username {get;set;}
    public String password {get;set;}
    
    public SolicitudSiteOlvidoPasswordController(ApexPages.StandardController stdcontroller){
        csId =stdcontroller.getId();
    }
    
    public PageReference actualizarPassword(String csPassId){
        PageReference  page;
        ContactoSite__c cs = [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Activo__c, Contrasena__c, UsuarioRegistrado__c, OlvidoContrasena__c 
                              FROM ContactoSite__c WHERE Id =: csPassId LIMIT 1];
        
        if(cs.UsuarioRegistrado__c==true && cs.OlvidoContrasena__c==true){
            cs.Contrasena__c = password;
            cs.OlvidoContrasena__c = false;
        }
        try{
            update cs;
            page = System.Page.SolicitudSiteCambioPasswordConf;
            page.setRedirect(true);
            System.debug(page);
            
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
            System.debug('Error actualizar contraseña: ' + e.getMessage());
        }
        return page;
    }
    
    public PageReference cambiarPassword() {
        PageReference page;
        Map<String,ContactoSite__c> mapCs = new Map<String,ContactoSite__c>();
        for(ContactoSite__c cs: [SELECT Id, Contacto__c,Contacto__r.Email, NombreUsuario__c, Contrasena__c, UsuarioRegistrado__c, OlvidoContrasena__c FROM ContactoSite__c]){
            if(cs.UsuarioRegistrado__c==true){
                mapCs.put(cs.Contacto__r.Email, cs);
                System.debug(mapCs);
            }
        }
        
        if(mapCs.containsKey(username)){
            System.debug('El usuario si esta registrado');
            mapCs.get(username).OlvidoContrasena__c = true;
            try{
                update mapCs.values();
                page = actualizarPassword(mapCs.get(username).Id);
            }catch(Exception e){
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(msg);
                System.debug('Error actualizar contacto site para cambio de contraseña: ' + e.getMessage());
            }    
        }else{
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Tu usuario no esta registrado, para cambiar contraseña necesitas un usuario registrado');
            ApexPages.addMessage(msg);
        }
        return page;
    }
}