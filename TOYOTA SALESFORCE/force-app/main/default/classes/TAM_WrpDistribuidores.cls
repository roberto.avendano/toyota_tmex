/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para asociar los distribuidores cuando se trata de una Venta Especial
    					Para Flotilla o Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    11-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_WrpDistribuidores {
    
    @AuraEnabled 
    public String strId {get;set;}    
    @AuraEnabled 
    public String strNumero {get;set;}    
    @AuraEnabled 
    public String strDescripcion {get;set;}        
    @AuraEnabled 
    public Boolean bolSeleccionado {get;set;}
    
    //Un constructor por default
    public TAM_WrpDistribuidores(){
    	this.strId = '';
    	this.strNumero = '';
    	this.strDescripcion = '';
    	this.bolSeleccionado = false;
    }
    
    //Un constructor que recibe parametros 
    public TAM_WrpDistribuidores(String strId, String strNumero, String strDescripcion, Boolean bolSeleccionado){
    	this.strId = strId;
    	this.strNumero = strNumero;
    	this.strDescripcion = strDescripcion;
    	this.bolSeleccionado = bolSeleccionado;
    }    
    
}