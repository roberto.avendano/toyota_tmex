@isTest
private class DeleteInvTransito_Schedule_Test {
    
    
    static testMethod void myTestMethod() {         
         Inventario_en_Transito__c invf = new Inventario_en_Transito__c();
          invf.name = '1234567qwertyu';
          insert invf;
       	
          Datetime yesterday = Datetime.now().addDays(-2);
		  Test.setCreatedDate(invf.Id, yesterday); 
       	  invf.Name = '2345678id';
          update invf;
        
        test.starttest();        
         	DeleteInvTransito_Schedule  myClass = new DeleteInvTransito_Schedule();   
         	String chron = '0 0 23 * * ?';        
         	system.schedule('Test Sched', chron, myClass);
        
         test.stopTest();
    }
}