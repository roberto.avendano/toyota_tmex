/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_CheckOutDetalleSolicitudCompra__c
                        y toma los reg que ya tienen un TAM_Vin__c y no tienen valor en el campo de TAM_FechaVentaDD__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    20-Dic-2020          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActFechaDDChkOutBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_ActFechaDDChkOutBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActFechaDDChkOutBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    //global void execute(Database.BatchableContext BC, List<Movimiento__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
    global void execute(Database.BatchableContext BC, List<TAM_CheckOutDetalleSolicitudCompra__c> scope){ //List<TAM_CheckOutDetalleSolicitudCompra__c>
        System.debug('EN TAM_ActFechaDDChkOutBch_cls.');

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        String sRectorTypeCheckOutInvGeneral = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        //Crea la lista para poder eliminar los reg de ClientesPaso__c
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapFechaDDUpd = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, Set<String>> mapIdVinReg = new Map<String, Set<String>>();
        Set<String> setDodCons = new Set<String>();
        Set<String> setDodAsig = new Set<String>();
        Set<String> setCheckOutAsig = new Set<String>();
        Set<String> setDodAsigNoExiste = new Set<String>();
        Map<String, DateTime> mapVinesVtas = new map<String, DateTime>();
        Map<String, TAM_CheckOutDD__c> mapCheckOutDDUpsSinMov = new Map<String, TAM_CheckOutDD__c>();
        Map<String, TAM_CheckOutDD__c> mapCheckOutDDUpsFinal = new Map<String, TAM_CheckOutDD__c>();

        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdObjCheckOut = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, String> mapVinObjVehic = new Map<String, String>();
        Map<String, Vehiculo__c> mapVinVehicCons = new Map<String, Vehiculo__c>();
        Set<String> setCheckVinCons = new Set<String>();
        
        //Un Objero para el error en caso de exista
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();
        Set<String> setIdCheckout = new Set<String>();
        
        String sLblFechaCalToyota = System.Label.TAM_FechaMesToyota;
        Date dFechaConsulta = sLblFechaCalToyota != 'null' ? Date.valueOf(sLblFechaCalToyota) : null;
        System.debug('EN TAM_ActFechaDDChkOutBch_cls dFechaConsulta: ' + dFechaConsulta);
        
        Date dtFechsIniNoInv;
        Date dFechaFinConsultaMov;
        if (dFechaConsulta != null)
            dFechaFinConsultaMov = dFechaConsulta;
        if (dFechaConsulta == null)
            dFechaConsulta = Date.today().addDays(-1);
        System.debug('EN TAM_ActFechaDDChkOutBch_cls dFechaConsulta2: ' + dFechaConsulta);            
        //Busca en el objeto de TAM_CalendarioToyota__c el rango al que le corresponda
        for (TAM_CalendarioToyota__c objCalToy : [Select id, TAM_FechaInicio__c, TAM_FechaFin__c 
            From TAM_CalendarioToyota__c Where TAM_FechaInicio__c <= :dFechaConsulta 
            And TAM_FechaFin__c >= :dFechaConsulta]){
            dtFechsIniNoInv = objCalToy.TAM_FechaInicio__c;    
            if (dFechaFinConsultaMov == null)
                dFechaFinConsultaMov = objCalToy.TAM_FechaFin__c;
        }
        //Es una prueba
        if (Test.isRunningTest()){
            dFechaFinConsultaMov = Date.today();
            dtFechsIniNoInv = Date.today();
        }//Fin si Test.isRunningTest()
        System.debug('EN TAM_ActFechaDDChkOutBch_cls dtFechsIniNoInv: ' + dtFechsIniNoInv + ' dFechaFinConsultaMov: ' + dFechaFinConsultaMov);
        String strdtFechsIniNoInv = String.valueOf(dtFechsIniNoInv);
        String strdFechaFinConsultaMov = String.valueOf(dFechaFinConsultaMov);
        if (Test.isRunningTest())
            strdtFechsIniNoInv = '2021-07-01';        
        System.debug('EN TAM_ActFechaDDChkOutBch_cls strdtFechsIniNoInv: ' + strdtFechsIniNoInv + ' strdFechaFinConsultaMov: ' + strdFechaFinConsultaMov);
        
        Set<String> setVinMov = new Set<String>();
        List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOut = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
                
        //Recorre el mapa de mapIdVinReg 
        for (TAM_CheckOutDetalleSolicitudCompra__c objChkOut : scope){
        //for (TAM_CheckOutDetalleSolicitudCompra__c objChkOut : lCheckOut){
            //Una bandera para ver si existe en Vehiculo
            Boolean blnExisteVenta = false;
            Movimiento__c objMovInv;
            
            //Consulta los reg de checkout  para el Vin objChkOut.TAM_VIN__c y ordenalos por la fecha de Creación
            List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckoutCons = [Select Id, TAM_VIN__c, TAM_FechaCancelacion__c, 
                TAM_EstatusDOD__c, TAM_SolicitudFlotillaPrograma__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, 
                TAM_EstatusDealerSolicitud__c, TAM_TipoVenta__c, TAM_FechaCreacionFormula__c
                From TAM_CheckOutDetalleSolicitudCompra__c Where TAM_VIN__c =:objChkOut.TAM_VIN__c Order by TAM_FechaCreacionFormula__c ASC];

            String queryVehiculo = 'Select v.id, v.TAM_Fecha_Envio_Ultimo_Mov__c, v.Name, Estatus__c, Fleet__c, ' +
                ' Ultimo_Movimiento__r.Month__c, Ultimo_Movimiento__r.Year__c, Ultimo_Movimiento__r.Submitted_Date__c,' +  
                ' Ultimo_Movimiento__r.Qty__c, Ultimo_Movimiento__r.TAM_VIN__c, Ultimo_Movimiento__c, Ultimo_Movimiento__r.Trans_Type__c,' +   
                ' (Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, VIN__r.Name, ' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, Fleet__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c From Movimientos__r Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' And TAM_FechaEnvForm__c >= ' + strdtFechsIniNoInv + ' And TAM_FechaEnvForm__c <= ' + strdFechaFinConsultaMov + '' + //CreatedDate
                //' And TAM_FechaEnvForm__c >= ' + strdtFechsIniNoInv + //CreatedDate
                ' Order by Submitted_Date__c DESC)' +
                ' From Vehiculo__c v Where Name = \'' + String.escapeSingleQuotes(objChkOut.TAM_VIN__c) + '\'';
            //Ees una prueba
            if (Test.isRunningTest())    
                queryVehiculo = 'Select v.id, v.TAM_Fecha_Envio_Ultimo_Mov__c, v.Name, Estatus__c, Fleet__c, ' +
                ' Ultimo_Movimiento__r.Month__c, Ultimo_Movimiento__r.Year__c, Ultimo_Movimiento__r.Submitted_Date__c,' +  
                ' Ultimo_Movimiento__r.Qty__c, Ultimo_Movimiento__r.TAM_VIN__c, Ultimo_Movimiento__c, Ultimo_Movimiento__r.Trans_Type__c,' +  
                ' (Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, VIN__r.Name, Fleet__c,' +  
                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c From Movimientos__r Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
                ' Order by Submitted_Date__c DESC)' +
                ' From Vehiculo__c v' +
                ' LIMIT 1';
            System.debug('EN TAM_ActFechaDDChkOutBch_cls queryVehiculo: ' + queryVehiculo);

            System.debug('EN TAM_ActFechaDDChkOutBch_cls ANTES DE ENTRAR A TAM_getUltimoMovVin.getUltimoMovVin() dFechaFinConsultaMov: ' + dFechaFinConsultaMov);
            //Busca el ultimo mov y la suma aritmetica de los miv del mes en cuestión
            objMovInv = TAM_getUltimoMovVin.getUltimoMovVin(objChkOut.TAM_VIN__c, dFechaFinConsultaMov, dFechaFinConsultaMov, true);
            System.debug('EN TAM_ActFechaDDChkOutBch_cls objMovInv1: ' + objMovInv);
                
            //Crea los objetos del tipo TAM_CheckOutDetalleSolicitudCompra__c y metelos a el mapa de mapFechaDDUpd
            TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = new TAM_CheckOutDetalleSolicitudCompra__c(
                id = objChkOut.id,
                TAM_Vehiculo__c = objMovInv != null ? objMovInv.VIN__c : null, //objVenta.id,
                TAM_FechaVentaDD__c = objMovInv != null ? objMovInv.Submitted_Date__c : null, //objVenta.TAM_Fecha_Envio_Ultimo_Mov__c,
                TAM_Fleet__c = objMovInv != null ? objMovInv.Fleet__c : null,  //objVenta.Fleet__c,
                TAM_AnioDD__c = objMovInv != null ? objMovInv.Year__c : null, //objVenta.Ultimo_Movimiento__r.Year__c,
                TAM_MesDD__c = objMovInv != null ? objMovInv.Month__c : null,  //objVenta.Ultimo_Movimiento__r.Month__c,
                TAM_UltimoMovimiento__c = objMovInv != null ? objMovInv.Trans_Type__c : null, //objVenta.Estatus__c,
                TAM_UltimoMovCalToyota__c = objMovInv != null ? objMovInv.Trans_Type__c : null,
                TAM_UltMovCalToyota__c = objMovInv != null ? objMovInv.id : null
            );
            //Agregalo al mapa de mapFechaDDUpd
            mapFechaDDUpd.put(objChkOut.id, objCheckOutPaso);
            
            //Consulta los datos del VIN en Vehiculo__c y ve si se trata de un Flet N                
            for (Vehiculo__c objVenta : Database.query(queryVehiculo)){                    
                //Prende la bandera de blnExisteVenta
                blnExisteVenta = true;
                                
                //Obten el ultimo mov del mes de la clase TAM_ActTotVtaInvBch_cls.getUltMov
                if (!objVenta.Movimientos__r.isEmpty()){
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls TIENE MOVS EN LA FECHA SELECCIONADA2...');
                    Integer intTotMov = 0;
                    //Tiene un solo mov
                    if (objVenta.Movimientos__r.size() == 1){
                        objMovInv = objVenta.Movimientos__r.get(0);
                        if (objMovInv.Trans_Type__c == 'RDR')
                           intTotMov = 1;
                        if (objMovInv.Trans_Type__c == 'RVRSL')
                           intTotMov = -1;
                    }//Fin si objVenta.Movimientos__r.size() == 1
                    //Tiene mas de un mov    
                    if (objVenta.Movimientos__r.size() > 1){
                        //Toma el ultimo mov
                        objMovInv = TAM_ActTotVtaInvBch_cls.getUltMov(objVenta.Movimientos__r);
                        //Recorre la lista de reg del tipo Movimiento__c
                        for (Movimiento__c objMov : objVenta.Movimientos__r){
                            System.debug('EN TAM_ActFechaDDChkOutBch_cls Fleet__c: ' + objMov.Fleet__c + ' intTotMov: ' + objMov.Qty__c + ' Trans_Type__c: ' + objMov.Trans_Type__c + ' Submitted_Date__c: ' + objMov.Submitted_Date__c + ' Submitted_Time__c: ' + objMov.Submitted_Time__c);
                            if (objMov.Trans_Type__c == 'RDR')
                                intTotMov += 1;
                            if (objMov.Trans_Type__c == 'RVRSL')
                                intTotMov += -1;
                        }//Fin del for para objVenta.Movimientos__r
                    }//Fin si objVenta.Movimientos__r.size() > 1
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls intTotMov: ' + intTotMov + ' objMovInv2: ' + objMovInv);
	                //Inicializa el objeto del tipo mov
	                TAM_CheckOutDD__c objCheckOutDD;
	                //Crea el id externo para el objeto de TAM_CheckOutDD__c
	                //String sIdExterno = objChkOut.TAM_VIN__c + '-' + objMovInv.Month__c + '-' + objMovInv.Year__c + '-' + objMovInv.Submitted_Date__c + '-' + objMovInv.Submitted_Time__c + '-' + sRectorTypeCheckOutInv;
	                String sIdExterno = objChkOut.TAM_VIN__c + '-' + objMovInv.Month__c + '-' + objMovInv.Year__c + '-' + sRectorTypeCheckOutInv;
	                System.debug('EN TAM_ActFechaDDChkOutBch_cls sIdExterno2: ' + sIdExterno);         
	                objCheckOutDD = new TAM_CheckOutDD__c(Name = sIdExterno, TAM_IDExterno__c = sIdExterno, TAM_CheckOut__c = objChkOut.id);                       
	                //Crea el objeto del tipo TAM_CheckOutDD__c
	                objCheckOutDD.Name = sIdExterno;
	                objCheckOutDD.TAM_IDExterno__c = sIdExterno;
	                objCheckOutDD.TAM_VIN__c = objMovInv.VIN__c;
	                objCheckOutDD.TAM_MesCalToyota__c = objMovInv.Month__c;
	                objCheckOutDD.TAM_AnioCalToyota__c = objMovInv.Year__c; 
	                objCheckOutDD.TAM_CheckOut__c = objChkOut.id;
	                objCheckOutDD.RecordTypeId = sRectorTypeCheckOutInv;
	                objCheckOutDD.TAM_TipoMov__c = objMovInv.Trans_Type__c;
	                objCheckOutDD.TAM_SubmittedDate__c = objMovInv.Submitted_Date__c;
	                objCheckOutDD.TAM_SubmittedTime__c = objMovInv.Submitted_Time__c;   
	                objCheckOutDD.TAM_Movimiento__c = objMovInv.ID;
	                objCheckOutDD.TAM_CountMov__c = intTotMov;
	                objCheckOutDD.TAM_Escenario__c = 'Se encuentra en CheckOut y Dealer Daily';    
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls objMovInv2: ' + objMovInv);         
	                //Ve si se trata de objVenta.Fleet__c = 'N'
	                if ( objMovInv.Fleet__c == 'N' || Test.isRunningTest()){
	                   //Agregalo al mapa de mapCheckOutDDUps
	                   mapCheckOutDDUpsFinal.put(sIdExterno, objCheckOutDD);
	                   //Crea un ID Externo para el VIN sin Mov
	                   Date dtFechaCierreSol =  objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c != null ? objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c : null;                
                       String sIdExternoSinVin = objChkOut.TAM_VIN__c + '-SINMOV-' + (dtFechaCierreSol != null ? String.valueOf(dtFechaCierreSol) : '') + '-' + sRectorTypeCheckOutInv;
	                   //Agregalo al set de setCheckVinCons
	                   setCheckVinCons.add(sIdExternoSinVin);
	                }//Fin si objVenta.Fleet__c == 'N'
                }//Fin si !objVenta.Movimientos__r.isEmpty()
                
                //Obten el ultimo mov del mes de la clase TAM_ActTotVtaInvBch_cls.getUltMov
                if (objVenta.Movimientos__r.isEmpty()){
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls NO TIENE MOVS EN LA FECHA SELECCIONADA3...');
	               //Ve si no exste en ventas
	               if (objChkOut.TAM_TipoVenta__c  == 'Inventario'){
	                   //Crea el objeto del tipo TAM_CheckOutDD__c
	                   TAM_CheckOutDD__c objCheckOutDD;
	                   Date dtFechaCierreSol =  objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c != null ? objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c : null;                
	                   String sIdExterno = objChkOut.TAM_VIN__c + '-SINMOV-' + (dtFechaCierreSol != null ? String.valueOf(dtFechaCierreSol) : '') + '-' + sRectorTypeCheckOutInv;
	                   objCheckOutDD = new TAM_CheckOutDD__c(Name = sIdExterno, TAM_IDExterno__c = sIdExterno, TAM_CheckOut__c = objChkOut.id );
	                   System.debug('EN TAM_ActFechaDDChkOutBch_cls objCheckOutDD3: ' + objCheckOutDD);
	                   //Crea el objeto del tipo TAM_CheckOutDD__c
	                   objCheckOutDD.TAM_VIN__c = null;
	                   objCheckOutDD.TAM_MesCalToyota__c = null;
	                   objCheckOutDD.TAM_AnioCalToyota__c = null;
	                   objCheckOutDD.RecordTypeId = sRectorTypeCheckOutInv;
	                   objCheckOutDD.TAM_Escenario__c = 'Se encuentra en CheckOut y No se encuentra en Dealer Daily';
                       System.debug('EN TAM_ActFechaDDChkOutBch_cls dtFechsIniNoInv0: ' + dtFechsIniNoInv + ' dFechaFinConsultaMov: ' + dFechaFinConsultaMov + ' dtFechaCierreSol: ' + dtFechaCierreSol);
	                   //Agregalo al mapa de mapCheckOutDDUps
	                   if (dtFechsIniNoInv <= dtFechaCierreSol && dFechaFinConsultaMov >= dtFechaCierreSol)
                           mapCheckOutDDUpsSinMov.put(sIdExterno, objCheckOutDD);
	                   if (dtFechsIniNoInv > dtFechaCierreSol)
	                       System.debug('EN TAM_ActFechaDDChkOutBch_cls la fecha de inicio del mes toyota es mayor a la fecha de cierre de la solicitud dtFechsIniNoInv0: ' + dtFechsIniNoInv + ' dtFechaCierreSol: ' + dtFechaCierreSol);                        
	               }//Fin si objChkOut.TAM_EstatusDealerSolicitud__c != 'Cancelada' && objChkOut.TAM_TipoVenta__c  == 'Inventario'
                }//Fin si objVenta.Movimientos__r.isEmpty()
                                
            }//Fin del for para Vehiculo__c            
            
            //Ve si no exste en ventas
            if (!blnExisteVenta || Test.isRunningTest()){
               //Ve si no exste en ventas
               if (objChkOut.TAM_TipoVenta__c  == 'Inventario'){
                   Boolean blnFleetC = false; 
                   //Va si tiene mov en la sol tiene un mov y es diferente de fleet C
                   for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOut : [Select id, 
                        TAM_UltMovCalToyota__c, TAM_UltMovCalToyota__r.Fleet__c 
                        From TAM_CheckOutDetalleSolicitudCompra__c Where ID =:objChkOut.id]){
                       System.debug('EN TAM_ActFechaDDChkOutBch_cls objCheckOut3.Fleet__c: ' + objCheckOut.TAM_UltMovCalToyota__r.Fleet__c);
                       //Si tiene un mov asociado
                       if (objCheckOut.TAM_UltMovCalToyota__c != null)
                           if (objCheckOut.TAM_UltMovCalToyota__r.Fleet__c == 'C')                    
                            blnFleetC = true;
                   } 
                   System.debug('EN TAM_ActFechaDDChkOutBch_cls blnFleetC3: ' + blnFleetC);
	               TAM_CheckOutDD__c objCheckOutDD;
                   Date dtFechaCierreSol =  objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c != null ? objChkOut.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c : null;	               
	               String sIdExterno = objChkOut.TAM_VIN__c + '-SINMOV-' + (dtFechaCierreSol != null ? String.valueOf(dtFechaCierreSol) : '') + '-' + sRectorTypeCheckOutInv;
	               objCheckOutDD = new TAM_CheckOutDD__c(Name = sIdExterno, TAM_IDExterno__c = sIdExterno, TAM_CheckOut__c = objChkOut.id );
                   System.debug('EN TAM_ActFechaDDChkOutBch_cls objCheckOutDD3: ' + objCheckOutDD);
                   //Crea el objeto del tipo TAM_CheckOutDD__c
                   objCheckOutDD.TAM_VIN__c = null;
                   objCheckOutDD.TAM_MesCalToyota__c = null;
                   objCheckOutDD.TAM_AnioCalToyota__c = null;
                   objCheckOutDD.RecordTypeId = sRectorTypeCheckOutInv;
                   objCheckOutDD.TAM_Escenario__c = 'Se encuentra en CheckOut y No se encuentra en Dealer Daily';
                   System.debug('EN TAM_ActFechaDDChkOutBch_cls dtFechsIniNoInv1: ' + dtFechsIniNoInv + ' dFechaFinConsultaMov: ' + dFechaFinConsultaMov + ' dtFechaCierreSol: ' + dtFechaCierreSol);
                   //Agregalo al mapa de mapCheckOutDDUps es Fleet N
                   if (!blnFleetC && dtFechsIniNoInv <= dtFechaCierreSol && dFechaFinConsultaMov >= dtFechaCierreSol)
                        mapCheckOutDDUpsSinMov.put(sIdExterno, objCheckOutDD);
                   if (!blnFleetC && dtFechsIniNoInv > dtFechaCierreSol)
                       System.debug('EN TAM_ActFechaDDChkOutBch_cls la fecha de inicio del mes toyota es mayor a la fecha de cierre de la solicitud dtFechsIniNoInv1: ' + dtFechaCierreSol + ' dtFechaCierreSol: ' + dtFechaCierreSol);
               }//Fin si objChkOut.TAM_EstatusDealerSolicitud__c != 'Cancelada' && objChkOut.TAM_TipoVenta__c  == 'Inventario'
            
            }//Fin si !blnExisteVenta && TAM_EstatusDealerSolicitud__c != 'Cancelada' && TAM_TipoVenta__c  == 'Inventario'
        
        }//Fin del for para la lista de TAM_CheckOutDetalleSolicitudCompra__c
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapFechaDDUpd: ' + mapFechaDDUpd.keySet());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapFechaDDUpd: ' + mapFechaDDUpd.values());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDUpsFinal: ' + mapCheckOutDDUpsFinal.keySet());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDUpsFinal: ' + mapCheckOutDDUpsFinal.values());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDUpsSinMov: ' + mapCheckOutDDUpsSinMov.keySet());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDUpsSinMov: ' + mapCheckOutDDUpsSinMov.values());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls setCheckVinCons: ' + setCheckVinCons);  
        
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        

        //Ve si tiene algo la lista de mapFechaDDUpd
        if (!mapFechaDDUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapFechaDDUpd.values(), TAM_CheckOutDetalleSolicitudCompra__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess())
                    setDodAsig.add(objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapFechaDDUpd.isEmpty()
        System.debug('EN TAM_ActFechaDDChkOutBch_cls setDodAsig: ' + setDodAsig);         
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapCheckOutDDUpsFinal.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapCheckOutDDUpsFinal.values(), TAM_CheckOutDD__c.TAM_IDExterno__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess())
                    setCheckOutAsig.add(objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()
        System.debug('EN TAM_ActFechaDDChkOutBch_cls setCheckOutAsig: ' + setCheckOutAsig);

        //Ve si tiene algo la lista de mapFechaDDUpd
        if (!mapCheckOutDDUpsSinMov.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapCheckOutDDUpsSinMov.values(), TAM_CheckOutDD__c.TAM_IDExterno__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess()){
                   setDodAsig.Add(objDtbUpsRes.getId());
                   lError.add(new TAM_LogsErrores__c( TAM_Proceso__c = 'Tipo Venta Inventario Sin Mov',
                        TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'En TAM_CheckOutDD__c eñ ID: ' + objDtbUpsRes.getId())
                   );                    
                   System.debug('EN TAM_ActFechaDDChkOutBch_cls Los datos de los Modelos seleccionados se crear/Actualizar con exito');
                }//Fin si objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUpsSinMov.isEmpty()
        System.debug('EN TAM_ActFechaDDChkOutBch_cls setDodAsig: ' + setDodAsig);         

        //Un mapa para la conulta de los que no tienen mov        
        Map<String, TAM_CheckOutDD__c> mapChekoutDel = new Map<String, TAM_CheckOutDD__c>();
        //Consulta los reg del obj TAM_CheckOutDD__c donde el IDEXTERNO contenga SINMOV
        for (TAM_CheckOutDD__c objCchekOut : [Select id, TAM_IDExterno__c From TAM_CheckOutDD__c
            Where TAM_IDExterno__c LIKE :'%SINMOV%' And RecordTypeId =:sRectorTypeCheckOutInv]){
            String[] lIdExterno = objCchekOut.TAM_IDExterno__c.split('-');
            //Agregalo al mapa de mapChekoutDel
            mapChekoutDel.put(objCchekOut.TAM_IDExterno__c, objCchekOut);
        }
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapChekoutDel: ' + mapChekoutDel.keySet());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapChekoutDel: ' + mapChekoutDel.values());
        
        Map<String, TAM_CheckOutDD__c> mapCheckOutDDDel = new  Map<String, TAM_CheckOutDD__c>();
        //Recorre el mapa de mapChekoutDel y busca la llave en setCheckVinCons
        for (String sLlavePaso : mapChekoutDel.KeySet()){
            //Ve si sLlavePaso esta en setCheckVinCons
            if (setCheckVinCons.contains(sLlavePaso)){
                System.debug('EN TAM_ActFechaDDChkOutBch_cls FIN sLlavePaso: ' + sLlavePaso);
                //Metelo al mapa de mapCheckOutDDDel
                mapCheckOutDDDel.put(mapChekoutDel.get(sLlavePaso).id, New TAM_CheckOutDD__c(id = mapChekoutDel.get(sLlavePaso).id ));
                //setDodAsig.Add(objDtbUpsRes.getId());
                lError.add(new TAM_LogsErrores__c( TAM_Proceso__c = 'Delete Tipo Venta Inventario Sin Mov',
                        TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'En TAM_CheckOutDD__c eñ ID: ' + mapChekoutDel.get(sLlavePaso).TAM_IDExterno__c) );            
            }//Fin si setCheckVinCons.contains(sLlavePaso)
        }//Fin del for para mapChekoutDel.KeySet()
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDDel: ' + mapCheckOutDDDel.keySet());
        System.debug('EN TAM_ActFechaDDChkOutBch_cls mapCheckOutDDDel: ' + mapCheckOutDDDel.values());
        //Elimina los reg
        delete mapCheckOutDDDel.values();
        
        //Roleback a todo
        //Database.rollback(sp);
        
        //Ve si hay errores          
        if (!lError.isEmpty())
            insert lError;
            
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActFechaDDChkOutBch_cls.finish Hora: ' + DateTime.now());      
    } 
        
}