global class TAM_ScheduledlUltimoMovimiento  implements Schedulable {
    global void execute(SchedulableContext sc)
        
    {
        TAM_BatchClassUltimoMov ultimoMovimiento = new TAM_BatchClassUltimoMov();
        database.executeBatch(ultimoMovimiento);
        
    }
}