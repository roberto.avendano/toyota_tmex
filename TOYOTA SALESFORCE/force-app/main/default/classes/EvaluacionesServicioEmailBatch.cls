global class EvaluacionesServicioEmailBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global EvaluacionesServicioEmailBatch(String q) {
		this.query = q;	
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
		List<EmailTemplate> let = [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE DeveloperName='ReporteResultadoEvaluacionServicio' limit 1];
		if(let.size()>0){
			EmailTemplate emailTemplate = let.get(0);
			for(Evaluaciones_Dealer__c evalDealer: (List<Evaluaciones_Dealer__c>) scope){
				Set<String> uniqueEmail = new Set<String>();
				List<String> listSendEmail = new List<String>();
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		        //uniqueEmail.add(evalDealer.Owner.Email);
		        if(evalDealer.Nombre_Dealer__r!=null && evalDealer.Nombre_Dealer__r.Promotor_Kaizen__r!=null){
		        	uniqueEmail.add(evalDealer.Nombre_Dealer__r.Promotor_Kaizen__r.Email);
		        }	        
		        
		        listSendEmail.addAll(uniqueEmail);
		        if(listSendEmail.size()>0){
	        		mail.setToAddresses(listSendEmail);
	        	}
	        	mail.setTargetObjectId(evalDealer.OwnerId);
	        	mail.setWhatId(evalDealer.Id);
	        	mail.setSaveAsActivity(false);
	        	mail.setTemplateId(emailTemplate.Id);        	
				allmsg.add(mail);
			}
			System.debug(allmsg);
			Messaging.sendEmail(allmsg,false);
		}else{
			System.debug('No existe en template ReporteResultadoEvaluacionServicio');
			throw new ToyotaException('No existe en template ReporteResultadoEvaluacionServicio');
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('Completado');
	}
	
}