@isTest
public class TAM_GestorLeadsCRM_Test {
    @testSetup 
    static void setup() {
        //Usuario Admin
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        
        //RecordId Dealer
        String recordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId(); 
        
        //Cuenta
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            TAM_GestorLeads__c = thisUser.id,
            recordTypeId = recordTypeId
        );
        insert a01;
        
        //Contacto
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a01.id;
        insert contactRH1;
        
        //Lead inventario
        Lead l1 = new Lead();
        l1.FirstName = 'Test1';
        l1.FWY_Intencion_de_compra__c = 'Este mes'; 
        l1.Email = 'a@a.com';
        l1.phone = '5571123596';
        l1.Status='Nuevo Lead';
        l1.LastName = 'Test2';
        l1.TAM_NumeroContactoPorWhatsApp__c = null;
        l1.TAM_DatosValidosFacturacion__c = false;
        insert L1;
       
        //Se Inserta un Usuario
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia
            //UserRole uR = [SELECT Id FROM UserRole WHERE Name='TOYOTA DE MONTERREY Socio Ejecutivo'];
            UserRole uR;
            for (UserRole uRPaso : [SELECT Id FROM UserRole WHERE Name='TOYOTA DE MONTERREY Socio Ejecutivo']){
                uR = uRPaso;
            }

            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,ContactId = contactRH1.id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
        }
    }
    
    //Metodo de prueba 1
    @isTest static void testMethod1(){
        Test.startTest();
	        User user = [Select Id  From User WHERE LastName =  'Testing1'];
	        Account acct = TAM_GestorLeadsCRM.getGestorIdByAccount(user.id);
	        User objUserPaso = TAM_GestorLeadsCRM.getUsuarioRedirect('57000');
	        system.assertEquals(acct,acct);
        Test.stopTest();        
    }

}