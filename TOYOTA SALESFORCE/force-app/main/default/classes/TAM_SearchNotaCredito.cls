/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Buscardor de notas de credito para intereses de autos demo TFS
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		11/Agosto/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
public without sharing class TAM_SearchNotaCredito {

    //Constantes
    static final String STRING_PUE = 'PUE';
    static final String STRING_TOYOTA_MOTOR = 'TOYOTA MOTOR SALES DE MEXICO S. DE R.L. DE C.V.';

    @AuraEnabled
    public static List <wrapVIN > vinNotaCredito(String searchKeyWord,String userId) {
        User[] usr;
        List<wrapVIN> wrapListVIN = new List<wrapVIN>();
        List <Movimiento__C > returnList = new List <Movimiento__C > ();
        Date fechaHoy = system.today();
        String searchKey = searchKeyWord + '%';
        usr = [Select id,CodigoDistribuidor__c FROM User where id =:userId];
        
        //Consultar en Solicitudes de Auto Demo
        List<TAM_Notas_Credito__c> lstNotaCredito = [SELECT TAM_Folio__c,TAM_Codigo_Distribuidor__c,SubTotal__c
                                                    FROM 	TAM_Notas_Credito__c
                                                    WHERE   TAM_Folio__c LIKE:searchKey  
                                                    AND     TAM_Codigo_Distribuidor__c =: usr[0].CodigoDistribuidor__c
                                                    AND     TAM_MetodoPago__c =: STRING_PUE
                                                    AND     TAM_NombreReceptor__c =: STRING_TOYOTA_MOTOR
                                                    LIMIT 5];
        if(!lstNotaCredito.isEmpty()){
            for (TAM_Notas_Credito__c  objNota: lstNotaCredito) {
                wrapVIN newVIN = new wrapVIN();
                newVIN.codigoDealer = objNota.TAM_Codigo_Distribuidor__c;
                newVIN.folio = objNota.TAM_Folio__c;
                newVIN.dblSubTotal = objNota.SubTotal__c.setScale(1, RoundingMode.HALF_UP);
                newVIN.boolNotaCreditoEncontrada = true;
                wrapListVIN.add(newVIN);
            }
        }
        return wrapListVIN;
    }

     //Wrapper
     public class wrapVIN {
        @AuraEnabled public String codigoDealer {get; set;}
        @AuraEnabled public String folio {get; set;}
        @AuraEnabled public Decimal dblSubTotal {get; set;}
        @AuraEnabled public Boolean boolNotaCreditoEncontrada {get; set;}
    }
}