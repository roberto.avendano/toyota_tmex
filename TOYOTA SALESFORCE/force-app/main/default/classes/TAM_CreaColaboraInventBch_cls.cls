/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para colaborar los registris que vienen de la clases
                        TAM_AdminVisibInventarioToyota.bpCreaColaboracion.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CreaColaboraInventBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sDealerColabora;
    
    //Un constructor por default
    global TAM_CreaColaboraInventBch_cls(string query, String sDealerColaboraParam){
        this.query = query;
        this.sDealerColabora = sDealerColaboraParam;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaboraInventBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_CreaColaboraInventBch_cls.');

        Set<String> setVodDealer = new Set<String>();
        Map<String, String> mapIdUsrNomDis = new Map<String, String>();
        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        Map<String, String> mapNoDealerNomGrupo = new Map<String, String>();
        List<TAM_InventarioVehiculosToyota__Share >  lInvenVehiculoShare = new List<TAM_InventarioVehiculosToyota__Share >();
        Map<String, set<String>> mapNoDelSelNomDeaDest = new Map<String, set<String>>();
         
        Set<String> setNomDealerOrigen = new Set<String>();
        Set<String> setNomDealer = new Set<String>();
        
        String sFleterosColaboracionEspecial = System.Label.TAM_FleterosColaboracionEspecial;
        String sCuernavacaColaboracionEspecial = System.Label.TAM_CuernavacaColaboracionEspecial;
        
        Map<String, TAM_AdminInventarioDealer__c> mapAdmInvUps = new Map<String, TAM_AdminInventarioDealer__c>();
        
        //Toma la variable de sDealerColabora y destripala para quw tomes los Dealers
        //57019, TOYOTA AGUASCALIENTES SUR;57213, TOYOTA DEL BAJÍO, TOYOTA GUNAJUATO;
        System.debug('EN TAM_CreaColaboraInventBch_cls sDealerColabora: ' + this.sDealerColabora);        
        String[] ssDealerColabora = this.sDealerColabora.split(';');
        //Recorre la lista de ssDealerColabora y toma los Distribuidores asocuados a cada Delaer
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            if (!mapNoDelSelNomDeaDest.containsKey(ssDealerColaboraFinal[0]))
                mapNoDelSelNomDeaDest.put(ssDealerColaboraFinal[0], new set<String>());                            
            setNomDealerOrigen.add(ssDealerColaboraFinal[0] + ' DistRelMod');
            for (String sDealerPaso : ssDealerColaboraFinal){
                String sNomDisPaso = sDealerPaso.trim(); //'\'' + sDealerPaso.trim() + '\'';
                setNomDealer.add(sNomDisPaso);
                //Agregalo al mapa de mapNoDelSelNomDeaDest
                mapNoDelSelNomDeaDest.get(ssDealerColaboraFinal[0]).add(sNomDisPaso); 
            }//Fin del for para ssDealerColaboraFinal
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls setNomDealerOrigen: ' + setNomDealerOrigen);
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls setNomDealer: ' + setNomDealer);           
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls mapNoDelSelNomDeaDest: ' + mapNoDelSelNomDeaDest);           
        
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvVhToy : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvVhToy.Dealer_Code__c != null)
                setVodDealer.add(objInvVhToy.Dealer_Code__c);
        }//Fin del for para scope
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls setVodDealer: ' + setVodDealer);           

        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Name IN :setNomDealer ORDER BY NAME]){
            String sName = Dealer.Name;    
            mapNoDealerNomGrupo.put(sName.toUpperCase(), Dealer.Codigo_Distribuidor__c);
        }
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.KeySet());           
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.Values());           
                
        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group WHERE (NAME LIKE '%TOYOTA%' OR NAME LIKE '%TMEX%') ORDER BY NAME]){
            String sNombreGrupo = Gropo.Name;
            mapNomGrupoIdGrupo.put(sNombreGrupo, Gropo.id);
            sNombreGrupo = Gropo.Name + ' ASESOR';
            mapNomGrupoIdGrupo.put(sNombreGrupo, Gropo.id);
        }
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.KeySet());           
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           

        //Crea la colaboracion para el reg 
        for(TAM_InventarioVehiculosToyota__c objInvenFyG : scope){
            //System.debug('ENTRO TAM_CreaColaboraInventBch_cls Dealer_Code__c: ' + objInvenFyG.Dealer_Code__c);            
            //Recorre la lista de setNomDealer para poder  crear la colaboracion del inventario de objInvenFyG.Dealer_Code__c
            Set<String> setDealerDest = new Set<String>();
            setDealerDest = mapNoDelSelNomDeaDest.get(objInvenFyG.Dealer_Code__c);
            //System.debug('ENTRO TAM_CreaColaboraInventBch_cls setDealerDest: ' + setDealerDest);
            //for (String sNomDealCola : setNomDealer){
            for (String sNoDealerPaso : setDealerDest){                
                //System.debug('ENTRO TAM_CreaColaboraInventBch_cls sNomDealCola: ' + sNomDealCola);
	            String sNombreGrupo = sNoDealerPaso; //mapNoDealerNomGrupo.containsKey(sNomDealCola) ? mapNoDealerNomGrupo.get(sNomDealCola) : ' ';
	            //System.debug('ENTRO TAM_CreaColaboraInventBch_cls sNombreGrupo: ' + sNombreGrupo.toUpperCase());
	            if (Test.isRunningTest())
	                sNombreGrupo = 'TOYOTA INNOVA FLETEROS';
	            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
	            TAM_InventarioVehiculosToyota__Share  objPasoShare = new TAM_InventarioVehiculosToyota__Share (
	                    ParentId = objInvenFyG.id,
	                    UserOrGroupId = mapNomGrupoIdGrupo.get(sNombreGrupo.toUpperCase()),
	                    AccessLevel = 'Edit'
	            );
	            
	            //Agregalo a la lista lInvenVehiculoShare
	            if (mapNomGrupoIdGrupo.containsKey(sNombreGrupo.toUpperCase())){
	               lInvenVehiculoShare.add(objPasoShare);
	               System.debug('ENTRO TAM_CreaColaboraInventBch_cls objPasoShare: ' + objPasoShare); 
	            }//Fin si mapNomGrupoIdGrupo.containsKey(sNombreGrupo)
	            
	            //Ve si se trata de Inova Fleteros y el inventario del tipo G
	            String sNombreGrupo2 = sNoDealerPaso + ' ASESOR'; //mapNoDealerNomGrupo.containsKey(objInvenFyG.Dealer_Code__c) ? mapNoDealerNomGrupo.get(objInvenFyG.Dealer_Code__c) + ' ASESOR': ' ';           
	            //System.debug('ENTRO TAM_CreaColaboraInventBch_cls sNombreGrupo2: ' + sNombreGrupo2);
	            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
	            TAM_InventarioVehiculosToyota__Share  objPasoShareAsesor = new TAM_InventarioVehiculosToyota__Share (
	                ParentId = objInvenFyG.id,
	                UserOrGroupId = mapNomGrupoIdGrupo.get(sNombreGrupo2.toUpperCase()),
	                AccessLevel = 'Edit'
	            );
	            
	            //Agregalo a la lista lInvenVehiculoShare
                if (mapNomGrupoIdGrupo.containsKey(sNombreGrupo2.toUpperCase())){
	               lInvenVehiculoShare.add(objPasoShareAsesor);
                   System.debug('ENTRO TAM_CreaColaboraInventBch_cls objPasoShareAsesor: ' + objPasoShareAsesor);
                }//Fin si mapNomGrupoIdGrupo.containsKey(sNombreGrupo2)
            }//Fin del for para setVodDealer
        }//Fin del for para el scope
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls lInvenVehiculoShare IdNomdis: ' + lInvenVehiculoShare);           
        
        //Cosnulta los reg asociados a setNomDealerOrigen en TAM_AdminInventarioDealer__c
        for (TAM_AdminInventarioDealer__c objAdmInvDealer : [Select id, Name, TAM_IdExterno__c 
            From TAM_AdminInventarioDealer__c Where TAM_IdExterno__c IN :setNomDealerOrigen]){
            //Agregalo al mapa de mapAdmInvUps
            mapAdmInvUps.put(objAdmInvDealer.TAM_IdExterno__c, new TAM_AdminInventarioDealer__c(
                    id = objAdmInvDealer.id,
                    TAM_Procesar__c = true,
                    TAM_Procesada__c = false
                )
            );
        }
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls lInvenVehiculoShare mapAdmInvUps: ' + mapAdmInvUps);           
        
        Boolean blnError = false;
        Set<String> setIdOkCol = new Set<String>();

        Set<String> setDealDel = new Set<String>();
        Map<String, TAM_AdminInventarioDealer__c> mapAdminInvDel = new Map<String, TAM_AdminInventarioDealer__c>();
        
        //Recorre la lista de arrslDealesDelFinalPrmFinal para crear los reg que se van a eliminar
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            System.debug('ENTRO TAM_CreaColaboraInventBch_cls sNoDealerPaso: ' + sNoDealerPaso + ' size: ' + ssDealerColaboraFinal.size());
            if (ssDealerColaboraFinal.size() > 1)
                if (ssDealerColaboraFinal[1] == null || ssDealerColaboraFinal[1] == '')
                    setDealDel.add(ssDealerColaboraFinal[0] + ' DistRelMod');
            if (ssDealerColaboraFinal.size() == 1)
                setDealDel.add(ssDealerColaboraFinal[0] + ' DistRelMod');    
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls setNomDealerOrigen: ' + setNomDealerOrigen);

        for (TAM_AdminInventarioDealer__c objAdmInv : [Select id, TAM_IdExterno__c 
            From TAM_AdminInventarioDealer__c Where TAM_IdExterno__c IN :setDealDel]){
            //Metelo al mapa de mapAdminInvDel
            mapAdminInvDel.put(objAdmInv.TAM_IdExterno__c, new TAM_AdminInventarioDealer__c(id = objAdmInv.id));
        }
        System.debug('EN TAM_CreaColaboraInventBch_cls mapAdminInvDel: ' + mapAdminInvDel);        
        
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();
        
        if (!lInvenVehiculoShare.isEmpty()){
            //Actualiza las Opp 
            List<Database.Saveresult> lDtbUpsRes = Database.insert(lInvenVehiculoShare, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()

        if (!mapAdmInvUps.isEmpty()){
            //Actualiza las Opp 
            List<Database.UpsertResult> lDtbUpsRes = Database.upsert(mapAdmInvUps.values(), TAM_AdminInventarioDealer__c.id, false);
            //Ve si hubo error
            for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
                if (objDtbUpsRes.isSuccess())                    
                    setIdOkCol.add(objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()
        System.debug('ENTRO TAM_CreaColaboraInventBch_cls lInvenVehiculoShare setIdOkCol: ' + setIdOkCol);           

        /*List<Database.DeleteResult> ldtSvrDel = Database.delete(mapAdminInvDel.values(), false);
        for(Database.DeleteResult objSvrDel : ldtSvrDel){
           if (!objSvrDel.isSuccess()){
                System.debug('Hubo un error a la hora de eliminar el objeto TAM_AdminInventarioDealer__c: ' + objSvrDel.getErrors()[0].getMessage());
                blnError = true;
           }//Fin si !objSvrDel.isSuccess()
        }*/

        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);

    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaboraInventBch_cls.finish Hora: ' + DateTime.now());      
    } 

    
}