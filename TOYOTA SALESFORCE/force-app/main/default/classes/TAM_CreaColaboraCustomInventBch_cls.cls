/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para eliminar la colaboración de los registros 
                        que vienen de la clases TAM_AdminVisibInventarioToyota.bpDepuraColaboracionDealer.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    08-Junio-2021        Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CreaColaboraCustomInventBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global string strCodDealer;
    global List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerPrm;
    global Boolean blnDepuraColaboraReglas;
    global Boolean blnDepuraGruposReglasRel;
    
    //Un constructor por default
    global TAM_CreaColaboraCustomInventBch_cls(string query, string strCodDealer, 
        List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerPrm, Boolean blnDepuraColaboraReglas,
        Boolean blnDepuraGruposReglasRel){
        this.query = query;
        this.strCodDealer = strCodDealer;
        this.lAdminInventarioDealerPrm = lAdminInventarioDealerPrm;
        this.blnDepuraColaboraReglas = blnDepuraColaboraReglas;
        this.blnDepuraGruposReglasRel = blnDepuraGruposReglasRel;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.');

        Boolean blnPermiteColaborarInventario = (System.Label.TAM_ColaborarInventario != null && System.Label.TAM_ColaborarInventario != '') 
            ? Boolean.valueOf(System.Label.TAM_ColaborarInventario) : false ;        
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls blnPermiteColaborarInventario: ' + blnPermiteColaborarInventario);           

        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        Map<String, String> mapNoDealerNomGrupo = new Map<String, String>();        
        List<TAM_InventarioVehiculosToyota__Share> lInvenVehiculoShare = new List<TAM_InventarioVehiculosToyota__Share>();
        List<TAM_AdminInventarioDealer__c> lDistAsociado = new List<TAM_AdminInventarioDealer__c>();
        Map<String, List<TAM_InventarioVehiculosToyota__c>> MapNoCaveDeaObjInvToy = new Map<String, List<TAM_InventarioVehiculosToyota__c>>(); 
        Set<String> setIdInvSel = new Set<String>();
        Set<String> setNomDistRel = new Set<String>();

        String sNameDist = '';   
        Boolean blnError = false;
        Boolean blnAplicaColDefault = false;
                      
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvenFyG : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvenFyG.Dealer_Code__c != null)
                setIdInvSel.add(objInvenFyG.id);
            if (MapNoCaveDeaObjInvToy.containsKey(objInvenFyG.Dealer_Code__c))
                MapNoCaveDeaObjInvToy.get(objInvenFyG.Dealer_Code__c).add(objInvenFyG);
            if (!MapNoCaveDeaObjInvToy.containsKey(objInvenFyG.Dealer_Code__c))
                MapNoCaveDeaObjInvToy.put(objInvenFyG.Dealer_Code__c, new List<TAM_InventarioVehiculosToyota__c>{objInvenFyG});                      
        }//Fin del for para scope
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setIdInvSel: ' + setIdInvSel);           
        
        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Codigo_Distribuidor__c =:this.strCodDealer ORDER BY NAME]){
            sNameDist = Dealer.Name; 
            mapNoDealerNomGrupo.put(Dealer.Codigo_Distribuidor__c, sNameDist.toUpperCase());
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNameDist: ' + sNameDist);           
        }

        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group WHERE (NAME LIKE '%TOYOTA%' OR NAME LIKE '%TMEX%') ORDER BY NAME]){
            String sNombreGrupo = Gropo.Name;
            mapNomGrupoIdGrupo.put(sNombreGrupo, Gropo.id);
            sNombreGrupo = Gropo.Name + ' ASESOR';
            mapNomGrupoIdGrupo.put(sNombreGrupo, Gropo.id);
        }
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.KeySet());           
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           
        
        //Si es una prueba
        if(Test.isRunningTest()) this.blnDepuraGruposReglasRel = false;
        
        //Ve si no depura todo this.blnDepuraGruposReglasRel
        if (!this.blnDepuraGruposReglasRel){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls NO APLICA blnDepuraGruposReglasRel: ' + this.blnDepuraGruposReglasRel);                                                                 

        Boolean blnTieneDistRel = false;
        String strDistRel = '';
        Boolean blnTieneRegDistRel = false;
        List<TAM_AdminInventarioDealer__c> lReglasDistRel = new List<TAM_AdminInventarioDealer__c>();
                    
        //Recorre la lista de this.lAdminInventarioDealerPrm y ve si tiene dist relacionados y ademas reglas
        for (TAM_AdminInventarioDealer__c objAdmInvDea : this.lAdminInventarioDealerPrm){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls objAdmInvDea: ' + objAdmInvDea);                         
            //Ve si tiene TAM_DistribuidorAsociado__c
            if (objAdmInvDea.TAM_DistribuidorAsociado__c != null) {
                strDistRel = objAdmInvDea.TAM_DistribuidorAsociado__c;
                blnTieneDistRel = true;
            }//Fin si objAdmInvDea.TAM_DistribuidorAsociado__c == null
            //Ve si tiene reglas
            if (objAdmInvDea.TAM_Funcion__c != null && objAdmInvDea.TAM_TipoInventario__c != null) {
                blnTieneRegDistRel = true;
                lReglasDistRel.add(objAdmInvDea);
            }//Fin si objAdmInvDea.TAM_Funcion__c != null && objAdmInvDea.TAM_TipoInventario__c != null
        }//Fin del for para this.lAdminInventarioDealerPrm
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls blnTieneDistRel: ' + blnTieneDistRel + ' blnTieneRegDistRel: ' + blnTieneRegDistRel);                         
        
        //Si es una prueba
        if(Test.isRunningTest()){ 
            blnTieneDistRel = true;
            blnTieneRegDistRel = false;
            strDistRel = 'Toyota Bajío Sur;';
        }//Test.isRunningTest()
        
        //Ahora si aplica la colaboración,ve si tiene dist rel pero no dio de alta reglas
        if (blnTieneDistRel && !blnTieneRegDistRel){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls ENTRO A TIENE DIST REL Y NO REGLAS...');                         
            //Toma la lista de Inv de MapNoCaveDeaObjInvToy
            List<TAM_InventarioVehiculosToyota__c> lInvVeicToyPaso = MapNoCaveDeaObjInvToy.get(this.strCodDealer);
            //Despedaza la cadena de TAM_DistribuidorAsociado__c por el caracter de ;
            String[] lDealerColabora = strDistRel.contains(';') ? strDistRel.split(';') : new List<String>();
            for (String sDealer : lDealerColabora){
                //Recorre la lista de lInvVeicToyPaso
                for (TAM_InventarioVehiculosToyota__c objInvVehToy : lInvVeicToyPaso){
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls objInvVehToy1: ' + objInvVehToy);
                    String sDealerPaso = sDealer.toUpperCase();
                    //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
                    TAM_InventarioVehiculosToyota__Share objPasoShare = new TAM_InventarioVehiculosToyota__Share (
                        ParentId = objInvVehToy.id, UserOrGroupId = mapNomGrupoIdGrupo.get(sDealerPaso), AccessLevel = 'Edit');
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sDealerPaso1 : ' + sDealerPaso + ' objPasoShare: ' + objPasoShare);
                    //Agregalo a la lista de lInvenVehiculoShare
                    if (blnPermiteColaborarInventario)
                       lInvenVehiculoShare.add(objPasoShare);                                 
            
                    sDealerPaso = sDealer.toUpperCase() + ' ASESOR';
                    //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
                    TAM_InventarioVehiculosToyota__Share objPasoShareAsor = new TAM_InventarioVehiculosToyota__Share (
                        ParentId = objInvVehToy.id, UserOrGroupId = mapNomGrupoIdGrupo.get(sDealerPaso), AccessLevel = 'Edit');                     
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sDealerPaso2 : ' + sDealerPaso + ' objPasoShareAsor: ' + objPasoShareAsor);
                    //Agregalo a la lista de lInvenVehiculoShare
                    if (blnPermiteColaborarInventario)
                        lInvenVehiculoShare.add(objPasoShareAsor);
                }//Fin si 
            }//Fin del for para lDealerColabora                        
        }//Fin si blnTieneDistRel && !blnTieneDistRel

        //Si es una prueba
        if(Test.isRunningTest()){ 
            blnTieneDistRel = false;
            blnTieneRegDistRel = true;
        }//Test.isRunningTest()
                    
        //Ahora si aplica la colaboración,ve si NO tiene dist rel pero SI dio de alta reglas
        if (!blnTieneDistRel && blnTieneRegDistRel){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls ENTRO Y NO TIENE DIST PER SI REGLAS...');                         
            //Ya tienes los Id de los inventarios que va a colaborar busca las funciones e inventarios que aplica
            for (TAM_AdminInventarioDealer__c objAdminInveDealer : lReglasDistRel){
                sNameDist = mapNoDealerNomGrupo.get(this.strCodDealer);
                //Toma las funciones primero TAM_Funcion__c, TAM_TipoInventario__c
                String sFuncionPsso = objAdminInveDealer.TAM_Funcion__c;
                String[] arrFunciones = sFuncionPsso != null ? sFuncionPsso.split(';') : new List<String>();
                Set<String> setFunciones = new Set<String>();
                Set<String> setUsersFunciones = new Set<String>();
                Set<String> setInventarios = new Set<String>();                       

		        //Si es una prueba
		        if(Test.isRunningTest()){ 
		            arrFunciones = new List<String>{'EJECUTIVO;','GESTOR;','ASESOR;'};
		            setInventarios = new Set<String>{'PRODUCCIÓN;','TRÁFICO;','PISO;'};
		        }//Test.isRunningTest()
                
                //Dale vuelta a la lista de arrFunciones
                for (String sFuncPaso : arrFunciones){
                    //Crea el nombre de la funcion 
                    String sNameFuncion;
                    String sFuncPasoPaso = sFuncPaso.toUpperCase();
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sFuncPasoPaso: ' + sFuncPasoPaso);           
                    if (sFuncPasoPaso == 'EJECUTIVO') 
                        sNameFuncion = sNameDist + ' Socio Ejecutivo';
                    if (sFuncPasoPaso == 'GESTOR') 
                        sNameFuncion = sNameDist + ' Socio Gestor';
                    if (sFuncPasoPaso == 'ASESOR') 
                        sNameFuncion = sNameDist + ' Socio Usuario';
                    //Agregalo al set de setFunciones                    
                    setFunciones.add(sNameFuncion);
                }//Fin del for para arrFunciones
                
                //Consulta los usuaros asociados a las funciones setFunciones 
                for (User usuario : [Select u.id, u.Name, u.CodigoDistribuidor__c, u.UserRole.Name 
                    From User u Where u.IsActive = true and UserRole.Name IN :setFunciones]){
                    //Metelos en el SET de setUsersFunciones
                    setUsersFunciones.add(usuario.id);
                }
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setUsersFunciones: ' + setUsersFunciones);
                
                //Ya tienes a los usuarios ve por el tipo de inventario que le aplica
                String[] arrInventario = objAdminInveDealer.TAM_TipoInventario__c != null ? objAdminInveDealer.TAM_TipoInventario__c.split(';') : new List<String>();
                //Dale vuelta a la lista de arrFunciones
                for (String sInventPaso : arrInventario){
                    //Crea el nombre de la funcion 
                    String sNameInvenPaso = '';
                    String sInventPasoPaso = sInventPaso.toUpperCase();
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sInventPasoPaso: ' + sInventPasoPaso);           
                    if (sInventPasoPaso == 'PRODUCCIÓN') 
                        sNameInvenPaso = 'A';
                    if (sInventPasoPaso == 'TRÁFICO') 
                        sNameInvenPaso = 'F';
                    if (sInventPasoPaso == 'PISO') 
                        sNameInvenPaso = 'F';
                    //Agregalo al set de setFunciones                    
                    setInventarios.add(sNameInvenPaso);
                }//Fin del for para arrFunciones
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setInventarios: ' + setInventarios);
                            
                //Toma la lista de Inv de MapNoCaveDeaObjInvToy
                List<TAM_InventarioVehiculosToyota__c> lInvVeicToyPaso = MapNoCaveDeaObjInvToy.get(this.strCodDealer);
                Set<String> setIdInvToyPaso = setIdInvToy(lInvVeicToyPaso);
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setIdInvToyPaso0: ' + setIdInvToyPaso);
                //Consulta los inventarios que vienen en 
                for (TAM_InventarioVehiculosToyota__c objInvPaso : [Select id From TAM_InventarioVehiculosToyota__c
                    Where TAM_TipoInventarioLetra__c IN: setInventarios And Id IN :setIdInvToyPaso]){
                    //Recorre la lista de usuarios activos para las funciones
                    for (String sUsrPaso : setUsersFunciones){
                        //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
                        TAM_InventarioVehiculosToyota__Share  objPasoShare = new TAM_InventarioVehiculosToyota__Share (
                            ParentId = objInvPaso.id,
                            UserOrGroupId = sUsrPaso,
                            AccessLevel = 'Edit'
                        );
                        //Metelo a la lista de lInvenVehiculoShare 
                        if (blnPermiteColaborarInventario)
                            lInvenVehiculoShare.add(objPasoShare);
                    }//Fin del for para setUsersFunciones
                }//Fin del for para TAM_InventarioVehiculosToyota__c
            }//Fin del for para this.lAdminInventarioDealerPrm                                       
        }//Fin si !blnTieneDistRel && blnTieneDistRel

        //Si es una prueba
        if(Test.isRunningTest()){ 
            blnTieneDistRel = true;
            blnTieneRegDistRel = true;
        }//Test.isRunningTest()
        
        //Ahora si aplica la colaboración, ve SI tiene dist rel y SI tiene reglas
        if (blnTieneDistRel && blnTieneRegDistRel){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls ENTRO TIENE DIST Y REGLAS strDistRel: ' + strDistRel);
            sNameDist = mapNoDealerNomGrupo.get(this.strCodDealer);
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls ENTRO TIENE DIST Y REGLAS sNameDist: ' + sNameDist);
            //Concatena al la lista de strDistRel el sNameDist 
            if (strDistRel == null)
                strDistRel = '';
            strDistRel += ';' + sNameDist + ';';
            //Despedaza la cadena de TAM_DistribuidorAsociado__c por el caracter de ;
            String[] lDealerColabora = strDistRel.contains(';') ? strDistRel.split(';') : new List<String>();
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls ENTRO TIENE DIST Y REGLAS strDistRel: ' + strDistRel + ' lDealerColabora: ' + lDealerColabora);                                                 
                        
            //La variables para los invent, las funciones y los usuarios finales
            Set<String> setInventarios = new Set<String>();                       
            Set<String> setFunciones = new Set<String>();
            Set<String> setUsersFunciones = new Set<String>();                        
            
            //Ya tienes los Id de los inventarios que va a colaborar busca las funciones e inventarios que aplica
            for (TAM_AdminInventarioDealer__c objAdminInveDealer : lReglasDistRel){
                //Recorre la lista de lDealerColabora
                for (String sNameDistPaso : lDealerColabora){
                    //Toma las funciones primero TAM_Funcion__c, TAM_TipoInventario__c
                    String sFuncionPsso = objAdminInveDealer.TAM_Funcion__c;
                    String[] arrFunciones = sFuncionPsso != null ? sFuncionPsso.split(';') : new List<String>();

		            //Si es una prueba
		            if(Test.isRunningTest()){ 
		                arrFunciones = new List<String>{'EJECUTIVO;','GESTOR;','ASESOR;'};
		                setInventarios = new Set<String>{'PRODUCCIÓN;','TRÁFICO;','PISO;'};
		            }//Test.isRunningTest()

                    //Dale vuelta a la lista de arrFunciones
                    for (String sFuncPaso : arrFunciones){
                        //Crea el nombre de la funcion 
                        String sNameFuncion;
                        String sFuncPasoPaso = sFuncPaso.toUpperCase();
                        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sFuncPasoPaso: ' + sFuncPasoPaso);           
                        if (sFuncPasoPaso == 'EJECUTIVO') 
                            sNameFuncion = sNameDistPaso + ' Socio Ejecutivo';
                        if (sFuncPasoPaso == 'GESTOR') 
                            sNameFuncion = sNameDistPaso + ' Socio Gestor';
                        if (sFuncPasoPaso == 'ASESOR') 
                            sNameFuncion = sNameDistPaso + ' Socio Usuario';
                        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNameFuncion: ' + sNameFuncion);           
                        //Agregalo al set de setFunciones                    
                        setFunciones.add(sNameFuncion);
                    }//Fin del for para arrFunciones
                    
                    //Consulta los usuaros asociados a las funciones setFunciones 
                    for (User usuario : [Select u.id, u.Name, u.CodigoDistribuidor__c, u.UserRole.Name 
                        From User u Where u.IsActive = true and UserRole.Name IN :setFunciones]){
                        //Metelos en el SET de setUsersFunciones
                        setUsersFunciones.add(usuario.id);
                    }//Fin del for para User
                    
                    //Ya tienes a los usuarios ve por el tipo de inventario que le aplica
                    String[] arrInventario = objAdminInveDealer.TAM_TipoInventario__c != null ? objAdminInveDealer.TAM_TipoInventario__c.split(';') : new List<String>();
                    //Dale vuelta a la lista de arrFunciones
                    for (String sInventPaso : arrInventario){
                        //Crea el nombre de la funcion 
                        String sNameInvenPaso = '';
                        String sInventPasoPaso = sInventPaso.toUpperCase();
                        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sInventPasoPaso: ' + sInventPasoPaso);           
                        if (sInventPasoPaso == 'PRODUCCIÓN') 
                            sNameInvenPaso = 'A';
                        if (sInventPasoPaso == 'TRÁFICO') 
                            sNameInvenPaso = 'F';
                        if (sInventPasoPaso == 'PISO') 
                            sNameInvenPaso = 'F';
                        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNameInvenPaso: ' + sNameInvenPaso);
                        //Agregalo al set de setFunciones                    
                        setInventarios.add(sNameInvenPaso);
                    }//Fin del for para arrFunciones
                }//Fin del for para 
                //System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setFunciones2: ' + setFunciones);                         
                //System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setInventarios2: ' + setInventarios);                         
                //System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setUsersFunciones2: ' + setUsersFunciones);                            
                //Toma la lista de Inv de MapNoCaveDeaObjInvToy
                List<TAM_InventarioVehiculosToyota__c> lInvVeicToyPaso = MapNoCaveDeaObjInvToy.get(this.strCodDealer);
                Set<String> setIdInvToyPaso = setIdInvToy(lInvVeicToyPaso);
                System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setIdInvToyPaso: ' + setIdInvToyPaso);
                //Consulta los inventarios que vienen en 
                for (TAM_InventarioVehiculosToyota__c objInvPaso : [Select id From TAM_InventarioVehiculosToyota__c
                    Where TAM_TipoInventarioLetra__c IN: setInventarios And Id IN :setIdInvToyPaso]){
                    System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls entro a la consulta de InventPaso2: ' + objInvPaso);                         
                    //Recorre la lista de usuarios activos para las funciones
                    for (String sUsrPaso : setUsersFunciones){
                        //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
                        TAM_InventarioVehiculosToyota__Share  objPasoShare = new TAM_InventarioVehiculosToyota__Share (
                            ParentId = objInvPaso.id,
                            UserOrGroupId = sUsrPaso,
                            AccessLevel = 'Edit'
                        );
                        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls entro a la consulta de objPasoShare2: ' + objPasoShare);                         
                        //Metelo a la lista de lInvenVehiculoShare 
                        if (blnPermiteColaborarInventario)
                            lInvenVehiculoShare.add(objPasoShare);
                    }//Fin del for para setUsersFunciones
                }//Fin del for para TAM_InventarioVehiculosToyota__c
                //System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls lInvenVehiculoShare1: ' + lInvenVehiculoShare);                                                     
            }//Fin del for para this.lAdminInventarioDealerPrm
        }//Fin si blnTieneDistRel && blnTieneDistRel

        //Ahora si aplica la colaboración, ve SI tiene dist rel y SI tiene reglas
        if (!blnTieneDistRel && !blnTieneRegDistRel)
            blnAplicaColDefault = true;

        //Si es una prueba
        if(Test.isRunningTest()) blnAplicaColDefault = true;

        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls lInvenVehiculoShare1: ' + lInvenVehiculoShare);                                                                 
        }//Fin si !this.blnDepuraGruposReglasRel


        //Ve si depura todo this.blnDepuraGruposReglasRel regresa a la colaboración por defautl    
        if (this.blnDepuraGruposReglasRel || blnAplicaColDefault){
            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls APLICA blnDepuraGruposReglasRel: ' + this.blnDepuraGruposReglasRel);                                                                 
                              
        //Agrega el distribuidor actual para que lo colabore
        setNomDistRel.add(mapNoDealerNomGrupo.get(this.strCodDealer));        
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls setNomDistRel: ' + setNomDistRel);           

        //Crea la colaboracion para el reg 
        for(TAM_InventarioVehiculosToyota__c objInvenFyG : scope){
            //Dale vuelta al set de setNomDistRel
            for (String sDistPaso : setNomDistRel){
	            //System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls Dealer_Code__c: ' + objInvenFyG.Dealer_Code__c);            
	            String sNombreGrupo = sDistPaso; //mapNoDealerNomGrupo.get(objInvenFyG.Dealer_Code__c); //mapNoDealerNomGrupo.containsKey(sNomDealCola) ? mapNoDealerNomGrupo.get(sNomDealCola) : ' ';
	            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNombreGrupo: ' + sNombreGrupo.toUpperCase());
	            if (Test.isRunningTest())
	                sNombreGrupo = 'TOYOTA INNOVA FLETEROS';
	            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
	            TAM_InventarioVehiculosToyota__Share  objPasoShare = new TAM_InventarioVehiculosToyota__Share (
	                    ParentId = objInvenFyG.id,
	                    UserOrGroupId = mapNomGrupoIdGrupo.get(sNombreGrupo.toUpperCase()),
	                    AccessLevel = 'Edit'
	            );
	                
	            //Agregalo a la lista lInvenVehiculoShare
	            if (mapNomGrupoIdGrupo.containsKey(sNombreGrupo.toUpperCase()) && (this.blnDepuraColaboraReglas || this.blnDepuraGruposReglasRel)){
	               lInvenVehiculoShare.add(objPasoShare);
	               System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls objPasoShare: ' + objPasoShare); 
	            }//Fin si mapNomGrupoIdGrupo.containsKey(sNombreGrupo)
	                
	            //Ve si se trata de Inova Fleteros y el inventario del tipo G
	            String sNombreGrupo2 = sNombreGrupo + ' ASESOR';           
	            System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls sNombreGrupo2: ' + sNombreGrupo2);
	            //crea el objeto del tipo TAM_InventarioVehiculosToyota__Share
	            TAM_InventarioVehiculosToyota__Share  objPasoShareAsesor = new TAM_InventarioVehiculosToyota__Share (
	                ParentId = objInvenFyG.id,
	                UserOrGroupId = mapNomGrupoIdGrupo.get(sNombreGrupo2.toUpperCase()),
	                AccessLevel = 'Edit'
	            );
	                
	            //Agregalo a la lista lInvenVehiculoShare
	            if (mapNomGrupoIdGrupo.containsKey(sNombreGrupo2.toUpperCase()) && (this.blnDepuraColaboraReglas || this.blnDepuraGruposReglasRel) ){
	               lInvenVehiculoShare.add(objPasoShareAsesor);
	               System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls objPasoShareAsesor: ' + objPasoShareAsesor);
	            }//Fin si mapNomGrupoIdGrupo.containsKey(sNombreGrupo2)
            }//Fin el for para setNomDistRel
        }//Fin del for para el scope
        System.debug('ENTRO TAM_CreaColaboraCustomInventBch_cls lInvenVehiculoShare2: ' + lInvenVehiculoShare);                                                                 

        }//Fin si this.blnDepuraGruposReglasRel

         
        //Ve si tiene algo la lista de lInvenVehiculoShare y crear los reg
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();
        
        if (!lInvenVehiculoShare.isEmpty()){
            //Actualiza las Opp 
            List<Database.Saveresult> lDtbUpsRes = Database.insert(lInvenVehiculoShare, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()

        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);        

    }

    //Llama el metodo de actualizaDod para actuelizar el tipo de invcentario en el objeto de 
    public static Set<String> setIdInvToy(List<TAM_InventarioVehiculosToyota__c> lstNewClienteVehiculo){
        System.debug('EN setIdInvToy...');
        Set<String> setIdInvToyPaso = new Set<String>();
        
        //Recorre la lista de TAM_InventarioVehiculosToyota__c
        for (TAM_InventarioVehiculosToyota__c objInvPaso : lstNewClienteVehiculo){
            setIdInvToyPaso.add(objInvPaso.id);
        }//Fin del for para lstNewClienteVehiculo
        
        System.debug('EN setIdInvToy setIdInvToyPaso: ' + setIdInvToyPaso);        
        return setIdInvToyPaso;
    }    
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.finish Hora: ' + DateTime.now());      
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.finish this.strCodDealer: ' + this.strCodDealer);      
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.finish this.lAdminInventarioDealerPrm: ' + this.lAdminInventarioDealerPrm);              
        List<TAM_AdminInventarioDealer__c> lAdminInventarioDealerDel = new List<TAM_AdminInventarioDealer__c>();
        
        //Si depuro todo entonces eliminda todo lo que hay en el objeto de 
        //Ve si tiene Dealer relacionados el this.strCodDealer en TAM_AdminInventarioDealer__c
        for (TAM_AdminInventarioDealer__c objAdminInventDealerPaso : [Select Id, TAM_IdExterno__c,
                TAM_CodigoDistribuidor__c, TAM_Funcion__c, TAM_TipoInventario__c, TAM_DistribuidorAsociado__c
                From TAM_AdminInventarioDealer__c Where TAM_CodigoDistribuidor__c =: this.strCodDealer]){
                String ssDistRel = objAdminInventDealerPaso.TAM_DistribuidorAsociado__c;
                String[] sDistRel = ssDistRel != null ? ssDistRel.split(';') : new List<String>();
                //No depura la informacion de las reglas y la informacion de los gurupos relacionados
                if (this.blnDepuraGruposReglasRel)
                    lAdminInventarioDealerDel.add(new TAM_AdminInventarioDealer__c(id = objAdminInventDealerPaso.id));
        }//Fin del for  TAM_AdminInventarioDealer__c
        System.debug('EN TAM_CreaColaboraCustomInventBch_cls.finish lAdminInventarioDealerDel: ' + lAdminInventarioDealerDel);      

        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();        
        
        //Ve si la lista de lAdminInventarioDealerDel y elimina
        delete lAdminInventarioDealerDel;
        
        //Para pruebas un rollback
        //Database.rollback(svInv);        
        
    } 
    
}