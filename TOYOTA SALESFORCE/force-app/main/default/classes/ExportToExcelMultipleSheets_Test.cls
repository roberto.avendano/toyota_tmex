@isTest
public class ExportToExcelMultipleSheets_Test {
    
    @testSetup static void setup() {
        
        //VIN de prueba
        Vehiculo__c v01 = new Vehiculo__c(
            Name='12345678901234567',
            Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        
        //Provisión de incentivos
        TAM_ProvisionIncentivos__c provTest01 = new TAM_ProvisionIncentivos__c();
        provTest01.name = 'Provisión Agosto de prueba';
        provTest01.TAM_AnioDeProvision__c = '2020';
        provTest01.TAM_MesDeProvision__c = 'Agosto';
        insert provTest01;
        
        //Detalle de Provisión
        String recordIdProvisionRetail  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        //Retail
        TAM_DetalleProvisionIncentivo__c detalleProv01 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv01.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv01.RecordTypeId = recordIdProvisionRetail;
        detalleProv01.RetailSinReversa_Cerrado__c = false;
        detalleProv01.TAM_AnioModelo__c = '2020';
        detalleProv01.TAM_AplicanAmbos__c = false;
        detalleProv01.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv01.TAM_CodigoContable__c = '7400B2';
        detalleProv01.TAM_CodigoDealer__c = '57002';
        detalleProv01.TAM_Factura__c = 'FVA12330';
        detalleProv01.TAM_FechaCierre__c = date.today();
        detalleProv01.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv01.TAM_FechaVenta__c = '2020-06-17';
        detalleProv01.TAM_FirstName__c = 'DORA LUZ';
        detalleProv01.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv01.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv01.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv01.TAM_Lealtad__c = false;
        detalleProv01.TAM_Modelo__c = '7495';
        detalleProv01.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv01.TAM_PagadoConTFS__c = false;
        detalleProv01.TAM_PagadoSinTFS__c = true;
        detalleProv01.TAM_ProvicionarEfectivo__c = true;
        detalleProv01.TAM_Provisionado__c = false;
        detalleProv01.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv01.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv01.TAM_ProvisionarLealtad__c = false;
        detalleProv01.TAM_ProvisionCerrada__c = true;
        detalleProv01.TAM_Serie__c	 = 'HILUX';
        detalleProv01.TAM_SolicitadoDealer__c = false;
        detalleProv01.TAM_TipoMovimiento__c = 'RDR';
        detalleProv01.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv01.TAM_TransmitirDealer__c = true;
        detalleProv01.TAM_VIN__c  =  v01.id;
        detalleProv01.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv01;
        
      
        
        //Se crea el estado de cuenta
        TAM_EstadoCuenta__c edoCta = new TAM_EstadoCuenta__c();
        edoCta.name = 'JULIO-TEST';
            edoCta.TAM_EstadoRegistro__c = 'Abierto';
            edoCta.TAM_CodigoDealer__c = '57011';
            edoCta.TAM_FechaInicio__c = date.today();
            edoCta.TAM_FechaCierre__c = date.today()+90;
 			insert edoCta;
        
        
        //lineas del estado de cuenta retail
        String recordIdedoCtaRetail  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCta = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta.RecordTypeId = recordIdedoCtaRetail;
        detalleEdoCta.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCta.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCta.TAM_AnioModelo__c = '2020';
        detalleEdoCta.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCta.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCta.TAM_CodigoDealer__c = '57011';
        detalleEdoCta.TAM_Codigo_Producto__c = '7400';
        detalleEdoCta.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCta.TAM_EnviadoFacturar__c = false;
        detalleEdoCta.TAM_EdoCtaCerrado__c = false;
        detalleEdoCta.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCta.TAM_VINEntregado__c = true;
        detalleEdoCta.TAM_VIN__c = '12345678901234567';
        detalleEdoCta.TAM_pagoPorTFS__c = false;
        detalleEdoCta.TAM_Serie__c = 'HILUX';
        insert detalleEdoCta;
        
        //lineas del estado de cuenta VC
        String recordIdedoCtaVC = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Flotilla').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCtaVC = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCtaVC.RecordTypeId = recordIdedoCtaVC;
        detalleEdoCtaVC.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCtaVC.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCtaVC.TAM_AnioModelo__c = '2020';
        detalleEdoCtaVC.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCtaVC.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCtaVC.TAM_CodigoDealer__c = '57011';
        detalleEdoCtaVC.TAM_Codigo_Producto__c = '7400';
        detalleEdoCtaVC.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCtaVC.TAM_EnviadoFacturar__c = false;
        detalleEdoCtaVC.TAM_EdoCtaCerrado__c = false;
        detalleEdoCtaVC.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCtaVC.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCtaVC.TAM_VINEntregado__c = true;
        detalleEdoCtaVC.TAM_VIN__c = '12345678901234567';
        detalleEdoCtaVC.TAM_pagoPorTFS__c = false;
        detalleEdoCtaVC.TAM_Serie__c = 'HILUX';
        insert detalleEdoCtaVC;
        
        //lineas del estado de cuenta Bono
        String recordIdedoCtaBono = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCtaBono = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCtaBono.RecordTypeId = recordIdedoCtaBono;
        detalleEdoCtaBono.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCtaBono.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCtaBono.TAM_AnioModelo__c = '2020';
        detalleEdoCtaBono.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCtaBono.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCtaBono.TAM_CodigoDealer__c = '57011';
        detalleEdoCtaBono.TAM_Codigo_Producto__c = '7400';
        detalleEdoCtaBono.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCtaBono.TAM_EnviadoFacturar__c = false;
        detalleEdoCtaBono.TAM_EdoCtaCerrado__c = false;
        detalleEdoCtaBono.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCtaBono.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCtaBono.TAM_VINEntregado__c = true;
        detalleEdoCtaBono.TAM_VIN__c = '12345678901234567';
        detalleEdoCtaBono.TAM_pagoPorTFS__c = false;
        detalleEdoCtaBono.TAM_Serie__c = 'HILUX';
        insert detalleEdoCtaBono;
        
        //lineas del estado de cuenta AutoDemo
        String recordIdedoCtaAutoDemo = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('AutoDemo').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCtarecordIdedoCtaAutoDemo = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCtarecordIdedoCtaAutoDemo.RecordTypeId = recordIdedoCtaAutoDemo;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_ApellidosPropietario__c = 'Figueroa';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_ApellidosPropietario__c = '2020';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_AnioModelo__c = '2020';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_CodigoDealer__c = '57011';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_Codigo_Producto__c = '7400';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_EnviadoFacturar__c = false;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_EdoCtaCerrado__c = false;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_VINEntregado__c = true;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_VIN__c = '12345678901234567';
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_pagoPorTFS__c = false;
        detalleEdoCtarecordIdedoCtaAutoDemo.TAM_Serie__c = 'HILUX';
        insert detalleEdoCtarecordIdedoCtaAutoDemo;
        
    }
    
    @isTest static void testMethod1(){
        TAM_EstadoCuenta__c edoCta = [Select id From TAM_EstadoCuenta__c WHERE name =: 'JULIO-TEST' ];
        
        //Retail 
        PageReference pg = new PageReference('/apex/EstadoCuentaExportarXLS');
        Test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('id', edoCta.id);
        ApexPages.currentPage().getParameters().put('Provision','Retail');    
        ExportToExcelMultipleSheets ctrl = new ExportToExcelMultipleSheets();
        
        //Venta Corporativa
        PageReference pg2 = new PageReference('/apex/EstadoCuentaVCxls');
        Test.setCurrentPage(pg2);
        ApexPages.currentPage().getParameters().put('id', edoCta.id);
        ApexPages.currentPage().getParameters().put('Provision','VentaCorporativa');    
        ExportToExcelMultipleSheets ctrl2 = new ExportToExcelMultipleSheets();
        
        
        //Bono
         PageReference pg3 = new PageReference('/apex/EstadoCuentaExportarBono');
        Test.setCurrentPage(pg3);
        ApexPages.currentPage().getParameters().put('id', edoCta.id);
        ApexPages.currentPage().getParameters().put('Provision','Bono');    
        ExportToExcelMultipleSheets ctrl3 = new ExportToExcelMultipleSheets();
        
        //Autos Demo
        PageReference pg4 = new PageReference('/apex/EstadoCuentaExportarAutoDemo');
        Test.setCurrentPage(pg4);
        ApexPages.currentPage().getParameters().put('id', edoCta.id);
        ApexPages.currentPage().getParameters().put('Provision','AutoDemo');    
        ExportToExcelMultipleSheets ctrl4 = new ExportToExcelMultipleSheets();
        
        
    }
    
    
    
    
}