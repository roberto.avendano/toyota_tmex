@isTest
private class BajaAutoDemoDistribuidorControllerTest {
	
	@TestSetup static void loadData() {
		Schema.DescribeSObjectResult vinSchema = Schema.SObjectType.Vehiculo__c;
		Schema.RecordTypeInfo rtVin = vinSchema.getRecordTypeInfosByName().get('Vehículo');

		List<Vehiculo__c> newVehicles = new List<Vehiculo__c>();
		List<Account> newAccounts = new List<Account>();
		List<SolicitudAutoDemo__c> newSolicitudes = new List<SolicitudAutoDemo__c>();

		for(Integer i=0; i<10;i++){
			newAccounts.add(new Account(
				Name='Account'+i,
				UnidadesAutosDemoAutorizadas__c = 15
			));			
		}		
		insert newAccounts;

		for(Integer j=0; j<20; j++){			
		 	Integer rand = BajaAutoDemoDistribuidorControllerTest.randomWithMax(newAccounts.size());
		 	newVehicles.add(new Vehiculo__c(
	            Name='VIN'+j,          
	            Distribuidor__c=newAccounts.get(rand).Id,	            
	            RecordTypeId= rtVin.recordTypeId
	        ));	        
		}
		insert newVehicles;

		for(Integer k=0; k<5; k++){
			Integer randVIN = BajaAutoDemoDistribuidorControllerTest.randomWithMax(newVehicles.size());
			newSolicitudes.add(new SolicitudAutoDemo__c(
				Estatus__c = Constantes.CASE_STATUS_ALTA,
				VIN__c = newVehicles.get(randVIN).Id
			));
		}
		insert newSolicitudes;
	}

	public static Integer randomWithMax(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
	@isTest static void test_one() {
		ApexPages.StandardController stdController = new ApexPages.StandardController(new SolicitudAutoDemo__c());
		BajaAutoDemoDistribuidorController extController = new BajaAutoDemoDistribuidorController(stdController);
		List<SolicitudAutoDemo__c> solicitudes = extController.getSolicitudes();
		extController.anterior();
		extController.siguiente();
		extController.solicitudID = solicitudes.get(BajaAutoDemoDistribuidorControllerTest.randomWithMax(solicitudes.size())).Id;
		extController.solicitudEdit();
	}
	
}