/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_CheckOutDetalleSolicitudCompra__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    25-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CancelaVinCheckoutDupSch_cls implements Schedulable {

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_CancelaVinCheckoutDupSch_cls.execute...');
        String strEstatusDeaSol = 'Rechazada';
        String strTipoPedidoInv = 'Inventario';

        this.sQuery = 'SELECT TAM_VIN__c';
        this.sQuery += ' FROM TAM_CheckOutDetalleSolicitudCompra__c';
        this.sQuery += ' Where (TAM_Historico__c = true OR (TAM_FECHACANCELACION__C != null And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedidoInv) + '\')) ';
        this.sQuery += ' And TAM_VIN__c != null And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(strEstatusDeaSol) + '\'';
        this.sQuery += ' Order by TAM_VIN__c';
        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_CancelaVinCheckoutDupSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_CancelaVinCheckoutDupBch_cls objCancelaVinCheckoutDupBch = new TAM_CancelaVinCheckoutDupBch_cls(sQuery);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCancelaVinCheckoutDupBch, 25);
    }
    
}