/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Proceso Batch para la generacion de provision de incentivos de venta corporativa.
________________________________________________________________
Autor               Fecha               Descripción
________________________________________________________________
Cecilia Cruz        16/Julio/2020       Versión Inicial
________________________________________________________________
********************************************************************************/
public class TAM_ProvisionBatch_VtaCorp implements Database.Batchable<sObject>, Database.Stateful{
    
    //Variables y constantes.
    static final String STRING_SALESCODE_AUTODEMO_06 = '06';
    static final String STRING_SALESCODE_AUTODEMO_6 = '6';
    static final String STRING_FLEET_C = 'C';
    static final String STRING_RDR = 'RDR';
    static final String STRING_DEALERCODE_57000 = '57000';
    
    public final String recordId;
    public final String strMesProv;
    public final String strAnioProv;
    public final String strMesAnioProv_1;
    public final String strMesAnioProv_2;
    public final String strMesAnioProv_3;
        
    public final TAM_ProvisionIncentivos__c objNewProvision;
    public final String AllVinesDelMes;
    public final String VaRtExcepVtaCorpo;
    public final String RecordTypeId_Flotilla;
    public final Id RecordTypeId_Excepcion;
    public static List<SolicitudAutoDemo__c> lstDemos;

    public static Map<String, List<Movimiento__c>> mapMovimientosVtaCorp;
    public static List<Movimiento__c> lstMovimientosFlotillas;
    public static Map<String, List<TAM_DetallePoliticaJunction__c>> mapIncentivosFlotilla;
    public static Map<String, TAM_SolicitudExpecionIncentivo__c> mapExcepcionesFlotilla;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaFlotilla;
    public static Map<String, TAM_ModeloCodigoProducto__c> mapModeloCodigoProd;
    public static Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVtaCorporativaFlotillaInventario;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaExcepciones;
    public static Map<String, List<TAM_DetallePoliticaJunction__c>> mapIncentivosRetail;
    public static List<TAM_HistoricoFlotillasVines__c> lstHistoricoFlotillas;
    public static Map<String, TAM_CalendarioToyota__c> mapCalendarioTOYOTA;
    public static Map<String, TAM_DetalleEstadoCuenta__c> mapEdoCuentaFacturados;
    public static Set<String> setReversas;
    
    
    //Constructor
    public TAM_ProvisionBatch_VtaCorp(String record_Id, String strMesProvision, String strAnioProvision, TAM_ProvisionIncentivos__c objProvision){

        recordId = record_Id;
        objNewProvision = objProvision;
        strAnioProv = strAnioProvision;
        strMesProv = strMesProvision;
        
        Integer intMesAnterior;
        Integer intAnioAnterior;
        Integer intMesAnterior_2;
        Integer intAnioAnterior_2;
        
        //Mes Anterior
        if(strMesProvision == '1'){
            intMesAnterior = 12;
            intAnioAnterior = Integer.valueOf(strAnioProvision) -1;
        } else {
            intMesAnterior = Integer.valueOf(strMesProvision) -1;
            intAnioAnterior = Integer.valueOf(strAnioProvision);
        }
        //Mes ante-aterior
         if(intMesAnterior == 1){
            intMesAnterior_2 = 12;
            intAnioAnterior_2 = intAnioAnterior -1;
        } else {
            intMesAnterior_2 = intMesAnterior -1;
            intAnioAnterior_2 = intAnioAnterior;
        }
        
        strMesAnioProv_1 = strMesProvision + strAnioProvision;
        strMesAnioProv_2 = String.valueOf(intMesAnterior) + String.valueOf(intAnioAnterior);
        strMesAnioProv_3 = String.valueOf(intMesAnterior_2) + String.valueOf(intAnioAnterior_2);

        AllVinesDelMes = 'SELECT VIN__r.Name FROM Movimiento__c WHERE (TAM_MesAnio__c =: strMesAnioProv_1 OR TAM_MesAnio__c =: strMesAnioProv_2 OR TAM_MesAnio__c =: strMesAnioProv_3)' +
                        ' AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_06  AND Sale_Code__c !=: STRING_SALESCODE_AUTODEMO_6' +
                        ' AND Distribuidor__r.Codigo_Distribuidor__c !=: STRING_DEALERCODE_57000';            
                        
        //Records Types
        VaRtExcepVtaCorpo = Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByName().get('Excepción de Venta Corporativa').getRecordTypeId();
        RecordTypeId_Flotilla = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
        RecordTypeId_Excepcion = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Excepcion').getRecordTypeId();
    }
    
    //Start 
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(AllVinesDelMes);
    }
    
    //Excecute
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<String> setVIN = new Set<String>();
        for(sObject objScope : scope){
            Movimiento__c objVIN = (Movimiento__c)objScope;
            setVIN.add(objVIN.VIN__r.Name);
        }

        mapCalendarioTOYOTA = TAM_ProvisionHelperClass.getCalendarioTOYOTA();
        mapEdoCuentaExcepciones = TAM_ProvisionHelperClass_Batch.getEstadoCuentaExcepciones(strAnioProv, strMesProv);
        mapMovimientosVtaCorp = TAM_ProvisionHelperClass.getMovimientosFlotilla(setVIN);
        mapIncentivosFlotilla = TAM_ProvisionHelperClass.getIncentivosFlotilla(strAnioProv, strMesProv);
        mapExcepcionesFlotilla = TAM_ProvisionHelperClass.getExcepcionesFlotilla(strAnioProv, strMesProv, VaRtExcepVtaCorpo);
        mapEdoCuentaFlotilla = TAM_ProvisionHelperClass.getEstadoCuentaFlotilla(setVIN);
        mapModeloCodigoProd = TAM_ProvisionHelperClass.getModeloCodProd();
        lstMovimientosFlotillas = TAM_ProvisionHelperClass.getListMov(setVIN);
        mapVtaCorporativaFlotillaInventario = TAM_ProvisionHelperClass.getVtaCorporativaInventario(setVIN, strAnioProv, strMesProv, mapCalendarioTOYOTA);
        mapIncentivosRetail = TAM_ProvisionHelperClass_Batch.getIncentivosRetail(strAnioProv, strMesProv);
        lstHistoricoFlotillas = TAM_ProvisionHelperClass.getHistoricoVinesFlotilla();
        mapEdoCuentaFacturados = TAM_ProvisionHelperClass_Batch.getEstadoCuentaFacturados(strAnioProv, strMesProv);
        lstDemos = TAM_ProvisionHelperClass_Batch.getDemos(setVIN);
        
        //Reversas
        setReversas = TAM_ProvisionHelperClass.setFlotillaConReversa(strMesProv, strAnioProv, recordId, RecordTypeId_Flotilla, mapEdoCuentaFlotilla, lstMovimientosFlotillas, mapModeloCodigoProd, mapVtaCorporativaFlotillaInventario);
        TAM_ProvisionHelperClass_Batch.setReversasRetailPendientes(strMesProv, strAnioProv,  recordId, mapEdoCuentaFacturados);

        //Venta Corporativa
        TAM_ProvisionHelperClass.setFlotillaDetalle(recordId, objNewProvision, mapIncentivosFlotilla, setVIN, mapMovimientosVtaCorp, mapExcepcionesFlotilla, mapEdoCuentaFlotilla,
        lstMovimientosFlotillas, mapModeloCodigoProd, RecordTypeId_Flotilla, mapVtaCorporativaFlotillaInventario,mapIncentivosRetail, lstHistoricoFlotillas, strAnioProv, strMesProv, mapCalendarioTOYOTA, setReversas,lstDemos);

        //Excepciones
        TAM_ProvisionHelperClass.setExcepcionVtaCorp( recordId, mapEdoCuentaExcepciones, lstMovimientosFlotillas, RecordTypeId_Excepcion,  mapModeloCodigoProd);
    }

    //Finish
    public void finish(Database.BatchableContext BC){}
}