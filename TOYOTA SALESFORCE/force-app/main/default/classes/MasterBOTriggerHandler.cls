public class MasterBOTriggerHandler extends TriggerHandler{
	private Map<Id, MasterBO__c> newItemsMap;
	private List<MasterBO__c> newItemsList;
	private Map<Id, MasterBO__c> oldItemsMap;

	public MasterBOTriggerHandler() {
		this.newItemsMap = (Map<Id, MasterBO__c>) Trigger.newMap;
		this.newItemsList = (List<MasterBO__c>) Trigger.new;
		this.oldItemsMap = (Map<Id, MasterBO__c>) Trigger.oldMap;
	}

	public override void beforeInsert() {
		setBusquedasBefore(this.newItemsList,null);
	}

	public override void beforeUpdate() {
		setBusquedasBefore(this.newItemsList, this.oldItemsMap);
	}

	public static void setBusquedasBefore(List<MasterBO__c> newItems, Map<Id,MasterBO__c> itemsOld){
		//Obtiene los ids de referencia para consulta de campos de busqueda
		Set<Id> idsParts = new Set<Id>();
		Set<Id> idsDealers = new Set<Id>();
		Set<Id> idsAnalistas = new Set<Id>();
		for(MasterBO__c mbo : newItems){
			if(mbo.PartNameId__c!=null){
				idsParts.add(mbo.PartNameId__c);
			}
			if(mbo.DealerId__c!=null){
				idsDealers.add(mbo.DealerId__c);
			}
			if(mbo.AnalystNameId__c!=null){
				idsAnalistas.add(mbo.AnalystNameId__c);
			}
		}

		
		Map<Id, Product2> mapProductos = new Map<Id, Product2>(
			[select 
				Id, IdExternoProducto__c, IsActive, Name, ProductCode, 
				(Select Id, Name, FPDD__c, EtaDte__c, EtaDte2__c, PlanShpQty2__c, PlanShpQty3__c FROM BOPCPivots__r Order by CreatedDate desc Limit 1),
				(Select Id, Name, CreatedDate, Transport__r.TimeArrivalNumber__c FROM PDCXsInts__r Order by CreatedDate desc Limit 1),
				(select Id, Name, Campaign__c from NoParte_Campanas__r Order by CreatedDate desc Limit 1)
			from Product2
			Where Id IN :idsParts]
		);
		
		Map<Id, Account> mapDealers = new Map<Id, Account>(
			[select 
				Id, Name, FTSId__c, DSPMId__c, PRAId__c, PRAId__r.UsuarioPRAId__c, PRAId__r.UsuarioPRAId__r.IsActive
			from Account
			Where Id IN :idsDealers]
		);

		Map<Id, Contact> mapAnalistas = new Map<Id, Contact>(
			[select 
				Id, Name, PRAAsignadoId__c
			from Contact
			Where Id IN :idsAnalistas]
		);
		System.debug('Mi mapDealers: '+mapDealers);
		for(MasterBO__c mbo : newItems){
			// Validacion para codigos mac que cargan la cantidad en el campo ManualAllocationQty__c
			if(mbo.BOQTYDealerOrder__c==null || mbo.BOQTYDealerOrder__c==0){
				if(mbo.ManualAllocationQty__c==null || mbo.ManualAllocationQty__c==0){
					mbo.BOQTYDealerOrder__c = 1;
				}else{
					mbo.BOQTYDealerOrder__c = mbo.ManualAllocationQty__c;
				}
			}
			

			if(mbo.PartNameId__c!=null && (mbo.BOPCPivotId__c==null || mbo.PDCXsIntId__c==null)){
				Product2 p = mapProductos.get(mbo.PartNameId__c);
				if(p!=null){
					BOPCPivot__c bop = (p.BOPCPivots__r!=null && p.BOPCPivots__r.size()>0)?p.BOPCPivots__r.get(0):null;
					PDCXsInt__c pdx = (p.PDCXsInts__r!=null && p.PDCXsInts__r.size()>0)?p.PDCXsInts__r.get(0):null;
					PartNoCMPG__c pcmpg = (p.NoParte_Campanas__r!=null && p.NoParte_Campanas__r.size()>0)?p.NoParte_Campanas__r.get(0):null;
					if(bop!=null){
						mbo.BOPCPivotId__c = bop.Id;
						mbo.FPDDPivot__c = bop.FPDD__c;
						mbo.ETAPCDatePivot__c = bop.EtaDte__c;
						mbo.ETAMaxPCPivot__c = bop.EtaDte2__c;
						mbo.PlanShipMaxPivot__c = bop.PlanShpQty2__c;
						mbo.TotalPlanShipQtyPivot__c = bop.PlanShpQty3__c;
					}
					
					mbo.PDCXsIntId__c = pdx!=null?pdx.Id:null;
					mbo.PDCXsIntId__r = pdx!=null?pdx:null;
					mbo.PartNoCMPGId__c = pcmpg!=null?pcmpg.Id:null;
				}
			}
			if(mbo.DealerId__c!=null){
				Account dealer = mapDealers.get(mbo.DealerId__c);
				if(dealer!=null){
					mbo.FTSId__c = dealer.FTSId__c;
					mbo.DSPMId__c = dealer.DSPMId__c;
					mbo.PRAId__c = dealer.PRAId__c;
					System.debug('Mi dealer.PRAId__r.UsuarioPRAId__c: '+dealer.PRAId__r.UsuarioPRAId__c);
					if(dealer.PRAId__r.UsuarioPRAId__c!=null && dealer.PRAId__r.UsuarioPRAId__r.IsActive){
						mbo.OwnerId = dealer.PRAId__r.UsuarioPRAId__c;
					}
				}
			}
			if(mbo.AnalystNameId__c!=null){
				Contact analista = mapAnalistas.get(mbo.AnalystNameId__c);
				if(analista!=null){
					mbo.PRAbyAnalystId__c = analista.PRAAsignadoId__c;
				}
			}
			mbo.SalidaTentativaPDC__c = MasterBOTriggerHandler.calculaSalidaTentativa(mbo);
			mbo.Categoria_BO__c = MasterBOTriggerHandler.calculaCategoriaBO(mbo);
		}
	}

	public static String calculaCategoriaBO(MasterBO__c mbo){
		System.debug(mbo);
		if(mbo.CategoriaA1__c!=null && mbo.CategoriaA1__c!='' && mbo.CategoriaA1__c!='Sin categoría'){
			return mbo.CategoriaA1__c;
		}else if(mbo.CategoriaA2__c!=null && mbo.CategoriaA2__c!='' && mbo.CategoriaA2__c!='Sin categoría'){
			return mbo.CategoriaA2__c;
		}else if(mbo.CategoriaB1__c!=null && mbo.CategoriaB1__c!='' && mbo.CategoriaB1__c!='Sin categoría'){
			return mbo.CategoriaB1__c;
		}else if(mbo.CategoriaB2__c!=null && mbo.CategoriaB2__c!='' && mbo.CategoriaB2__c!='Sin categoría'){
			return mbo.CategoriaB2__c;
		}else if(mbo.CategoriaC1__c!=null && mbo.CategoriaC1__c!='' && mbo.CategoriaC1__c!='Sin categoría'){
			return mbo.CategoriaC1__c;
		}else if(mbo.CategoriaC2__c!=null && mbo.CategoriaC2__c!='' && mbo.CategoriaC2__c!='Sin categoría'){
			return mbo.CategoriaC2__c;
		}else {
			return '';
		}
	}

	public static Date calculaSalidaTentativa(MasterBO__c mbo){
		if(mbo.LlegadaTentativaPDC__c==null || mbo.LlegadaTentativaPDC__c=='' || mbo.LlegadaTentativaPDC__c=='TBD'){
			System.debug('sin LlegadaTentativaPDC__c');
			return null;
		}else{
			System.debug('con LlegadaTentativaPDC__c =' + mbo.LlegadaTentativaPDC__c);
			List<String> arrDate = mbo.LlegadaTentativaPDC__c.split('-');
			Date dLlegadaTent = Date.newInstance(
				Integer.valueOf(arrDate[0]),
				Integer.valueOf(arrDate[1]),
				Integer.valueOf(arrDate[2])
			);

			Decimal horaLlegada = 0;
			if(mbo.PDCXsIntId__r!=null && mbo.PDCXsIntId__r.Transport__r!=null && mbo.PDCXsIntId__r.Transport__r.TimeArrivalNumber__c!=null){
				horaLlegada = mbo.PDCXsIntId__r.Transport__r.TimeArrivalNumber__c;
			}

			System.debug(dLlegadaTent);
			if(mbo.Grupo__c == null || mbo.Grupo__c==''){
				return null;
			}else if(mbo.Grupo__c=='GA'){
				System.debug('GA');
				dLlegadaTent = dLlegadaTent.addDays(1);
			}else{ 
				String rGrupo = mbo.Grupo__c.right(1);
				if(rGrupo<='E' && horaLlegada<=0.4){
					System.debug('E');
					dLlegadaTent = dLlegadaTent.addDays(1);
				}else if(rGrupo<='J' && (horaLlegada > 0.5416 && horaLlegada < 0.5417 )){
					System.debug('J');
					dLlegadaTent = dLlegadaTent.addDays(1);
				}else{
					System.debug('Otro');
					//dLlegadaTent = dLlegadaTent.addDays(0);
				}
			}
			Integer diaSem = Utils.diaSemanaInicioLunes(dLlegadaTent);
			if(diaSem>=6){
				System.debug('diaSem>=6 = ' + diaSem);
				dLlegadaTent = dLlegadaTent.addDays(2);
			}
			System.debug(dLlegadaTent);
			return dLlegadaTent;
		}
	}

}