@isTest
private class Cobertura_SendEmail {
	static testMethod void myUnitTest() {
		User usuarioTest = new User();
		usuarioTest.Username = 'username@formaCorreo.com';
		usuarioTest.LastName = 'LastName';
		usuarioTest.Alias = 'Alias';
		usuarioTest.CommunityNickname = 'CommunityNickname';
		usuarioTest.TimeZoneSidKey = 'America/Mexico_City';
		usuarioTest.LocaleSidKey = 'es_MX';
		usuarioTest.EmailEncodingKey = 'ISO-8859-1';
		usuarioTest.ProfileId = '00ei0000000wytDAAQ';
		usuarioTest.LanguageLocaleKey = 'es';
		usuarioTest.Email = 'a@b.c';
		insert usuarioTest;	
		
		String day = string.valueOf(system.now().day());
		String month = string.valueOf(system.now().month());
		String hour = string.valueOf(system.now().hour());
		String minute = string.valueOf(system.now().minute() + 1);
		String second = string.valueOf(system.now().second());
		String year = string.valueOf(system.now().year());
		String strJobName = 'Envio de Emails';
		String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
		System.schedule(strJobName, strSchedule, new SendEmail());
    }
}