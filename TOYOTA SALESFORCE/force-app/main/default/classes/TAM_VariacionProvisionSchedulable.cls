/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Clase programada para ejecutarse diariamente.
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		09/Junio/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
global without sharing class TAM_VariacionProvisionSchedulable implements Schedulable {
    global void execute(SchedulableContext SC) {
         Id batchJobId = Database.executeBatch(new TAM_CalculoVariacionBatch(),200);
    }
}