/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_SolicitudCompraEspecialDistCmpCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    22-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_SolicitudCompraEsplDistCmpCtrl_tst {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
	
	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

	static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
		
	static String sListaPreciosCutom = getCustomPriceBookList();
	
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
	static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
	static	Rangos__c rango = new Rangos__c();
	static  TAM_OrdenDeCompraWrapperClass objTAMOrdenDeCompraWrapperClass;
 	 
	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;

		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor,
			Venta_Corporativa__c = 'Si'
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  

		CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
        	Name = 'AVANZA-22060-2020-B79-10-LE AT',
        	Marca__c = 'Toyota',
        	Serie__c = 'AVANZA',
        	Modelo__c = '22060',
        	AnioModelo__c = '2020',
        	CodigoColorExterior__c = 'B79', 
        	CodigoColorInterior__c = '10',
        	DescripcionColorExterior__c = 'Azul', 
        	DescripcionColorInterior__c = 'Gris',
        	Version__c = 'LE AT',
        	Disponible__c = 'SI'
	    );
	    insert catCenMod;  
		
		Id standardPricebookId = Test.getStandardPricebookId();
		Product2 ProducStdPriceBook = new Product2(
				Name = '22060',
				Anio__c = '2020', 
				NombreVersion__c = 'LE MT',
				IdExternoProducto__c = '220602020',
				ProductCode = '220602020',
				Description= 'AVANZA-22060-2020-B79-10-LE AT', 
				RecordTypeId = sRectorTypePasoProductoUnidad,
				Family = 'Toyota',
				IsActive = true
		);
		insert ProducStdPriceBook;
		
		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id = standardPricebookId,
			UnitPrice = 0.0,
			Product2Id = ProducStdPriceBook.Id,			
			IsActive = true,
			IdExterno__c = '220602020'
		);
		insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
		
		PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
			Pricebook2Id = customPB.id,
			UnitPrice = 1.0,
			Product2Id = ProducStdPriceBook.Id,			
			IsActive = true
		);
		insert pbeCustomProceBookEntry;
		
		TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
			TAM_Cliente__c = clienteMoral.id,			
			TAM_Estatus__c = 'El Proceso',
			RecordTypeId = sRectorTypePasoSolVentaCorporativa,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert solVentaFlotillaPrograma;
				
		TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma = new TAM_DetalleOrdenCompra__c(
			Name = '2020-AVANZA-LE AT-22060',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			RecordTypeId = sRectorTypePasoPedidoEspecial,
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_FechaSolicitud__c = Date.today(),
			TAM_EntregarEnMiDistribuidora__c = true,
			TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
			TAM_Cantidad__c = 10
		);
		insert detSolVentaFlotillaPrograma;
				
		TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra = new TAM_DistribuidoresFlotillaPrograma__c(
			Name = '2020-AVANZA-LE AT-22060',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			RecordTypeId = sRectorTypePasoDistFlotilla,
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_Estatus__c = 'Pendiente',
			TAM_Cantidad__c = 10,
			TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma.id,
			TAM_Cuenta__c = clienteMoral.id
		);
		insert detSolDistFltoProgra;
				
				             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

	public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;		
	}
	
	public static TAM_OrdenDeCompraWrapperClass getObjTAMOrdenDeCompraWrapperClass(String solVentaFlotillaPrograma, CatalogoCentralizadoModelos__c catCenMod){
		String sPasoObjTAMOrdenDeCompraWrapperClass = '';

		String strIdExternoDistPaso = solVentaFlotillaPrograma + '-' + catCenMod.AnioModelo__c + '-' +  catCenMod.Serie__c + '-' + catCenMod.Modelo__c + '-' + catCenMod.CodigoColorExterior__c + '-' + catCenMod.CodigoColorInterior__c ;
		System.debug('EN A TAM_SolicitudCompraEsplDistCmpCtrl_tst.getObjTAMOrdenDeCompraWrapperClass strIdExternoDistPaso: ' + strIdExternoDistPaso);      
			 
        String strCodigoModelo = catCenMod.Modelo__c + catCenMod.AnioModelo__c;
                                                     
        objTAMOrdenDeCompraWrapperClass = new TAM_OrdenDeCompraWrapperClass(    
            					catCenMod.Marca__c,
                                catCenMod.Serie__c,
                                catCenMod.Modelo__c,
                                catCenMod.AnioModelo__c,
                                catCenMod.Version__c,
                                catCenMod.CodigoColorExterior__c,
                                catCenMod.CodigoColorInterior__c,
                                catCenMod.DescripcionColorExterior__c,
                                catCenMod.DescripcionColorInterior__c,
                                String.valueOf(10),
                                null,
                                String.valueOf(100.00),
                                null,
                                catCenMod.Name,
                                catCenMod.Serie__c + '-' + catCenMod.Version__c + '-' + catCenMod.Modelo__c + '-' + catCenMod.AnioModelo__c,
                                catCenMod.CodigoColorExterior__c + '-' + catCenMod.DescripcionColorExterior__c,
                                catCenMod.CodigoColorInterior__c + '-' + catCenMod.DescripcionColorInterior__c,
                                false   //false
       );
       objTAMOrdenDeCompraWrapperClass.strIdCatModelos = catCenMod.id;
       objTAMOrdenDeCompraWrapperClass.strIdExterno = strIdExternoDistPaso;
       					
		return objTAMOrdenDeCompraWrapperClass;
	}
	
	public static TAM_WrpDistSolFlotillaPrograma getobjTAMWrpDistSolFlotillaPrograma(String solVentaFlotillaPrograma, CatalogoCentralizadoModelos__c catCenMod){
		String sPasoObjTAMOrdenDeCompraWrapperClass = '';

		String strIdExternoDistPaso = solVentaFlotillaPrograma + '-' + catCenMod.AnioModelo__c + '-' +  catCenMod.Serie__c + '-' + catCenMod.Modelo__c + '-' + catCenMod.CodigoColorExterior__c + '-' + catCenMod.CodigoColorInterior__c ;
		System.debug('EN A TAM_SolicitudCompraEsplDistCmpCtrl_tst.getObjTAMOrdenDeCompraWrapperClass strIdExternoDistPaso: ' + strIdExternoDistPaso);      
			 
        String strCodigoModelo = catCenMod.Modelo__c + catCenMod.AnioModelo__c;

		/*Integer intIndex, Boolean bolEliminar, String strClaveNomDistrib,
    	String strAnioModelo, String strSerie, String strModelo, String strVersion, String strIdExterno,
    	Integer intCantidad, String strIdDistrib, List<TAM_WrpDistribuidores> listWrpDistrib	*/
                                                     
        TAM_WrpDistSolFlotillaPrograma objTAMWrpDistSolFlotillaPrograma = new TAM_WrpDistSolFlotillaPrograma(
        						0,
        						false,
        						'570550-TOYOTA PRUEBA',    
                                catCenMod.AnioModelo__c,
                                catCenMod.Serie__c,
                                catCenMod.Modelo__c,
                                catCenMod.Version__c,
                                '',
                                10,
                                '570550 - TOYOTA PRUEBA',
                                new List<TAM_WrpDistribuidores>()
       );
       objTAMWrpDistSolFlotillaPrograma.strIdColExt = catCenMod.CodigoColorExterior__c;
       objTAMWrpDistSolFlotillaPrograma.strIdColInt = catCenMod.CodigoColorInterior__c;
       objTAMWrpDistSolFlotillaPrograma.strIdDistrib = '570550 - TOYOTA PRUEBA';
       					
		return objTAMWrpDistSolFlotillaPrograma;
	}
	
	
    static testMethod void TAM_SolicitudCompraEsplDistCmpCtrlOK() {
    	
		//Consulta los datos del cliente
		solFlotillaPrograma = [Select Id, Name From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];        
   		System.debug('EN TAM_SolicitudCompraEsplDistCmpCtrlOK solFlotillaPrograma: ' + solFlotillaPrograma);
 
		CatalogoCentMod = [Select Id, Name , Marca__c, Serie__c, Modelo__c, AnioModelo__c, CodigoColorExterior__c, CodigoColorInterior__c,
        	DescripcionColorExterior__c, DescripcionColorInterior__c, Version__c, Disponible__c	From CatalogoCentralizadoModelos__c LIMIT 1];        
   		System.debug('EN TAM_SolicitudCompraEsplDistCmpCtrlOK solFlotillaPrograma: ' + solFlotillaPrograma);
 		
 		objTAMOrdenDeCompraWrapperClass = getObjTAMOrdenDeCompraWrapperClass(solFlotillaPrograma.id, CatalogoCentMod);
 		
		//Serializa el objeto sPasoObjTAMOrdenDeCompraWrapperClass			
		String sPasoObjTAMOrdenDeCompraWrapperClass = JSON.serialize(objTAMOrdenDeCompraWrapperClass);
		System.debug('EN A TAM_SolicitudCompraEsplDistCmpCtrl_tst.getObjTAMOrdenDeCompraWrapperClass sPasoObjTAMOrdenDeCompraWrapperClass: ' + sPasoObjTAMOrdenDeCompraWrapperClass);      
        
       //Llama la clase de TAM_CargaArchivosCtrl y metodo getModelos
       TAM_SolicitudCompraEspecialDistCmpCtrl.getWrpModelo(sPasoObjTAMOrdenDeCompraWrapperClass);

       //Llama la clase de TAM_CargaArchivosCtrl y metodo addWrpDist
       TAM_SolicitudCompraEspecialDistCmpCtrl.addWrpDist(sPasoObjTAMOrdenDeCompraWrapperClass, solFlotillaPrograma.id, 0, objTAMOrdenDeCompraWrapperClass.strAnioModelo,
    	objTAMOrdenDeCompraWrapperClass.strSerie, objTAMOrdenDeCompraWrapperClass.strModelo, objTAMOrdenDeCompraWrapperClass.strVersion, 
    	objTAMOrdenDeCompraWrapperClass.strCodigoColorExterior, objTAMOrdenDeCompraWrapperClass.strCodigoColorInterior, 
    	sRectorTypePasoSolVentaCorporativa);

       //Llama la clase de TAM_CargaArchivosCtrl y metodo getModelos
       TAM_SolicitudCompraEspecialDistCmpCtrl.getDistribuidores();

	   TAM_WrpDistSolFlotillaPrograma objTAMWrpDistSolFlotillaPrograma = getobjTAMWrpDistSolFlotillaPrograma(solFlotillaPrograma.id, CatalogoCentMod);
		//Serializa el objeto sPasoObjTAMOrdenDeCompraWrapperClass			
		String sobjTAMWrpDistSolFlotillaPrograma = JSON.serialize(objTAMWrpDistSolFlotillaPrograma);
		System.debug('EN A TAM_SolicitudCompraEsplDistCmpCtrl_tst.getObjTAMOrdenDeCompraWrapperClass sobjTAMWrpDistSolFlotillaPrograma: ' + sobjTAMWrpDistSolFlotillaPrograma);      
				
       //Llama la clase de TAM_CargaArchivosCtrl y metodo saveDatos
       TAM_SolicitudCompraEspecialDistCmpCtrl.saveDatos(sobjTAMWrpDistSolFlotillaPrograma, sobjTAMWrpDistSolFlotillaPrograma, 
       	solFlotillaPrograma.id, 1, objTAMOrdenDeCompraWrapperClass.strAnioModelo, objTAMOrdenDeCompraWrapperClass.strSerie, 
       	objTAMOrdenDeCompraWrapperClass.strModelo, objTAMOrdenDeCompraWrapperClass.strVersion, 
    	objTAMOrdenDeCompraWrapperClass.strCodigoColorExterior, objTAMOrdenDeCompraWrapperClass.strCodigoColorInterior, 
    	sRectorTypePasoSolVentaCorporativa);

       //Llama la clase de TAM_CargaArchivosCtrl y metodo etDatosDistribuidores
       TAM_SolicitudCompraEspecialDistCmpCtrl.getDatosDistribuidores(solFlotillaPrograma.id, 1, objTAMOrdenDeCompraWrapperClass.strAnioModelo, 
       	objTAMOrdenDeCompraWrapperClass.strSerie, objTAMOrdenDeCompraWrapperClass.strModelo, objTAMOrdenDeCompraWrapperClass.strVersion, 
    	objTAMOrdenDeCompraWrapperClass.strCodigoColorExterior, objTAMOrdenDeCompraWrapperClass.strCodigoColorInterior, 
    	sRectorTypePasoSolVentaCorporativa);

       //Llama la clase de TAM_CargaArchivosCtrl y metodo getSeries
       TAM_WrpDistSolFlotillaPrograma objTAMOrdenDeCompraWrapperClass = new TAM_WrpDistSolFlotillaPrograma();
       
       TAM_WrpDistribuidores objTAMWrpDistribuidores = new TAM_WrpDistribuidores();
       TAM_WrpDistribuidores objTAMWrpDistribuidoresParam = new TAM_WrpDistribuidores(solFlotillaPrograma.id, '10', 'Descripción'
       	, false);
       
    }
    
}