/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_CheckOutProgramaCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    23-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CheckOutProgramaCtrl_tst {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
	
	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

	static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	
	static String sRectorTypePasoVinesFlotilla = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String sRectorTypePasoVinesPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static String sRectorTypePoliticaIncentivos = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
		
	static String sListaPreciosCutom = getCustomPriceBookList();
	
	static	Account clienteDealer = new Account();
	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	Contact contactoPrueba = new Contact();
	static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
	static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
	static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
	static	Rangos__c rango = new Rangos__c();
	static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
	static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
	static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();
	
	@TestSetup static void loadData(){
		
		Group grupoPrueba = new Group(
			Name = 'CARSON',
			Type = 'Regular'
		);
		insert grupoPrueba;
		
		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;

		Rangos__c rangoPrograma = new Rangos__c(
			Name = 'UBER',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'UBER | 10 | 50',
			RecordTypeId = sRectorTypePasoPrograma
			 
		);
		insert rangoPrograma;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor,
			TAM_FechaVigenciaInicio__c = Date.today(),
			TAM_FechaVigenciaFin__c = Date.today()
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id,
			TAM_FechaVigenciaInicio__c = Date.today(),
			TAM_FechaVigenciaFin__c = Date.today()
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica,
			TAM_FechaVigenciaInicio__c = Date.today(),
			TAM_FechaVigenciaFin__c = Date.today()
		);
		//insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    //insert cont;  

		CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
        	Name = 'AVANZA-22060-2020-B79-10-LE AT',
        	Marca__c = 'Toyota',
        	Serie__c = 'AVANZA',
        	Modelo__c = '22060',
        	AnioModelo__c = '2020',
        	CodigoColorExterior__c = 'B79', 
        	CodigoColorInterior__c = '10',
        	DescripcionColorExterior__c = 'Azul', 
        	DescripcionColorInterior__c = 'Gris',
        	Version__c = 'LE AT',
        	Disponible__c = 'SI'
	    );
	    insert catCenMod;  
		CatalogoCentMod = catCenMod; 
		
		CatalogoCentralizadoModelos__c catCenMod2 = new CatalogoCentralizadoModelos__c(
        	Name = 'AVANZA-22060-2020-B79-10-LE AT',
        	Marca__c = 'Toyota',
        	Serie__c = 'AVANZA',
        	Modelo__c = '22060',
        	AnioModelo__c = '2020',
        	CodigoColorExterior__c = 'B79', 
        	CodigoColorInterior__c = '11',
        	DescripcionColorExterior__c = 'Azul', 
        	DescripcionColorInterior__c = 'Gris',
        	Version__c = 'LE AT',
        	Disponible__c = 'SI'
	    );
	    insert catCenMod2;  
		CatalogoCentMod2 = catCenMod2;
		
		Id standardPricebookId = Test.getStandardPricebookId();
		Product2 ProducStdPriceBook = new Product2(
				Name = '22060',
				Anio__c = '2020', 
				NombreVersion__c = 'LE MT',
				IdExternoProducto__c = '220602020',
				ProductCode = '220602020',
				Description= 'AVANZA-22060-2020-B79-10-LE AT', 
				RecordTypeId = sRectorTypePasoProductoUnidad,
				Family = 'Toyota',
				IsActive = true
		);
		insert ProducStdPriceBook;
		
		PricebookEntry pbeStandard = new PricebookEntry(
			Pricebook2Id = standardPricebookId,
			UnitPrice = 0.0,
			Product2Id = ProducStdPriceBook.Id,			
			IsActive = true,
			IdExterno__c = '220602020'
		);
		insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
		
		PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
			Pricebook2Id = customPB.id,
			UnitPrice = 1.0,
			Product2Id = ProducStdPriceBook.Id,			
			IsActive = true
		);
		//insert pbeCustomProceBookEntry;

		TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
			TAM_Cliente__c = clienteMoral.id,			
			TAM_Estatus__c = 'El Proceso',
			RecordTypeId = sRectorTypePasoSolVentaCorporativa,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert solVentaFlotillaPrograma;

		TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma = new TAM_DetalleOrdenCompra__c(
			Name = '2020-AVANZA-LE AT-22060',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			RecordTypeId = sRectorTypePasoPedidoEspecial,
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_FechaSolicitud__c = Date.today(),
			TAM_EntregarEnMiDistribuidora__c = true,
			TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
			TAM_Cantidad__c = 10
		);
		insert detSolVentaFlotillaPrograma;

        TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma2 = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoPedidoEspecial,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = false,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Cantidad__c = 10
        );
        insert detSolVentaFlotillaPrograma2;

		TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra = new TAM_DistribuidoresFlotillaPrograma__c(
			Name = '2020-AVANZA-LE AT-22060',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			RecordTypeId = sRectorTypePasoDistFlotilla,
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_Estatus__c = 'Pendiente',
			TAM_Cantidad__c = 10,
			TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma.id,
			TAM_Cuenta__c = clienteMoral.id,
    		TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA',
    		TAM_IdDistribuidor__c = '570550'		
		);
		insert detSolDistFltoProgra;

        TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra2 = new TAM_DistribuidoresFlotillaPrograma__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoDistFlotilla,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_Estatus__c = 'Pendiente',
            TAM_Cantidad__c = 10,
            TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma2.id,
            TAM_Cuenta__c = clienteMoral.id,
            TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA',
            TAM_IdDistribuidor__c = '570550'        
        );
        insert detSolDistFltoProgra2;

		TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma2 = new TAM_SolicitudesFlotillaPrograma__c(
			TAM_Cliente__c = clienteMoral.id,			
			TAM_Estatus__c = 'Cerrada',
			RecordTypeId = sRectorTypePasoSolProgramaCerrada,
			TAM_ProgramaRango__c = rangoPrograma.id,
			TAM_FechaCierreSolicitud__c = Date.today()						
		);
		insert solVentaFlotillaPrograma2;
				
		TAM_DetalleOrdenCompra__c detSolVentaFlotillaProgramaInv = new TAM_DetalleOrdenCompra__c(
			Name = '2020-AVANZA-LE AT-22060',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			RecordTypeId = sRectorTypePasoInventario,
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-12' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_FechaSolicitud__c = Date.today(),
			TAM_EntregarEnMiDistribuidora__c = true,
			TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
			TAM_Cantidad__c = 10
		);
		insert detSolVentaFlotillaProgramaInv;
						
        InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c(
        		  Name = 'XXXXXXXXXXX',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvWholeSale;
		
        Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c(
        		  Name = 'XXXXXXXXXX1',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvTransito;
		
		TAM_VinesFlotillaPrograma__c objTAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c(
		  Name = 'XXXXXXXXXX1', 
		  TAM_IdExterno__c = solVentaFlotillaPrograma2.id + '-2020-AVANZA-22060-B79-10-JTDBBRBE1LJ018487-' + sRectorTypePasoVinesPrograma, 
		  TAM_Estatus__c = 'Cerrada',
		  TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma2.ID
		);	
		insert objTAMVinesFlotillaPrograma;	

		TAM_VinesFlotillaPrograma__c objTAMVinesFlotillaPrograma2 = new TAM_VinesFlotillaPrograma__c(
		  Name = 'XXXXXXXXXX1', 
		  TAM_IdExterno__c = solVentaFlotillaPrograma2.id + '-2020-AVANZA-22060-B79-11-JTDBBRBE1LJ018487-' + sRectorTypePasoVinesPrograma, 
		  TAM_Estatus__c = 'Cerrada',
		  TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma2.ID
		);	
		insert objTAMVinesFlotillaPrograma2;	

		TAM_VinesFlotillaPrograma__c objTAMVinesFlotillaPrograma3 = new TAM_VinesFlotillaPrograma__c(
		  Name = 'XXXXXXXXXX1', 
		  TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-2020-AVANZA-22060-B79-10-JTDBBRBE1LJ018487-' + sRectorTypePasoVinesFlotilla, 
		  TAM_Estatus__c = 'Cerrada',
		  TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.ID
		);	
		//insert objTAMVinesFlotillaPrograma3;			

		TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
			Name = 'AVANZA-22060-2020-B79-10-LE AT',
			TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
			TAM_TipoPago__c = 'Arrendadora',
			TAM_TipoVenta__c = 'Pedido Especial',
			TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
			TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
			TAM_Intercambio__c = false
		);
		insert objCheckOutDetSolComp;
		
		TAM_PoliticasIncentivos__c objTAMPoliticasIncentivos = new TAM_PoliticasIncentivos__c(
			Name = 'ABIRL 2100 - RETAIL' ,
			TAM_InicioVigencia__c = Date.today(),
			TAM_FinVigencia__c = Date.today().addDays(10)
		);
		insert objTAMPoliticasIncentivos;
		
		TAM_DetallePolitica__c objTAMDetallePolitica = new TAM_DetallePolitica__c(
              TAM_AnioModelo__c = '2020',
              TAM_Serie__c = 'AVANZA',
              TAM_Version__c = 'LE AT',
              TAM_Pago_TFS__c = false,
              TAM_IncTFS_Porcentaje__c = 1.00,
              TAM_IncTFS_Cash__c = 1.00,
              TAM_IncDealer_Cash__c = 1.00,
              TAM_IncDealer_Porcentaje__c = 1.00,
              TAM_Clave__c = '220602020',
              TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id
		);		  
		insert objTAMDetallePolitica;  
				
        String sFolSol = '';
        //Consulta la solicitud asociada a TAM_SolicitudesFlotillaPrograma__c
        for (TAM_SolicitudesFlotillaPrograma__c ovbjSol : [Select id, Name From TAM_SolicitudesFlotillaPrograma__c
            Where id = :solVentaFlotillaPrograma.id]){
            sFolSol = ovbjSol.Name;
        }
        System.debug('EN TAM_CheckOutProgramaCtrlOK sFolSol: ' + sFolSol);
            
        TAM_SolicitudDealerDestino__c objSolicitudDealerDestino = new TAM_SolicitudDealerDestino__c(
            Name = sFolSol,
            TAM_IdExterno__c = sFolSol + ' - ' + '570550-TOYOTA PRUEBA',
            TAM_Estatus__c = 'Pendiente',
            TAM_FechaCancelacion__c = Date.today(),
            TAM_FechaCheckOut__c = Date.today(),
            TAM_DealerOrigen__c = '57000-TMEX',
            TAM_DealerDestino__c = '570550-TOYOTA PRUEBA'
        );                    
		insert objSolicitudDealerDestino;
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

	public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;		
	}
	
	public static List<TAM_CheckoutWrapperClass> getWrpModeloCheckOut(String solVentaFlotillaPrograma, CatalogoCentralizadoModelos__c catCenMod){
		String sPasoObjTAMOrdenDeCompraWrapperClass = '';
		List<TAM_CheckoutWrapperClass> lTAMCheckoutWrapperClass = new List<TAM_CheckoutWrapperClass>();
				
        TAM_CheckoutWrapperClass objTAMCheckoutWrapperClass1 = new TAM_CheckoutWrapperClass(
        				'AVANZA-22060-2020-B79-10-LE AT',
                        catCenMod.CodigoColorExterior__c,
                        catCenMod.CodigoColorInterior__c,
            			catCenMod.Marca__c,
                        catCenMod.Modelo__c,
                        catCenMod.AnioModelo__c,
                        String.valueOf(1000.00),
                        String.valueOf(1),
                        'Flotilla',
                        catCenMod.Serie__c,
                        catCenMod.Version__c,
                        sRectorTypePasoInventario
        );
		objTAMCheckoutWrapperClass1.strIdCatCentMod = CatalogoCentMod.id;
		objTAMCheckoutWrapperClass1.strTipoPago = 'Contado';
		objTAMCheckoutWrapperClass1.strDistribuidor = 'true';
		objTAMCheckoutWrapperClass1.strPorDescDTM = '% 1.00';
		objTAMCheckoutWrapperClass1.strPorDescTMEX = '% 1.00';
		objTAMCheckoutWrapperClass1.strPorDescDLER = '% 1.00';
		objTAMCheckoutWrapperClass1.strMontoDescDTM = '$ 1.00';
		objTAMCheckoutWrapperClass1.strMontoDescTMEX = '$ 1.00';
		objTAMCheckoutWrapperClass1.strMontoDescDLER = '$ 1.00';
		
		//Agregalo a la lista de lTAMCheckoutWrapperClass	
		lTAMCheckoutWrapperClass.add(objTAMCheckoutWrapperClass1);
		
        TAM_CheckoutWrapperClass objTAMCheckoutWrapperClass2 = new TAM_CheckoutWrapperClass(
        				'AVANZA-22060-2020-B79-10-LE AT',
                        catCenMod.CodigoColorExterior__c,
                        catCenMod.CodigoColorInterior__c,
            			catCenMod.Marca__c,
                        catCenMod.Modelo__c,
                        catCenMod.AnioModelo__c,
                        String.valueOf(1000.00),
                        String.valueOf(1),
                        'Inventario',
                        catCenMod.Serie__c,
                        catCenMod.Version__c,
                        sRectorTypePasoPedidoEspecial
        );
        objTAMCheckoutWrapperClass2.strIdCatCentMod = CatalogoCentMod2.id;
        objTAMCheckoutWrapperClass2.strTipoPago = 'Contado';
        objTAMCheckoutWrapperClass2.strVin = 'XXXXXXXXXXXXXXX';
        objTAMCheckoutWrapperClass2.strDistribuidor = 'true';
		objTAMCheckoutWrapperClass2.strPorDescDTM = '% 1.00';
		objTAMCheckoutWrapperClass2.strPorDescTMEX = '% 1.00';
		objTAMCheckoutWrapperClass2.strPorDescDLER = '% 1.00';
		objTAMCheckoutWrapperClass2.strMontoDescDTM = '$ 1.00';
		objTAMCheckoutWrapperClass2.strMontoDescTMEX = '$ 1.00';
		objTAMCheckoutWrapperClass2.strMontoDescDLER = '$ 1.00';
        
        //Agregalo a la lista de lTAMCheckoutWrapperClass		
        lTAMCheckoutWrapperClass.add(objTAMCheckoutWrapperClass2);
        
       	//Regresa la lista de lTAMCheckoutWrapperClass				
		return lTAMCheckoutWrapperClass;
	}


    static testMethod void TAM_CheckOutProgramaCtrlOK() {
    	
    	Test.startTest();

			//Consulta los datos del cliente
			solFlotillaPrograma = [Select Id, Name From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];        
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK solFlotillaPrograma: ' + solFlotillaPrograma);
			//Consulta los datos del cliente
			clienteDealer = [Select Id, Name, Codigo_Distribuidor__c From Account Where RecordTypeId = :sRectorTypePasoDistribuidor LIMIT 1];        
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK solFlotillaPrograma: ' + clienteDealer);
			//Consulta los datos del cliente
			objInvTransito = [Select Id, Name From Inventario_en_Transito__c LIMIT 1];        
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK objInvTransito: ' + objInvTransito);
			//Consulta los datos del cliente
			objInvWholeSale = [Select Id, Name From InventarioWholesale__c LIMIT 1];        
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK objInvWholeSale: ' + objInvWholeSale);
			TAMVinesFlotillaPrograma = [Select Id, Name, TAM_IdExterno__c, TAM_Estatus__c, TAM_SolicitudFlotillaPrograma__c  From TAM_VinesFlotillaPrograma__c LIMIT 1];  
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK TAMVinesFlotillaPrograma: ' + TAMVinesFlotillaPrograma);		 
			CatalogoCentMod = [Select Id, Name , Marca__c, Serie__c, Modelo__c, AnioModelo__c, CodigoColorExterior__c, CodigoColorInterior__c,
	        	DescripcionColorExterior__c, DescripcionColorInterior__c, Version__c, Disponible__c	From CatalogoCentralizadoModelos__c LIMIT 1];        
	   		System.debug('EN TAM_CheckOutProgramaCtrlOK solFlotillaPrograma: ' + solFlotillaPrograma);
			
			//Llama la clase de TAM_CargaArchivosCtrl y metodo getDetalleOrdenCompra
	       TAM_CheckOutProgramaCtrl.getDetalleOrdenCompra(solFlotillaPrograma.id);
			//Llama la clase de TAM_CargaArchivosCtrl y metodo getDistribuidoresFlotilla
	       TAM_CheckOutProgramaCtrl.getDistribuidoresFlotilla(solFlotillaPrograma.id);
		   //obten la lista de TAM_CheckoutWrapperClass
		   List<TAM_CheckoutWrapperClass> lTAMCheckoutWrapperClass = getWrpModeloCheckOut(solFlotillaPrograma.id, CatalogoCentMod);
		   System.debug('EN TAM_CheckOutProgramaCtrlOK lTAMCheckoutWrapperClass: ' + lTAMCheckoutWrapperClass);
			//Llama la clase de TAM_CargaArchivosCtrl y metodo SaveCheckout
	       TAM_CheckOutProgramaCtrl.SaveCheckout(solFlotillaPrograma.id, lTAMCheckoutWrapperClass, 'prueba@hotmail.com', 'prueba@hotmail.com');
           //Actualiza el reg del DOD para que pase el Trigger de: TAM_DODSolicitPedidoEspecial_tgr_handler   
           for (TAM_DODSolicitudesPedidoEspecial__c objDod : [Select Id, Name, TAM_CatalogoCentralizadoModelos__c, 
                TAM_SolicitudFlotillaPrograma__c, TAM_Pago__c, TAM_PoliticaIncentivos__c,
                TAM_DescuentoAutorizadoDtm__c, TAM_IncentivoTMEXPorcentaje__c, TAM_IncDealerPorcentaje__c, TAM_IncPropuestoCash__c,
                TAM_IncentivoTMEXCash__c, TAM_IncDealerCash__c, TAM_VIN__c, TAM_Historico__c, TAM_CantidadProducir__c, 
                TAM_IdExterno__c From TAM_DODSolicitudesPedidoEspecial__c LIMIT 1]){
	           objDod.TAM_ActRegCheckOut__c = true;    
               objDod.TAM_VIN__c = 'XXXXXXXXXX12345678';	           
	           System.debug('EN TAM_CheckOutProgramaCtrlOK objDod: ' + objDod);
	           //Actualiza el reg
	           Upsert objDod TAM_DODSolicitudesPedidoEspecial__c.TAM_IdExterno__c;               
           }
		   //Llama la clase de TAM_CargaArchivosCtrl y metodo getSolicitud
	       TAM_CheckOutProgramaCtrl.getSolicitud(solFlotillaPrograma.id);
		   //Llama la clase de TAM_CargaArchivosCtrl y metodo getSolicitud
	       TAM_CheckOutProgramaCtrl.getReistrosCheckOutSolicitud(solFlotillaPrograma.id);
		   //Llama la clase de TAM_CargaArchivosCtrl y metodo getSolicitud
	       TAM_CheckOutProgramaCtrl.getTipoRegFlotilla(); 
		   //Llama la clase de TAM_CargaArchivosCtrl y metodo getPoliticasIncentivos
	       TAM_CheckOutProgramaCtrl.getIncentivosFlotillaModelo(solFlotillaPrograma.ID, '220602020');
       
       Test.stopTest();
		
    }
    
}