/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_DelInvVehiToySch y
    					eliminar los registros del objeto TAM_InventarioVehiculosToyota__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_DelInvVehiToyBchTst {

 	@TestSetup static void loadData(){

		CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
        	Name = 'AVANZA-22060-2020-B79-10-LE AT',
        	Marca__c = 'Toyota',
        	Serie__c = 'AVANZA',
        	Modelo__c = '22060',
        	AnioModelo__c = '2020',
        	CodigoColorExterior__c = 'B79', 
        	CodigoColorInterior__c = '10',
        	DescripcionColorExterior__c = 'Azul', 
        	DescripcionColorInterior__c = 'Gris',
        	Version__c = 'LE AT',
        	Disponible__c = 'SI'
	    );
	    insert catCenMod;  
				
		TAM_InventarioVehiculosToyota__c TAMInventarioVehiculosToyota = new TAM_InventarioVehiculosToyota__c(
			Name='MR2B29F36K1168300', 
			Dealer_Code__c='57002', 
			Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
			Dummy_VIN__c='36K1168300', 
			Exterior_Color_Code__c='01G3', 
			Exterior_Color_Description__c='GRAY ME.', 
			Interior_Color_Code__c='FC20', 
			Interior_Color_Description__c='S/D BLACK', 
			Model_Number__c='22060', 
			Model_Year__c='2020', 
			Toms_Series_Name__c='YARIS SD'
	    );
	    insert TAMInventarioVehiculosToyota;
				             				
	}

    static testMethod void TAM_DelInvVehiToyBchTstOk() {

		//Inicia las pruebas    	
        Test.startTest();

			String sQuery = ' SELECT Id ';
			sQuery += ' FROM TAM_InventarioVehiculosToyota__c ';
			sQuery += ' Limit 1';
			System.debug('EN TAM_DelInvVehiToyBchTstOk.execute sQuery: ' + sQuery);
			
			//Crea el objeto de  OppUpdEnvEmailBch_cls   	    	    
			TAM_DelInvVehiToyBch objTAMDelInvVehiToyBch = new TAM_DelInvVehiToyBch(sQuery);
    	    
        	//No es una prueba entonces procesa de 1 en 1
       		Id batchInstanceId = Database.executeBatch(objTAMDelInvVehiToyBch, 1);

	        string strSeconds = '0';
    	    string strMinutes = '0';
	        string strHours = '1';
	        string strDay_of_month = 'L';
	        string strMonth = '6,12';
	        string strDay_of_week = '?';
	        string strYear = '2050-2051';
	        String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
       		
       		TAM_DelInvVehiToySch objDelInvVehiToySch = new TAM_DelInvVehiToySch();       		
       		System.schedule('Ejecuta_TAM_DelInvVehiToySch:', sch, objDelInvVehiToySch);
       		
        Test.stopTest();
        
    }
}