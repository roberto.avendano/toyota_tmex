/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para crear las Excepciones para las solicitudes de
    					Venta Retail, Flotilla y Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    11-Mayo-2020      Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitCerradaFlotProgExcepcionCtrl {

    static String sRectorTypePasoFlotilla = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();

    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();

	static	List<String> lMesesSolicitudesCons = new List<String>{'ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'};    		

    //Mostrar distribuidor seleccionado en modal
    @AuraEnabled
    public static List<String> getMesesSolicitud(){
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMesesSolicitud...');	
		//Map<String, String> lMesesSolicitudesFinal = new Map<String, String>();
		List<String> lMesesSolicitudesFinal = new List<String>();
				
		Date dtFechaActual = Date.today();
		Integer intAnioActual = dtFechaActual.year();
		Integer intMes = dtFechaActual.month() -1;
		Integer intMesIni = intMes;
		Integer intMesFin = intMesIni - 6;

		Integer cntIniPaso = intMesIni;
        System.debug('cntIniPaso: ' + cntIniPaso + ' intMesIni: ' + intMesIni + ' intMesFin: '  + intMesFin);
		for (Integer cntIni = intMesIni; cntIni > intMesFin; cntIni--){
			if (cntIniPaso == -1 )
				cntIniPaso = 11;
			lMesesSolicitudesFinal.add(lMesesSolicitudesCons.get(cntIniPaso));				
			cntIniPaso--;
		}		
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMesesSolicitud lMesesSolicitudesFinal: ' + lMesesSolicitudesFinal);	

		//Regresa la lista de lMesesSolicitudesFinal
		return lMesesSolicitudesFinal;
	}

    //Mostrar distribuidor seleccionado en modal
    @AuraEnabled
    public static Map<String, String> getMapMesesSolicitud(){
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMapMesesSolicitud...');	
		Map<String, String> mapMesesSolicitudesFinal = new Map<String, String>();
				
		Date dtFechaActual = Date.today();
		Integer intAnioActual = dtFechaActual.year();
		Integer intMes = dtFechaActual.month() -1;
		Integer intMesIni = intMes;
		Integer intMesFin = intMesIni - 6;

		Integer cntIniPaso = intMesIni;
        System.debug('cntIniPaso: ' + cntIniPaso + ' intMesIni: ' + intMesIni + ' intMesFin: '  + intMesFin);
		for (Integer cntIni = intMesIni; cntIni > intMesFin; cntIni--){
			String sFechaPaso = '';
			if (cntIniPaso == -1 ){
				sFechaPaso = 'CALENDAR_YEAR(TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c) = ' + (intAnioActual - 1) + ' And ';
				sFechaPaso += 'CALENDAR_MONTH(TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c) = ' + (cntIniPaso + 1) + '';					
                cntIniPaso = 11;
			}else{
				sFechaPaso = 'CALENDAR_YEAR(TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c) = ' + intAnioActual + ' And ';
				sFechaPaso += 'CALENDAR_MONTH(TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c) = ' + (cntIniPaso + 1) + '';					
			}
            System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMapMesesSolicitud cntIniPaso: ' + cntIniPaso + ' sFechaPaso: ' + sFechaPaso); 
			mapMesesSolicitudesFinal.put(lMesesSolicitudesCons.get(cntIniPaso), sFechaPaso);				
			cntIniPaso--;
		}		
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getMapMesesSolicitud mapMesesSolicitudesFinal: ' + mapMesesSolicitudesFinal);	

		//Regresa la lista de mapMesesSolicitudesFinal
		return mapMesesSolicitudesFinal;
	}

    //Mostrar distribuidor seleccionado en modal
    @AuraEnabled
    public static List<TAM_WrpSolicitudFlotillaProgramaModelos> getSolicitudesFlotillaPrograma(String strTipoconsulta,
    	String strValor, Map<String, String> mapMesesSolicitud){
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla strTipoconsulta: ' + strTipoconsulta + ' strValor: ' + strValor);
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapMesesSolicitud: ' + mapMesesSolicitud);
    	
    	String sIdPerfil = Userinfo.getProfileId();
    	Boolean bAdmin = false;
    	for (User usuarioActual : [Select u.Profile.Name, u.ProfileId From User u
    		Where ProfileId = :sIdPerfil]){
    		String strPerfil = usuarioActual.Profile.Name;
    		if (strPerfil.contains('Admin'))
    			bAdmin = true;
    	}
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla bAdmin: ' + bAdmin);    	
    	
    	List<TAM_WrpSolicitudFlotillaProgramaModelos> lWrpSolFlotProgModelos = new List<TAM_WrpSolicitudFlotillaProgramaModelos>();
    	Map<String, Map<String, TAM_DetalleOrdenCompra__c>> mapIdDatosSolLModelos = new Map<String, Map<String, TAM_DetalleOrdenCompra__c>>();
    	Map<String, Map<String, String>> mapIdDatosSolLModelosTipoPago = new Map<String, Map<String, String>>();
    	Map<String, Map<String, Integer>> mapIdDatosSolLModelosTotVines = new Map<String, Map<String, Integer>>();
    	Map<String, Map<String, Integer>> mapIdDatosSolLModelosTotVinesPD = new Map<String, Map<String, Integer>>();
    	Map<String, String> mapIdSolNoDistProp = new Map<String, String>();
    	Map<String, String> mapIdPropNodDist = new Map<String, String>();
    	String sNoDistribUsrActual;
        
        //Ve si se trata del tipo de consulta por VIN
        if (strTipoconsulta == 'VinSolicitud'){
            //Busca el vin el el objeto de 
            for (TAM_DODSolicitudesPedidoEspecial__c objSolFlot : [Select t.TAM_VIN__c, t.TAM_SolicitudFlotillaPrograma__c, 
                t.TAM_SolicitudFlotillaPrograma__r.Name From TAM_DODSolicitudesPedidoEspecial__c t 
                Where t.TAM_VIN__c =:strValor And TAM_SolicitudFlotillaPrograma__r.TAM_Estatus__c = 'Cerrada' 
                ORDER BY t.TAM_VIN__c]){
                //Encontro el vin en una solicitud 
                strTipoconsulta = 'FolioSolicutd';
                strValor = objSolFlot.TAM_SolicitudFlotillaPrograma__r.Name;
            }            
            System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla ES POR VinSolicitud Y EXISTE EL VIN strTipoconsulta: ' + strTipoconsulta + ' strValor: ' + strValor);
        }//Fin si strTipoconsulta == 'VinSolicitud'

        //Crea el SQL para la solicitud que contiene el VIN     	
    	String sWQuery = 'Select Id, Name,' +
    			'TAM_EntregarEnMiDistribuidora__c, TAM_Cantidad__c, TAM_IdExterno__c, RecordType.Name,' +
    		    'TAM_CatalogoCentralizadoModelos__c,' + 
    			'TAM_CatalogoCentralizadoModelos__r.Name, ' +
    			'TAM_CatalogoCentralizadoModelos__r.CodigoColorExterior__c, ' +
    			'TAM_CatalogoCentralizadoModelos__r.DescripcionColorExterior__c, ' +
    			'TAM_CatalogoCentralizadoModelos__r.CodigoColorInterior__c, ' +
    			'TAM_CatalogoCentralizadoModelos__r.DescripcionColorInterior__c, ' +  	
    			'TAM_CatalogoCentralizadoModelos__r.Version__c, ' +
    			'TAM_SolicitudFlotillaPrograma__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.OwnerId, ' +
    			'TAM_SolicitudFlotillaPrograma__r.Name, ' +
    			'TAM_SolicitudFlotillaPrograma__r.RecordType.Name, ' +
    			'TAM_SolicitudFlotillaPrograma__r.Owner.Name, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, ' + 
    			'TAM_SolicitudFlotillaPrograma__r.TAM_NombreClienteCompleto__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_NombreCompletoDistribuidor__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.Name, ' +    			
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.Name, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_IdExternoNombre__c, ' + 
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_FechaVigenciaFin__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_TipoPersonaProspecto__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.RFC__c, '+
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_RFC_1__pc, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_ProgramaRango__c, ' + 
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_ProgramaRango__r.Name, ' + 
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_ProgramaRango__r.NumUnidadesMinimas__c, ' +
    			'TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_ProgramaRango__r.NumUnidadesMaximas__c, ' +
    			'(Select TAM_IdExterno__c, TAM_Cuenta__r.Codigo_Distribuidor__c From TAM_DetalleDistribFlotillas__r), ' + 
    			'(Select Name, TAM_Estatus__c From TAM_DetalleVinesFlotillas__r) ' + 
    			'From TAM_DetalleOrdenCompra__c ' +
    			'Where TAM_SolicitudFlotillaPrograma__r.TAM_Estatus__c = \'Cerrada\' '+
    			' And ( RecordType.Name = \'Pedido Especial\' ' +
    			' OR RecordType.Name = \'Inventario\' )';  		    			
    			//Se trata de FechaCierre
    			if (strTipoconsulta == 'FechaCierre'){
    				String sWQueryPaso = mapMesesSolicitud.get(strValor);
    				sWQuery += ' And ' + sWQueryPaso + ' ';    				
    			}
    			//Se trata de FechaCierre    			
    			if (strTipoconsulta == 'FolioSolicutd')
    				sWQuery += 'And TAM_SolicitudFlotillaPrograma__r.Name = \'' + strValor + '\' ';
    			if (strTipoconsulta == 'NombreClienteSolicitud'){
    				String strValorPaso = '%' + strValor.toUpperCase() + '%';
    				sWQuery += 'And TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_IdExternoNombre__c LIKE \'' + strValorPaso + '\' ';    				
    			}
    			sWQuery +='Order by TAM_SolicitudFlotillaPrograma__r.Name ';
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sWQuery: ' + sWQuery);
        System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla strTipoconsulta: ' + strTipoconsulta);
    	
    	//Ejecuta la consulta
    	List<SObject> lSObject = new List<SObject>();
        if (strTipoconsulta != 'VinSolicitud')    	
        	lSObject = Database.query(sWQuery);
		
		for (SObject objSolFlotPaso : lSObject){
			TAM_DetalleOrdenCompra__c objSolFlot = (TAM_DetalleOrdenCompra__c)objSolFlotPaso;  
	    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla TAM_NombreClienteCompleto__c: ' + objSolFlot.TAM_SolicitudFlotillaPrograma__r.TAM_NombreClienteCompleto__c);		
	    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla TAM_NombreClienteCompleto__c.TAM_DetalleDistribFlotillas__r: ' + objSolFlot.TAM_DetalleDistribFlotillas__r);				
		}
		
		//Cionsulta los datos de los lSObject
		for (SObject SolicitudFlotProgPaso : lSObject){
			TAM_DetalleOrdenCompra__c SolicitudFlotProg = (TAM_DetalleOrdenCompra__c) SolicitudFlotProgPaso;  
    			
    		String sIdCatCenModName = SolicitudFlotProg.TAM_CatalogoCentralizadoModelos__r.Name;
    		String sIdCatCenMod = SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name + '-' + sIdCatCenModName.toUpperCase();
    		
    		//Mete los datos de la sol y el propietario a el mapa de mapIdSolNoDistProp
    		mapIdSolNoDistProp.put(SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name, SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.OwnerId);
    		
    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
    		if (mapIdDatosSolLModelos.containsKey(SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name))
    			mapIdDatosSolLModelos.get(SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name).put(sIdCatCenMod, SolicitudFlotProg);
    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
    		if (!mapIdDatosSolLModelos.containsKey(SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name))
    			mapIdDatosSolLModelos.put(SolicitudFlotProg.TAM_SolicitudFlotillaPrograma__r.Name, new Map<String, TAM_DetalleOrdenCompra__c>{sIdCatCenMod => SolicitudFlotProg});    		
    	}//Fin del for para las TAM_CheckOutDetalleSolicitudCompra__c 
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelos: ' + mapIdDatosSolLModelos.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sMesFechaCierre: ' + mapIdDatosSolLModelos.Values());

    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdSolNoDistProp '+ mapIdSolNoDistProp.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdSolNoDistProp '+ mapIdSolNoDistProp.Values());    	
    	//Consulta el No de distribuidor del Propoietario de la sol
    	for (User usuario : [Select u.Name, u.CodigoDistribuidor__c 
    		From User u Where (id IN :mapIdSolNoDistProp.values() OR id =: UserInfo.getUserId())
    		And CodigoDistribuidor__c != null]){
   			mapIdPropNodDist.put(usuario.id, usuario.CodigoDistribuidor__c);	
    		if (usuario.id == UserInfo.getUserId())
    			sNoDistribUsrActual = usuario.CodigoDistribuidor__c;    			
    	}//Fin del for para User
    	if (sNoDistribUsrActual == null)
    		sNoDistribUsrActual = '57000';
    		
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdPropNodDist '+ mapIdPropNodDist.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdPropNodDist '+ mapIdPropNodDist.Values());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sNoDistribUsrActual '+ sNoDistribUsrActual);
    	    	
    	//Consulta los reg de la solicitudes en TAM_CheckOutDetalleSolicitudCompra__c donde la solicitud sea igual a
    	for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOut : [Select id, Name, TAM_IdExterno__c, TAM_TipoPago__c, 
    		TAM_SolicitudFlotillaPrograma__r.Name, TAM_CatalogoCentralizadoModelos__r.Name, TAM_TipoVenta__c, TAM_Cantidad__c 
    		From TAM_CheckOutDetalleSolicitudCompra__c
    		Where TAM_SolicitudFlotillaPrograma__r.Name IN :mapIdDatosSolLModelos.KeySet()
    		And (TAM_TipoVenta__c = 'Pedido Especial' OR TAM_TipoVenta__c = 'Inventario')]){
    		//Toma los datos del TAM_IdExterno__c
    		//a20e0000003BYkoAAG-2020-AVANZA-2205-1G3-10-012e0000000AYwbAAG-012e0000000AYwZAAW-Arrendadora
    		String[] arrTamIdExternoPaso = objCheckOut.TAM_IdExterno__c.split('-');
    		String sIdCatCenModName = objCheckOut.TAM_CatalogoCentralizadoModelos__r.Name;
    		String sTamIdExterno = objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name + '-' + sIdCatCenModName.toUpperCase();
    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
    		if (mapIdDatosSolLModelosTipoPago.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name))
    			mapIdDatosSolLModelosTipoPago.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).put(sTamIdExterno, objCheckOut.TAM_TipoPago__c);
    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
    		if (!mapIdDatosSolLModelosTipoPago.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name))
    			mapIdDatosSolLModelosTipoPago.put(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name, new Map<String, String>{sTamIdExterno => objCheckOut.TAM_TipoPago__c});

			//Se trata de Pedido Especial
			if (objCheckOut.TAM_TipoVenta__c == 'Pedido Especial'){
	    		if (mapIdDatosSolLModelosTotVinesPD.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name)){
		    		if (!mapIdDatosSolLModelosTotVinesPD.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).containsKey(sTamIdExterno))
		    			mapIdDatosSolLModelosTotVinesPD.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).put(sTamIdExterno, Integer.valueOf(objCheckOut.TAM_Cantidad__c));	
	    		}//Fin si mapIdDatosSolLModelosTotVinesPD.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name)
	    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
	    		if (!mapIdDatosSolLModelosTotVinesPD.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name))
	    			mapIdDatosSolLModelosTotVinesPD.put(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name, new Map<String, Integer>{sTamIdExterno => Integer.valueOf(objCheckOut.TAM_Cantidad__c)});
			}//Fin si objCheckOut.TAM_TipoVenta__c == 'Inventario'
			
			//Se trata de Inventario
			if (objCheckOut.TAM_TipoVenta__c == 'Inventario'){
	    		if (mapIdDatosSolLModelosTotVines.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name)){
		    		if (mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).containsKey(sTamIdExterno)){
		    			Integer intTotal = mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).get(sTamIdExterno) + 1;
		    			mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).put(sTamIdExterno, intTotal);	
		    		}//Fin si mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).containsKey(sTamIdExterno)
		    		if (!mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).containsKey(sTamIdExterno))
		    			mapIdDatosSolLModelosTotVines.get(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name).put(sTamIdExterno, 1);	
	    		}//Fin si mapIdDatosSolLModelosTotVines.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name)
	    		//Ve si ya existe la llave en el mapa de mapIdDatosSolLModelos
	    		if (!mapIdDatosSolLModelosTotVines.containsKey(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name))
	    			mapIdDatosSolLModelosTotVines.put(objCheckOut.TAM_SolicitudFlotillaPrograma__r.Name, new Map<String, Integer>{sTamIdExterno => 1});
			}//Fin si objCheckOut.TAM_TipoVenta__c == 'Inventario'
    			
    	}
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTipoPago: ' + mapIdDatosSolLModelosTipoPago.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTipoPago: ' + mapIdDatosSolLModelosTipoPago.Values());

    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTotVines: ' + mapIdDatosSolLModelosTotVinesPD.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTotVines: ' + mapIdDatosSolLModelosTotVinesPD.Values());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTotVines: ' + mapIdDatosSolLModelosTotVines.KeySet());
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla mapIdDatosSolLModelosTotVines: ' + mapIdDatosSolLModelosTotVines.Values());
    	    	
    	//Ya tienes el mapa con todas las solicitudes cerradas en la fecha tal metelas a la lista de lWrpSolFlotProgModelos
		for(String sFolioSol : mapIdDatosSolLModelos.KeySet()){
			System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sFolioSol: ' + sFolioSol);				
			//Crea la lista de modelos para TAM_WrpSolicitudFlotillaProgramaModelos.lModelosSeleccionados
			List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> lWrpDatModSel = 
				new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>();
			//Recorre todos los registros de sFolioSol
			for (String sIdExtMod : mapIdDatosSolLModelos.get(sFolioSol).KeySet()){
				//Crea el objeto del tipo TAM_DetalleOrdenCompra__c
				TAM_DetalleOrdenCompra__c objDetOrdCompra = mapIdDatosSolLModelos.get(sFolioSol).get(sIdExtMod);
				System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla objDetOrdCompra.RecordType.Name: ' + objDetOrdCompra.RecordType.Name);

				//Inicializa el objeto del tipo TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados
				TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objPasoModelos = 
					new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
				//Inicializa los datos del objeto objPasoModelos
				objPasoModelos.strIdSolicitud = objDetOrdCompra.TAM_SolicitudFlotillaPrograma__c;
				objPasoModelos.strNombre = objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.Name;
				objPasoModelos.strVersion = objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.Version__c;
				objPasoModelos.strDescripcionColorExterior = objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.CodigoColorExterior__c + '-' + objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.DescripcionColorExterior__c; 
				objPasoModelos.strDescripcionColorInterior = objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.CodigoColorInterior__c + '-' + objDetOrdCompra.TAM_CatalogoCentralizadoModelos__r.DescripcionColorInterior__c; 
				objPasoModelos.strCantidadSolicitada = String.valueOf(objDetOrdCompra.TAM_Cantidad__c);
				Integer intCantidadSolicitada = 0;
				if (mapIdDatosSolLModelosTotVinesPD.containsKey(sFolioSol)){
					intCantidadSolicitada = mapIdDatosSolLModelosTotVinesPD.get(sFolioSol).containsKey(sIdExtMod) ? (mapIdDatosSolLModelosTotVinesPD.get(sFolioSol).get(sIdExtMod) != null ? mapIdDatosSolLModelosTotVinesPD.get(sFolioSol).get(sIdExtMod) : 0) : 0;
					System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla intCantidadSolicitada1: ' + intCantidadSolicitada);							
				}
				if (mapIdDatosSolLModelosTotVines.containsKey(sFolioSol)){
					intCantidadSolicitada += mapIdDatosSolLModelosTotVines.get(sFolioSol).containsKey(sIdExtMod) ? (mapIdDatosSolLModelosTotVines.get(sFolioSol).get(sIdExtMod) != null ? mapIdDatosSolLModelosTotVines.get(sFolioSol).get(sIdExtMod) : 0) : 0;
					System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla intCantidadSolicitada2: ' + intCantidadSolicitada);							
				}
				//Finalmente acttualiza el campo de strCantidadSolicitada 
				objPasoModelos.strCantidadSolicitada = String.valueOf(intCantidadSolicitada);
				System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla objPasoModelos.strCantidadSolicitada: ' + objPasoModelos.strCantidadSolicitada);				
				objPasoModelos.strIdCatCentrModelos = objDetOrdCompra.TAM_CatalogoCentralizadoModelos__c;
				objPasoModelos.strIdExterno = sIdExtMod;
				//Busca objDetOrdCompra en mapIdDatosSolLModelosTipoPago
				if (mapIdDatosSolLModelosTipoPago.containsKey(sFolioSol)){
					if (mapIdDatosSolLModelosTipoPago.get(sFolioSol).containsKey(sIdExtMod)){
						objPasoModelos.strTipoVenta = mapIdDatosSolLModelosTipoPago.get(sFolioSol).get(sIdExtMod);
						System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sFolioSol: ' + sFolioSol + ' sIdExtMod: ' + sIdExtMod + ' strTipoVenta: ' + objPasoModelos.strTipoVenta );
					}//Fin si mapIdDatosSolLModelosTipoPago.get(sFolioSol).containsKey(sIdExtMod)
				}//Fin si mapIdDatosSolLModelosTipoPago.containsKey(sFolioSol)
				System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sFolioSol: ' + sFolioSol + ' objPasoModelos: ' + objPasoModelos);
				//Tiene distribiudores
				if (!objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c){
					if(!objDetOrdCompra.TAM_DetalleDistribFlotillas__r.isEmpty()){
						System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla TAM_EntregarEnMiDistribuidora__c0: ' + objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c + ' NoDistriOwnerSol: ' + mapIdPropNodDist.get(mapIdSolNoDistProp.get(sFolioSol)) + ' sNoDistribUsrActual: ' + sNoDistribUsrActual);					
						if (mapIdPropNodDist.containsKey(mapIdSolNoDistProp.get(sFolioSol)))
							if (mapIdPropNodDist.get(mapIdSolNoDistProp.get(sFolioSol)) == sNoDistribUsrActual)
								objPasoModelos.bolMismoDistribuidor = false;
						if (mapIdPropNodDist.get( mapIdSolNoDistProp.get(sFolioSol) ) == null){
							System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla UserInfo.getUserId(): ' + UserInfo.getUserId() + ' mapIdPropNodDist.KeySet(): ' + mapIdPropNodDist.KeySet());
							if (mapIdPropNodDist.containsKey(UserInfo.getUserId())){
								if (mapIdPropNodDist.get(UserInfo.getUserId()) == sNoDistribUsrActual)
									objPasoModelos.bolMismoDistribuidor = true;
							}
						}
					}//Fin si !objDetOrdCompra.TAM_DetalleDistribFlotillas__r.isEmpty()
				}//Fin si !objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c
				//No tiene distribiudores
				if (objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c){
					System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla TAM_EntregarEnMiDistribuidora__c1: ' + objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c + ' NoDistriOwnerSol: ' + mapIdPropNodDist.get(mapIdSolNoDistProp.get(sFolioSol)) + ' sNoDistribUsrActual: ' + sNoDistribUsrActual);
					if ( mapIdPropNodDist.get(mapIdSolNoDistProp.get(sFolioSol)) == sNoDistribUsrActual )
							objPasoModelos.bolMismoDistribuidor = true;			
				}//Fin si objDetOrdCompra.TAM_EntregarEnMiDistribuidora__c
								
				//No tiene distribiudores
				if (mapIdPropNodDist.get(mapIdSolNoDistProp.get(sFolioSol)) == sNoDistribUsrActual)
					objPasoModelos.bolCreaExcepcion = true;

				//Ve si se trata del administrador para que habilites el boton de comfirmar
				if (bAdmin){
					objPasoModelos.bolMismoDistribuidor = true;			
					objPasoModelos.bolCreaExcepcion = true;
				}//Fin si bAdmin
				
				//objPasoModelos.bolMismoDistribuidor = true;
				System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla objPasoModelos: ' + objPasoModelos);				
				//Agregala a la lista de lWrpDatModSel
				lWrpDatModSel.add(objPasoModelos);
			}//fin del for para mapIdDatosSolLModelos.get(sFolioSol)

			//Toma el primer reg de la orden de compra
			Map<String, TAM_DetalleOrdenCompra__c> mapObjDetOrdCompra = mapIdDatosSolLModelos.get(sFolioSol);
			TAM_DetalleOrdenCompra__c objDetOrdCompra = mapObjDetOrdCompra.Values().get(0);
			//Acorta el nombre para que se muestra unaparte 
			String sFolioSolCCorto = objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_NombreClienteCompleto__c;  
			sFolioSolCCorto = sFolioSolCCorto != null ? (sFolioSolCCorto.length() > 40 ? sFolioSolCCorto.substring(0,40) : sFolioSolCCorto) : '';
			System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla sFolioSolCCorto: ' + sFolioSolCCorto);				
			
			String sDatosSolicitud = sFolioSol + ' - ' + String.valueOf(objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c) + ' - ' +
			sFolioSolCCorto + ' - ' + //objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_NombreClienteCompleto__c 
			objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.RecordType.Name + ' - ' +
			objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.Name + ' - ' +
			objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_NombreCompletoDistribuidor__c; 
			//Crear los datos del cliente en el objeto TAM_WrpSolicitudFlotillaProgramaModelos}
			TAM_WrpSolicitudFlotillaProgramaModelos objWrpSolFlotProMod = new TAM_WrpSolicitudFlotillaProgramaModelos(
				sFolioSolCCorto, sDatosSolicitud, sFolioSol, 
				String.valueOf(objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c),
				objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_NombreClienteCompleto__c,
				objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.RecordType.Name,
				objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.Name,
				objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.Owner.Name,
				String.valueOf(objDetOrdCompra.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__r.TAM_FechaVigenciaFin__c),
				lWrpDatModSel
			);
			System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla objWrpSolFlotProMod: ' + objWrpSolFlotProMod);
			
			//Agrega a la lista de lWrpSolFlotProgModelos el objeto objWrpSolFlotProMod
			lWrpSolFlotProgModelos.add(objWrpSolFlotProMod);
			
		}//Fin del for para mapIdDatosSolLModelos.KeySet()
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getDistribuidoresFlotilla lWrpSolFlotProgModelos: ' + lWrpSolFlotProgModelos);
    	
        //Regresa la lista de lstDistOrdenCompra
        return lWrpSolFlotProgModelos;
    }
		
    @AuraEnabled
    public static TAM_WrpSolicitudFlotillaProgramaModelos getWrpSolFlotProgFinal(String strIdExterno, 
    	TAM_WrpSolicitudFlotillaProgramaModelos varObjModeloSelExcep ){
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getWrpSolFlotProgFinal strIdExterno: ' + strIdExterno + ' varObjModeloSelExcep: ' + varObjModeloSelExcep);
    	
    	TAM_WrpSolicitudFlotillaProgramaModelos objPaso = new TAM_WrpSolicitudFlotillaProgramaModelos();
		List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> lObjetoPasoModeolos = 
			new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>(); 
		
    	//Inicializa el objeto de objPaso 
    	objPaso.strNombreCorto = varObjModeloSelExcep.strNombreCorto;
    	objPaso.strDatosSolicitd = varObjModeloSelExcep.strDatosSolicitd;
    	objPaso.strFolioSolicitd = varObjModeloSelExcep.strFolioSolicitd;
    	objPaso.strFechaCierre = varObjModeloSelExcep.strFechaCierre;
    	objPaso.strNombreCliente = varObjModeloSelExcep.strNombreCliente;
    	objPaso.strTipoSolicitud = varObjModeloSelExcep.strTipoSolicitud;
    	objPaso.strRangoPrograma = varObjModeloSelExcep.strRangoPrograma;
    	objPaso.strPropietario = varObjModeloSelExcep.strPropietario;
    	objPaso.strFechaVigencia = varObjModeloSelExcep.strFechaVigencia;

    	//Recorre la lista de lModelosSeleccionados del objeto de varObjModeloSelExcep
    	for (TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objPasoMod : varObjModeloSelExcep.lModelosSeleccionados){
    		System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getWrpSolFlotProgFinal strIdExterno: ' + strIdExterno + ' strIdCatCentrModelos: ' + objPasoMod.strIdCatCentrModelos);
    	}
    	    	
    	//Recorre la lista de lModelosSeleccionados del objeto de varObjModeloSelExcep
    	for (TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados objPasoMod : varObjModeloSelExcep.lModelosSeleccionados){
    		if (objPasoMod.strIdExterno == strIdExterno){
    			System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getWrpSolFlotProgFinal objPasoMod: ' + objPasoMod);
    			objPaso.lModelosSeleccionados.add(objPasoMod);
    			break;
    		}//Fin si objPaso.strIdExterno == strIdExterno
    	}//Fin del for para varObjModeloSelExcep.lModelosSeleccionados
    	    	
    	System.debug('EN TAM_SolicitCerradaFlotProgExcepcionCtrl.getWrpSolFlotProgFinal strIdExterno: ' + strIdExterno + ' objPaso: ' + objPaso);
        return objPaso;
    }


    
}