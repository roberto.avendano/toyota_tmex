public class ReporteResultadoEIntegralController {
    public String doc{
        get{
            doc= [SELECT Id, DeveloperName FROM Document WHERE DeveloperName='ToyotaReporteIntegralLogoDe'].Id;
            return doc;
        }
        set;
    }
    public Evaluaciones_Dealer__c eD{get;set;}
    public ReporteResultadoEIntegralController(){}
    
    public ReporteResultadoEIntegralController(ApexPages.StandardController controller){             	
        eD=(Evaluaciones_Dealer__c)controller.getRecord();        
    }
    
    public Evaluaciones_Dealer__c getObjEval(){
    	return [SELECT Id, Name, PuntosObtenidos__c,PuntosTotales__c, RecordType.DeveloperName, (SELECT Id, Name, Owner.Name,Condicion_Observada__c,Actividad_Mejora__c, Fecha_Compromiso__c, Responsable_Dealer__c  FROM API__r)FROM Evaluaciones_Dealer__c WHERE Id=:eD.Id];    
    }
        
}