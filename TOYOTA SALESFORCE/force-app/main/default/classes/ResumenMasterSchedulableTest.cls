@isTest
public class ResumenMasterSchedulableTest {
	
	@isTest static void test_general() {
		Test.startTest();
			ResumenMasterSchedulable che = new ResumenMasterSchedulable();	
			String cron = '0 0 3 * * ? ';
			String jobID = system.schedule('Merge Job', cron, che);

			CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id=:jobID];
			
		Test.stopTest();
	}	
}