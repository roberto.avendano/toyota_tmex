@isTest
global class HttpCalloutMockRH implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type','application/json');
        res.setBody('{"controllerValues" : { },"defaultValue" : null,"eTag" : "bcbd3def49051ea6e9117926822cbb41","url" : "/services/data/v45.0/ui-api/object-info/RH_EvaluacionCandidato__c/picklist-values/0123F0000013HKDQA2/De_Respuesta__c","values" : [ {"attributes" : null,"label" : "Pedro Zavala (Director Corporativo Finanzas y Admon.)","validFor" : [ ],"value" : "Pedro Zavala (Director Corporativo Finanzas y Admon.)"}]}');
        res.setStatusCode(200);
        return res;
    }
}