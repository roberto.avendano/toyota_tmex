/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para desactivar las cuentas corporativas que ya 
    					vencieron su vigencia TAM_InactivaCorporateAccountsBch

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    20-Octubre-2020 		Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_InactivaCorporateAccountsBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    global class wrpFechasConsSol{
        global Date dtFechaIni {get;set;}
        global Date dtFechaFin {get;set;}
        //Un constructor por default
        public wrpFechasConsSol(){
            this.dtFechaIni = Date.today();
            this.dtFechaFin = Date.today();
        }
        //Un constructor con parametros  
        public wrpFechasConsSol(Date dtFechaIni, Date dtFechaFin){
            this.dtFechaIni = dtFechaIni;
            this.dtFechaFin = dtFechaFin;
        }
    }
    
    //Un constructor por default
    global TAM_InactivaCorporateAccountsBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_InactivaCorporateAccountsBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Account> scope){
        System.debug('EN TAM_InactivaCorporateAccountsBch.');

	    String VaRtCtePF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
    	String VaRtCtePM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
        
		List<AccountShare> lAccountShare = new List<AccountShare>();
		List<Account> lAccountInactUpd = new List<Account>();
        List<Account> lAccountActivaUpd = new List<Account>();

        Map<String, Account> mapIdCteActivo = new Map<String, Account>();      

		Set<String> setIdCteCorp = new Set<String>();
		Map<String, Set<String>> mapIdCteCorpVin = new Map<String, Set<String>>();
        Map<String, wrpFechasConsSol> mapIdCteWrpFechas = new Map<String, wrpFechasConsSol>();
		Set<String> setVinCte = new Set<String>();
        Map<String, Integer> mapIdCteTotRango = new Map<String, Integer>();
        Map<String, Integer> mapIdCteTotVtas = new Map<String, Integer>();		

        Set<String> setIdCteDesactivar = new Set<String>();
		
		//Crea un objeto del tipo 
        SavePoint spRollbck = Database.setSavepoint();
				
        //Recorre la lista de Casos para cerrarlos 
        for (Account objCteVencido : scope){
            //Toma el ID de la cuenta ´para buscar todas su solicitudes cerradas en el los ultimos 6 o doce meses
            setIdCteCorp.add(objCteVencido.id);
            mapIdCteActivo.put(objCteVencido.id, objCteVencido);
        }
       	System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteActivo: ' + mapIdCteActivo.keyset());
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteActivo: ' + mapIdCteActivo.values());
        System.debug('EN TAM_InactivaCorporateAccountsBch setIdCteCorp: ' + setIdCteCorp);
        
        //Consulta los dias de l rango del cliente
        for (TAM_CheckOutDetalleSolicitudCompra__c objDoDPaso : [Select t.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__c, 
            t.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.NumUnidadesMinimas__c, 
            t.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.DiasVigentes__c, TAM_EstatusDealerSolicitud__c,
            t.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c, TAM_EstatusDOD__c, TAM_TipoVenta__c   
            From TAM_CheckOutDetalleSolicitudCompra__c t
            Where t.TAM_VIN__c != null And (TAM_TipoVenta__c = 'Pedido Especial' or TAM_TipoVenta__c = 'Inventario')
            And TAM_SolicitudFlotillaPrograma__r.TAM_Estatus__c = 'Cerrada' 
            And t.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c IN : setIdCteCorp
            Order by t.TAM_SolicitudFlotillaPrograma__r.Name, t.TAM_VIN__c]){
            Integer intDiasVigentesPaso = Integer.valueOf(objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.DiasVigentes__c) * -1;
            System.debug('EN TAM_InactivaCorporateAccountsBch intDiasVigentesPaso: ' + intDiasVigentesPaso + ' DiasVigentes__c: ' + objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.DiasVigentes__c + ' NumUnidadesMinimas__c: ' + objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.NumUnidadesMinimas__c);

            //Calcula la fecha inicio y fin
            Date dtFechaFin = Date.today().addDays(-1);
            Date dtFechaIni = Date.today().addDays(intDiasVigentesPaso);
            System.debug('EN TAM_InactivaCorporateAccountsBch dtFechaIni: ' + dtFechaIni + ' dtFechaFin: ' + dtFechaFin);
            
            //Mete los datos del cliente y las fecha de consulta
            if ( (objDoDPaso.TAM_TipoVenta__c == 'Pedido Especial' && objDoDPaso.TAM_EstatusDOD__c == 'Asignado') || objDoDPaso.TAM_TipoVenta__c == 'Inventario' ){
                if (objDoDPaso.TAM_EstatusDealerSolicitud__c != 'En proceso de cancelación' && objDoDPaso.TAM_EstatusDealerSolicitud__c != 'Cancelada'){                    
                    mapIdCteWrpFechas.put(objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c, new wrpFechasConsSol(dtFechaIni, dtFechaFin));
                    mapIdCteTotRango.put(objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c, Integer.valueOf(objDoDPaso.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.NumUnidadesMinimas__c));
                }//Fin si objCheckout.TAM_EstatusDealerSolicitud__c != 'En proceso de cancelación' && objCheckout.TAM_EstatusDealerSolicitud__c != 'Cancelada'
            }//Fin si (objDoDPaso.TAM_TipoVenta__c == 'Pedido Especial' && objDoDPaso.TAM_EstatusDOD__c = 'Asignado')
        }
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteWrpFechas: ' + mapIdCteWrpFechas.keySet());
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteWrpFechas: ' + mapIdCteWrpFechas.values());
        
        //Ya tienes las cuentas asocidas a cada cliente busca sus solicitudes.
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckout : [Select t.TAM_VIN__c, t.TAM_SolicitudFlotillaPrograma__c,
            t.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, 
            t.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__c, TAM_EstatusDealerSolicitud__c,
            t.TAM_SolicitudFlotillaPrograma__r.TAM_ProgramaRango__r.NumUnidadesMinimas__c, 
            t.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c,  t.TAM_EstatusDOD__c, TAM_TipoVenta__c 
            From TAM_CheckOutDetalleSolicitudCompra__c t
            Where t.TAM_VIN__c != null And (TAM_TipoVenta__c = 'Pedido Especial' or TAM_TipoVenta__c = 'Inventario')
            And TAM_SolicitudFlotillaPrograma__r.TAM_Estatus__c = 'Cerrada' 
            And t.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c IN : setIdCteCorp
            Order by t.TAM_SolicitudFlotillaPrograma__r.Name, t.TAM_VIN__c]){
            //Mete los datos del cliente y las fecha de consulta
            if ( (objCheckout.TAM_TipoVenta__c == 'Pedido Especial' && objCheckout.TAM_EstatusDOD__c == 'Asignado') || objCheckout.TAM_TipoVenta__c == 'Inventario' ){
                if (objCheckout.TAM_EstatusDealerSolicitud__c != 'En proceso de cancelación' && objCheckout.TAM_EstatusDealerSolicitud__c != 'Cancelada'){    
		            if (mapIdCteCorpVin.containsKey(objCheckout.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c))
		                mapIdCteCorpVin.get(objCheckout.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c).add(objCheckout.TAM_VIN__c);
		            if (!mapIdCteCorpVin.containsKey(objCheckout.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c))
		                mapIdCteCorpVin.put(objCheckout.TAM_SolicitudFlotillaPrograma__r.TAM_Cliente__c, new Set<String>{objCheckout.TAM_VIN__c});
                }//Fin si objCheckout.TAM_EstatusDealerSolicitud__c != 'En proceso de cancelación' && objCheckout.TAM_EstatusDealerSolicitud__c != 'Cancelada'
            }//Fin si (objCheckoutPaso.TAM_TipoVenta__c == 'Pedido Especial' && objCheckoutPaso.TAM_EstatusDOD__c = 'Asignado')
            //Agrega el vin al ser de setVinCte
            setVinCte.add(objCheckout.TAM_VIN__c);
        }
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteCorpVin: ' + mapIdCteCorpVin.keySet());
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteCorpVin: ' + mapIdCteCorpVin.values());
        System.debug('EN TAM_InactivaCorporateAccountsBch setVinCte: ' + setVinCte);
        
        //Consulta el ultimo mov de los vines de los clientes del mapa mapIdCteCorpVin
        for (Vehiculo__c objVehi : [Select v.Ultimo_Movimiento__r.TAM_FechaEnvForm__c, 
             v.Ultimo_Movimiento__r.Trans_Type__c, v.Ultimo_Movimiento__c, v.Name, v.Id 
             From Vehiculo__c v Where Name IN : setVinCte And Ultimo_Movimiento__r.Trans_Type__c = 'RDR'             
             ]){
            System.debug('EN TAM_InactivaCorporateAccountsBch objVehi: ' + objVehi.Name + ' Trans_Type__c: ' + objVehi.Ultimo_Movimiento__r.Trans_Type__c);
                 
            //busca el vin en el mapa de mapIdCteCorpVin y si existe compara la fecha
            for (String sIdCteCorp  : mapIdCteCorpVin.keySet()){
                System.debug('EN TAM_InactivaCorporateAccountsBch VAL1 sIdCteCorp: ' + sIdCteCorp + ' VIN: ' + objVehi.Name);
                //Ve si existe objVehi.Name en mapIdCteCorpVin.get(sIdCteCorp).contains()
                if ( mapIdCteCorpVin.get(sIdCteCorp).contains(objVehi.Name) ){
                    //Ve si la fecha de envio del Ultimo_Movimiento__r.TAM_FechaEnvForm__c en el mapa de mapIdCteWrpFechas 
                    wrpFechasConsSol objWrpFechasConsSolPaso = mapIdCteWrpFechas.get(sIdCteCorp);
                    System.debug('EN TAM_InactivaCorporateAccountsBch VAL2 objWrpFechasConsSolPaso: ' + objWrpFechasConsSolPaso + ' TAM_FechaEnvForm: ' + objVehi.Ultimo_Movimiento__r.TAM_FechaEnvForm__c);
                    if (objVehi.Ultimo_Movimiento__r.TAM_FechaEnvForm__c >= objWrpFechasConsSolPaso.dtFechaIni && objVehi.Ultimo_Movimiento__r.TAM_FechaEnvForm__c <= objWrpFechasConsSolPaso.dtFechaFin){
                        //Metelo al mapa de mapIdCteTotVtas
                        if (mapIdCteTotVtas.containsKey(sIdCteCorp)){
                            Integer intTotal = mapIdCteTotVtas.get(sIdCteCorp) + 1;
                            mapIdCteTotVtas.put(sIdCteCorp, intTotal);              
                        }//Fin si mapIdCteTotVtas.containsKey(sIdCteCorp)
                        if (!mapIdCteTotVtas.containsKey(sIdCteCorp))
                            mapIdCteTotVtas.put(sIdCteCorp, 1);
                    }//Fin si objVehi.Ultimo_Movimiento__r.TAM_FechaEnvForm__c >= objWrpFechasConsSolPaso.dtFechaIni && objVehi.Ultimo_Movimiento__r.TAM_FechaEnvForm__c <= objWrpFechasConsSolPaso.dtFechaFin
                }//Fin si mapIdCteCorpVin.get(sIdCteCorp).contains(objVehi.Name)
            }//Fin del for para mapIdCteCorpVin.keySet()
        }        
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteTotVtas: ' + mapIdCteTotVtas.keySet());
        System.debug('EN TAM_InactivaCorporateAccountsBch mapIdCteTotVtas: ' + mapIdCteTotVtas.values());
        
        //Ahora si recorre la lista de clientes que se van a inactivar y ve si ya complieron con el rango de vtas para su periodo
        for (String sIdCtePaso : setIdCteCorp){
            //Busca al cliente en el mapa de mapIdCteTotVtas
            if (mapIdCteTotVtas.containsKey(sIdCtePaso)){
                Integer intCteVtasTot = mapIdCteTotVtas.get(sIdCtePaso);
                Integer intCteVtasMinRango = mapIdCteTotRango.get(sIdCtePaso);
                System.debug('EN TAM_InactivaCorporateAccountsBch intCteVtasTot: ' + intCteVtasTot + ' intCteVtasMinRango: ' + intCteVtasMinRango);
                //Llego a las ventas totales
                if (intCteVtasTot >= intCteVtasMinRango){
                    System.debug('SI llego a las ventas totales sIdCtePaso: ' + sIdCtePaso);
	                Account objCteRenovaAutom = mapIdCteActivo.get(sIdCtePaso);
	                Date dtFechaFinUpd = objCteRenovaAutom.TAM_FechaVigenciaFin__c.AddDays(30);
	                System.debug('EN TAM_InactivaCorporateAccountsBch objCteRenovaAutom: ' + objCteRenovaAutom + ' dtFechaFinUpd: ' + dtFechaFinUpd);
	                //Crea el objeto con los datos del cliente que se va a mantaner actibo
	                Account objCteVencido = new Account(id = sIdCtePaso, TAM_Renovacion__c = true,
	                    TAM_FechaRenovacion__c = objCteRenovaAutom.TAM_FechaVigenciaFin__c,
	                    TAM_FechaVigenciaFin__c = dtFechaFinUpd);
	                System.debug('EN TAM_InactivaCorporateAccountsBch objCteVencido: ' + objCteVencido);                
	                //Agregalo a la lista de lAccountActivaUpd
	                lAccountActivaUpd.add(objCteVencido);
                }//Fin si intCteVtasTot >= intCteVtasMinRango
                //No llego a las ventas totales
                if (intCteVtasTot < intCteVtasMinRango){
                    System.debug('NO llego a las ventas totales sIdCtePaso: ' + sIdCtePaso);
                    setIdCteDesactivar.add(sIdCtePaso);
                }//Fin si intCteVtasTot < intCteVtasMinRango
            }//Fin si mapIdCteTotVtas.containsKey(sIdCtePaso)
            //No existe en el mapa de mapIdCteTotVtas desactivalo
            if (!mapIdCteTotVtas.containsKey(sIdCtePaso))
                setIdCteDesactivar.add(sIdCtePaso);
        }//Fin del for para mapIdCteTotRango.keySet()

        //Recorre la lista de reg que se van a desactivar setIdCteDesactivar
        for (String sCteDesac : setIdCteDesactivar){
            Account objCteVencido = mapIdCteActivo.get(sCteDesac);
            System.debug('EN TAM_InactivaCorporateAccountsBch objCteVencido: ' + objCteVencido);
            String rtVaRtCte = VaRtCtePM;
            if (objCteVencido.RecordType.Name == 'Cliente Corporativo' || objCteVencido.RecordType.Name == 'Cliente - Persona Moral' || objCteVencido.TAM_TIPOPERSONAPROSPECTO__C == 'Persona moral')
                rtVaRtCte = VaRtCtePM;
            if (objCteVencido.RecordType.Name == 'Cliente Corporativo CP' || objCteVencido.RecordType.Name == 'Cliente - Persona Fisica' || objCteVencido.TAM_TIPOPERSONAPROSPECTO__C == 'Person física')
                rtVaRtCte = VaRtCtePF;
            System.debug('EN TAM_InactivaCorporateAccountsBch rtVaRtCte: ' + rtVaRtCte);             
            //Crea el objeto del tipo Account y metelo a la lista de lAccountInactUpd
            lAccountInactUpd.add(new Account(
                    id = objCteVencido.id,
                    TAM_EstatusCliente__c = 'Vencido',
                    RecordTypeId = rtVaRtCte 
                )
            );
        }//fin del for para setIdCteDesactivar
        System.debug('EN TAM_InactivaCorporateAccountsBch setIdCteDesactivar: ' + setIdCteDesactivar);
        System.debug('EN TAM_InactivaCorporateAccountsBch lAccountActivaUpd: ' + lAccountActivaUpd);
        System.debug('EN TAM_InactivaCorporateAccountsBch lAccountInactUpd: ' + lAccountInactUpd);

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lAccountActivaUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Saveresult> lDtbUpsRes = Database.update(lAccountActivaUpd, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_InactivaCorporateAccountsBch Hubo un error a la hora de activar los clientes corporativos ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                if (objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_InactivaCorporateAccountsBch Los datos del clente corporativo se Actualizaron con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lAccountInactUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Saveresult> lDtbUpsRes = Database.update(lAccountInactUpd, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_InactivaCorporateAccountsBch Hubo un error a la hora de desactivar al cliente corporativo ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_InactivaCorporateAccountsBch Los datos del cliente corporativo se Actualizaron con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
				
		//Para pruebas		
		//Database.rollback(spRollbck);
						
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_InactivaCorporateAccountsBch.finish Hora: ' + DateTime.now());         
    } 
    
}