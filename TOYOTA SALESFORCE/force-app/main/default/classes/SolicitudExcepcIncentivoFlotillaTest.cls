/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de las solicitudes de excepciónes VC	
******************************************************************************* */

@isTest
public class SolicitudExcepcIncentivoFlotillaTest {
    
    @testSetup static void setup() {
        
        //Se declara un fecha 
        Date fecha = date.today().addDays(-198); 
        
        //Se inserta el documento adjunto
        document doc = new Document();
        doc.Body = Blob.valueOf('Some Text');
        doc.ContentType = 'application/pdf';
        doc.DeveloperName = 'mydocument';
        doc.IsPublic = true;
        doc.FolderId = UserInfo.getUserId();
        doc.Name = 'My Document';
        insert doc;
        
        //RangoPrograma
        Rangos__c rangoPrograma = new Rangos__c();
        rangoPrograma.name  = 'UBER';
        rangoPrograma.Activo__c = true;
        rangoPrograma.NumUnidadesMinimas__c = 5;
        rangoPrograma.NumUnidadesMaximas__c = 10;
        insert rangoPrograma;
        
        //Politica de Incentivos   
        TAM_PoliticasIncentivos__c politicaRetail = new TAM_PoliticasIncentivos__c();
        politicaRetail.Name = 'Politica Test';
        politicaRetail.TAM_InicioVigencia__c = Date.today()-1;
        politicaRetail.TAM_FinVigencia__c = Date.today() + 1;
        politicaRetail.TAM_PeriodoDeGracia__c = 5;
        politicaRetail.TAM_EstatusIncentivos__c = 'Vigente';
        politicaRetail.TAM_CargaHistorica__c = true;
        politicaRetail.RecordTypeId = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
        insert  politicaRetail;
        
        //Detalle de la politica 1
        TAM_DetallePoliticaJunction__c detallePolitica = new TAM_DetallePoliticaJunction__c();
        detallePolitica.TAM_AnioModelo__c = '2020';
        detallePolitica.TAM_BonoLealtad__c = 5000;
        detallePolitica.TAM_BonoLealtadDealer__c = 2500;
        detallePolitica.TAM_BonoLealtadTMEX__c = 2500;
        detallePolitica.TAM_Descripcion__c = 'Test';
        detallePolitica.Name = 'Detalle de prueba';
        detallePolitica.TAM_IncPropuesto_Cash__c = 10000;
        detallePolitica.TAM_IncDealer_Cash__c = 5000;
        detallePolitica.TAM_IncentivoTMEX_Cash__c = 5000;
        detallePolitica.TAM_Lealtad__c  = true;
        detallePolitica.TAM_PoliticaIncentivos__c = politicaRetail.id;
        detallePolitica.TAM_Serie__c = 'Corolla';
        detallePolitica.TAM_Clave__c = '1780';
        detallePolitica.TAM_Pago_TFS__c = true;
        detallePolitica.TAM_Rango__c = rangoPrograma.id;
        detallePolitica.TAM_MSRP__c = 350000;
        insert detallePolitica;
        
        //Detalle de la politica 2
        TAM_DetallePoliticaJunction__c detallePoliticaDos = new TAM_DetallePoliticaJunction__c();
        detallePoliticaDos.TAM_AnioModelo__c = '2020';
        detallePoliticaDos.TAM_BonoLealtad__c = 5000;
        detallePoliticaDos.TAM_BonoLealtadDealer__c = 2500;
        detallePoliticaDos.TAM_BonoLealtadTMEX__c = 2500;
        detallePoliticaDos.TAM_Descripcion__c = 'Test';
        detallePoliticaDos.Name = 'Detalle de prueba';
        detallePoliticaDos.TAM_IncPropuesto_Cash__c = 10000;
        detallePoliticaDos.TAM_IncDealer_Cash__c = 5000;
        detallePoliticaDos.TAM_IncentivoTMEX_Cash__c = 5000;
        detallePoliticaDos.TAM_Lealtad__c  = true;
        detallePoliticaDos.TAM_PoliticaIncentivos__c = politicaRetail.id;
        detallePoliticaDos.TAM_Serie__c = 'Corolla';
        detallePoliticaDos.TAM_Clave__c = '1780';
        detallePoliticaDos.TAM_Pago_TFS__c = true;
        detallePoliticaDos.TAM_Rango__c = rangoPrograma.id;
        detallePoliticaDos.TAM_MSRP__c = 350000;
        detallePoliticaDos.TAM_IncPropuesto_Porcentaje__c = 20;
        insert detallePoliticaDos;
        
        //Productos
        Product2 productVIN = new Product2();
        productVIN.Name='2205';
        productVIN.Anio__c = '2020';
        productVIN.IdExternoProducto__c='22052020';
        productVIN.ProductCode='22052020';
        productVIN.CantidadMaxima__c = 5;
        productVIN.IsActive = true;
        insert productVIN;
        
        //Lista de precios
        Pricebook2 listaPrecio = new Pricebook2();
        listaPrecio.IsActive = true;
        listaPrecio.Name = 'VH-MARZO-2020';
        listaPrecio.IdExternoListaPrecios__c = 'VH-MARZO-2020';
        listaPrecio.InicioVigencia__c = fecha;
        listaPrecio.FinVigencia__c = fecha.addDays(30);
        system.debug('Lista precios inicio'+listaPrecio.InicioVigencia__c);
        system.debug('Lista precios fin'+listaPrecio.FinVigencia__c);
        insert listaPrecio;
        
        //Precio 
        Id standardPricebookId = Test.getStandardPricebookId();
        PricebookEntry precioStandard = new PricebookEntry();
        precioStandard.Pricebook2Id= standardPricebookId;
        precioStandard.Product2Id= productVIN.Id;
        precioStandard.UnitPrice= 0.0;
        precioStandard.IsActive = true;
        precioStandard.PrecioSinIva__c = 211176.00;
        precioStandard.PrecioDealer__c = 67829;
        precioStandard.PrecioPublico__c = 134324;
        precioStandard.IsActive = true;
        insert precioStandard;
        
        //Entrada de precio
        PricebookEntry precioEntrada = new PricebookEntry();
        precioEntrada.Pricebook2Id= listaPrecio.id;
        precioEntrada.Product2Id= productVIN.Id;
        precioEntrada.UnitPrice= 0.0;
        precioEntrada.IsActive = true;
        precioEntrada.PrecioSinIva__c = 211176.00;
        precioEntrada.PrecioDealer__c = 67829;
        precioEntrada.PrecioPublico__c = 134324;
        precioEntrada.IsActive = true;
        insert precioEntrada;
        
        //Cuenta de tipo distribuidor
        Account acct = new Account();
        acct.name = 'DEALER DE PRUEBA';
        acct.Codigo_Distribuidor__c = '570002';
        acct.recordtypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();
        insert acct;
        
        
    }
    
    
    @isTest static void testMethod1(){
        String fechaVenta = '2020-05-02 19:00:00';
        TAM_DetallePoliticaJunction__c politica = [Select id from TAM_DetallePoliticaJunction__c where name = 'Detalle de prueba' limit 1];
        
        TAM_DetallePoliticaJunction__c politicasFechaCierre = [Select id from TAM_DetallePoliticaJunction__c limit 1];
        politicasFechaCierre = SolicitudExcepcionIncentivosFlotilla.getPoliticaFechaCierre('1780','2020','UBER','TFS',fechaVenta);
        system.assertEquals(true,politicasFechaCierre != null);
        
        List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio> listaWrapper = new List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio>();        
        listaWrapper = SolicitudExcepcionIncentivosFlotilla.getPoliticasHistoricas('1780','2020','UBER','TDS',fechaVenta,'ENERO');
        system.assertEquals(true,!listaWrapper.isEmpty());
        
        List<PricebookEntry> listaPrecios = new List<PricebookEntry>();
        listaPrecios = SolicitudExcepcionIncentivosFlotilla.getPreciosVIN('1780','2020');
		system.assertEquals(false,!listaPrecios.isEmpty());

        List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio> listaWrapperPorcentaje = new List<SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio>();
        listaWrapperPorcentaje = SolicitudExcepcionIncentivosFlotilla.getPoliticasVigentesPorcentaje('1780','2020','UBER','TFS');
        system.assertEquals(true,!listaWrapperPorcentaje.isEmpty());
        
        List<String> listaMotivos = new List<String>();
        listaMotivos = SolicitudExcepcionIncentivosFlotilla.obtieneMotivosSolicitud();
        system.assertEquals(true,!listaMotivos.isEmpty());
        
        SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio calculoManual = new SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio();
        calculoManual = SolicitudExcepcionIncentivosFlotilla.getCalculoManual(350000,10,10);
        system.assertEquals(true,calculoManual != null);
        
        TAM_WrpSolicitudFlotillaProgramaModelos wrapSolicitud = new TAM_WrpSolicitudFlotillaProgramaModelos();
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion itemVIN = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion();
        itemVIN.strVin = 'QWERTY45678';
        List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion> lVinesSeleccionados = new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion>();
        lVinesSeleccionados.adD(itemVIN);
        wrapSolicitud.lVinesSeleccionados = lVinesSeleccionados;
        wrapSolicitud.strFolioSolicitd = '1238209101AQ';
        wrapSolicitud.strNombreCliente = 'Hector Figueroa';
        wrapSolicitud.strRangoPrograma = 'UBER';
        wrapSolicitud.strFechaVigencia   = '2020-05-02';
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados modeloSelect = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados();
        modeloSelect.strIdSolicitud = '00120101';
        modeloSelect.strIdExterno = '2345678JAJSHY';
        modeloSelect.strNombre = 'Avanza-2205-2020-X12-10';
        modeloSelect.strVersion = 'LE';
        modeloSelect.strDescripcionColorExterior = '0101';
        modeloSelect.strDescripcionColorInterior  = 'E427';
        modeloSelect.strCantidadSolicitada  = '5';
        modeloSelect.strIdCatCentrModelos = 'A001';
        modeloSelect.bolMismoDistribuidor = true;
        modeloSelect.bolCreaExcepcion = true;
        list<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados> listmodeloSelect =  new List<TAM_WrpSolicitudFlotillaProgramaModelos.wrpDatosModeloSelecionados>();
        listmodeloSelect.adD(modeloSelect);
        wrapSolicitud.lModelosSeleccionados = listmodeloSelect;
        List<String> listaVINES = new  List<String> ();
        listaVINES.add('QWERTYUIO4567890:57011');
        listaVINES.add('QWERTYUIO4567891:57011');
        String listaVinesST = listaVINES.toString();
        String fechaVentaVIN = '2021-03-30 19:00:00';
        
        Document doc = [Select id from Document where DeveloperName = 'mydocument'];
        
        SolicitudExcepcionIncentivosFlotilla.guardaSolicitudExcepcion(wrapSolicitud,'Otro motivo.','comentario',370000,true,politica.id,10,10,10,politica,'570002',listaVinesST,fechaVentaVIN,true,doc.id);
        TAM_SolicitudExpecionIncentivo__c solicitudIncentivo1 = [Select id,TAM_AprobacionFinanzas__c FROM TAM_SolicitudExpecionIncentivo__c limit 1];
        system.assertEquals('Aprobado',solicitudIncentivo1.TAM_AprobacionFinanzas__c);
        
        
        SolicitudExcepcionIncentivosFlotilla.guardaSolicitudExcepcion(wrapSolicitud,'Otro motivo.','comentario',370000,false,politica.id,10,10,10,politica,'570002',listaVinesST,fechaVentaVIN,false,doc.id);
        TAM_SolicitudExpecionIncentivo__c solicitudIncentivo2 = [Select id,TAM_AprobacionFinanzas__c FROM TAM_SolicitudExpecionIncentivo__c limit 1];
        system.assertEquals('Aprobado',solicitudIncentivo2.TAM_AprobacionFinanzas__c);
        
        TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion test = new TAM_WrpSolicitudFlotillaProgramaModelos.wrpVinesExcepcion ('test');
        TAM_WrpSolicitudFlotillaProgramaModelos constructorVIN = new  TAM_WrpSolicitudFlotillaProgramaModelos('Carlos','Test','A001','2020-05-02','Carlos','TFS','UBER','Roberto','2020-05-02',null);
        
        SolicitudExcepcionIncentivosFlotilla.deleteDocument(doc.Id);
        List<ContentDocument> documentRecord = new List<ContentDocument>();
        documentRecord = [Select id from ContentDocument WHERE id =: doc.Id];
        system.assertEquals(true,documentRecord.isEmpty());
        
    }
    
    
    @isTest static void testMethod2(){
        TAM_DetallePoliticaJunction__c politica = [Select id from TAM_DetallePoliticaJunction__c where name = 'Detalle de prueba' limit 1];
        
        SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio calculoTotal = new SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio();
        calculoTotal = SolicitudExcepcionIncentivosFlotilla.calculoTotal(politica.id, 380000);
        system.assertEquals(true, calculoTotal != null);
        
        SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio calculoResumenPrecio = new SolicitudExcepcionIncentivosFlotilla.wrapResumenPrecio();
        calculoResumenPrecio = SolicitudExcepcionIncentivosFlotilla.calculoPoliticaSeleccionada(politica.id);
        system.assertEquals(true, calculoResumenPrecio != null);
        
    }
    
}