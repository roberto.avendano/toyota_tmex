/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS TAM_CallRestWebService

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    30-Agosto-2020	    Héctor Figueroa            	Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CallRestWebServiceTest {

    static testMethod void TAM_CallRestWebServiceOK() {

        String sUrl = 'http://www.it-group.com.mx:9093/api/login/authenticate';
        String sMethod = 'POST';
        String sBody = '';
        Map<String, String> mapParameters = new Map<String, String>();
        mapParameters.put('Content-Type', 'application/json');

   		//Crea un objeto del tipo TAM_CallRestWebService.datosWebService   		
   		TAM_CallRestWebService.datosWebService objdatosWebService = 
   			new TAM_CallRestWebService.datosWebService();
   		objdatosWebService.sUrl = '';
   		objdatosWebService.sMethod = '';
   		objdatosWebService.sBody = '';    	
		objdatosWebService.mapParameters = new Map<String, String>();			
   		TAM_CallRestWebService.datosWebService objdatosWebService2 = 
   			new TAM_CallRestWebService.datosWebService(sUrl, sMethod, sBody, mapParameters);

   		//Crea un objeto del tipo TAM_CallRestWebService.datosWebService   		
   		TAM_CallRestWebService.loginResponse objloginResponse = 
   			new TAM_CallRestWebService.loginResponse();
   		objloginResponse.UnniqId = '';
   		objloginResponse.DealarID = '';
   		objloginResponse.Dealar = '';    	
   		objloginResponse.Token = '';
   		objloginResponse.strDate = '';
   		TAM_CallRestWebService.loginResponse objloginResponse2 = 
   			new TAM_CallRestWebService.loginResponse('UnniqId', 'DealarID', 'Dealar', 'Token', 'strDate');

		//Llama la clase de TAM_CallRestWebService y metodo login
        TAM_CallRestWebService.login(objdatosWebService);
		//Llama la clase de TAM_CallRestWebService y metodo login
        TAM_CallRestWebService.call(objdatosWebService);
        
        TAM_RespCallWebService objRespCallWebService =
        	new TAM_RespCallWebService('NombreMetodo', false, 'Detalle', 'JsonRes',	objloginResponse);
        
    }
}