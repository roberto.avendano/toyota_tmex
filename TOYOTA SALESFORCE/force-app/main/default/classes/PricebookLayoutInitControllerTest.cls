@isTest
private class PricebookLayoutInitControllerTest {
	
	public static Pricebook2 getPricebook(){
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook',
            IdExternoListaPrecios__c='CustomPricebook', 
            isActive=true
        );

        insert customPB;
     	return customPB;
	}	

	
	@isTest static void test_method() {
		Pricebook2 customPB = PricebookLayoutInitControllerTest.getPricebook();
		ApexPages.StandardController stdController = new ApexPages.StandardController(customPB);
		PricebookLayoutInitController extController = new PricebookLayoutInitController(stdController);
		extController.redirectToHome();
	}
	
}