public class TAM_SolicitudInternaTriggerHand  extends TriggerHandler {
    
    private List<SolicitudInternaVehiculos__c> itemsNew;    
    
    public TAM_SolicitudInternaTriggerHand(){
        this.itemsNew = (List<SolicitudInternaVehiculos__c>) Trigger.new;      
        
    }
    
    public override void afterUpdate(){
        updSharingRules(itemsNew);
        
    } 
    
    public static void updSharingRules (List<SolicitudInternaVehiculos__c> itemsNew){
        Id recordTypeDistribuidor =  Schema.SObjectType.SolicitudInternaVehiculos__c.getRecordTypeInfosByDeveloperName().get('PoolEntrega').getRecordTypeId();
        List<SolicitudInternaVehiculos__c> solicitudUpd = new List<SolicitudInternaVehiculos__c>();
        Set<String> dealersSolicitud = new Set<String>();
        Map<SolicitudInternaVehiculos__c,String> mapSolicitudDealer = new Map<SolicitudInternaVehiculos__c,String>();
        
        for(SolicitudInternaVehiculos__c SolIn : itemsNew){
            if(SolIn.RecordTypeId == recordTypeDistribuidor){
                solicitudUpd.add(SolIn);
                dealersSolicitud.add(SolIn.DistribuidoraEntrega__c);
                mapSolicitudDealer.put(SolIn,SolIn.DistribuidoraEntrega__c);
                
            }  
        }
        
        // Se busca los grupos en base al nombre del dealer entrega de la solicitud
        List<Group> grupoDealeres = new List<Group>();        
        Map<String,Id> mapByDealers = new Map<String,Id>();
        
        if(!dealersSolicitud.isEmpty()){
            grupoDealeres = [Select id,name From Group WHERE name IN : dealersSolicitud];
        }
        
        
        if(!grupoDealeres.isEmpty()){
            for(Group grp : grupoDealeres){
                mapByDealers.put(grp.name,grp.id);
                
            }
            
        }
        
        
        //For en solicitud interna de vehiculos para asociar los Grupos
        List<SolicitudInternaVehiculos__Share > colaboracionSolicitudInterna = new List<SolicitudInternaVehiculos__Share >();
        if(!mapByDealers.isEmpty()){
            for(SolicitudInternaVehiculos__c SolInUpd : itemsNew){
                system.debug('Map dealer'+SolInUpd.DistribuidoraEntrega__c.toUpperCase());
                if(mapByDealers.containsKey(SolInUpd.DistribuidoraEntrega__c.toUpperCase())){
                    Id idGrupoDealer = mapByDealers.get(SolInUpd.DistribuidoraEntrega__c.toUpperCase());
                    SolicitudInternaVehiculos__Share shareRecord = new SolicitudInternaVehiculos__Share();
                    shareRecord.UserOrGroupId = idGrupoDealer;
                    shareRecord.ParentId = SolInUpd.id;
                    shareRecord.AccessLevel = 'Edit';
                    shareRecord.RowCause = 'Manual';
                    colaboracionSolicitudInterna.add(shareRecord);
                }
            }
        }
        
        system.debug('colaboracion registro:::'+colaboracionSolicitudInterna);
        
        if(!colaboracionSolicitudInterna.isEmpty()){
            Database.SaveResult[] jobShareInsertResult = Database.insert(colaboracionSolicitudInterna,false);
            
        }
        
    }
    
}