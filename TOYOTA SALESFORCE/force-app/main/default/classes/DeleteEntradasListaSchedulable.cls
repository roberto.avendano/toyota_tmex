global class DeleteEntradasListaSchedulable implements Schedulable {
    global void execute(SchedulableContext sc) {        
        String query = 'SELECT Id, Name, Parte__c FROM EntradaListaPrecios__c';
        DeleteEntradasListaBatch b = new DeleteEntradasListaBatch(query);
        database.executebatch(b);
    }
}