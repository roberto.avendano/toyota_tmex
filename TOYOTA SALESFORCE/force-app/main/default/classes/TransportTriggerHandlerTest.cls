@isTest
private class TransportTriggerHandlerTest{
	@isTest
	static void itShould(){
		Transport__c t01 = new Transport__c(
			Name = 'T01',
			ETA__c = Date.today()
		);
		insert t01;

		t01.TimeArrivalText__c = '0';
		update t01;

		t01.TimeArrivalText__c = '0:0:0';
		update t01;

		Transport__c t02 = new Transport__c(
			Name = 'T02',
			ETA__c = Date.today(),
			TimeArrivalText__c = '12:00:00'
		);
		insert t02;
	}
}