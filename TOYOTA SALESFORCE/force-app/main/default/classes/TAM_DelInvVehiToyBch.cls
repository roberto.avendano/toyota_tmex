/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_DelInvVehiToySch y
    					eliminar los registros del objeto TAM_InventarioVehiculosToyota__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_DelInvVehiToyBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_DelInvVehiToyBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_DelInvVehiToyBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_DelInvVehiToyBch.');
		List<TAM_InventarioVehiculosToyota__c> lOppDel = new List<TAM_InventarioVehiculosToyota__c>();

		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_InventarioVehiculosToyota__c objInvVehiToy : scope){
        	//Crea el objeto del tipo TAM_InventarioVehiculosToyota__c
        	TAM_InventarioVehiculosToyota__c OppPaso = new TAM_InventarioVehiculosToyota__c(id = objInvVehiToy.id); 
        	lOppDel.add(OppPaso);
        }

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lOppDel.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Deleteresult> lDtbUpsRes = Database.delete(lOppDel);
			//Ve si hubo error
			for (Database.Deleteresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_DelInvVehiToyBch Hubo un error a la hora de crear/Actualizar los registros en Opp ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_DelInvVehiToyBch Los datos de la Opp se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_DelInvVehiToyBch.finish Hora: ' + DateTime.now());     
        Integer intMergeShareAccountsHora = Integer.valueOf(System.Label.TAM_MergeDelInvVehiHora);
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_MergeDelInvVehiMinutos);
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraManana = dtHoraActual.addDays(1);
		String CRON_EXP = dtHoraManana.second() + ' ' + intMergeShareAccountsMinutos + ' ' + intMergeShareAccountsHora + ' ' + dtHoraManana.day() + ' ' + dtHoraManana.month() + ' ? ' + dtHoraManana.year();
    	System.debug('EN TAM_DelInvVehiToyBch.finish CRON_EXP: ' + CRON_EXP);

		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
		TAM_DelInvVehiToySch objDelInvVehiToySch = new TAM_DelInvVehiToySch();
    	System.debug('EN TAM_DelInvVehiToyBch.finish objDelInvVehiToySch: ' + objDelInvVehiToySch);
		//Programa el proceso desde System
		if (!Test.isRunningTest())
			System.schedule('TAM_DelInvVehiToySch: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objDelInvVehiToySch);         
    } 
        
}