/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros de los Vehiculos
                        que se crean o actualizan.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    12-Agosto-2021    Héctor Figueroa             Modificación
******************************************************************************* */

public without sharing class TAM_VehiculoHandler extends TriggerHandler{

    private List<Vehiculo__c> lstVehiculos; 
    private Map<Id,Vehiculo__c> mapVentas;
    private Map<String,Map<String,RecordType>> recordTypesMap;
    
    public TAM_VehiculoHandler() {
        this.lstVehiculos = Trigger.new;
        this.mapVentas = (Map<Id,Vehiculo__c>)Trigger.newMap;
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
    }
    
    public override void afterInsert(){
        cancelaVentaEspacial(lstVehiculos);
    }
    public override void afterUpdate(){
        cancelaVentaEspacial(lstVehiculos);
    }

    //Metodo para cancelar las ventas espaciales que cambiaron de tipo de venta y de cliente
    private void cancelaVentaEspacial(List<Vehiculo__c> lstVehiculos){
        System.debug('EM cancelaVentaEspacial...');     
        
        Date dFechaCancelaFinal = Date.today();
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();        
        Map<String, Vehiculo__c> vines = new Map<String, Vehiculo__c>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapVinCheckout = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_LeadInventarios__c> mapLeadInvUpd = new Map<String, TAM_LeadInventarios__c>();
        Map<String, List<Movimiento__c>> mapVinLstMov = new Map<String, List<Movimiento__c>>();
        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapIdCheckoutObjUps = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
        for(Vehiculo__c v: lstVehiculos){
            if (v.Name != null)
                vines.put(v.Name, v);
        }//Fin del for para los vines
        System.debug('EM cancelaVentaEspacial vines: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial vines: ' + vines.values());     

        String sLVineas = '(';
        //Recorre la lista de lClientes
        for (String sIdVin : vines.keySet()){
            sLVineas += '\'' + sIdVin + '\',';
        }
        sLVineas = sLVineas.substring(0, sLVineas.lastIndexOf(','));
        sLVineas += ')';

        //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
        String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
            ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, ' +  
            ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
            ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
            ' And VIN__r.Name IN ' + sLVineas +
            ' Order by Submitted_Date__c DESC';
        //Ees una prueba
        if (Test.isRunningTest())    
            queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
            ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c, ' +  
            ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
            ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
            ' Order by Submitted_Date__c DESC LIMIT 1';
        System.debug('EN TAM_ActCancelSolInvBch_cls queryMovimiento: ' + queryMovimiento);

        //Busca los movimientos    
        List<sObject> lstObjLstMov = new List<sObject>();
        if (!vines.isEmpty()){
            lstObjLstMov = Database.query(queryMovimiento);
            for (sObject objsObjectPaso : lstObjLstMov){
                Movimiento__c objMovPaso = (Movimiento__c) objsObjectPaso;
	            //Mete los mov en el mapa de mapVinLstMov
	            if (mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
	                mapVinLstMov.get(objMovPaso.VIN__r.Name).add(objMovPaso);            
	            if (!mapVinLstMov.containsKey(objMovPaso.VIN__r.Name))
	                mapVinLstMov.put(objMovPaso.VIN__r.Name, new List<Movimiento__c>{objMovPaso});
            }//Fin del for para lstObjLstMov
        }//Fin si !vines.isEmpty        
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.keySet());     
        System.debug('EM cancelaVentaEspacial mapVinLstMov: ' + vines.values());     

        //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
        String queryChecOut = 'Select t.id, t.TAM_VIN__c,' +  
            ' t.TAM_SolicitudFlotillaPrograma__c, t.TAM_NombreDistribuidor__c, t.TAM_TipoVenta__c' +  
            ' From TAM_CheckOutDetalleSolicitudCompra__c t' +
            ' Where TAM_EstatusDealerSolicitud__c = \'Cerrada\' And TAM_TipoVenta__c = \'Pedido Especial\'' + 
            ' And TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ' + 
            ' And TAM_VIN__c IN ' + sLVineas +
            ' Order by TAM_VIN__c ASC';
        //Ees una prueba
        if (Test.isRunningTest())    
            queryChecOut = 'Select t.id, t.TAM_VIN__c, ' +  
            ' t.TAM_SolicitudFlotillaPrograma__c, t.TAM_NombreDistribuidor__c, t.TAM_TipoVenta__c' +  
            ' From TAM_CheckOutDetalleSolicitudCompra__c t' +
            ' Where TAM_EstatusDealerSolicitud__c = \'Cerrada\' ' + 
            ' Order by TAM_VIN__c ASC LIMIT 1';
        System.debug('EN TAM_ActCancelSolInvBch_cls queryChecOut: ' + queryChecOut);

        //Busca los movimientos    
        List<sObject> lstObjChecOut = new List<sObject>();
        if (!vines.isEmpty()){
            lstObjChecOut = Database.query(queryChecOut);
            for (sObject objsObjectPaso : lstObjChecOut){
                TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = (TAM_CheckOutDetalleSolicitudCompra__c) objsObjectPaso;
                //Metelo al mapa de mapVinCheckout
                if ((objCheckOutPaso.TAM_VIN__c != null) || Test.isRunningTest())                
                    mapVinCheckout.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso);                
            }//Fin del for para lstObjChecOut
        }//Fin si !vines.isEmpty
                
        //Reorre la lista de vines y obten su ultimo mov
        for (String sVinPaso : vines.keySet()){
            //Busca el vin en mapVinLstMov
            if (mapVinLstMov.containsKey(sVinPaso)){
                Movimiento__c objMvPaso = new Movimiento__c();
                List<Movimiento__c> lstMov = mapVinLstMov.get(sVinPaso);
                System.debug('EM cancelaVentaEspacial lstMov: ' + lstMov);     
                //Busca su ultimo mov en TAM_ActTotVtaInvBch_cls.getUltMov();
                if (lstMov.size() > 1)
                    objMvPaso =  TAM_ActTotVtaInvBch_cls.getUltMov(lstMov);
                if (lstMov.size() == 1)
                    objMvPaso =  lstMov.get(0);
                //Ve si el VIN esta en el para de mapVinCheckout asociado a una solicutude de pedido especial
                if (mapVinCheckout.containsKey(sVinPaso)){
                    TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso = mapVinCheckout.get(sVinPaso);
                    System.debug('EM cancelaVentaEspacial objMvPaso: ' + objMvPaso);
                    System.debug('EM cancelaVentaEspacial objCheckOutPaso: ' + objCheckOutPaso);                                                  
                    //Ve si el ultimo mov es un pedido especial y cambio a INVENTARIO y ademas esta rechazado
                    if ( (objMvPaso.Fleet__c == 'N' && objMvPaso.Trans_Type__c =='RDR' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c)
                        || Test.isRunningTest()){
                        //Agregalo al mapa de mapIdCheckoutObjUps 
                        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPasoUpd = new TAM_CheckOutDetalleSolicitudCompra__c(
                                Id = objCheckOutPaso.id, TAM_ListaParaCancelar__c = true,
                                TAM_EstatusDealerSolicitud__c = 'Cancelada',
                                TAM_FechaCancelacion__c = dFechaCancelaFinal,
                                TAM_VIN__c = objCheckOutPaso.TAM_VIN__c
                        );
                        System.debug('EM cancelaVentaEspacial objCheckOutPasoUpd: ' + objCheckOutPasoUpd);
                        //Agregar el objeto al mapa de mapIdCheckoutObjUps
                        mapIdCheckoutObjUps.put(objCheckOutPaso.id, objCheckOutPasoUpd);
                        //Crea el registro en el log de errores                
                        lError.add(new TAM_LogsErrores__c(TAM_idExtReg__c= objCheckOutPaso.id + '-' + objCheckOutPaso.TAM_VIN__c, TAM_VIN__c = objCheckOutPaso.TAM_VIN__c, TAM_Proceso__c = 'Cancelada CheckOut Pedio Especial Cambio Fleet', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'ObjCheckOut' + objCheckOutPaso) );            
                    }//Fin si  objMvPaso.Fleet__c == 'N' && objCheckOutPaso.TAM_NombreDistribuidor__c != objMvPaso.TAM_CodigoDistribuidorFrm__c
                }//Fin si mapVinCheckout.containsKey(sVinPaso)
            }//Fin si mapVinLstMov.containsKey(sVinPaso)
        }//Fin del for para vines.keySet()
        System.debug('EN cancelaVentaEspacial mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.keyset());
        System.debug('EN cancelaVentaEspacial mapIdCheckoutObjUps: ' + mapIdCheckoutObjUps.values());
        System.debug('EN cancelaVentaEspacial lError: ' + lError);
    
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        //Ve si tiene algo la lista de mapCheckOutDDUps
        if (!mapIdCheckoutObjUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdCheckoutObjUps.values(), TAM_CheckOutDetalleSolicitudCompra__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN VehiculoSIVTriggerHandler Hubo un error a la hora de crear/Actualizar los registros en Checkout ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapCheckOutDDUps.isEmpty()

        //Crea los reg en el objeto de Log
        if (!lError.isEmpty())
            List<Database.Upsertresult> lCheckoutDtbUpsRes = Database.upsert(lError, TAM_LogsErrores__c.TAM_idExtReg__c, false);

        //Roleback a todo
        //Database.rollback(sp);
        
    }
    
}