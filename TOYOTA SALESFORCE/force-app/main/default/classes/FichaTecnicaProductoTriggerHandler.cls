public class FichaTecnicaProductoTriggerHandler extends TriggerHandler {
    
    private List<FichaTecnicaProducto__c> itemsNew;
    
    public FichaTecnicaProductoTriggerHandler(){ 
        this.itemsNew = (List<FichaTecnicaProducto__c>) Trigger.new;
    }
    
    public override void afterUpdate() {
        crearColores(itemsNew);
    }

    private void crearColores(List<FichaTecnicaProducto__c> fichas){
        Set<Id> setFichasActivas = new Set<Id>();
        Set<Id> setVersiones = new Set<Id>();
        Set<String> productoName = new Set<String>();
        Set<Id> setColoresVersiones = new Set<Id>();
        List<ColorInternoModelo__c> newListCim = new List<ColorInternoModelo__c>();
        List<ColorExternoModelo__c> newListCem = new List<ColorExternoModelo__c>();
        List<VehiculoSIV__c> vehiculoSIV  = new List<VehiculoSIV__c>();
        Map<String,ColorInternoModelo__c> mapCim = new Map<String,ColorInternoModelo__c>();
        Map<Id,Producto_Version__c> mapModelo = new Map<Id,Producto_Version__c>();
        Map<String,VehiculoSIV__c> mapVinSIV = new Map<String,VehiculoSIV__c>();
        Map<String,ColorExternoModelo__c> mapColExt = new Map<String,ColorExternoModelo__c>();
        
        for(FichaTecnicaProducto__c ficha :fichas){
            if(ficha.Estatus__c=='Preliminar' || ficha.Estatus__c == 'Final'){
                setFichasActivas.add(ficha.Id);
            }
        }
        
        List<FichaColores__c> fichasColores = [SELECT Name, Id, FichaTcnicaProducto__c,FichaTcnicaProducto__r.Anio__c, Activo__c, 
                                               (SELECT Id, Name, Ficha_Colores__c, Orden__c FROM Versiones__r) 
                                               FROM FichaColores__c WHERE FichaTcnicaProducto__c IN :setFichasActivas
                                               AND Activo__c=true];
        System.debug('fichasColores:' +JSON.serialize(fichasColores));
        
        for(FichaColores__c fc :fichasColores){
            for(Versiones_Ficha_Colores__c version : fc.versiones__r){
                setVersiones.add(version.Id);
                productoName.add(version.name);
            }
        }
        
        List<Versiones_Ficha_Colores__c> versiones = [SELECT Name, Id, Ficha_Colores__c, AcabadoInterior__c, Ficha_Colores__r.FichaTcnicaProducto__r.Anio__c,
                                                      (SELECT Id, Name, Orden__c,Producto__r.NombreVersion__c,Producto__r.Anio__c, Producto__c,Producto__r.Name,Producto__r.IdExternoProducto__c, Version__c 
                                                       FROM Productos_de_version__r), 
                                                      (SELECT Id, Name, ColorInterior__c, ColorInterior__r.Name, Version__c, ColorInteriorCod__c 
                                                       FROM Colores_version__r) FROM Versiones_Ficha_Colores__c WHERE Id  IN :setVersiones];

        List<String> codigoModeloFicha = new List<String>();
        List<String> anioModeloFicha = new List<String>();
        for(Versiones_Ficha_Colores__c v : versiones){
            for(Color_version__c cv :v.Colores_version__r){
                setColoresVersiones.add(cv.Id);
                for(Producto_Version__c pv :v.Productos_de_version__r){
                    VehiculoSIV__c vinSIV = new VehiculoSIV__c();
                    vinSIV.name = pv.Producto__r.NombreVersion__c;
                    vinSIV.InicioVigencia__c = date.today();
                    vinSIV.Modelo__c  = pv.Producto__c;
                    vinSIV.NombreVehiculo__c = pv.Producto__r.NombreVersion__c;
                    vinSiv.AnoModelo__c = pv.Producto__r.anio__c;
                    vinSiv.PrecioPublico__c = 0.99;
                    vinSiv.PrecioTotalEmpleado__c = 0.99;
                    vinSiv.Id_Externo__c = pv.Producto__r.name + pv.Producto__r.anio__c;
                    mapVinSIV.put(vinSIV.Id_Externo__c,vinSIV);
                    codigoModeloFicha.add(pv.Producto__r.name);
                    anioModeloFicha.add(pv.Producto__r.anio__c);
                    mapModelo.put(pv.Producto__r.Id,pv);
                    ColorInternoModelo__c cim = new ColorInternoModelo__c(
                        Name = cv.ColorInterior__r.Name,
                        codigoProducto__c = pv.Producto__r.Name,
                        Modelo__c = pv.Producto__c, 
                        IDExterno__c = pv.Producto__r.IdExternoProducto__c + cv.ColorInteriorCod__c + v.Ficha_Colores__r.FichaTcnicaProducto__r.Anio__c,
                        CodigoColorInterno__c = cv.ColorInteriorCod__c, 
                        AgnioModelo__c = v.Ficha_Colores__r.FichaTcnicaProducto__r.Anio__c
                        //Disponible__c,
                    );
                    mapCim.put(cim.CodigoColorInterno__c,cim);
                    newListCim.add(cim);
                    System.debug('Nombres verions'+pv.Producto__c);
                }
            } 
        }
        System.debug('newListCim: '+JSON.serialize(newListCim));
        System.debug('mapCim: '+JSON.serialize(mapCim));
        System.debug('mapModelo: ' +JSON.serialize(mapModelo));
        system.debug('setColoresVersiones'+setColoresVersiones);
        System.debug('UPSER CIM');
        system.debug('SIV'+mapVinSIV.values());
        system.debug('modelo de la ficha'+codigoModeloFicha);
        system.debug('anio de la ficha'+anioModeloFicha);
        
        
        try{
            upsert mapVinSIV.values() name;
        }catch(Exception e){
            system.debug('erores'+e.getMessage());
        }
        
        try{ 
            system.debug('colores internos por modelo' + newListCem);
            upsert newListCim IDExterno__c;
        }catch(Exception e){
            system.debug('erores colores internos'+e.getMessage());
        }
        
        try{ 
            //Cambiar estatus de los colores externos
            List<ColorExternoModelo__c> listColorExt = new List<ColorExternoModelo__c>();
            if(!codigoModeloFicha.isEmpty() && !anioModeloFicha.isEmpty()){
                listColorExt = [select id,Disponible__c from ColorExternoModelo__c  where Modelo__c IN : codigoModeloFicha  AND AnioModelo__c IN : anioModeloFicha];
            }
            
            if(!listColorExt.isEmpty()){
                for(ColorExternoModelo__c colExtList : listColorExt){
                    colExtList.Disponible__c = false;
                    
                }
                update listColorExt;
            }
            
            List<Color_version__c> colorVersiones = [SELECT Id, Name,Version__c, ColorInterior__c, ColorInterior__r.Name, ColorInterior__r.CodigoColor__c,
                                                     (SELECT Id, Name, Agregado__c, ColorExterno__c, ColorExterno__r.Name, ColorExterno__r.CodigoColor__c,
                                                      Color_version__c, CodigoColor__c, DPMS__c
                                                      FROM Colores_Externos_Seleccionados__r WHERE Agregado__c=true or DPMS__c = true) FROM Color_version__c WHERE Id IN:setColoresVersiones];
            system.debug('cv'+setColoresVersiones);   
            for(Color_version__c cv :colorVersiones){
                for(Color_Externo_Seleccionado__c ces :cv.Colores_Externos_Seleccionados__r){
                    for(ColorInternoModelo__c colorModelo : newListCim){
                        if(cv.ColorInterior__r.CodigoColor__c == colorModelo.CodigoColorInterno__c){
                            if(mapCim.get(cv.ColorInterior__r.CodigoColor__c).Id != null){
                                ColorExternoModelo__c cem = new ColorExternoModelo__c( 
                                    Name = ces.ColorExterno__r.Name,
                                    Disponible__c = true,
                                    ColorInternoModelo__c = mapCim.get(cv.ColorInterior__r.CodigoColor__c).Id,
                                    Modelo__c = colorModelo.codigoProducto__c,
                                    ID_Externo__c = mapModelo.get(mapCim.get(cv.ColorInterior__r.CodigoColor__c).Modelo__c).Producto__r.IdExternoProducto__c + colorModelo.Modelo__c + ces.ColorExterno__r.CodigoColor__c +cv.ColorInterior__r.CodigoColor__c ,
                                    CodigoColorExterno__c = ces.ColorExterno__r.CodigoColor__c ,
                                    AnioModelo__c = mapCim.get(cv.ColorInterior__r.CodigoColor__c).AgnioModelo__c
                                );
                                
                                if(ces.DPMS__c==true){
                                    cem.Tipo__c = 'DPMS';
                                }else{
                                    cem.Tipo__c = 'Estandar';
                                }   
                                mapColExt.put(cem.ID_Externo__c,cem);
                            }
                        }
                    }
                }
            }
            
            System.debug('newListCem: '+JSON.serialize(newListCem));
            try{
                system.debug('colores externos por modelo' + newListCem);
                upsert mapColExt.values() ID_Externo__c;
            }catch(Exception ex){
                System.debug('ERROR colores externos: ' + ex.getMessage() + ex.getLineNumber());
            }
        }catch(Exception e){
            System.debug('ERROR upsert CIM: '+e.getMessage());
        }
    } 
}