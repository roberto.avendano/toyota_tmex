public without sharing class SolicitudSiteEditarController { 
    
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
    
    public String csId{get;set;}
    public String sivId{get;set;}
    public SolicitudInternaVehiculos__c siv{get;set;}
    public ApexPages.StandardController sivController; 
    private Map<String,Map<String,RecordType>> recordTypesMap;
    public Map<String, VehiculoSIV__c> vehiclesSIVMap;
    private Map<String, ColorExternoModelo__c> externalColors1Map;
    private Map<String, ColorExternoModelo__c> externalColors2Map;
    private String urlRefresh{get;set;}
    public Boolean alertaOKApp {get;set;}
    public boolean showLectura{get;set;}
    public boolean showEditar{get;set;}
    public boolean showBotonApp {get;set;} 
    public boolean showBotonCan{get;set;}
    public boolean showBotonGua{get;set;}
    public boolean showBotonEdi{get;set;}
    public String selectVehicle{get;Set;}
    public String selectDistribuidora{get;Set;}
    public String selectExternalColor1{get;Set;}
    public String selectExternalColor2{get;Set;}
    public String urlImagen {get;Set;}
    public Boolean avisoPrivacidad {get;set;}
    
    public SolicitudSiteEditarController(ApexPages.StandardController stdcontroller){
        this.sivController = stdcontroller;
        this.sivId =  sivController.getId();
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        this.urlRefresh = ApexPages.currentPage().getURL();
        String id = stdController.getId();
        this.siv = [select id,Licencia__c,NoLicencia__c,VIN__c,EstatusDOD__c,NoMotor__c,SeEstaraProduciendoEn__c,FechaEstimadaLlegada__c,FechaRecepcionSolicitudDOD__c,VehiculoSolicitado__r.name,ColorExternoOpcion1__r.name,ColorExternoOpcion2__r.name,name,VehiculoSolicitado__c,Distribuidor_recepcion__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c,Estatus__c, 
                    solicitante__c,Solicitante__r.Name from SolicitudInternaVehiculos__c where id =: id];
        SolicitudInternaVehiculos__c sivEstatus = siv;
        this.selectVehicle = sivEstatus.VehiculoSolicitado__c;
        this.selectDistribuidora = sivEstatus.Distribuidor_recepcion__c;
        this.selectExternalColor1 = sivEstatus.ColorExternoOpcion1__c;
        this.selectExternalColor2 = sivEstatus.ColorExternoOpcion2__c;
        this.vehiclesSIVMap = new Map<String, VehiculoSIV__c>();
        this.externalColors1Map = new  Map<String, ColorExternoModelo__c>();
        this.externalColors2Map = new  Map<String, ColorExternoModelo__c>();
        
        
        
        getContactoSite(sivId);
        getImage(sivId);
        
    }
    
    public String getContactoSite(String sivId){
        System.debug(sivId);
        siv = [SELECT Id,Distribuidor_recepcion__c,VehiculoSolicitado__r.name,ColorExternoOpcion1__r.name,ColorExternoOpcion2__r.name,Solicitante__c,IDSerie__c,Estatus__c,Observaciones__c,name,Solicitante__r.Name,TipoSolicitud__c,Correoelectronico__c,Telefono__c,WorkID__c,Puesto__c,Compania__c,MontoMaximoSolicitud__c
               , VehiculoSolicitado__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c FROM SolicitudInternaVehiculos__c WHERE Id=:sivId];
        System.debug(siv);
        
        Id rtContactoS = recordTypesMap.get('ContactoSite__c').get('UsuarioSite').Id;
        ContactoSite__c cs =[SELECT Id, RecordTypeId FROM ContactoSite__c WHERE Contacto__c =:siv.Solicitante__c AND RecordTypeId = :rtContactoS LIMIT 1];
        System.debug(cs);
        csId = cs.Id;
        return csId;
    }  
    public String getImage(String sivId){
        System.debug(sivId);
        urlImagen='';
        siv = [SELECT Id,Licencia__c,NoLicencia__c,VIN__c,NoMotor__c,FinanciamientoTFS__c,SeguroToyota__c,NoFactura__c,DistribuidoraDondeDeseaRecibir__c,PolizaSeguro__c,FechaFacturacion__c,NoPlaca__c,EstadoDondeEmplaco__c,FechaEstimadaLlegada__c,FechaRecepcionSolicitudDOD__c,SeEstaraProduciendoEn__c,EstatusDOD__c,Distribuidor_recepcion__c,VehiculoSolicitado__r.name,ColorExternoOpcion1__r.name,ColorExternoOpcion2__r.name,Solicitante__c,IDSerie__c,Estatus__c,Observaciones__c,name,Solicitante__r.Name,TipoSolicitud__c,Correoelectronico__c,Telefono__c,WorkID__c,Puesto__c,Compania__c,MontoMaximoSolicitud__c,
               VehiculoSolicitado__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c FROM SolicitudInternaVehiculos__c WHERE Id=:sivId];
        System.debug(siv);
        List<Serie__c> lisModserie = [Select Id , Imagen__c,imagenSite__c from Serie__c where Id =:siv.IDSerie__c ];  
        System.debug(lisModserie);
        if(!lisModserie.isEmpty()){
            string strDocUrl = System.Label.urlSiteImagen+lisModserie[0].imagenSite__c;
            urlImagen = strDocUrl.replace('amp;', '');
        }
        
        return urlImagen;
    }   
    
    public PageReference editar(){
        showBotonCan = true;
        showBotonGua = true;
        showBotonApp = false;
        showBotonEdi = false;
        showLectura = false;
        showEditar = true;
        return null;
    }
    
    public PageReference cancelarEdi(){
        showBotonEdi = true;
        showBotonGua = false;
        showBotonApp = true;
        showBotonCan = false;
        showLectura = true;
        showEditar = false;
        return null;
    }
    
    public PageReference guardar(){
        try{
            String Id = sivController.getId();
            SolicitudInternaVehiculos__c sivsOL = [select id,name,VehiculoSolicitado__c,Distribuidor_recepcion__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c,Estatus__c, 
                                                   solicitante__c,Solicitante__r.Name from SolicitudInternaVehiculos__c where id =: id];
            update sivsOL;
            showBotonEdi = true;
            showBotonGua = false;
            showBotonApp = true;
            showBotonCan = false;
            showLectura = true;
            showEditar = false;
            
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
        return null;
    } 
    
    public PageReference enviarAprobacion(){
        PageReference page;
        SolicitudInternaVehiculos__c sivAprove = [SELECT AllocNo__c,AnioModelo__c,AnoModelo__c,AnoProduccion__c,BarcoSemana__c,BlackSpaceFieldSet1__c,BlackSpaceFieldSet2__c,BlackSpaceFieldSet__c,Calle__c,Ciudad__c,CodigoColorExternoOp1__c,CodigoColorExternoOp2__c,CodigoColorInternoOp1__c,CodigoColorInternoOp2__c,CodigoDistribuidor__c,Colonia__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c,ColorInternoOpcion1__c,ColorInternoOpcion2__c,Color_disponible__c,Compania__c,ComprobanteDomicilio__c,ComprobanteRFC__c,ConnectionReceivedId,ConnectionSentId,Correoelectronico__c,CreatedById,CreatedDate,CURP__c,DelegacionOMunicipio__c,DistribuidoraDondeDeseaRecibir__c,DistribuidoraEntrega__c,Distribuidor_recepcion__c,DummyVIN__c,Entregado__c,EnviarSolicitud__c,EstadoDondeEmplaco__c,Estado__c,EstatusDOD__c,Estatus__c,EvidenciaEntrega__c,fechaAsignacionVehiculo__c,FechaEntrega__c,FechaEstimadaLlegada__c,FechaFacturacion__c,FechaHoy__c,FechaLlegadaDistribuidor__c,FechaRecepcionSolicitudDOD__c,Fecha_de_inicio_P_liza__c,FinanciamientoTFS__c,FormaPago__c,HojaManifiesto__c,Id,IdentificacionOficial__c,IDSerie__c,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Licencia__c,Meses__c,Modelo__c,MontoMaximoSolicitud__c,Name,NoFactura__c,NoLicencia__c,NombreQuienAutoriza__c,NombreQuienRecogeVehiculo__c,NombreVehiculoSite__c,Nombre__c,NoMotor__c,NoPlaca__c,Observaciones__c,OwnerId,Parentesco__c,Plazo__c,PoliticaAsignada__c,PolizaSeguro__c,PrecioEmpleado__c,Puesto__c,QuienRecogevehculo__c,RecogeVehiculo__c,RecordTypeId,Regresar__c,Responsiva__c,RFC__c,SeEstaraProduciendoEn__c,SeguroToyota__c,Solicitante__c,SolicitudCredito__c,SystemModstamp,TelefonoCasa__c,TelefonoOficina__c,Telefono__c,TipoSolicitud__c,VehiculoSIV__c,VehiculoSolicitado__c,VencimientoPoliza__c,VIN__c,WorkID__c FROM SolicitudInternaVehiculos__c WHERE Id=:sivId];
        sivAprove.EnviarSolicitud__c = true;
        List<String> sivId = new List<String>(); 
        sivId.add(ApexPages.currentPage().getParameters().get('id'));
        boolean withArchi = false;
        //boolean withUser = false;
        String sivIdString = sivAprove.id;
        List<String> listSiv = new List<String>();
        listSiv.add(sivIdString);
        withArchi = validateArchive(listSiv);
        //withUser = validateUser(sivId);
        String mess = '';
        if (!withArchi){
            mess = 'Debe adjuntar archivo para poder enviar a Aprobación.\n';
        }/* if (!withUser){
mess += 'Debe tener por lo menos un Usuario del vehículo para poder enviar a Aprobación.\n';
}*/
        try{
            if( withArchi){
                update sivAprove;
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Se envio solicitud para aprobacion');
                //ApexPages.addMessage(msg);
                alertaOKApp = true;
                showBotonApp = false;
                showBotonGua = false;
                showBotonEdi = false;
                // page = new PageReference('/SolicitudSiteHome?id='+csId);
                // page.setRedirect(true);            
            } else {
                sivAprove.EnviarSolicitud__c = false;
                alertaOKApp = false;
                ApexPages.Message msg =  new ApexPages.Message(ApexPages.Severity.ERROR, mess);    
                ApexPages.addMessage(msg);
            }
        }catch(Exception e){
            sivAprove.EnviarSolicitud__c = false;
            System.debug(e.getMessage());
            alertaOKApp = false;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
        return page; 
        
    }
    
    public PageReference actualizar(){
        PageReference page;
        page = new PageReference(urlRefresh);
        page.setRedirect(true);
        return page;
    }
    
    
    public PageReference vehicleSelectedUpdate(){
        SolicitudInternaVehiculos__c solicitud = siv;
        VehiculoSIV__c sivSelected = vehiclesSIVMap.get(selectVehicle);
        if(sivSelected != null)
        solicitud.VehiculoSolicitado__c = sivSelected.Id;
        
        solicitud.ColorExternoOpcion1__c = null;
        solicitud.ColorInternoOpcion1__c = null;
        solicitud.ColorExternoOpcion2__c = null;
        solicitud.ColorInternoOpcion2__c = null;
        selectExternalColor1 = null;
        selectExternalColor2 = null;
        /*solicitud.ColorExternoOpcion1__c = null;
solicitud.ColorInternoOpcion1__c = null;
solicitud.ColorExternoOpcion2__c = null;
solicitud.ColorInternoOpcion2__c = null;*/
        
        try{
            update solicitud;
            getImage(solicitud.Id);
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    public PageReference externalColor1Update(){
        SolicitudInternaVehiculos__c solicitud = siv;
        solicitud.VehiculoSolicitado__c = siv.VehiculoSolicitado__c;
        solicitud.ColorExternoOpcion1__c = selectExternalColor1;
        solicitud.ColorInternoOpcion1__c = externalColors1Map.containsKey(selectExternalColor1) ? externalColors1Map.get(selectExternalColor1).ColorInternoModelo__c : null;
        try{
            update solicitud;
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    public PageReference externalColor2Update(){
        SolicitudInternaVehiculos__c solicitud = siv;
        solicitud.ColorExternoOpcion2__c = selectExternalColor2;
        solicitud.ColorInternoOpcion2__c = externalColors2Map.containsKey(selectExternalColor2) ? externalColors2Map.get(selectExternalColor2).ColorInternoModelo__c : null;
        
        try{
            update solicitud;
        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    public List<SelectOption> getVehiclesSIVOptions(){
        List<SelectOption> options = new List<SelectOption>();
        SolicitudInternaVehiculos__c solicitud = siv;
        List<VehiculoSIV__c> sivQuery;
        List<Relacion_PoliticaSIV__c> sivQueryAux;
        SolicitudInternaVehiculos__c ca = new SolicitudInternaVehiculos__c();
        System.debug(sivController.getId());
        ca = [SELECT Id, politicaAsignada__c, IDSerie__c,solicitante__C,VehiculoSolicitado__c FROM SolicitudInternaVehiculos__c WHERE Id=:sivController.getId()];
        System.debug(siv);
        System.debug('%% Monto máximo de solicitud: ' + solicitud.MontoMaximoSolicitud__c);
        System.debug('%% Monto politicaAsignada: ' + ca.politicaAsignada__c );
        System.debug('%% Vehículo solicitado: ' + solicitud.VehiculoSolicitado__c);
        
        if(solicitud.TipoSolicitud__c == 'Asignación'){
            //sivQuery = [SELECT Id, Name, AnoModelo__c, PrecioTotalEmpleado__c, Modelo__r.Name FROM VehiculoSIV__c WHERE ((InicioVigencia__c <= TODAY AND FinVigencia__c >= TODAY) AND PrecioTotalEmpleado__c <=: solicitud.MontoMaximoSolicitud__c) OR Id=:solicitud.VehiculoSolicitado__c ORDER BY Name DESC];
            sivQueryAux = [Select r.Veh_culo_SIV__c, r.Precio_Empleado__c, r.Pol_tica_Autos_Asignados__c, r.Name, r.Id, r.Anio_Modelo__c From Relacion_PoliticaSIV__c r where Pol_tica_Autos_Asignados__c =: ca.politicaAsignada__c ORDER BY Name DESC];
            List<String> misIds = new  List<String>();
            for(Relacion_PoliticaSIV__c otr: sivQueryAux){
                misIds.add( otr.Veh_culo_SIV__c);
            }
            sivQuery = [SELECT Id, Name, AnoModelo__c, PrecioTotalEmpleado__c, Modelo__r.Name FROM VehiculoSIV__c WHERE id in : misIds ORDER BY Name DESC];
            
        } else if(solicitud.TipoSolicitud__c == 'Pool'){
            //sivQuery = [SELECT Id, Name, AnoModelo__c, PrecioTotalEmpleado__c, Modelo__r.Name FROM VehiculoSIV__c WHERE (InicioVigencia__c <= TODAY AND FinVigencia__c >= TODAY) OR Id=:solicitud.VehiculoSolicitado__c ORDER BY Name DESC];
            sivQuery = [SELECT Id, Name, AnoModelo__c, PrecioTotalEmpleado__c, Modelo__r.Name FROM VehiculoSIV__c ORDER BY Name DESC];
        }
        
        
        if(sivQuery != null){ 
            vehiclesSIVMap.putAll(sivQuery);
            if(solicitud.VehiculoSolicitado__c == null){
                options.add(new SelectOption('0', '---------------Seleccione---------------'));
            }
            
            for(VehiculoSIV__c siv: sivQuery){
                options.add(new SelectOption(siv.Id, siv.Name+'    '+siv.AnoModelo__c+'-$'+siv.PrecioTotalEmpleado__c));
            }
        }
        
        return options;
    }
    
    
    public List<SelectOption> getExternalColor1(){
        List<SelectOption> options = new List<SelectOption>();
        SolicitudInternaVehiculos__c solicitud = siv;
        if(vehiclesSIVMap.containsKey(selectVehicle)){
            VehiculoSIV__c sivSelected = vehiclesSIVMap.get(selectVehicle);
            system.debug('vehiculo de la solicitd'+sivSelected);
            system.debug('modelo de solicitud'+sivSelected.Modelo__r.Name);
            List<ColorExternoModelo__c> colorsQuery = [SELECT Id, Name, ColorInternoModelo__c, ColorInternoModelo__r.Name FROM ColorExternoModelo__c WHERE ( Disponible__c = true AND AnioModelo__c =:sivSelected.AnoModelo__c AND Modelo__c=:sivSelected.Modelo__r.Name) OR Id=:selectExternalColor1];
            system.debug('query de colores'+colorsQuery);
            if(colorsQuery != null){
                externalColors1Map.putAll(colorsQuery);
                
                if(solicitud.ColorExternoOpcion1__c == null){
                    options.add(new SelectOption('0', '---------------Seleccione---------------'));
                }
                
                for(ColorExternoModelo__c ce: colorsQuery){
                    options.add(new SelectOption(ce.Id, 'Ext: '+ce.Name+'- Int: '+ce.ColorInternoModelo__r.Name));
                }
            }
        }
        
        return options;
        
    }
    
    public List<SelectOption> getExternalColor2(){
        List<SelectOption> options = new List<SelectOption>();
        SolicitudInternaVehiculos__c solicitud = siv;
        
        if(vehiclesSIVMap.containsKey(selectVehicle)){
            VehiculoSIV__c sivSelected = vehiclesSIVMap.get(selectVehicle);
            List<ColorExternoModelo__c> colorsQuery = [SELECT Id, Name, ColorInternoModelo__c, ColorInternoModelo__r.Name FROM ColorExternoModelo__c WHERE ( Disponible__c = true AND AnioModelo__c =:sivSelected.AnoModelo__c AND Modelo__c=:sivSelected.Modelo__r.Name AND Tipo__c = 'Estandar') OR Id=:selectExternalColor2];
            System.debug(colorsQuery);
            if(colorsQuery != null){
                externalColors2Map.putAll(colorsQuery);
                
                if(solicitud.ColorExternoOpcion2__c == null){
                    options.add(new SelectOption('0', '---------------Seleccione---------------'));
                }
                
                for(ColorExternoModelo__c ce: colorsQuery){
                    options.add(new SelectOption(ce.Id, 'Ext: '+ce.Name+'- Int: '+ce.ColorInternoModelo__r.Name));
                }
            }
        }
        
        return options;
    }
    
    
    public  List<SelectOption> DistribuidorList{
        get
        {
            List<Account> UserTemp = [Select name,Codigo_Distribuidor__c ,Id From Account WHERE recordtype.name = 'Distribuidor' AND Asignado_a_autos_Pool__c = true order by Codigo_Distribuidor__c asc];
            DistribuidorList = new List<SelectOption>(); 
            DistribuidorList.add(new SelectOption('0', '----------------Seleccione----------------'));
            for(Account temp : UserTemp) 
            {
                DistribuidorList.add(new SelectOption(temp.Id, temp.Codigo_Distribuidor__c+' - '+temp.name));
            }
            if(Test.isRunningTest()){
                DistribuidorList.add(new SelectOption('test', 'test'));
            }
            return DistribuidorList;
        }
        set;
    }
    
    public PageReference updateAvisoPrivacidad(){
        Contact ctcSite = new Contact(); 
        String correoEmpleado = [Select Correoelectronico__c,VehiculoSolicitado__c FROM SolicitudInternaVehiculos__c WHERE id =: siv.id].Correoelectronico__c;
        if(correoEmpleado != null){
            ctcSite = [Select id,Permiso_de_Privacidad__c,Email  FROM Contact WHERE Email  =: correoEmpleado AND recordtypeId = '012i0000001QOyJAAW'];    
            
        }
        if(ctcSite != null){
            ctcSite.Permiso_de_Privacidad__c = false;
            system.debug('upd aviso priv'+avisoPrivacidad);
            update ctcSite;
            
        }        
        
        return null;
    }
    
    public PageReference updateSolicitudFields(){
        try{
            if(!selectDistribuidora.contains('----------------Seleccione----------------')){
                String solicitudId = sivController.getId();
                
                SolicitudInternaVehiculos__c solicitud = [SELECT Id, Solicitante__c,IDSerie__c,Estatus__c,Observaciones__c,name,Solicitante__r.Name,TipoSolicitud__c,Correoelectronico__c,Telefono__c,WorkID__c,Puesto__c,Compania__c,MontoMaximoSolicitud__c
                                                          , VehiculoSolicitado__c,ColorExternoOpcion1__c,ColorExternoOpcion2__c FROM SolicitudInternaVehiculos__c WHERE Id=:solicitudId];
                solicitud.Distribuidor_recepcion__c = selectDistribuidora ;
                system.debug('soliti up'+solicitud);
                update solicitud;
            }
            
        } catch(Exception ex){
            System.debug(ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    public boolean validateArchive(List<String> sivId){
        boolean validate = false;
        List<ContentDocumentLink> contDL = [Select Id,LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility From ContentDocumentLink where LinkedEntityId  IN : sivId];
        System.debug('%% este es el tam: ' + contDL.size());
        if(contDL!= null && !contDL.isEmpty() ){
            validate = true;
        }
        System.debug('%% En SolicitudSiteEditarController.validateArchive() sí se tienen documentos adjuntos: ' + validate);
        return validate;
        
    }
    
    /*public boolean validateUser(List<String> sivId){
boolean validate = false;
List<UsuarioVehiculo__c> contSolIntV = [SELECT Id, Name, NombreCompleto__c,RelacionEmpleado__c, RecogeVehiculo__c FROM UsuarioVehiculo__c WHERE SolicitudInternaVehiculos__c =:sivId];
System.debug('%% este es el tam de contSolIntV: ' + contSolIntV.size());
if(contSolIntV!= null && !contSolIntV.isEmpty() ){
validate = true;
}
System.debug('%% En SolicitudSiteEditarController.validateUser() sí se tienen Users: ' + validate);
return validate;
} */
    
    /*
public List<SelectOption> getLugarRecep(){
List<SelectOption> options = new List<SelectOption>();
Schema.DescribeFieldResult fieldResult = SolicitudInternaVehiculos__c.DistribuidoraDondeDeseaRecibir__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
options.add(new SelectOption('0', '---------------Seleccione---------------'));
Integer au = 0;
for( Schema.PicklistEntry pickListVal : ple){
options.add( new SelectOption(''+au, pickListVal.getLabel()) );
au++;
}
return options;
}*/
    
    /* public List<SelectOption> getDistRecep(){
List<SelectOption> options = new List<SelectOption>();
SolicitudInternaVehiculos__c solicitud = (SolicitudInternaVehiculos__c)sivController.getRecord();
List<VehiculoSIV__c> sivQuery;
List<Account> listAcc = new List<Account>();
System.debug(sivController.getId());
listAcc = [SELECT Id, name, Codigo_Distribuidor__c FROM account WHERE RecordType.Name IN ('Distribuidor') and Asignado_a_autos_Pool__c = true order by name limit 1000];
if(listAcc != null && listAcc.size() > 0){
options.add(new SelectOption('0', '---------------Seleccione---------------'));
for(Account a: listAcc){
options.add(new SelectOption(a.id, a.Codigo_Distribuidor__c + ' - ' + a.Name));
}
}
return options;
}*/
    
    /* public PageReference distRecepSelectedUpdate(){
System.debug('%%En distRecepSelectedUpdate');
SolicitudInternaVehiculos__c solicitud = (SolicitudInternaVehiculos__c)sivController.getRecord();
System.debug('%% selectDistribuidora tiene ' + selectDistribuidora);
solicitud.Distribuidor_recepcion__c = selectDistribuidora;

try{
update solicitud;
System.debug('%% se hizo update ' + solicitud);
} catch(Exception ex){
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
}
return null;
}*/
    
    
}