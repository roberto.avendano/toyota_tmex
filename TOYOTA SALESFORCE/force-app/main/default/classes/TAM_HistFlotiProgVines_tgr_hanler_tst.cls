/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clas de prueba para la logica de la clase 
    					TAM_HistFlotillasProgVines_tgr_hanler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    29-Noviembre-2020    Héctor Figueroa            	Creación
******************************************************************************* *

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_HistFlotiProgVines_tgr_hanler_tst {

	static String VaRtAccRegPMCC = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();
	static String VaRtAccRegPFCC = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo CP').getRecordTypeId();

    static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();   
    static String VaRtCteFisicaMor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();   
    static String VaRtCteFisicaFis = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();   

	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	@TestSetup static void loadData(){
		
		Group grupoPrueba = new Group(
			Name = 'CARSON',
			Type = 'Regular'
		);
		insert grupoPrueba;
		
		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'Rango B (+31 unidades',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'Rango B (+31 unidades | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;
		
		Account clienteMoral = new Account(
			Name = 'BAYER DE MEXICO SA DE CV',
			UnidadesAutosDemoAutorizadas__c = 5,
			RecordTypeId = VaRtAccRegPMCC,
			TAM_ProgramaRango__c = rangoFlotilla.id,
			TAM_FechaVigenciaInicio__c = Date.today(),
			TAM_FechaVigenciaFin__c = Date.today(),
			TAM_IdExternoNombre__c = 'BAYER DE MEXICO SA DE CV'			
		);
		insert clienteMoral;

		TAM_HistoricoFlotillasProgVines__c histoFlotProgVin = new TAM_HistoricoFlotillasProgVines__c(
        	Name = 'C9628-57001-121219-0189',
        	No_VC_Upsert__c = 'C9628-57001-121219-0189',
        	Fecha_de_Aprobacion__c = Date.today(),
        	Nombre_Cliente_Corporativo__c = 'BAYER DE MEXICO SA DE CV',
        	Clave_Dealer__c = '57001',
        	Nombre_Dealer__c = 'TOYOTA LÓPEZ MATEOS', 
        	Rango__c = 'Rango B (+31 unidades)',
        	Status__c = 'Cerrada',
        	Anio__c = '2020',
        	Codigo__c = '7491',
        	Modelo__c = 'HILUX',
        	Color__c = '040/20',
            TAM_ActSoloCheckOut__c = false
	    );
	    insert histoFlotProgVin;  
		             				
	}

    static testMethod void TAM_HistFlotiProgVinesOK() {

		//Inicia la prueba
    	Test.startTest();
		
			Account objCliente = [Select Id, Name, TAM_IdExternoNombre__c 
				From Account LIMIT 1];        
	   		System.debug('EN TAM_HistFlotiProgVinesOK objCliente: ' + objCliente);
		
			//Consulta los datos del cliente
			TAM_HistoricoFlotillasProgVines__c objHistProgFlot = [Select Id, Name, No_VC_Upsert__c 
				From TAM_HistoricoFlotillasProgVines__c LIMIT 1];        
	   		System.debug('EN TAM_HistFlotiProgVinesOK objHistProgFlot: ' + objHistProgFlot);
            
            //Un array de fixedSearchResults
            Id [] fixedSearchResults = new Id[1];
            //Inicializa el array fixedSearchResults con el cliente de la prueba
            fixedSearchResults[0] = objCliente.Id;
            //Inicializa setFixedSearchResults en Test
            Test.setFixedSearchResults(fixedSearchResults);
            //Crea un objeto del tipo TAM_HistFlotillasProgVines_tgr_hanler
            TAM_HistFlotillasProgVines_tgr_hanler objPaso = new TAM_HistFlotillasProgVines_tgr_hanler();
            objPaso.getIdCliente(objCliente.Name, new Set<String>{VaRtAccRegPMCC}); //'BAYER DE MEXICO'
	   		
	   	//Termina la prueba	
		Test.stopTest();

    }
}