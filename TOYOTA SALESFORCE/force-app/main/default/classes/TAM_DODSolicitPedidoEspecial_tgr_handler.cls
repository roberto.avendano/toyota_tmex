/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_DODSolicitPedidoEspecial__c
    					y actualiza los campos del VIN y No Cto a partir del objeto  Inventario_en_Transito__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    06-Abril-2020     Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_DODSolicitPedidoEspecial_tgr_handler extends TriggerHandler {

    String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    String sRectorTypePasoPrograma = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    String sRectorTypePasoFlotilla = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();
    String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();
    
    private List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPedEspList;
    private Map<Id,TAM_DODSolicitudesPedidoEspecial__c> mapDodSolPedEsp;
    private Map<Id,TAM_DODSolicitudesPedidoEspecial__c> oldMapDodSolPedEsp;
    
    public TAM_DODSolicitPedidoEspecial_tgr_handler() {
        this.lDodSolPedEspList = Trigger.new;
        this.mapDodSolPedEsp = (Map<Id,TAM_DODSolicitudesPedidoEspecial__c>)Trigger.newMap;
        this.oldMapDodSolPedEsp = (Map<Id,TAM_DODSolicitudesPedidoEspecial__c>)Trigger.oldMap;      
    }

    public override void beforeUpdate(){
        actualizaCampos(lDodSolPedEspList, oldMapDodSolPedEsp);
    }

    public override void afterUpdate(){
        creaColaboracion(lDodSolPedEspList, oldMapDodSolPedEsp);
        creaRegCheckOut(lDodSolPedEspList, oldMapDodSolPedEsp);
    }
    
    private void actualizaCampos(List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPedEspList,
        Map<Id,TAM_DODSolicitudesPedidoEspecial__c> oldMapDodSolPedEsp){
        System.debug('ENTRO A TAM_DODSolicitPedidoEspecial_tgr_handler.actualizaCampos...');        
        
        Set<String> setIdVin = new Set<String>();
        Set<String> setIdNoContrato = new Set<String>();
        Map<String, Inventario_en_Transito__c> mapInvTransitoVIN = new Map<String, Inventario_en_Transito__c>();
        Map<String, Inventario_en_Transito__c> mapInvTransitoNoOrdn = new Map<String, Inventario_en_Transito__c>();
                
        //Recorre la lista de reg lDodSolPedEspList del tipo TAM_DODSolicitudesPedidoEspecial__c
        for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEsp : lDodSolPedEspList){
            //Ya tiene TAM_NoOrden__c 
            if (oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_NoOrden__c == null && objDodSolPedEsp.TAM_NoOrden__c != null
                && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_VIN__c  == null ){
                //Busca ese VIN em el mapa de mapInvTransitoVIN
                objDodSolPedEsp.TAM_Estatus__c = 'Ordenado';
                System.debug('ENTRO A TAM_DODSolicitPedidoEspecial_tgr_handler.actualizaCampos objDodSolPedEsp2: ' + objDodSolPedEsp);  
            }//fin si oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_VIN__c == null && objDodSolPedEsp.TAM_VIN__c != null
            if (oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_DummyVin__c == null && objDodSolPedEsp.TAM_DummyVin__c != null){
                //Busca ese VIN em el mapa de mapInvTransitoVIN
                if (objDodSolPedEsp.TAM_Estatus__c != 'Cancelado')
                    objDodSolPedEsp.TAM_Estatus__c = 'Asignado';
                System.debug('ENTRO A TAM_DODSolicitPedidoEspecial_tgr_handler.actualizaCampos objDodSolPedEsp2: ' + objDodSolPedEsp);  
            }//fin si oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_VIN__c == null && objDodSolPedEsp.TAM_VIN__c != null
            
            //Actualiza el campo de TAM_EstatusAnterior__c si el estatus de la soliicutud es != 'En proceso de cancelación'
            if (objDodSolPedEsp.TAM_Estatus__c != 'En proceso de cancelación')
                objDodSolPedEsp.TAM_EstatusAnterior__c = objDodSolPedEsp.TAM_Estatus__c;                    
        }//Fin del for para lDodSolPedEspList
        
    }

    private void creaColaboracion(List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPedEspList,
        Map<Id,TAM_DODSolicitudesPedidoEspecial__c> oldMapDodSolPedEsp){
        System.debug('ENTRO A TAM_DODSolicitPedidoEspecial_tgr_handler.creaColaboracion...');        
        
        List<TAM_DODSolicitudesPedidoEspecial__Share > lColaboraDODFlotillaPrograma1erNivel = new List<TAM_DODSolicitudesPedidoEspecial__Share >();
        List<TAM_DODSolicitudesPedidoEspecial__Share > lColaboraDODFlotillaProgramaDOD = new List<TAM_DODSolicitudesPedidoEspecial__Share >();
        Map<String, String> mapIdGrupoNombre = new Map<String, String>();

        //Consulta los grupos asociados a setPropVin
        for (Group grupoPublico : [Select g.Name, g.Id From Group g
                where Name LIKE '%Aprobador DTM (Nivel 2)%' OR Name LIKE '%DOD Seguimiento%']){
             mapIdGrupoNombre.put(grupoPublico.Name, grupoPublico.id);
        }
        System.debug('EN creaColaboracion mapIdGrupoNombre: ' + mapIdGrupoNombre);
                
        //Dale vuelta de nuevo a los reg que se estan actualizando 
        for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEsp : lDodSolPedEspList){
            //Ve si la solicutud ya fue autorizada por DTM 1er nivel
            if (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Autorizado'){
               //Crea la colaboracion de todos los reg para el grupo 'Aprobador DTM (Nivel 2)''
               lColaboraDODFlotillaPrograma1erNivel.add(new TAM_DODSolicitudesPedidoEspecial__Share (
                   UserOrGroupId = mapIdGrupoNombre.get('Aprobador DTM (Nivel 2)'),
                   ParentId = objDodSolPedEsp.ID,
                   AccessLevel = 'Edit'
                   )
               );
            }//fin si oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_VIN__c == null && objDodSolPedEsp.TAM_VIN__c != null

            //Crea la colaboracion de todos los reg para el grupo 'Aprobador DTM (Nivel 1)''
            if (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' && objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Autorizado' 
                && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Autorizado'){
               //Crea la colaboracion de todos los reg para el grupo 'Aprobador DTM (Nivel 2)''
               lColaboraDODFlotillaProgramaDOD.add(new TAM_DODSolicitudesPedidoEspecial__Share (
                   UserOrGroupId = mapIdGrupoNombre.get('DOD Seguimiento'),
                   ParentId = objDodSolPedEsp.ID,
                   AccessLevel = 'Edit'
                   )
               );
            }//fin si oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_VIN__c == null && objDodSolPedEsp.TAM_VIN__c != null
        }//Fin del for para lDodSolPedEspList
        
        System.debug('EN creaColaboracion lColaboraDODFlotillaPrograma1erNivel: ' + lColaboraDODFlotillaPrograma1erNivel);        
        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsRes1erNivel = Database.insert(lColaboraDODFlotillaPrograma1erNivel, false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsRes1erNivel){
            if (!objDtbUpsRes.isSuccess())
                System.debug('EN Hubo un error a la hora de crear/Actualizar la colaboración con TAM_DODSolicitudesPedidoEspecial__Share 1er nivelERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());                        
        }//Fin del for para lDtbUpsRes

        System.debug('EN creaColaboracion lColaboraDODFlotillaProgramaDOD: ' + lColaboraDODFlotillaProgramaDOD);
        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsResDOD = Database.insert(lColaboraDODFlotillaProgramaDOD, false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsResDOD){
            if (!objDtbUpsRes.isSuccess())
                System.debug('EN Hubo un error a la hora de crear/Actualizar la colaboración con TAM_DODSolicitudesPedidoEspecial__Share DOD ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());                        
        }//Fin del for para lDtbUpsRes
        
    }

    private void creaRegCheckOut(List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPedEspList,
        Map<Id,TAM_DODSolicitudesPedidoEspecial__c> oldMapDodSolPedEsp){
        System.debug('ENTRO A TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut...');        

        String sRectorTypeFlotillaRep = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Pedido Especial Rep').getRecordTypeId();    
        String sRectorTypePasoFlotilla = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();

        List<TAM_CheckOutDetalleSolicitudCompra__c> lTAMCheckOutDetalleSolicitudCompraUps = new List<TAM_CheckOutDetalleSolicitudCompra__c>();
        Set<String> setIdExtCheckOutDtm2doNivel = new Set<String>();
        //Un objeto para  TAM_LogsErrores__c   
        List<TAM_LogsErrores__c> lErrores = new List<TAM_LogsErrores__c>();        

        Map<String, String> mapIdGrupoNombre = new Map<String, String>();
        List<TAM_CheckOutDetalleSolicitudCompra__Share > lColaboraCheckOutDtm2doNivel = new List<TAM_CheckOutDetalleSolicitudCompra__Share >();        
        //Consulta los grupos asociados a setPropVin
        for (Group grupoPublico : [Select g.Name, g.Id From Group g
                where Name LIKE '%Aprobador DTM (Nivel 2)%']){
             mapIdGrupoNombre.put(grupoPublico.Name, grupoPublico.id);
        }
        System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapIdGrupoNombre: ' + mapIdGrupoNombre);

        Set<String> setIdSolUpdProc = new Set<String>();
        Map<String, TAM_SolicitudesFlotillaPrograma__c> mapSoFlotProgUps = new Map<String, TAM_SolicitudesFlotillaPrograma__c> ();
        
        Savepoint sp = Database.setSavepoint();        
        try{

            Set<String> setVin = new Set<String>();
            Set<String> setIdSol = new Set<String>();
            Map<String, String> mapIdSolIdTipoReg = new Map<String, String>();
            Map<String, Date> mapVinIdUltMov = new Map<String, Date>();

            //Dale vuelta de nuevo a los reg que se estan actualizando 
            for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEsp : lDodSolPedEspList){
                //Ve si la solicutud ya fue autorizada por DTM 1er nivel
                if ( objDodSolPedEsp.TAM_Estatus__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Estatus__c
                    || (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Autorizado')
                    || (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Rechazado')
                    || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Autorizado')
                    || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Rechazado')
                    || objDodSolPedEsp.TAM_ActRegCheckOut__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActRegCheckOut__c
                    || objDodSolPedEsp.TAM_ActualizaAlgunosCampos__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActualizaAlgunosCampos__c      
                    ){
                    //
                    //Crea la Llave externa para el objeto de TAM_CheckOutDetalleSolicitudCompra__c
                    String[] arrIdExterDod = objDodSolPedEsp.TAM_IdExterno__c.split('-');
                    if (arrIdExterDod[0] != null)    
                        setIdSol.add(arrIdExterDod[0]);
                    if (objDodSolPedEsp.TAM_VIN__c != null) 
                        setVin.add(objDodSolPedEsp.TAM_VIN__c);
                }
            }
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut setIdSol: ' + setIdSol);
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut setVin: ' + setVin);

	        //Ve si existe el VIN en Vehiculo__c
	        for (Vehiculo__c ventas : [Select v.id, v.Fecha_de_Venta__c, v.Name From Vehiculo__c v
	            Where Name IN :setVin And Estatus__c = 'RDR']){
	            //Inicializa  dtFechaDD
	            mapVinIdUltMov.put(ventas.Name, ventas.Fecha_de_Venta__c);
	        }
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapVinIdUltMov: ' + mapVinIdUltMov.keyset());
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapVinIdUltMov: ' + mapVinIdUltMov.values());

            //Consulta el tipo de reg de la solicitd
            for (TAM_SolicitudesFlotillaPrograma__c objSol : [Select id, RecordTypeid From TAM_SolicitudesFlotillaPrograma__c 
                Where id IN:setIdSol]){
                mapIdSolIdTipoReg.put(objSol.id, objSol.RecordTypeid);
            }
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapIdSolIdTipoReg: ' + mapIdSolIdTipoReg.keyset());
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapIdSolIdTipoReg: ' + mapIdSolIdTipoReg.values());
               
            //Dale vuelta de nuevo a los reg que se estan actualizando 
            for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEsp : lDodSolPedEspList){
                //Ve si la solicutud ya fue autorizada por DTM 1er nivel
                if ( objDodSolPedEsp.TAM_Estatus__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Estatus__c
                    || (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Autorizado')
                    || (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Rechazado')
                    || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Autorizado')
                    || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Rechazado')
                    || objDodSolPedEsp.TAM_ActRegCheckOut__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActRegCheckOut__c
                    || objDodSolPedEsp.TAM_ActualizaAlgunosCampos__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActualizaAlgunosCampos__c      
                    ){
                    //Crea la Llave externa para el objeto de TAM_CheckOutDetalleSolicitudCompra__c
                    String[] arrIdExterDod = objDodSolPedEsp.TAM_IdExterno__c.split('-');
                    String sName = arrIdExterDod[2] + '-' + objDodSolPedEsp.TAM_Serie__c + '-' + arrIdExterDod[3] + '-' + arrIdExterDod[1]; // HIACE-Hiace 12 Pasajeros-5609-2021
                    String sCntReg;
                    String sCodColInt;                
                    String sIdExtCheckOut; ////a20e0000003EXVOAA4-2020-AVANZA-2206-W09-10-012e0000000AxEWAA0-012e0000000AYwYAAW-Arrendadora-0

                    String sIdTipoRegSol = mapIdSolIdTipoReg.get(arrIdExterDod[0]);
                    String sRectorTypePasoCheckOut = getTipoRegSolCheckOut(arrIdExterDod[0], sIdTipoRegSol);
                    System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut sRectorTypePasoCheckOut: ' + sRectorTypePasoCheckOut + ' sIdTipoRegSol: ' + sIdTipoRegSol);
                    
                    //Tiene una long de 7
                    if (arrIdExterDod.size() < 9){ //  a241Y00001VgU6gQAF-2020-HILUX-7494-040-200-
                        //Toma el campo 5 y despues el ultimo digito ya que es el consecutivo
                        sCntReg = arrIdExterDod[5].right(1); 
                        sCodColInt = arrIdExterDod[5].substring(0, arrIdExterDod[5].length()-1);
                        sIdExtCheckOut = arrIdExterDod[0] + '-' + arrIdExterDod[1] + '-' + arrIdExterDod[2] + '-' + arrIdExterDod[3] + '-' + arrIdExterDod[4] + '-' + sCodColInt + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoCheckOut + '-' + objDodSolPedEsp.TAM_Pago__c + '-' + sCntReg;
                    }//Fin si arrIdExterDod.size() = 7
                    //Tiene una long mayor a 9                    
                    if (arrIdExterDod.size() > 8){ //  a241Y00001NaVm9QAF-2021-HILUX-7490-040-20-0-57605-TOYOTA COUNTRY
                        sCntReg = arrIdExterDod[6];                    
                        sIdExtCheckOut = arrIdExterDod[0] + '-' + arrIdExterDod[1] + '-' + arrIdExterDod[2] + '-' + arrIdExterDod[3] + '-' + arrIdExterDod[4] + '-' + arrIdExterDod[5] + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoCheckOut + '-' + objDodSolPedEsp.TAM_Pago__c + '-' + sCntReg;
                    }//Fin arrIdExterDod.size() > 8                
                    System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut sIdExtCheckOut: ' + sIdExtCheckOut);        
                                        
                    //Un objeto del tipo TAM_CheckOutDetalleSolicitudCompra__c
                    TAM_CheckOutDetalleSolicitudCompra__c objTAMCheckOutDetalleSolicitudCompra = new TAM_CheckOutDetalleSolicitudCompra__c();

                    //Si lo esta actualizando Edoardo de DTM entonces crea la colaboración del CheckOut Para 2do nivel Lucio
                    if(objDodSolPedEsp.TAM_ActRegCheckOut__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActRegCheckOut__c
                        || objDodSolPedEsp.TAM_Estatus__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Estatus__c
                        || Test.isRunningTest()){
                            
                        objTAMCheckOutDetalleSolicitudCompra.RecordTypeId = sRectorTypeFlotillaRep;
                        objTAMCheckOutDetalleSolicitudCompra.OwnerId = objDodSolPedEsp.OwnerId;
                        objTAMCheckOutDetalleSolicitudCompra.Name = sName;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_CatalogoCentralizadoModelos__c = objDodSolPedEsp.TAM_CatalogoCentralizadoModelos__c; 
                        objTAMCheckOutDetalleSolicitudCompra.TAM_SolicitudFlotillaPrograma__c = arrIdExterDod[0]; 
                        objTAMCheckOutDetalleSolicitudCompra.TAM_TipoPago__c = objDodSolPedEsp.TAM_Pago__c;
                                                                                
                        objTAMCheckOutDetalleSolicitudCompra.TAM_PoliticaIncentivos__c = objDodSolPedEsp.TAM_PoliticaIncentivos__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncPropuestoPorcentaje__c = (objDodSolPedEsp.TAM_DescuentoAutorizadoDtm__c != null) ? objDodSolPedEsp.TAM_DescuentoAutorizadoDtm__c : 0.00;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncentivoTMEXPorcentaje__c = (objDodSolPedEsp.TAM_IncentivoTMEXPorcentaje__c != null ) ? objDodSolPedEsp.TAM_IncentivoTMEXPorcentaje__c : 0.00;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncDealerPorcentaje__c = (objDodSolPedEsp.TAM_IncDealerPorcentaje__c != null ) ? objDodSolPedEsp.TAM_IncDealerPorcentaje__c : 0.00;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncPropuestoCash__c = (objDodSolPedEsp.TAM_IncPropuestoCash__c != null ) ? objDodSolPedEsp.TAM_IncPropuestoCash__c : 0.00;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncentivoTMEXCash__c = (objDodSolPedEsp.TAM_IncentivoTMEXCash__c != null ) ? objDodSolPedEsp.TAM_IncentivoTMEXCash__c : 0.00;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IncDealerCash__c = (objDodSolPedEsp.TAM_IncDealerCash__c != null ) ? objDodSolPedEsp.TAM_IncDealerCash__c : 0.00;          
                        //objTAMCheckOutDetalleSolicitudCompra.TAM_FechaVentaDD__c = objDodSolPedEsp.TAM_VIN__c != null ? obtenFechaVentaDD(objDodSolPedEsp.TAM_VIN__c) : null;
                        if (objDodSolPedEsp.TAM_VIN__c != null)
                            if (mapVinIdUltMov.containsKey(objDodSolPedEsp.TAM_VIN__c))
                                objTAMCheckOutDetalleSolicitudCompra.TAM_FechaVentaDD__c = mapVinIdUltMov.get(objDodSolPedEsp.TAM_VIN__c);
                        objTAMCheckOutDetalleSolicitudCompra.TAM_VIN__c = objDodSolPedEsp.TAM_VIN__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_FechaCancelacion__c = objDodSolPedEsp.TAM_FechaCancelacion__c;
                        String sComCancela = objDodSolPedEsp.TAM_ComentarioDTM1__c != null ? objDodSolPedEsp.TAM_ComentarioDTM1__c + ' ' : ' ';       
                        sComCancela += objDodSolPedEsp.TAM_ComentarioDTM2__c != null ? objDodSolPedEsp.TAM_ComentarioDTM2__c : '';       
                        objTAMCheckOutDetalleSolicitudCompra.TAM_ComentarioDealer__c = sComCancela;         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Autorizacion1erNivel__c = objDodSolPedEsp.TAM_Autorizacion1erNivel__c;         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Autorizacion2doNivel__c = objDodSolPedEsp.TAM_Autorizacion2doNivel__c;         
                         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Historico__c = objDodSolPedEsp.TAM_Historico__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_TipoVenta__c = 'Pedido Especial';
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Cantidad__c = objDodSolPedEsp.TAM_CantidadProducir__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_DistribuidorEntrega__c = objDodSolPedEsp.TAM_Entrega__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_EntregarEnMiDistribuidora__c = objDodSolPedEsp.TAM_EntregarEnMiDistribuidora__c;
                        
                        //Actualiza el estatus TAM_EstatusDealerSolicitud__c si la solicutud fue Cancelado
                        if (objDodSolPedEsp.TAM_Estatus__c == 'Cancelado' || objDodSolPedEsp.TAM_Estatus__c == 'Rechazado Dealer')
                            objTAMCheckOutDetalleSolicitudCompra.TAM_EstatusDealerSolicitud__c  = 'Cancelada';
                        //Actualiza el campo de TAM_EstatusAnterior__c si el estatus de la soliicutud es != 'En proceso de cancelación'
                        if (objDodSolPedEsp.TAM_Estatus__c == 'En proceso de cancelación'){
                            mapSoFlotProgUps.put(objDodSolPedEsp.TAM_SolicitudFlotillaPrograma__c, new TAM_SolicitudesFlotillaPrograma__c(
                                id = objDodSolPedEsp.TAM_SolicitudFlotillaPrograma__c, TAM_EstatusCancelacion__c = 'En proceso de cancelación')
                            );
                        }//Fin si objDodSolPedEsp.TAM_Estatus__c == 'En proceso de cancelación'
                        objTAMCheckOutDetalleSolicitudCompra.TAM_EstatusDOD__c = objDodSolPedEsp.TAM_Estatus__c;                        
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IdExterno__c = sIdExtCheckOut;
                        //Agrega el objeto del tipo objTAMCheckOutDetalleSolicitudCompra a la lista de 
                        lTAMCheckOutDetalleSolicitudCompraUps.add(objTAMCheckOutDetalleSolicitudCompra);
                    }//Fin si objDodSolPedEsp.TAM_ActRegCheckOut__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActRegCheckOut__c

                    //Si lo esta actualizando Edoardo de DTM entonces crea la colaboración del CheckOut Para 2do nivel Lucio
                    if( (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Autorizado') 
                        || (objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion1erNivel__c != 'Rechazado')
                        || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Autorizado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Autorizado')
                        || (objDodSolPedEsp.TAM_Autorizacion2doNivel__c == 'Rechazado' && oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Autorizacion2doNivel__c != 'Rechazado')                
                        || Test.isRunningTest()){
                        //Inizializa los campos que se necesiten
                        String sComCancela = objDodSolPedEsp.TAM_ComentarioDTM1__c != null ? objDodSolPedEsp.TAM_ComentarioDTM1__c + ' ' : ' ';       
                        sComCancela += objDodSolPedEsp.TAM_ComentarioDTM2__c != null ? objDodSolPedEsp.TAM_ComentarioDTM2__c : '';       
                        objTAMCheckOutDetalleSolicitudCompra.TAM_ComentarioDealer__c = sComCancela;         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Autorizacion1erNivel__c = objDodSolPedEsp.TAM_Autorizacion1erNivel__c;         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_Autorizacion2doNivel__c = objDodSolPedEsp.TAM_Autorizacion2doNivel__c;         
                        objTAMCheckOutDetalleSolicitudCompra.TAM_IdExterno__c = sIdExtCheckOut;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_EstatusDOD__c  = objDodSolPedEsp.TAM_Estatus__c;
                        objTAMCheckOutDetalleSolicitudCompra.TAM_EstatusDealerSolicitud__c = objDodSolPedEsp.TAM_Autorizacion1erNivel__c;                        
                        //Agrega el objeto del tipo objTAMCheckOutDetalleSolicitudCompra a la lista de 
                        if (!Test.isRunningTest())
                            lTAMCheckOutDetalleSolicitudCompraUps.add(objTAMCheckOutDetalleSolicitudCompra);
                    }// Fin si esta actualizando Nivel 1 o Nivel 2

                    //Si lo esta actualizando el campo TAM_ActualizaAlgunosCampos__c
                    if(objDodSolPedEsp.TAM_ActualizaAlgunosCampos__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActualizaAlgunosCampos__c || Test.isRunningTest()){
                        //Busca en check out si existe un reg con el sIdExtCheckOut
                        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckout : [Select id From TAM_CheckOutDetalleSolicitudCompra__c
                            Where TAM_IdExterno__c =:sIdExtCheckOut]){
                            //Agrega el reg del checkot que se va a modificae a la lista de lTAMCheckOutDetalleSolicitudCompraUps
                            lTAMCheckOutDetalleSolicitudCompraUps.add(new TAM_CheckOutDetalleSolicitudCompra__c(TAM_IdExterno__c = sIdExtCheckOut,
                                    TAM_DistribuidorEntrega__c = objDodSolPedEsp.TAM_Entrega__c,
                                    TAM_EntregarEnMiDistribuidora__c = true,
                                    TAM_EstatusDOD__c = objDodSolPedEsp.TAM_Estatus__c
                                )
                            );
                        }
                    }//Fin si objDodSolPedEsp.TAM_ActualizaAlgunosCampos__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_ActualizaAlgunosCampos__c || Test.isRunningTest()
                    System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut objTAMCheckOutDetalleSolicitudCompra.TAM_IdExterno__c: '+ objTAMCheckOutDetalleSolicitudCompra.TAM_IdExterno__c);
                    System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut objTAMCheckOutDetalleSolicitudCompra: '+ objTAMCheckOutDetalleSolicitudCompra);                                                                     
                    
                    //Si lo esta actualizando Edoardo de DTM entonces crea la colaboración del CheckOut Para 2do nivel Lucio
                    if(objDodSolPedEsp.TAM_Autorizacion1erNivel__c == 'Autorizado' || Test.isRunningTest())
                       setIdExtCheckOutDtm2doNivel.add(sIdExtCheckOut);
                }//fin si objDodSolPedEsp.TAM_Estatus__c != oldMapDodSolPedEsp.get(objDodSolPedEsp.id).TAM_Estatus__c
            }//Fin del for para lDodSolPedEspList
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut setIdExtCheckOutDtm2doNivel: ' + setIdExtCheckOutDtm2doNivel);                                                                     
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut lTAMCheckOutDetalleSolicitudCompraUps: ' + lTAMCheckOutDetalleSolicitudCompraUps);
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapSoFlotProgUps: '+ mapSoFlotProgUps.keyset());
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut mapSoFlotProgUps: '+ mapSoFlotProgUps.values());            
                
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsCheckOut = Database.upsert(lTAMCheckOutDetalleSolicitudCompraUps, TAM_CheckOutDetalleSolicitudCompra__c.TAM_IdExterno__c, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsCheckOut){
                TAM_LogsErrores__c objError = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd CheckOut Estatus', TAM_Fecha__c = Date.today());        
                if (!objDtbUpsRes.isSuccess()){
                    objError.TAM_DetalleError__c = objDtbUpsRes.getErrors()[0].getMessage();
                    lErrores.add(objError);
                }
                if (objDtbUpsRes.isSuccess())
                    System.debug(' ID del reg actualizado en TAM_CheckOutDetalleSolicitudCompra__c: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes

            //Actualiza el estatus de la solicitud  cuando DOD esta en 'En proceso de cancelación'
            List<Database.Upsertresult> lDtbUpsSolFlotProg = Database.upsert(mapSoFlotProgUps.values(), TAM_SolicitudesFlotillaPrograma__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsSolFlotProg){
                TAM_LogsErrores__c objError = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd Sol Flot Progra Estatus', TAM_Fecha__c = Date.today());        
                if (!objDtbUpsRes.isSuccess()){
                    objError.TAM_DetalleError__c = objDtbUpsRes.getErrors()[0].getMessage();
                    lErrores.add(objError);
                }
                if (objDtbUpsRes.isSuccess())
                    System.debug(' ID del reg actualizado en TAM_SolicitudesFlotillaPrograma__c: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes

            //Consulta los datos del Checkout para setIdExtCheckOutDtm2doNivel
            for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOut2doNivel : [Select id From TAM_CheckOutDetalleSolicitudCompra__c
                Where TAM_IdExterno__c IN :setIdExtCheckOutDtm2doNivel ]){
                //Crea la colaboracion de todos los reg para el grupo 'Aprobador DTM (Nivel 1)''
                lColaboraCheckOutDtm2doNivel.add(new TAM_CheckOutDetalleSolicitudCompra__Share (
                   UserOrGroupId = mapIdGrupoNombre.get('Aprobador DTM (Nivel 2)'),
                   ParentId = objCheckOut2doNivel.ID,
                   AccessLevel = 'Edit'
                   )
                );   
            }
            System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut lColaboraCheckOutDtm2doNivel: ' + lColaboraCheckOutDtm2doNivel);

            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.SaveResult> lDtbUpsRes = Database.insert(lColaboraCheckOutDtm2doNivel, false);
            //Ve si hubo error         
            for (Database.SaveResult objDtbUpsRes : lDtbUpsRes){
                TAM_LogsErrores__c lerrorShare = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd CheckOut Estatus', TAM_Fecha__c = Date.today());
                if (!objDtbUpsRes.isSuccess()){
                    lerrorShare.TAM_DetalleError__c = 'Error en CheckOut2doNivelShare ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage();
                    lErrores.add(lerrorShare);                
                }
                if (objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_DODSolicitPedidoEspecial_tgr_handler.creaRegCheckOut Id: ' + objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
            
            //rollback para pruebas nada nas
            //Database.rollback(sp);
        
        }catch(Exception e){
            TAM_LogsErrores__c objErrorGen = new TAM_LogsErrores__c( TAM_Proceso__c = 'Upd CheckOut Estatus', TAM_Fecha__c = Date.today());            
            objErrorGen.TAM_DetalleError__c = 'Error general ERROR: ' + e.getMessage() + ' Linea: ' + e.getLineNumber();
            lErrores.add(objErrorGen);
        }
        System.debug('EN Hubo un error a la hora de crear/Actualizar TAM_CheckOutDetalleSolicitudCompra__c lErrores: ' + lErrores);
        
        //Crea el reg en lErrores
        insert lErrores;
        
    }
        
    private string getTipoRegSolCheckOut(String sIdSol, String sTipoRegSolConsPrm) {
        System.debug('EN getTipoRegSolCheckOut sIdSol: ' + sIdSol + ' sTipoRegSolConsPrm: ' + sTipoRegSolConsPrm);
        
        String sTipoRegSolPaso = '';
        String sTipoRegSolCons = '';
                
        //inicializa sTipoRegSolCons con sTipoRegSolConsPrm
        sTipoRegSolCons = sTipoRegSolConsPrm;
        System.debug('EN getTipoRegSolCheckOut sTipoRegSolCons: ' + sTipoRegSolCons);
        
        sTipoRegSolPaso = sTipoRegSolCons == sRectorTypePasoSolPrograma ? sRectorTypePasoPrograma :
        (sTipoRegSolCons == sRectorTypePasoSolProgramaCerrada ? sRectorTypePasoPrograma :
         ( sTipoRegSolCons == sRectorTypePasoSolVentaCorporativa ? sRectorTypePasoFlotilla : 
          ( sTipoRegSolCons == sRectorTypePasoSolVentaCorporativaCerrada ? sRectorTypePasoFlotilla : sRectorTypePasoPrograma)
         ) 
        );
            
        System.debug('EN getTipoRegSolCheckOut sTipoRegSolPaso: ' + sTipoRegSolPaso);
        return sTipoRegSolPaso;
    } 
        
}