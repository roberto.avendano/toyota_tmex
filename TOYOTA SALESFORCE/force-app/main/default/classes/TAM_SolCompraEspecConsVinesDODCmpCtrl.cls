/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para consuñltar los VINES asociados por el area de DOD para una solicitud
    					de tipo especial

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    07-Abril-2020     Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolCompraEspecConsVinesDODCmpCtrl {

	static String sRectorTypePasoPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();

    //Un constructor por default
    public TAM_SolCompraEspecConsVinesDODCmpCtrl(){}

   //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_OrdenDeCompraWrapperClass getWrpModelo(String sWrpModelo) {
    	System.debug('EN getWrpModelo: ' + sWrpModelo);
    	String sCadenaPaso = '';
    	
    	//Serializa el JSON sobre la clase de TAM_OrdenDeCompraWrapperClass
		TAM_OrdenDeCompraWrapperClass objTAM_OrdenDeCompraWrapperClassPaso = new TAM_OrdenDeCompraWrapperClass();
		objTAM_OrdenDeCompraWrapperClassPaso = (TAM_OrdenDeCompraWrapperClass) JSON.deserialize(sWrpModelo, TAM_OrdenDeCompraWrapperClass.class);
		System.debug('EN getWrpModelo objTAM_OrdenDeCompraWrapperClassPaso: ' + objTAM_OrdenDeCompraWrapperClassPaso);		
       
        //Regresa la cadena
        return objTAM_OrdenDeCompraWrapperClassPaso;
    }

    @AuraEnabled
    public static List<TAM_DODSolicitudesPedidoEspecial__c> getWrpModeloCheckOut(String sWrpModelo) {
    	System.debug('EN getWrpModeloCheckOut: ' + sWrpModelo);
    	String sCadenaPaso = '';

		List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPediEspe = new List<TAM_DODSolicitudesPedidoEspecial__c>();		
    	
    	//Serializa el JSON sobre la clase de TAM_OrdenDeCompraWrapperClass
		TAM_CheckoutWrapperClass objTAMCheckoutWrapperClass = new TAM_CheckoutWrapperClass();
		objTAMCheckoutWrapperClass = (TAM_CheckoutWrapperClass) JSON.deserialize(sWrpModelo, TAM_CheckoutWrapperClass.class);			
		System.debug('EN getWrpModeloCheckOut objTAMCheckoutWrapperClass: ' + objTAMCheckoutWrapperClass);

    	String[] lIdExterno = objTAMCheckoutWrapperClass.strIdExterno.split('-');
		String strIdSolicitudFinal = lIdExterno[0];
		String strIdCatCentMod = objTAMCheckoutWrapperClass.strIdCatCentMod;
        System.debug('EN getWrpModeloCheckOut strIdSolicitudFinal: ' + strIdSolicitudFinal + ' strIdCatCentMod: ' + strIdCatCentMod);

		//Ahora ve busca los registros asociados a recordId y objConsCatCentPaso.id en el objeto de TAM_DODSolicitudesPedidoEspecial__c
		for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspe : [Select id, Name, 
			//TAM_SolicitudCompraFLOTILLA__c,
			TAM_SolicitudFlotillaPrograma__c, 
			TAM_CatalogoCentralizadoModelos__c,
			TAM_Entrega__c, TAM_VIN__c, TAM_Estatus__c, TAM_NoOrden__c
			From TAM_DODSolicitudesPedidoEspecial__c Where 
			TAM_SolicitudFlotillaPrograma__c =: strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c =: strIdCatCentMod
			Order by TAM_Entrega__c, TAM_Estatus__c, TAM_VIN__c]){
			//Agregalos a la lista de lDodSolPediEspe
			lDodSolPediEspe.add(objDodSolPedEspe);
			System.debug('EN getWrpModeloCheckOut objDodSolPedEspe: ' + objDodSolPedEspe);
		}
       
		//Regresa la lista del tipo TAM_DODSolicitudesPedidoEspecial__c
		return lDodSolPediEspe;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static List<TAM_DODSolicitudesPedidoEspecial__c> getDatosDistribuidores(String recordId, Integer intIndex, String strAnioModelo,
    	String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, String strRecortTypeId, 
    	String strIdCatCentMod, String strsTipoRegFlotProg, Boolean blnSolFlotilla, Boolean blnSolPrograma,
    	Boolean blnSolFlotillaProgramaExcepcion, String strIdCatCentModFinal, String strIdRegistro, 
    	TAM_WrpSolicitudFlotillaProgramaModelos objModeloSelExcepCte) {
    		
    	System.debug('EN getDatosDistribuidores...');
    	System.debug('EN getDatosDistribuidores recordId: ' + recordId + ' intIndex: ' + intIndex + ' strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt + ' sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma + ' strIdCatCentMod: ' + strIdCatCentMod + ' strsTipoRegFlotProg: ' + strsTipoRegFlotProg + ' blnSolFlotilla: ' +  blnSolFlotilla + ' blnSolPrograma: ' + blnSolPrograma + ' blnSolFlotillaProgramaExcepcion: ' + blnSolFlotillaProgramaExcepcion);
    	System.debug('EN getDatosDistribuidores strIdCatCentModFinal: ' + strIdCatCentModFinal);
    	System.debug('EN getDatosDistribuidores strIdRegistro: ' + strIdRegistro);
    	System.debug('EN getDatosDistribuidores objModeloSelExcepCte: ' + objModeloSelExcepCte);

		List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPediEspe = new List<TAM_DODSolicitudesPedidoEspecial__c>();
    	String sCadenaPaso = '';  

    	//String strIdCatCentModCteFinal = strIdCatCentModFinal;
    	//String strIdSolicitudFinal = strIdRegistro;

    	String strIdSolicitudFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdSolicitud;
    	String strIdCatCentModCteFinal = objModeloSelExcepCte.lModelosSeleccionados[0].strIdCatCentrModelos;
    	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdCatCentModCteFinal: ' + strIdCatCentModCteFinal);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdSolicitudFinal: ' + strIdSolicitudFinal);	

		//Ahora ve busca los registros asociados a recordId y objConsCatCentPaso.id en el objeto de TAM_DODSolicitudesPedidoEspecial__c
		for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspe : [Select id, Name, 
			//TAM_SolicitudCompraFLOTILLA__c,
			TAM_SolicitudFlotillaPrograma__c, 
			TAM_CatalogoCentralizadoModelos__c,
			TAM_Entrega__c, TAM_VIN__c, TAM_Estatus__c, TAM_NoOrden__c
			From TAM_DODSolicitudesPedidoEspecial__c Where 
			TAM_SolicitudFlotillaPrograma__c =: strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c =: strIdCatCentModCteFinal
			Order by TAM_Entrega__c, TAM_Estatus__c, TAM_VIN__c]){
			//Agregalos a la lista de lDodSolPediEspe
			lDodSolPediEspe.add(objDodSolPedEspe);
			System.debug('EN getDatosDistribuidores objDodSolPedEspe: ' + objDodSolPedEspe);
		}		
		
		//Regresa la lista del tipo TAM_DODSolicitudesPedidoEspecial__c
		return lDodSolPediEspe;
		
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static List<TAM_DODSolicitudesPedidoEspecial__c> getDatosDistribuidoresFinal(String sWrpModelo ) {    	
    	System.debug('EN getDatosDistribuidores...');
    	System.debug('EN getDatosDistribuidores sWrpModelo: ' + sWrpModelo);

    	String sCadenaPaso = '';  
		
		List<TAM_DODSolicitudesPedidoEspecial__c> lDodSolPediEspe = new List<TAM_DODSolicitudesPedidoEspecial__c>();
		TAM_OrdenDeCompraWrapperClass objModeloSelExcepCte;
		TAM_CheckoutWrapperClass objTAMCheckoutWrapperClass;
		
		if (!Test.isRunningTest())		
			objModeloSelExcepCte = (TAM_OrdenDeCompraWrapperClass) JSON.deserializeStrict(sWrpModelo, TAM_OrdenDeCompraWrapperClass.class);
		if (Test.isRunningTest())
			objTAMCheckoutWrapperClass = (TAM_CheckoutWrapperClass) JSON.deserializeStrict(sWrpModelo, TAM_CheckoutWrapperClass.class);
			
    	System.debug('EN getDatosDistribuidores objModeloSelExcepCte: ' + objModeloSelExcepCte);
    	String[] lIdExterno;
		String strIdSolicitudFinal = '';
		String strIdCatCentMod = '';
		
		//Fin si !Test.isRunningTest()
		if (!Test.isRunningTest()){
    		lIdExterno = objModeloSelExcepCte.strIdExterno.split('-');
			strIdSolicitudFinal = lIdExterno[0];
			strIdCatCentMod = objModeloSelExcepCte.strIdCatModelos;			
		}//fin si !Test.isRunningTest()
		//Si es una prueba				
		if (Test.isRunningTest()){
	    	lIdExterno = objTAMCheckoutWrapperClass.strIdExterno.split('-');
			strIdSolicitudFinal = lIdExterno[0];
			strIdCatCentMod = objTAMCheckoutWrapperClass.strIdCatCentMod;
		}//Fin si Test.isRunningTest()

		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdCatCentMod: ' + strIdCatCentMod);	
		System.debug('EN TAM_SolicitFlotProgConfirmaVinesCmpCtrl.getDatosDistribuidores strIdSolicitudFinal: ' + strIdSolicitudFinal);	

		//Ahora ve busca los registros asociados a recordId y objConsCatCentPaso.id en el objeto de TAM_DODSolicitudesPedidoEspecial__c
		for (TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspe : [Select id, Name, 
			//TAM_SolicitudCompraFLOTILLA__c,
			TAM_SolicitudFlotillaPrograma__c, 
			TAM_CatalogoCentralizadoModelos__c,
			TAM_Entrega__c, TAM_VIN__c, TAM_Estatus__c, TAM_NoOrden__c
			From TAM_DODSolicitudesPedidoEspecial__c Where 
			TAM_SolicitudFlotillaPrograma__c =: strIdSolicitudFinal
			And TAM_CatalogoCentralizadoModelos__c =: strIdCatCentMod
			Order by TAM_Entrega__c, TAM_Estatus__c, TAM_VIN__c]){
			//Agregalos a la lista de lDodSolPediEspe
			lDodSolPediEspe.add(objDodSolPedEspe);
			System.debug('EN getDatosDistribuidores objDodSolPedEspe: ' + objDodSolPedEspe);
		}
		
		//Regresa la lista del tipo TAM_DODSolicitudesPedidoEspecial__c
		return lDodSolPediEspe;
		
    }
    
}