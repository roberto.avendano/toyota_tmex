@isTest
public class TAM_EstadoCuentaIncentivos_Test {
    
    @testSetup static void setup() {
         Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        //Dealer de prueba
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordtypeId = recordTypeDistribuidor,
            TAM_ActivarEdoCta__c = true
        );
        insert a01;
        
        //VIN de prueba
        Vehiculo__c v01 = new Vehiculo__c(
            Name='12345678901234567',
            Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        //Serie de prueba
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        //Modelo de prueba
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        //Provisión de incentivos
        TAM_ProvisionIncentivos__c provTest01 = new TAM_ProvisionIncentivos__c();
        provTest01.name = 'Provisión Agosto de prueba';
        provTest01.TAM_AnioDeProvision__c = '2020';
        provTest01.TAM_MesDeProvision__c = 'Agosto';
        insert provTest01;
        
        //Detalle de Provisión
        String recordIdProvisionRetail  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        //Retail
        TAM_DetalleProvisionIncentivo__c detalleProv01 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv01.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv01.RecordTypeId = recordIdProvisionRetail;
        detalleProv01.RetailSinReversa_Cerrado__c = false;
        detalleProv01.TAM_AnioModelo__c = '2020';
        detalleProv01.TAM_AplicanAmbos__c = false;
        detalleProv01.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv01.TAM_CodigoContable__c = '7400B2';
        detalleProv01.TAM_CodigoDealer__c = '57002';
        detalleProv01.TAM_Factura__c = 'FVA12330';
        detalleProv01.TAM_FechaCierre__c = date.today();
        detalleProv01.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv01.TAM_FechaVenta__c = '2020-11-09';
        detalleProv01.TAM_FirstName__c = 'DORA LUZ';
        detalleProv01.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv01.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv01.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv01.TAM_Lealtad__c = false;
        detalleProv01.TAM_Modelo__c = '7495';
        detalleProv01.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv01.TAM_PagadoConTFS__c = false;
        detalleProv01.TAM_PagadoSinTFS__c = true;
        detalleProv01.TAM_ProvicionarEfectivo__c = true;
        detalleProv01.TAM_Provisionado__c = false;
        detalleProv01.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv01.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv01.TAM_ProvisionarLealtad__c = false;
        detalleProv01.TAM_ProvisionCerrada__c = true;
        detalleProv01.TAM_Serie__c	 = 'HILUX';
        detalleProv01.TAM_SolicitadoDealer__c = false;
        detalleProv01.TAM_TipoMovimiento__c = 'RDR';
        detalleProv01.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv01.TAM_TransmitirDealer__c = true;
        detalleProv01.TAM_VIN__c  =  v01.id;
        detalleProv01.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv01;
    	    
        //VentaCorporativa
	    String recordIdProvisionVC  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('TAM_VentaCorporativa').getRecordTypeId();
        TAM_DetalleProvisionIncentivo__c detalleProv02 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv02.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv02.RecordTypeId = recordIdProvisionVC;
        detalleProv02.RetailSinReversa_Cerrado__c = false;
        detalleProv02.TAM_AnioModelo__c = '2020';
        detalleProv02.TAM_AplicanAmbos__c = false;
        detalleProv02.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv02.TAM_CodigoContable__c = '7400B2';
        detalleProv02.TAM_CodigoDealer__c = '57002';
        detalleProv02.TAM_Factura__c = 'FVA12330';
        detalleProv02.TAM_FechaCierre__c = date.today();
        detalleProv02.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv02.TAM_FechaVenta__c = '2020-11-09';
        detalleProv02.TAM_FirstName__c = 'DORA LUZ';
        detalleProv02.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv02.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv02.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv02.TAM_Lealtad__c = false;
        detalleProv02.TAM_Modelo__c = '7495';
        detalleProv02.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv02.TAM_PagadoConTFS__c = false;
        detalleProv02.TAM_PagadoSinTFS__c = true;
        detalleProv02.TAM_ProvicionarEfectivo__c = true;
        detalleProv02.TAM_Provisionado__c = false;
        detalleProv02.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv02.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv02.TAM_ProvisionarLealtad__c = false;
        detalleProv02.TAM_ProvisionCerrada__c = true;
        detalleProv02.TAM_Serie__c	 = 'HILUX';
        detalleProv02.TAM_SolicitadoDealer__c = false;
        detalleProv02.TAM_TipoMovimiento__c = 'RDR';
        detalleProv02.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv02.TAM_TransmitirDealer__c = true;
        detalleProv02.TAM_VIN__c  =  v01.id;
        detalleProv02.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv02;
        
        //Bono
	    String recordIdProvisionBono  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Bono').getRecordTypeId();
        TAM_DetalleProvisionIncentivo__c detalleProv03 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv03.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv03.RecordTypeId = recordIdProvisionBono;
        detalleProv03.RetailSinReversa_Cerrado__c = false;
        detalleProv03.TAM_AnioModelo__c = '2020';
        detalleProv03.TAM_AplicanAmbos__c = false;
        detalleProv03.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv03.TAM_CodigoContable__c = '7400B2';
        detalleProv03.TAM_CodigoDealer__c = '57002';
        detalleProv03.TAM_Factura__c = 'FVA12330';
        detalleProv03.TAM_FechaCierre__c = date.today();
        detalleProv03.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv03.TAM_FechaVenta__c = '2020-11-09';
        detalleProv03.TAM_FirstName__c = 'DORA LUZ';
        detalleProv03.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv03.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv03.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv03.TAM_Lealtad__c = false;
        detalleProv03.TAM_Modelo__c = '7495';
        detalleProv03.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv03.TAM_PagadoConTFS__c = false;
        detalleProv03.TAM_PagadoSinTFS__c = true;
        detalleProv03.TAM_ProvicionarEfectivo__c = true;
        detalleProv03.TAM_Provisionado__c = false;
        detalleProv03.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv03.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv03.TAM_ProvisionarLealtad__c = false;
        detalleProv03.TAM_ProvisionCerrada__c = true;
        detalleProv03.TAM_Serie__c	 = 'HILUX';
        detalleProv03.TAM_SolicitadoDealer__c = false;
        detalleProv03.TAM_TipoMovimiento__c = 'RDR';
        detalleProv03.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv03.TAM_TransmitirDealer__c = true;
        detalleProv03.TAM_VIN__c  =  v01.id;
        detalleProv03.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv03;
        
        //Con Reversa
        TAM_DetalleProvisionIncentivo__c detalleProv04 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv04.name = 'MR0EX3DD1L0005455-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv04.RecordTypeId = recordIdProvisionRetail;
        detalleProv04.RetailSinReversa_Cerrado__c = false;
        detalleProv04.TAM_AnioModelo__c = '2020';
        detalleProv04.TAM_AplicanAmbos__c = false;
        detalleProv04.TAM_Clasificacion__c = 'Con Reversa';
        detalleProv04.TAM_CodigoContable__c = '7400B2';
        detalleProv04.TAM_CodigoDealer__c = '57002';
        detalleProv04.TAM_Factura__c = 'FVA12330';
        detalleProv04.TAM_FechaCierre__c = date.today();
        detalleProv04.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv04.TAM_FechaVenta__c = '2020-11-09';
        detalleProv04.TAM_FirstName__c = 'DORA LUZ';
        detalleProv04.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv04.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv04.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv04.TAM_Lealtad__c = false;
        detalleProv04.TAM_Modelo__c = '7495';
        detalleProv04.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv04.TAM_PagadoConTFS__c = false;
        detalleProv04.TAM_PagadoSinTFS__c = true;
        detalleProv04.TAM_ProvicionarEfectivo__c = true;
        detalleProv04.TAM_Provisionado__c = false;
        detalleProv04.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv04.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv04.TAM_ProvisionarLealtad__c = false;
        detalleProv04.TAM_ProvisionCerrada__c = true;
        detalleProv04.TAM_Serie__c	 = 'HILUX';
        detalleProv04.TAM_SolicitadoDealer__c = false;
        detalleProv04.TAM_TipoMovimiento__c = 'RDR';
        detalleProv04.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv04.TAM_TransmitirDealer__c = true;
        detalleProv04.TAM_VIN__c  =  v01.id;
        detalleProv04.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv04;

		//Se inserta el custom Setting de días de vigencia
		TAM_DetalleEdoCta__c customSetting1 = new TAM_DetalleEdoCta__c();
        customSetting1.TAM_DiasVigencia__c = 97;
        insert customSetting1;
		        
    }
    
    @isTest static void testMethod1(){
		Date fechaHoy = date.today();
        TAM_ProvisionIncentivos__c provisionCreada = [select id from TAM_ProvisionIncentivos__c where name = 'Provisión Agosto de prueba'];
       TAM_EstadoCuentaIncentivos.generaEstadoCuenta(provisionCreada.id,fechaHoy,'Retail',fechaHoy);
        
    }
    
    @isTest static void testMethod2(){
		Date fechaHoy = date.today();
        TAM_ProvisionIncentivos__c provisionCreada = [select id from TAM_ProvisionIncentivos__c where name = 'Provisión Agosto de prueba'];
       TAM_EstadoCuentaIncentivos.generaEstadoCuenta(provisionCreada.id,fechaHoy,'VentaCorporativa',fechaHoy);
        
    }
    
    @isTest static void testMethod3(){
		Date fechaHoy = date.today();
        TAM_ProvisionIncentivos__c provisionCreada = [select id from TAM_ProvisionIncentivos__c where name = 'Provisión Agosto de prueba'];
       TAM_EstadoCuentaIncentivos.generaEstadoCuenta(provisionCreada.id,fechaHoy,'Bono',fechaHoy);
        
    }
    
    @isTest static void testMethod4(){
		Date fechaHoy = date.today();
        TAM_ProvisionIncentivos__c provisionCreada = [select id from TAM_ProvisionIncentivos__c where name = 'Provisión Agosto de prueba'];
       TAM_EstadoCuentaIncentivos.generaEstadoCuenta(provisionCreada.id,fechaHoy,'Reversa',fechaHoy);
        
    }
    
    
}