public with sharing class EvaluacionPersonalizadaController {

    public Evaluaciones_Dealer__c eDealer;
    public String txtdebug{get; set;}
    
    public EvaluacionPersonalizadaController(ApexPages.StandardController controller) {
        System.debug(controller);
        eDealer = (Evaluaciones_Dealer__c)controller.getRecord();
        System.debug(eDealer);
        txtdebug = JSON.serializePretty(ApexPages.currentPage().getParameters());
    }

    public PageReference getFormatoPersonalizado(){
        Map<String,String> parametros = ApexPages.currentPage().getParameters();
        Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        
        String vfRegistro ='';
        System.debug(eDealer);
        PageReference pRedirect;
        
        if(eDealer.RecordTypeId == null){
            List<String> tiposPermitidos = this.getTiposRegistro();
            System.debug(tiposPermitidos);
            if(tiposPermitidos.size()==1){
                eDealer.RecordTypeId = tiposPermitidos.get(0);
            }
        }
        
        if(!parametros.containsKey('id') || parametros.get('id')=='null'){
        	parametros.remove('id');
        	if(!parametros.containsKey('save_new')){
        		parametros.put('save_new','');
        	}
        }
        System.debug(eDealer.Id);
        System.debug(eDealer.RecordTypeId);
        if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Kodawari').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionKodawariWizard?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionesDealerDetail?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral').Id){
            if(parametros.containsKey('save_new')){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionIntegral?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral_Como_Nuevos').Id){
            if(parametros.containsKey('save_new')){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionIntegral?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Como_Nuevos').Id){
            if(parametros.containsKey('save_new')){
                pRedirect = new PageReference('/apex/EvaluacionComoNuevos?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionComoNuevos?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('EvaluacionRetencionClientes').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionKodawariWizard?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionRetencionClientesDetail?id='+eDealer.Id);
            }//Inician Evaluaciones EDER, SSC y de servicio
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_EDER').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionServicioDetail?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_SSC').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionServicioDetail?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_OperacionServicio').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionServicioDetail?id='+eDealer.Id);
            }
        }else if(eDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_BodyPaint').Id){
            if(parametros.containsKey('save_new') || parametros.containsKey('edit') || eDealer.Id==null){
                pRedirect = new PageReference('/apex/EvaluacionIntegral?RecordType='+eDealer.RecordTypeId);
            }else{
                pRedirect = new PageReference('/apex/EvaluacionServicioDetail?id='+eDealer.Id);
            }
        }
        
        if(pRedirect!=null){
            pRedirect.getParameters().putAll(parametros);
            pRedirect.setRedirect(true);
            return pRedirect;
        }else{
            return null;
        }
        
    }
    
    public String getTipoRegistro(){
        return JSON.serializePretty(eDealer);
    }

    public List<Id> getTiposRegistro(){
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        DescribeSObjectResult result = gd.get('Evaluaciones_Dealer__c').getDescribe();
        Map<String, Schema.RecordTypeInfo> recordTypeInfo = result.getRecordTypeInfosByName();
		System.debug(recordTypeInfo.values());
        List<Id> optns = new List<Id>();
        String principal ='';
        for(Schema.RecordTypeInfo rt : recordTypeInfo.values()){
            if(rt.isAvailable()){
                if(!rt.getName().equals('Principal')){
                    optns.add(rt.getRecordTypeId());
                }else{
                	principal = rt.getRecordTypeId();
                }
            }
        }
        if(optns.size()==0 && principal!=''){
        	optns.add(principal);
        }
        System.debug(optns);
        return optns;

    }
}