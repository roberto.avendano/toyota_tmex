public without sharing class TAM_ProvisionExcepcionesControllerClass {
    
    /***	EXCEPCIONES RETAIL	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getExcepcionesRetail(String recordId){
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  	TAM_VIN__r.Name,
                                                                                TAM_VIN__r.Id,
                                                                                TAM_CodigoDealer__c,
                                                                                TAM_NombreDealer__c,
                                                                                TAM_CodigoVenta__c,
                                                                                TAM_Serie__c,
                                                                                TAM_Modelo__c,
                                                                                TAM_AnioModelo__c,
                                                                                TAM_FechaEnvio__c,
                                                                                TAM_TipoMovimiento__c,
                                                                                TAM_FirstName__c,
                                                                                TAM_LastName__c,
                                                                                TAM_Version__c,
                                                                                TAM_FechaVenta__c,
                                                                                TAM_Factura__c,
                                                                                TAM_ExepcionIncentivo__r.Id,
                                                                                TAM_ExepcionIncentivo__r.Name,
                                                                                TAM_ExepcionIncentivo__r.Nombre_DM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_Fecha_Autorizacion_DM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_PoliticaSeleccionadaRetail__r.TAM_PoliticaIncentivos__r.Id,
                                                                                TAM_ExepcionIncentivo__r.TAM_PoliticaSeleccionadaRetail__r.TAM_PoliticaIncentivos__r.Name,
                                                                                TAM_ExepcionIncentivo__r.TAM_IncentivoTMEX__c,
                                                                                
                                                                                TAM_ExepcionIncentivo__r.TAM_VarianzaRetail__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_Comentario__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_ComentarioDM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_ComentarioFinanzas__c,
                                                                                TAM_ProvicionarEfectivo__c,
                                                                                TAM_TransmitirDealer__c,
                                                                                TAM_Guardado__c,
                                                                                TAM_FechaCierre__c,
                                                                                TAM_ProvisionCerrada__c,
                                                                                TAM_IdExterno__c,
                                                                                TAM_CodigoContable__c,

                                                                                TAM_ExcepcionEstadoCuenta__r.Id,
                                                                                TAM_ExcepcionEstadoCuenta__r.Name,
                                                                                TAM_ExcepcionEstadoCuenta__r.TAM_FolioFacturaCobroTMEX__c,
                                                                                TAM_ExcepcionEstadoCuenta__r.TAM_Fecha_Pago__c,
                                                                                TAM_IncentivoTMEX_Efectivo__c

                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Excepción Retail'
                                                                      ORDER BY TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        return lstDetalleProvision;
    }
    
    //guardar registros provision retail
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> setRecordExcepcionRetail(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }        
        return lstDetalleUpsert;
    } 
    
    //cerrar provision excepcion retail
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvisionExcepcionRetail(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
         Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10);
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetalleUpsert;
    }
    
    /***	EXCEPCIONES BONOS	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getExcepcionesBonos(String recordId){
    List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT    TAM_VIN__r.Name,
                                                                            TAM_VIN__r.Id,
                                                                            TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                            TAM_CodigoDealer__c,
                                                                            TAM_NombreDealer__c,
                                                                            TAM_CodigoVenta__c,
                                                                            TAM_Serie__c,
                                                                            TAM_Modelo__c,
                                                                            TAM_AnioModelo__c,
                                                                            TAM_FechaEnvio__c,
                                                                            TAM_TipoMovimiento__c,
                                                                            TAM_FirstName__c,
                                                                            TAM_LastName__c,
                                                                            TAM_Version__c,
                                                                            TAM_FechaVenta__c,
                                                                            TAM_Factura__c,
                                                                            Bonos__c,
                                                                            Bonos__r.Id,
                                                                            Bonos__r.Name,
                                                                            TAM_BonoTMEX_Efectivo__c,
                                                                            TAM_ProductoFinanciero__c,
                                                                            TAM_BonoTMEX_PF__c,
                                                                            TAM_ProvisionarBonoEfectivo__c,
                                                                            TAM_ProvisionarBonoFinanciero__c,
                                                                            TAM_TransmitirBonoEfectivo__c,
                                                                            TAM_AplicanAmbos__c,
                                                                            TAM_IdExterno__c,
                                                                            TAM_ProvisionCerrada__c,
                                                                            TAM_CodigoContable__c,
                                                                 			TAM_SolicitudBono__r.Id,
                                                                  			TAM_SolicitudBono__r.Name,
                                                                  			TAM_SolicitudBono__r.TAM_BonoSeleccionado__r.Id,
                                                                  			TAM_SolicitudBono__r.TAM_BonoSeleccionado__r.Name,

                                                                              TAM_ExcepcionEstadoCuenta__r.Id,
                                                                              TAM_ExcepcionEstadoCuenta__r.Name,
                                                                              TAM_ExcepcionEstadoCuenta__r.TAM_FolioFacturaCobroTMEX__c,
                                                                              TAM_ExcepcionEstadoCuenta__r.TAM_Fecha_Pago__c

                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Excepción Bono'
                                                                      ORDER BY TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        return lstDetalleProvision;
    }
    
    //guardar registros provision bono
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> setRecordExcepcionBono(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }        
        return lstDetalleUpsert;
    } 
    
    //cerrar provision excepcion bono
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvisionExcepcionBono(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10);
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
            
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetalleUpsert;
    }
    
    
    /***	EXCEPCIÓN VENTA CORPORATIVA	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getExcepcionesVtaCorp(String recordId){
    List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  	TAM_VIN__r.Name,
                                                                                TAM_VIN__r.Id,
                                                                                TAM_CodigoDealer__c,
                                                                                TAM_NombreDealer__c,
                                                                                TAM_CodigoVenta__c,
                                                                                TAM_Serie__c,
                                                                                TAM_Modelo__c,
                                                                                TAM_AnioModelo__c,
                                                                                TAM_FechaEnvio__c,
                                                                                TAM_TipoMovimiento__c,
                                                                                TAM_FirstName__c,
                                                                                TAM_LastName__c,
                                                                                TAM_Version__c,
                                                                                TAM_FechaVenta__c,
                                                                                TAM_Factura__c,
                                                                  
                                                                                TAM_ExepcionIncentivo__r.Id,
                                                                                TAM_ExepcionIncentivo__r.Name,
                                                                                TAM_ExepcionIncentivo__r.Nombre_DM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_Fecha_Autorizacion_DM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_PoliticaSeleccionadaRetail__r.TAM_PoliticaIncentivos__r.Id,
                                                                                TAM_ExepcionIncentivo__r.TAM_PoliticaSeleccionadaRetail__r.TAM_PoliticaIncentivos__r.Name,
                                                                  
                                                                                TAM_ExepcionIncentivo__r.TAM_Varianza__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_Comentario__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_ComentarioDM__c,
                                                                                TAM_ExepcionIncentivo__r.TAM_ComentarioFinanzas__c,
                                                                  
                                                                  				TAM_ExepcionIncentivo__r.TAM_IncentivoManual__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_IncentivoTMEXPorcentaje__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_MontoIncentivoTMEXPolitica__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_IncentivoTMEX__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_TotalIncentivoTMEXManual__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_MontoIncentivoTMEXManual__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_PrecioSolicitadoDealer__c ,
                                                                  				TAM_ExepcionIncentivo__r.TAM_PorcentajeDealer__c,
                                                                  				TAM_ExepcionIncentivo__r.TAM_PorcentajeTMEX__c,
                                                                  
                                                                                TAM_ProvicionarEfectivo__c,
                                                                                TAM_TransmitirDealer__c,
                                                                                TAM_Guardado__c,
                                                                                TAM_FechaCierre__c,
                                                                                TAM_ProvisionCerrada__c,
                                                                                TAM_IdExterno__c,
                                                                                TAM_CodigoContable__c,

                                                                                TAM_ExcepcionEstadoCuenta__r.Id,
                                                                                TAM_ExcepcionEstadoCuenta__r.Name,
                                                                                TAM_ExcepcionEstadoCuenta__r.TAM_FolioFacturaCobroTMEX__c,
                                                                                TAM_ExcepcionEstadoCuenta__r.TAM_Fecha_Pago__c,
                                                                                TAM_IncentivoTMEX_Efectivo__c

                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Excepción Venta Corporativa'
                                                                      ORDER BY TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        return lstDetalleProvision;
    }
    
    //guardar registros provision bono
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> setRecordExcepcionVentaCorp(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }        
        return lstDetalleUpsert;
    } 
    
    //cerrar provision excepcion bono
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvisionExcepcionVentaCorp(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
         Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10);
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
            
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetalleUpsert;
    }
    
    /***	TRANSFERENCIAS VTA CORP A RETAIL	***/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getTransferidos(String recordId){
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  	TAM_VIN__r.Name,
                                                                                TAM_VIN__r.Id,
                                                                                TAM_CodigoDealer__c,
                                                                                TAM_NombreDealer__c,
                                                                                TAM_CodigoVenta__c,
                                                                                TAM_Serie__c,
                                                                                TAM_Modelo__c,
                                                                                TAM_AnioModelo__c,
                                                                                TAM_FechaEnvio__c,
                                                                                TAM_TipoMovimiento__c,
                                                                                TAM_FirstName__c,
                                                                                TAM_LastName__c,
                                                                                TAM_Version__c,
                                                                                TAM_FechaVenta__c,
                                                                                TAM_Factura__c,
                                                                               
                                                                                TAM_IncentivoTMEX_Efectivo__c,
                                                                                
                                                                                TAM_ProvicionarEfectivo__c,
                                                                                TAM_TransmitirDealer__c,
                                                                                TAM_Guardado__c,
                                                                                TAM_FechaCierre__c,
                                                                                TAM_ProvisionCerrada__c,
                                                                                TAM_IdExterno__c,
                                                                                TAM_CodigoContable__c
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      AND     TAM_Clasificacion__c = 'Transferido Vta.Corp. a Retail'
                                                                      ORDER BY TAM_CodigoDealer__c ASC,TAM_VIN__r.Name ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        return lstDetalleProvision;
    }
    
    //guardar registros provision retail
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> setRecordTransferidos(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }        
        return lstDetalleUpsert;
    } 
    
    //cerrar provision excepcion retail
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvisionTransferidos(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10);
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetalleUpsert;
    }
    
}