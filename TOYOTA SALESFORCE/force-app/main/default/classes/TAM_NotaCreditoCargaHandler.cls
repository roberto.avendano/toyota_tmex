/******************************************************************************** 
    Desarrollado por:   Globant México
    Autor:              Cecilia Cruz Moran
    Proyecto:           TOYOTA TAM
    Descripción:        Trigger Handler que procesa la carga de la notas de crédito

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                 Autor                       Descripcion
    1.0    15-Junio-2021        Cecilia Cruz Moran           Creación
******************************************************************************* */
public class TAM_NotaCreditoCargaHandler {
    public static void getNotasCargadas(List<TAM_Notas_Credito_Carga__c> lstNewNotasCarga){
        
        //Obtener registros de VIN y Facturas relacionadas a las notas de credito.
        Set<String> setFacturas = new Set<String>();
        Set<String> setFacturas_UUID1 = new Set<String>();
        Set<String> setFacturas_UUID2 = new Set<String>();
        for(TAM_Notas_Credito_Carga__c obj : lstNewNotasCarga){
            setFacturas.add(obj.TAM_UUIDRelacionado__c);
            setFacturas_UUID1.add(obj.TAM_UUIDRelacionado1__c);
            setFacturas_UUID2.add(obj.TAM_UUIDRelacionado2__c);
        }
        /*Map Facturas*/		Map<String,Factura__c> mapFactura = getFacturas(setFacturas);
        /*Map Facturas UUID 1*/	Map<String,Factura__c> MapFactura_UUID1 = getFacturas_UUID1(setFacturas_UUID1);
        /*Map Facturas UUID 2*/	Map<String,Factura__c> MapFactura_UUID2 = getFacturas_UUID2(setFacturas_UUID2);

        
        //Obtener notas de credito cargadas y mapear al obj TAM_Notas_Credito__c
        List<TAM_Notas_Credito__c> lstNotasCredito = new List<TAM_Notas_Credito__c>();
        for(TAM_Notas_Credito_Carga__c objNotaCargada : lstNewNotasCarga){
            TAM_Notas_Credito__c objNotasCredito = new TAM_Notas_Credito__c();
            /*Código Distribuidor*/		objNotasCredito.TAM_Codigo_Distribuidor__c = objNotaCargada.TAM_Codigo_Distribuidor__c;
            /*Fecha de Operación*/		objNotasCredito.TAM_FechaOperacion__c = objNotaCargada.TAM_FechaOperacion__c;
            /*Total*/					objNotasCredito.TAM_Total__c = objNotaCargada.TAM_Total__c;
            /*Subtotal*/				objNotasCredito.SubTotal__c = objNotaCargada.TAM_SubTotal__c;
            /*Folio*/					objNotasCredito.TAM_Folio__c = objNotaCargada.TAM_Folio__c;
            /*Fecha de Folio*/			objNotasCredito.TAM_FechaFolio__c = objNotaCargada.TAM_FechaFolio__c;
            /*RFC Receptor*/			objNotasCredito.TAM_RFCReceptor__c = objNotaCargada.TAM_RFCReceptor__c;
            /*Nombre Receptor*/			objNotasCredito.TAM_NombreReceptor__c = objNotaCargada.TAM_NombreReceptor__c;
            /*UUID Nota Credito*/		objNotasCredito.TAM_UUID_Nota_Credito__c = objNotaCargada.TAM_UUID_Nota_Credito__c;
            /*UUID Relacionado*/		objNotasCredito.TAM_UUIDRelacionado__c = objNotaCargada.TAM_UUIDRelacionado__c;
            /*UUID Relacionado 1*/		objNotasCredito.TAM_UUIDRelacionado1__c = objNotaCargada.TAM_UUIDRelacionado1__c;
            /*UUID Relacionado 2*/		objNotasCredito.TAM_UUIDRelacionado2__c = objNotaCargada.TAM_UUIDRelacionado2__c;
            /*Metodo de Pago*/          objNotasCredito.TAM_MetodoPago__c = objNotaCargada.TAM_MetodoPago__c;
            
            /*Fecha de Timbrado*/		objNotasCredito.TAM_Fecha_Timbrado__c = objNotaCargada.TAM_Fecha_Timbrado__c;
            /*Nombre*/					objNotasCredito.Name = objNotaCargada.TAM_Folio__c + '-' + objNotaCargada.TAM_UUID_Nota_Credito__c;
            
            //Validar si contiene factura
            if(mapFactura.containsKey(objNotaCargada.TAM_UUIDRelacionado__c)){
                /*Factura*/				objNotasCredito.TAM_FacturaUUIDRelacionado__c = mapFactura.get(objNotaCargada.TAM_UUIDRelacionado__c).id;
            }
            //Validar si contiene factura de UUID 1
            if(MapFactura_UUID1.containsKey(objNotaCargada.TAM_UUIDRelacionado1__c)){
                /*Factura1*/			objNotasCredito.TAM_FacturaUUIDRelacionado1__c = MapFactura_UUID1.get(objNotaCargada.TAM_UUIDRelacionado1__c).id;
            }
            //Validar si contiene factura de UUID 2
            if(MapFactura_UUID2.containsKey(objNotaCargada.TAM_UUIDRelacionado2__c)){
                /*Factura2*/			objNotasCredito.TAM_FacturaUUIDRelacionado2__c = MapFactura_UUID2.get(objNotaCargada.TAM_UUIDRelacionado2__c).id;
            }
            lstNotasCredito.add(objNotasCredito);
        }
        
        //Insertar en el objecto TAM_Notas_Credito__c
        try{
            Database.upsert(lstNotasCredito, TAM_Notas_Credito__c.TAM_UUID_Nota_Credito__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
    }
    
    //Obtener mapa de Facturas
    public static Map<String,Factura__c> getFacturas(Set<String> setFacturas){
        Map<String,Factura__c> MapFactura = new Map<String,Factura__c>();
        List<Factura__c> lstFactura = [SELECT Id,TAM_UUID__c,VIN__r.id, VIN__r.Name FROM Factura__c WHERE TAM_UUID__c IN: setFacturas];
        for(Factura__c objFactura : lstFactura){
            MapFactura.put(objFactura.TAM_UUID__c, objFactura);
        }
        return MapFactura;
    }
    
    //Obtener mapa de Facturas UUID 1
    public static Map<String,Factura__c> getFacturas_UUID1(Set<String> setFacturas_UUID1){
        Map<String,Factura__c> MapFactura_UUID1 = new Map<String,Factura__c>();
        List<Factura__c> lstFactura = [SELECT Id,TAM_UUID__c,VIN__r.id, VIN__r.Name FROM Factura__c WHERE TAM_UUID__c IN: setFacturas_UUID1];
        for(Factura__c objFactura : lstFactura){
            MapFactura_UUID1.put(objFactura.TAM_UUID__c, objFactura);
        }
        return MapFactura_UUID1;
    }
    
    //Obtener mapa de Facturas UUID 2
    public static Map<String,Factura__c> getFacturas_UUID2(Set<String> setFacturas_UUID2){
        Map<String,Factura__c> MapFactura_UUID2 = new Map<String,Factura__c>();
        List<Factura__c> lstFactura = [SELECT Id,TAM_UUID__c,VIN__r.id, VIN__r.Name FROM Factura__c WHERE TAM_UUID__c IN: setFacturas_UUID2];
        for(Factura__c objFactura : lstFactura){
            MapFactura_UUID2.put(objFactura.TAM_UUID__c, objFactura);
        }
        return MapFactura_UUID2;
    }
}