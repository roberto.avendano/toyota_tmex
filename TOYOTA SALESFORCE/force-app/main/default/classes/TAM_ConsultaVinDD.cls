/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para consultar si un VIN esta apartado por otro distribuidor

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    28-Octubre-2021      Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_ConsultaVinDD {

    public class wrpInventDifDealer{
        @AuraEnabled public Boolean blnExisteDDLeadInventDifDealer  {get;set;}
        @AuraEnabled public String sOwnerFinal {get;set;}
        @AuraEnabled public String sCodDistFinal {get;set;}
        @AuraEnabled public Boolean blnExisteVin {get;set;}
        @AuraEnabled public Boolean blnMismoProspecto {get;set;}
        @AuraEnabled public Boolean blnMismoProp {get;set;}
        
        public wrpInventDifDealer(){
            this.blnExisteDDLeadInventDifDealer = false;
            this.sCodDistFinal = '';
        }
        
        public wrpInventDifDealer(Boolean blnExisteDDLeadInventDifDealer, String sCodDistFinal){
            this.blnExisteDDLeadInventDifDealer = blnExisteDDLeadInventDifDealer;
            this.sCodDistFinal = sCodDistFinal;
        }
    }
    
    //Un metodo para buscar el Vin en TAM_LeadInventarios__c 
    public static wrpInventDifDealer consultaVinDD(String sstrVinBusqPrm, String sCodDistPrm){
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD sstrVinBusqPrm: ' + sstrVinBusqPrm + ' sCodDistPrm: ' + sCodDistPrm);
        
        Boolean blnExiste = false;
        wrpInventDifDealer objWrpInventDifDealer = new wrpInventDifDealer(false, sCodDistPrm);
        objWrpInventDifDealer.blnExisteVin = false;
        objWrpInventDifDealer.blnMismoProp = false;
        objWrpInventDifDealer.blnMismoProspecto = false;

        String strIdUsrAct = UserInfo.getUserId();
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD strIdUsrAct: ' + strIdUsrAct);
        
        for (TAM_LeadInventarios__c objLedInvt : [Select t.Name, t.TAM_Prospecto__r.FWY_codigo_distribuidor__c, 
            t.TAM_Prospecto__c, t.TAM_Prospecto__r.OwnerId, t.TAM_Prospecto__r.Owner.Name From TAM_LeadInventarios__c t 
            Where Name = :sstrVinBusqPrm And TAM_Prospecto__r.FWY_codigo_distribuidor__c =: sCodDistPrm]){
            //Inicializa la variable blnExiste
            if (objLedInvt.TAM_Prospecto__r.OwnerId != strIdUsrAct){
                objWrpInventDifDealer.blnExisteDDLeadInventDifDealer = true;
                objWrpInventDifDealer.sCodDistFinal = sCodDistPrm;
                objWrpInventDifDealer.sOwnerFinal = objLedInvt.TAM_Prospecto__r.Owner.Name;
            }//Fin si objLedInvt.TAM_Prospecto__r.OwnerId != strIdUsrAct
            System.debug('EN TAM_ConsultaVinDD.consultaVinDD objLedInvt.OwnerId: ' + objLedInvt.TAM_Prospecto__r.OwnerId + ' strIdUsrAct: ' + strIdUsrAct);
        }
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD objWrpInventDifDealer: ' + objWrpInventDifDealer);
        
        return objWrpInventDifDealer;
    }
    
    //Un metodo para buscar el Vin en TAM_LeadInventarios__c 
    public static wrpInventDifDealer consultaVinInvLead(String sstrVinBusqPrm, String recordId){
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD sstrVinBusqPrm: ' + sstrVinBusqPrm + ' recordId: ' + recordId);
        
        Boolean blnExiste = false;
        wrpInventDifDealer objWrpInventDifDealer = new wrpInventDifDealer(false, recordId);
        objWrpInventDifDealer.blnExisteVin = false;
        objWrpInventDifDealer.blnMismoProp = false;
        objWrpInventDifDealer.blnMismoProspecto = false;
        
        String strIdUsrAct = UserInfo.getUserId();
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD strIdUsrAct: ' + strIdUsrAct);

        for (TAM_LeadInventarios__c objLeadInventarios : [Select id, Name, OwnerId, TAM_Prospecto__c 
            From TAM_LeadInventarios__c Where Name =:sstrVinBusqPrm LIMIT 1]){ //And TAM_Prospecto__c != :recordId                
            //Inicializa la variable blnExiste
            if ( (objLeadInventarios.TAM_Prospecto__c == recordId && objLeadInventarios.OwnerId != strIdUsrAct) || Test.isRunningTest()){
                objWrpInventDifDealer.blnExisteVin = true;
                objWrpInventDifDealer.blnMismoProspecto = true;  
                objWrpInventDifDealer.blnMismoProp = false;              
            }//Fin si objLedInvt.TAM_Prospecto__r.OwnerId != strIdUsrAct
            if ( (objLeadInventarios.TAM_Prospecto__c == recordId && objLeadInventarios.OwnerId == strIdUsrAct) || Test.isRunningTest()){
                objWrpInventDifDealer.blnExisteVin = true;
                objWrpInventDifDealer.blnMismoProspecto = true;
                objWrpInventDifDealer.blnMismoProp = true;
            }//Fin si objLedInvt.TAM_Prospecto__r.OwnerId != strIdUsrAct
        }        
        System.debug('EN TAM_ConsultaVinDD.consultaVinDD objWrpInventDifDealer: ' + objWrpInventDifDealer);
        
        return objWrpInventDifDealer;
    }    
    
    
}