/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para eliminar la colaboración de los registros 
                        que vienen de la clases TAM_AdminVisibInventarioToyota.bpDepuraColaboracionDealer.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_DepuraColaboraInventBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sDealerColabora;
    
    //Un constructor por default
    global TAM_DepuraColaboraInventBch_cls(string query, String sDealerColaboraParam){
        this.query = query;
        this.sDealerColabora = sDealerColaboraParam;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraInventBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_InventarioVehiculosToyota__c> scope){
        System.debug('EN TAM_DepuraColaboraInventBch_cls.');

        Set<String> setVodDealer = new Set<String>();
        Map<String, String> mapIdUsrNomDis = new Map<String, String>();
        Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
        Map<String, String> mapNoDealerNomGrupo = new Map<String, String>();
        List<TAM_InventarioVehiculosToyota__Share >  lInvenVehiculoShare = new List<TAM_InventarioVehiculosToyota__Share >();
        List<TAM_InventarioVehiculosToyota__Share> lInvVehToyotaDel = new List<TAM_InventarioVehiculosToyota__Share>();
        
        Set<String> setNomDealerOrigen = new Set<String>();
        Set<String> setNomDealer = new Set<String>();
        Set<String> setIdInvDel = new Set<String>();
        
        String sFleterosColaboracionEspecial = System.Label.TAM_FleterosColaboracionEspecial;
        String sCuernavacaColaboracionEspecial = System.Label.TAM_CuernavacaColaboracionEspecial;
        
        Map<String, TAM_AdminInventarioDealer__c> mapAdmInvUps = new Map<String, TAM_AdminInventarioDealer__c>();
        
        //Toma la variable de sDealerColabora y destripala para quw tomes los Dealers
        //57019, TOYOTA AGUASCALIENTES SUR;57213, TOYOTA DEL BAJÍO, TOYOTA GUNAJUATO;
        System.debug('EN TAM_DepuraColaboraInventBch_cls sDealerColabora: ' + this.sDealerColabora);        
        String[] ssDealerColabora = this.sDealerColabora.split(';');
        //Recorre la lista de ssDealerColabora y toma los Distribuidores asocuados a cada Delaer
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            setNomDealerOrigen.add(ssDealerColaboraFinal[0] + ' DistRelMod');
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setNomDealerOrigen: ' + setNomDealerOrigen);
        
        //Recorre la lista de reg que viene en el scope
        for (TAM_InventarioVehiculosToyota__c objInvVhToy : scope){
            //Ve si tiene algo el cammpo de Dealer_Code__c
            if (objInvVhToy.Dealer_Code__c != null)
                setVodDealer.add(objInvVhToy.Dealer_Code__c);
            setIdInvDel.add(objInvVhToy.id);            
        }//Fin del for para scope
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setVodDealer: ' + setVodDealer);           
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setIdInvDel: ' + setIdInvDel);           

        //Consulta los grupos 
        for (Account Dealer : [SELECT ID, Codigo_Distribuidor__c, Name 
            FROM Account WHERE Codigo_Distribuidor__c IN :setVodDealer ORDER BY NAME]){
            String sName = Dealer.Name;    
            mapNoDealerNomGrupo.put(sName.toUpperCase(), Dealer.Codigo_Distribuidor__c);
            String sNombreGrupo = Dealer.Name;
            sNombreGrupo = Dealer.Name + ' ASESOR';
            mapNoDealerNomGrupo.put(sNombreGrupo.toUpperCase(), Dealer.Codigo_Distribuidor__c);
        }
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.KeySet());           
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls mapNoDealerNomGrupo: ' + mapNoDealerNomGrupo.Values());           
                
        //Consulta los grupos 
        for (Group Grupo : [SELECT ID, Name FROM Group WHERE Name IN: mapNoDealerNomGrupo.KeySet() ORDER BY NAME]){
            String sNombreGrupo = Grupo.Name;
            mapNomGrupoIdGrupo.put(Grupo.id, sNombreGrupo);
        }
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.KeySet());           
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls mapNomGrupoIdGrupo: ' + mapNomGrupoIdGrupo.Values());           
        
        //Ahora si consulta los datos de la colaboración relacionada al inventario en setIdInvDel
        //y el grupo diferente de mapNomGrupoIdGrupo.LeySet() y el tipo de colaboración Manual 
        for (TAM_InventarioVehiculosToyota__Share objInvShare : [Select t.id, t.UserOrGroupId,  
            t.ParentId From TAM_InventarioVehiculosToyota__Share t
            Where t.RowCause = 'Manual' and UserOrGroupId != :mapNomGrupoIdGrupo.KeySet()
            and ParentId IN :setIdInvDel]){
            //Agregalos a la lista de lInvVehToyotaDel
            lInvVehToyotaDel.add(new TAM_InventarioVehiculosToyota__Share(
                    id = objInvShare.id
                )
            );
        }
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls lInvVehToyotaDel: ' + lInvVehToyotaDel);           
        
        //Cosnulta los reg asociados a setNomDealerOrigen en TAM_AdminInventarioDealer__c
        for (TAM_AdminInventarioDealer__c objAdmInvDealer : [Select id, Name, TAM_IdExterno__c 
            From TAM_AdminInventarioDealer__c Where TAM_IdExterno__c IN :setNomDealerOrigen]){
            //Agregalo al mapa de mapAdmInvUps
            mapAdmInvUps.put(objAdmInvDealer.TAM_IdExterno__c, new TAM_AdminInventarioDealer__c(
                    id = objAdmInvDealer.id,
                    TAM_Procesar__c = true,
                    TAM_Procesada__c = true
                )
            );
        }
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls lInvenVehiculoShare mapAdmInvUps: ' + mapAdmInvUps);           
        
        Boolean blnError = false;
        Set<String> setIdOkCol = new Set<String>();

        Set<String> setDealDel = new Set<String>();
        Map<String, TAM_AdminInventarioDealer__c> mapAdminInvDel = new Map<String, TAM_AdminInventarioDealer__c>();
        
        //Recorre la lista de arrslDealesDelFinalPrmFinal para crear los reg que se van a eliminar
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            System.debug('ENTRO TAM_DepuraColaboraInventBch_cls sNoDealerPaso: ' + sNoDealerPaso + ' size: ' + ssDealerColaboraFinal.size());
            if (ssDealerColaboraFinal.size() > 1)
                if (ssDealerColaboraFinal[1] == null || ssDealerColaboraFinal[1] == '')
                    setDealDel.add(ssDealerColaboraFinal[0] + ' DistRelMod');
            if (ssDealerColaboraFinal.size() == 1)
                setDealDel.add(ssDealerColaboraFinal[0] + ' DistRelMod');    
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls setNomDealerOrigen: ' + setNomDealerOrigen);

        for (TAM_AdminInventarioDealer__c objAdmInv : [Select id, TAM_IdExterno__c 
            From TAM_AdminInventarioDealer__c Where TAM_IdExterno__c IN :setDealDel]){
            //Metelo al mapa de mapAdminInvDel
            mapAdminInvDel.put(objAdmInv.TAM_IdExterno__c, new TAM_AdminInventarioDealer__c(id = objAdmInv.id));
        }
        System.debug('EN TAM_DepuraColaboraInventBch_cls mapAdminInvDel: ' + mapAdminInvDel);        
        
        //Un punto de retorno
        SavePoint svInv = Database.setSavepoint();
        
        if (!lInvVehToyotaDel.isEmpty()){
            //Actualiza las Opp 
            List<Database.DeleteResult> lDtbUpsRes = Database.delete(lInvVehToyotaDel, false);
            //Ve si hubo error
            for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de eliminar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()

        if (!mapAdmInvUps.isEmpty()){
            //Actualiza las Opp 
            List<Database.UpsertResult> lDtbUpsRes = Database.upsert(mapAdmInvUps.values(), TAM_AdminInventarioDealer__c.id, false);
            //Ve si hubo error
            for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess()){
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar el TAM_InventarioVehiculosToyota__Share de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    blnError = true;
                }//Fin si !objDtbUpsRes.isSuccess()
                if (objDtbUpsRes.isSuccess())                    
                    setIdOkCol.add(objDtbUpsRes.getId());
            }//Fin del for para lDtbUpsRes
        }//Fin si !lInvenVehiculoShare.isEmpty()
        System.debug('ENTRO TAM_DepuraColaboraInventBch_cls lInvenVehiculoShare setIdOkCol: ' + setIdOkCol);           

        //Regresa las cosas como estaban
        if (blnError)
            Database.rollback(svInv);
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_DepuraColaboraInventBch_cls.finish Hora: ' + DateTime.now());      
        System.debug('EN TAM_DepuraColaboraInventBch_cls.finish this.sDealerColabora: ' + this.sDealerColabora);      
        
        Set<String> setVodDealer = new Set<String>();
        //57019, TOYOTA AGUASCALIENTES SUR;57213, TOYOTA DEL BAJÍO, TOYOTA GUNAJUATO;
        String[] ssDealerColabora = this.sDealerColabora.split(';');
        //Recorre la lista de ssDealerColabora y toma los Distribuidores asocuados a cada Delaer
        for (String sNoDealerPaso : ssDealerColabora){
            String[] ssDealerColaboraFinal = sNoDealerPaso.split(',');
            setVodDealer.add(ssDealerColaboraFinal[0]);
        }//Fin del for para ssDealerColabora
        System.debug('ENTRO EN TAM_DepuraColaboraInventBch_cls.finish setVodDealer: ' + setVodDealer);           

        String sLDealerColaInv = '(';     
        //Recorre la lista de lClientes
        for (String sTipoInv : setVodDealer){
            sLDealerColaInv += '\'' + sTipoInv.trim() + '\',';
        }
        sLDealerColaInv = sLDealerColaInv.substring(0, sLDealerColaInv.lastIndexOf(','));
        sLDealerColaInv += ')';
        System.debug('EN EN TAM_DepuraColaboraInventBch_cls.finish sLDealerColaInv: ' + sLDealerColaInv);

        String strVinPrueba = 'JTDKDTB34M1141773';
        String sQuery = 'Select Id, Name, Dealer_Code__c ';
        sQuery += ' From TAM_InventarioVehiculosToyota__c ';
        sQuery += ' where Dealer_Code__c IN ' + sLDealerColaInv;
        sQuery += ' Order by Dealer_Code__c';
        //sQuery += ' Limit 10';

        //Si es una prueba
        if (Test.isRunningTest())
            sQuery += ' Limit 10';
        System.debug('EN EN TAM_DepuraColaboraInventBch_cls.finish sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_CreaColaboraInventBch_cls objCreaColInventBch = new TAM_CreaColaboraInventBch_cls(sQuery, this.sDealerColabora);
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objCreaColInventBch, 50);        
        
    } 
    
}