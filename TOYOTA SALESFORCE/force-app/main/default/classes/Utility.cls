public without sharing class Utility {
    public static Profile getProfileById(String idp) {
        return [SELECT Id, Name FROM Profile WHERE Id=:idp];
    }
}