public without sharing class ConsolidadoPagoTFSTriggerHandler extends TriggerHandler {
    private List<ConsolidadoPagoTFS__c> consolidadoPagoList;
    private Map<String,Map<String,RecordType>> recordTypesMap;
    
    public ConsolidadoPagoTFSTriggerHandler() {
        this.consolidadoPagoList = (List<ConsolidadoPagoTFS__c>) Trigger.new;
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
    }
    
    public override void beforeInsert(){
    	validateSadConsolidadoTFS(consolidadoPagoList);
    }
    
    public override void beforeUpdate(){
    	validateSadConsolidadoTFS(consolidadoPagoList);
    }
    
    public override void afterInsert(){
   	actualizaSadConConsolidadoMAX(consolidadoPagoList);
    actualizaSadConConsolidadoSUM(consolidadoPagoList);
    }
    
    public override void afterUpdate(){
    actualizaSadConConsolidadoMAX(consolidadoPagoList);
    actualizaSadConConsolidadoSUM(consolidadoPagoList);
    } 
    
    public void validateSadConsolidadoTFS(List<ConsolidadoPagoTFS__c> consolidados){
    	System.debug('Entra CONSOLIDADO, validación de vin y solicitud');
    	Set<String> vinID = new Set<String>();
    	Set<Id> sadID = new Set<Id>();
    	for(ConsolidadoPagoTFS__c cp:consolidados){
    		if(cp.VIN__c!=null){
    			vinID.add(cp.VIN__c);
    		}
    		if(cp.SerialNumber__c!=null){
    			sadID.add(cp.SerialNumber__c);
    		}
    	}
    	
    	Map<String,SolicitudAutoDemo__c> sadMap = new Map<String,SolicitudAutoDemo__c>();
    	for(SolicitudAutoDemo__c sad:[SELECT Id, Estatus__c, FechaBajaPagoCompletado__c, IDExternoAutoDemo__c,
    									SumatoriaTransacciones__c, OriginalPrincipalAmount__c, ValidacionBajaTMEX__c FROM SolicitudAutoDemo__c 
    									WHERE IDExternoAutoDemo__c IN:vinID]){
    		sadMap.put(sad.IDExternoAutoDemo__c,sad);
    	}
    	
    	Map<Id,SolicitudAutoDemo__c> sadMapID = new Map<Id,SolicitudAutoDemo__c>();
    	for(SolicitudAutoDemo__c sad:[SELECT Id, Estatus__c, FechaBajaPagoCompletado__c, IDExternoAutoDemo__c,
    									SumatoriaTransacciones__c, OriginalPrincipalAmount__c, ValidacionBajaTMEX__c FROM SolicitudAutoDemo__c 
    									WHERE Id IN:sadID]){
    		sadMapID.put(sad.Id,sad);
    	}
    	
    	List<SolicitudAutoDemo__c> solicitudesUpd =  new List<SolicitudAutoDemo__c>();
    	
    	for(ConsolidadoPagoTFS__c cp : consolidados){
    		if(cp.VIN__c==null){
    			System.debug('No han elegido un Vin' +cp.VIN__c);
    			cp.addError('Debes asignar un numero de vin al consolidado');
    			if(cp.SerialNumber__c!=null){
    				System.debug('Se eligió solicitud');
    				cp.addError('Debes elegir un VIN para validar la solicitud elegida');
    			}
    		}else{
    			System.debug('Se eligió un Vin' +cp.VIN__c);
				if(!sadMap.containsKey(cp.VIN__c)){
					cp.addError('No se encontro solicitud con ese VIN');
				}else{
					cp.SerialNumber__c=sadMap.get(cp.VIN__c).Id;
					System.debug('Si existe SA, el consolidado se guarda');
					if(cp.TransactionAmount__c>cp.OriginalPrincipalAmount__c){
						cp.addError('El monto de la transacción no puede ser mayor al monto original');
					}
    			}
    		}
    	}
}
    
    public void actualizaSadConConsolidadoMAX(List<ConsolidadoPagoTFS__c> consolidados){
    	System.debug('Entra consolidado MAX');
    	Set<String> cpVIN = new Set<String>();
    	Set<Id> cpSadID = new Set<Id>();
    	for(ConsolidadoPagoTFS__c cp:consolidados){
    	    if(cp.VIN__c!=null){
    			cpVIN.add(cp.VIN__c);
    		}
    	} 
    	
    	Map<Id,SolicitudAutoDemo__c> mapSadUpd = new Map<Id,SolicitudAutoDemo__c>(); 
    	for(SolicitudAutoDemo__c sad:[SELECT Id, (Select Id,TransactionAmount__c, OriginalPrincipalAmount__c, EffectiveDate__c, 
    									SerialNumber__c From ConsolidadoPagosTFS__r)
    									FROM SolicitudAutoDemo__c WHERE IDExternoAutoDemo__c IN:cpVIN]){
    									System.debug('SAD: '+sad);
    		if(sad.ConsolidadoPagosTFS__r.size()>0){
    			System.debug('La solicitud si tiene consolidados:' +sad.ConsolidadoPagosTFS__r.size());
    			for(ConsolidadoPagoTFS__c consolidado : sad.ConsolidadoPagosTFS__r){
    				System.debug('Lista relacionada de cps en la solicitud: ' +sad.ConsolidadoPagosTFS__r);
					cpSadID.add(consolidado.SerialNumber__c);
    			}
    			System.debug('Set de ID de sad(serial# de cp): ' +cpSadID);
    		}
    	}
    	
        for(AggregateResult ar : [SELECT SerialNumber__c, MAX(OriginalPrincipalAmount__c) maxOriginal FROM ConsolidadoPagoTFS__c
    								WHERE SerialNumber__c IN:cpSadID GROUP BY SerialNumber__c]){
    		SolicitudAutoDemo__c sadUpd = new SolicitudAutoDemo__c(
    		Id = (Id)ar.get('SerialNumber__c'),OriginalPrincipalAmount__c = (Decimal)ar.get('maxOriginal')
    		);
    		mapSadUpd.put(sadUpd.Id, sadUpd);
    	}
    	System.debug('Mapa de sad Upd: '+mapSadUpd);
    	try{
    		upsert mapSadUpd.values();
    		System.debug('Valores a actualizar del mapa: '+mapSadUpd.values());
    		System.debug('SE ACTUALIZO LA SOLICITUD MAX');
    	}catch(Exception ex){
    		System.debug('ERROR: '+ex.getMessage());
    	}    
    }  
    
        public void actualizaSadConConsolidadoSUM(List<ConsolidadoPagoTFS__c> consolidados){
    	System.debug('Entra consolidado SUM');
    	Set<String> cpVIN = new Set<String>();
    	Set<Id> cpSadID = new Set<Id>();
    	for(ConsolidadoPagoTFS__c cp:consolidados){
    	    if(cp.VIN__c!=null){
    			cpVIN.add(cp.VIN__c);
    		}
    	} 
    	
    	Map<Id,SolicitudAutoDemo__c> mapSadUpd = new Map<Id,SolicitudAutoDemo__c>(); 
    	for(SolicitudAutoDemo__c sad:[SELECT Id, (Select Id,TransactionAmount__c, OriginalPrincipalAmount__c, EffectiveDate__c, 
    									SerialNumber__c From ConsolidadoPagosTFS__r)
    									FROM SolicitudAutoDemo__c WHERE IDExternoAutoDemo__c IN:cpVIN]){
    									System.debug('SAD: '+sad);
    		if(sad.ConsolidadoPagosTFS__r.size()>0){
    			System.debug('La solicitud si tiene consolidados: ' +sad.ConsolidadoPagosTFS__r.size());
    			for(ConsolidadoPagoTFS__c consolidado : sad.ConsolidadoPagosTFS__r){
    				System.debug('lista relacionada a la colicitud: ' +sad.ConsolidadoPagosTFS__r);
					cpSadID.add(consolidado.SerialNumber__c);
    			}
    			System.debug('Set de ID de sad(serial# de cp): ' +cpSadID);
    		}
    	}

    	for(AggregateResult ar : [SELECT SerialNumber__c, SUM(TransactionAmount__c) amount FROM ConsolidadoPagoTFS__c
    								WHERE SerialNumber__c IN:cpSadID GROUP BY SerialNumber__c]){
    		SolicitudAutoDemo__c sadUpd = new SolicitudAutoDemo__c(
    		Id = (Id)ar.get('SerialNumber__c'),Sumatoriatransacciones__c = (Decimal)ar.get('amount') 
    		);
    		mapSadUpd.put(sadUpd.Id, sadUpd);
    	}   	
    	System.debug('Mapa de sad Upd: '+mapSadUpd);
    	try{
    		upsert mapSadUpd.values();
    		System.debug('Valores a actualizar del mapa: '+mapSadUpd.values());
    		System.debug('SE ACTUALIZO LA SOLICITUD SUM');
    	}catch(Exception ex){
    		System.debug('ERROR: '+ex.getMessage());
    		for(ConsolidadoPagoTFS__c cp:consolidados){
    	    	cp.addError('No es posible exceder el límite del valor de Original Principal Amount de la solicitud');
    		} 
    	}    
    }  
}