public without sharing class TAM_DuplicarPolitica {
    
    //Variables publicas
    public static List<TAM_DetallePoliticaJunction__c> lstDetalleOriginal{get;set;}
    public static Set<String> setClaveModelosClon{get;set;}
    
        
    @AuraEnabled
    public static TAM_PoliticasIncentivos__c politica(String recordId){
        TAM_PoliticasIncentivos__c objPolitica = [SELECT Name,TAM_InicioVigencia__c,TAM_FinVigencia__c,TAM_No_Boletin__c,
                                                  TAM_PeriodoDeGracia__c,RecordTypeId, RecordType.Name
                                                  FROM TAM_PoliticasIncentivos__c WHERE id=: recordId];
        return objPolitica;
    }
    
    //Consultar Detalle Politica Juncion "Original"
    @AuraEnabled
    public static List<TAM_DetallePoliticaJunction__c> getDetalleOriginal(String recordId){
        lstDetalleOriginal = [SELECT  TAM_AnioModelo__c,
                              TAM_Serie__c,
                              TAM_Version__c,
                              TAM_Clave__c,
                              TAM_Inventario_Dealer__c,
                              TAM_Dias_de_Inventario__c,
                              TAM_Venta_MY__c,
                              TAM_BalanceOut__c,
                              TAM_Pago_TFS__c,
                              TAM_IncPropuesto_Cash__c,
                              TAM_IncPropuesto_Porcentaje__c,
                              TAM_IncentivoTMEX_Cash__c,
                              TAM_IncentivoTMEX_Porcentaje__c,
                              TAM_IncDealer_Cash__c,
                              TAM_IncDealer_Porcentaje__c,
                              TAM_Tripleplay__c,
                              TAM_ProductoFinanciero__c,
                              TAM_Descripcion__c,
                              TAM_TotalEstimado__c,
                              TAM_TP_IncTMEX_Cash__c,
                              TAM_TP_IncTMEX_Porcentaje__c,
                              TAM_TP_IncDealer_Porcentaje__c,
                              TAM_TP_IncDealer_Cash__c,
                              TAM_IncTFS_Cash__c,
                              TAM_IncTFS_Porcentaje__c,
                              TAM_MSRP__c,
                              TAM_Duplicar__c,
                              TAM_TMEX_Margen_Pesos__c,
                              TAM_TMEX_Margen_Porcentaje__c,
                              TAM_Consecutivo__c,
                              TAM_Rango__c,
                              TAM_Rango__r.Name,
                              TAM_FechaInventario__c,
                              TAM_Lanzamiento__c,
                              TAM_MotivoIncentivo__c,
                              TAM_Incentivo_actual__c,
                              TAM_AplicanAmbosIncentivos__c,
                              TAM_IdExterno__c
                              FROM	  TAM_DetallePoliticaJunction__c
                              WHERE	  TAM_PoliticaIncentivos__c =:recordId
                              AND	 (TAM_IncPropuesto_Cash__c != null OR TAM_IncPropuesto_Porcentaje__c != null)
                              ORDER BY  TAM_Serie__c ASC,TAM_Clave__c ASC, TAM_AnioModelo__c DESC];
        
        for(TAM_DetallePoliticaJunction__c objDetalle : lstDetalleOriginal){
            objDetalle.TAM_DuplicarDetalle__c = true;
        }
        return lstDetalleOriginal;
    }
    
    //Clonar detalles
    @AuraEnabled
    public static String clonarDetalle(List<TAM_DetallePoliticaJunction__c> lstDetallesSeleccionados, TAM_PoliticasIncentivos__c objPoliticaOriginal){
        
        //Clonar detalle.
        List<TAM_DetallePoliticaJunction__c> lstClon = new List<TAM_DetallePoliticaJunction__c>();
        setClaveModelosClon = new Set<String>();
        for(TAM_DetallePoliticaJunction__c objSeleccionado : lstDetallesSeleccionados ){
            TAM_DetallePoliticaJunction__c objClon = objSeleccionado.clone(false,true,false,false);
            lstClon.add(objClon);
            String clonAnioClave = objClon.TAM_Clave__c + objClon.TAM_AnioModelo__c;
            setClaveModelosClon.add(clonAnioClave);
        }
        
        //Crear una nueva politica de incentivos
        TAM_PoliticasIncentivos__c objPoliticaClon = new TAM_PoliticasIncentivos__c();
        objPoliticaClon.Name = objPoliticaOriginal.Name;
        objPoliticaClon.RecordTypeId = objPoliticaOriginal.RecordTypeId;
		objPoliticaClon.TAM_InicioVigencia__c = objPoliticaOriginal.TAM_InicioVigencia__c;
        objPoliticaClon.TAM_FinVigencia__c = objPoliticaOriginal.TAM_FinVigencia__c;
        objPoliticaClon.TAM_PeriodoDeGracia__c = objPoliticaOriginal.TAM_PeriodoDeGracia__c;
        objPoliticaClon.TAM_EstatusIncentivos__c = 'En proceso';
        insert objPoliticaClon;
        String strRecordTypeName = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosById().get(objPoliticaOriginal.RecordTypeId).getname();

        String recordId = objPoliticaClon.Id;
        
        //Upsert sobre registros clonados
        List<TAM_DetallePoliticaJunction__c> lstDetalleInsertado = [SELECT  TAM_Clave__c,
                                                                   			TAM_Inventario_Dealer__c,
                                                                   			TAM_Dias_de_Inventario__c,
                                                                   			TAM_AnioModelo__c,
                                                                   			TAM_MSRP__c,
                                                                   			TAM_Rango__c,
                                                                   			TAM_TMEX_Margen_Pesos__c,
                                                                   			TAM_TMEX_Margen_Porcentaje__c,
                                                                   			
                                                                   			TAM_IdExterno__c,
                                                                            Id,
                                                                            TAM_IncPropuesto_Cash__c,
                                                                            TAM_IncPropuesto_Porcentaje__c
                                                                   FROM		TAM_DetallePoliticaJunction__c
                                                                   WHERE 	TAM_PoliticaIncentivos__c =:recordId];
        
        Map<String, List<TAM_DetallePoliticaJunction__c>> mapDetalleInsertado = new Map<String, List<TAM_DetallePoliticaJunction__c>>();
        List<TAM_DetallePoliticaJunction__c> lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
        Set<String> setAniosModelo = new Set<String>();
        for(TAM_DetallePoliticaJunction__c objDetalleJunction: lstDetalleInsertado){
            
            //Set de años modelo
            setAniosModelo.add(objDetalleJunction.TAM_AnioModelo__c);

            //Llenado de mapa detalle insertado
            String strClaveAnio = objDetalleJunction.TAM_Clave__c + objDetalleJunction.TAM_AnioModelo__c;
            lstDetalleAux = mapDetalleInsertado.get(strClaveAnio);
            
            if(mapDetalleInsertado.containsKey(strClaveAnio)){
                
                if(!lstDetalleAux.contains(objDetalleJunction)){
                    
                    lstDetalleAux.add(objDetalleJunction);
                    mapDetalleInsertado.put(strClaveAnio, lstDetalleAux);
                }
            } else {
                lstDetalleAux = new List<TAM_DetallePoliticaJunction__c>();
                lstDetalleAux.add(objDetalleJunction);
                mapDetalleInsertado.put(strClaveAnio, lstDetalleAux);
            }
        }
       



        //Añadir insentivo a registros de año modelo nuevos
        List<String> lstAnioModelo = new List<String>();
        lstAnioModelo.addAll(setAniosModelo);
        lstAnioModelo.sort();
        String strAnioModeloNuevo = lstAnioModelo[lstAnioModelo.size() -1];
        Integer intAnioAnterior = Integer.valueOf(strAnioModeloNuevo);
        intAnioAnterior = intAnioAnterior -1;
        String strAnioAnterior = String.valueOf(intAnioAnterior);
        
        //Match
        List<TAM_DetallePoliticaJunction__c> lstUpdateClones = new List<TAM_DetallePoliticaJunction__c>();
        List<TAM_DetallePoliticaJunction__c> lstRegistrosBorrar = new List<TAM_DetallePoliticaJunction__c>();
        
        for(TAM_DetallePoliticaJunction__c objClon : lstClon){
            String clonAnioClave = objClon.TAM_Clave__c + objClon.TAM_AnioModelo__c;
            String llaveNuevoModelo = objClon.TAM_Clave__c + strAnioModeloNuevo;
            

            //Clonar registros Existentes
            if(objClon.TAM_DuplicarDetalle__c == true && mapDetalleInsertado.containsKey(clonAnioClave)){
                objClon.TAM_Dias_de_Inventario__c = mapDetalleInsertado.get(clonAnioClave)[0].TAM_Dias_de_Inventario__c;
                objClon.TAM_Inventario_Dealer__c = mapDetalleInsertado.get(clonAnioClave)[0].TAM_Inventario_Dealer__c;
                objClon.TAM_MSRP__c = mapDetalleInsertado.get(clonAnioClave)[0].TAM_MSRP__c;
                objClon.TAM_TMEX_Margen_Pesos__c = mapDetalleInsertado.get(clonAnioClave)[0].TAM_TMEX_Margen_Pesos__c;
                objClon.TAM_TMEX_Margen_Porcentaje__c = mapDetalleInsertado.get(clonAnioClave)[0].TAM_TMEX_Margen_Porcentaje__c;
                
                String idPoliticaClon = objPoliticaOriginal.Id;
                String ExternalIdAux = objClon.TAM_IdExterno__c.remove(idPoliticaClon);
                objClon.TAM_IdExterno__c = recordId + ExternalIdAux;
                objClon.TAM_PoliticaIncentivos__c = recordId;
                lstUpdateClones.add(objClon);
                if(!lstRegistrosBorrar.contains(mapDetalleInsertado.get(clonAnioClave)[0])){
                    lstRegistrosBorrar.add(mapDetalleInsertado.get(clonAnioClave)[0]);
                }
            }

            //Validar incentivos y añadirlos a nuevos año modelo
            if(strRecordTypeName == 'Venta Corporativa'){
                if(objClon.TAM_DuplicarDetalle__c == true && objClon.TAM_AnioModelo__c == strAnioAnterior  && mapDetalleInsertado.containsKey(llaveNuevoModelo)){
                    if(!setClaveModelosClon.contains(llaveNuevoModelo)){
                        TAM_DetallePoliticaJunction__c objClonNuevo = objClon.clone(false,true,false,false);
                        objClonNuevo.TAM_AnioModelo__c = strAnioModeloNuevo;
        
                        objClonNuevo.TAM_Dias_de_Inventario__c = mapDetalleInsertado.get(llaveNuevoModelo)[0].TAM_Dias_de_Inventario__c;
                        objClonNuevo.TAM_Inventario_Dealer__c = mapDetalleInsertado.get(llaveNuevoModelo)[0].TAM_Inventario_Dealer__c;
                        objClonNuevo.TAM_MSRP__c = mapDetalleInsertado.get(llaveNuevoModelo)[0].TAM_MSRP__c;
                        objClonNuevo.TAM_TMEX_Margen_Pesos__c = mapDetalleInsertado.get(llaveNuevoModelo)[0].TAM_TMEX_Margen_Pesos__c;
                        objClonNuevo.TAM_TMEX_Margen_Porcentaje__c = mapDetalleInsertado.get(llaveNuevoModelo)[0].TAM_TMEX_Margen_Porcentaje__c;
                        
                        String idPoliticaClon = objPoliticaOriginal.Id;
                        String ExternalIdAux = objClonNuevo.TAM_IdExterno__c.remove(idPoliticaClon);
                        objClonNuevo.TAM_IdExterno__c = recordId + ExternalIdAux;
                        objClonNuevo.TAM_PoliticaIncentivos__c = recordId;
                        lstUpdateClones.add(objClonNuevo);
                        if(!lstRegistrosBorrar.contains(mapDetalleInsertado.get(llaveNuevoModelo)[0])){
                            lstRegistrosBorrar.add(mapDetalleInsertado.get(llaveNuevoModelo)[0]);
                        }
                    }
                }
            }
            
        }
        try {
            Database.upsert(lstUpdateClones, TAM_DetallePoliticaJunction__c.Id, false);
            delete lstRegistrosBorrar;
            return recordId; 
        } catch (DmlException e) {
            System.debug(e.getMessage());
            return null; 
        }
    }

    
}