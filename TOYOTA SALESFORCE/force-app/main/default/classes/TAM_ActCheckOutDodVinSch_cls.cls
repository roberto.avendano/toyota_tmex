/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DODSolicitudesPedidoEspecial__c y actualiza el status a Pendiente 
                        y despues a Asignada.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    31-May-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActCheckOutDodVinSch_cls implements Schedulable {

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActCheckOutDodVinSch_cls.execute...');
        String strEstatusDod = 'Asignado';

        this.sQuery = 'Select Id, Name, TAM_VIN__c, TAM_Estatus__c, TAM_ActRegCheckOut__c, TAM_TieneCheckout__c ';
        this.sQuery += ' From TAM_DODSolicitudesPedidoEspecial__c ';
        this.sQuery += ' where TAM_VIN__c != null And TAM_Historico__c = false And (TAM_TieneCheckout__c = false OR TAM_TipoInventario__c != \'H - Reportado\')';
        this.sQuery += ' And TAM_Estatus__c = \'' + String.escapeSingleQuotes(strEstatusDod) + '\'';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActCheckOutDodVinSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActCheckOutDodVinBch_cls objActCheckOutDodVinBch = new TAM_ActCheckOutDodVinBch_cls(sQuery);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActCheckOutDodVinBch, 10);
                     
    }
    
}