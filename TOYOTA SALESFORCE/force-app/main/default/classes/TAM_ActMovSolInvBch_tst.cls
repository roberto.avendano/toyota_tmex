/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_ActFechaDDChkOutBch_cls.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Julio-2021        Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActMovSolInvBch_tst {

    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    
    static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
    static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
    
    static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();

    static String sRectorTypePasoDistFlotilla = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String sRectorTypePasoDistPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    static String sRectorTypePasoVinesFlotilla = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String sRectorTypePasoVinesPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

    static String sRectorTypePoliticaIncentivos = Schema.SObjectType.TAM_PoliticasIncentivos__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();

    static String sRectorTypeFlotillaRep = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Pedido Especial Rep').getRecordTypeId();    
    static String sRectorTypePasoFlotillaPE = Schema.SObjectType.TAM_CheckOutDetalleSolicitudCompra__c.getRecordTypeInfosByName().get('Flotilla (Pedido Especial)').getRecordTypeId();
    static String strRecordTypeIdMov = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        
    static String sListaPreciosCutom = getCustomPriceBookList();
    
    static  Account clienteDealer = new Account();
    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod = new CatalogoCentralizadoModelos__c();
    static  CatalogoCentralizadoModelos__c CatalogoCentMod2 = new CatalogoCentralizadoModelos__c();
    static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
    static  Rangos__c rango = new Rangos__c();
    static  Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c();
    static  InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c();
    static  TAM_VinesFlotillaPrograma__c TAMVinesFlotillaPrograma = new TAM_VinesFlotillaPrograma__c();
    
    @TestSetup static void loadData(){
        
        Group grupoPrueba = new Group(
            Name = 'CARSON',
            Type = 'Regular'
        );
        insert grupoPrueba;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoFlotilla
             
        );
        insert rangoFlotilla;
        
        Account clienteDealer = new Account(
            Name = 'TOYOTA PRUEBA',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoDistribuidor,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        //insert clienteDealer;

        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
            LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaFisica,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        //insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  

        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI',
            Id_Externo__c = 'AVANZA-22060-2020-B79-10'
        );
        insert catCenMod;  
        CatalogoCentMod = catCenMod; 
        
        CatalogoCentralizadoModelos__c catCenMod2 = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '11',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI',
            Id_Externo__c = 'AVANZA-22060-2020-B79-12'
        );
        insert catCenMod2;  
        CatalogoCentMod2 = catCenMod2;
        
        Id standardPricebookId = Test.getStandardPricebookId();
        Product2 ProducStdPriceBook = new Product2(
                Name = '22060',
                Anio__c = '2020', 
                NombreVersion__c = 'LE MT',
                IdExternoProducto__c = '220602020',
                ProductCode = '220602020',
                Description= 'AVANZA-22060-2020-B79-10-LE AT', 
                RecordTypeId = sRectorTypePasoProductoUnidad,
                Family = 'Toyota',
                IsActive = true
        );
        insert ProducStdPriceBook;
        
        PricebookEntry pbeStandard = new PricebookEntry(
            Pricebook2Id = standardPricebookId,
            UnitPrice = 0.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true,
            IdExterno__c = '220602020'
        );
        insert pbeStandard;

        Pricebook2 customPB = new Pricebook2(
            Name = sListaPreciosCutom,
            IdExternoListaPrecios__c = sListaPreciosCutom, 
            isActive = true
        );
        insert customPB;
        
        PricebookEntry pbeCustomProceBookEntry = new PricebookEntry(
            Pricebook2Id = customPB.id,
            UnitPrice = 1.0,
            Product2Id = ProducStdPriceBook.Id,         
            IsActive = true
        );
        insert pbeCustomProceBookEntry;

        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = sRectorTypePasoSolVentaCorporativa,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaCierreSolicitud__c = Date.today()
        );
        insert solVentaFlotillaPrograma;

        TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoPedidoEspecial,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = true,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Cantidad__c = 10
        );
        insert detSolVentaFlotillaPrograma;

        TAM_DetalleOrdenCompra__c detSolVentaFlotillaPrograma2 = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoPedidoEspecial,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = false,
            TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            TAM_Cantidad__c = 10
        );
        insert detSolVentaFlotillaPrograma2;

        TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra = new TAM_DistribuidoresFlotillaPrograma__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoDistFlotilla,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-10' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_Estatus__c = 'Pendiente',
            TAM_Cantidad__c = 10,
            TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma.id,
            TAM_Cuenta__c = clienteMoral.id,
            TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA',
            TAM_IdDistribuidor__c = '570550'        
        );
        insert detSolDistFltoProgra;

        TAM_DistribuidoresFlotillaPrograma__c detSolDistFltoProgra2 = new TAM_DistribuidoresFlotillaPrograma__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoDistFlotilla,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-11' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_Estatus__c = 'Pendiente',
            TAM_Cantidad__c = 10,
            TAM_DetalleSolicitudCompra_FLOTILLA__c = detSolVentaFlotillaPrograma2.id,
            TAM_Cuenta__c = clienteMoral.id,
            TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA',
            TAM_IdDistribuidor__c = '570550'        
        );
        insert detSolDistFltoProgra2;
                
        TAM_DetalleOrdenCompra__c detSolVentaFlotillaProgramaInv = new TAM_DetalleOrdenCompra__c(
            Name = '2020-AVANZA-LE AT-22060',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypePasoInventario,
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-12' + '-' + sRectorTypePasoPedidoEspecial,
            TAM_FechaSolicitud__c = Date.today(),
            TAM_EntregarEnMiDistribuidora__c = true,
            TAM_Cantidad__c = 10
            //TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
        );
        insert detSolVentaFlotillaProgramaInv;
                        
        InventarioWholesale__c objInvWholeSale = new InventarioWholesale__c(
                  Name = 'XXXXXXXXXXX',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvWholeSale;
        
        Inventario_en_Transito__c objInvTransito = new Inventario_en_Transito__c(
                  Name = 'XXXXXXXXXX1',
                  Dealer_Code__c = '570550',
                  Interior_Color_Description__c = 'Azul', 
                  Exterior_Color_Description__c = 'Gris',
                  Model_Number__c = '22060',
                  Model_Year__c = '2020', 
                  Toms_Series_Name__c = 'AVANZA',
                  Exterior_Color_Code__c = '00B79', 
                  Interior_Color_Code__c =  '010',
                  Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvTransito;
        
        TAM_VinesFlotillaPrograma__c objTAMVinesFlotillaPrograma3 = new TAM_VinesFlotillaPrograma__c(
          Name = 'XXXXXXXXXX1', 
          TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-2020-AVANZA-22060-B79-10-JTDBBRBE1LJ018487-' + sRectorTypePasoVinesFlotilla, 
          TAM_Estatus__c = 'Cerrada',
          TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.ID
        );  
        insert objTAMVinesFlotillaPrograma3;            
        
        TAM_PoliticasIncentivos__c objTAMPoliticasIncentivos = new TAM_PoliticasIncentivos__c(
            Name = 'ABIRL 2100 - RETAIL' ,
            TAM_InicioVigencia__c = Date.today(),
            TAM_FinVigencia__c = Date.today().addDays(10)
        );
        insert objTAMPoliticasIncentivos;
        
        TAM_DetallePolitica__c objTAMDetallePolitica = new TAM_DetallePolitica__c(
              TAM_AnioModelo__c = '2020',
              TAM_Serie__c = 'AVANZA',
              TAM_Version__c = 'LE AT',
              TAM_Pago_TFS__c = false,
              TAM_IncTFS_Porcentaje__c = 1.00,
              TAM_IncTFS_Cash__c = 1.00,
              TAM_IncDealer_Cash__c = 1.00,
              TAM_IncDealer_Porcentaje__c = 1.00,
              TAM_Clave__c = '220602020',
              TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id
        );        
        insert objTAMDetallePolitica;  

        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutDetSolComp = new TAM_CheckOutDetalleSolicitudCompra__c(
            Name = 'AVANZA-22060-2020-B79-12-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-12'  + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoFlotillaPE + '-Arrendadora-0',
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_TipoPago__c = 'Arrendadora',
            TAM_TipoVenta__c = 'Inventario',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            RecordTypeId = sRectorTypeFlotillaRep,
            TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id,
            TAM_Intercambio__c = false
            //TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
        );
        insert objCheckOutDetSolComp;

        TAM_DODSolicitudesPedidoEspecial__c objDodSolPedEspPaso = new TAM_DODSolicitudesPedidoEspecial__c(
            Name = 'AVANZA-22060-2020-B79-12-LE AT',
            TAM_IdExterno__c = solVentaFlotillaPrograma.id + '-' + '2020-AVANZA-22060-B79-12'  + '-' + sRectorTypeFlotillaRep  + '-' +  sRectorTypePasoFlotillaPE + '-Arrendadora-0',
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id,
            TAM_PoliticaIncentivos__c = objTAMPoliticasIncentivos.id
            //TAM_CatalogoCentralizadoModelos__c = catCenMod.id,
            //RecordTypeId = sRectorTypeFlotillaRep
            //TAM_Intercambio__c = false
            //TAM_TipoPago__c = 'Arrendadora',
            //TAM_TipoVenta__c = 'Inventario',
        );
        insert objDodSolPedEspPaso;
        
        Vehiculo__c objVehiculo = new Vehiculo__c(
            Name = 'XXXXXXXXXX1',
            TAM_SERIAL__c = 'KS986899',
            Id_Externo__c = 'XXXXXX1KS986899'
        );
        insert objVehiculo;
        
        DateTime dtFechaActual = Datetime.now();
        DateTime dtFechaActualFinal = dtFechaActual.addDays(-30);

        TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
            Name = 'XXXXXXXXXX1-' + dtFechaActual.date(),
            TAM_VIN__c = 'XXXXXXXXXX1',
            TAM_Total__c = 0,
            TAM_FechaMovimiento__c = dtFechaActual.date(),
            TAM_EstatusSolicitud__c = 'Autorizada',
            TAM_IdExternoSFDC__c = 'XXXXXXXXXX1-SOL-000002722-' + dtFechaActualFinal.date() + '0121Y000001B55sQAC'
        );
        insert objMovSol;
        
        Movimiento__c objMovimiento = new Movimiento__c(
            VIN__c = objVehiculo.Id,
            Distribuidor__c = clienteMoral.id,
            Trans_Type__c = 'RDR',
            Submitted_Date__c = dtFechaActual,
            Submitted_Time__c = '15.42.13.872359',
            Id_Externo__c = 'XXXXXXXXXX1-' + dtFechaActualFinal.date() + '-15.42.13.872359', //JTDKARFU7L3124760-01/04/2021-15.42.11.872359
            RecordTypeId = strRecordTypeIdMov,
            Fleet__c = 'N'
        );
        insert objMovimiento;

        Movimiento__c objMovimiento2 = new Movimiento__c(
            VIN__c = objVehiculo.Id,
            Distribuidor__c = clienteMoral.id,
            Trans_Type__c = 'RVRSL',
            Submitted_Date__c = dtFechaActual,
            Submitted_Time__c = '15.42.11.872359',
            Id_Externo__c = 'XXXXXXXXXX1-' + dtFechaActualFinal.date() + '-15.42.11.872359', //JTDKARFU7L3124760-01/04/2021-15.42.11.872359
            RecordTypeId = strRecordTypeIdMov,
            Fleet__c = 'N'
        );
        insert objMovimiento2;

        Movimiento__c objMovimiento3 = new Movimiento__c(
            VIN__c = objVehiculo.Id,
            Distribuidor__c = clienteMoral.id,
            Trans_Type__c = 'RDR',
            Submitted_Date__c = dtFechaActual,
            Submitted_Time__c = '15.42.11.872359',
            Id_Externo__c = 'XXXXXXXXXX1-' + dtFechaActualFinal.date() + '-15.42.11.872360', //JTDKARFU7L3124760-01/04/2021-15.42.11.872359
            RecordTypeId = strRecordTypeIdMov,
            Fleet__c = 'N'
        );
        insert objMovimiento3;
                    
    }

    public static String getCustomPriceBookList(){
         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;
        
        return strNombreCatalogoPrecios;        
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_ActMovSolInvBchOK() {
        //Consulta los datos del cliente
        TAM_CheckOutDetalleSolicitudCompra__c objCheckOutCons = [Select Id, TAM_VIN__c, TAM_FechaVentaDD__c  From TAM_CheckOutDetalleSolicitudCompra__c LIMIT 1];        
        System.debug('EN TAM_ActMovSolInvBchOK objCheckOutCons: ' + objCheckOutCons);
        
        Test.startTest();

            String sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
            sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name ';
            sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
            sQuery += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
            //sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
            //sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= 2021-02-08';
            sQuery += ' Order by TAM_VIN__c';
            sQuery += ' Limit 1';           
            System.debug('EN TAM_ActMovSolInvBchOK.execute sQuery: ' + sQuery);

            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActMovSolInvBch_cls objActMovSolInvBch = new TAM_ActMovSolInvBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActMovSolInvBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_ActMovSolInvSch_cls ActMovSolInvSch_Jobh = new TAM_ActMovSolInvSch_cls();
            System.schedule('Ejecuta_TAM_ActMovSolInvBch_cls', sch, ActMovSolInvSch_Jobh);
            
            
            
            //PARA EL PROCESO DE TAM_ActCancelSolInvBch_cls
		    sQuery= '';
		    Boolean blnCanSolMesAntPrm = false;
		    Date dtFechaIniConsPrm = Date.today();
		    Date dtFechaFinConsPrm = Date.today();
            
	        String strTipoPedido = 'Inventario';
	        String strVinPrueba = '5TDGZRAH0MS049120';
	        String EstatusDOD = 'Cancelado';
	        String EstDealerSol = 'Cancelada';
	        String EstDealerSol2 = 'Rechazada';
	
	        //Date dFechaConsulta = Date.newInstance(2021,07,01);//Date.today();
	        Date dFechaConsulta = Date.today();
	        Date dFechaFinCal = dFechaConsulta.addDays(-1);
	        System.debug('EN TAM_ActCancelSolInvSch_cls.execute dFechaFinCal: ' + dFechaFinCal);
	        
	        blnCanSolMesAntPrm = false;
	        dtFechaIniConsPrm = Date.today();
	        dtFechaFinConsPrm = Date.today();
	        
	        //Consulta los datos del calendario donde le fecha de fin del calendario coincida con: 
	        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
	            From TAM_CalendarioToyota__c t Where TAM_FechaFin__c =:dFechaFinCal]){
	            //Coincide la fecha del calendario Toyota
	            blnCanSolMesAntPrm = true;
	            dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
	            dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
	            System.debug('EN TAM_ActCancelSolInvSch_cls.execute blnCanSolMesAntPrm: ' + blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
	        }
	        
	        //Si no es el fin del calendario del mes entonces toma la fecha de hoy y consulta
	        if (!blnCanSolMesAntPrm){
	            dFechaFinCal = Date.today();
	            for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
	                From TAM_CalendarioToyota__c t Where TAM_FechaInicio__c <=:dFechaFinCal
	                And TAM_FechaFin__c >=:dFechaFinCal]){
	                //Coincide la fecha del calendario Toyota
	                dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
	                dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
	                System.debug('EN TAM_ActCancelSolInvSch_cls.execute blnCanSolMesAntPrm: ' + blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
	            }//Fin del for para TAM_CalendarioToyota__c
	        }//Fin si 
	        
	        //Una prueba
	        if (Test.isRunningTest()){
	             dtFechaIniConsPrm = Date.today();
	             dtFechaFinConsPrm = Date.today();
	             blnCanSolMesAntPrm = true;
	             System.debug('EN TAM_ActCancelSolInvSch_cls.execute blnCanSolMesAntPrm2: ' + blnCanSolMesAntPrm + ' dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
	        }//Fin si Test.isRunningTest()
	        
	        sQuery = 'Select Id, TAM_TipoVenta__c, TAM_VIN__c, TAM_SolicitudFlotillaPrograma__c, TAM_EstatusDOD__c, TAM_EstatusDealerSolicitud__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, ';
	        sQuery += ' TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitud__c, TAM_FechaCreacionFormula__c, TAM_FechaCancelacion__c, TAM_SolicitudFlotillaPrograma__r.Name, TAM_NombreDistribuidor__c ';
	        sQuery += ' From TAM_CheckOutDetalleSolicitudCompra__c ';
	        sQuery += ' where TAM_VIN__c != null and TAM_SolicitudFlotillaPrograma__c != null ';
	        
	        //sQuery += ' And TAM_VIN__c = \'' + String.escapeSingleQuotes(strVinPrueba) + '\'';
	        //sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c >= ' + String.valueOf(dtFechaIniConsPrm);
	        //sQuery += ' And TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c <= ' + String.valueOf(dtFechaFinConsPrm);      
	        
	        sQuery += ' And TAM_TipoVenta__c = \'' + String.escapeSingleQuotes(strTipoPedido) + '\'';
	        sQuery += ' And TAM_EstatusDOD__c != \'' + String.escapeSingleQuotes(EstatusDOD) + '\'';
	        sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol) + '\'';
	        sQuery += ' And TAM_EstatusDealerSolicitud__c != \'' + String.escapeSingleQuotes(EstDealerSol2) + '\'';
	
	        sQuery += ' Order by TAM_VIN__c, TAM_FechaCreacionFormula__c ASC';
	        sQuery += ' Limit 1';
            System.debug('EN TAM_ActCancelSolInvSch_cls.execute sQuery: ' + sQuery);

            //Crea el objeto de  objActCancelSolInvBch      
            TAM_ActCancelSolInvBch_cls objActCancelSolInvBch = new TAM_ActCancelSolInvBch_cls(sQuery, blnCanSolMesAntPrm, dtFechaIniConsPrm, dtFechaFinConsPrm );
            Id batchInstanceId2 = Database.executeBatch(objActCancelSolInvBch, 1);
                  
            string strSeconds2 = '0';
            string strMinutes2 = '0';
            string strHours2 = '1';
            string strDay_of_month2 = 'L';
            string strMonth2 = '6,12';
            string strDay_of_week2 = '?';
            string strYear2 = '2050-2051';
            String sch2 = strSeconds2 + ' ' + strMinutes2 + ' ' + strHours2 + ' ' + strDay_of_month2 + ' ' + strMonth2 + ' ' + strDay_of_week2 + ' ' + strYear2;
            
            TAM_ActCancelSolInvSch_cls ActCancelSolInvSch_Jobh = new TAM_ActCancelSolInvSch_cls();
            System.schedule('Ejecuta_TAM_ActCancelSolInvSch_cls', sch2, ActCancelSolInvSch_Jobh);          
            //FIN PARA EL PROCESO DE TAM_ActCancelSolInvBch_cls




            //PARA EL PROCESO DE TAM_ConsMovDDSolInvBch_cls
	        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
	        String sFleet = 'N';
	
	        String sHabilitaMergeAccounts = System.Label.TAM_FechaMesToyota;        
	        dFechaConsulta = sHabilitaMergeAccounts != 'null' ? Date.valueOf(sHabilitaMergeAccounts) : Date.today();
	        dtFechaIniConsPrm = Date.today();
	        dtFechaFinConsPrm = Date.today();
	        if (Test.isRunningTest()) dFechaConsulta = Date.today();
	        
	        //Consulta las fechas en el calendario de toyota
	        for (TAM_CalendarioToyota__c objCalToy : [Select t.TAM_FechaInicio__c, t.TAM_FechaFin__c 
	            From TAM_CalendarioToyota__c t Where TAM_FechaInicio__c <=:dFechaConsulta
	            And TAM_FechaFin__c >=:dFechaConsulta]){
	            //Coincide la fecha del calendario Toyota
	            dtFechaIniConsPrm = objCalToy.TAM_FechaInicio__c;
	            dtFechaFinConsPrm = objCalToy.TAM_FechaFin__c;
	            System.debug('EN TAM_ActCancelSolInvSch_cls.execute dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
	        }//Fin del for para TAM_CalendarioToyota__c
	
	        //Una prueba
	        if (Test.isRunningTest()){
	           dtFechaIniConsPrm = Date.today();
	           dtFechaFinConsPrm = Date.today();
	           System.debug('EN TAM_ActCancelSolInvSch_cls.execute  dtFechaIniConsPrm: ' + dtFechaIniConsPrm + ' dtFechaFinConsPrm: ' + dtFechaFinConsPrm);
	        }//Fin si Test.isRunningTest()
	
	        //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
	        String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
	                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
	                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
	                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
	                ' And TAM_FechaEnvForm__c = ' + String.valueOf(dFechaConsulta) +
	                ' And Fleet__c = \'' + String.escapeSingleQuotes(sFleet) + '\'' +
	                ' Order by Submitted_Date__c DESC';
	        //Ees una prueba
            if (Test.isRunningTest())
                queryMovimiento += ' Limit 1';
	        System.debug('EN TAM_ConsMovDDSolInvBch_cls queryMovimiento: ' + queryMovimiento);
	        
	        //Crea el objeto de  OppUpdEnvEmailBch_cls      
	        //TAM_ConsMovDDSolInvBch_cls objConsMovDDSolInvBch = new TAM_ConsMovDDSolInvBch_cls(queryMovimiento, dtFechaIniConsPrm, dtFechaFinConsPrm);
            //Id batchInstanceId2 = Database.executeBatch(objConsMovDDSolInvBch, 1);

            string strSeconds3 = '0';
            string strMinutes3 = '0';
            string strHours3 = '1';
            string strDay_of_month3 = 'L';
            string strMonth3 = '6,12';
            string strDay_of_week3 = '?';
            string strYear3 = '2050-2051';
            String sch3 = strSeconds3 + ' ' + strMinutes3 + ' ' + strHours3 + ' ' + strDay_of_month3 + ' ' + strMonth3 + ' ' + strDay_of_week3 + ' ' + strYear3;

            //TAM_ConsMovDDSolInvSch_cls ConsMovDDSolInvSch_Jobh = new TAM_ConsMovDDSolInvSch_cls();
            //System.schedule('Ejecuta_TAM_ConsMovDDSolInvSch_cls', sch2, ConsMovDDSolInvSch_Jobh);          
            //FIN PARA EL PROCESO DE TAM_ConsMovDDSolInvBch_cls



            //PARA EL PROCESO DE TAM_ConsMovDDSolInvBch_cls    
            //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
	        String sQuery4 = 'Select Id, TAM_CatalogoCentralizadoModelos__c, TAM_SolicitudFlotillaPrograma__c, ';
	        sQuery4 += ' TAM_IdExterno__c From TAM_DetalleOrdenCompra__c ';
	        sQuery4 += ' Where TAM_CatalogoCentralizadoModelos__c = null ';
            //Ees una prueba
            sQuery4 += ' Limit 1';
            System.debug('EN TAM_ConsMovDDSolInvBch_cls sQuery4: ' + sQuery4);
            
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActCatCentModBch_cls objActCatCentModBch = new TAM_ActCatCentModBch_cls(sQuery4);
            Id batchInstanceId4 = Database.executeBatch(objActCatCentModBch, 1);

            string strSeconds4 = '0';
            string strMinutes4 = '0';
            string strHours4 = '1';
            string strDay_of_month4 = 'L';
            string strMonth4 = '6,12';
            string strDay_of_week4 = '?';
            string strYear4 = '2050-2051';
            String sch4 = strSeconds4 + ' ' + strMinutes4 + ' ' + strHours4 + ' ' + strDay_of_month4 + ' ' + strMonth4 + ' ' + strDay_of_week4 + ' ' + strYear4;

            TAM_ActCatCentModSch_cls ActCatCentModSch_Jobh = new TAM_ActCatCentModSch_cls();
            System.schedule('Ejecuta_TAM_ActCatCentModSch_cls', sch4, ActCatCentModSch_Jobh);          
            //FIN PARA EL PROCESO DE TAM_ConsMovDDSolInvBch_cls

        Test.stopTest();        
    }

}