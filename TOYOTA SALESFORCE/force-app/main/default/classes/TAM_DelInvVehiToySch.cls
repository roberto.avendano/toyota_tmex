/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_DelInvVehiToySch y
    					eliminar los registros del objeto TAM_InventarioVehiculosToyota__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_DelInvVehiToySch implements Schedulable{

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_DelInvVehiToySch.execute...');

        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_UpdLeadRecodTypeNoReg);		
		
		//Crea la consulta para los Leads
		this.sQuery = ' SELECT Id ';
		this.sQuery += ' FROM TAM_InventarioVehiculosToyota__c ';
		if (!Test.isRunningTest())
			this.sQuery += ' WHERE CreatedDate < TODAY Order by CreatedDate ASC ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_DelInvVehiToySch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_DelInvVehiToyBch objUpdRtTmexLeadBch = new TAM_DelInvVehiToyBch(sQuery);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objUpdRtTmexLeadBch, 200);
			    	 
    }
    
}