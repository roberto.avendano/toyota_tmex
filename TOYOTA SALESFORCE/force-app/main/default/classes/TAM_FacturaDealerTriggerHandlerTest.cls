/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de la clase de TAM_FacturaDealerTriggerHandler
******************************************************************************* */
@isTest
public class TAM_FacturaDealerTriggerHandlerTest {
    @testSetup static void setup(){
        //Se crea un estado de cuenta de prueba
        TAM_EstadoCuenta__c edoCta = new TAM_EstadoCuenta__c();
        edoCta.name = 'JULIO-TEST';
        edoCta.TAM_EstadoRegistro__c = 'Abierto';
        edoCta.TAM_CodigoDealer__c = '57002';
        edoCta.TAM_NombreDealer__c  = 'TOYOTA DE PRUEBA';
        edoCta.TAM_FechaInicio__c = date.today();
        edoCta.TAM_FechaCierre__c = date.today()+90;
        insert edoCta;
        
        //Se inserta la factura de dealer
        TAM_Factura_Dealer__c factDealer = new TAM_Factura_Dealer__c();
        factDealer.TAM_EstadoCuenta__c = edoCta.Id;
        factDealer.TAM_EstadoFactura__c = 'Pendiente';
        factDealer.TAM_FechaTentativaPago__c = date.today();
        factDealer.TAM_FolioFactura__c = 'A0001';
        factDealer.TAM_TipoIncentivo__c	 = 'Retail';
        insert factDealer;
        
        //Se inserta la factura de dealer VC 
        TAM_Factura_Dealer__c factDealer2 = new TAM_Factura_Dealer__c();
        factDealer2.TAM_EstadoCuenta__c = edoCta.Id;
        factDealer2.TAM_EstadoFactura__c = 'Pendiente';
        factDealer2.TAM_FechaTentativaPago__c = date.today();
        factDealer2.TAM_FolioFactura__c = 'A0002';
        factDealer2.TAM_TipoIncentivo__c	 = 'Retail';
        insert factDealer2;
        
        //Se inserta el detalle de estado de cuenta - retail
        String recordIdedoCtaRetail  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCta = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta.TAM_Monto_sin_IVA__c = 25000;
        detalleEdoCta.RecordTypeId = recordIdedoCtaRetail;
        detalleEdoCta.TAM_ApellidosPropietario__c = 'Figueroas';
        detalleEdoCta.TAM_AnioModelo__c = '2020';
        detalleEdoCta.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCta.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCta.TAM_CodigoDealer__c = '57002';
        detalleEdoCta.TAM_Codigo_Producto__c = '7400';
        detalleEdoCta.TAM_EnviadoFacturar__c = false;
        detalleEdoCta.TAM_EdoCtaCerrado__c = false;
        detalleEdoCta.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCta.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCta.TAM_VINEntregado__c = true;
        detalleEdoCta.TAM_VIN__c = '12345678901234567';
        detalleEdoCta.TAM_pagoPorTFS__c = false;
        detalleEdoCta.TAM_Serie__c = 'HILUX';
        detalleEdoCta.TAM_EstadoCuenta__c = edoCta.id;
        detalleEdocta.TAM_Factura_Dealer__c = factDealer.Id;
        detalleEdocta.TAM_EstatusVINEdoCta__c = 'Enviado a Facturar';
        insert detalleEdoCta;
        
        //Se inserta el detalle de estado de cuenta VC
        String recordIdedoCtaVC  = Schema.SObjectType.TAM_DetalleEstadoCuenta__c.getRecordTypeInfosByDeveloperName().get('Flotilla').getRecordTypeId();
        TAM_DetalleEstadoCuenta__c detalleEdoCtaVC = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCtaVC.TAM_Monto_sin_IVA__c = 25000;
        detalleEdoCtaVC.RecordTypeId = recordIdedoCtaVC;
        detalleEdoCtaVC.TAM_ApellidosPropietario__c = 'Figueroas';
        detalleEdoCtaVC.TAM_AnioModelo__c = '2020';
        detalleEdoCtaVC.TAM_ComentarioDealer__c = 'Metodo de prueba';
        detalleEdoCtaVC.TAM_Comentario_Finanzas__c = 'Metodo de prueba';
        detalleEdoCtaVC.TAM_CodigoDealer__c = '57002';
        detalleEdoCtaVC.TAM_Codigo_Producto__c = '7400';
        detalleEdoCtaVC.TAM_EnviadoFacturar__c = false;
        detalleEdoCtaVC.TAM_EdoCtaCerrado__c = false;
        detalleEdoCtaVC.TAM_EstatusVINEdoCta__c = 'Revisado y listo para cobro';
        detalleEdoCtaVC.TAM_EstatusVINProvision__c = 'Autorizado para Pago';
        detalleEdoCtaVC.TAM_VINEntregado__c = true;
        detalleEdoCtaVC.TAM_VIN__c = '12345678901234567';
        detalleEdoCtaVC.TAM_pagoPorTFS__c = false;
        detalleEdoCtaVC.TAM_Serie__c = 'HILUX';
        detalleEdoCtaVC.TAM_EstadoCuenta__c = edoCta.id;
        detalleEdoCtaVC.TAM_Factura_Dealer__c = factDealer.Id;
        detalleEdoCtaVC.TAM_EstatusVINEdoCta__c = 'Enviado a Facturar';
        insert detalleEdoCtaVC;
        
        
    }
    
    @isTest static void testMethod1(){
        //Se actualiza la factura dealer que detona el evento del trigger
        TAM_Factura_Dealer__c facturaDealer = [Select Id,TAM_FolioFactura__c,TAM_EstadoFactura__c FROM TAM_Factura_Dealer__c WHERE 
                                              TAM_FolioFactura__c = 'A0001'];
        facturaDealer.TAM_EstadoFactura__c = 'Autorizada';
     	update facturaDealer;
        
    }
    
    @isTest static void testMethod2(){
        //Se actualiza la factura dealer que detona el evento del trigger
        TAM_Factura_Dealer__c facturaDealer = [Select Id,TAM_FolioFactura__c,TAM_EstadoFactura__c FROM TAM_Factura_Dealer__c WHERE 
                                              TAM_FolioFactura__c = 'A0001'];
        facturaDealer.TAM_EstadoFactura__c = 'Rechazada';
     	update facturaDealer;
        
    }
    
    @isTest static void testMethod3(){
        //Se actualiza la factura dealer que detona el evento del trigger
        TAM_Factura_Dealer__c facturaDealer = [Select Id,TAM_FolioFactura__c,TAM_EstadoFactura__c FROM TAM_Factura_Dealer__c WHERE 
                                              TAM_FolioFactura__c = 'A0001'];
        facturaDealer.TAM_EstadoFactura__c = 'Pendiente';
     	update facturaDealer;
        
    }
    
        @isTest static void testMethod4(){
        //Se actualiza la factura dealer que detona el evento del trigger
        TAM_Factura_Dealer__c facturaDealer = [Select Id,TAM_FolioFactura__c,TAM_EstadoFactura__c FROM TAM_Factura_Dealer__c WHERE 
                                              TAM_FolioFactura__c = 'A0001'];
        facturaDealer.TAM_EstadoFactura__c = 'Pago Cancelado Finanzas';
     	update facturaDealer;
        
    }
    
    @isTest static void testMethod5(){
        //Se actualiza la factura dealer que detona el evento del trigger
        TAM_Factura_Dealer__c facturaDealer = [Select Id,TAM_FolioFactura__c,TAM_EstadoFactura__c FROM TAM_Factura_Dealer__c WHERE 
                                              TAM_FolioFactura__c = 'A0002'];
        facturaDealer.TAM_EstadoFactura__c = 'Autorizada';
     	update facturaDealer;
        
    }
}