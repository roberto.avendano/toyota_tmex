global with sharing class TAM_CreaLeadsFaceBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    global String sOppAct;
    
    //Un constructor por default
    global TAM_CreaLeadsFaceBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CreaLeadsFaceBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_LeadFacebook__c> scope){
        System.debug('EN TAM_CreaLeadsFaceBch_cls.');
        
        Map<String, String> mapCodDistNom = new Map<String, String>();
        Map<String, TAM_LeadFacebook__c> mapLeadFaceUps = new Map<String, TAM_LeadFacebook__c>();
                        
        //Recorre la lista de Casos para cerrarlos 
        for (TAM_LeadFacebook__c objUsrSegLead : scope){
            //Metelo al mapa de mapLeadFaceUps            
            mapLeadFaceUps.put(objUsrSegLead.id, new TAM_LeadFacebook__c(
                    id = objUsrSegLead.id,
                    Name = objUsrSegLead.Name
                )
            );
        }
        System.debug('EN TAM_CreaLeadsFaceBch_cls mapLeadFaceUps: ' + mapLeadFaceUps);

        //Ve si tiene algo la lista de mapLeadFaceUps
        if (!mapLeadFaceUps.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
            List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapLeadFaceUps.values(), TAM_LeadFacebook__c.id, false);
            //Ve si hubo error
            for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('EN TAM_CreaLeadsFaceBch_cls Hubo un error a la hora de crear/Actualizar los registros en TAM_UsuariosSeguimientoLead__c ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !mapLeadFaceUps.isEmpty()        

    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CreaLeadsFaceBch_cls.finish Hora: ' + DateTime.now());
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_ActDatosUsrSegLeadMinut);
        DateTime dtHoraActual = DateTime.now();
        DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //5
        String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
        System.debug('EN TAM_CreaLeadsFaceBch_cls.finish CRON_EXP: ' + CRON_EXP);

        //Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
        TAM_ActDatosUsrSegLeadSch_cls objActDatosUsrSegLeadSch = new TAM_ActDatosUsrSegLeadSch_cls();
        System.debug('EN TAM_CreaLeadsFaceBch_cls.finish objActDatosUsrSegLeadSch: ' + objActDatosUsrSegLeadSch);
        //Programa el proceso desde System
        if (!Test.isRunningTest())
            System.schedule('TAM_ActDatosUsrSegLeadSch_cls: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objActDatosUsrSegLeadSch);
    } 
    
}