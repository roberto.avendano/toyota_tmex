@isTest
public class BuscadorArticuloTest {
    static testmethod void test(){
        Knowledge__kav articulo = new Knowledge__kav();
        articulo.Title = 'TestPDF';
        articulo.Cuerpo__c = 'Test Text to Rich content';
        articulo.UrlName = 'TestPDFURL';
        //articulo.Publishstatus = = 'online';
        articulo.Language = 'es_MX';
        insert articulo;
        Knowledge__kav articuloSinPublicar = [SELECT id, KnowledgeArticleId FROM Knowledge__kav where id = :articulo.id];
        String articleID = articuloSinPublicar.KnowledgeArticleId;
        KbManagement.PublishingService.publishArticle(articleID, true);
        System.debug('articulo ' + articulo.id);
        System.debug('articulo ' + articulo.Publishstatus);
        
        PageReference pageRef = Page.KnowledgeAsPDF;
		Test.setCurrentPage(pageRef);
        apexpages.currentpage().getparameters().put('id' , articulo.Id);
        BuscadorArticulo controlador = new BuscadorArticulo();
        controlador.SaveAspdf();
    }

}