/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM 
    Descripción:        Clase que contiene la logica para probar la clase TAM_VentasCRM_handler.

    Infomación de cambios (versiones)VaRtAccRegPF
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    25-Mayo-2020    		Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_VentasCRM_handler_tst {

    static String VaRtLeadRetailNuevos = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Nuevos').getRecordTypeId();

	@TestSetup static void loadData(){
		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cuenta;
		
        Lead cand = new Lead(
            RecordTypeId = VaRtLeadRetailNuevos,
            FirstName = 'Test12',
            FWY_Intencion_de_compra__c = 'Este mes',    
            Email = 'aw@a.com',
            phone = '5554565432',
            Status='Pedido en Proceso',
            LastName = 'Test3',
            FWY_Tipo_de_persona__c = 'Person física',
            TAM_TipoCandidato__c = 'Retail Nuevos',
            TAM_DatosValidosFacturacion__c = true,
            FWY_codigo_distribuidor__c = '57039'      
        );
        insert cand;
        System.debug('EN TAM_VentasCRM_handler_tst loadData cand: '+ cand);

        TAM_InventarioVehiculosToyota__c objInvAFG = new TAM_InventarioVehiculosToyota__c(
              Name = 'JTDKBRFUXH3031000',
              Dealer_Code__c = '57039',
              Interior_Color_Description__c = 'Azul', 
              Exterior_Color_Description__c = 'Gris',
              Model_Number__c = '22060',
              Model_Year__c = '2020', 
              Toms_Series_Name__c = 'AVANZA',
              Exterior_Color_Code__c = '00B79', 
              Interior_Color_Code__c =  '010',
              Distributor_Invoice_Date_MM_DD_YYYY__c = '23/04/20 12:00 AM'
        );
        insert objInvAFG;
        System.debug('EN TAM_VentasCRM_handler_tst loadData objInvAFG: '+ objInvAFG);

        TAM_LeadInventarios__c objLeadInvAFG = new TAM_LeadInventarios__c(
              Name = 'JTDKBRFUXH3031000',
              TAM_InventarioVehiculosFyG__c = objInvAFG.id,
              TAM_Prospecto__c = cand.id, 
              TAM_Idexterno__c = cand.id + ' JTDKBRFUXH3031000',
              TAM_VINFacturacion__c = 'JTDKBRFUXH3031000'
        );
        insert objLeadInvAFG;
        System.debug('EN TAM_VentasCRM_handler_tst loadData objLeadInvAFG: '+ objLeadInvAFG);
		
	}

	public static List<TAM_VentasCRM__c> loadVentasH(){
		List<TAM_VentasCRM__c> ventasH = new List<TAM_VentasCRM__c>();
		List<String> dealerCode = new List<String>{'57051', '57052', '57053', '57054', '57055'};
		List<String> modelos = new List<String>{'1250', '1251', '1252', '1253', '1254'};
		List<String> colores = new List<String>{'40', '50', '60', '70', '80'};
		List<String> seriales = new List<String>{'H3031090', 'H3031080', 'H3031070', 'H3031050', 'H3031040'};
		
		String sTAMTipoCliente = 'Primero';
		for(Integer i=0; i < 3; i++){			
		 	String sufix = i < 3 ? '0' + String.valueOf(i):String.valueOf(i);  
		 	
		 	ventasH.add(new TAM_VentasCRM__c(
        		Name = 'JTDKBRFUXH30310'+sufix,
        		IdDealerCode__c = dealerCode.get(aleatorio(dealerCode.size())),    			
        		IdExterno__c = 'JTDKBRFUXH30310'+ sufix +'-' + String.valueOf(Date.today()),
        		TAM_TipoCliente__c = sTAMTipoCliente,
        		FirstName__c = 'PruebaFN',
        		LastName__c = 'PruebaLN',
        		TAM_dCOORGX__c = '-100.3434513',
        		TAM_dCOORGY__c = '25.6563693',
        		TAM_dSEXO__c = 'E',
        		TAM_dINDFIA__c = 'V',
        		TAM_dINDICA__c = '7'
	 		));
	 		//Cambia el estatus
	 		if (sTAMTipoCliente == 'Primero')
				sTAMTipoCliente = 'Segundo';
	 		else if (sTAMTipoCliente == 'Segundo')
				sTAMTipoCliente = 'Primero';
		}		
				
		System.debug(JSON.serialize(ventasH));		
		return ventasH;
	}

	@isTest static void test_one() {
	    Test.startTest();
            TAM_InventarioVehiculosToyota__c objPasoInv = [Select id, Name From TAM_InventarioVehiculosToyota__c LIMIT 1];
            System.debug('EN TAM_VentasCRM_handler_tst test_one objPasoInv: '+ objPasoInv);
	        TAM_LeadInventarios__c objPaso = [Select id, Name, TAM_VINFacturacion__c, TAM_Prospecto__c From TAM_LeadInventarios__c LIMIT 1];
            System.debug('EN TAM_VentasCRM_handler_tst test_one objPaso: '+ objPaso);
	       
			List<TAM_VentasCRM__c> ventas = TAM_VentasCRM_handler_tst.loadVentasH();
			insert ventas;
			//Llama al metodo de ConvierteFecha
			VentasHTriggerHandler objVentasHTriggerHandler = new VentasHTriggerHandler();
			Time horaAct = Time.newInstance(4,26,18,0);
			
			//Actualiza el reg de ventas.get(0)
			update ventas.get(0);
	        System.debug('EN TAM_VentasCRM_handler_tst test_one ventas.get(0): '+ ventas.get(0));
		Test.stopTest();
	}

	@isTest static void test_two() {
		List<TAM_VentasCRM__c> ventas = TAM_VentasCRM_handler_tst.loadVentasH();
		for(Integer i=0; i<ventas.size(); i++){
			ventas[i].SubmittedDate__c = Datetime.newInstance(2017, 10, 13);
			ventas[i].SaleCode__c = String.valueOf(i);
		}
		insert ventas;
		
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
}