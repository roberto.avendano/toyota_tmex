public with sharing class ResumenMasterBOBatchButtonController {

    public ResumenMasterBOBatchButtonController(ApexPages.StandardSetController stdController) {
        
    }

    public PageReference consolidarBO(){
        ResumenMasterBOBatch batchMasterBO = new ResumenMasterBOBatch();
        Database.executeBatch(batchMasterBO);
        return null;
    }
}