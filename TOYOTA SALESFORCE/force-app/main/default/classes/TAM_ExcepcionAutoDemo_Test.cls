@isTest(SeeAllData=false)
public class TAM_ExcepcionAutoDemo_Test {
    
    @testSetup static void setup() {
        DateTime fechaVenta = DateTime.now();
        DateTime fechaVenta2 = DateTime.now() -70;
        String month = String.valueOf(fechaVenta.month());
        String year = String.valueOf(fechaVenta.year());
        String month2 = String.valueOf(fechaVenta2.month());
        String year2 = String.valueOf(fechaVenta2.year());
        
        //User
        User usuarioGuadalajara = TAM_TestUtilityClass.crearUsuarioPartnerCommunity('Gerente Guadalajara', '57011');
        
        //Vehiculo
        Vehiculo__c objVehiculo = TAM_TestUtilityClass.crearVIN('JTMR13FV1LD061623', '2020');
        
        //Serie
        Serie__c objSerie = TAM_TestUtilityClass.crearSerie('RAV4');
        
        //Producto
        Product2 objProducto = TAM_TestUtilityClass.crearProducto(objSerie.Id, '4501', '2020', true);
        
        //Movimiento
        Account acc = [SELECT Id FROM Account WHERE Name='Gerente Guadalajara' LIMIT 1];
        Movimiento__c objMovimiento = TAM_TestUtilityClass.crearMovimientos('2020','RDR', '6', fechaVenta, 'N',month, year, objProducto.Id, objVehiculo.Id, acc.Id);
        Movimiento__c objMovimiento2 = TAM_TestUtilityClass.crearMovimientos('2020','RDR', '6', fechaVenta2, 'N',month2, year2, objProducto.Id, objVehiculo.Id, acc.Id);

        //Factura
        Factura__c objFactura = TAM_TestUtilityClass.crearFactura(objVehiculo.Id, fechaVenta, 491100, acc.Id);
        
        //AutoDemo
        SolicitudAutoDemo__c objDemo = TAM_TestUtilityClass.crearAutoDemo(objVehiculo.Id, 'Baja por pago completado sin solicitud');
        objDemo.FechaBajaAutoDemoSolicitud__c = system.today();
        objDemo.FechaBajaPagoCompletado__c = system.today();
        objDemo.FechaAltaAutoDemo__c = system.today().addMonths(-2);
        objDemo.VIN__c = objVehiculo.Id;
        objDemo.Distribuidor__c = acc.Id;
        update objDemo;
        
        //Inventario en Piso
        TAM_TestUtilityClass.crearInventarioPiso('RAV4', '4501', '2020', 'x', Date.valueOf(fechaVenta), 50, 20);
        
        //Catálogo Centralizado Modelos
        TAM_TestUtilityClass.crearModeloCatalogoCentralizado('RAV4', '4501','2020', 'x');
        
        //PriceBook
        Id standardPricebookId = Test.getStandardPricebookId();
        TAM_TestUtilityClass.crearPricebookEntryCustom(standardPricebookId, objProducto, true, 400000, 10, 40000);
        
        //Política Incentivos
        TAM_PoliticasIncentivos__c objPoliticaIncentivos = TAM_TestUtilityClass.crearPoliticaIncentivos('Retail', 'Politica_01', Date.valueOf(fechaVenta), Date.valueOf(fechaVenta), false, 'Vigente');
        
        //Detalle Politica Incentivos
        //TAM_TestUtilityClass.crearDetallePoliticaIncentivos('RAV4', '4501','2020', 'x',10000, 5000, 5000, objPoliticaIncentivos.Id,400000); 
    }
    
    @isTest
    public static void searchAutoDemo(){
		User usuarioGuadalajara = [SELECT Id,CodigoDistribuidor__c FROM User WHERE CodigoDistribuidor__c = '57011'];       
        Vehiculo__c objVehiculo = [SELECT Id, Name FROM Vehiculo__c WHERE Name = 'JTMR13FV1LD061623'];
        List<TAM_SearchAutoDemoExcepcion.wrapVIN> lstAux;
        Test.startTest();
            system.runAs(usuarioGuadalajara){
                lstAux = TAM_SearchAutoDemoExcepcion.vinBDDealerDaily(objVehiculo.Name , usuarioGuadalajara.Id);
            }
            TAM_SearchAutoDemoExcepcion.wrapVIN objWrapper = lstAux[0];
            String myJSON = JSON.serialize(objWrapper);
            TAM_ExcepcionAutoDemo.saveRecord(myJSON,'Test');
        Test.stopTest();
        
        System.assertEquals(objVehiculo.Name, lstAux[0].VIN);  
    }
}