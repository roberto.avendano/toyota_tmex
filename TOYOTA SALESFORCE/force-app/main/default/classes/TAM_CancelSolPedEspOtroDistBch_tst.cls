/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_DistribuidoresFlotillaPrograma__c y cancelar las soicitudes 
                        que el usuario final no ha enviado a autorizar.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    07-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CancelSolPedEspOtroDistBch_tst {

    static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();
    static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
    static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    static String srecordTypePrograma =  Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
    
    @testSetup static void setup() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        
        Id recordTypeDistribuidor =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Dealer').getRecordTypeId();
        
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57002',
            recordTypeId = recordTypeDistribuidor
        );
        insert a01;
        
        Contact contactRH1 = new Contact();
        contactRH1.NombreUsuario__c = 'Contacto RH1';
        contactRH1.lastName = 'Contacto RH1';
        contactRH1.Email = 'test@test.com';
        contactRH1.AccountId = a01.id;
        insert contactRH1;
        
        Contact contactRH2 = new Contact();
        contactRH2.NombreUsuario__c = 'Contacto RH2';
        contactRH2.lastName = 'Contacto RH2';
        contactRH2.Email = 'test2@test.com';
        contactRH2.AccountId = a01.id;
        insert contactRH2;
        
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia 
            UserRole uR = [SELECT Id FROM UserRole WHERE Name='Consultor comonuevos'];
            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US',ContactId = contactRH2.id,ProfileId = p.Id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
            //Usuario 2
            User u2 = new User(Alias = 'standt', Email='standarduser2@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US',ContactId = contactRH1.id,ProfileId = p.Id,PortalRole  = 'Manager',
                               TimeZoneSidKey='America/Los_Angeles', UserName='test992@testorg.com');
            system.debug('usuario'+u2);
            insert u2;
            
            
            //Grupo 1
            Group grp = new Group();
            grp.name = 'Aprobador DTM (nivel 1)';
            grp.Type = 'Regular'; 
            Insert grp; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = u.Id;
            grpMem1.GroupId = grp.Id;
            Insert grpMem1;
            
            
            //Grupo 2
            Group grp2 = new Group();
            grp2.name = 'Aprobador DTM (nivel 2)';
            grp2.Type = 'Regular'; 
            Insert grp2; 
            
            //Create Group Member Aprobador DTM (nivel 1)
            GroupMember grpMem2 = new GroupMember();
            grpMem2.UserOrGroupId = u2.Id;
            grpMem2.GroupId = grp2.Id;
            Insert grpMem2;
            
            
        }
        
        //Se crea un registro de prueba de tipo TAM_SolicitudesFlotillaPrograma__c
        Rangos__c rangoPrograma = new Rangos__c(
            Name = 'UBER',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'UBER | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoPrograma;
        
        Rangos__c rangoFlotilla = new Rangos__c(
            Name = 'AAA',
            NumUnidadesMinimas__c = 10,         
            NumUnidadesMaximas__c = 50,
            PerGraciaRango__c = 15,
            Activo__c = true,
            DiasVigentes__c = 180,
            ID_Externo__c = 'AAA | 10 | 50',
            RecordTypeId = sRectorTypePasoPrograma
            
        );
        insert rangoFlotilla;
        
        Account clienteMoral = new Account(
            Name = 'CARSON',
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoPersonaMoral,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteMoral;
                
        TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
            TAM_Cliente__c = clienteMoral.id,           
            TAM_Estatus__c = 'El Proceso',
            RecordTypeId = srecordTypePrograma,
            TAM_ProgramaRango__c = rangoFlotilla.id,
            TAM_NomDistribuidorPropietario__c = 'Es de Prueba',
            TAM_ComentarioDTM1__c = 'TEST',
            TAM_ComentarioDTM2__c = 'TEST',
            TAM_AprobacionDTM1__c = 'Aprobar Cancelación',
            TAM_AprobacionDTM2__c = 'Aprobar Cancelación',
            TAM_FechaCierreSolicitud__c = Date.today()-1
        );
        insert solVentaFlotillaPrograma;

        //Se crean lineas de tipo TAM_CheckOutDetalleSolicitudCompra__c
        TAM_DistribuidoresFlotillaPrograma__c lineaDistFlotprog = new TAM_DistribuidoresFlotillaPrograma__c();
        lineaDistFlotprog.Name = '57002-Dealer-2021-AVANZA'; 
        lineaDistFlotprog.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        lineaDistFlotprog.TAM_NombreDistribuidor__c = 'TOYOTA PRUEBA';
        lineaDistFlotprog.TAM_RegistroRevisadoDealer__c = true;
        //lineaDistFlotprog.TAM_IdExterno__c = 'Contado';
        lineaDistFlotprog.TAM_IdDistribuidor__c = '570550';
        lineaDistFlotprog.TAM_FechaCancelacion__c = DateTime.now();
        lineaDistFlotprog.TAM_Estatus__c = 'Pendiente';
        lineaDistFlotprog.TAM_Cantidad__c = 1;
        insert lineaDistFlotprog;

        String sFolSol = '';
        //Consulta la solicitud asociada a TAM_SolicitudesFlotillaPrograma__c
        for (TAM_SolicitudesFlotillaPrograma__c ovbjSol : [Select id, Name From TAM_SolicitudesFlotillaPrograma__c
            Where id = :solVentaFlotillaPrograma.id]){
            sFolSol = ovbjSol.Name;
        }
        System.debug('EN TAM_CheckOutProgramaCtrlOK sFolSol: ' + sFolSol);

        TAM_SolicitudDealerDestino__c objSolicitudDealerDestino = new TAM_SolicitudDealerDestino__c(
            Name = sFolSol,
            TAM_IdExterno__c = sFolSol + ' - ' + '570550-TOYOTA PRUEBA',
            TAM_Estatus__c = 'Pendiente',
            TAM_FechaCancelacion__c = Date.today(),
            TAM_FechaCheckOut__c = Date.today(),
            TAM_DealerOrigen__c = '57000-TMEX',
            TAM_DealerDestino__c = '570550-TOYOTA PRUEBA'
        );                    
        insert objSolicitudDealerDestino;

        //Se crean lineas de tipo TAM_CheckOutDetalleSolicitudCompra__c
        TAM_CheckOutDetalleSolicitudCompra__c lineaCheckOut = new TAM_CheckOutDetalleSolicitudCompra__c();
        lineaCheckOut.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        lineaCheckOut.TAM_VIN__c = 'CAERQIOO1829101';
        lineaCheckOut.TAM_EstatusDealerSolicitud__c = 'En proceso de cancelación';
        lineaCheckOut.TAM_TipoPago__c = 'Contado';
        lineaCheckOut.TAM_IncPropuestoPorcentaje__c = 5;
        lineaCheckOut.TAM_DistribuidorEntrega__c = '570550-TOYOTA PRUEBA';
        insert lineaCheckOut;
        
        //Se crean lineas de tipo TAM_DODSolicitudesPedidoEspecial__c
        TAM_DODSolicitudesPedidoEspecial__c solicitudDOD = new TAM_DODSolicitudesPedidoEspecial__c();
        solicitudDOD.TAM_SolicitudFlotillaPrograma__c = solVentaFlotillaPrograma.id;
        solicitudDOD.TAM_VIN__c = 'QWERTYUIO45670';
        solicitudDOD.TAM_Estatus__c = 'Autorizado';
        solicitudDOD.TAM_Modelo__c  = '2701';
        solicitudDOD.TAM_Codigo__c = '2701';
        solicitudDOD.TAM_Anio__c = '2020';
        solicitudDOD.TAM_DescripcionColor__c = 'Rojo';
        solicitudDOD.TAM_Pago__c  = 'Contado';
        solicitudDOD.TAM_DescuentoAutorizadoDtm__c  = 5;
        solicitudDOD.TAM_ComentarioDTM1__c = 'tEST';
        solicitudDOD.TAM_Entrega__c = '570550-TOYOTA PRUEBA';
        insert solicitudDOD;
                        
    }

    //Lalama el metodo de prueba
    static testMethod void TAM_CancelSolPedEspOtroDistBchOK() {
        Test.startTest();

	        String sHoraTimeZone = '';    
	        for (User objUsr : [Select u.id, u.TimeZoneSidKey From User u Where id =:UserInfo.getUserId()]){ //UserInfo.getUserId()
	            System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute objUsr.TimeZoneSidKey: ' + objUsr.TimeZoneSidKey);
	            Schema.DescribeFieldResult fieldResult = User.TimeZoneSidKey.getDescribe();
	            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	            for( Schema.PicklistEntry pickListVal : ple){
	                if (pickListVal.getValue() == objUsr.TimeZoneSidKey){
	                    System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute getLabel(): ' + pickListVal.getLabel() + ' getValue(): ' + pickListVal.getValue());  
	                    //Toma los primeros caracteres de la pickListVal.getLabel()
	                    String strTimeZonePaso = pickListVal.getLabel().left(11);                                 
	                    System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso);
	                    String[] arrFechaGmt = strTimeZonePaso.split('-');
	                    String[] arrHoraTimeZoneGmtPaso = arrFechaGmt[1].split(':');
	                    sHoraTimeZone = arrHoraTimeZoneGmtPaso[0]; 
	                    System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute strTimeZonePaso : ' + strTimeZonePaso + ' arrFechaGmt: ' + arrFechaGmt + ' arrHoraTimeZoneGmtPaso: ' + arrHoraTimeZoneGmtPaso + ' sHoraTimeZone: ' + sHoraTimeZone);
	                }
	            }
	        }
	        //Crea la fecha de paso para la tarea                                                                            
	        //Date dtFechaPaso = Date.newInstance(Integer.valueOf(sArrFechaPaso[0]), Integer.valueOf(sArrFechaPaso[1]), Integer.valueOf(sArrFechaPaso[2]));
 
	        //Para la fecha Ini        
	        DateTime dtFechaCancelacionIni = DateTime.now().addMinutes(-3);
	        DateTime dtFechaCancelacionIni2 = dtFechaCancelacionIni.addHours(Integer.valueOf(sHoraTimeZone)); //dtFechaCancelacionIni;
	        String sdtFechaCancelacionIni = String.valueOf(dtFechaCancelacionIni2);
	        String sdtFechaCancelacionIniFinal = sdtFechaCancelacionIni.replace(' ', 'T') + 'Z';
	        //Para la fecha Fin
	        DateTime dtFechaCancelacionFin = DateTime.now().addMinutes(3);
	        DateTime dtFechaCancelacionFin2 = dtFechaCancelacionFin.addHours(Integer.valueOf(sHoraTimeZone)); //dtFechaCancelacionFin;
	        String sdtFechaCancelacionFin = String.valueOf(dtFechaCancelacionFin2);
	        String sdtFechaCancelacionFinFinal = sdtFechaCancelacionFin.replace(' ', 'T') + 'Z';
	        System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute sdtFechaCancelacionIniFinal: ' + sdtFechaCancelacionIniFinal + ' sdtFechaCancelacionFinFinal: ' + sdtFechaCancelacionFinFinal);
	            
	        String sQuery = 'Select t.Name, t.TAM_FechaCheckOut__c, t.TAM_FechaCancelacion__c, t.TAM_Estatus__c,';
	        sQuery += ' t.TAM_DealerOrigen__c, t.TAM_DealerDestino__c, t.TAM_IdExterno__c';
	        sQuery += ' From TAM_SolicitudDealerDestino__c t';
	        sQuery += ' Order by t.Name LIMIT 1';
            System.debug('EN TAM_CancelSolPedEspOtroDistSch_cls.execute sQuery: ' + sQuery);

            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_CancelSolPedEspOtroDistBch_cls objActFechaDDChkOutBch = new TAM_CancelSolPedEspOtroDistBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActFechaDDChkOutBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_CancelSolPedEspOtroDistSch_cls CancelSolPedEspOtroDistSch_Jobh = new TAM_CancelSolPedEspOtroDistSch_cls();
            System.schedule('Ejecuta_TAM_CancelSolPedEspOtroDistSch_cls', sch, CancelSolPedEspOtroDistSch_Jobh);
            
        Test.stopTest();        
    }
}