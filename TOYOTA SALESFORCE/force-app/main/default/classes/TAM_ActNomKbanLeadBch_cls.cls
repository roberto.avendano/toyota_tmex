/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto Lead
                        y actuelizar los campos de FWY_codigo_distribuidor__c y TAM_NomPropKban__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    17-Sep-2021          Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_ActNomKbanLeadBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global string query;
    
    //Un constructor por default
    global TAM_ActNomKbanLeadBch_cls(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_ActNomKbanLeadBch_cls.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<LeadHistory> scope){ //List<LeadHistory>
        System.debug('EN TAM_ActNomKbanLeadBch_cls.');

        Map<String, Lead> mapLeadUpd = new Map<String, Lead>();

        //Recorre el mapa de mapIdVinReg 
        for (LeadHistory objLeadHistoryMod : scope){
            Boolean blnActua = false;
            Lead objLeadHist = new Lead(id = objLeadHistoryMod.LeadId);
            //Ve si el campo Lead.TAM_CodDistribuidorUsr__c != FWY_codigo_distribuidor__c
            if (objLeadHistoryMod.Lead.TAM_CodDistribuidorUsr__c != objLeadHistoryMod.Lead.FWY_codigo_distribuidor__c){
                objLeadHist.FWY_codigo_distribuidor__c = objLeadHistoryMod.Lead.TAM_CodDistribuidorUsr__c;
                blnActua = true;
            }//Fin si objLeadHistoryMod.Lead.TAM_CodDistribuidorUsr__c != objLeadHistoryMod.Lead.FWY_codigo_distribuidor__c
            //Ve si el campo Lead.TAM_CodDistribuidorUsr__c != FWY_codigo_distribuidor__c
            if (objLeadHistoryMod.Lead.TAM_NomPropUsr__c != objLeadHistoryMod.Lead.TAM_NomPropKban__c){
                objLeadHist.TAM_NomPropKban__c = objLeadHistoryMod.Lead.TAM_NomPropUsr__c;
                blnActua = true;            
            }//Fin si objLeadHistoryMod.Lead.TAM_NomPropUsr__c != objLeadHistoryMod.Lead.TAM_NomPropKban__c
            //Ve si el campo Lead.FWY_Distribuidor_f__c != Distribuidor_para_cuenta__c
            if (objLeadHistoryMod.Lead.FWY_Distribuidor_f__c != objLeadHistoryMod.Lead.Distribuidor_para_cuenta__c){
                objLeadHist.Distribuidor_para_cuenta__c = objLeadHistoryMod.Lead.FWY_Distribuidor_f__c;
                blnActua = true;
            }//Fin si objLeadHistoryMod.Lead.FWY_Distribuidor_f__c != objLeadHistoryMod.Lead.Distribuidor_para_cuenta__c
            //Metelo al mapa de mapLeadUpd
            if (blnActua || Test.isRunningTest())
                mapLeadUpd.put(objLeadHist.id, objLeadHist);                           
        }//Fin del for para la lista de Lead        
        System.debug('EN TAM_ActNomKbanLeadBch_cls mapLeadUpd:' + mapLeadUpd.keySet());
        System.debug('EN TAM_ActNomKbanLeadBch_cls mapLeadUpd:' + mapLeadUpd.values());
        
        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapLeadUpd.values(), Lead.id, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess())
               System.debug('EN TAM_ActNomKbanLeadBch_cls Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
        }//Fin del for para lDtbUpsRes

        //Roleback a todo
        //Database.rollback(sp);
   
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_ActNomKbanLeadBch_cls.finish Hora: ' + DateTime.now());      
    } 
    
}