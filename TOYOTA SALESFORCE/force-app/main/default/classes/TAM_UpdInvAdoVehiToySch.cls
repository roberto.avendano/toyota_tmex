/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_UpdInvAdoVehiToySch y
                        eliminar y liberar los reg que cumplieron con su fecha de Apartado
                        en TAM_LeadInventarios__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Febrero-2021      Héctor Figueroa             Creación
******************************************************************************* */

global class TAM_UpdInvAdoVehiToySch implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_UpdInvAdoVehiToySch.execute...');
        
        //Crea la consulta para los Leads
        this.sQuery = ' SELECT Id, Name, TAM_Idexterno__c ';
        this.sQuery += ' FROM TAM_LeadInventarios__c ';
        if (!Test.isRunningTest()){
            this.sQuery += ' WHERE TAM_Apartado__c = true And TAM_FechaFinApartado__c != null';
            this.sQuery += ' And TAM_DatosValidosFacturacion__c = false And TAM_FechaFinApartado__c < TODAY';
        }
        this.sQuery += ' Order by CreatedDate ASC';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_UpdInvAdoVehiToySch.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  objUpdInvAdoVehiToyBch      
        TAM_UpdInvAdoVehiToyBch objUpdInvAdoVehiToyBch = new TAM_UpdInvAdoVehiToyBch(sQuery);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objUpdInvAdoVehiToyBch, 50);

    }
    
}