@isTest
private class ReporteEvaluacionServicioControllerTest {
	private static String tipoEvaluacion = 'Integral';
	public static Evaluaciones_Dealer__c getTestDealer() {
		///Registro
		String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
		String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
		RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
		RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
		RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
		
		RecordType recordTypeAPI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType='Actividad_Plan_Integral__c' and DeveloperName='API_TSM' limit 1];
		RecordType recordTypePreguntaKodawari = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Evaluacion_Integral' limit 1];
		RecordType recordTypePreguntaEI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Kodawari' limit 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
		
		User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;

		Document testDoc = new Document(
			Name='testDoc',
			DeveloperName='logo_Retencion',
			FolderId= u.Id		
		);
		insert testDoc;

		Document testDoc2 = new Document(
			Name='Logo_TSM',
			DeveloperName='Logo_TSM',
			FolderId= u.Id		
		);
		insert testDoc2;
		
		Seccion__c seccion01 = new Seccion__c(
			Name = 'Secccion01',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccion01;


		//Seccion de Seguridad.
		Seccion__c seccionSeguridad = new Seccion__c(
			Name = 'Seguridad',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccionSeguridad;
		
		Pregunta__c pregunta01 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta01;

		Pregunta__c pregunta02 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id
		);
		insert pregunta02;		

		Pregunta__c pregunta03 = new Pregunta__c(
			Name = 'AC-03',
			Reactivo__c = '¿Cual es la seccion de seguridad?',
			Seccion_Toyota_Mexico__c = seccionSeguridad.Id
		);
		insert pregunta03;		

		Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta01.Id
		);
		insert oe01;

		Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta02.Id
		);
		insert oe02;

		Objeto_de_Evaluacion_TSM__c oe03 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta03.Id
		);
		insert oe03;

		Account dealer = new Account(
			RecordTypeId = recordTypeDealer.Id,
			Name = 'Test Record'
		);
		insert dealer;

		Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
			RecordTypeId = recordTypeED.Id,
			Consultor_TSM_Titular__c = u.Id,
			Nombre_Dealer__c = dealer.Id
		);
		insert evalDealer;

		Relacion_Seccion_Consultor__c rsc01 = new Relacion_Seccion_Consultor__c(
            Evaluacion_Dealer__c = evalDealer.Id,
            Usuario__c = u.Id,
            Seccion_Toyota_Mexico__c = seccion01.Id
        );
        insert rsc01;
        
        Respuestas_Preguntas_TSM__c resp1 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta01.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=2,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp1;
		
		Respuestas_Preguntas_TSM__c resp2 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta02.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp2;

		Respuestas_Preguntas_TSM__c resp3 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta03.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp3;
                
        update evalDealer;
       
	        Actividad_Plan_Integral__c api01 = new Actividad_Plan_Integral__c(
	            Celula_Area__c='DO',
	            Observaciones__c='Observaciones__c',
	            Condicion_Observada__c='Condicion_Observada__c',
	            Referencia__c='TSM',
	            Cuenta__c=evalDealer.Nombre_Dealer__c,
	            Respuestas_Preguntas_TSM__c=resp1.Id
	        );
	        insert api01;
        
	    	Respuestas_ObjetosTSM__c ro01 = new Respuestas_ObjetosTSM__c(
				Objeto_Evaluacion_Relacionado__c=oe01.Id, 
				Pregunta_Relacionada_del__c=resp1.Pregunta_Relacionada__c, 
				Respuestas_Preguntas_TSM_del__c=resp1.Id,
				Respuesta__c='Si'
	    	);
	    	insert ro01;
	        				
        return evalDealer;
	}
	
	@isTest static void itShould() {
		Test.startTest();
			RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName='EvaluacionRetencionClientes' limit 1];
			RecordType recordTypeDealerEED = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName='Evaluacion_EDER' limit 1];
			RecordType recordTypeDealerSSC = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName='Evaluacion_SSC' limit 1];
			RecordType recordTypeDealerOS = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName='Evaluacion_OperacionServicio' limit 1];
			RecordType recordTypeDealerBP = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName='Evaluacion_BodyPaint' limit 1];
			
			Evaluaciones_Dealer__c evDealer = ReporteEvaluacionServicioControllerTest.getTestDealer();
			ApexPages.StandardController stdController = new ApexPages.StandardController(evDealer);
			ReporteEvaluacionServicioController extController = new ReporteEvaluacionServicioController(stdController);
			String logo = extController.logoFormato;
			extController.getObjEvaluaciones();
			extController.getResumenRespuestasList();
			extController.getValoresBarras2();
			extController.getBarLabels();
			extController.getBarLabels2();
			extController.getValoresBarras();
			extController.getColoresBarras();
			extController.getEtiquetasBarras();
			extController.getLeyendBarras();
			extController.getColoresLeyend();
			extController.getResumenRespuestasSeguridadList();
			extController.getResumenRespuestasSeguridadSize();
			extController.getRetActividadesSeguridad();
			extController.getValoresBarras3();

			evDealer.RecordTypeId = recordTypeDealer.Id;
			update evDealer;

			stdController = new ApexPages.StandardController(evDealer);
			extController = new ReporteEvaluacionServicioController(stdController);
			logo = extController.logoFormato;

			evDealer.RecordTypeId = recordTypeDealerEED.Id;
			update evDealer;

			stdController = new ApexPages.StandardController(evDealer);
			extController = new ReporteEvaluacionServicioController(stdController);
			logo = extController.logoFormato;

			evDealer.RecordTypeId = recordTypeDealerSSC.Id;
			update evDealer;

			stdController = new ApexPages.StandardController(evDealer);
			extController = new ReporteEvaluacionServicioController(stdController);
			logo = extController.logoFormato;

			evDealer.RecordTypeId = recordTypeDealerOS.Id;
			update evDealer;

			stdController = new ApexPages.StandardController(evDealer);
			extController = new ReporteEvaluacionServicioController(stdController);
			logo = extController.logoFormato;

			evDealer.RecordTypeId = recordTypeDealerBP.Id;
			update evDealer;

			stdController = new ApexPages.StandardController(evDealer);
			extController = new ReporteEvaluacionServicioController(stdController);
			logo = extController.logoFormato;
		Test.stopTest();

	}
	
}