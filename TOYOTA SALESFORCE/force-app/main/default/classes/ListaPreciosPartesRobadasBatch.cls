global class ListaPreciosPartesRobadasBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global ListaPreciosPartesRobadasBatch() {
		ParametrosConfiguracionToyota__c pcOrigen = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosOrigen');
		query = 'select IdExterno__c, IsActive, PrecioDealer__c, PrecioMayorista__c, Pricebook2Id, Product2Id, UnitPrice, UseStandardPrice, Product2.ProductCode from PricebookEntry where Pricebook2.Name=\'' + pcOrigen.Valor__c + '\' and IsActive=true';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('Inicia ListaPreciosPartesRobadasBatch ' + scope.size());
   		ParametrosConfiguracionToyota__c pcOrigen = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosOrigen');
   		ParametrosConfiguracionToyota__c pcDestino = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosDestino');

   		ParametrosConfiguracionToyota__c pcPLStandard = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosOrigenName');
   		ParametrosConfiguracionToyota__c pcPLPR = ParametrosConfiguracionToyota__c.getInstance('PRListaPreciosDestinoName');

   		Map<Id,PricebookEntry> mapPBE = new Map<Id,PricebookEntry>();
   		Set<Id> setIdsProducts = new Set<Id>();
   		
   		// Crea mapa de registros de scope para actualizar los registros en la consulta de la lista de precios destino
   		for(PricebookEntry pbe : (List<PricebookEntry>)scope){
   			mapPBE.put(pbe.Product2Id,pbe);
   		}
   		System.debug('mapPBE.size ' + mapPBE.size());
   		//Busca las entradas de lista de precio destino con base en los id de productos de la lista origen(scope)
		List<PricebookEntry> lpbPR = [select Id, Pricebook2Id, Product2Id from PricebookEntry where Pricebook2.Name=:pcDestino.Valor__c and Product2Id IN: mapPBE.keyset()];
		System.debug('Inicia PricebookEntry ' + pcDestino.Valor__c);
		List<PricebookEntry> lpbPRNew = new List<PricebookEntry>();

		//Recorre los registros encontrados en la lista destino para actualizar los valores con los de la lista origen(scope)
   		for(PricebookEntry pbe : lpbPR){
			PricebookEntry pbeO = mapPBE.get(pbe.Product2Id);
			pbe.PrecioDealer__c = pbeO.PrecioDealer__c;
			pbe.PrecioMayorista__c = pbeO.PrecioMayorista__c;
			pbe.UnitPrice = pbeO.UnitPrice;
			pbe.UseStandardPrice = pbeO.UseStandardPrice;
			pbe.IsActive = pbeO.IsActive;
			mapPBE.remove(pbe.Product2Id);//Remueve del mapa de precios origen que se encontraron en la lista destino
   		}
   		System.debug('mapPBE.size ' + mapPBE.size());
   		//Si el mapa de precios origen tiene registros despues de la validación anterior, se crean los registros en la lista destino
   		if(mapPBE.size()>0){
   			List<PriceBook2> lpb2PR = [SELECT Id, Name From PriceBook2 WHERE Name=:pcDestino.Valor__c and IsActive=true];
			if(lpb2PR!=null && lpb2PR.size()==1){
				PriceBook2 pbPR = lpb2PR.get(0);
		   		for(PricebookEntry pbeO : mapPBE.values()){
		   			PricebookEntry pbe = new PricebookEntry();
					pbe.PrecioDealer__c = pbeO.PrecioDealer__c;
					pbe.PrecioMayorista__c = pbeO.PrecioMayorista__c;
					pbe.UnitPrice = pbeO.UnitPrice;
					pbe.UseStandardPrice = pbeO.UseStandardPrice;
					pbe.Product2Id = pbeO.Product2Id;
					pbe.IsActive = pbeO.IsActive;

					pbe.Pricebook2Id = pbPR.Id;					
					pbe.IdExterno__c = pbeO.IdExterno__c.replace(pcPLStandard.Valor__c, pcPLPR.Valor__c);					
					
					lpbPRNew.add(pbe);
		   		}
		   	}else{
		   		System.debug('Error de consistencia en la lista de precios '+pcDestino.Valor__c);
		   	}
	   	}
	   	if(lpbPR.size()>0){   			
			update lpbPR;			
   		}

	   	if(lpbPRNew.size()>0){   			
			upsert lpbPRNew IdExterno__c;
   		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('Termino la ejecución del bloque');
	}
	
}