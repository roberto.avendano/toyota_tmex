@isTest
private class EvaluacionDealerReportSf1ControllerTest {
	
	private static String tipoEvaluacion = 'Integral';
	public static Evaluaciones_Dealer__c getTestDealer(){
		///Registro
		String tipoEvaluacionED = 'Evaluacion_'+tipoEvaluacion;
		String tipoEvaluacionSecc = 'Seccion_'+tipoEvaluacion;
		RecordType recordTypeDealer = [SELECT Id, Name, DeveloperName FROM RecordType Where SobjectType='Account' and DeveloperName='Dealer' limit 1];
		RecordType recordTypeED = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Evaluaciones_Dealer__c' and DeveloperName=:tipoEvaluacionED limit 1];
		RecordType recordTypeSec = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Seccion__c' and DeveloperName=:tipoEvaluacionSecc limit 1];
		
		RecordType recordTypeAPI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType WHERE SobjectType='Actividad_Plan_Integral__c' and DeveloperName='API_TSM' limit 1];
		RecordType recordTypePreguntaKodawari = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Evaluacion_Integral' limit 1];
		RecordType recordTypePreguntaEI = [SELECT Id, Name, SobjectType, DeveloperName, NamespacePrefix FROM RecordType Where SobjectType='Pregunta__c' and DeveloperName='Preguntas_Kodawari' limit 1];
		Profile p = [SELECT Id FROM Profile WHERE Name='Consultor TSM Force'];
		
		User u = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u;
		User u2 = new User(
			Alias = 'ConsTSM', 
			Email='ConsultorTSM2@testorgtoyota.com',
			LastName='TSM',
			ProfileId = p.Id,
			UserName='standarduser2@testorgtoyota.com',
			EmailEncodingKey='UTF-8',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Los_Angeles'
		);
		insert u2;

		Seccion__c seccion01 = new Seccion__c(
			Name = 'Secccion01',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccion01;
		Seccion__c seccion02 = new Seccion__c(
			Name = 'Secccion02',
			RecordTypeId = recordTypeSec.Id
		);
		insert seccion02;

		Pregunta__c pregunta01 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id,
			RecordTypeId = recordTypePreguntaKodawari.Id
		);
		insert pregunta01;

		Pregunta__c pregunta02 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion02.Id,
			RecordTypeId = recordTypePreguntaKodawari.Id
		);
		insert pregunta02;

		Pregunta__c pregunta03 = new Pregunta__c(
			Name = 'AC-01',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion01.Id,
			RecordTypeId = recordTypePreguntaEI.Id
		);
		insert pregunta03;

		Pregunta__c pregunta04 = new Pregunta__c(
			Name = 'AC-02',
			Reactivo__c = 'Reactivo__c',
			Seccion_Toyota_Mexico__c = seccion02.Id,
			RecordTypeId = recordTypePreguntaEI.Id
		);
		insert pregunta04;

		Objeto_de_Evaluacion_TSM__c oe01 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta01.Id
		);
		insert oe01;

		Objeto_de_Evaluacion_TSM__c oe02 = new Objeto_de_Evaluacion_TSM__c(
			Name = 'OE',
			Orden__c = '1',
			Clave_Preg__c = pregunta02.Id
		);
		insert oe02;

		Account dealer = new Account(
			RecordTypeId = recordTypeDealer.Id,
			Name = 'Test Record'
		);
		insert dealer;

		Evaluaciones_Dealer__c evalDealer = new Evaluaciones_Dealer__c(
			RecordTypeId = recordTypeED.Id,
			Consultor_TSM_Titular__c = u2.Id,
			Nombre_Dealer__c = dealer.Id
		);
		insert evalDealer;

		Contact cont01 = new Contact(
			LastName='test contact',
			AccountId= dealer.Id
		);
		insert cont01;

		Respuestas_Preguntas_TSM__c resp1 = new Respuestas_Preguntas_TSM__c(
			Evaluacion_Dealer__c= evalDealer.Id,
			Pregunta_Relacionada__c= pregunta01.Id,
			Ponderacion__c=2,
			PuntosObtenidos__c=1,
			EtapaReactivo__c='En Proceso',
			Observaciones__c='Pregunta de proceso'
		);
		insert resp1;

		Actividad_Plan_Integral__c api01= new Actividad_Plan_Integral__c(
			Celula_Area__c='Atención a clientes',
            Condicion_Observada__c='Falta de actualización en base de datos',
            Actividad_Mejora__c='Generar base de datos diaria',
            Cuenta__c=evalDealer.Nombre_Dealer__c,
            Responsable_Dealer__c='Gerente de Ventas',
            Fecha_Compromiso__c= Date.newInstance(2016, 8, 16),
            Respuestas_Preguntas_TSM__c=resp1.Id,
            RecordTypeId= recordTypeAPI.Id
		);
		insert api01;

        update evalDealer;
        return evalDealer;
	}

	@isTest static void test_method() {
		Evaluaciones_Dealer__c evalDealer = EvaluacionDealerReportSf1ControllerTest.getTestDealer();
		ApexPages.StandardController stdCtl = new ApexPages.StandardController(evalDealer);
		EvaluacionDealerReportSf1Controller controller = new EvaluacionDealerReportSf1Controller(stdCtl);
		for(String head: controller.getCabeceras()){
			System.debug(head);
		}
		List<Actividad_Plan_Integral__c> apis = controller.getApis();
	}

	
}