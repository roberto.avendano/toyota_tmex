global with sharing class InteresAutosDemo  {

    
    public static final Integer ZERO =0;
    public static final Integer AUTODEMOFYACTIVO =1;
    public static final Integer AUTODEMOPROXIMOFY =2;

	public static void montosAutoDFYA(List<CortePresupuesto__c> cortesP){
		System.debug('####Entra InteresAutoDemo montosAutoDFYA List<CortePresupuesto__c> tamanio: '+cortesP.size());
       Set<String>idsCorte =  new Set<String>();
        for (CortePresupuesto__c con: cortesP) {
        	idsCorte.add(con.Id);
        }
		

		List<CortePresupuesto__c>lstcortePresupuestoUpdate =  new List<CortePresupuesto__c>();
		try{
			List<CortePresupuesto__c> lstCortes = [Select InicioCortePresupuesto__c,FinCortePresupuesto__c,SolicitudAutoDemo__r.FechaAltaAutoDemo__c,AsignacionPresupuesto__r.InicioFY__c,MontoAsignado__c,
						SolicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c,SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieActiva__r.InteresPromedio__c, MontoMensual__c,
						SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__r.InteresPromedio__c , AsignacionPresupuesto__r.FinFY__c,
						SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__c
					From CortePresupuesto__c where Id in:idsCorte];
			for(CortePresupuesto__c corte : lstCortes){
				lstcortePresupuestoUpdate.add(costoPromedio(corte, tipoValidacion(corte)));		
			}
			
			update lstcortePresupuestoUpdate;
			System.debug('---->>> se hace el update correctpo lstcortePresupuestoUpdate' );
		}catch(Exception e){
			System.debug('####InteresAutoDemoQUE montosAuto Error:  ' +e.getMessage());
		}
	}

	public static Integer tipoValidacion(CortePresupuesto__c corte){
		Integer mesInicioPresupuesto = corte.InicioCortePresupuesto__c.month();
		Integer mesFechaAltaDemo = corte.SolicitudAutoDemo__r.FechaAltaAutoDemo__c.month();
		//Validacion para Auto Demo /FY Activo
		if(corte.SolicitudAutoDemo__c != null && corte.SolicitudAutoDemo__r.VIN__r.Producto__c != null &&
		corte.SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieActiva__c != null && corte.MontoAsignado__c == null &&
		corte.InicioCortePresupuesto__c >= corte.AsignacionPresupuesto__r.InicioFY__c && corte.FinCortePresupuesto__c <= corte.AsignacionPresupuesto__r.FinFY__c){
				return AUTODEMOFYACTIVO;
		}else if(corte.SolicitudAutoDemo__c != null &&
		corte.InicioCortePresupuesto__c >= corte.AsignacionPresupuesto__r.InicioFY__c && corte.InicioCortePresupuesto__c >= corte.AsignacionPresupuesto__r.FinFY__c
		&& corte.SolicitudAutoDemo__r.VIN__r.Producto__c != null &&
		corte.SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__c != null && corte.MontoAsignado__c == null ){//Validacion para Auto Proximo /FY 
			return AUTODEMOPROXIMOFY;
		}
		return ZERO;
		
	}
	
	public static CortePresupuesto__c costoPromedio(CortePresupuesto__c corte , Integer tipo){
		Decimal diferencia = 0.0;
		if(tipo == AUTODEMOFYACTIVO){
			corte.CostoPromedio__c = corte.SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieActiva__r.InteresPromedio__c;
		}
		if(tipo == AUTODEMOPROXIMOFY){
			corte.CostoPromedio__c = corte.SolicitudAutoDemo__r.VIN__r.Producto__r.Serie__r.InteresSerieProximoFY__r.InteresPromedio__c;
		}
		Integer day = corte.FinCortePresupuesto__c.day();
		System.debug('## 	corte.FinCortePresupuesto__c.day(); '+corte.FinCortePresupuesto__c.day());
		Decimal difFechas = 1.0;
		System.debug('## 	corte.solicitudAutoDemo__r.FechaAltaAutoDemo__c == corte.InicioCortePresupuesto__c '+corte.solicitudAutoDemo__r.FechaAltaAutoDemo__c.month() +' '+corte.InicioCortePresupuesto__c.month());
		System.debug('## 	corte.solicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c == corte.InicioCortePresupuesto__c '+corte.solicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c.month()  +' '+corte.InicioCortePresupuesto__c.month());
		
		
		if(corte.solicitudAutoDemo__r.FechaAltaAutoDemo__c.month() == corte.InicioCortePresupuesto__c.month()){
			 difFechas = corte.FinCortePresupuesto__c.daysBetween(corte.SolicitudAutoDemo__r.FechaAltaAutoDemo__c);
			 difFechas = math.abs(difFechas);
			 difFechas = difFechas + AUTODEMOFYACTIVO;
			  diferencia = difFechas / day;
			  System.debug('## 	diferencia  '+ diferencia);
			corte.MontoAsignado__c =  (corte.MontoMensual__c) * (diferencia);
			System.debug('## 	MontoAsignado__c 1 '+ corte.MontoAsignado__c);
		}else if(corte.SolicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c.month() == corte.InicioCortePresupuesto__c.month()){
			difFechas = corte.SolicitudAutoDemo__r.FechaBajaAutoDemoVigencia__c.daysBetween(corte.InicioCortePresupuesto__c);
			difFechas = math.abs(difFechas);
			 difFechas = difFechas + AUTODEMOFYACTIVO;
			 diferencia = difFechas / day;
			  System.debug('## 	diferencia  '+ diferencia);
			corte.MontoAsignado__c =  corte.MontoMensual__c * (diferencia);
			System.debug('## 	MontoAsignado__c 2 '+ corte.MontoAsignado__c);
		}else{
			corte.MontoAsignado__c	= corte.MontoMensual__c;
		}
		return corte;
		
				
	}
	

	public static void montosAutoPAGOCompletoSolicitudes(List<SolicitudAutoDemo__c> solicitudes){
		System.debug('####Entra InteresAutoDemo montosAutoPAGOCompletoSolicitudes List<SolicitudAutoDemo__c> tamanio: '+solicitudes.size());
			Decimal diferencia = 0.0;
       Set<String>idsSolicitudes =  new Set<String>();
        for (SolicitudAutoDemo__c sol: solicitudes) {
        	idsSolicitudes.add(sol.Id);
        }
		

		List<CortePresupuesto__c>lstcortePresupuestoUpdate =  new List<CortePresupuesto__c>();
		try{
			List<CortePresupuesto__c> lstCortes = [Select InicioCortePresupuesto__c,FinCortePresupuesto__c, MontoAsignado__c,
			 solicitudAutoDemo__r.FechaBajaPagoCompletado__c,solicitudAutoDemo__r.EstatusAutoDemo__c,MontoMensual__c
			From CortePresupuesto__c where solicitudAutoDemo__c in:idsSolicitudes And solicitudAutoDemo__r.EstatusAutoDemo__c = 'Inactivo'];
			for(CortePresupuesto__c corte : lstCortes){
				System.debug('## 	corte.CostoPromedio__c '+corte.solicitudAutoDemo__r.FechaBajaPagoCompletado__c.month());
				System.debug('## 	corte.CostoPromedio__c '+corte.InicioCortePresupuesto__c.month());
				if(corte.solicitudAutoDemo__r.FechaBajaPagoCompletado__c != null && (corte.solicitudAutoDemo__r.FechaBajaPagoCompletado__c.month() == corte.InicioCortePresupuesto__c.month())){
					Integer day = corte.FinCortePresupuesto__c.day();
					Decimal montoPorDia = corte.MontoMensual__c / day;
					Decimal difFechas = 1.0;
					difFechas = corte.InicioCortePresupuesto__c.daysBetween(corte.SolicitudAutoDemo__r.FechaBajaPagoCompletado__c);
					difFechas = math.abs(difFechas);
			 		difFechas = difFechas + AUTODEMOFYACTIVO;
					corte.MontoAsignado__c =  montoPorDia * (difFechas);	
					lstcortePresupuestoUpdate.add(corte);	
				}				
			}
						
			update lstcortePresupuestoUpdate;
			System.debug('---->>> se hace el update correcto montosAutoPAGOCompletoSolicitudes lstcortePresupuestoUpdate' );
		}catch(Exception e){
			System.debug('####montosAutoPAGOCompleto  Error:  ' +e.getMessage());
		}
	}
	
}