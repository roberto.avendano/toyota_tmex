@isTest
private class ResumenMasterBOBatchTest {
    //Uno inactivo, otro que sea activo con tipo de registro master tnt, otro activo con una cantidad diferente
    static testMethod void myUnitTest() {
        MasterBO__c objmasterBO1 = new MasterBO__c();
        MasterBO__c objmasterBO2 = new MasterBO__c();
        MasterBO__c objmasterBO3 = new MasterBO__c();
        MasterBO__c objmasterBO4 = new MasterBO__c();

        Product2 prod= new Product2();
        prod.Name='Test';
        prod.Description='Test';
        prod.ProductCode='0';
        prod.IdExternoProducto__c='0';
        insert prod;
        
        Account cuenta = new Account();
        cuenta.Name='TestCuenta';
        cuenta.Promotor_Kaizen__c=UserInfo.getUserId();
        insert cuenta;
        
        RecordType recordMasterBOID= [SELECT ID from RecordType WHERE DeveloperName='MasterBO'];
        RecordType recordMasterTNTID= [SELECT ID from RecordType WHERE DeveloperName='MasterTNT'];

        Modelo_PDC__c modelo= new Modelo_PDC__c();
        modelo.MdlCd__c='mtest';
        insert modelo;
        
        //OBJETO MASTER BO 1 STATUS INACTIVO
        objmasterBO1.Status__c='Inactive';
        objmasterBO1.BOQTYDealerOrder__c=5;
        objmasterBO1.PartNameId__c=prod.ID;
        objmasterBO1.DealerId__c=cuenta.ID;
        objmasterBO1.MdlCdId__c=modelo.ID;
        objmasterBO1.RecordTypeId=recordMasterBOID.ID;
        objmasterBO1.AsDate__c = Date.today().addDays(-1);

        //OBJETO MASTER BO 2 MASTER TNT
        objmasterBO2.Status__c='Active';
        objmasterBO2.BOQTYDealerOrder__c=5;
        objmasterBO2.PartNameId__c=prod.ID;
        objmasterBO2.DealerId__c=cuenta.ID;
        objmasterBO2.MdlCdId__c=modelo.ID;
        objmasterBO2.RecordTypeId=recordMasterTNTID.ID;
        objmasterBO2.AsDate__c = Date.today();
        
        //OBJETO MASTER BO 3 CANTIDAD=5
        objmasterBO3.Status__c='Active';
        objmasterBO3.BOQTYDealerOrder__c=5;
        objmasterBO3.PartNameId__c=prod.ID;
        objmasterBO3.DealerId__c=cuenta.ID;
        objmasterBO3.MdlCdId__c=modelo.ID;
        objmasterBO3.RecordTypeId=recordMasterBOID.ID;
        objmasterBO3.AsDate__c = Date.today();

        //OBJETO MASTER BO 4 CANTIDAD=2
        objmasterBO4.Status__c='Active';
        objmasterBO4.BOQTYDealerOrder__c=2;
        objmasterBO4.PartNameId__c=prod.ID;
        objmasterBO4.DealerId__c=cuenta.ID;
        objmasterBO4.MdlCdId__c=modelo.ID;
        objmasterBO4.RecordTypeId=recordMasterBOID.ID;
        objmasterBO4.AsDate__c = Date.today();

        insert objmasterBO1;
        insert objmasterBO2;
        insert objmasterBO3;
        insert objmasterBO4;

        Test.startTest();
            ResumenMasterBOBatch batchMasterBO = new ResumenMasterBOBatch('Select PartNameId__c, Id, BOQTYDealerOrder__c from MasterBO__c where StatusByAsDate__c=\'Yes\' and RecordType.DeveloperName= \'MasterBO\'');
            ResumenMasterBOBatch batchMasterBO2 = new ResumenMasterBOBatch();
            Database.executeBatch(batchMasterBO);
        Test.stopTest();

        MasterBO__c objMaster1 = [SELECT CountOrder__c, TotalBOQty__c, ID, Status__c, Name from MasterBO__c WHERE ID=:objmasterBO1.ID];
        MasterBO__c objMaster2 = [SELECT CountOrder__c, TotalBOQty__c, ID, Status__c, Name from MasterBO__c WHERE ID=:objmasterBO2.ID];
        MasterBO__c objMaster3 = [SELECT CountOrder__c, TotalBOQty__c, ID, Status__c, Name from MasterBO__c WHERE ID=:objmasterBO3.ID];
        MasterBO__c objMaster4 = [SELECT CountOrder__c, TotalBOQty__c, ID, Status__c, Name from MasterBO__c WHERE ID=:objmasterBO4.ID];
        
        System.debug(objMaster1);
        System.debug(objMaster2);
        System.debug(objMaster3);
        System.debug(objMaster4);

        //System.assertEquals(1, objMaster1.CountOrder__c);
        //System.assertEquals(1, objMaster2.CountOrder__c);

        //validos
        System.assertEquals(2, objMaster3.CountOrder__c);
        System.assertEquals(2, objMaster4.CountOrder__c);

        //System.assertEquals(5,objMaster1.TotalBOQty__c);
        //System.assertEquals(5,objMaster2.TotalBOQty__c);
        
        //validos
        System.assertEquals(7,objMaster3.TotalBOQty__c);
        System.assertEquals(7,objMaster4.TotalBOQty__c);
    }
}