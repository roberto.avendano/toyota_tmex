/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del archivo de Ventas-Servicios
    
    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    02-Agosto-2019    Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_VehiculoServicioPaso_tgr_handler extends TriggerHandler {

	private List<TAM_VehiculoServicioPaso__c> ventasHList;
	private Map<Id,TAM_VehiculoServicioPaso__c> mapVentas;
    private Map<String,Map<String,RecordType>> recordTypesMap;
	private Map<String,String> mapAnios = new Map<String,String>{'Jan' =>'01','Feb' =>'02','Mar' =>'03','Apr' =>'04','May' =>'05','Jun' =>'06','Jul' =>'07','Aug' =>'08','Sep' =>'09','Oct' =>'10','Nov' =>'11','Dec' =>'12'};


    public TAM_VehiculoServicioPaso_tgr_handler() {
		this.ventasHList = Trigger.new;
		this.mapVentas = (Map<Id,TAM_VehiculoServicioPaso__c>) Trigger.newMap;
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID; 
	}

	public override void afterInsert(){
		validateRecords(ventasHList);
		eliminaRecords(mapVentas);
	}

	private void eliminaRecords(Map<Id,TAM_VehiculoServicioPaso__c> mapVentas){
		System.debug('ENTRO A TAM_VehiculoServicioPaso_tgr_handler eliminaRecords...');		
		Set<String> vines = new Set<String>();
		for(TAM_VehiculoServicioPaso__c v: mapVentas.values()){
			vines.add(v.id);
		}
		if(vines.size()>0){
			List<TAM_VehiculoServicioPaso__c> borrar = new List<TAM_VehiculoServicioPaso__c>();
			for(TAM_VehiculoServicioPaso__c v : [SELECT Id FROM TAM_VehiculoServicioPaso__c WHERE id IN :vines]){
				borrar.add(v);
			}
			System.debug('ENTRO A TAM_VehiculoServicioPaso_tgr_handler eliminaRecords borrar: ' + borrar);			
			if(borrar.size()>0){
				//delete borrar;				
				List<Database.DeleteResult> lDtbUpsRes = Database.delete(borrar, false);
				//Ve si hubo error
				for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						System.debug('Error a la hora de eliminar el reg ' + objDtbUpsRes.getErrors()[0].getMessage());	
				}//Fin del for para lDtbUpsRes
			}
		}
	}

	private void validateRecords(List<TAM_VehiculoServicioPaso__c> vehiculoList){
		System.debug('ENTRO A TAM_VehiculoServicioPaso_tgr_handler.validateRecords...');
		String sToyota = 'TOYOTA FINANCIAL SERVICES';
		String sToyotaCompleto = 'TOYOTA FINANCIAL SERVICES MEXICO SA DE CV';
		String sTAM_QTY = '1';

        Map<String, Account> dealers;
        System.debug(JSON.serialize(mapVentas));
        
	    String VaRtAccRegPM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
    	String VaRtAccRegPF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
    	String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();    	
    	String VaRtContRegPF = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contactos-Clientes').getRecordTypeId();    	
    	String VaRtProdReg = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
    	//String VaRtPriceBookIdReg = [Select p.Name, p.IsStandard, p.Id, IdExternoListaPrecios__c From Pricebook2 p where p.IsStandard = true].id;
    	//String VaNameStdPriceBookIdReg = [Select p.Name, p.IsStandard, p.Id, IdExternoListaPrecios__c From Pricebook2 p where p.IsStandard = true].IdExternoListaPrecios__c;
    	String VaRtStdPriceBookIdReg;
    	String VaNameStdPriceBookIdReg;
    	//No es una prueba
    	if (!Test.isRunningTest()){ 
    		VaRtStdPriceBookIdReg =[Select p.Name, p.IsStandard, p.Id From Pricebook2 p where p.IsStandard = true].id;
			VaNameStdPriceBookIdReg = [Select p.Name, p.IsStandard, p.Id, IdExternoListaPrecios__c From Pricebook2 p where p.IsStandard = true].IdExternoListaPrecios__c;    		
    	}//Fin si !Test.isRunningTest()
    	//Es una prueba	
    	if (Test.isRunningTest()){
    		VaRtStdPriceBookIdReg = Test.getStandardPricebookId();
    		VaNameStdPriceBookIdReg = Test.getStandardPricebookId();
    	}//Fin si Test.isRunningTest()
		String[] setMeses = new list<String>{'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'}; 

        Set<String> dealersCode = new Set<String>();               	
    		
		Set<String> setIdDistribuidor = new Set<String>();
		map<String, Account> mapDistribuidores = new map<String, Account>();

    	map<String, Account> mapClientesUps = new Map<String, Account>();
		Set<String> setIdCtesCreados = new Set<String>();    	
    	map<String, String> mapIdClientesId = new Map<String, String>();

    	map<String, Contact> mapContactosUps = new Map<String, Contact>();
		Set<String> setIdContactCreados = new Set<String>();
		Set<String> setErroresContactosUps = new Set<String>();		    	
    	map<String, String> mapIdContactosId = new Map<String, String>();
		
		map<String, String> mapVinOppIdExtCliente = new map<String, String>();
		Set<String> setErroresAccUps = new Set<String>();

		map<String, String> mapVinOppIdExtClienteContacto = new map<String, String>();
		Set<String> setErroresAccContactUps = new Set<String>();
		
		Set<String> setIdVines = new Set<String>();
        Map<String, Vehiculo__c> mapVehiculoCons = new Map<String, Vehiculo__c>();		
		Set<String> setErroresVehiUps = new Set<String>();
		Set<String> setSuccesVehiUps = new Set<String>();		

		Map<String, TAM_Servicio__c> mapTAMServiciosUps = new Map<String, TAM_Servicio__c>();
		Set<String> setErroresServVehic = new Set<String>();
		Set<String> setSuccesServVehic = new Set<String>();		

		Set<String> setProduct2 = new Set<String>();
        Map<String, Product2> mapProduct2 = new Map<String, Product2>();

		Map<String, String> mapVidIdProd = new Map<String, String>();
		Map<String, String> mapIdAccountName = new Map<String, String>();

		//Para guardar los daros del VIN y los sevicios asociados a ese 
		Map<String, List<TAM_Servicio__c>> mapIdVinListServicios = new Map<String, List<TAM_Servicio__c>>();

		Map<String, TAM_ContactosServicios__c> mapTAMContactosServiciosUps = new Map<String, TAM_ContactosServicios__c>();
		Set<String> setErroresContServ = new Set<String>();
		Set<String> setSuccesContServ = new Set<String>();		

		Set<String> sIdVin = new Set<String>();
		map<String, Opportunity> mapOppCons = new map<String, Opportunity>();
		map<String, Opportunity> mapOppUps = new map<String, Opportunity>();		
		Set<String> setErroresOppUps = new Set<String>();
		Set<String> setSuccessIdOppUps = new Set<String>();		
		map<String, String> mapVinOppIdUps = new map<String, String>();
				
		//Recorre la lista de reg que se estan nsertando
        for(TAM_VehiculoServicioPaso__c v: vehiculoList){
        	
			//Actualiza los VINES que vienen en el campo de la Opp
			setIdVines.add(v.Name);
			
			//Para obtener los datos de los distrubuidores
            if(v.TAM_IdDealerCode__c != null)
        		setIdDistribuidor.add(v.TAM_IdDealerCode__c);
			//Para obtener los datos de los Productos en Product2
            if(v.TAM_Model__c != null && v.TAM_YearModelo__c != null)
        		setProduct2.add(v.TAM_Model__c + '' + v.TAM_YearModelo__c);
        	//Agrega el VIN al mapa de mapVidIdProd
        	if (v.Name != null && v.TAM_Model__c != null && v.TAM_YearModelo__c != null)
        		mapVidIdProd.put(v.Name, v.TAM_Model__c + '' + v.TAM_YearModelo__c);
			//Para obtener los datos de los distrubuidores
            if(v.Name != null)
        		sIdVin.add(v.Name);        		
        }
       	System.debug('EN validateRecords setIdDistribuidor: '+ setIdDistribuidor);
       	System.debug('EN validateRecords mapVidIdProd: '+ mapVidIdProd);
       	       	
        //Consulta todos los colores internos 
        for (Account Cliente : [Select c.Name, c.Id, c.Codigo_Distribuidor__c  From Account c
        		Where Codigo_Distribuidor__c IN :setIdDistribuidor]){
        	mapDistribuidores.put(Cliente.Codigo_Distribuidor__c, Cliente);
        }

        //Consulta todos los productos 
        for (Product2 Producto : [Select p.Id, p.NombreVersion__c, p.Name, p.IdExternoProducto__c, p.Anio__c 
        		From Product2 p where RecordTypeId = :VaRtProdReg And IdExternoProducto__c IN :setProduct2]){
        	mapProduct2.put(Producto.IdExternoProducto__c, Producto);
        }

        //Consulta todos los productos 
        for (Vehiculo__c vehiculo : [Select v.Name, v.Id_Externo__c, v.Id From Vehiculo__c v  
        	where Id_Externo__c  IN :setIdVines]){
        	mapVehiculoCons.put(vehiculo.Id_Externo__c, vehiculo);
        }

        //Consulta todos los colores internos 
        for (Opportunity objOpp : [Select c.Id, CloseDate, TAM_Vin__c, Account.TAM_IdExternoNombre__c 
        	From Opportunity c Where TAM_Vin__c IN :sIdVin And c.AccountId != null]){
        	mapOppCons.put(objOpp.TAM_Vin__c, objOpp);        	
        }
        System.debug('EN validateRecords mapOppCons: '+ mapOppCons.KeySet());
        System.debug('EN validateRecords mapOppCons: '+ mapOppCons.values());        
       	
       	//Inicializa un SavePoint para hacer el rollback en caso de error
       	SavePoint svUpsDatos = Database.setSavepoint();

		//Un try para los errores
        try{

        	//Recorremos los reg de ventas
        	for(TAM_VehiculoServicioPaso__c v: vehiculoList){

                Boolean bPersonaMoral = false;
                String sLlaveCompuestaNombre = '';
    	    	String[] sLlaveNombreCompuesto = new List<String>();

				//****** PARA LOS DATOS DE LOS SERVICIOS AUTOMOVIL (TAM_Servicio__c) *****///
				//Crea el objeto del tipo TAM_Servicio__c
				if (v.Name != null){
					
					//Crea la llave compuesta para cada servicio
					String sNombreServicio = v.Name + '-' + v.TAM_ServiceRepDescription__c;
					sNombreServicio = sNombreServicio.length() > 80 ? sNombreServicio.substring(0, 79) : sNombreServicio; 					
					String sLlaveExterna;
					String sFechaCargaArchivo;
					//Ve si tiene datos la orden
					if (v.TAM_RepairOrder__c != null && v.TAM_ServiceDate__c != null && v.TAM_ServiceRepDescription__c != null ){
						String sTAMRepairOrder = v.TAM_RepairOrder__c;
						String sTAMRepairOrder5 = sTAMRepairOrder.trim();
						String sTAMRepairOrder4 = sTAMRepairOrder5.replaceAll(' ', '');
	                	//System.debug('EN validateRecords sTAMRepairOrder4: ' + sTAMRepairOrder4);						
						String sTAMRepairOrder3 = sTAMRepairOrder4.replace(' ', '');
	                	//System.debug('EN validateRecords sTAMRepairOrder3: ' + sTAMRepairOrder3);						
						String sTAMRepairOrder2 = sTAMRepairOrder3.replace(' ','');		
	                	//System.debug('EN validateRecords sTAMRepairOrder2: ' + sTAMRepairOrder2);																						
						String sTAMRepairOrder1 = sTAMRepairOrder2.replace(' ','');
	                	//System.debug('EN validateRecords sTAMRepairOrder1: ' + sTAMRepairOrder1);						
						String sTAMRepairOrder0 = sTAMRepairOrder1.replace(' ','');
	                	//System.debug('EN validateRecords sTAMRepairOrder0: ' + sTAMRepairOrder0);						
						String sTAMServiceRepDescription = v.TAM_ServiceRepDescription__c;
						sTAMServiceRepDescription = sTAMServiceRepDescription.length() > 80 ? sTAMServiceRepDescription.substring(0, 79) : sTAMServiceRepDescription;						
						sLlaveExterna = v.Name + '-' + v.TAM_RepairOrder__c + '-' + (ConvierteFecha(v.TAM_ServiceDate__c) != null ? ConvierteFecha(v.TAM_ServiceDate__c) : '') + '-' + sTAMServiceRepDescription + '-' + v.TAM_MontoTotal__c;
		                //Para el LA FECHA DE CARGA DEL ARCHIVO 
		                sFechaCargaArchivo = v.TAM_Year__c != null && v.TAM_Month__c != '' && v.TAM_Day__c != null ? v.TAM_Year__c.trim() + '' + v.TAM_Month__c.trim() + '' + v.TAM_Day__c.trim() : '';
						
					}//Fin si v.TAM_RepairOrder__c != null && v.TAM_ServiceDate__c != null && v.TAM_ServiceRepDescription__c != null 
                	System.debug('EN validateRecords sLlaveExterna: ' + sLlaveExterna);
                	
                	//Ve si no tiene lista de precios entonces no crees el objeto
                	if (sLlaveExterna != null && mapVehiculoCons.containsKey(v.Name)){
                		System.debug('EN validateRecords sTAMMontoTotalFinal: ' + v.TAM_MontoTotal__c);                		
                		String sTAMMontoTotalPaso = v.TAM_MontoTotal__c != null ? v.TAM_MontoTotal__c : '0.00';
                		String sTAMMontoTotalPesos = sTAMMontoTotalPaso.replace('$','');
                		String sTAMMontoTotalComas = sTAMMontoTotalPesos.replace(',','');
                		String sTAMMontoTotalFinal = sTAMMontoTotalComas.trim();
                		System.debug('EN validateRecords sTAMMontoTotalFinal: ' + sTAMMontoTotalFinal);                		
						//Crea el objeto del tipo OpportunityLineItem 
						TAM_Servicio__c objServiciosPaso = new TAM_Servicio__c(
							TAM_ID_EXTERNO__c = sLlaveExterna,						
							Name = sNombreServicio,
							TAM_Vehiculo__c = mapVehiculoCons.get(v.Name).id,
							TAM_VIN__c = v.Name, 
							TAM_TIPO_SERVICIO__c = v.TAM_ServiceType__c, 
							TAM_ORDEN_DE_REPARACION__c = v.TAM_RepairOrder__c, 
							TAM_ODOMETRO_ULTIMO__c = v.TAM_PreviousOdometer__c, 
							TAM_ODOMETRO_ACTUAL__c = v.TAM_CurrentOdometer__c, 
							TAM_MONTO_TOTAL__c = sTAMMontoTotalFinal.isNumeric() ? Decimal.valueOf(sTAMMontoTotalFinal) : 0.00, 
							TAM_FECHA_SERVICIO__c = ConvierteFecha(v.TAM_ServiceDate__c) != null ? Date.valueOf(ConvierteFecha(v.TAM_ServiceDate__c)) : null, 
							TAM_DESCRIPCION_SERVICIO__c = v.TAM_ServiceRepDescription__c,
							TAM_FechaCargaArchivo__c = sFechaCargaArchivo,
							TAM_IdExternoNombreCliente__c =  mapOppCons.containsKey(v.Name) ? mapOppCons.get(v.Name).Account.TAM_IdExternoNombre__c : null
						);
						//Agregalo al mapa de mapTAMServiciosUps 
						mapTAMServiciosUps.put(sLlaveExterna, objServiciosPaso);
                	}//sIdPriceBookEntry != null
                	//Crea el reg para actul la opp
                	if (mapOppCons.containsKey(v.Name)){
                		mapOppUps.put(v.Name, new Opportunity(
                				id = mapOppCons.get(v.Name).id, 
                				CloseDate = Date.valueOf(ConvierteFecha(v.TAM_dofu__c)) 
                			)
                		);
                	}//Fin si mapOppCons.containsKey(v.Name) 
				}//Fin si v.Name != null
				//****** FIN PARA LOS DATOS DE LOS SERVICIOS AUTOMOVIL (TAM_Servicio__c) *****///

        	}//Fin del for para vehiculoList
       		System.debug('EN validateRecords mapOppUps: ' + mapOppUps.KeySet());
       		System.debug('EN validateRecords mapOppUps: ' + mapOppUps.Values());
			//Antes que nada actualiza las oportunidades que vienen en el mapa de mapOppUps
			if (!mapOppUps.isEmpty()){
        		Integer icntUpsCtes = 0;				
        		List<Opportunity> lOppDistUps = mapOppUps.values();				
        		//Actualiza las Opp 
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lOppDistUps, Opportunity.Id, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess()){
						setErroresOppUps.add('En la Opp: ' + lOppDistUps.get(icntUpsCtes).TAM_Distribuidor__c + ' hubo error a la hora de crear/Actualizar el precio de lista de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
	           			System.debug('En la Opp: ' + lOppDistUps.get(icntUpsCtes).TAM_Distribuidor__c + ' hubo error a la hora de crear/Actualizar el precio de lista de la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
					}
					if (objDtbUpsRes.isSuccess()){
						setSuccessIdOppUps.add(objDtbUpsRes.getId());
	           			System.debug('EN validateRecords objDtbUpsRes.getId() Opportunity: ' + objDtbUpsRes.getId());							
					}
					icntUpsCtes++;	
				}//Fin del for para lDtbUpsRes
			}//Fin si !mapOppUps.isEmpty()
           	System.debug('EN validateRecords setErroresOppUps: ' + setErroresOppUps);
           	System.debug('EN validateRecords setSuccessIdOppUps: ' + setSuccessIdOppUps);
        	
       		System.debug('EN validateRecords mapTAMServiciosUps: ' + mapTAMServiciosUps.KeySet());
       		System.debug('EN validateRecords mapTAMServiciosUps: ' + mapTAMServiciosUps.Values());
			//Ve si tiene algo el mapa de mapTAMServiciosUps
        	if (!mapTAMServiciosUps.isEmpty()){
   	        	Integer icntUpsCtes = 0;        		
        		List<TAM_Servicio__c> lServicioVehiculosUps = mapTAMServiciosUps.values();        		
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lServicioVehiculosUps, TAM_Servicio__c.TAM_ID_EXTERNO__c, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						setErroresServVehic.add('En el servicio: ' + lServicioVehiculosUps.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
					if (objDtbUpsRes.isSuccess()){
						setSuccesServVehic.add(objDtbUpsRes.getId());
       					System.debug('EN validateRecords objDtbUpsRes.getId() TAM_Servicio__c: ' + objDtbUpsRes.getId());						
					}//Fin si objDtbUpsRes.isSuccess()
					icntUpsCtes++;	
				}//Fin del for para lDtbUpsRes
        	}//Fin si !objServiciosPaso.isEmpty()			
       		System.debug('EN validateRecords setErroresServVehic: ' + setErroresServVehic);
       		System.debug('EN validateRecords setSuccesServVehic: ' + setSuccesServVehic);
			//Consulta los servicios asociados a los vines que se acaban de crear
			for (TAM_Servicio__c Servicio : [Select id, TAM_VIN__c, TAM_FechaCargaArchivo__c 
				From TAM_Servicio__c Where Id IN : setSuccesServVehic]){
				//Metelo al mapa de mapIdVinListServicios	
            	if (mapIdVinListServicios.containsKey(Servicio.TAM_VIN__c))            		
        			mapIdVinListServicios.get(Servicio.TAM_VIN__c).add(Servicio);
            	if (!mapIdVinListServicios.containsKey(Servicio.TAM_VIN__c))            		
        			mapIdVinListServicios.put(Servicio.TAM_VIN__c, new List<TAM_Servicio__c>{Servicio});        			
			}//Fin del for para TAM_Servicio__c
	       	System.debug('EN validateRecords mapIdVinListServicios: '+ mapIdVinListServicios.KeySet());
    	   	System.debug('EN validateRecords mapIdVinListServicios: '+ mapIdVinListServicios.Values());
			
			//Consulta todos los contactos asociados a los vines
			for (AccountContactRelation CtaCoontactRel : [SELECT AccountId, ContactId, TAM_IdExterno__c, TAM_VIN__c,
					Contact.TAM_FechaCargaArchivo__c 
					FROM AccountContactRelation	Where TAM_VIN__c IN : mapIdVinListServicios.keySet()]){
				//Ve si existe CtaCoontactRel.TAM_VIN__c en mapIdVinListServicios
				if (mapIdVinListServicios.containsKey(CtaCoontactRel.TAM_VIN__c)){
					//Recorre la lsta de servicios
					for (TAM_Servicio__c Servicio : mapIdVinListServicios.get(CtaCoontactRel.TAM_VIN__c)){
    	   				//System.debug('EN validateRecords CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c: '+ CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c + ' Servicio.TAM_FechaCargaArchivo__c: ' + Servicio.TAM_FechaCargaArchivo__c);						
						//Ve si el campo de CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c es igual al  campo Servicio.TAM_FechaCargaArchivo__c
						if (CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c == Servicio.TAM_FechaCargaArchivo__c){
	    	   				System.debug('EN validateRecords YA LO ENCOTRE CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c: '+ CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c + ' Servicio.TAM_FechaCargaArchivo__c: ' + Servicio.TAM_FechaCargaArchivo__c);							
							//Toma los datos del contacto y crea una llave foranea
							String sIdExterno = CtaCoontactRel.TAM_VIN__c + '-' + CtaCoontactRel.ContactId + '-' + Servicio.id;  
							//Crea el objeto del tipo TAM_ContactosServicios__c
							TAM_ContactosServicios__c objTAM_ContactosServicios = new TAM_ContactosServicios__c(
								Name = CtaCoontactRel.TAM_VIN__c,
								TAM_IdExterno__c = sIdExterno,
								TAM_Contacto__c = CtaCoontactRel.ContactId,
								TAM_Servicio__c = Servicio.id 
							);
							//Metelo al mapa de mapTAMContactosServiciosUps
							mapTAMContactosServiciosUps.put(sIdExterno, objTAM_ContactosServicios);
						}//Fin si CtaCoontactRel.Contact.TAM_FechaCargaArchivo__c == Servicio.TAM_FechaCargaArchivo__c 
					} //Fin del for para mapIdVinListServicios.get(CtaCoontactRel.TAM_VIN__c)
				}//Fin si mapIdVinListServicios.containsKey(CtaCoontactRel.TAM_VIN__c)
			}//Fin del for para AccountContactRelation
	       	System.debug('EN validateRecords mapTAMContactosServiciosUps: '+ mapTAMContactosServiciosUps.KeySet());
    	   	System.debug('EN validateRecords mapTAMContactosServiciosUps: '+ mapTAMContactosServiciosUps.Values());

			//Ve si tiene algo el mapa de mapTAMContactosServiciosUps
        	if (!mapTAMContactosServiciosUps.isEmpty()){
   	        	Integer icntUpsCtes = 0;        		
        		List<TAM_ContactosServicios__c> lServicioVehiculosUps = mapTAMContactosServiciosUps.values();        		
				List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lServicioVehiculosUps, TAM_ContactosServicios__c.TAM_IdExterno__c, false);
				//Ve si hubo error
				for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
					if (!objDtbUpsRes.isSuccess())
						setErroresContServ.add('En el Contacto-Servicio: ' + lServicioVehiculosUps.get(icntUpsCtes).Name + ' hubo error a la hora de crear/Actualizar: ' + objDtbUpsRes.getErrors()[0].getMessage());
					if (objDtbUpsRes.isSuccess()){
						setSuccesContServ.add(objDtbUpsRes.getId());
       					System.debug('EN validateRecords objDtbUpsRes.getId() TAM_ContactosServicios__c: ' + objDtbUpsRes.getId());						
					}//Fin si objDtbUpsRes.isSuccess()
					icntUpsCtes++;	
				}//Fin del for para lDtbUpsRes
        	}//Fin si !objServiciosPaso.isEmpty()			
       		System.debug('EN validateRecords setErroresContServ: ' + setErroresContServ);
       		System.debug('EN validateRecords setSuccesContServ: ' + setSuccesContServ);	
			
        } catch(Exception ex){
            System.debug('Error upsert objServiciosPaso: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber());
        }		

		//Un rollback para todo solo estamos en pruebas
		//Database.rollback(svUpsDatos);

	}

	@TestVisible
    private String ConvierteFechaFechHora(String dtFecha, String sHora){
    	System.debug('Entro a ConvierteFechaFechHora dtFecha: ' + dtFecha + ' sHora: ' + sHora);
    	String sFechaPaso;
    	String sHoraPaso = String.valueOf(sHora);

        try{

	    	//Destipa la hora
	    	List<String> lHoraPaso = new List<String>();
	    	if (sHoraPaso != null){
	    		String sHoraPaso2 = sHoraPaso.replace('.',':');
	    		System.debug('Entro a ConvierteFechaFechHora sHoraPaso: ' + sHoraPaso + ' sHoraPaso2: ' + sHoraPaso2);
	    		lHoraPaso = sHoraPaso2.split(':'); //19.05.30.077173
	    		System.debug('Entro a ConvierteFechaFechHora lHoraPaso: ' + lHoraPaso.size());    		
	    	}
	
	    	//Toma el parametro de dtFecha
	    	Time dHora = lHoraPaso.size() >= 3 ? Time.newInstance(Integer.valueOf(lHoraPaso[0]),Integer.valueOf(lHoraPaso[1]),Integer.valueOf(lHoraPaso[2]),0) : Time.newInstance(0,0,0,0);
	    	System.debug('Entro a ConvierteFechaFechHora dHora: ' + dHora);
	    	    	
	    	String sDtFecha = ConvierteFecha(dtFecha);    	
	    	//Crea la fecha de paso 
	    	Date dFechaPaso = Date.valueOf(sDtFecha); 
	    	DateTime dtFechaPaso = DateTime.newInstance(dFechaPaso, dHora);
	
			sFechaPaso = String.valueOf(dtFechaPaso);
	    	System.debug('Entro a ConvierteFechaFechHora sFechaPaso: ' + sFechaPaso);
	    	        	
        }catch(Exception ex){
            System.debug('Error en ConvierteFechaFechHora detalle: ' + ex.getMessage() + ' Linea: ' + ex.getLineNumber());
        }
            	    	    	
    	return sFechaPaso;
    }

	@TestVisible
    private String ConvierteFecha(String sFecha){
    	System.debug('Entro a ConvierteFecha sFecha: ' + sFecha);
    	String sFechaPaso;
    	
    	String sDia;
    	String sMes;
    	String sAnio;
    	
    	//Ve si tiene algo la fecha sFecha (/)
    	if (sFecha != null && sFecha != '' && sFecha.contains('/')){
			sMes = sFecha.substring(0, sFecha.indexOf('/'));
    		System.debug('Entro a ConvierteFecha sMes: ' + sMes);			
			sMes = Integer.valueOf(sMes) < 10 ? '0' + Integer.valueOf(sMes) : sMes;			 
			sDia = sFecha.substring(sFecha.indexOf('/') + 1, sFecha.lastIndexOf('/'));
    		System.debug('Entro a ConvierteFecha sDia: ' + sDia);
    		//Ve si el mes viene en letra y no en numero
    		if (sDia.length() > 2){
    			sDia = mapAnios.containsKey(sDia) ? mapAnios.get(sDia) : sDia;
    			System.debug('Entro a ConvierteFecha sDia2: ' + sDia);
    		}//Fin si sDia.length() > 2
			sDia = Integer.valueOf(sDia) < 10 ? '0' + Integer.valueOf(sDia)  : sDia;
    		if (sFecha.length() >= 10){
				sAnio = sFecha.substring(sFecha.lastIndexOf('/') + 1, 10);
				sAnio = sAnio.substring(0,4);
    			System.debug('Entro a ConvierteFecha sAnio0: ' + sAnio);				
    		}//Fin si sFecha.length() >= 10
    		if (sFecha.length() > 8 && sFecha.length() < 10){
				sAnio = sFecha.substring(sFecha.lastIndexOf('/') + 1, 9);
				sAnio = sAnio.substring(0,sAnio.length());
				if (sAnio.length() < 3)
					sAnio = '20' + sAnio;				
    			System.debug('Entro a ConvierteFecha sAnio1: ' + sAnio);
    		}//Fin si sFecha.length() > 8 && sFecha.length() < 10
    		if (sFecha.length() < 9){
				sAnio = sFecha.substring(sFecha.lastIndexOf('/') + 1, 8);
				sAnio = sAnio.substring(0,sAnio.length());
				if (sAnio.length() < 3)
					sAnio = '20' + sAnio;
    			System.debug('Entro a ConvierteFecha sAnio2: ' + sAnio);
    		}//Fin si sFecha.length() < 9
    		System.debug('En ConvierteFecha sDia: ' + sDia + ' sMes: ' + sMes + ' sAnio: ' + sAnio);    		
			//Ya tienes los datos de la fecha entonces creala con el formato adecuado
			//Ve si el dato del mesa es mayor a 12 quiere decir que esta cambiado
			if (Integer.valueOf(sMes) < 13)
				sFechaPaso = sAnio.trim() + '-' + sMes.trim() + '-' + sDia.trim();
			if (Integer.valueOf(sMes) > 12)
				sFechaPaso = sAnio.trim() + '-' + sDia.trim() + '-' + sMes.trim();			
    	}//Fin si sFecha != null && sFecha != '' && sFecha.contains('/')
		//La fecha tien guiones
    	if (sFecha != null && sFecha != '' && sFecha.contains('-')){
    		Boolean bContieneMes = false;
    		//Recorre la lista de llaves del mapa de mapAnios
    		for (String sMesPaso : mapAnios.keySet()){
    			//Ve si la fecha sFecha contiene a sMes entonces reemplaza
    			if (sFecha.contains(sMesPaso)){
					sFecha = sFecha.replace(sMesPaso, mapAnios.get(sMesPaso));
					String ssAnio = sFecha.substring(sFecha.lastIndexOf('-') + 1, sFecha.length());
    				System.debug('Entro a ConvierteFecha ssAnio3: ' + ssAnio);					
					ssAnio = ssAnio.substring(0, ssAnio.indexOf(' '));
					if (ssAnio.length() < 3)
						ssAnio = '20' + ssAnio;
   					System.debug('Entro a ConvierteFecha guiones ssAnio: ' + ssAnio);					
					sFecha = sFecha.substring(0,sFecha.lastIndexOf('-') + 1);
					String sDiaPasoFinal = sFecha.substring(0,sFecha.indexOf('-'));
   					System.debug('Entro a ConvierteFecha guiones sDiaPasoFinal: ' + sDiaPasoFinal);					
					String sMesPasoFinal = sFecha.substring(sFecha.indexOf('-') + 1, sFecha.lastIndexOf('-'));
   					System.debug('Entro a ConvierteFecha guiones sMesPasoFinal: ' + sMesPasoFinal);
					sFechaPaso = ssAnio + '-' + sMesPasoFinal + '-' + sDiaPasoFinal;
   					System.debug('Entro a ConvierteFecha guiones sFechaPaso0: ' + sFechaPaso);					
					bContieneMes = true;
					Break;
    			}//Fin si sFecha.contains(sMesPaso)
    		}
   			if (!bContieneMes)    		
    			sFechaPaso = ConvierteFechaAAAAMMDD(sFecha);
    	}//Fin si sFecha != null && sFecha != '' && sFecha.contains('/')
    	System.debug('Entro a ConvierteFecha sFechaPaso1: ' + sFechaPaso); 
    	    	
    	return sFechaPaso;
    }

	@TestVisible
    private String ConvierteFechaAAAAMMDD(String sFecha){
    	System.debug('Entro a ConvierteFechaAAAAMMDD sFecha: ' + sFecha);
    	String sFechaPaso;
    	
    	//Ve si tiene algo la fecha sFecha
    	if (sFecha != null && sFecha != '')
    		sFechaPaso = sFecha.substring(0, 10);
    	System.debug('Entro a ConvierteFechaAAAAMMDD sFechaPaso: ' + sFechaPaso);
    	    	
    	return sFechaPaso;
    }

    
}