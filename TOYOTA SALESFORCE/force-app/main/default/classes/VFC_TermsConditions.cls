public class VFC_TermsConditions {
    Public User usuario{get;set;}
    Public ParametrosConfiguracionToyota__c redireccionaextranet{get;set;}
    Public ParametrosConfiguracionToyota__c redireccionatermsconditions{get;set;}
    Public Boolean verboton{get;set;}
    
    Public PageReference HomePage(){
        PageReference OpenPageURL = New PageReference(redireccionaextranet.Valor__c);
        OpenPageURL.setRedirect(true);
        usuario.AceptaTerminos__c=true;
        update usuario;
        return OpenPageURL;
    }
    Public VFC_TermsConditions(){
       redireccionaextranet=[SELECT Consecutivo__c, valor__c FROM ParametrosConfiguracionToyota__c where Consecutivo__c = 752];
       redireccionatermsconditions=[SELECT Consecutivo__c, valor__c FROM ParametrosConfiguracionToyota__c where Consecutivo__c = 753];
       usuario=([SELECT AceptaTerminos__c FROM User WHERE id =: UserInfo.getUserId()]);
       verboton=false; 
    }
    Public PageReference RedireccionaExtranet(){
        PageReference OpenPageURL;
        IF(usuario.AceptaTerminos__c == true){
           OpenPageURL = New PageReference(redireccionaextranet.Valor__c);            
        }
        ELSE{
           OpenPageURL = New PageReference(redireccionatermsconditions.Valor__c);   
        }
        return OpenPageURL;
    }
}