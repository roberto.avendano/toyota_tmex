/**
    Descripción General: Clase wrapper TriplePlay para Grid
    ________________________________________________________________
        Autor               Fecha               Descripción
    ________________________________________________________________
        Cecilia Cruz        05/Nov/2019         Versión Inicial
    ________________________________________________________________
**/
public class TAM_GridTripePayWrapperClass {

    @AuraEnabled 
    public Boolean boolTriplePlay {get;set;}
    @AuraEnabled 
    public String strProductoFinanciero {get;set;}
    @AuraEnabled 
    public String strDescripcion {get;set;}
    @AuraEnabled 
    public Double intTotalEstimado {get;set;}
    @AuraEnabled 
    public Double intIncentivoTFS_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoTFS_Porcentaje {get;set;}
    @AuraEnabled 
    public Double intIncentivoTMEX_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoTMEX_Porcentaje {get;set;}
    @AuraEnabled 
    public Double intIncentivoDealer_Pesos {get;set;}
    @AuraEnabled 
    public Double intIncentivoDealer_Porcentaje {get;set;}

	public TAM_GridTripePayWrapperClass() {
        this.boolTriplePlay = false;
        this.strProductoFinanciero = '';
        this.strDescripcion = '';
        this.intTotalEstimado = 0;
        this.intIncentivoTFS_Pesos = 0;
        this.intIncentivoTFS_Porcentaje = 0;
        this.intIncentivoTMEX_Pesos = 0;
        this.intIncentivoTMEX_Porcentaje = 0;
        this.intIncentivoDealer_Pesos = 0;
        this.intIncentivoDealer_Porcentaje = 0;
    }

    //Constructor
    public TAM_GridTripePayWrapperClass( Boolean boolTriplePlay, String strProductoFinanciero, String strDescripcion, Double intTotalEstimado,
                                         Double intIncentivoTFS_Pesos, Double intIncentivoTFS_Porcentaje, Double intIncentivoTMEX_Pesos,
                                         Double intIncentivoTMEX_Porcentaje, Double intIncentivoDealer_Pesos, Double intIncentivoDealer_Porcentaje) {
        this.boolTriplePlay = boolTriplePlay;
        this.strProductoFinanciero = strProductoFinanciero;
        this.strDescripcion = strDescripcion;
        this.intTotalEstimado = intTotalEstimado;
        this.intIncentivoTFS_Pesos = intIncentivoTFS_Pesos;
        this.intIncentivoTFS_Porcentaje = intIncentivoTFS_Porcentaje;
        this.intIncentivoTMEX_Pesos = intIncentivoTMEX_Pesos;
        this.intIncentivoTMEX_Porcentaje = intIncentivoTMEX_Porcentaje;
        this.intIncentivoDealer_Pesos = intIncentivoDealer_Pesos;
        this.intIncentivoDealer_Porcentaje = intIncentivoDealer_Porcentaje;
    }

}