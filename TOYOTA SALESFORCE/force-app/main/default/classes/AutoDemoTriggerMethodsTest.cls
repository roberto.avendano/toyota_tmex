@isTest
private class AutoDemoTriggerMethodsTest {

	@isTest static void test_one() {
		Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;

		Account cuenta = new Account(
			Name = 'TestAccount',
			Codigo_Distribuidor__c = '57055',			
			UnidadesAutosDemoAutorizadas__c = 5
		);
		insert cuenta;

		Vehiculo__c vin = new Vehiculo__c(
    		Name = 'JTDKBRFUXH3031005',
        	Id_Externo__c = 'JTDKBRFUXH3031005',
            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id                    
    	);
    	insert vin;

		Map<String, Account> cuentaMapCode = AutoDemoTriggerMethods.getDealerCodeInfoMap(new Set<String>{cuenta.Codigo_Distribuidor__c});
		Map<Id, Account> cuentaMapId = AutoDemoTriggerMethods.getDealerInfoMap(new Set<Id>{cuenta.Id});
		Map<String, Vehiculo__c> vinesMapCode = AutoDemoTriggerMethods.getVinesInfoByName(new Set<String>{'JTDKBRFUXH3031005'});
		Map<Id, Vehiculo__c> vinesMapId = AutoDemoTriggerMethods.getVinesInfoById(new Set<Id>{vin.Id});
	}
}