/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_SolicitudesParaAprobarTriggerHandler.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    29-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest
public with sharing class TAM_SolicitParaAprobarTriggerHandler_tst {

	static String VaRtAccRegCteCorp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente Corporativo').getRecordTypeId();
	static String VaRtAccRegPM = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String VaRtAccRegPF = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String VaRtAccRegPMNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
	static String VaRtAccRegPFNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
	static String VaRtAccRegPMPreAut = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
	static String VaRtAccRegPFPreAut = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();
	
	static String VaRtLeadEstAuto = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Estatus Autorización').getRecordTypeId();
	static String VaRtLeadCteCreado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cliente Creado').getRecordTypeId();
    static String VaRtLeadRtRetNvo = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String VaRtLeadRtRetSemNvo = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Programa').getRecordTypeId();

	static String VaRtLeadSolFlot = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
	static String VaRtLeadSolProg = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String VaRtLeadSolFlotSoloLec = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Flotilla solo lectura').getRecordTypeId();
	static String VaRtLeadSolProgSoloLec = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Programa solo lectura').getRecordTypeId();

	static String VaRtCLienteSolPreautoriza = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Cliente Preautorizado').getRecordTypeId();
	static String VaRtCLienteSolActivacion = Schema.SObjectType.TAM_SolicitudesParaAprobar__c.getRecordTypeInfosByName().get('Activación de Cliente').getRecordTypeId();

	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static	Account clientePruebaMoral = new Account();
	static	Account clientePruebaFisica = new Account();
	static	TAM_SolicitudesParaAprobar__c solicitudesParaAprobar = new TAM_SolicitudesParaAprobar__c();
	static	TAM_SolicitudesParaAprobar__c solicitudesParaAprobarFisico = new TAM_SolicitudesParaAprobar__c();
	static	Rangos__c rango = new Rangos__c();
	static	Lead leadPrograma = new Lead();
	
	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;

		TAM_GrupoCorporativo__c objGrupoCorp = new TAM_GrupoCorporativo__c(
			Name = 'TOYOTA PRUEBA',
			TAM_FechaRegistro__c = Date.today(),			
			TAM_Estatus__c = true
		);
		insert objGrupoCorp;

		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral
		);
		insert clienteMoral;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  

	    Lead leadPrueba = new Lead(
	        RecordTypeId = VaRtLeadRtRetNvo,
	        FirstName = 'Test',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'as@a.com',
	        phone = '225',
	        Status='Nuevo Lead',
	        LastName = 'Test1',
	        LeadSource = 'Tráfico Piso'
		);
		insert leadPrueba;
				
		TAM_SolicitudesParaAprobar__c SolicitudesParaAprobar = new TAM_SolicitudesParaAprobar__c(
			Name = 'CARSON S.A.',
			TAM_Candidato__c = leadPrueba.id,
			TAM_ClienteCorporativoRef__c = clienteMoral.id,
			RecordTypeId = VaRtLeadSolFlot,
			TAM_Estatus__c = 'Proceso',
			TAM_RazonSocial__c = 'CARSON DEL SURESTE S.A. DE C.V.',
			TAM_NombreComercial__c = 'CARSON DEL SURESTE S.A. DE C.V.',
			TAM_GrupoCorporativoRef__c = objGrupoCorp.id
		);
		insert SolicitudesParaAprobar;

	    Lead leadPrueba2 = new Lead(
	        RecordTypeId = VaRtLeadRtRetSemNvo,
	        FirstName = 'Test2',
	        FWY_Intencion_de_compra__c = 'Este mes',	
	        Email = 'as@a2.com',
	        phone = '225',
	        Status='Nuevo Lead2',
	        LastName = 'Test12',
	        LeadSource = 'Tráfico Piso'
		);
		insert leadPrueba2;
				
		TAM_SolicitudesParaAprobar__c SolicitudesParaAprobar2 = new TAM_SolicitudesParaAprobar__c(
			Name = 'CARSON DEL SURESTE',
			TAM_Candidato__c = leadPrueba2.id,
			TAM_ClienteCorporativoRef__c = clienteFisico.id,
			RecordTypeId = VaRtLeadSolProg,
			TAM_Estatus__c = 'Proceso',
			TAM_RazonSocial__c = 'CLIENTE CARSON DEL SURESTE S.A. DE C.V. II',
			TAM_NombreComercial__c = 'CLIENTE CARSON DEL SURESTE S.A. DE C.V. II',
			TAM_GrupoCorporativoRef__c = objGrupoCorp.id
		);
		insert SolicitudesParaAprobar2;

	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_SolicitParaAprobarTriggerHandlerOK() {
		
		Test.startTest();
			//Consulta los datos del cliente
			clientePruebaMoral = [Select Id, Name From Account where RecordTypeId = :sRectorTypePasoPersonaMoral LIMIT 1];
	   		System.debug('EN TAM_SolicitParaAprobarTriggerHandlerOK solicitudesParaAprobar: ' + solicitudesParaAprobar);
	    	
			//Consulta los datos del cliente
			solicitudesParaAprobar = [Select Id, 
				Name ,
				TAM_Candidato__c,
				TAM_ClienteCorporativoRef__c,
				RecordTypeId,
				TAM_Estatus__c,
				TAM_RazonSocial__c,
				TAM_NombreComercial__c,
				TAM_NombreNuevoClienteCorporativo__c,
				TAM_FechaVigenciaInicio__c,
				TAM_FechaVigenciaFin__c,
				TAM_CorreoElectronico__c,
				TAM_TelefonoCuenta__c, 
				TAM_RFC__c, 
				TAM_NombreRL__c,
				TAM_ApellidoPaternoRL__c,
				TAM_ApellidoMaternoRL__c,
				TAM_RFCRL__c,
				TAM_CorreoElectronicoRL__c,
				TAM_DireccionRL__c,
				TAM_TelefonoRL__c,
				TAM_TipoPersona__c,
				TAM_Comentarios__c,
				TAM_GrupoCorporativoRef__c,
				TAM_ProgramaRandoRef__c,
				TAM_CalleRef__c,
				TAM_CodigoPostalRef__c, 
				TAM_ColoniaRef__c,
				TAM_CiudadRef__c,
				TAM_EstadoRef__c,
				TAM_Pais__c,
				TAM_CorreoCorporativoRef__c,
				TAM_TelefonoCorporativoRef__c,
				TAM_RFCRef__c,
				TAM_NombreRefRL__c,
				TAM_ApellidoPaternoRefRL__c,
				TAM_ApellidoMaternoRefRL__c,
				TAM_RFCRefRL__c,
				TAM_CorreoElectronicoRefRL__c,
				TAM_TelefonoRefRL__c,
				OwnerId,
				TAM_SitioWeb__c
			From TAM_SolicitudesParaAprobar__c LIMIT 1];
	   		System.debug('EN TAM_SolicitParaAprobarTriggerHandlerOK clientePruebaMoral: ' + clientePruebaMoral);
			
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdRechaza = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Rechazado'
			);
			update solicitudesParaAprobarUpdRechaza;
	
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdAutoriza = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Autorizado'
			);
			update solicitudesParaAprobarUpdAutoriza;
			
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdPreAutoriza = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Preautorizado',
				RecordTypeId = VaRtCLienteSolPreautoriza
			);
			update solicitudesParaAprobarUpdPreAutoriza;
	
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			Account clientePersonaMoralUpdRT = new Account(id = clientePruebaMoral.id,
				RecordTypeId = VaRtAccRegPMPreAut
			);
			update clientePersonaMoralUpdRT;
	
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdActiva = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Activación',
				RecordTypeId = VaRtCLienteSolActivacion
				
			);
			update solicitudesParaAprobarUpdActiva;
	
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdActiva2 = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Autorizado'			
			);
			update solicitudesParaAprobarUpdActiva2;
			
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdActiva3 = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Activación',
				RecordTypeId = VaRtCLienteSolActivacion,
				TAM_FechaVigenciaInicio__c = Date.today()
			);
			update solicitudesParaAprobarUpdActiva3;
			
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdActiva4 = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Autorizado'			
			);
			update solicitudesParaAprobarUpdActiva4;
			
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdActiva5 = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobar.id,
				TAM_Estatus__c = 'Activación',
				RecordTypeId = VaRtCLienteSolActivacion,
				TAM_FechaVigenciaInicio__c = Date.today(),
				TAM_FechaVigenciaFin__c = Date.today()
			);
			update solicitudesParaAprobarUpdActiva5;
	
			//Manda llamar  el metodo de TAM_SolicitudesParaAprobarTriggerHandler.creaClienteCorporativo();
			TAM_SolicitudesParaAprobarTriggerHandler.creaClienteCorporativo(solicitudesParaAprobar, new Map<id,TAM_SolicitudesParaAprobar__c>{solicitudesParaAprobar.id => solicitudesParaAprobar});	
		Test.stopTest();				         
   }

    static testMethod void TAM_SolicitParaAprobarTriggerHandlerOKPrograma() {
		Test.startTest();
			//Consulta los datos del cliente
			solicitudesParaAprobarFisico = [Select Id, 
				Name ,
				TAM_Candidato__c,
				TAM_ClienteCorporativoRef__c,
				RecordTypeId,
				TAM_Estatus__c,
				TAM_RazonSocial__c,
				TAM_NombreComercial__c,
				TAM_NombreNuevoClienteCorporativo__c,
				TAM_FechaVigenciaInicio__c,
				TAM_FechaVigenciaFin__c,
				TAM_CorreoElectronico__c,
				TAM_TelefonoCuenta__c, 
				TAM_RFC__c, 
				TAM_NombreRL__c,
				TAM_ApellidoPaternoRL__c,
				TAM_ApellidoMaternoRL__c,
				TAM_RFCRL__c,
				TAM_CorreoElectronicoRL__c,
				TAM_DireccionRL__c,
				TAM_TelefonoRL__c,
				TAM_TipoPersona__c,
				TAM_Comentarios__c,
				TAM_GrupoCorporativoRef__c,
				TAM_ProgramaRandoRef__c,
				TAM_CalleRef__c,
				TAM_CodigoPostalRef__c, 
				TAM_ColoniaRef__c,
				TAM_CiudadRef__c,
				TAM_EstadoRef__c,
				TAM_Pais__c,
				TAM_CorreoCorporativoRef__c,
				TAM_TelefonoCorporativoRef__c,
				TAM_RFCRef__c,
				TAM_NombreRefRL__c,
				TAM_ApellidoPaternoRefRL__c,
				TAM_ApellidoMaternoRefRL__c,
				TAM_RFCRefRL__c,
				TAM_CorreoElectronicoRefRL__c,
				TAM_TelefonoRefRL__c,
				OwnerId,
				TAM_SitioWeb__c
			From TAM_SolicitudesParaAprobar__c Where RecordTypeId = :VaRtLeadSolProg LIMIT 1];
	   		System.debug('EN TAM_SolicitParaAprobarTriggerHandlerOKPrograma solicitudesParaAprobarFisico: ' + solicitudesParaAprobarFisico);
	
			//Toma el reg de la solicitud y axctualizalo para que pase por RECHAZADO
			TAM_SolicitudesParaAprobar__c solicitudesParaAprobarUpdAutorizaProrama = new TAM_SolicitudesParaAprobar__c(id = solicitudesParaAprobarFisico.id,
				TAM_Estatus__c = 'Autorizado'
			);
			update solicitudesParaAprobarUpdAutorizaProrama;
		
		    //Manda llamar  el metodo de TAM_SolicitudesParaAprobarTriggerHandler.creaClientePrograma();
		    TAM_SolicitudesParaAprobarTriggerHandler.creaClientePrograma(solicitudesParaAprobarFisico, new Map<id,TAM_SolicitudesParaAprobar__c>{solicitudesParaAprobarFisico.id => solicitudesParaAprobarFisico});		
		
            //System.assertEquals(1,lSolFlotProgExcepVinesCmWrp.size(),'1 reistro debe ser regresado');
            System.assertEquals(solicitudesParaAprobarUpdAutorizaProrama.TAM_Estatus__c, 'Autorizado');    
		
		Test.stopTest();
   }
    
}