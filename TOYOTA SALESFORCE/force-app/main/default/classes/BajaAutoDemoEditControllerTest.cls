@isTest
private class BajaAutoDemoEditControllerTest {
	
	@TestSetup static void loadData() {
		Schema.DescribeSObjectResult vinSchema = Schema.SObjectType.Vehiculo__c;
		Schema.RecordTypeInfo rtVin = vinSchema.getRecordTypeInfosByName().get('Vehículo');

		List<Vehiculo__c> newVehicles = new List<Vehiculo__c>();
		List<Account> newAccounts = new List<Account>();
		List<SolicitudAutoDemo__c> newSolicitudes = new List<SolicitudAutoDemo__c>();

		for(Integer i=0; i<3;i++){
			newAccounts.add(new Account(
				Name='Account'+i,
				UnidadesAutosDemoAutorizadas__c = 15
			));			
		}		
		insert newAccounts;

		for(Integer j=0; j<5; j++){			
		 	Integer rand = BajaAutoDemoEditControllerTest.randomWithMax(newAccounts.size());
		 	newVehicles.add(new Vehiculo__c(
	            Name='VIN'+j,          
	            Distribuidor__c=newAccounts.get(rand).Id,	            
	            RecordTypeId= rtVin.recordTypeId
	        ));	        
		}
		insert newVehicles;

	}

	public static Integer randomWithMax(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
	@isTest static void test_one() {
		Vehiculo__c vin = [SELECT Id, Name FROM Vehiculo__c LIMIT 1];
		SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c(
			VIN__c = vin.Id,
			Estatus__c = Constantes.CASE_STATUS_ALTA
		);

		insert sad;
		ApexPages.StandardController stdController = new ApexPages.StandardController(sad);
		BajaAutoDemoEditController extController = new BajaAutoDemoEditController(stdController);
		extController.picklistValue = 'Cliente persona moral';
		extController.mostrarCampo();
		extController.guardar();


		stdController = new ApexPages.StandardController(new SolicitudAutoDemo__c());
		extController = new BajaAutoDemoEditController(stdController);
		extController.guardar();
	}
	
}