/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para asociar los vines a una solicitud de tipo Inventario 
    					para programa en Venta Especial por Inventario.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    07-Enero-2020    	Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitudCompraEspecialVinesCmpCtrl {

	static String sRectorTypePasoPrograma = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoGFlotilla = Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();

	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();
	
    //Un constructor por default
    public TAM_SolicitudCompraEspecialVinesCmpCtrl(){}
    
   //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_OrdenDeCompraWrapperClass getWrpModelo(String sWrpModelo) {
    	System.debug('EN getWrpModelo: ' + sWrpModelo);
    	String sCadenaPaso = '';
    	
    	//Serializa el JSON sobre la clase de TAM_OrdenDeCompraWrapperClass
		TAM_OrdenDeCompraWrapperClass objTAM_OrdenDeCompraWrapperClassPaso = new TAM_OrdenDeCompraWrapperClass();
		objTAM_OrdenDeCompraWrapperClassPaso = (TAM_OrdenDeCompraWrapperClass) JSON.deserialize(sWrpModelo, TAM_OrdenDeCompraWrapperClass.class);
		System.debug('EN getWrpModelo objTAM_OrdenDeCompraWrapperClassPaso: ' + objTAM_OrdenDeCompraWrapperClassPaso);
       
        //Regresa la cadena
        return objTAM_OrdenDeCompraWrapperClassPaso;
    }
    

   //Obtener un Set de las series disponibles
    @AuraEnabled
    public static string addWrpDist(String sWrpDist, String recordId, Integer intIndex, String strAnioModelo,
    	String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strIdCantidadInv, String strIdCantidadSol, String strSolRecortTypeId) {    	
    	System.debug('EN TAM_SolicitudCompraEspecialVinesCmpCtrl getWrpDist: ' + sWrpDist + ' recordId: ' + recordId + ' intIndex: ' + intIndex + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt + ' strIdCantidadSol: ' + strIdCantidadSol);
		System.debug('EN TAM_SolicitudCompraEspecialVinesCmpCtrl strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' strIdColInt: ' + strIdColInt + ' strIdCantidadSol: ' + strIdCantidadSol);    	
    	String sCadenaPaso = '';  
    	TAM_WrpDistSolFlotillaPrograma objPasoWrpDist = new TAM_WrpDistSolFlotillaPrograma();
    	
    	//Ve si ya es mas de un Vin
		if (intIndex > 0)
			intIndex++;
		if (intIndex < Integer.valueOf(strIdCantidadSol)){			
			objPasoWrpDist = new TAM_WrpDistSolFlotillaPrograma(intIndex, false, '' , strAnioModelo, strSerie
				, strModelo, strVersion, '', 0, ' NINGUNO ', new List<TAM_WrpDistribuidores>());
			objPasoWrpDist.strIdColExt = strIdColExt;
			objPasoWrpDist.strIdColInt = strIdColInt;	
			objPasoWrpDist.sVIN = '';
			objPasoWrpDist.bolDisable = false;
		}//Fin si intIndex < Integer.valueOf(strIdCantidadSol)
        //serializa la lista y crea el JSON
		sCadenaPaso = JSON.serialize(objPasoWrpDist);              
        System.debug('EN addWrpDist sCadenaPaso: ' + sCadenaPaso);
        
        //Regresa la cadena
        return sCadenaPaso;
    }

    //Obtener distribuidores.
    @AuraEnabled
    public static List<Account> getDistribuidores() {
    	System.debug('EN getDistribuidores...');
        List<Account> lstDistribuidores = [ SELECT Id, Name, Codigo_Distribuidor__c 
                                            FROM Account 
                                            WHERE RecordType.DeveloperName = 'Dealer'
                                            ORDER BY Name ASC ];
		System.debug('EN getDistribuidores lstDistribuidores: ' + lstDistribuidores);
        return lstDistribuidores;
    }   

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion saveDatos(String sWrpDist, String sWrpDistElim, String recordId, Integer intIndex, 
    	String strAnioModelo, String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strSolRecortTypeId) {    	
    	System.debug('EN saveDatos sWrpDist: ' + sWrpDist );
    	System.debug('EN saveDatos sWrpDistElim: ' + sWrpDistElim );
    	System.debug('EN saveDatos recordId: ' + recordId + ' intIndex: ' + intIndex + ' strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt + ' sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma);
    	String sMensaejeSalidaUpd = '';  
		
		String sRectorTypePaso = getTipoRegSol(strSolRecortTypeId, true);
		System.debug('EN saveDatos sRectorTypePaso: ' + sRectorTypePaso );		

		String sRectorTypePasoIdExt = getTipoRegSol(strSolRecortTypeId, false);
		System.debug('EN saveDatos sRectorTypePasoIdExt: ' + sRectorTypePasoIdExt );		
				
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDist = new List<TAM_WrpDistSolFlotillaPrograma>();		
		List<TAM_VinesFlotillaPrograma__c>	ListDistProgUps = new List<TAM_VinesFlotillaPrograma__c>();
		Set<String> setCaveDistr = new Set<String>();
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDistElim = new List<TAM_WrpDistSolFlotillaPrograma>();
		Set<String> setCaveDistrElim = new Set<String>();
		List<TAM_VinesFlotillaPrograma__c>	ListDistProgDel = new List<TAM_VinesFlotillaPrograma__c>();
		Set<String> setIdExtDel = new Set<String>();
		List<TAM_VinesFlotillaPrograma__c> lTAMSolicitudCompraPROGRAMADel = new List<TAM_VinesFlotillaPrograma__c>();
			
		//Ve si tiene algo la lista 
		if (intIndex > 0 || sWrpDist.contains('sVIN')){
			lObjWrpDist = JSONParserSFDC(sWrpDist);
						
			//Recorre la lista de y obten el id del distribuidor
			for (TAM_WrpDistSolFlotillaPrograma objWrpDist : lObjWrpDist){
				//Solo los que son mayor a 0
				if (objWrpDist.sVIN != null && objWrpDist.sVIN != '' && objWrpDist.bolSeleccionado){
					//String sCaveDist = objWrpDist.strIdDistrib.substring(0, objWrpDist.strIdDistrib.indexOf(' - '));
					String sRecordName = objWrpDist.sVIN;
					//String sIdExterno = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strVersion + '-' + strIdColExt + '-' + strIdColInt + '-' + objWrpDist.sVIN + '-' +sRectorTypePasoPrograma;
					String sIdExterno = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt + '-' + objWrpDist.sVIN + '-' + sRectorTypePasoIdExt; //sRectorTypePasoPrograma;
					if (objWrpDist.strIdExterno != null && objWrpDist.strIdExterno != '')
						sIdExterno = objWrpDist.strIdExterno;
					System.debug('EN saveDatos sIdExterno: ' + sIdExterno);	
					//Crea el registro y agregalo a la lista de ListDistProgUps
					ListDistProgUps.add(new TAM_VinesFlotillaPrograma__c(
							Name = sRecordName,
							//TAM_SolicitudCompraPROGRAMA__c = recordId,
							TAM_SolicitudFlotillaPrograma__c = recordId,
							RecordTypeid = sRectorTypePaso, //sRectorTypePasoPrograma,
							//TAM_Cliente__c = mapClaveDistIdSfdc.containsKey(sCaveDist) ? mapClaveDistIdSfdc.get(sCaveDist).id : null,
							TAM_Estatus__c = 'Pendiente',
							TAM_IdExterno__c = sIdExterno,
							TAM_EstatusInventario__c = objWrpDist.strEstatusVIN
						)
					);
				}//Fin si objWrpDist.strIdDistrib != null && objWrpDist.strIdDistrib != ''
				if (objWrpDist.sVIN != null && objWrpDist.sVIN != '' && !objWrpDist.bolSeleccionado && objWrpDist.strIdExterno != null && objWrpDist.strIdExterno != '')
					setIdExtDel.add(objWrpDist.strIdExterno);
			}//Fin del for para lObjWrpDist
			System.debug('EN saveDatos setIdExtDel: ' + setIdExtDel);
			
			//Consulta los datos de los reg asociado a setIdExtDel
			for (TAM_VinesFlotillaPrograma__c objVin : [Select id From TAM_VinesFlotillaPrograma__c 
				Where TAM_IdExterno__c IN :setIdExtDel]){
				ListDistProgDel.add(objVin);
			}
		}//Fin si intIndex > 0
    	System.debug('EN saveDatos ListDistProgUps: ' + ListDistProgUps);
    	System.debug('EN saveDatos ListDistProgDel: ' + ListDistProgDel);

        Savepoint sp = Database.setSavepoint();
     	Boolean bErrror = false;
     	
    	//Ve si tiene algo la lista de ListDistProgDel
    	if (!ListDistProgDel.isEmpty()){
	        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.DeleteResult> lDtbUpsRes = Database.delete(ListDistProgDel, false);
			//Ve si hubo error
			for (Database.DeleteResult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess()){
					objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Hubo un error a la hora de crear/Actualizar los registros de los Vines ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
					bErrror = true;
				}
			}//Fin del for para lDtbUpsRes
    	} //Fin si ListDistProgDel.isEmpty()
		System.debug('EN getWrpModelo objRespoPaso.strDetalle: ' + objRespoPaso.strDetalle);
    	
    	//Ve si tiene algo la lista de ListDistProgUps
    	if (!ListDistProgUps.isEmpty()){
	        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Upsertresult> lDtbUpsRes = Database.upsert(ListDistProgUps, TAM_VinesFlotillaPrograma__c.TAM_IdExterno__c, false);
			//Ve si hubo error
			for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess()){
					objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Hubo un error a la hora de crear/Actualizar los registros de los Vines ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
					bErrror = true;
				}
				if (objDtbUpsRes.isSuccess())
					objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos de los vines se crear/Actualizar con exito');
			}//Fin del for para lDtbUpsRes
    	} //Fin si ListDistProgUps.isEmpty()
		System.debug('EN getWrpModelo objRespoPaso.strDetalle: ' + objRespoPaso.strDetalle);

        //Hecha para atras todo los cambios
        if (bErrror)
        	Database.rollback(sp);
		  	        
        //serializa la lista y crea el JSON
		//sCadenaPaso = JSON.serialize(objPasoWrpDist);              
        System.debug('EN getWrpModelo objRespoPaso: ' + objRespoPaso);
                 
        //Regresa la cadena
        return objRespoPaso;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static string getDatosDistribuidores(String recordId, Integer intIndex, String strAnioModelo,
    	String strSerie, String strModelo, String strVersion, String strIdColExt, String strIdColInt, 
    	String strSolRecortTypeId) {    	
    	System.debug('EN getDatosDistribuidores...');
    	System.debug('EN getDatosDistribuidores recordId: ' + recordId + ' intIndex: ' + intIndex + ' strAnioModelo: ' + strAnioModelo + ' strSerie: ' + strSerie + ' strModelo: ' + strModelo + ' strVersion: ' + strVersion + ' strIdColExt: ' + strIdColExt + ' strIdColInt: ' + strIdColInt + ' sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma);
    	String sCadenaPaso = '';  
    	String sNombreCliente = '';
		Map<String, TAM_WrpDistSolFlotillaPrograma> mapIdExtObjWrp = new Map<String, TAM_WrpDistSolFlotillaPrograma>();
		
		String sRectorTypePaso = getTipoRegSol(strSolRecortTypeId, false);
		System.debug('EN getDatosDistribuidores sRectorTypePaso: ' + sRectorTypePaso );		
				 
		for (TAM_SolicitudesFlotillaPrograma__c cliente : [Select id, TAM_Cliente__r.Name From TAM_SolicitudesFlotillaPrograma__c 
			Where id =:recordId ]){
			sNombreCliente = cliente.TAM_Cliente__r.Name;
		}
		 
		//Consulta el inventario asociado al 
		String sIdModeloAnio = strModelo + '' + strAnioModelo; 
		System.debug('EN getDatosDistribuidores sIdModeloAnio: ' + sIdModeloAnio);
		
		//Para los datos del inventario 57000
        Id userId = UserInfo.getUserId();
		System.debug('EN getDatosDistribuidores userId: ' + userId);        
        user usuario = [SELECT id,CodigoDistribuidor__c FROM user WHERE id =: userId];
        String strCodigoDistribuidor = usuario.CodigoDistribuidor__c == null ? '57000' : usuario.CodigoDistribuidor__c;
        
        //Es una prueba
        if (Test.isRunningTest())
        	strCodigoDistribuidor = '570550';
        	
        Map<String, Set<String>> mapIdExtTotalSolCerradasVines = new Map<String, Set<String>>();
		//Conslya las solicitudes que ya estan cerradas donde el 
		for (TAM_VinesFlotillaPrograma__c objVinesFlotProg : [Select t.Name, t.TAM_IdExterno__c, t.TAM_Estatus__c 
				From TAM_VinesFlotillaPrograma__c t	Where
				/*(t.TAM_SolicitudCompraFLOTILLA__r.TAM_Estatus__c = 'Cerrada' OR
				t.TAM_SolicitudCompraPROGRAMA__r.TAM_Estatus__c = 'Cerrada') And 
				t.TAM_SolicitudCompraPROGRAMA__c != :recordId*/
                t.TAM_SolicitudFlotillaPrograma__r.TAM_Estatus__c = 'Cerrada' And
                t.TAM_Estatus__c != 'Cancelada' and t.TAM_SolicitudFlotillaPrograma__c != :recordId 
				Order by TAM_EstatusInventario__c, Name]){
			//Toma el campo de TAM_IdExterno__c y destripalo
			String[] sTAMIdExterno = objVinesFlotProg.TAM_IdExterno__c.split('-');
			//Toma desde el numero 1 hasta el 5
			String ssTAMIdExternoPaso = sTAMIdExterno[1] + '-' + sTAMIdExterno[2] + '-' + sTAMIdExterno[3] + '-' + sTAMIdExterno[4] + '-' + sTAMIdExterno[5];
			
			System.debug('EN getDatosDistribuidores ssTAMIdExternoPaso: ' + ssTAMIdExternoPaso);
			//Ve si existe el ssTAMIdExternoPaso en el mapa de mapIdExtTotalSolCerradasVines
			if (mapIdExtTotalSolCerradasVines.containsKey(ssTAMIdExternoPaso))
				mapIdExtTotalSolCerradasVines.get(ssTAMIdExternoPaso).add(objVinesFlotProg.Name);
			if (!mapIdExtTotalSolCerradasVines.containsKey(ssTAMIdExternoPaso))
				mapIdExtTotalSolCerradasVines.put(ssTAMIdExternoPaso, new Set<String>{objVinesFlotProg.Name});
		}
		System.debug('EN getDatosDistribuidores mapIdExtTotalSolCerradasVines: ' + mapIdExtTotalSolCerradasVines.keySet());
		System.debug('EN getDatosDistribuidores mapIdExtTotalSolCerradasVines: ' + mapIdExtTotalSolCerradasVines.Values());				        
        
        Map<String, Set<String>> mapIdExtTotal = new Map<String, Set<String>>();
        
        Set<String> setVinesPiso = new Set<String>();
        for (InventarioWholesale__c objInve : [SELECT  Id, TAM_IdExterno__c, Name FROM InventarioWholesale__c
             WHERE Dealer_Code__c =: strCodigoDistribuidor 
             AND TAM_IdExterno__c =:sIdModeloAnio
             AND Model_Year__c =: strAnioModelo 
             //AND Toms_Series_Name__c =: strSerie
             AND Model_Number__c =: strModelo
             AND TAM_ExteriorColorCode__c =: strIdColExt
             AND TAM_InteriorColorCode__c =: strIdColInt
             Order by Name
             ]){
             //Metelos al set de  setVinesPiso
             setVinesPiso.add(objInve.Name);
        }                                                            
		System.debug('EN getDistribuidores setVinesPiso: ' + setVinesPiso);		

        Set<String> setVinesTransito = new Set<String>();
        for (Inventario_en_Transito__c objInve : [SELECT  Id, TAM_IdExterno__c, Name FROM Inventario_en_Transito__c
             WHERE Dealer_Code__c =: strCodigoDistribuidor 
             AND TAM_IdExterno__c =:sIdModeloAnio
             AND Model_Year__c =: strAnioModelo 
             //AND Toms_Series_Name__c =: strSerie
             AND Model_Number__c =: strModelo
             AND TAM_ExteriorColorCode__c =: strIdColExt
             AND TAM_InteriorColorCode__c =: strIdColInt
             Order by Name
             ]){
             //Metelos al set de  setVinesTransito
             setVinesTransito.add(objInve.Name);
        }                                                            
		System.debug('EN getDistribuidores setVinesTransito: ' + setVinesTransito);		
		
		List<TAM_WrpDistSolFlotillaPrograma> lObjWrpDist = new List<TAM_WrpDistSolFlotillaPrograma>();	
		List<TAM_VinesFlotillaPrograma__c>	ListDistProgUps = new List<TAM_VinesFlotillaPrograma__c>();
		Set<String> setCaveDistr = new Set<String>();
		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
		
		//Toma los datos de los paremtros y forma la llave
		//String sIdExternoIni = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strVersion + '-' + strIdColExt + '-' + strIdColInt + '%';
		String sIdExternoIni = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt + '%';
		System.debug('EN getDatosDistribuidores sIdExternoIni: ' + sIdExternoIni);
		
		Integer intCnt = 0;
		//Ya tienes la llave consulta los datos en TAM_VinesFlotillaPrograma__c
		for (TAM_VinesFlotillaPrograma__c objDistrib : [Select ID, Name, TAM_Cliente__r.Codigo_Distribuidor__c,
			TAM_Cliente__r.Name, TAM_IdExterno__c From TAM_VinesFlotillaPrograma__c
			Where 
			//TAM_SolicitudCompraPROGRAMA__c =:recordId 
			TAM_SolicitudFlotillaPrograma__c =:recordId
			And TAM_IdExterno__c Like :sIdExternoIni 
			Order by Name]){
			System.debug('EN getDatosDistribuidores TAM_IdExterno__c: ' + objDistrib.TAM_IdExterno__c);	
			//Crea el objeto del tipo TAM_WrpDistSolFlotillaPrograma 	
			TAM_WrpDistSolFlotillaPrograma objPaso = new TAM_WrpDistSolFlotillaPrograma(
				intCnt, false, strCodigoDistribuidor, strAnioModelo, strSerie, strModelo, strVersion,
				objDistrib.TAM_IdExterno__c, 0, strCodigoDistribuidor + ' - ' + objDistrib.TAM_Cliente__r.Name,
				new List<TAM_WrpDistribuidores>()	
			);
			objPaso.sVIN = objDistrib.Name;
			objPaso.strIdColExt = strIdColExt;
			objPaso.strIdColInt = strIdColInt;
			objPaso.bolDisable = true;
			objPaso.bolSeleccionado = true;
			intCnt++;
			//Metelo al mapa de mapIdExtObjWrp
			mapIdExtObjWrp.put(objDistrib.TAM_IdExterno__c, objPaso);
		}//fin del for para TAM_VinesFlotillaPrograma__c
		
		//Recorre la lista de vines del set setVinesPiso
		for (String sVin : setVinesPiso){
			//String sIdExternoIniPaso  = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strVersion + '-' + strIdColExt + '-' + strIdColInt + '-' + sVin + '-' + sRectorTypePasoPrograma;
			String sIdExternoIniPaso  = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt + '-' + sVin + '-' + sRectorTypePaso; //sRectorTypePasoPrograma;
			String sIdExternoIniPasoCons = strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt;
			Set<String> setVinModelo = new Set<String>();
			
			System.debug('EN getDatosDistribuidores sIdExternoIniPasoCons: ' + sIdExternoIniPasoCons);
			//Busca el objInvWholeSale.TAM_IdExterno__c en el mapa de mapIdExtTotalSolCerradasVines
			if (mapIdExtTotalSolCerradasVines.containsKey(sIdExternoIniPasoCons)){
				setVinModelo = mapIdExtTotalSolCerradasVines.get(sIdExternoIniPasoCons);
				System.debug('EN getDatosDistribuidores YA LO ENCONTRO setVinModelo: ' + setVinModelo);
			}//Fin si mapIdExtTotalSolCerradasVines.containsKey(strIdExternoDistVal
				
			Boolean bExiste = false;
			//Busca objInvWholeSale.Name en setVinModelo
			if (!setVinModelo.isEmpty()) 
				if (setVinModelo.contains(sVin))
					bExiste = true;
			System.debug('EN getDatosDistribuidores bExiste: ' + bExiste);
			
			// Ve si no exite ya puesto eo otra solicitud
            if (!bExiste){
				if (mapIdExtObjWrp.containsKey(sIdExternoIniPaso)){
					mapIdExtObjWrp.get(sIdExternoIniPaso).strEstatusVIN = 'Piso';
					System.debug('EN getDatosDistribuidores TAM_WrpDistSolFlotillaPrograma Piso: ' + mapIdExtObjWrp.get(sIdExternoIniPaso));
				}            	
				//Si no exite el sIdExternoIniPaso en mapIdExtObjWrp agregalo
				if (!mapIdExtObjWrp.containsKey(sIdExternoIniPaso)){
					TAM_WrpDistSolFlotillaPrograma objPaso = new TAM_WrpDistSolFlotillaPrograma(
						intCnt, false, strCodigoDistribuidor, strAnioModelo, strSerie, strModelo, strVersion,
						sIdExternoIniPaso, 0, strCodigoDistribuidor + ' - ' + sNombreCliente,
						new List<TAM_WrpDistribuidores>()	
					);
					objPaso.sVIN = sVin;
					objPaso.strIdColExt = strIdColExt;
					objPaso.strIdColInt = strIdColInt;
					objPaso.bolDisable = true;
					objPaso.bolSeleccionado = false;
					objPaso.strEstatusVIN = 'Piso';
					intCnt++;				
					//Metelo al mapa de mapIdExtObjWrp
					mapIdExtObjWrp.put(sIdExternoIniPaso, objPaso);
				}//fin si !mapIdExtObjWrp.containsKey(sIdExternoIniPaso)
            }//Fin si !bExiste
			
		}//Fin del for para setVinesPiso
		
		//Recorre la lista de vines del set setVinesTransito
		for (String sVin : setVinesTransito){
			//String sIdExternoIniPaso  = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strVersion + '-' + strIdColExt + '-' + strIdColInt + '-' + sVin + '-' + sRectorTypePasoPrograma;
			String sIdExternoIniPaso  = recordId + '-' + strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt + '-' + sVin + '-' + sRectorTypePaso; //sRectorTypePasoPrograma;
			String sIdExternoIniPasoCons = strAnioModelo + '-' +  strSerie + '-' + strModelo + '-' + strIdColExt + '-' + strIdColInt;
			Set<String> setVinModelo = new Set<String>();
			
			System.debug('EN getDatosDistribuidores sIdExternoIniPasoCons: ' + sIdExternoIniPasoCons);
			//Busca el objInvWholeSale.TAM_IdExterno__c en el mapa de mapIdExtTotalSolCerradasVines
			if (mapIdExtTotalSolCerradasVines.containsKey(sIdExternoIniPasoCons)){
				setVinModelo = mapIdExtTotalSolCerradasVines.get(sIdExternoIniPasoCons);
				System.debug('EN getDatosDistribuidores YA LO ENCONTRO setVinModelo: ' + setVinModelo);
			}//Fin si mapIdExtTotalSolCerradasVines.containsKey(strIdExternoDistVal
				
			Boolean bExiste = false;
			//Busca objInvWholeSale.Name en setVinModelo
			if (!setVinModelo.isEmpty()) 
				if (setVinModelo.contains(sVin))
					bExiste = true;
					
			if (Test.isRunningTest())
				bExiste = false;		
			System.debug('EN getDatosDistribuidores bExiste: ' + bExiste);
			
			// Ve si no exite ya puesto eo otra solicitud
            if (!bExiste){
				if (mapIdExtObjWrp.containsKey(sIdExternoIniPaso)){
					mapIdExtObjWrp.get(sIdExternoIniPaso).strEstatusVIN = 'Transito';
					System.debug('EN getDatosDistribuidores TAM_WrpDistSolFlotillaPrograma Trans: ' + mapIdExtObjWrp.get(sIdExternoIniPaso));
				}
				//Si no exite el sIdExternoIniPaso en mapIdExtObjWrp agregalo
				if (!mapIdExtObjWrp.containsKey(sIdExternoIniPaso)){
					TAM_WrpDistSolFlotillaPrograma objPaso = new TAM_WrpDistSolFlotillaPrograma(
						intCnt, false, strCodigoDistribuidor, strAnioModelo, strSerie, strModelo, strVersion,
						sIdExternoIniPaso, 0, strCodigoDistribuidor + ' - ' + sNombreCliente,
						new List<TAM_WrpDistribuidores>()	
					);
					objPaso.sVIN = sVin;
					objPaso.strIdColExt = strIdColExt;
					objPaso.strIdColInt = strIdColInt;
					objPaso.bolDisable = true;
					objPaso.bolSeleccionado = false;
					objPaso.strEstatusVIN = 'Transito';
					intCnt++;				
					//Metelo al mapa de mapIdExtObjWrp
					mapIdExtObjWrp.put(sIdExternoIniPaso, objPaso);
				}//fin si !mapIdExtObjWrp.containsKey(sIdExternoIniPaso)
            }//Fin si !bExiste
			
		}//Fin del for para setVinesTransito		
		
		//Recorre la lista de reg del mapa mapIdExtObjWrp 
		for (String sIdExtrVin : mapIdExtObjWrp.keySet()){
			//Metelo a la lista de  lObjWrpDist
			lObjWrpDist.add(mapIdExtObjWrp.get(sIdExtrVin));
		}
		    	        
        //serializa la lista y crea el JSON
		sCadenaPaso = JSON.serialize(lObjWrpDist);              
        System.debug('EN getWrpModelo sCadenaPaso: ' + sCadenaPaso);
                 
        //Regresa la cadena
        return sCadenaPaso;
    }


    //Obtener distribuidores.
    public static Map<String, Account> getDistribuidores(Set<String> setCaveDistr) {
    	System.debug('EN getDistribuidores...');
    	Map<String, Account> mapClaveDistIdSfdc = new Map<String, Account>();
    	
    	for (Account objAccount : [SELECT Id, Name, Codigo_Distribuidor__c 
			FROM Account WHERE RecordType.DeveloperName = 'Dealer'
            ORDER BY Name ASC]){
    		//Mete los datos en el mapa de mapClaveDistIdSfdc
    		mapClaveDistIdSfdc.put(objAccount.Codigo_Distribuidor__c, objAccount);
    	}
        
		System.debug('EN getDistribuidores lstDistribuidores: ' + mapClaveDistIdSfdc);
        return mapClaveDistIdSfdc;
    }   

    //Obtener distribuidores.
    @AuraEnabled
    public static string getTipRegPrograma(String strTipRegNombre) {
    	System.debug('EN getTipRegPrograma...');
    	String sRectorTypePasoProgramaPaso = sRectorTypePasoPrograma; //Schema.SObjectType.TAM_VinesFlotillaPrograma__c.getRecordTypeInfosByName().get(strTipRegNombre).getRecordTypeId();
    	
		System.debug('EN getDistribuidores sRectorTypePasoProgramaPaso: ' + sRectorTypePasoProgramaPaso);
        return sRectorTypePasoProgramaPaso;
    }   

	//Regresa un objeto del tipo AddTelephoneDto si no hubo error a la hora de registrar el cliente		
	public static List<TAM_WrpDistSolFlotillaPrograma> JSONParserSFDC(String sJsonResp){
		System.debug('EN JSONParserSFDC: sJsonResp: ' + sJsonResp);
		
		List<TAM_WrpDistSolFlotillaPrograma> listObjWrp = new List<TAM_WrpDistSolFlotillaPrograma>();
		try{
            JSONParser parser = JSON.createParser(sJsonResp);
            //Ve si tiene algo el objeto de parser  
            while (parser.nextToken() != null) {//[{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":10,"intInventario":8,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2201","strSerie":"AVANZA","strVersion":"LE MT","strYear_Serie":"2019-AVANZA"},'{"boolBalanceOut":false,"boolClonado":false,"boolPagoTFS":false,"boolTriplePlay":false,"intDiasInventario":2,"intInventario":1,"intMSRP":100,"intTMEXMargen_Pesos":0,"intTMEXMargen_Porcentaje":0,"strAnioModelo":"2019","strClaveModelo":"2202","strSerie":"AVANZA","strVersion":"LE AT","strYear_Serie":"2019-AVANZA"} '']'
				//Inicia el detalle del objeto: sNombreObj
				if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
					//Toma el contenido del Json y conviertelo a SignInCls.class 
					TAM_WrpDistSolFlotillaPrograma objTAM_WrpDistSolFlotillaPrograma = (TAM_WrpDistSolFlotillaPrograma)parser.readValueAs(TAM_WrpDistSolFlotillaPrograma.class);
					listObjWrp.add(objTAM_WrpDistSolFlotillaPrograma);
				}//Fin si parser.getCurrentToken() == JSONToken.START_OBJECT
            }//Fin mientras parser.nextToken() != null			
		}catch(Exception ex){
			System.debug('ERROR EN JSONParserSFDC: sJsonResp: ' + ex.getMessage());
	 	}
		System.debug('ANTES DE SALIR DE JSONParserSFDC: listObjWrp: ' + listObjWrp);
			 	
		//Regresa el objeto objSignInClsPaso
		return listObjWrp;
	}    
    
    public static string getTipoRegSol(String sTipoRegSol, Boolean bSaveDatos) {
    	System.debug('EN getTipoRegSol sTipoRegSol: ' + sTipoRegSol);
    	
    	String sTipoRegSolPaso = '';
    	
    	if (bSaveDatos){
    	sTipoRegSolPaso = sTipoRegSol == sRectorTypePasoSolPrograma ? sRectorTypePasoPrograma :
    		(sTipoRegSol == sRectorTypePasoSolProgramaCerrada ? sRectorTypePasoPrograma :
    			( sTipoRegSol == sRectorTypePasoSolVentaCorporativa ? sRectorTypePasoGFlotilla : 
    		 		( sTipoRegSol == sRectorTypePasoSolVentaCorporativaCerrada ? sRectorTypePasoGFlotilla : sRectorTypePasoPrograma)
    		 	) 
    		);
    	}
		
		if (!bSaveDatos){	
    	sTipoRegSolPaso = sTipoRegSol == sRectorTypePasoSolPrograma ? sRectorTypePasoSolPrograma :
    		(sTipoRegSol == sRectorTypePasoSolProgramaCerrada ? sRectorTypePasoSolPrograma :
    			( sTipoRegSol == sRectorTypePasoSolVentaCorporativa ? sRectorTypePasoSolVentaCorporativa : 
    		 		( sTipoRegSol == sRectorTypePasoSolVentaCorporativaCerrada ? sRectorTypePasoSolVentaCorporativa : sRectorTypePasoSolPrograma)
    		 	) 
    		);
		}
		
		System.debug('EN getTipoRegSol sTipoRegSolPaso: ' + sTipoRegSolPaso);
        return sTipoRegSolPaso;
    }    
    
}