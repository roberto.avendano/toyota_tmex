/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica del WS SFDC_CreaLead_ws_rst

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    10-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class SFDC_CreaLead_ctrl_rst {

    //Un constructor por default
    public SFDC_CreaLead_ctrl_rst(){}

    public class ListaContactos{
        public List<candidato> candidatos;  
    }

    /* firstName = objCand.nombre != null ? objCand.nombre : null,
       lastName = objCand.apellidoPaterno != null ? objCand.apellidoPaterno : null,
       FWY_ApellidoMaterno__c = objCand.apelidoMaterno != null ? objCand.apelidoMaterno : null,
       email = objCand.correo != null ? objCand.correo : null,
       phone = objCand.telefono != null ? objCand.telefono : null,
       leadSource = objCand.origen != null ? objCand.origen : null,   
       TAM_FormaContactoPreferida__c =  sMediosContacto,
       FWY_Vehiculo__c = objCand.vehiculo != null ? objCand.vehiculo : null, 
       FWY_codigo_distribuidor__c = objCand.codigoDistribuidor != null ? objCand.codigoDistribuidor : null,
       TAM_OrigenProspectoFacebook__c = objCand.origenFacebook != null ? objCand.origenFacebook : null
    */
        
    public class candidato{
        public String nombre {get;set;}             //firstName
        public String apellidoPaterno {get;set;}    //lastName
        public String apelidoMaterno {get;set;}     //FWY_ApellidoMaterno__c
        public String correo {get;set;}             //email
        public String telefono {get;set;}           //phone
        public String origen {get;set;}             //leadSource
        public List<String> mediosContacto {get;set;} //TAM_FormaContactoPreferida__c
        public String vehiculo {get;set;}           //FWY_Vehiculo__c
        public String codigoDistribuidor {get;set;} //FWY_codigo_distribuidor__c    
                                                
        public String comentarios {get;set;} //FWY_Comentarios__c
        public String Source {get;set;} //leadSource
        public String Medium {get;set;} //TAM_FormaContactoPreferida__c
        public String campania {get;set;} //TAM_Campania__c

        public String origenFacebook {get;set;} 
    }

    public class DatosSalida{
        public List<salida> candidatos;          
    }
    
    public class salida{
        public String nombreCompleto {get;set;} 
        public String correo {get;set;}
        public String telefono {get;set;}        
        public Boolean statusCreacion {get;set;}        
        public String detalle {get;set;}
        
        public salida(String nombreCompleto, String correo, String telefono, Boolean statusCreacion
            , String detalle){
            this.nombreCompleto = nombreCompleto;
            this.correo = correo;
            this.telefono = telefono;
            this.statusCreacion = statusCreacion;
            this.detalle = detalle;
        }
    }   

    //El metodo que va a consultar los datos de los clientes que ya estan en la etapa de pedido en proceso y 
    //ya estan listos para facturarrse
    public static String getCreaLead(String sBody){ 
        System.debug('EN SFDC_CreaLead_ctrl_rst.getCreaLead sBody: ' + sBody);
        String sOk = '';
        
        StrIng sCodDist = ''; 
        Map<String, Lead> mapLeadIns = new Map<String, Lead>();
        DatosSalida objDatosSalida = new DatosSalida();
        List<salida> lSalida = new List<salida>();
        List<candidato> lDatosProspecto = new List<candidato>();
        ListaContactos objListaContactos = (ListaContactos) System.JSON.deserialize(sBody, ListaContactos.class);       
        System.debug('EN SFDC_CreaLead_ctrl_rst.getClientesParaFacturar objListaContactos: ' + objListaContactos);
        
        try{
                
	        //Ve si tiene algo la lista de objListaContactos.candidatos
	        if (!objListaContactos.candidatos.isEmpty()){
                //Recorre la lista de  candidatos y crea los objetos
                for (candidato objCand : objListaContactos.candidatos){
                    String sIdExt = (objCand.nombre != null ? objCand.nombre : '') + ' ' + (objCand.apellidoPaterno != null ? objCand.apellidoPaterno : '') + ' ' + (objCand.apelidoMaterno != null ? objCand.apelidoMaterno : '') + ' ' + (objCand.correo != null ? objCand.correo : '') + ' ' +  (objCand.telefono != null ? objCand.telefono : '');                   
                    String sMediosContacto = '';
                    //Recorre la lista de List<medioContacto> mediosContacto
                    if (!objCand.mediosContacto.isEmpty()){
                        for(String sMedioContac : objCand.mediosContacto){
                            sMediosContacto += sMedioContac + ';'; 
                        }//Fin del for para objCand.mediosContacto
                    }//Fin si !objCand.mediosContacto.isEmpty()
                    //Agrega el nuevo Lead al mapa de mapLeadIns
                    mapLeadIns.put(sIdExt, new Lead(
                          firstName = objCand.nombre != null ? objCand.nombre : null,
                          lastName = objCand.apellidoPaterno != null ? objCand.apellidoPaterno : null,
                          FWY_ApellidoMaterno__c = objCand.apelidoMaterno != null ? objCand.apelidoMaterno : null,
                          email = objCand.correo != null ? objCand.correo : null,
                          phone = objCand.telefono != null ? objCand.telefono : null,
                          leadSource = objCand.origen != null ? objCand.origen : null,   
                          TAM_FormaContactoPreferida__c =  sMediosContacto,
                          FWY_Vehiculo__c = objCand.vehiculo != null ? objCand.vehiculo : null, 
                          FWY_codigo_distribuidor__c = objCand.codigoDistribuidor != null ? objCand.codigoDistribuidor : null,
                          FWY_Comentarios__c = objCand.comentarios != null ? objCand.comentarios : null,
                          TAM_Campania__c = objCand.campania != null ? objCand.campania : null,
                          TAM_OrigenProspectoFacebook__c = objCand.origenFacebook != null ? objCand.origenFacebook : null
                        )
                    );
                }//Fin del for para objListaContactos.candidatos
	        }//Fin si !objListaContactos.candidatos.isEmpty()
            
            //No tiene nada la lista de candidatos 
            if (objListaContactos.candidatos.isEmpty())
                objDatosSalida.candidatos.add(new salida('', '', '', false, 'La lista de candidatos viene vacia.'));        

            System.debug('EN SFDC_CreaLead_ctrl_rst.getClientesParaFacturar mapLeadIns: ' + mapLeadIns.KeySet());
            System.debug('EN SFDC_CreaLead_ctrl_rst.getClientesParaFacturar mapLeadIns: ' + mapLeadIns.Values());            
            //Un SavePoint de prueba
            SavePoint svLead = Database.setSavepoint();
            
            //Ve si tiene algo el mapa de mapLeadIns
            if (!mapLeadIns.isEmpty()){
                Integer intCnt = 0;
                salida objSalida;
                //Toma la lista de Leads del mapa mapLeadIns
                List<Lead> lLeadsIns = mapLeadIns.values();
	            //Actualiza las Opp 
	            List<Database.UpsertResult> lDtbUpsRes = Database.upsert(lLeadsIns, Lead.id, false);
	            //Ve si hubo error
	            for (Database.UpsertResult objDtbUpsRes : lDtbUpsRes){
	                if (!objDtbUpsRes.isSuccess()){
	                    String sIdExtNom = (lLeadsIns.get(intCnt).firstName != null ? lLeadsIns.get(intCnt).firstName : '') + ' ' + (lLeadsIns.get(intCnt).lastName != null ? lLeadsIns.get(intCnt).lastName : '') + ' ' + (lLeadsIns.get(intCnt).FWY_ApellidoMaterno__c != null ? lLeadsIns.get(intCnt).FWY_ApellidoMaterno__c : '');
                        objSalida = new salida(sIdExtNom, 
                            (lLeadsIns.get(intCnt).email != null ? lLeadsIns.get(intCnt).email : ''), 
                            (lLeadsIns.get(intCnt).phone != null ? lLeadsIns.get(intCnt).phone : ''), 
                            false,
                            objDtbUpsRes.getErrors()[0].getMessage()
                        );
	                }//Fin si !objDtbUpsRes.isSuccess()
	                if (objDtbUpsRes.isSuccess()){                    
                        String sIdExtNom = (lLeadsIns.get(intCnt).firstName != null ? lLeadsIns.get(intCnt).firstName : '') + ' ' + (lLeadsIns.get(intCnt).lastName != null ? lLeadsIns.get(intCnt).lastName : '') + ' ' + (lLeadsIns.get(intCnt).FWY_ApellidoMaterno__c != null ? lLeadsIns.get(intCnt).FWY_ApellidoMaterno__c : '');
                        objSalida = new salida(sIdExtNom, 
                            (lLeadsIns.get(intCnt).email != null ? lLeadsIns.get(intCnt).email : ''), 
                            (lLeadsIns.get(intCnt).phone != null ? lLeadsIns.get(intCnt).phone : ''), 
                            true,
                            'El candidato fue creado con exito'
                        );	                
	                }
	                intCnt++;
	                //Agrega a la lista e lSalida el objeto salida
	                lSalida.add(objSalida);
	            }//Fin del for para lDtbUpsRes
                //Inicializa la lista de objDatosSalida.candidatos
                objDatosSalida.candidatos = lSalida; 
            }//fin si !mapLeadIns.isEmpty()
            
            //Un rollback de prueba
            //Database.rollback(svLead);
            
        }catch(Exception e){
            objDatosSalida.candidatos.add(new salida('', '', '', false, e.getMessage() + ' Linea: ' + e.getLineNumber()));
        }
        
        //Serializa el objeto final objDatosSalida
        sCodDist = JSON.serialize(objDatosSalida);

        System.debug('EN SFDC_CreaLead_ctrl_rst.getCreaLead ANTES DE ALIR sCodDist: ' + sCodDist);        
        //Regresa el resultado de la actualización
        return sCodDist;
    }
    
}