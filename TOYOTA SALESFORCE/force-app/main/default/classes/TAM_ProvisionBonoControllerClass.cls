public without sharing class TAM_ProvisionBonoControllerClass {
     
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> getBonos(String recordId){
        Id RecordTypeId = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByName().get('Bono').getRecordTypeId();
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT  TAM_VIN__r.Name,
                                                                      		  TAM_VIN__r.Id,
                                                                              TAM_VIN__r.Total_de_Movimientos_Reversa__c,
                                                                              TAM_CodigoDealer__c,
                                                                              TAM_NombreDealer__c,
                                                                              TAM_CodigoVenta__c,
                                                                              TAM_Serie__c,
                                                                              TAM_Modelo__c,
                                                                              TAM_AnioModelo__c,
                                                                              TAM_FechaEnvio__c,
                                                                              TAM_TipoMovimiento__c,
                                                                              TAM_FirstName__c,
                                                                              TAM_LastName__c,
                                                                      		  TAM_Version__c,
                                                                      		  TAM_FechaVenta__c,
                                                                              TAM_Factura__c,
                                                                              Bonos__c,
                                                                              Bonos__r.Id,
                                                                              Bonos__r.Name,
                                                                              TAM_BonoTMEX_Efectivo__c,
                                                                              TAM_ProductoFinanciero__c,
                                                                              TAM_BonoTMEX_PF__c,
                                                                              TAM_ProvisionarBonoEfectivo__c,
                                                                              TAM_ProvisionarBonoFinanciero__c,
                                                                              TAM_TransmitirBonoEfectivo__c,
                                                                              TAM_AplicanAmbos__c,
                                                                              TAM_IdExterno__c,
                                                                              TAM_ProvisionCerrada__c,
                                                                              TAM_CodigoContable__c
                                                                      FROM    TAM_DetalleProvisionIncentivo__c
                                                                      WHERE   TAM_ProvisionIncentivos__c =: recordId
                                                                      
                                                                      AND     RecordTypeId =: RecordTypeId
                                                                      ORDER BY Bonos__r.Name ASC,TAM_AnioModelo__c ASC, TAM_Modelo__c ASC,  TAM_FechaEnvio__c ASC
                                                                     ];
        
        return lstDetalleProvision;
    }

    /*GUARDADO DE REGISTROS*/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> setRecordsBonos(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            objDetalle.TAM_Guardado__c = true;
            lstDetalleUpsert.add(objDetalle);
        }
                
        try {
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }        
        return lstDetalleUpsert;
    } 

    /*CERRAR PROVISION*/
    @AuraEnabled
    public static List<TAM_DetalleProvisionIncentivo__c> cerrarProvision(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId){
        Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10);
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            lstDetalleUpsert.add(objDetalle);
        }

        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
            
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
        return lstDetalleUpsert;
    }

}