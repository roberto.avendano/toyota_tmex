public with sharing class TAM_ClienteVehiculoClass {

    //After
    public static void afteUpdate(List<TAM_ClienteVehiculo__c> lstNewClienteVehiculo){
       	Set<String> setIdOwner = new Set<String>();
		Map<String, String> mapIdUsrNomDis = new Map<String, String>();
		Map<String, String> mapNomGrupoIdGrupo = new Map<String, String>();
		List<TAM_ClienteVehiculo__Share>  lClienteVehiculoShare = new List<TAM_ClienteVehiculo__Share>();
        List<AccountShare> lAccountShare = new List<AccountShare>();        
		
		Map<String, String> mapVinVehiculoIdReg = new Map<String, String>();
        Map<String, Map<String, String>> mapNomDistVinIdCte = new Map<String, Map<String, String>>();
		
        System.debug('ENTRO AL HANDLER TRIGGER ...');           
        for(TAM_ClienteVehiculo__c objCteVehic : lstNewClienteVehiculo){
        	//Ve si tiene algo el cammpo de OwnerId
			setIdOwner.add(objCteVehic.OwnerId);
			//Toma el campo Name y quita el vin
			String sVinName = objCteVehic.Name;
			String sVinPaso = sVinName.contains('-') ? sVinName.substring(sVinName.indexOf('-') + 2, sVinName.length()) : sVinName;
            System.debug('ENTRO AL HANDLER TRIGGER sVinPaso: ' + sVinPaso);
			if (sVinPaso != '' && sVinPaso != null)
               mapVinVehiculoIdReg.put(sVinPaso, objCteVehic.id); 
        } //VALOR MOTRIZ S DE RL DE CV - 3MYDLBYV0LY711312
        System.debug('ENTRO AL HANDLER TRIGGER mapVinVehiculoIdReg: ' + mapVinVehiculoIdReg.keySet());           
        System.debug('ENTRO AL HANDLER TRIGGER mapVinVehiculoIdReg: ' + mapVinVehiculoIdReg.Values());           
        
        //Consulta los datos de las opp asociadas a los vines y saca el nombre del dealer
        for (Opportunity objOpp : [Select o.Name, o.AccountId, o.TAM_Vin__c, o.TAM_Distribuidor__r.Name 
                From Opportunity o where o.TAM_Vin__c IN :mapVinVehiculoIdReg.KeySet()]){
            if (objOpp.TAM_Distribuidor__c != null){
                if (mapNomDistVinIdCte.containsKey(objOpp.TAM_Distribuidor__r.Name))
                    mapNomDistVinIdCte.get(objOpp.TAM_Distribuidor__r.Name).put(objOpp.TAM_Vin__c, objOpp.AccountId);
                if (!mapNomDistVinIdCte.containsKey(objOpp.TAM_Distribuidor__r.Name))
                    mapNomDistVinIdCte.put(objOpp.TAM_Distribuidor__r.Name, new Map<String, String>{objOpp.TAM_Vin__c => objOpp.AccountId});                    
            }
        }
        System.debug('ENTRO AL HANDLER TRIGGER mapNomDistVinIdCte: ' + mapNomDistVinIdCte.keySet());           
        System.debug('ENTRO AL HANDLER TRIGGER mapNomDistVinIdCte: ' + mapNomDistVinIdCte.Values());           
        
        //Consulta los reg asiados a setIdOwner
        for (User usuario : [Select u.id, u.Distribuidor__c, u.CodigoDistribuidor__c From User u
			where u.CodigoDistribuidor__c != null and ID IN :setIdOwner]){
        	mapIdUsrNomDis.put(usuario.id, usuario.Distribuidor__c);
        }
        
        //Consulta los grupos 
        for (Group Gropo : [SELECT ID, Name FROM Group 
            WHERE Name LIKE '%TOYOTA%' OR Name LIKE '%TMEX%' ORDER BY NAME]){
        	mapNomGrupoIdGrupo.put(Gropo.Name, Gropo.id);
        }
        
        //Crea la colaboracion para el reg 
        for(TAM_ClienteVehiculo__c objCteVehic : lstNewClienteVehiculo){
			String IdNomdis = mapIdUsrNomDis.containsKey(objCteVehic.OwnerId) ? mapIdUsrNomDis.get(objCteVehic.OwnerId) : ' ';
            //Toma el campo Name y quita el vin
            String sVinName = objCteVehic.Name;
            String sVinPaso = sVinName.contains('-') ? sVinName.substring(sVinName.indexOf('-') + 2, sVinName.length()) : null;
            System.debug('ENTRO AL HANDLER TRIGGER sVinPaso: ' + sVinPaso);           
			
			//Recorre el mapa de mapNomDistVinIdCte y busca el VIN
			for (String sNomDistPaso : mapNomDistVinIdCte.KeySet()){
			    //Ve si el vin esxste en mapNomDistVinIdCte.get(sNomDistPaso)
			    if (mapNomDistVinIdCte.get(sNomDistPaso).KeySet().contains(sVinPaso)){
                    System.debug('ENTRO AL HANDLER TRIGGER sNomDistPaso.toUpperCase(): ' + sNomDistPaso.toUpperCase()); 
                    //Si esa una prueba
		            if (Test.isRunningTest())
		                IdNomdis = 'TOYOTA INNOVA FLETEROS';
		            TAM_ClienteVehiculo__Share objPasoShare = new TAM_ClienteVehiculo__Share(
		                    ParentId = objCteVehic.id,
		                    UserOrGroupId = mapNomGrupoIdGrupo.get(sNomDistPaso.toUpperCase()),
		                    AccessLevel = 'Read'
		            );
		            //Agregalo a la lista lClienteVehiculoShare     
		            lClienteVehiculoShare.add(objPasoShare);
		            System.debug('ENTRO AL HANDLER TRIGGER objCteVehic.ID: ' + objPasoShare); 
		            //Crea la colaboración para el cliente asociado a la opp
                    
                    String sCteId = mapNomDistVinIdCte.get(sNomDistPaso).get(sVinPaso);
                    System.debug('ENTRO AL HANDLER TRIGGER sCteId: ' + sCteId);
		            //Crea la colaboración para las cuetas a partir del Dealer
		            AccountShare objAccountShare = new AccountShare(
                            AccountId = sCteId,
                            AccountAccessLevel = 'Read',
                            CaseAccessLevel = 'None',
                            OpportunityAccessLevel = 'None',
                            UserOrGroupId = mapNomGrupoIdGrupo.get(sNomDistPaso.toUpperCase())
                    );
                    //Agregalo a la lista lClienteVehiculoShare
                    if ( mapNomDistVinIdCte.get(sNomDistPaso).get(sVinPaso) != null) 
		              lAccountShare.add(objAccountShare);
                    System.debug('ENTRO AL HANDLER TRIGGER objAccountShare: ' + objAccountShare);
			    }//Fin si mapNomDistVinIdCte.get(sNomDistPaso).contains(sVinPaso)
			}//Fin del for para mapNomDistVinIdCte.KeySet()
			
        }
        System.debug('ENTRO AL HANDLER TRIGGER lClienteVehiculoShare lClienteVehiculoShare: ' + lClienteVehiculoShare);           
        System.debug('ENTRO AL HANDLER TRIGGER lClienteVehiculoShare lAccountShare: ' + lAccountShare);           

		if (!lClienteVehiculoShare.isEmpty()){
       		//Actualiza las Opp 
			List<Database.Saveresult> lDtbUpsRes = Database.insert(lClienteVehiculoShare, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
           			System.debug('En la AccSh: hubo error a la hora de crear/Actualizar la colabora para la Opp: ' + objDtbUpsRes.getErrors()[0].getMessage());
			}//Fin del for para lDtbUpsRes
		}//Fin si !lClienteVehiculoShare.isEmpty()

        if (!lAccountShare.isEmpty()){
            //Actualiza las Opp 
            List<Database.Saveresult> lDtbUpsRes = Database.insert(lAccountShare, false);
            //Ve si hubo error
            for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
                if (!objDtbUpsRes.isSuccess())
                    System.debug('En la AccSh: hubo error a la hora de crear/Actualizar la colabora para el Cte: ' + objDtbUpsRes.getErrors()[0].getMessage());
            }//Fin del for para lDtbUpsRes
        }//Fin si !lAccountShare.isEmpty()
        
    }
    
}