/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de la clase Test
******************************************************************************* */
@isTest
public class TAM_lookUpController_Test {
    
    @testSetup static void setup() {
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009UruJQAS'];
        //Cuenta
        Account a01 = new Account(
            Name = 'Dealer',
            Codigo_Distribuidor__c = '57000'
        );
        insert a01;
        
        //Se inserta un registro del custom setting TAM_DetalleEdoCta__c
        TAM_DetalleEdoCta__c customSetting1 = new TAM_DetalleEdoCta__c();
        customSetting1.TAM_DiasVigencia__c = 97;
        insert customSetting1;
        
        
        //Calendario TOYOTA
        TAM_CalendarioToyota__c  calToyota = new TAM_CalendarioToyota__c ();
        calToyota.name = 'De Prueba';
        calToyota.TAM_FechaInicio__c = date.today().addDays(-50);
        calToyota.TAM_FechaFin__c  = date.today().addDays(-10);
        insert calToyota;
        
        
        //Contacto
        Contact contact= new Contact();
        contact.NombreUsuario__c = 'Contacto RH1';
        contact.lastName = 'Contacto RH1';
        contact.Email = 'test@test.com';
        contact.AccountId = a01.id;
        insert contact;
        
        //VIN 1
        Vehiculo__c v01 = new Vehiculo__c(
            Name='12345678901234567',
            Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        //VIN 2
        Vehiculo__c v02 = new Vehiculo__c(
            Name='AQRE5678901234567',
            Id_Externo__c='AQRE5678901234567'
        );
        insert v02;
        
        //VIN 3 VC
         Vehiculo__c v03 = new Vehiculo__c(
            Name='VCCE5678901234567',
            Id_Externo__c='VCCE5678901234567'
        );
        insert v03;
        
		//Se inserta la solicitud de autodemo        
        TAM_SolicitudAutoDemo__c solAd = new TAM_SolicitudAutoDemo__c ();
        solAd.TAM_VIN__c = v03.id;
        insert solAd;
        
        
        // SERIE
        Serie__c ser01 = new Serie__c(
            Id_Externo_Serie__c = 'X',
            Name = 'X'
        );
        insert ser01;
        
        //MODELO
        Modelo__c modelo01 = new Modelo__c(
            Serie__c = ser01.Id,
            ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        //MOVIMIENTO 1
        Movimiento__c m01 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now().addDays(-30),
            Sale_Date__c = date.today().addDays(-30),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '06',
            Fleet__c = 'C'
        ); 
        insert m01;
        
        //MOVIMIENTO 2
        Movimiento__c m02 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now().addDays(-40),
            Sale_Date__c = date.today().addDays(-40),
            VIN__c = v01.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RDR',
            Sale_Code__c = '06',
            Fleet__c = 'N'
        );
        insert m02;
        
        //VIN
        Vehiculo__c vcUpd1 = new Vehiculo__c ();
        vcUpd1.id = v03.id;
        vcUpd1.Ultimo_Movimiento__c = m02.id;
        update vcUpd1;
        
        
        
        // MOVIMIENTO 3
        Movimiento__c m03 = new Movimiento__c(
            Distribuidor__c = a01.Id,
            Submitted_Date__c = Datetime.now().addDays(-40),
            Sale_Date__c = date.today().addDays(-40),
            VIN__c = v02.Id,
            Id_Modelo__c = modelo01.Id,
            Trans_Type__c = 'RVRSL',
            Sale_Code__c = '06',
            Fleet__c = 'N'
        );
        insert m03;
        
        // INVENTARIO WHOSALE
        InventarioWholesale__c invWh = new InventarioWholesale__c();
        invWh.name = '12345678901234567';
        invWh.Dealer_Code__c = '57000';
        insert invWh;
        
        //INVENTARIO EN TRANSITO
        Inventario_en_Transito__c invF = new Inventario_en_Transito__c();
        invF.name = '12345678901234567'; 
        invF.Dealer_Code__c = '57000';
        insert invF;
        
        //Provisión de incentivos
        TAM_ProvisionIncentivos__c provTest01 = new TAM_ProvisionIncentivos__c();
        provTest01.name = 'Provisión Agosto de prueba';
        provTest01.TAM_AnioDeProvision__c = '2020';
        provTest01.TAM_MesDeProvision__c = 'Agosto';
        insert provTest01;
        
        //Detalle de Provisión
        String recordIdProvisionRetail  = Schema.SObjectType.TAM_DetalleProvisionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
        //Retail
        TAM_DetalleProvisionIncentivo__c detalleProv01 = new TAM_DetalleProvisionIncentivo__c();
        detalleProv01.name = '12345678901234567-HILUX-7495-2020-a26e00000013ycLAAQ-Bono Lealtad Hilux Prius';
        detalleProv01.RecordTypeId = recordIdProvisionRetail;
        detalleProv01.RetailSinReversa_Cerrado__c = false;
        detalleProv01.TAM_AnioModelo__c = '2020';
        detalleProv01.TAM_AplicanAmbos__c = false;
        detalleProv01.TAM_Clasificacion__c = 'Sin Reversa';
        detalleProv01.TAM_CodigoContable__c = '7400B2';
        detalleProv01.TAM_CodigoDealer__c = '57002';
        detalleProv01.TAM_Factura__c = 'FVA12330';
        detalleProv01.TAM_FechaCierre__c = date.today();
        detalleProv01.TAM_FechaEnvio__c = '2020-06-18 15:39:44';
        detalleProv01.TAM_FechaVenta__c = '2020-06-17';
        detalleProv01.TAM_FirstName__c = 'DORA LUZ';
        detalleProv01.TAM_IncentivoTMEX_Efectivo__c = 	5040.0;
        detalleProv01.TAM_IncentivoTMEX_PF__c = 0;
        detalleProv01.TAM_LastName__c = 'VAZQUEZ PEREZ';
        detalleProv01.TAM_Lealtad__c = false;
        detalleProv01.TAM_Modelo__c = '7495';
        detalleProv01.TAM_NombreDealer__c = 'TOYOTA TEST';
        detalleProv01.TAM_PagadoConTFS__c = false;
        detalleProv01.TAM_PagadoSinTFS__c = true;
        detalleProv01.TAM_ProvicionarEfectivo__c = true;
        detalleProv01.TAM_Provisionado__c = false;
        detalleProv01.TAM_ProvisionarBonoEfectivo__c = false;
        detalleProv01.TAM_ProvisionarBonoFinanciero__c = false;
        detalleProv01.TAM_ProvisionarLealtad__c = false;
        detalleProv01.TAM_ProvisionCerrada__c = true;
        detalleProv01.TAM_Serie__c	 = 'HILUX';
        detalleProv01.TAM_SolicitadoDealer__c = false;
        detalleProv01.TAM_TipoMovimiento__c = 'RDR';
        detalleProv01.TAM_TMEXEfectivoMenor__c = 5040; 
        detalleProv01.TAM_TransmitirDealer__c = true;
        detalleProv01.TAM_VIN__c  =  v01.id;
        detalleProv01.TAM_ProvisionIncentivos__c =  provTest01.id;
        insert detalleProv01;
                
        //DETALLE ESTADO DE CUENTA 1
        TAM_DetalleEstadoCuenta__c detalleEdoCta = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta.TAM_DetalleProvision__c = detalleProv01.id;
        detalleEdoCta.TAM_VIN__c = '12345678901234567';
        detalleEdocta.TAM_IncentivoPropuestoDealer__c = 4959;
        detalleEdocta.TAM_Estatus_Finanzas__c = 'Pago Autorizado TMEX';
        detalleEdocta.TAM_CodigoDealer__c = '57002';
        detalleEdoCta.TAM_FolioFactura__c = 'SAS1OE';
        detalleEdoCta.TAM_Fecha_Pago__c = date.today();
        insert detalleEdoCta;        
        
        //DETALLE ESTADO DE CUENTA 2
        TAM_DetalleEstadoCuenta__c detalleEdoCta02 = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta02.TAM_VIN__c = 'RAQ45678901234567';
        detalleEdoCta02.TAM_IncentivoPropuestoDealer__c = 4959;
        detalleEdoCta02.TAM_Serie__c = 'HILUX';
        detalleEdoCta02.TAM_Modelo__c = '9405';
        detalleEdoCta02.TAM_AnioModelo__c = '2020';
        detalleEdoCta02.TAM_CodigoDealer__c = '57000';
        detalleEdoCta02.TAM_Estatus_Finanzas__c = 'Pago Autorizado TMEX';
        detalleEdoCta02.TAM_CodigoDealer__c = '57002';
        detalleEdoCta02.TAM_FolioFactura__c = 'SAS1OE';
        detalleEdoCta02.TAM_Fecha_Pago__c = date.today();  
        insert detalleEdoCta02;
        
        //DETALLE ESTADO DE CUENTA 3
        TAM_DetalleEstadoCuenta__c detalleEdoCta03 = new TAM_DetalleEstadoCuenta__c();
        detalleEdoCta03.TAM_VIN__c = 'AQRE5678901234567';
        detalleEdoCta03.TAM_IncentivoPropuestoDealer__c = 4959;
        detalleEdoCta03.TAM_Serie__c = 'HILUX';
        detalleEdoCta03.TAM_Modelo__c = '9405';
        detalleEdoCta03.TAM_AnioModelo__c = '2020';
        detalleEdoCta03.TAM_CodigoDealer__c = '57000';
        detalleEdoCta03.TAM_Estatus_Finanzas__c = 'Pago Autorizado TMEX';
        detalleEdoCta03.TAM_CodigoDealer__c = '57002';
        detalleEdoCta03.TAM_FolioFactura__c = 'SAS1OE';
        detalleEdoCta03.TAM_Fecha_Pago__c = date.today();
        insert detalleEdoCta03;
        
        system.runAs(thisUser){  
            //Obtenemos el rol de la instancia 
            UserRole uR = [SELECT Id FROM UserRole limit 1];
            //Obtenemos un perfil RH de muestra de la instancia
            Profile p = [SELECT Id FROM Profile WHERE Name='Gerente de Ventas Distribuidor']; 
            
            //Se crea un usuario de SFDC relacionado a el contacto usado en la licencia
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                              EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = p.Id,ContactId = contact.id,PortalRole  = 'Manager',
                              TimeZoneSidKey='America/Los_Angeles', UserName='test99@testorg.com');
            system.debug('usuario'+u);
            insert u;
            
            
        }
        
    }
    
    
    @isTest static void testMethod1(){
        User usrObj = [Select id,CodigoDistribuidor__c FROM User WHERE UserName='test99@testorg.com'];
        List<lookUpController.wrapperMovimiento> listaDD = new List<lookUpController.wrapperMovimiento>();
        listaDD = lookUpController.vinDealerDaily('12345678901234567', usrObj.id,true);
        system.assertEquals(true,!listaDD.isEmpty());
        
        List<lookUpController.wrapperMovimiento> listaDD2 = new List<lookUpController.wrapperMovimiento>();
        listaDD2 = lookUpController.vinDealerDaily('12345678901234567', usrObj.id,false);
        system.assertEquals(true,!listaDD2.isEmpty());
        
        List<lookUpController.wrapVIN> listaBDDVentas = new List<lookUpController.wrapVIN>();
        listaBDDVentas = lookUpController.vinBDDVentas('12345678901234567', usrObj.id);
        system.assertEquals(true,!listaBDDVentas.isEmpty());
        
        List<lookUpController.wrapVIN> listaBDDVentasF = new List<lookUpController.wrapVIN>();
        listaBDDVentasF = lookUpController.vinInventarioF('123456789012345', usrObj.id);
        system.assertEquals(true,!listaBDDVentasF.isEmpty());
    }
    
    @isTest static void testMethod2(){
        User usrObj = [Select id,CodigoDistribuidor__c FROM User WHERE UserName='test99@testorg.com'];
        
        List<TAM_SearchVINBono.wrapVIN> listaBDDVentasG = new List<TAM_SearchVINBono.wrapVIN>();
        listaBDDVentasG = TAM_SearchVINBono.vinInventarioG('12345678901234567', usrObj.id);
        system.assertEquals(true,!listaBDDVentasG.isEmpty());
        
        List<TAM_SearchVINBono.wrapVIN> listaBDD = new List<TAM_SearchVINBono.wrapVIN>();
        listaBDD = TAM_SearchVINBono.vinBDDealerDaily('12345678901234567', usrObj.id);
        system.assertEquals(false,!listaBDD.isEmpty());
    }
    
    @isTest static void testMethod3(){
        User usrObj = [Select id,CodigoDistribuidor__c FROM User WHERE UserName='test99@testorg.com'];
        List<lookUpController.wrapperEstadoCta> wrrapperTest = new List<lookUpController.wrapperEstadoCta>();
        lookUpController.wrapperEstadoCta wrrapperItem = new lookUpController.wrapperEstadoCta();
        wrrapperItem.VIN = 'QWERTYAKDKAQP20';
        wrrapperItem.Id  = '01029101';
        wrrapperItem.apellidoPropietario = 'Figueroa';
        wrrapperItem.anioModelo = '2020';
        wrrapperItem.codigoDealer = '57011';
        wrrapperItem.comentarioDealer = 'Todo ok';
        wrrapperItem.EdoCtaCerrado = false;
        wrrapperItem.estatusVINEdoCta = 'Autorizado de cobro';
        wrrapperItem.facturaVenta  =  'Test001';
        wrrapperItem.nombreDealer  ='TOYOTA GUADALAJARA';
        wrrapperItem.nombrePropietario = 'Hector';
        wrrapperItem.IdDetalleProvision = 'Test';
        wrrapperItem.IdDetalleEdoCta = 'Estado cuenta';
        wrrapperItem.IdEstadoCuenta = 'EstadoCuenta';
        wrrapperItem.serie  = 'Corolla';
        wrrapperItem.codigoModelo = '5462';    
        wrrapperItem.incentivoProvisionadoDealer = 12391;
        wrrapperItem.incentivoSolicitadoDealer = 12391;
        wrrapperItem.nombreProvision = 'Provision de prueba';
        wrrapperItem.idProvision = 'AQDNJHED56474';
        wrrapperItem.idFacturaVenta = 'FACTURA2010';
        wrrapperItem.VINEntregado = true;
        
        wrrapperTest.add(wrrapperItem);
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009Un4iQAC'];        
        system.runAs(thisUser){
            List<lookUpController.wrapVIN> agregarVINRetail1 = new List<lookUpController.wrapVIN>();
            agregarVINRetail1 = lookUpController.agregarVINEdoCta('12345678901234567',usrObj.id,false,wrrapperTest,'Retail'); 
            system.assertEquals(true,!agregarVINRetail1.isEmpty());
            
            List<lookUpController.wrapVIN> agregarVINRetail2 = new List<lookUpController.wrapVIN>();
            agregarVINRetail2 = lookUpController.agregarVINEdoCta('RAQ45678901234567',usrObj.id,false,wrrapperTest,'Retail');
            system.assertEquals(true,!agregarVINRetail2.isEmpty());
            
            List<lookUpController.wrapVIN> agregarVINVC = new List<lookUpController.wrapVIN>();
            agregarVINVC = lookUpController.agregarVINEdoCta('12345678901234567',usrObj.id,false,wrrapperTest,'VentaCorporativa'); 
            system.assertEquals(true,!agregarVINVC.isEmpty());
            
            List<lookUpController.wrapVIN> agregarVINBono = new List<lookUpController.wrapVIN>();
            agregarVINBono = lookUpController.agregarVINEdoCta('12345678901234567',usrObj.id,false,wrrapperTest,'Bono'); 
            system.assertEquals(true,!agregarVINBono.isEmpty());
        }
    }
    
    //Metodo para probar las reglas de validación de por que no esta un VIN en el estado de cuenta
    @isTest static void testMethod4(){
        User usrObj = [Select id,CodigoDistribuidor__c FROM User WHERE UserName='test99@testorg.com'];
        List<lookUpController.wrapperEstadoCta> wrrapperTest = new List<lookUpController.wrapperEstadoCta>();
        lookUpController.wrapperEstadoCta wrrapperItem = new lookUpController.wrapperEstadoCta();
        wrrapperItem.VIN = 'AQRE5678901234567';
        wrrapperItem.Id  = '01029101';
        wrrapperItem.apellidoPropietario = 'Figueroa';
        wrrapperItem.anioModelo = '2020';
        wrrapperItem.codigoDealer = '57011';
        wrrapperItem.comentarioDealer = 'Todo ok';
        wrrapperItem.EdoCtaCerrado = false;
        wrrapperItem.estatusVINEdoCta = 'Autorizado de cobro';
        wrrapperItem.facturaVenta  =  'Test001';
        wrrapperItem.nombreDealer  ='TOYOTA GUADALAJARA';
        wrrapperItem.nombrePropietario = 'Hector';
        wrrapperItem.IdDetalleProvision = 'Test';
        wrrapperItem.IdDetalleEdoCta = 'Estado cuenta';
        wrrapperItem.IdEstadoCuenta = 'EstadoCuenta';
        wrrapperItem.serie  = 'Corolla';
        wrrapperItem.codigoModelo = '5462';    
        wrrapperItem.incentivoProvisionadoDealer = 12391;
        wrrapperItem.incentivoSolicitadoDealer = 12391;
        wrrapperItem.nombreProvision = 'Provision de prueba';
        wrrapperItem.idProvision = 'AQDNJHED56474';	
        wrrapperItem.idFacturaVenta = 'FACTURA2010';
        wrrapperItem.VINEntregado = true;
        
        wrrapperTest.add(wrrapperItem);
        User thisUser = [SELECT Id FROM User WHERE Id = '0051Y000009Un4iQAC'];        
        system.runAs(thisUser){
            List<lookUpController.wrapVIN> agregarVINRetail1 = new List<lookUpController.wrapVIN>();
            agregarVINRetail1 = lookUpController.agregarVINEdoCta('AQRE5678901234567',usrObj.id,false,wrrapperTest,'Retail'); 
            system.assertEquals(false, !agregarVINRetail1.isEmpty());
			
            List<lookUpController.wrapVIN> agregarVINRetailVC1 = new List<lookUpController.wrapVIN>();
            agregarVINRetailVC1 = lookUpController.agregarVINEdoCta('AQRE5678901234567',usrObj.id,false,wrrapperTest,'VentaCorporativa');
            system.assertEquals(false, !agregarVINRetailVC1.isEmpty());
            
            List<lookUpController.wrapVIN> agregarVINRetailVC2 = new List<lookUpController.wrapVIN>();
            agregarVINRetailVC2 = lookUpController.agregarVINEdoCta('VCCE5678901234567',usrObj.id,false,wrrapperTest,'VentaCorporativa');
            system.assertEquals(true, !agregarVINRetailVC2.isEmpty());
            
        }
    }
}