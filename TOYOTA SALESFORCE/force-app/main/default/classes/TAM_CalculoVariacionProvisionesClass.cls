/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Clase para obtener variaciones de incentivos cobrados vs lo provisionado por el área de finanzas de Toyota.
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		09/Junio/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
public without sharing class TAM_CalculoVariacionProvisionesClass {
    
    //Constantes
	static final Boolean BOOLEAN_TRUE = TRUE;
    static final Boolean BOOLEAN_FALSE = FALSE;
    static final Double DOUBLE_CERO = 0.0;
    
	//Variables Publicas
    public static Map<String, TAM_DetalleProvisionIncentivo__c> mapDetalleProvisiones {get;set;}
    
    //Obtener Detalle Estado de Cuenta Pagados
    public static void getDetalleCuentaPagados(Map<String, TAM_DetalleEstadoCuenta__c> mapDetalleCuenta){ 
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision = [SELECT 	Id, TAM_VIN__r.Id,TAM_VIN__r.Name, TAM_IncentivoTMEX_Efectivo__c
                                                                      FROM		TAM_DetalleProvisionIncentivo__c
                                                                      WHERE		TAM_VIN__r.Name IN: mapDetalleCuenta.keySet()
                                                                      AND		TAM_IncentivoTMEX_Efectivo__c != NULL];
        mapDetalleProvisiones =  new Map<String, TAM_DetalleProvisionIncentivo__c>();
        if(!lstDetalleProvision.isEmpty()){
            for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
                mapDetalleProvisiones.put(objDetalle.TAM_VIN__r.Name, objDetalle);
            }
        }
        if(!mapDetalleProvisiones.isEmpty()){
            calculoVariacion(mapDetalleCuenta);
        }
    }

    //Realizar el calculo de Variación e insertar variaciones negativas.
    public static void calculoVariacion(Map<String, TAM_DetalleEstadoCuenta__c> mapDetalleEstadoCuenta){
        Double dblVariacion = DOUBLE_CERO;
        Double dblMontoProvisionado = DOUBLE_CERO;
        Double dblMontoPagado = DOUBLE_CERO;
        List<TAM_VariacionProvisiones__c> lstVariacionProvisiones = new List<TAM_VariacionProvisiones__c>();
        
        //Obtener Variación
        if(!mapDetalleEstadoCuenta.isEmpty()){
            for(String strVIN : mapDetalleEstadoCuenta.keySet()){
                if(mapDetalleProvisiones.containsKey(strVIN)){
                    TAM_VariacionProvisiones__c objVariacionProvision = new TAM_VariacionProvisiones__c();
                    dblMontoProvisionado = mapDetalleProvisiones.get(strVIN).TAM_IncentivoTMEX_Efectivo__c;
                    dblMontoPagado = mapDetalleEstadoCuenta.get(strVIN).TAM_Monto_sin_IVA__c;
                    //Calcular variación "Monto Pagado" - "Monto Provisionado"
                    dblVariacion = dblMontoPagado - dblMontoProvisionado;
                    //Llenar obj
                    objVariacionProvision.Name = strVIN;
                    objVariacionProvision.TAM_VIN__c=mapDetalleProvisiones.get(strVIN).TAM_VIN__r.Id;
                    objVariacionProvision.TAM_DetalleEstadoCuenta__c = mapDetalleEstadoCuenta.get(strVIN).Id;
                    objVariacionProvision.TAM_DetalleProvision__c = mapDetalleProvisiones.get(strVIN).Id;
                    lstVariacionProvisiones.add(objVariacionProvision);
                    
                }
            }
        }
        
        //Insertar registros negativos
        if(!lstVariacionProvisiones.isEmpty()){
            try {
                Database.upsert(lstVariacionProvisiones,TAM_VariacionProvisiones__c.Id);
            }catch (DmlException e) {
                System.debug(e.getMessage());
            } 
        }
    }
}