/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene los campos que se necesitan para crear la expcecion de tipo 
    					Flotilla o Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    14-Mayo-2020      Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitFlotProgExcepcionVinesCmWrp {

    @AuraEnabled public String strDistribuidor {get;set;}
    @AuraEnabled public String strVin {get;set;}
    @AuraEnabled public String strFolioExcepcion {get;set;}
    @AuraEnabled public String strFechaRecibo {get;set;}
    @AuraEnabled public Date dtFechaRecibo {get;set;}    
    @AuraEnabled public String strIdRegistroDOD {get;set;}

    @AuraEnabled public Boolean bolSeleccionar {get;set;}
    @AuraEnabled public Boolean bolSeleccionado {get;set;}
    @AuraEnabled public Boolean bolVinBloqueado {get;set;}
    @AuraEnabled public Boolean bolSelBloqueado {get;set;}

    @AuraEnabled public Boolean bolConfirmaEntrega {get;set;}
    @AuraEnabled public Boolean bolConfirmado {get;set;}
    @AuraEnabled public Date dtFechaEntrega {get;set;}
    @AuraEnabled public String strTipoPago {get;set;}
    @AuraEnabled public String strCancelacion {get;set;}
    @AuraEnabled public Boolean bolCancelada {get;set;}
    @AuraEnabled public String strMotivoCancelacion {get;set;}
    @AuraEnabled public String strStyleCancelacion {get;set;}
    @AuraEnabled public Boolean bolCancelComent {get;set;}
    @AuraEnabled public Boolean blnFlotillaPrograma {get;set;}
    @AuraEnabled public String strTipoSolicitud {get;set;}
    @AuraEnabled public Boolean blnConfirmaFactura {get;set;}
    @AuraEnabled public String strEstatusVin {get;set;}

    @AuraEnabled public String strNomFilGpoCorp {get;set;}
    @AuraEnabled public TAM_DODSolicitudesPedidoEspecial__c regDodSolPedEspecial {get;set;}

	//un constructor por default
	public TAM_SolicitFlotProgExcepcionVinesCmWrp(){
	    this.strDistribuidor = '';
	    this.strVin = '';
	    this.strFolioExcepcion = '';
	    this.bolSeleccionar = false;
	    this.bolSeleccionado = false;
	    this.strFechaRecibo = '';
	    this.strIdRegistroDOD = '';
	    this.bolVinBloqueado = false;
	    this.bolSelBloqueado = false;
	    
	    this.bolConfirmaEntrega = false;
	    this.bolConfirmado = false;
	    this.dtFechaEntrega = null;
	    this.strTipoPago = '';

        this.strEstatusVin = '';
        this.strNomFilGpoCorp = '';
	    this.regDodSolPedEspecial = new TAM_DODSolicitudesPedidoEspecial__c();
	}

	//un constructor con parametros
	public TAM_SolicitFlotProgExcepcionVinesCmWrp(String strDistribuidor, String strVin, String strFolioExcepcion, 
		Boolean bolSeleccionar, Boolean bolSeleccionado){
	    this.strDistribuidor = strDistribuidor;
	    this.strVin = strVin;
	    this.strFolioExcepcion = strFolioExcepcion;
	    this.bolSeleccionar = bolSeleccionar;
	    this.bolSeleccionado = bolSeleccionado;
	}
    
}