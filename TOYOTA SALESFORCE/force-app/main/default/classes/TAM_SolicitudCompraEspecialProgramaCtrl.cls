/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar las solicitudes de tipo programa en Venta Especial.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    02-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_SolicitudCompraEspecialProgramaCtrl {

	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();

	static String sRectorTypePasoSolPrograma = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Programa').getRecordTypeId();
	static String sRectorTypePasoSolProgramaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Programa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativaCerrada = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Cerrada Venta Corporativa').getRecordTypeId();
		    
    //Un controlados por default
	public TAM_SolicitudCompraEspecialProgramaCtrl(){}

    @AuraEnabled
    public static List<TAM_OrdenDeCompraWrapperClass> getModelos(String recordId, String solicitudRTId) {
		System.debug('ENTRO A TAM_SolicitudCompraEspecialProgramaCtrl.getModelos recordId: ' + recordId + ' solicitudRTId: ' + solicitudRTId);

		String sEstatus = getEstatusSol(recordId);
		Boolean bSolCerrada = sEstatus == 'Cerrada' ?  true : false; 
		
		//Consulta los registro de los distrivudidores asociados a esta solicitud
		Map<String, Integer> mapIdExternoDist = getSolDistrib(recordId, solicitudRTId);
		System.debug('EN getDistribuidores getModelos mapIdExternoDist: ' + mapIdExternoDist.keySet());
		System.debug('EN getDistribuidores getModelos mapIdExternoDist: ' + mapIdExternoDist.Values());
		
		Map<String, Boolean> mapIdExternoEntregaDist = getEntregaDistribuidora(recordId, sRectorTypePasoPedidoEspecial);
		System.debug('EN getDistribuidores getModelos mapIdExternoEntregaDist: ' + mapIdExternoEntregaDist.keySet());
		System.debug('EN getDistribuidores getModelos mapIdExternoEntregaDist: ' + mapIdExternoEntregaDist.Values());		
		
		Set<String> setIdExterno = new Set<String>();
		List<CatalogoCentralizadoModelos__c> lstModelos = new List<CatalogoCentralizadoModelos__c>();
		
		for(CatalogoCentralizadoModelos__c objModelo : [  SELECT  Id,
                                                          Name,
                                                          Marca__c,
                                                          Serie__c,
                                                          Modelo__c,
                                                          AnioModelo__c,
                                                          CodigoColorExterior__c,
                                                          CodigoColorInterior__c,
                                                          DescripcionColorExterior__c,
                                                          DescripcionColorInterior__c,
                                                          Version__c
                                                          FROM CatalogoCentralizadoModelos__c 
                                                          WHERE Disponible__c = 'SI'
                                                          ORDER BY Name]) {
				String sIdExterno = objModelo.Modelo__c + '' + objModelo.AnioModelo__c;
				setIdExterno.add(sIdExterno);                                                            	
				//Agregao a la lista de lstModelos
				lstModelos.add(objModelo);
		}
		
		if (bSolCerrada){
			lstModelos.clear();
			for(CatalogoCentralizadoModelos__c objModelo : [  SELECT Id, Name, Marca__c,Serie__c,
                                                          Modelo__c,AnioModelo__c,CodigoColorExterior__c,
                                                          CodigoColorInterior__c, DescripcionColorExterior__c,
                                                          DescripcionColorInterior__c,Version__c
                                                          FROM CatalogoCentralizadoModelos__c ORDER BY Name]) {
				String sIdExterno = objModelo.Modelo__c + '' + objModelo.AnioModelo__c;
				setIdExterno.add(sIdExterno);                                                            	
				//Agregao a la lista de lstModelos
				lstModelos.add(objModelo);
			}
		}//Fin si bSolCerrada
		                                                            
		System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.lstModelos: ' + lstModelos);        
       	
       	Map<String, String> mapIdExtProdSerie = new Map<String, String>();
		//Cinsulta el catalogo de prodictos para que te traigas las series reales
		for (Product2 producto :   [Select p.Serie__r.Id_Externo_Serie__c, p.Serie__c, p.IdExternoProducto__c From Product2 p
				where IdExternoProducto__c IN : setIdExterno]){
			//System.debug('EN getDistribuidores getSeries producto.IdExternoProducto__c: ' + producto.IdExternoProducto__c);		
			//Agregalo al mapa de mapIdExtProdSerie
			mapIdExtProdSerie.put(producto.IdExternoProducto__c, producto.Serie__r.Id_Externo_Serie__c);
		}
		System.debug('EN getDistribuidores getSeries mapIdExtProdSerie: ' + mapIdExtProdSerie.keySet());
		System.debug('EN getDistribuidores getSeries mapIdExtProdSerie: ' + mapIdExtProdSerie.Values());
		
       	
        Map<String,Double> mapPriceBookEntry = ObtenerPrecios();
        System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.mapPriceBookEntry: ' + mapPriceBookEntry.KeySet());      
        
        List<TAM_OrdenDeCompraWrapperClass> lstOrdenCompraWrapper = new List<TAM_OrdenDeCompraWrapperClass>();
        for(CatalogoCentralizadoModelos__c objCatalogoModelos : lstModelos){
			
			//Crea el id extenrno 
			String strCodigoModelo = objCatalogoModelos.Modelo__c + objCatalogoModelos.AnioModelo__c;
			//System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.strCodigoModelo1: ' + strCodigoModelo);  
			
			//Crea la llave externa del reg para buscarlo en el mapa de mapIdExternoDist y obtener el total
			//String strIdExternoDistPaso = recordId + '-' + objCatalogoModelos.AnioModelo__c + '-' +  objCatalogoModelos.Serie__c + '-' + objCatalogoModelos.Modelo__c + '-' + objCatalogoModelos.Version__c + '-' + objCatalogoModelos.CodigoColorExterior__c + '-' + objCatalogoModelos.CodigoColorInterior__c ;
			//String strIdExternoDistPaso = recordId + '-' + objCatalogoModelos.AnioModelo__c + '-' +  objCatalogoModelos.Serie__c + '-' + objCatalogoModelos.Modelo__c + '-' + objCatalogoModelos.CodigoColorExterior__c + '-' + objCatalogoModelos.CodigoColorInterior__c ;
			String strIdExternoDistPaso = recordId + '-' + objCatalogoModelos.AnioModelo__c + '-' +  mapIdExtProdSerie.get(strCodigoModelo) + '-' + objCatalogoModelos.Modelo__c + '-' + objCatalogoModelos.CodigoColorExterior__c + '-' + objCatalogoModelos.CodigoColorInterior__c ;			
			System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.getModelos strIdExternoDistPaso: ' + strIdExternoDistPaso);      
			System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.getModelos CANTIDAD: ' + ( mapIdExternoDist.containskey(strIdExternoDistPaso) ? String.valueOf(mapIdExternoDist.get(strIdExternoDistPaso)) : null )); 
			 
			//Crea el oibjeto del tipo TAM_OrdenDeCompraWrapperClass
            TAM_OrdenDeCompraWrapperClass objPasoTAM_OrdenDeCompraWrapperClass = new TAM_OrdenDeCompraWrapperClass(    
            					objCatalogoModelos.Marca__c,
                                mapIdExtProdSerie.get(strCodigoModelo), //objCatalogoModelos.Serie__c,
                                objCatalogoModelos.Modelo__c,
                                objCatalogoModelos.AnioModelo__c,
                                objCatalogoModelos.Version__c,
                                objCatalogoModelos.CodigoColorExterior__c,
                                objCatalogoModelos.CodigoColorInterior__c,
                                objCatalogoModelos.DescripcionColorExterior__c,
                                objCatalogoModelos.DescripcionColorInterior__c,
                                mapIdExternoDist.containskey(strIdExternoDistPaso) ? String.valueOf(mapIdExternoDist.get(strIdExternoDistPaso)) : null,
                                null,
                                String.valueOf(mapPriceBookEntry.get(strCodigoModelo)),
                                null,
                                objCatalogoModelos.Name,
                                mapIdExtProdSerie.get(strCodigoModelo) + '-' + objCatalogoModelos.Version__c + '-' + objCatalogoModelos.Modelo__c + '-' + objCatalogoModelos.AnioModelo__c,
                                objCatalogoModelos.CodigoColorExterior__c + '-' + objCatalogoModelos.DescripcionColorExterior__c,
                                objCatalogoModelos.CodigoColorInterior__c + '-' + objCatalogoModelos.DescripcionColorInterior__c,
                                mapIdExternoEntregaDist.containskey(strIdExternoDistPaso) ? Boolean.valueOf(mapIdExternoEntregaDist.get(strIdExternoDistPaso)) : false   //false
            );
            objPasoTAM_OrdenDeCompraWrapperClass.strIdCatModelos = objCatalogoModelos.id;
            objPasoTAM_OrdenDeCompraWrapperClass.strIdExterno = strIdExternoDistPaso;
			
			System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.bSolCerrada: ' + bSolCerrada);
			Integer intCantidad = objPasoTAM_OrdenDeCompraWrapperClass.strCantidad != null ? Integer.valueOf(objPasoTAM_OrdenDeCompraWrapperClass.strCantidad) : 0;  			
			if (bSolCerrada && intCantidad > 0 ){
	        	System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl objPasoTAM_OrdenDeCompraWrapperClass: ' + objPasoTAM_OrdenDeCompraWrapperClass.strIdExterno + ' strCantidad: ' +  objPasoTAM_OrdenDeCompraWrapperClass.strCantidad);                                                               
            	lstOrdenCompraWrapper.add(objPasoTAM_OrdenDeCompraWrapperClass);
			}//fin si bSolCerrada && intCantidad > 0 
			if (!bSolCerrada)
            	lstOrdenCompraWrapper.add(objPasoTAM_OrdenDeCompraWrapperClass);
        }
        
        System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.lstOrdenCompraWrapper.size(): ' + lstOrdenCompraWrapper.size());   
        return lstOrdenCompraWrapper;
    }

    //Obtener distribuidores.
    @AuraEnabled
    public static List<Account> getDistribuidores() {
        List<Account> lstDistribuidores = [ SELECT Id, Name, Codigo_Distribuidor__c 
                                            FROM Account 
                                            WHERE RecordType.DeveloperName = 'Dealer'
                                            ORDER BY Name ASC ];

        return lstDistribuidores;
    }

    //Acciones sobre el obj Distribuidores Orden de compra
    //@AuraEnabled
    public static List<TAM_DetalleOrdenCompra__c> getDistribuidoresOrdenCompra() {
        List<TAM_DetalleOrdenCompra__c> lstDistOrdenCompra = [   SELECT id, Name, TAM_Cantidad__c
                                                                        FROM TAM_DetalleOrdenCompra__c];
        return lstDistOrdenCompra;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static Map<String, List<String>> getSeries(String recordId) {
		System.debug('ENTRO A TAM_SolicitudCompraEspecialProgramaCtrl.getSeries...');
		System.debug('ENTRO A TAM_SolicitudCompraInventProgramComCtrl.getSeries recordId: ' + recordId);

		//Obtern el estaus de la solicitud
		String sEstatus = getEstatusSol(recordId);
		Boolean bSolCerrada = sEstatus == 'Cerrada' ?  true : false; 
		System.debug('ENTRO A TAM_SolicitudCompraEspecialProgramaCtrl.getSeries bSolCerrada: ' + bSolCerrada);
		Map<String, String> mapIdExtProdSerie = new Map<String, String>();
		
		Map<String, Set<String>> mapIdExtSerColExtColInt = new Map<String, Set<String>>();
		//Consulta lops vines asociados a esta solicitud
		for (TAM_DetalleOrdenCompra__c objVinesFlotProg : [Select t.TAM_IdExterno__c 
				From TAM_DetalleOrdenCompra__c t 
				Where TAM_SolicitudFlotillaPrograma__c = :recordId 
				//t.TAM_SolicitudCompraPROGRAMA__c = :recordId 
				And t.RecordTypeId =: sRectorTypePasoPedidoEspecial
				]){
			System.debug('EN TAM_SolicitudCompraEspecialProgramaCtrl TAM_IdExterno__c: ' + objVinesFlotProg.TAM_IdExterno__c);			
			//Toma el campo de TAM_IdExterno__c y destripalo
			String[] sTAMIdExterno = objVinesFlotProg.TAM_IdExterno__c.split('-');
			System.debug('EN TAM_SolicitudCompraEspecialProgramaCtrl sTAMIdExterno.size(): ' + sTAMIdExterno.size());			

			String sTAMIdExternoPaso = '';
			String sSerie = '';
			//Toma desde el numero 1 y 3			
			if (sTAMIdExterno.size() == 8){
				sTAMIdExternoPaso = sTAMIdExterno[4] + '' + sTAMIdExterno[1];
				sSerie = sTAMIdExterno[2] + '-' + sTAMIdExterno[3];
			}//Fin si sTAMIdExterno.size() > 7
			if (sTAMIdExterno.size() == 7){
				sTAMIdExternoPaso = sTAMIdExterno[3] + '' + sTAMIdExterno[1];
				sSerie = sTAMIdExterno[2];			
			}//Fin si sTAMIdExterno.size() <= 7
			System.debug('EN TAM_SolicitudCompraEspecialProgramaCtrl sTAMIdExternoPaso: ' + sTAMIdExternoPaso + ' sSerie: ' + sSerie);
			
             //Ve si no exoste en el mapa de mapIdExtTotal entonces agregalo
             if (mapIdExtSerColExtColInt.containsKey(sTAMIdExternoPaso))
             	mapIdExtSerColExtColInt.get(sTAMIdExternoPaso).add(sSerie);
             if (!mapIdExtSerColExtColInt.containsKey(sTAMIdExternoPaso))
             	mapIdExtSerColExtColInt.put(sTAMIdExternoPaso, new Set<String>{sSerie});
		}
		System.debug('EN TAM_SolicitudCompraEspecialProgramaCtrl getSeries mapIdExtSerColExtColInt: ' + mapIdExtSerColExtColInt.keySet());
		System.debug('EN TAM_SolicitudCompraEspecialProgramaCtrl getSeries mapIdExtSerColExtColInt: ' + mapIdExtSerColExtColInt.Values());		

        /* Serie__c AnioModelo__c */
        List<String> lstAnioModelo;
        Map<String, Set<String>> mapModelos = new Map<String, Set<String>>();
        Map<String, List<String>> mapModelosFinal = new Map<String, List<String>>();
		Set<String> setIdExterno = new Set<String>();
		
        //Recorre la lista de CatalogoCentralizadoModelos__c
        List<CatalogoCentralizadoModelos__c> lstSeries = new List<CatalogoCentralizadoModelos__c>();
        //No esta cerrada
	    for (CatalogoCentralizadoModelos__c objSeries : [SELECT Serie__c, AnioModelo__c, Modelo__c 
	        		FROM CatalogoCentralizadoModelos__c Where Disponible__c = 'SI' ORDER BY Name]){
	        	String sIdExt = objSeries.Modelo__c + '' + 	objSeries.AnioModelo__c;
	        	//System.debug('EN getDistribuidores getSeries NO ESTA CERRADA sIdExt: ' + sIdExt);	
	        	setIdExterno.add(sIdExt);		
	        	lstSeries.add(objSeries);
	    }        
		//Ya esta cerrada
        if (bSolCerrada){
        	setIdExterno.clear();
        	lstSeries.clear();
	        for (CatalogoCentralizadoModelos__c objSeries : [SELECT Serie__c, AnioModelo__c, Modelo__c 
	        		FROM CatalogoCentralizadoModelos__c ORDER BY Name]){
	        	String sIdExt = objSeries.Modelo__c + '' + 	objSeries.AnioModelo__c;
	        	System.debug('EN getDistribuidores getSeries YA ESTA CERRADA sIdExt: ' + sIdExt + ' Serie: ' + objSeries.Serie__c);	
	        	setIdExterno.add(sIdExt);		
	        	lstSeries.add(objSeries);
	        }
        }//!bSolCerrada
		System.debug('EN getDistribuidores getSeries lstSeries2: ' + lstSeries);
				
		//Cinsulta el catalogo de prodictos para que te traigas las series reales
		for (Product2 producto :   [Select p.Serie__r.Id_Externo_Serie__c, p.Serie__c, p.IdExternoProducto__c From Product2 p
				where IdExternoProducto__c IN : setIdExterno]){
			System.debug('EN getDistribuidores getSeries producto.IdExternoProducto__c: ' + producto.IdExternoProducto__c + ' Serie: ' + producto.Serie__r.Id_Externo_Serie__c);		
			//Agregalo al mapa de mapIdExtProdSerie
			mapIdExtProdSerie.put(producto.IdExternoProducto__c, producto.Serie__r.Id_Externo_Serie__c);
		}
		//System.debug('EN getDistribuidores getSeries mapIdExtProdSerie: ' + mapIdExtProdSerie.keySet());
		//System.debug('EN getDistribuidores getSeries mapIdExtProdSerie: ' + mapIdExtProdSerie.Values());		
				
        //Recorre la lista de lstSeries
        for(CatalogoCentralizadoModelos__c objCatalogoModelo : lstSeries){
			String strCodigoModelo = objCatalogoModelo.Modelo__c + objCatalogoModelo.AnioModelo__c;
			
			//Ve si el strCodigoModelo existe en mapIdExtTotal
			if (!bSolCerrada){
				String sSerie = mapIdExtProdSerie.get(strCodigoModelo);
				//Ya existe en el mapa de mapModelos
				if(mapModelos.containsKey(objCatalogoModelo.AnioModelo__c))
					mapModelos.get(objCatalogoModelo.AnioModelo__c).add(sSerie); //objCatalogoModelo.Serie__c
				if(!mapModelos.containsKey(objCatalogoModelo.AnioModelo__c))
					mapModelos.put(objCatalogoModelo.AnioModelo__c, new Set<String>{sSerie}); //objCatalogoModelo.Serie__c
			}//Fin si !bSolCerrada*/
			if (bSolCerrada){
				//ve si existe en el mapa de 
				if (mapIdExtSerColExtColInt.containsKey(strCodigoModelo)){
					String sSerie = mapIdExtProdSerie.get(strCodigoModelo);
					System.debug('EN getDistribuidores getSeries sSerie: ' + sSerie);
					if (mapIdExtSerColExtColInt.get(strCodigoModelo).contains(sSerie)){ //objCatalogoModelo.Serie__c
						//Ya existe en el mapa de mapModelos
						if(mapModelos.containsKey(objCatalogoModelo.AnioModelo__c))
							mapModelos.get(objCatalogoModelo.AnioModelo__c).add(sSerie); //objCatalogoModelo.Serie__c
						if(!mapModelos.containsKey(objCatalogoModelo.AnioModelo__c))
							mapModelos.put(objCatalogoModelo.AnioModelo__c, new Set<String>{sSerie}); //objCatalogoModelo.Serie__c
					}//Fin si mapIdExtSerColExtColInt.get(strCodigoModelo).containsKey(objCatalogoModelo.Serie__c)
				}//fin si mapIdExtSerColExtColInt.containsKey(strCodigoModelo)
			}//Fin si !bSolCerrada				
			
        }//Fin del for para  CatalogoCentralizadoModelos__c
        
        //Recorre el mapa de mapModelos y crea el mapa final de mapModelosFinal
        for (String sModYear : mapModelos.keySet()){
        	List<String> lPasoMod = new List<String>(); 
        	lPasoMod.addAll(mapModelos.get(sModYear));
        	System.debug('EN getDistribuidores getSeries objCatalogoModelo sModYear: ' + sModYear);
        	System.debug('EN getDistribuidores getSeries objCatalogoModelo lPasoMod: ' + lPasoMod);
        	mapModelosFinal.put(sModYear, lPasoMod);
        }
		               
        System.debug('EN A TAM_SolicitudCompraEspecialProgramaCtrl.getSeries mapModelosFinal: ' + mapModelosFinal);
        return mapModelosFinal;
    }

    public static Map<String,Double>  ObtenerPrecios(){

         //Obtiene mes y año para buscar la lista de precios correspondiente.
        Date fechaActual = System.today();
        String strAnio = String.valueOf(fechaActual.year());
        Integer intMesActual = fechaActual.month();
        Map<Integer,String> mapMes = new Map<Integer,String>();
        mapMes.put(1, 'ENERO');
        mapMes.put(2, 'FEBRERO');
        mapMes.put(3, 'MARZO');
        mapMes.put(4, 'ABRIL');
        mapMes.put(5, 'MAYO');
        mapMes.put(6, 'JUNIO');
        mapMes.put(7, 'JULIO');
        mapMes.put(8, 'AGOSTO');
        mapMes.put(9, 'SEPTIEMBRE');
        mapMes.put(10, 'OCTUBRE');
        mapMes.put(11, 'NOVIEMBRE');
        mapMes.put(12, 'DICIEMBRE');

        String strMes = mapMes.get(intMesActual);
        String strNombreCatalogoPrecios = 'VH-' + strMes + '-' + strAnio;

        System.debug('**** CODIGO PRECIO******* ' + strNombreCatalogoPrecios);

        //Mapa de catálogo de lista de precios.
        List<PricebookEntry> lstCatalogoPrecios = [ SELECT  Id,
                                                            ProductCode,
                                                            PrecioSinIva__c
                                                    FROM    PricebookEntry
                                                    WHERE   IsActive = true AND Pricebook2.Name =:strNombreCatalogoPrecios
        ];
        System.debug('**** TAMAÑO LISTA PRICEBOOK ******* ' + lstCatalogoPrecios.size());

        String strCodigoProducto;
        Double dblPrecioSinIVA;
        Map<String,Double> mapPriceBookEntry = new Map<String,Double>();
        for(PricebookEntry objListaPrecio : lstCatalogoPrecios){
            strCodigoProducto = objListaPrecio.ProductCode;
            dblPrecioSinIVA = objListaPrecio.PrecioSinIva__c;
            mapPriceBookEntry.put(strCodigoProducto, dblPrecioSinIVA);
        }
        return mapPriceBookEntry;
    }
    
     //Obtener un Set de las series disponibles
    @AuraEnabled
    public static void SaveOrdenCompraEspecial()
    {
        
    }

    //Obtener distribuidores.
    @AuraEnabled
    public static string getTipRegPrograma(String strTipRegNombre) {
    	System.debug('EN getTipRegPrograma strTipRegNombre: ' + strTipRegNombre);
    	
    	String sRectorTypePasoPrograma = '';
    	if (strTipRegNombre != null && strTipRegNombre != '')
    		sRectorTypePasoPrograma = Schema.SObjectType.TAM_DistribuidoresFlotillaPrograma__c.getRecordTypeInfosByName().get(strTipRegNombre).getRecordTypeId();
    	
		System.debug('EN getDistribuidores sRectorTypePasoPrograma: ' + sRectorTypePasoPrograma);
        return sRectorTypePasoPrograma;
    }   


    //Obtener distribuidores.
    @AuraEnabled
    public static Map<String, Integer> getSolDistrib(String recordId, String solicitudRTId) {
    	System.debug('EN getSolDistrib recordId: ' + recordId + ' solicitudRTId: ' + solicitudRTId);
    	String sRectorTypePasoPrograma = '';
    	Map<String, Integer> mapIdExtrCantidad = new Map<String, Integer>();
    	String sGetTipoRegSol = getTipoRegSol(recordId);
    	
    	//Ve al objeto de TAM_DistribuidoresFlotillaPrograma__c en base al recordId de la solicitud
		//Ya tienes la llave consulta los datos en TAM_DistribuidoresFlotillaPrograma__c
		for (TAM_DistribuidoresFlotillaPrograma__c objDistrib : [Select ID, TAM_Cantidad__c, TAM_IdExterno__c,
			RecordType.Name, RecordTypeId
			From TAM_DistribuidoresFlotillaPrograma__c	
			Where TAM_SolicitudFlotillaPrograma__c = :recordId  
			//TAM_SolicitudCompraPROGRAMA__c =:recordId 
			And TAM_IdExterno__c != null]){
				//Toma la el ID externo y cortalo hasta el ultimo  -
				String sIdExternoPaso = objDistrib.TAM_IdExterno__c;
				System.debug('EN getSolDistrib sIdExternoPaso0: ' + sIdExternoPaso + ' objDistrib.TAM_Cantidad__c: ' +  objDistrib.TAM_Cantidad__c + ' RecordType.Name: ' + objDistrib.RecordType.Name + ' RecordTypeId: ' + objDistrib.RecordTypeId + ' objDistrib.id: ' + objDistrib.id);
				sIdExternoPaso = sIdExternoPaso.substring(0, sIdExternoPaso.lastIndexOf('-'));
				//System.debug('EN getSolDistrib sIdExternoPaso: ' + sIdExternoPaso);
				String sIdExternoPaso2 = sIdExternoPaso.substring(0, sIdExternoPaso.lastIndexOf('-'));
				//System.debug('EN getSolDistrib sIdExternoPaso2: ' + sIdExternoPaso2);
				//Ahora si agregalo al mapa de mapIdExtrCantidad
				if (mapIdExtrCantidad.containsKey(sIdExternoPaso2)){
					Integer intCanti = mapIdExtrCantidad.get(sIdExternoPaso2) + Integer.valueOf(objDistrib.TAM_Cantidad__c);
					mapIdExtrCantidad.put(sIdExternoPaso2, intCanti);
				}//Fin si mapIdExtrCantidad.containsKey(sIdExternoPaso2)
				//No existe la clave 
				if (!mapIdExtrCantidad.containsKey(sIdExternoPaso2))
					mapIdExtrCantidad.put(sIdExternoPaso2, Integer.valueOf(objDistrib.TAM_Cantidad__c));
		}//fin del for para TAM_DistribuidoresFlotillaPrograma__c
    	
        //Ve al objeto de TAM_DistribuidoresFlotillaPrograma__c en base al recordId de la solicitud
        //Ya tienes la llave consulta los datos en TAM_DistribuidoresFlotillaPrograma__c
        for (TAM_DetalleOrdenCompra__c objDistrib : [Select t.TAM_IdExterno__c, t.TAM_EntregarEnMiDistribuidora__c, t.TAM_Cantidad__c,
        	RecordType.Name, RecordTypeId
        	From TAM_DetalleOrdenCompra__c t 
        	Where TAM_SolicitudFlotillaPrograma__c = :recordId  
        	//And RecordTypeId = :sGetTipoRegSol
        	//t.TAM_SolicitudCompraPROGRAMA__c =:recordId 
        	AND TAM_IdExterno__c != null]){                                      
        	//Toma la el ID externo y cortalo hasta el ultimo  -
            String sIdExternoPaso = objDistrib.TAM_IdExterno__c;
            //if (sIdExternoPaso.contains(sGetTipoRegSol)){
	            System.debug('EN getDistribuidores sIdExternoPaso1.0: ' + sIdExternoPaso);
	            //System.debug('EN getSolDistrib sIdExternoPaso: ' + sIdExternoPaso);
	            sIdExternoPaso = sIdExternoPaso.substring(0, sIdExternoPaso.lastIndexOf('-'));
	            System.debug('EN getSolDistrib sIdExternoPaso1.1: ' + sIdExternoPaso + ' objDistrib.TAM_Cantidad__c: ' +  objDistrib.TAM_Cantidad__c + ' objDistrib.id: ' + objDistrib.id + ' RecordType.Name: ' + objDistrib.RecordType.Name + ' RecordTypeId: ' + objDistrib.RecordTypeId);
	            //No existe la clave 
				if (Integer.valueOf(objDistrib.TAM_Cantidad__c) > 0)            
	            	if (!mapIdExtrCantidad.containsKey(sIdExternoPaso))
	            		mapIdExtrCantidad.put(sIdExternoPaso, Integer.valueOf(objDistrib.TAM_Cantidad__c));
            //}
        }//fin del for para TAM_DistribuidoresFlotillaPrograma__c
        System.debug('EN getDistribuidores mapIdExtrCantidad: ' + mapIdExtrCantidad.keySet());
        System.debug('EN getDistribuidores mapIdExtrCantidad: ' + mapIdExtrCantidad.Values());
		
		//Regresa el mapa con oos datos
        return mapIdExtrCantidad;
    }   

    //Obtener distribuidores.
    @AuraEnabled
    public static Map<String, Boolean> getEntregaDistribuidora(String recordId, String sRectTyoeId) {
    	System.debug('EN getEntregaDistribuidora recordId: ' + recordId + ' sRectTyoeId: ' + sRectTyoeId);
    	String sRectorTypePasoPrograma = '';
    	Map<String, Boolean> mapIdExtrEntregaDistrib = new Map<String, Boolean>();
    	
    	//Ve al objeto de TAM_DistribuidoresFlotillaPrograma__c en base al recordId de la solicitud
		//Ya tienes la llave consulta los datos en TAM_DistribuidoresFlotillaPrograma__c
		for (TAM_DetalleOrdenCompra__c objTAMDetalleOrdenCompra : [Select TAM_IdExterno__c, TAM_EntregarEnMiDistribuidora__c 
			From TAM_DetalleOrdenCompra__c 
			Where TAM_SolicitudFlotillaPrograma__c = :recordId 
			//TAM_SolicitudCompraPROGRAMA__c = :recordId 
			And RecordTypeId =: sRectTyoeId ]){
				//Toma la el ID externo y cortalo hasta el ultimo  -
				String sIdExternoPaso = objTAMDetalleOrdenCompra.TAM_IdExterno__c;
				System.debug('EN getEntregaDistribuidora sIdExternoPaso: ' + sIdExternoPaso);
				sIdExternoPaso = sIdExternoPaso.substring(0, sIdExternoPaso.lastIndexOf('-'));
				//System.debug('EN getSolDistrib sIdExternoPaso: ' + sIdExternoPaso);
				//No existe la clave 
				if (!mapIdExtrEntregaDistrib.containsKey(sIdExternoPaso))
					mapIdExtrEntregaDistrib.put(sIdExternoPaso, Boolean.valueOf(objTAMDetalleOrdenCompra.TAM_EntregarEnMiDistribuidora__c));			
		}    	
		System.debug('EN getDistribuidores mapIdExtrEntregaDistrib: ' + mapIdExtrEntregaDistrib.keySet());
		System.debug('EN getDistribuidores mapIdExtrEntregaDistrib: ' + mapIdExtrEntregaDistrib.Values());
		
		//Regresa el mapa con oos datos
        return mapIdExtrEntregaDistrib;
    }   

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion eliminaDatosModelos(String recordId, String sstrIdCatModelos, String sstrIdExterno) {    	
    	System.debug('EN eliminaDatosModelos recordId: ' + recordId );
    	System.debug('EN eliminaDatosModelos sstrIdCatModelos: ' + sstrIdCatModelos);
    	System.debug('EN eliminaDatosModelos sstrIdExterno: ' + sstrIdExterno);
    	String sMensaejeSalidaUpd = '';  
    	
    	String sTAMIdExternoPaso = sstrIdExterno + '%';
        System.debug('EN eliminaDatosModelos sTAMIdExternoPaso: ' + sTAMIdExternoPaso);

		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
    	Set<String> setIdDetOrdCom = new Set<String>();
    	List<TAM_DetalleOrdenCompra__c> lTAMDetalleOrdenCompraDel = new List<TAM_DetalleOrdenCompra__c>();
    	List<TAM_DistribuidoresFlotillaPrograma__c> lTAMDistribuidoresFlotillaProgramaDel = new List<TAM_DistribuidoresFlotillaPrograma__c>();
    	
    	//Busca los regitros en el objeto de TAM_DetalleOrdenCompra__c
    	for (TAM_DetalleOrdenCompra__c objDetOrd : [Select Id, TAM_IdExterno__c From TAM_DetalleOrdenCompra__c
    		Where TAM_SolicitudFlotillaPrograma__c = :recordId And TAM_CatalogoCentralizadoModelos__c = :sstrIdCatModelos
    		And RecordTypeid =: sRectorTypePasoPedidoEspecial
    		And TAM_IdExterno__c LIKE :sTAMIdExternoPaso]){
    		setIdDetOrdCom.add(objDetOrd.id);
    		lTAMDetalleOrdenCompraDel.add(objDetOrd);
	    	System.debug('EN eliminaDatosModelos objDetOrd: ' + objDetOrd);
    	}
    	
    	//Consulta los datos en el objeto de TAM_DistribuidoresFlotillaPrograma__c
    	for (TAM_DistribuidoresFlotillaPrograma__c objDistFlotPro : [Select Id, TAM_IdExterno__c 
    		From TAM_DistribuidoresFlotillaPrograma__c Where TAM_SolicitudFlotillaPrograma__c =:recordId 
    		//And TAM_DetalleSolicitudCompra_FLOTILLA__c IN :setIdDetOrdCom 
    		And TAM_IdExterno__c LIKE :sTAMIdExternoPaso]){
    		lTAMDistribuidoresFlotillaProgramaDel.add(objDistFlotPro);	
	    	System.debug('EN eliminaDatosModelos objDistFlotPro: ' + objDistFlotPro);    		
    	}
		   
		//Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
    	
    	//Elimina los reg de lTAMDistribuidoresFlotillaProgramaDel
    	if (!lTAMDistribuidoresFlotillaProgramaDel.isEmpty())
    		delete lTAMDistribuidoresFlotillaProgramaDel;
    	//Elimina los reg de lTAMDetalleOrdenCompraDel
    	if (!lTAMDetalleOrdenCompraDel.isEmpty())
    		delete lTAMDetalleOrdenCompraDel;

		//*** NO SE TE OLVIDE QUITAR ESTE COMENTARIO ES PARA PRUREBAS ****///
        //Database.rollback(sp);		
		
		//Crea el menaje de regreso
		objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos del Modelo seleccionado fueron eliminados del pedido.');
		
        //Regresa la cadena
        return objRespoPaso;
    }

    //Obtener un Set de las series disponibles
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion getDistDest(String recordId, TAM_OrdenDeCompraWrapperClass objOrdCompWrp) {    	
    	System.debug('EN getDistDest recordId: ' + recordId );
    	System.debug('EN getDistDest objOrdCompWrp: ' + objOrdCompWrp);
    	String sMensaejeSalidaUpd = '';  
    	
    	String sTAMIdExternoPaso = objOrdCompWrp.strIdExterno + '%';

		TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
    	Set<String> setIdDetOrdCom = new Set<String>();
    	List<TAM_DetalleOrdenCompra__c> lTAMDetalleOrdenCompraDel = new List<TAM_DetalleOrdenCompra__c>();
    	List<TAM_DistribuidoresFlotillaPrograma__c> lTAMDistribuidoresFlotillaProgramaDel = new List<TAM_DistribuidoresFlotillaPrograma__c>();
    	    	
    	//Consulta los datos en el objeto de TAM_DistribuidoresFlotillaPrograma__c
    	for (TAM_DistribuidoresFlotillaPrograma__c objDistFlotPro : [Select Id, TAM_IdExterno__c 
    		From TAM_DistribuidoresFlotillaPrograma__c Where TAM_SolicitudFlotillaPrograma__c =:recordId 
    		//And TAM_DetalleSolicitudCompra_FLOTILLA__c IN :setIdDetOrdCom 
    		And TAM_IdExterno__c LIKE :sTAMIdExternoPaso]){
    		lTAMDistribuidoresFlotillaProgramaDel.add(objDistFlotPro);	
	    	System.debug('EN getDistDest objDistFlotPro: ' + objDistFlotPro);
		    //Crea el menaje de regreso
		    objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'Ya asociaste disribuidres para la entrega, debes quitarlos si quieres recibir los autos en tu distribuidora.');                
        }
		
        //Regresa la cadena
        return objRespoPaso;
    }    
    
    //Obtener distribuidores.
    @AuraEnabled
    public static string getEstatusSol(String recordId) {
    	System.debug('EN getEstatusSol recordId: ' + recordId);
    	
    	String sEstatusSol = '';
        List<TAM_SolicitudesFlotillaPrograma__c> solicitud = [Select t.TAM_Estatus__c, t.Name, t.Id 
        		From TAM_SolicitudesFlotillaPrograma__c t
                WHERE id =: recordId];
        sEstatusSol = solicitud[0].TAM_Estatus__c;                      
    	
		System.debug('EN getEstatusSol sEstatusSol: ' + sEstatusSol);
        return sEstatusSol;
    }

    //Obtener distribuidores.
    @AuraEnabled
    public static string getTipoRegSol(String recordId) {
    	System.debug('EN getTipoRegSol recordId: ' + recordId);
    	
    	String sRecordTypeId = '';	
    	String sRecordTypeName = '';	
        List<TAM_SolicitudesFlotillaPrograma__c> solicitud = [Select t.RecordTypeId, RecordType.Name
        		From TAM_SolicitudesFlotillaPrograma__c t
                WHERE id =: recordId];           
        sRecordTypeId = solicitud[0].RecordTypeId;
        sRecordTypeName = solicitud[0].RecordType.Name;
    	
		System.debug('EN getTipoRegSol sRecordTypeId: ' + sRecordTypeId + ' sRecordTypeName: ' + sRecordTypeName) ;
        return sRecordTypeId;
    }


}