/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para reasignar los registros de la Opp a otra diferente

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    05-Octubre-2020    	Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_MergeShareOppSch implements Schedulable {

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_MergeShareOppSch.execute...');
		
		this.sQuery = 'Select Id, Tam_MergeCuentaFinal__c ';
		this.sQuery += ' From Opportunity Where TAM_Merge__c = false ';
		this.sQuery += ' And Tam_MergeCuentaFinal__c != null ';
		this.sQuery += ' Order by Tam_MergeCuentaFinal__c ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_MergeShareOppSch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_MergeShareOppBch objMergeShareOppBch = new TAM_MergeShareOppBch(sQuery);
        
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objMergeShareOppBch, 50);
			    	 
    }
    
}