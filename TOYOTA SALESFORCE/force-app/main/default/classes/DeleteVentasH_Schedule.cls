global class DeleteVentasH_Schedule implements Schedulable {
	global void execute(SchedulableContext sc) {
        String query = 'SELECT Id, Name, CreatedDate FROM Venta__c WHERE CreatedDate < TODAY';
        List<Venta__c> ventasHoy = this.getVentasDeHoy();
        
        if(ventasHoy.size() > 0){
            DeleteVentasH_Batch b = new DeleteVentasH_Batch(query);
            database.executebatch(b);
        }
	}


    public List<Venta__c> getVentasDeHoy(){
        return [SELECT Id, Name, CreatedDate FROM Venta__c WHERE CreatedDate = TODAY LIMIT 5];
    }
}