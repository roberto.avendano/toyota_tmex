global without sharing class TAM_ActNomDistSch_cls implements Schedulable{

    global String sQuery {get;set;}
        
    global void execute(SchedulableContext ctx){
        System.debug('EN TAM_ActPropProsSch_cls.execute...');
        String strIdLeadPrueba = 'JTDKDTB34M1141773';

        this.sQuery = 'Select Id, FWY_codigo_distribuidor__c ';
        this.sQuery += ' From Lead ';
        this.sQuery += ' Where FWY_Nombre_distribuidor__c = null';

        //this.sQuery += ' And Id = \'' + String.escapeSingleQuotes(strIdLeadPrueba) + '\'';
        //this.sQuery += ' Limit 5';

        //Si es una prueba
        if (Test.isRunningTest())
            this.sQuery += ' Limit 1';
        System.debug('EN TAM_ActPropProsSch_cls.execute sQuery: ' + sQuery);
        
        //Crea el objeto de  OppUpdEnvEmailBch_cls      
        TAM_ActNomDistBch_cls objActNomDistBch = new TAM_ActNomDistBch_cls(sQuery);
        
        //No es una prueba entonces procesa de 25 en 25
        if (!Test.isRunningTest())
            Id batchInstanceId = Database.executeBatch(objActNomDistBch, 25);
                     
    }
    
}