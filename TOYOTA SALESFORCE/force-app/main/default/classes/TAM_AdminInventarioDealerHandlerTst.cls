/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_AdminInventarioDealerHandlerTst {

    static testMethod void TAM_AdminInventarioDealerHandlerTstOK() {
        System.debug('EN TAM_AdminInventarioDealerHandlerTstOK...');
        
        Test.startTest();
            
            //El objeto para TAM_AdminInventarioDealer__c
            TAM_AdminInventarioDealer__c objAdminInvent = new TAM_AdminInventarioDealer__c(
                Name ='Prende el Proceso de TAM_ActCancelSolInvSch_cls',
                TAM_ActivaProceso__c = false
            );
            insert objAdminInvent;  
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInvent: ' + objAdminInvent);
                
            //Ya que creaste el reg ahora actuelizalo
            TAM_AdminInventarioDealer__c objAdminInventUpd = [Select id, TAM_ActivaProceso__c 
                From TAM_AdminInventarioDealer__c Where id =: objAdminInvent.id];
            objAdminInventUpd.TAM_ActivaProceso__c = false;
            update objAdminInventUpd;
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInventUpd1: ' + objAdminInventUpd);
            
            //Ya que creaste el reg ahora actuelizalo
            TAM_AdminInventarioDealer__c objAdminInventUpd2 = [Select id, TAM_ActivaProceso__c 
                From TAM_AdminInventarioDealer__c Where id =: objAdminInvent.id];
            objAdminInventUpd2.TAM_ActivaProceso__c = true;
            objAdminInventUpd2.TAM_Proceso__c = 'TAM_ActTotVtaInvSch_cls';
            update objAdminInventUpd2;
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInventUpd2: ' + objAdminInventUpd2);

            //Ya que creaste el reg ahora actualizalo
            TAM_AdminInventarioDealer__c objAdminInventUpd3 = [Select id, TAM_ActivaProceso__c 
                From TAM_AdminInventarioDealer__c Where id =: objAdminInvent.id];
            objAdminInventUpd3.TAM_ActivaProceso__c = true;
            objAdminInventUpd3.TAM_Proceso__c = 'TAM_ActCancelSolInvSch_cls';
            update objAdminInventUpd3;
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInventUpd3: ' + objAdminInventUpd3);
                        
            //Ya que creaste el reg ahora actualizalo
            TAM_AdminInventarioDealer__c objAdminInventUpd4 = [Select id, TAM_ActivaProceso__c 
                From TAM_AdminInventarioDealer__c Where id =: objAdminInvent.id];
            objAdminInventUpd4.TAM_ActivaProceso__c = true;
            objAdminInventUpd4.TAM_Proceso__c = 'TAM_ConsMovDDSolInvSch_cls';
            update objAdminInventUpd4;
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInventUpd4: ' + objAdminInventUpd4);

            //Ya que creaste el reg ahora actualizalo
            TAM_AdminInventarioDealer__c objAdminInventUpd5 = [Select id, TAM_ActivaProceso__c 
                From TAM_AdminInventarioDealer__c Where id =: objAdminInvent.id];
            objAdminInventUpd5.TAM_ActivaProceso__c = true;
            objAdminInventUpd5.TAM_Proceso__c = 'TAM_SirenaWebServiceSch';
            update objAdminInventUpd5;
            System.debug('EN TAM_AdminInventarioDealerHandlerTstOK objAdminInventUpd5: ' + objAdminInventUpd5);
            
        Test.stopTest();
    }
}