@isTest
private class SolicitudAutoDemoTriggerHandlerTest {

    @TestSetup static void loadUserRole(){
        List<String> validAdmins= new List<String>{'Administrador del sistemaAD', 'Administrador del sistema', 'System Administrator'};
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile admin = [Select Id From Profile Where Name IN:validAdmins LIMIT 1];
        
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = admin.Id,
            Username = 'ToyotaAdminPartner@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
    }
    
    @TestSetup static void loadWholesales(){
        List<InventarioWholesale__c> wholesale = new List<InventarioWholesale__c>();
        
        for(Integer i=0; i<50; i++){			
            String sufix = i<10? '0'+String.valueOf(i):String.valueOf(i);  
            
            wholesale.add(new InventarioWholesale__c(
                Name = 'JTDKBRFUXH30310'+sufix,
                Model_Year__c = '2017',            		
                Dealer_Code__c = '57055',
                Exterior_Color_Description__c ='',
                Exterior_Color_Code__c = String.valueOf(i),
                Interior_Color_Description__c = '',
                Interior_Color_Code__c = String.valueOf(i),
                Distributor_Invoice_Date_MM_DD_YYYY__c = '12/11/17 12:00 AM',
                Toms_Series_Name__c = 'COROLLA',
                Model_Number__c = String.valueOf(SolicitudAutoDemoTriggerHandlerTest.randomWithMax(10))
            )); 			 				 				 		      
        }
        
        insert wholesale;
    }

    public static Integer randomWithMax(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    public static SolicitudAutoDemo__c getAutoDemoTest(){
        return [SELECT Id, Name, Estatus__c, VIN_Name__c FROM SolicitudAutoDemo__c LIMIT 1];
    }

    
    @isTest static void test_method_Community(){
        //Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;        			
        
        List<Presupuesto__c> nuevosPresupuestos = new List<Presupuesto__c>();
        List<AsignacionPresupuesto__c> nuevasAsignaciones = new List<AsignacionPresupuesto__c>();

        User testAcc = [SELECT Id, Name, Alias FROM User WHERE Alias='batman' LIMIT 1];
        Account portalAccount1 = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '57055',			
            UnidadesAutosDemoAutorizadas__c = 5,
            ActivarvalidacionesPresupuestosAD__c = true,
            OwnerId = testAcc.Id
        );
        insert portalAccount1;

        //Periodos Fiscales
        PeriodoFiscal__c fy2018 = new PeriodoFiscal__c(
            FechaInicioPeriodoFiscal__c = Date.newInstance(2017, 4, 1),
            FechaFinPeriodoFiscal__c = Date.newInstance(2018, 3, 31),
            Name = '2018'
        );
        insert fy2018;

        PeriodoFiscal__c fy2019 = new PeriodoFiscal__c(
            FechaInicioPeriodoFiscal__c = Date.newInstance(2018, 4, 1),
            FechaFinPeriodoFiscal__c = Date.newInstance(2019, 3, 31),
            Name = '2019'
        );
        insert fy2019;


        Serie__c serie = new Serie__c(
            Id_Externo_Serie__c = 'COROLLA',
            Name = 'COROLLA'
        );
        upsert serie Id_Externo_Serie__c;

        // Costos de series.
        CostoSerie__c costoSerie1 = new CostoSerie__c(
            Activar__c = true,
            FY__c = fy2018.Id,
            Serie__c = serie.Id,

            Enero__c = 200,
            Febrero__c = 200,
            Marzo__c = 200,
            Abril__c = 200,
            Mayo__c = 200,
            Junio__c = 200,
            Julio__c = 200,
            Agosto__c = 200,
            Septiembre__c = 200,
            Octubre__c = 200,
            Noviembre__c = 200,
            Diciembre__c = 200      
        );
        insert costoSerie1;

        // Presupuestos
        Presupuesto__c presupuesto1 = new Presupuesto__c(
            PeriodoFiscal__c = fy2018.Id,
            PresupuestoAutorizadoFinanzas__c = true,
            Activo__c = true,

            PresupuestoAutorizadoEnero__c = 1000,
            PresupuestoAutorizadoFebrero__c = 1000,
            PresupuestoAutorizadoMarzo__c = 1000,
            PresupuestoAutorizadoAbril__c = 1000,
            PresupuestoAutorizadoMayo__c = 1000,
            PresupuestoAutorizadoJunio__c = 1000,
            PresupuestoAutorizadoJulio__c = 1000,
            PresupuestoAutorizadoAgosto__c = 1000,
            PresupuestoAutorizadoSeptiembre__c = 1000,
            PresupuestoAutorizadoOctubre__c = 1000,
            PresupuestoAutorizadoNoviembre__c = 1000,
            PresupuestoAutorizadoDiciembre__c = 1000
        );
        nuevosPresupuestos.add(presupuesto1);

        Presupuesto__c presupuesto2 = new Presupuesto__c(
            PeriodoFiscal__c = fy2019.Id,
            PresupuestoAutorizadoFinanzas__c = true,
            Activo__c = true,

            PresupuestoAutorizadoEnero__c = 1000,
            PresupuestoAutorizadoFebrero__c = 1000,
            PresupuestoAutorizadoMarzo__c = 1000,
            PresupuestoAutorizadoAbril__c = 1000,
            PresupuestoAutorizadoMayo__c = 1000,
            PresupuestoAutorizadoJunio__c = 1000,
            PresupuestoAutorizadoJulio__c = 1000,
            PresupuestoAutorizadoAgosto__c = 1000,
            PresupuestoAutorizadoSeptiembre__c = 1000,
            PresupuestoAutorizadoOctubre__c = 1000,
            PresupuestoAutorizadoNoviembre__c = 1000,
            PresupuestoAutorizadoDiciembre__c = 1000
        );
        nuevosPresupuestos.add(presupuesto2);

        insert nuevosPresupuestos;

        // Asignacion de presupuestos
        AsignacionPresupuesto__c asignacionPres1 = new AsignacionPresupuesto__c(
            Distribuidor__c = portalAccount1.Id,
            Presupuesto__c = presupuesto1.Id,

            AsignadoEnero__c = 800, 
            AsignadoFebrero__c = 800,
            AsignadoMarzo__c = 800,
            AsignadoAbril__c = 800,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 800,
            AsignadoJulio__c = 800,
            AsignadoAgosto__c = 800,
            AsignadoSeptiembre__c = 800,
            AsignadoOctubre__c = 800,
            AsignadoNoviembre__c = 800,
            AsignadoDiciembre__c = 800
        );
        nuevasAsignaciones.add(asignacionPres1);        

        AsignacionPresupuesto__c asignacionPres2 = new AsignacionPresupuesto__c(
            Distribuidor__c = portalAccount1.Id,
            Presupuesto__c = presupuesto2.Id,

            AsignadoEnero__c = 800, 
            AsignadoFebrero__c = 800,
            AsignadoMarzo__c = 800,
            AsignadoAbril__c = 800,
            AsignadoMayo__c = 800,
            AsignadoJunio__c = 800,
            AsignadoJulio__c = 800,
            AsignadoAgosto__c = 800,
            AsignadoSeptiembre__c = 800,
            AsignadoOctubre__c = 800,
            AsignadoNoviembre__c = 800,
            AsignadoDiciembre__c = 800
        );
        nuevasAsignaciones.add(asignacionPres2);        
        insert nuevasAsignaciones;

        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        
        Profile p = [Select Id, Name From Profile Where Name='Gerente de Ventas Distribuidor' LIMIT 1];
        User user1 = new User(
            Username = 'toyotaPartnerU@test.com',
            ContactId = contact1.Id,
            ProfileId = p.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert user1;
        
        SolicitudAutoDemo__c sad = new SolicitudAutoDemo__c();
        
        System.runAs(user1){
            //System.debug([SELECT InteresSerieActiva__c, InteresSerieActiva__r.Name FROM Serie__c]);

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(sad);
            AltaAutoDemoDistribuidorController ctmCtrl = new AltaAutoDemoDistribuidorController(stdCtrl);
            ctmCtrl.buscarFiltros();
            ctmCtrl.anterior();
            ctmCtrl.siguiente();

            ctmCtrl.camposBusqueda.selectedWholeSale = ctmCtrl.getVines().get(0).Id;
            ctmCtrl.help.VIN_Name__c = ctmCtrl.getVines().get(0).Name;

            ctmCtrl.guardar();            
        }

        Test.startTest();
            SolicitudAutoDemo__c sadUpdate = SolicitudAutoDemoTriggerHandlerTest.getAutoDemoTest();
            //SolicitudAutoDemoTriggerHandlerTest.test_method_update_demo_one(sadUpdate);
            SolicitudAutoDemoTriggerHandlerTest.test_method_update_demo_two(sadUpdate);
            SolicitudAutoDemoTriggerHandlerTest.test_method_update_demo_three(sadUpdate);
            SolicitudAutoDemoTriggerHandlerTest.test_method_update_demo_four(sadUpdate);        
        Test.stopTest();
    } // Fin test_method_Community

    public static void test_method_update_demo_one(SolicitudAutoDemo__c sad){                
        //Consolidado de Pagos
        ConsolidadoPagoTFS__c consolidado1 = new ConsolidadoPagoTFS__c(
            EffectiveDate__c = Date.today(),
            VIN__c = sad.VIN_Name__c,
            TransactionAmount__c = 200.00,
            OriginalPrincipalAmount__c = 200.00,
            SerialNumber__c = sad.Id
        );
        insert consolidado1;
        update sad;
        
        sad.Estatus__c = 'Solicitud baja';
        sad.ValidacionBajaTMEX__c ='Si';
        update sad;


        sad.Estatus__c = 'Solicitud alta';
        update sad;
        delete consolidado1;
    }

    public static void test_method_update_demo_two(SolicitudAutoDemo__c sad){
        sad.ValidacionTMEX__c = 'Si';
        //sad.ActivarDesarrollosInteresesAD__c = true;
        sad.ValidacionTFS__c ='Si';
        sad.FechaExibilidadPago__c = Date.today();
        sad.FechaBajaAutoDemoVigenciaPlanPiso__c = Date.today().addMonths(4);
        //sad.Estatus__c = 'Alta';
        sad.FechaAltaAutoDemo__c = Date.today();
        sad.SumatoriaTransacciones__c = 0.0;
        sad.OriginalPrincipalAmount__c = 0.0;
        
        update sad;

        sad.FechaAltaAutoDemo__c = Date.today().addMonths(3);
        update sad;
    }

    public static void test_method_update_demo_three(SolicitudAutoDemo__c sad){
        //System.debug(JSON.serialize(sad));
        List<CortePresupuesto__c> intereses = [SELECT Id, Mes__c, SolicitudAutoDemo__c FROM CortePresupuesto__c WHERE SolicitudAutoDemo__c=:sad.Id];
        if(intereses.size() > 0){
            CortePresupuesto__c cp =  intereses.get(1);
            delete cp;
        }

        update sad;
    }

    public static void test_method_update_demo_four(SolicitudAutoDemo__c sad){
        sad.Estatus__c = 'Baja anticipada';
        update sad;
    }

    @isTest static void test_method_one(){
        Account portalAccount1 = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '57055',           
            UnidadesAutosDemoAutorizadas__c = 5
        );
        insert portalAccount1;
        
        Map<String,Map<String,RecordType>> recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        Map<String, Account> mapaCuentas = AutoDemoTriggerMethods.getDealerCodeInfoMap(new Set<String>{'57055'});
        

        Vehiculo__c vin = new Vehiculo__c(
            Name = 'VIN1 Test',
            Id_Externo__c = 'VIN1',
            Distribuidor__c = portalAccount1 != null ? portalAccount1.Id : null,
            RecordTypeId = recordTypesMap.get('Vehiculo__c').get('Vehiculo').Id
        );

        insert vin;

        SolicitudAutoDemo__c demo1 = new SolicitudAutoDemo__c(
            VIN__c = vin.Id
        );

        insert demo1;
    }

    @isTest static void test_method_two(){
        //Periodos Fiscales
        PeriodoFiscal__c fy2018 = new PeriodoFiscal__c(
            FechaInicioPeriodoFiscal__c = Date.newInstance(2017, 4, 1),
            FechaFinPeriodoFiscal__c = Date.newInstance(2018, 3, 31),
            Name = '2018'
        );
        insert fy2018;

        // Presupuestos
        Presupuesto__c presupuesto1 = new Presupuesto__c(
            PeriodoFiscal__c = fy2018.Id,
            PresupuestoAutorizadoFinanzas__c = true,
            Activo__c = true,

            PresupuestoAutorizadoEnero__c = 1000,
            PresupuestoAutorizadoFebrero__c = 1000,
            PresupuestoAutorizadoMarzo__c = 1000,
            PresupuestoAutorizadoAbril__c = 1000,
            PresupuestoAutorizadoMayo__c = 1000,
            PresupuestoAutorizadoJunio__c = 1000,
            PresupuestoAutorizadoJulio__c = 1000,
            PresupuestoAutorizadoAgosto__c = 1000,
            PresupuestoAutorizadoSeptiembre__c = 1000,
            PresupuestoAutorizadoOctubre__c = 1000,
            PresupuestoAutorizadoNoviembre__c = 1000,
            PresupuestoAutorizadoDiciembre__c = 1000
        );
        insert presupuesto1;

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(presupuesto1);
        PresupuestoDetailController controller = new PresupuestoDetailController(stdCtrl);
        controller.camposDeFieldSet();
        System.assertEquals(false, controller.cabecerasDetail == null);
        System.assertEquals(false, controller.cabecerasEdit == null);
    }
    
}