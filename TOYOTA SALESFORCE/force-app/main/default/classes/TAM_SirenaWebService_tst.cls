/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el metodo de prueba de la clases
                        TAM_SirenaWebService y TAM_SirenaWrpLead

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    27-Julio-2021        Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_SirenaWebService_tst {

    static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

    @TestSetup static void loadData(){
        
        Account clienteDealer = new Account(
            Name = 'TOYOTA PRUEBA',
            Codigo_Distribuidor__c = '57013',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización',
            RecordTypeId = sRectorTypePasoDistribuidor,
            TAM_FechaVigenciaInicio__c = Date.today(),
            TAM_FechaVigenciaFin__c = Date.today()
        );
        insert clienteDealer;
                
        TAM_Integraciones__c objIntegracion = new TAM_Integraciones__c(
            Name = 'Toyota Santa Fe'
            ,TAM_UrlConsultaCtesDealer__c = 'https://api.getsirena.com/v1/'
            , TAM_TipoIntegreacion__c = 'Sirena'
            , TAM_PermiteConsultarTodosLeads__c = true
            , TAM_PermiteConsultarTodosLea__c = true
            , TAM_Distribuidor__c = clienteDealer.id
            , TAM_ApiKey__c = 'pFm8HEczABluOuN2YjhOyWaba2f5c75H'
        );
        insert objIntegracion;
        
        Lead objLead = new Lead(
            FirstName = 'Prueba'
            , LastName = 'Prueba'
            , TAM_IdExternoSirena__c = '60fda1356e4b97000864dab6'
            , Phone = '5534567876'
            , Email = 'prueba@hotmail.com'
            , FWY_Vehiculo__c = 'Avanza'
            , FWY_codigo_distribuidor__c = '57013'
            , TAM_FormaContactoPreferida__c = 'WhatsApp'
            , TAM_ConfirmaDatos__c = true
        );
        insert objLead;

        Interacciones_Lead__c objIteracSirena = new Interacciones_Lead__c(
            TAM_IdExterno__c = objLead.id + '-' + aleatorio(15),
            TAM_MedioContacto__c = 'Whatsapp',
            TAM_FechaContacto__c = Date.today(),
            TAM_FechaFinContacto__c = Date.today(),
            TAM_Detalle__c = 'Prueba',
            TAM_Candidato__c = objLead.id,
            TAM_IdExternoLead__c = objLead.id + '-' + aleatorio(15)
        );
        insert objIteracSirena;
                                            
    }

    public static Integer aleatorio(Integer max){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, max);
    }

    static testMethod void TAM_SirenaWebServiceOK() {

        Test.startTest();
            Integer intGetLeadsSirenaMinutosPrm = 5;
            String sLstLeads = '';
            Lead objLeadPaso = new Lead();
            Set<String> setIdlead = new Set<String>();
             
            TAM_CallRestWebService.datosWebService objDatosWebService = new TAM_CallRestWebService.datosWebService();
            
            //Consulta el registro de TAM_Integraciones__c
            for (TAM_Integraciones__c objIntegraCons : [Select t.TAM_UrlConsultaCtesDealer__c, 
                t.TAM_TipoIntegreacion__c, t.TAM_PermiteConsultarTodosLeads__c, 
                t.TAM_PermiteConsultarTodosLea__c, t.TAM_Distribuidor__c, t.TAM_ApiKey__c, t.Name 
                From TAM_Integraciones__c t LIMIT 1]){
                String strUrlPaso = objIntegraCons.TAM_UrlConsultaCtesDealer__c;                
                strUrlPaso += 'prospect/60fda1356e4b97000864dab6?api-key=pFm8HEczABluOuN2YjhOyWaba2f5c75H';            
                //Inicializa el objeto de objDatosWebService
                objDatosWebService = new TAM_CallRestWebService.datosWebService(strUrlPaso, 'GET',
                '', new Map<String, String>());
            }
           
            //Consulta los datos del un lead
            for (Lead objLeadCons : [Select id, TAM_IdExternoSirena__c From Lead Limit 1]){
                objLeadPaso = objLeadCons;
                sLstLeads = objLeadCons.id + ';';
                setIdlead.add(objLeadCons.id);
            }
            System.debug('EN TAM_SirenaWebServiceOK sLstLeads: ' + sLstLeads);
            System.debug('EN TAM_SirenaWebServiceOK setIdlead: ' + setIdlead);
            
            //Llama al metodo TAM_SirenaWebService.getLeadsSirena
            TAM_SirenaWebService.getLeadsSirena(objDatosWebService, 5);
            //Llama al metodo TAM_SirenaWebService.getLeadsSirena
            TAM_SirenaWebService.getLeadsSirenaInteracciones(objDatosWebService, sLstLeads);

            //Llama al metodo TAM_SirenaWebService.getLeadsSirena
            TAM_SirenaWebService.getNuevosLeadsSirena(objDatosWebService, setIdlead);
            //Llama al metodo TAM_SirenaWebService.getNuevosLeadsSirenaInteracciones
            TAM_SirenaWebService.getNuevosLeadsSirenaInteracciones(objDatosWebService, setIdlead);

            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.SirenaWrpLead objSirenaWrpLead = new TAM_SirenaWrpLead.SirenaWrpLead();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.contactMediumsObj objcontactMediumsObj = new TAM_SirenaWrpLead.contactMediumsObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.productObj objproductObj = new TAM_SirenaWrpLead.productObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.leadObj objleadObj = new TAM_SirenaWrpLead.leadObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.SirenaWrpLeadInteraccion objSirenaWrpLeadInteraccion = new TAM_SirenaWrpLead.SirenaWrpLeadInteraccion();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.outputObj objoutputObj = new TAM_SirenaWrpLead.outputObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.questionObj objquestionObj = new TAM_SirenaWrpLead.questionObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.messageObj objmessageObj = new TAM_SirenaWrpLead.messageObj();
            //Crea los objertos para la clase TAM_SirenaWrpLead
            TAM_SirenaWrpLead.conversationsObj objconversationsObj = new TAM_SirenaWrpLead.conversationsObj();


            ///*** PRUEBA DE LOS PROCESOS PARA LA CONSULTA DE LOS DATOS DEL WEB SERVICE ****///
            
	        //String strCodDist = '57205';
            String strTipoIntegracion = 'Sirena';
	        String strCodDistSirenaIntegra = String.valueOf(System.Label.TAM_CodDistSirenaIntegra);
	                
	        String sQuery = 'Select t.id, t.TAM_UrlConsultaCtesDealer__c, t.TAM_TipoIntegreacion__c, t.TAM_Distribuidor__c, ';
	        sQuery += ' t.TAM_ApiKey__c, t.Name From TAM_Integraciones__c t  ';
	        sQuery += ' where t.TAM_Distribuidor__r.Codigo_Distribuidor__c = \'' + String.escapeSingleQuotes(strCodDistSirenaIntegra) + '\'';
            sQuery += ' And t.TAM_TipoIntegreacion__c = \'' + String.escapeSingleQuotes(strTipoIntegracion) + '\'';
	        sQuery += ' Order by Name';	
	        sQuery += ' Limit 1';
	        System.debug('EN TAM_SirenaWebServiceSch.execute sQuery: ' + sQuery);
            
            //Crea el objeto de  TAM_SirenaWebServiceBch      
            TAM_SirenaWebServiceBch objSirenaWebServiceBch = new TAM_SirenaWebServiceBch(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objSirenaWebServiceBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_SirenaWebServiceSch SirenaWebServiceSch_Jobh = new TAM_SirenaWebServiceSch();
            System.schedule('Ejecuta_TAM_SirenaWebServiceSch_cls', sch, SirenaWebServiceSch_Jobh);
            
            
            //Crea el objeto de  TAM_SirenaWebServiceBch      
            TAM_SirenaInteracWebServiceBch_cls objSirenaInteracWebServiceBch = new TAM_SirenaInteracWebServiceBch_cls(sQuery, sLstLeads);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId2 = Database.executeBatch(objSirenaInteracWebServiceBch, 1);
            
            string strSeconds2 = '0';
            string strMinutes2 = '0';
            string strHours2 = '1';
            string strDay_of_month2 = 'L';
            string strMonth2 = '6,12';
            string strDay_of_week2 = '?';
            string strYear2 = '2050-2051';
            String sch2 = strSeconds2 + ' ' + strMinutes2 + ' ' + strHours2 + ' ' + strDay_of_month2 + ' ' + strMonth2 + ' ' + strDay_of_week2 + ' ' + strYear2;
            
            TAM_SirenaInteracWebServicesch_cls SirenaInteracWebServicesch_Jobh = new TAM_SirenaInteracWebServicesch_cls();
            System.schedule('Ejecuta_TAM_SirenaInteracWebServicesch_cls', sch2, SirenaInteracWebServicesch_Jobh);            
            
            
            ///*** FIN PRUEBA DE LOS PROCESOS PARA LA CONSULTA DE LOS DATOS DEL WEB SERVICE ****///
        
        Test.stopTest();        
        
    }
}