global class DeleteNullOrders_Schedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		String query = 'SELECT Id, Name, (SELECT Id, OrderNumber, CreatedDate, RecordTypeId FROM Orders WHERE CreatedDate = THIS_MONTH) FROM Account';		
		System.debug(query);

		DeleteNullOrders_Batch b = new DeleteNullOrders_Batch(query);
		database.executebatch(b);
	}
}