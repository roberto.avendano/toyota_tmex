global class TAM_ActualizaSolicitudBono Implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        actualizaEstatusVINSolicitudBono();
        
    } 
     
    
    public void actualizaEstatusVINSolicitudBono (){ 
        Map<String,TAM_SolicitudDeBono__c> mapSolicitudes = new Map<String,TAM_SolicitudDeBono__c>();
        Map<Id,TAM_SolicitudDeBono__c> mapSolicitudesUpd = new Map<Id,TAM_SolicitudDeBono__c>();
        List<Movimiento__c> listaMovimientos = new  List<Movimiento__c>();
        List<TAM_SolicitudDeBono__c> solicitudesBono = new List<TAM_SolicitudDeBono__c>();
        Set<String> vinSolicitud = new Set<String>();
        
        solicitudesBono = [Select name,id,TAM_EstatusVIN__c,TAM_VIN__c,TAM_EstatusSolicitudBono__c  FROM
                           TAM_SolicitudDeBono__c WHERE TAM_EstatusVIN__c = 'VIN en Inventario'];
        
        if(!solicitudesBono.isEmpty()){
            for(TAM_SolicitudDeBono__c solIN : solicitudesBono){
                vinSolicitud.add(solIn.TAM_VIN__c);
                mapSolicitudes.put(solIn.TAM_VIN__c,solIN);
                
            }
            
            
        }
        system.debug('mapa de solicitudes'+mapSolicitudes);
        
        if(!vinSolicitud.isEmpty()){
            listaMovimientos = [Select id,Sale_Code__c,Submitted_Date__c,name,TAM_Serie__c,TAM_VIN__c,Last_Name__c,NombreCliente__c,Sale_Date__c,
                                Trans_Type__c   From Movimiento__c WHERE TAM_VIN__c IN : VINSolicitud AND 
                                VIN__r.Ultimo_Movimiento__r.Trans_Type__c = 'RDR' limit 1
                               ];  
            
            
        }

        if(!listaMovimientos.isEmpty()){
            for(Movimiento__c movIn : listaMovimientos){
                if(mapSolicitudes.containsKey(movIn.TAM_VIN__c)){
                    TAM_SolicitudDeBono__c solUpdate = new TAM_SolicitudDeBono__c();
                    solUpdate = mapSolicitudes.get(movIn.TAM_VIN__c);  
                    solUpdate.TAM_EstatusVIN__c = 'VIN en Dealer Daily';
                    solUpdate.TAM_CodigoVenta__c = movIn.Sale_Code__c;
                    solUpdate.TAM_FechaVenta__c = movIn.Submitted_Date__c.date();
                    mapSolicitudesUpd.put(solUpdate.id,solUpdate);

                }
            }
        } 
        
        system.debug('lista a insertar'+mapSolicitudesUpd);
        if(!mapSolicitudesUpd.isEmpty()){
            update mapSolicitudesUpd.values();
            
        }
        
    }
    
}