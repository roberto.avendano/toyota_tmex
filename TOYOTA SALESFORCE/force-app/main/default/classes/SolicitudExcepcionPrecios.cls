/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de las solicitudes de precios
******************************************************************************* */

public without sharing  class SolicitudExcepcionPrecios {
    
    
    @AuraEnabled
    public static TAM_DetallePolitica__c getPrecioVigente(String modelNumber, String modelYear){
        TAM_DetallePolitica__c politicaVigenteObj = new TAM_DetallePolitica__c();
        TAM_DetallePolitica__c[] politicaVigente = [SELECT TAM_IncentivoTMEX_Cash__c,TAM_PoliticaIncentivos__R.TAM_EstatusIncentivos__c,
                                                    id,TAM_AnioModelo__c,TAM_Clave__c,TAM_IncDealer_Cash__c FROM TAM_DetallePolitica__c 
                                                    WHERE
                                                    TAM_PoliticaIncentivos__R.TAM_EstatusIncentivos__c = 'Vigente' AND TAM_Clave__c =: modelNumber AND TAM_AnioModelo__c =: modelYear limit 1];
        
        if (politicaVigente.size() > 0) {
            return politicaVigente[0];
        }else{
            return politicaVigenteObj;
            
        }	
        
    }   
    
    @AuraEnabled
    public static List<PricebookEntry> getPreciosVIN(String modelNumber, String modelYear){
        String codigoProducto = modelNumber+modelYear;
        Date fechaHoy = system.today();
        Date fechaMenos2Meses = date.today().adddays(-60);
        List<PricebookEntry> listaPrecioObj = new List<PricebookEntry>();
        
        //Consultar el precio del Vehiculo en base a la lista de precos del dia de hoy
        List<PricebookEntry> listaPrecioVehiculo = [select Pricebook2.FinVigencia__c,id,PrecioDealer__c,Pricebook2.Name,PrecioMayorista__c,PrecioPublico__c
                                                    ,Product2.ProductCode,Product2.Anio__c From PricebookEntry 
                                                    WHERE  Product2.IdExternoProducto__c =: codigoProducto AND IsActive = true AND PrecioPublico__c != NULL order by Pricebook2.FinVigencia__c desc limit 3];
        
        if(!listaPrecioVehiculo.isEmpty()){
            return listaPrecioVehiculo;
        }else{
            return listaPrecioObj;
            
        }
        
        
    }
    
    
    @AuraEnabled
    public static Id registraSolicitudExcepcion(String vin, Integer precioVigente, Integer precioSolicitado, String comentario,String motivoSelect,String codigoDealer){
        
        //Consultamos la cuenta del distribuidor a partir del número de dealer
        Account acct = [Select id from Account WHERE Codigo_Distribuidor__c =:codigoDealer];
        
        
        Id recordTypeId =  Schema.SObjectType.TAM_SolicitudExpecionIncentivo__c.getRecordTypeInfosByDeveloperName().get('Cambio_de_Precio').getRecordTypeId();
        TAM_SolicitudExpecionIncentivo__c solicitudExepcion = new TAM_SolicitudExpecionIncentivo__c();
        solicitudExepcion.TAMVIN__c = vin;
        solicitudExepcion.TAM_PrecioVigente__c =  precioVigente;
        solicitudExepcion.TAM_PrecioSolicitadoDealer__c = precioSolicitado;
        solicitudExepcion.TAM_Comentario__c = Comentario;
        solicitudExepcion.TAM_AprobacionDM__c = 'Pendiente';
        solicitudExepcion.TAM_AprobacionFinanzas__c = 'Pendiente';
        solicitudExepcion.TAM_MotivoCambioPrecio__c = motivoSelect;
        solicitudExepcion.TAM_Distribuidor__c = acct.Id;
        solicitudExepcion.RecordTypeId = recordTypeId;
        
        
        try{
            insert solicitudExepcion;
            
        }catch(Exception e){
            system.debug('Error general al insertar la solicitud de exepeción'+e.getMessage());
            
        }
        
        return solicitudExepcion.id;    
        
    }
    
    @AuraEnabled
    public static wrapResumenPrecio calculoPrecioVigente(String modelNumber,String modelYear,Date fechaVenta){
        PricebookEntry[] precioVehiculo;
        PricebookEntry[] precioVehiculoFechaVenta;
        system.debug('fecha de venta vin'+fechaVenta);
        wrapResumenPrecio resumenPrecio = new wrapResumenPrecio();
        String codigoProducto = modelNumber+modelYear;       
        String nombreListaPrecio;
        /*
Map<Integer, String> mapMes = new Map<Integer, String>();
mapMes.put(1, 'Enero');
mapMes.put(2, 'Febrero');
mapMes.put(3, 'Marzo');
mapMes.put(4, 'Abril');
mapMes.put(5, 'Mayo');
mapMes.put(6, 'Junio');
mapMes.put(7, 'Julio');
mapMes.put(8, 'Agosto');
mapMes.put(9, 'Septiembre');
mapMes.put(10, 'Octubre');
mapMes.put(11, 'Noviembre');
mapMes.put(12, 'Diciembre');


if(fechaVenta != null){
integer saleMonth =  fechaVenta.month();
integer saleYear =  fechaVenta.year();         
String mes = mapMes.get(saleMonth);
String nombreListaPreciosVenta = 'VH-'+mes+'-'+saleYear;

//Consultar lista de precios 
Pricebook2[] listaPrecio = [Select id,name from Pricebook2 where name =: nombreListaPreciosVenta];
//Consultar el precio del Vehiculo en base a la lista de precos del dia de hoy
precioVehiculoFechaVenta = [select Pricebook2.FinVigencia__c,name,PrecioSinIva__c,Pricebook2.Name,PrecioDealer__c,PrecioMayorista__c,PrecioPublico__c
,Product2.ProductCode,Product2.Anio__c From PricebookEntry 
WHERE  Product2.IdExternoProducto__c =: codigoProducto AND Pricebook2Id =: listaPrecio[0].id order by Pricebook2.FinVigencia__c desc];

if(precioVehiculoFechaVenta.size() > 0){
decimal precioPublico = precioVehiculoFechaVenta[0].PrecioPublico__c;   
resumenPrecio.precioPublico =precioVehiculoFechaVenta[0].PrecioPublico__c;
resumenPrecio.precioDealer = precioVehiculoFechaVenta[0].PrecioDealer__c;
resumenPrecio.idListaPrecio = precioVehiculoFechaVenta[0].Id;
resumenPrecio.nombreListaPrecio = precioVehiculoFechaVenta[0].Pricebook2	.Name;
resumenPrecio.precioSinIVA = precioVehiculoFechaVenta[0].PrecioSinIva__c;
resumenPrecio.tipoListaPrecio = 'Precio de fecha de Venta'; 

}

if(precioVehiculoFechaVenta.size() == 0){

precioVehiculo = [select Pricebook2.FinVigencia__c,id,name,PrecioSinIva__c,Pricebook2.Name,PrecioDealer__c,PrecioMayorista__c,PrecioPublico__c
,Product2.ProductCode,Product2.Anio__c From PricebookEntry 
WHERE  Product2.IdExternoProducto__c =: codigoProducto AND PrecioPublico__c != NULL order by Pricebook2.FinVigencia__c desc];

decimal precioPublico = precioVehiculo[0].PrecioPublico__c;   
resumenPrecio.precioPublico =precioVehiculo[0].PrecioPublico__c;
resumenPrecio.precioDealer = precioVehiculo[0].PrecioDealer__c;
resumenPrecio.idListaPrecio = precioVehiculo[0].Id;
resumenPrecio.nombreListaPrecio = precioVehiculo[0].Pricebook2	.Name;
resumenPrecio.precioSinIVA = precioVehiculo[0].PrecioSinIva__c;
resumenPrecio.tipoListaPrecio = 'Precio de fecha de Venta'; 

}




}*/
        //Calcular el precio con la fecha de hoy
        if(fechaVenta == null){
            
            //Consultar lista de precios 
            //Pricebook2[] listaPrecio = [Select id,name from Pricebook2 where name =: nombreListaPreciosHoy];
            //Consultar el precio del Vehiculo en base a la lista de precos del dia de hoy
            precioVehiculo = [select Pricebook2.FinVigencia__c,id,name,PrecioSinIva__c,Pricebook2.Name,PrecioDealer__c,PrecioMayorista__c,PrecioPublico__c
                              ,Product2.ProductCode,Product2.Anio__c From PricebookEntry 
                              WHERE  Product2.IdExternoProducto__c =: codigoProducto AND PrecioPublico__c != NULL order by Pricebook2.FinVigencia__c desc];
            
            
            decimal precioPublico = precioVehiculo[0].PrecioPublico__c;
            
            resumenPrecio.precioPublico =precioVehiculo[0].PrecioPublico__c;
            resumenPrecio.precioDealer = precioVehiculo[0].PrecioDealer__c;
            resumenPrecio.idListaPrecio = precioVehiculo[0].Id;
            resumenPrecio.nombreListaPrecio = precioVehiculo[0].Pricebook2.Name;
            resumenPrecio.precioSinIVA = precioVehiculo[0].PrecioSinIva__c;
            resumenPrecio.tipoListaPrecio = 'Precio Vigente';
            
            
        }
        
        
        
        return resumenPrecio;
        
    } 
    
    @AuraEnabled
    public static List<String> obtieneMotivosSolicitud(){
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = TAM_SolicitudExpecionIncentivo__c.TAM_MotivoCambioPrecio__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }   
        
        return pickListValuesList;
        
    }
    
    @AuraEnabled
    public static List<TAM_FacturaCarga__c> buscaFacturasVIN (String VIN){
        List<TAM_FacturaCarga__c> facturasObj = new List<TAM_FacturaCarga__c> ();
        List<TAM_FacturaCarga__c> facturas    = [Select id,name,VIN__c,FechaFactura__c FROM TAM_FacturaCarga__c WHERE VIN__c =: VIN];
        
        if(!facturas.isEmpty()){
            return facturas;
            
        }else{
            return facturasObj;
            
        }
        
        
    }
    
    
    public class wrapResumenPrecio {
        @AuraEnabled public boolean inventarioF {get; set;} 
        @AuraEnabled public decimal precioPublico {get; set;}
        @AuraEnabled public decimal precioDealer {get; set;}
        @AuraEnabled public decimal incentivoSeleccionado {get; set;}
        @AuraEnabled public decimal precioFinal {get; set;}
        @AuraEnabled public decimal precioSinIVA {get; set;}
        @AuraEnabled public String  idListaPrecio {get; set;}
        @AuraEnabled public String  nombreListaPrecio {get; set;}
        @AuraEnabled public String  tipoListaPrecio {get; set;}
    }    
    
}