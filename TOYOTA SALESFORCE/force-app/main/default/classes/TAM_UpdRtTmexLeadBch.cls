/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_UpdRtTmexLeadSch y TAM_UpdRtTmexLeadBch.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    03-Septiembre-2020   Héctor Figueroa             Creación
******************************************************************************* */

global with sharing class TAM_UpdRtTmexLeadBch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
	
    global string query;
    
    //Un constructor por default
    global TAM_UpdRtTmexLeadBch(string query){
        this.query = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_UpdRtTmexLeadBch.start query: ' + this.query);
        return Database.getQueryLocator(this.query);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<Lead> scope){
        System.debug('EN TAM_UpdRtTmexLeadBch.');

		String VaRtLeadRetailNuevos = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Retail Nuevos').getRecordTypeId();  
	    String VaRtLeadRtPruebaMajejo = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Prueba de Manejo').getRecordTypeId();
		      
		List<Lead> lLeadUpd = new List<Lead>();
		
		//Crea un objeto del tipo 
		Savepoint sp = Database.setSavepoint();        
				
        //Recorre la lista de Casos para cerrarlos 
        for (Lead objLeadPaso : scope){
			//Crea el objeto del tipo Lead y metelo a la lista de lLeadUpd
			lLeadUpd.add(new Lead(
					id = objLeadPaso.id,
					RecordTypeId = VaRtLeadRtPruebaMajejo,
					TAM_ConfirmaDatos__c = true 
				)
			);        	
        }
       	System.debug('EN TAM_UpdRtTmexLeadBch lLeadUpd: ' + lLeadUpd);

        //Ve si tiene algo la lista de mapInvDummyPasoUpd
        if (!lLeadUpd.isEmpty()){
            //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
			List<Database.Saveresult> lDtbUpsRes = Database.update(lLeadUpd, false);
			//Ve si hubo error
			for (Database.Saveresult objDtbUpsRes : lDtbUpsRes){
				if (!objDtbUpsRes.isSuccess())
					System.debug('EN TAM_UpdRtTmexLeadBch Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
				if (objDtbUpsRes.isSuccess())
					System.debug('EN TAM_UpdRtTmexLeadBch Los datos del Lead se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
			}//Fin del for para lDtbUpsRes
        }//Fin si !mapInvDummyPasoUpd.isEmpty()
		
		//Solo para pruebas
		//Database.rollback(sp);
		
    }
    
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_UpdRtTmexLeadBch.finish Hora: ' + DateTime.now());     
        Integer intMergeShareAccountsMinutos = Integer.valueOf(System.Label.TAM_UpdLeadRecodTypeMinutos);
		DateTime dtHoraActual = DateTime.now();
		DateTime dtHoraFinal = dtHoraActual.addMinutes(intMergeShareAccountsMinutos); //5
		String CRON_EXP = dtHoraFinal.second() + ' ' + dtHoraFinal.minute() + ' ' + dtHoraFinal.hour() + ' ' + dtHoraFinal.day() + ' ' + dtHoraFinal.month() + ' ? ' + dtHoraFinal.year();
    	System.debug('EN TAM_UpdRtTmexLeadBch.finish CRON_EXP: ' + CRON_EXP);

		//Manda a llamar el proceso de Oportunidades asociadoa a las cuentas de sLClientes TAM_MergeShareAccountsSch
		TAM_UpdRtTmexLeadSch objUpdRtTmexLeadSch = new TAM_UpdRtTmexLeadSch();
    	System.debug('EN TAM_UpdRtTmexLeadBch.finish objUpdRtTmexLeadSch: ' + objUpdRtTmexLeadSch);
		//Programa el proceso desde System
		if (!Test.isRunningTest())
			System.schedule('TAM_UpdRtTmexLeadSch: ' + CRON_EXP + '-' + UserInfo.getUserId() + ': ' + CRON_EXP, CRON_EXP, objUpdRtTmexLeadSch);
    } 
	
}