public with sharing class CancelacionAutoDemoEditController {
	private ApexPages.StandardController myController;
	private SolicitudAutoDemo__c help;
	public String picklistValue{get;set;}

	public CancelacionAutoDemoEditController(ApexPages.StandardController controller) {
		this.myController = controller;
		picklistValue ='';
	}

	public PageReference guardar(){
		PageReference ret;
			try{
				help = (SolicitudAutoDemo__c) myController.getRecord();
				help.Estatus__c = 'Cancelada';				
				update help;
				ret = new PageReference('/apex/CancelacionAutoDemo');
				ret.setRedirect(true);

			} catch(Exception e){
				System.debug(e.getMessage());
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		return ret;
	}
}