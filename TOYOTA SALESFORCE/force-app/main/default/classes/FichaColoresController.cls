public with sharing class FichaColoresController {
    private FichaTecnicaProducto__c fichaProducto;
    public FichaColores__c fichaColoresActual{get;set;}
    public List<Versiones_Ficha_Colores__c> versiones{get;set;}
    public List<Versiones_Ficha_Colores__c> versionesDoc{get;set;}
    public Map<Id, Map<Id, Color_Externo_Seleccionado__c>> seleccionados{get;set;}
    public List<ColorExterno__c> coloresExternos{get;set;} 
    
    public FichaColoresController(FichaTecnicaController controller) {
        fichaColoresActual = FichaColoresController.getFichaActual((String)ApexPages.currentPage().getParameters().get('id'));
        
        //if(fichaColoresActual != null && fichaColoresActual.Id != null){
        //this.versiones = FichaColoresController.getVersionesColores(fichaColoresActual.Id);
        //this.coloresExternos = FichaColoresController.getColoresExternos(fichaColoresActual);
        //this.seleccionados = FichaColoresController.getColoresSeleccionados(fichaColoresActual.Id);
        //System.debug(JSON.serialize(seleccionados));
        //}
    }
    
    public PageReference actualizaColoresSeleccionados(){
        List<Color_version__c> coloresVersion = new List<Color_version__c>();
        if(fichaColoresActual != null && fichaColoresActual.Id != null){
            versiones = FichaColoresController.getVersionesColoresDoc(fichaColoresActual.Id);
            coloresExternos = FichaColoresController.getColoresExternos(fichaColoresActual);
            system.debug('seleccionados'+coloresExternos);
            for(Versiones_Ficha_Colores__c ver: versiones){
                coloresVersion.addAll(ver.Colores_version__r);
            }
            
            if(coloresVersion.size() > 0){
                try{
                    update coloresVersion;
                    System.debug('Actualizacion colores version exitosa.');
                    
                    seleccionados = FichaColoresController.getColoresSeleccionados(fichaColoresActual.Id);
                } catch(Exception ex){
                    System.debug('Error al actualizar colores de version: '+ex.getMessage());
                }
            }
        }
        
        return null;
    }
    
    
    @AuraEnabled
    public static Map<Id, Map<Id, Color_Externo_Seleccionado__c>> getColoresSeleccionados(String fichaID){
        Map<Id, Map<Id, Color_Externo_Seleccionado__c>> retMap = new Map<Id, Map<Id, Color_Externo_Seleccionado__c>>();
        Set<String> coloresVersionIDS = new Set<String>();
        
        for(Versiones_Ficha_Colores__c ver: FichaColoresController.getVersionesColores(fichaID)){
            for(Color_version__c cv : ver.Colores_version__r){
                coloresVersionIDS.add(cv.Id);
            }
        }
        
        for(Color_version__c cv: [SELECT Id, Name, (SELECT Name, Agregado__c, DPMS__c,TAM_OrdEspecial__c, ColorExterno__c, ColorExterno__r.Name, ColorExterno__r.CodigoColor__c FROM Colores_Externos_Seleccionados__r)
                                  FROM Color_version__c WHERE Id IN:coloresVersionIDS]){
                                      
                                      if(!retMap.containsKey(cv.Id)){
                                          retMap.put(cv.Id, new Map<Id, Color_Externo_Seleccionado__c>());
                                      }
                                      
                                      for(Color_Externo_Seleccionado__c colorExt: cv.Colores_Externos_Seleccionados__r){
                                          if(!retMap.get(cv.Id).containsKey(colorExt.ColorExterno__c)){
                                              retMap.get(cv.Id).put(colorExt.ColorExterno__c, colorExt);
                                          }
                                      }
                                  }
        System.debug(JSON.serialize(retMap));
        return retMap;
    }
    
    
    @AuraEnabled
    public static void guardarColor(Color_Externo_Seleccionado__c color){
        try{                    
            
            update color;
        }catch(Exception ex){
            System.debug(ex.getMessage());           
        }
    }
    
    
    @AuraEnabled
    public static FichaColores__c getFichaActual(String fichaProdID){
        FichaColores__c fichaActual;
        
        for(FichaColores__c fc: [SELECT Id, Name, ColoresAgregados__c, FichaTcnicaProducto__c, Activo__c, FichaProductoEstatus__c, FichaTcnicaProducto__r.Serie__r.Imagen__c, FichaTcnicaProducto__r.Serie__r.Logo__c,FichaTcnicaProducto__r.Serie__r.Name,FichaTcnicaProducto__r.Imagen_Auto__c,FichaTcnicaProducto__r.Logo_serie__c FROM FichaColores__c WHERE FichaTcnicaProducto__c=:fichaProdID]){
            if(fc.Activo__c){
                fichaActual = fc;
                break;
            }
        }
        
        return fichaActual;
    }
    
    @AuraEnabled
    public static List<Versiones_Ficha_Colores__c> getVersionesColoresDoc(String fichaID){
        system.debug('id ficha tecnica'+fichaID);
        //Logica para traer los códigos de versiones activas
        FichaColores__c fichaColres = [select id,FichaTcnicaProducto__c From FichaColores__c Where id =: fichaID];
        List<CodigoModeloFichaTecnica__c> versionesFicha = [Select id,CodigoModeloVersion__r.name,TAM_ActivarVersion__c From CodigoModeloFichaTecnica__c 
                                                            WHERE FichaTecnica__c =: fichaColres.FichaTcnicaProducto__c AND TAM_ActivarVersion__c = true];
        
        Set<String> codigosModelosActivos = new Set<String>();
        
        if(!versionesFicha.isEmpty()){
            for(CodigoModeloFichaTecnica__c codModelo : versionesFicha){
                codigosModelosActivos.add(codModelo.CodigoModeloVersion__r.name);
                
            }
            
            
        }
        
        system.debug('set de versiones'+codigosModelosActivos);
        List<Versiones_Ficha_Colores__c> versionesFC = [SELECT Name, Orden__c, Ficha_Colores__c, AcabadoInterior__r.Name, 
                                                        (SELECT Name,TAM_NombreProducto__c,TAM_NombreVersion__c, Orden__c, Producto__r.Name, Producto__r.ProductCode, Producto__r.NombreVersion__c, Producto__c FROM Productos_de_version__r where Producto__r.Name  != null AND Producto__r.Name IN: codigosModelosActivos ORDER BY Orden__c ASC),
                                                        (SELECT Name, ColorInterior__r.Name, ColorInterior__r.CodigoColor__c, Orden__c  FROM Colores_version__r ORDER BY Orden__c ASC) 
                                                        FROM Versiones_Ficha_Colores__c WHERE Ficha_Colores__c=:fichaID  ORDER BY Orden__c ASC ];
        
        Set<Versiones_Ficha_Colores__c> listaReturn = new Set<Versiones_Ficha_Colores__c> ();
        List<Versiones_Ficha_Colores__c> result = new List<Versiones_Ficha_Colores__c>();
        for(Versiones_Ficha_Colores__c ver : versionesFC){
            for(Producto_Version__c prod : ver.Productos_de_version__r){
                if(prod.Producto__r.Name != null){
                    listaReturn.add(ver);
                }
                
            }
        }
        
        result.addAll(listaReturn);
        system.debug('ver return'+result);
        return result;
    }
    
    @AuraEnabled
    public static List<Versiones_Ficha_Colores__c> getVersionesColores(String fichaID){
        return [SELECT Name, Orden__c, Ficha_Colores__c, AcabadoInterior__r.Name, 
                (SELECT Name,TAM_NombreProducto__c,TAM_NombreVersion__c, Orden__c, Producto__r.Name, Producto__r.ProductCode, Producto__r.NombreVersion__c, Producto__c FROM Productos_de_version__r  ORDER BY Orden__c ASC),
                (SELECT Name, ColorInterior__r.Name, ColorInterior__r.CodigoColor__c, Orden__c  FROM Colores_version__r ORDER BY Orden__c ASC) 
                FROM Versiones_Ficha_Colores__c WHERE Ficha_Colores__c=:fichaID ORDER BY Orden__c ASC];
    }
    
    @AuraEnabled
    public static List<ColorExterno__c> getColoresExternos(FichaColores__c ficha){
        List<String> externalColorIDS;
        List<ColorExterno__c> retList;
        
        if(ficha.ColoresAgregados__c != null){
            externalColorIDS = ficha.ColoresAgregados__c.split(',');
            retList = [SELECT Name, CodigoColor__c, Orden__c FROM ColorExterno__c WHERE CodigoColor__c IN:externalColorIDS ORDER BY Orden__c ASC];
        }
        
        return retList;     
    }
    
    @AuraEnabled
    public static List<ColoresAgregadosWrapper> getColoresAgregados(String fichaProdID){
        List<ColoresAgregadosWrapper> retList = new List<ColoresAgregadosWrapper>();
        FichaColores__c fichaActual = FichaColoresController.getFichaActual(fichaProdID);
        Set<String> agregadosSET = new Set<String>();
        
        if(fichaActual.ColoresAgregados__c != null){
            agregadosSET.addAll(fichaActual.ColoresAgregados__c.split(','));
        }
        
        for(ColorExterno__c ce: [SELECT Name, CodigoColor__c, Orden__c FROM ColorExterno__c ORDER BY CodigoColor__c DESC]){
            if(agregadosSET.contains(ce.CodigoColor__c)){
                retList.add(new ColoresAgregadosWrapper(ce, true));
            } else {
                retList.add(new ColoresAgregadosWrapper(ce, false));
            }
        }
        
        return retList;
    }
    
    @AuraEnabled
    public static void guardaColoresAgregados(String colores, String fichaProdID){
        List<String> lstColModificados = new List<String> ();
        FichaColores__c fichaActual = FichaColoresController.getFichaActual(fichaProdID);
        fichaActual.ColoresAgregados__c = colores;
        String coloresOrginales = [Select ColoresAgregados__c FROM FichaColores__c WHERE id =: fichaActual.id].ColoresAgregados__c;
        List<String> lstColOriginales =  colores.split(',');
        if(coloresOrginales != null){
            lstColModificados = coloresOrginales.split(','); 
        }        
        Set<String> coloresEliminados = new Set<String>(lstColModificados);
        List<Color_Externo_Seleccionado__c> coloresUpd = new List<Color_Externo_Seleccionado__c>();
        coloresEliminados.removeAll(lstColOriginales);  
        if(!coloresEliminados.isEmpty()){
            coloresUpd = [Select id,Agregado__c,DPMS__c,ColorExterno__r.CodigoColor__c FROM Color_Externo_Seleccionado__c WHERE Color_version__r.version__r.Ficha_Colores__c  =: fichaActual.Id AND ColorExterno__r.CodigoColor__c IN : coloresEliminados];
            
        }
        if(!coloresUpd.isEmpty()){
            for(Color_Externo_Seleccionado__c colEliminar : coloresUpd){
                colEliminar.Agregado__c = false;
                colEliminar.DPMS__c = false;
                
            }
            if(!coloresUpd.isEmpty()){
                update coloresUpd;        
            }
            
        }
        if(fichaActual != null){
            update fichaActual;    
        }
        
    }
    
    @AuraEnabled
    public static void agregaProductoVersion(String prodID, String verID){
        Producto_Version__c newVerProd = new Producto_Version__c(
            Producto__c = prodID,
            Version__c = verID
        );
        system.debug('version agregada'+newVerProd);
        try{
            insert newVerProd;
        }catch(exception e){
            system.debug('error gral'+e.getMessage());
        }
        
    }
    
    @AuraEnabled
    public static List<FichaProductoModelosWrapper> getModelosFichaProducto(String fichaProdID){
        List<FichaProductoModelosWrapper> retList = new List<FichaProductoModelosWrapper>();
        FichaColores__c fichaActual = FichaColoresController.getFichaActual(fichaProdID);
        Set<String> prodVersionesIDS = new Set<String>();
        
        
        if(fichaActual != null && fichaActual.Id != null){
            for(Versiones_Ficha_Colores__c vfc: FichaColoresController.getVersionesColores(fichaActual.Id)){
                for(Producto_Version__c pv: vfc.Productos_de_version__r){
                    prodVersionesIDS.add(pv.Producto__c);
                }
            }
        }
        
        
        for(CodigoModeloFichaTecnica__c cmft: [SELECT FichaTecnica__c, CodigoModeloVersion__c, CodigoModeloVersion__r.Name FROM CodigoModeloFichaTecnica__c WHERE FichaTecnica__c =:fichaProdID]){
            retList.add(new FichaProductoModelosWrapper(cmft.CodigoModeloVersion__c, cmft.CodigoModeloVersion__r.Name, prodVersionesIDS.contains(cmft.CodigoModeloVersion__c)));
        }
        
        return retList;
    }
    
    public class ColoresAgregadosWrapper{
        @AuraEnabled public ColorExterno__c colorExterno;
        @AuraEnabled public Boolean selected;
        
        public ColoresAgregadosWrapper(ColorExterno__c cExt, Boolean sel){
            this.colorExterno = cExt;
            this.selected = sel;
        }
    }
    
    
    public class FichaProductoModelosWrapper{
        @AuraEnabled public String modeloID;
        @AuraEnabled public String modeloName;
        @AuraEnabled public Boolean existeFichaColores;
        
        public FichaProductoModelosWrapper(String modId, String modName, Boolean exist){
            this.modeloID = modId;
            this.modeloName = modName;
            this.existeFichaColores = exist;
        }
    }
    
}