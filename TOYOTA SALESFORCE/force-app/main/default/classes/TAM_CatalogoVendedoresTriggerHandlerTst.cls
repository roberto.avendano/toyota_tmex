/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para la prueba TAM_CatalogoVendedoresTriggerHandler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    11-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_CatalogoVendedoresTriggerHandlerTst {

 	@TestSetup static void loadData(){

		TAM_CatalogoVendedores__c InventarioTransito = new TAM_CatalogoVendedores__c(
			Name='AHTZIRI JUAREZ JUAREZ .', 
			TAM_NoVendedor__c ='280', 
			TAM_NoDistribuidor__c='57001'
		);
	    upsert InventarioTransito TAM_CatalogoVendedores__c.TAM_NoVendedor__c;  
				             				
	}
    static testMethod void TAM_CatalogoVendedoresTriggerHandlerTstOK() {
		
		Test.startTest();
		
			TAM_CatalogoVendedores__c InventarioTransito = [Select id, Name From TAM_CatalogoVendedores__c LIMIT 1];  
			
		Test.stopTest();        
    }
}