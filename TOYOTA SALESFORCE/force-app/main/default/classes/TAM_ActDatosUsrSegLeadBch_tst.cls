/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto User
                        y actuelizarlos en el objeto de TAM_UsuariosSeguimientoLead__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    13-Mayo-202          Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActDatosUsrSegLeadBch_tst {

    static  Account Distrib = new Account();
    
    @TestSetup static void loadData(){
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '57000',
                TAM_GestorLeads__c = UserInfo.getUserId()
            );              
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id
            );
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive = true,
                ContactId = con.Id,
                timezonesidkey ='America/Los_Angeles', 
                username ='testOwnertfsproduc@noemail.com.test'
            );
            insert user;
        
	        TAM_MaximoNumeroLeads__c testCustum = new TAM_MaximoNumeroLeads__c();
	        testCustum.TAM_CodigoDealer__c = '57000';
	        testCustum.Id_Externo__c = '57000';
	        testCustum.TAM_MaximoLeads__c = '20';
	        insert testCustum;

            TAM_UsuariosSeguimientoLead__c testUsrSeg = new TAM_UsuariosSeguimientoLead__c();
            testUsrSeg.TAM_IdExterno__c = user.alias + '-' + '57000';
            testUsrSeg.TAM_IdUsuarioPartner__c = user.id;
            testUsrSeg.TAM_NoDistribuidor__c = '57000';
            insert testUsrSeg;
            
        Test.stopTest();

    }

    static testMethod void TAM_ActDatosUsrSegLeadBchOK() {
        System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK...');
        
        Test.startTest();

            Distrib = [Select id, Codigo_Distribuidor__c, TAM_GestorLeads__c From Account Limit 1];
            System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK Distrib: ' + Distrib);
            User usuarioPaso = [Select id, alias From User where email = 'test123tfsproduc@noemail.com'];
            System.debug('En TAM_ActivaUsrSegLeadMangCtrlOK usuarioPaso: ' + usuarioPaso);

            String sQuery = 'Select Id, Name, CodigoDistribuidor__c';
            sQuery += ' From User';
            sQuery += ' where CodigoDistribuidor__c != null and IsPortalEnabled = true';
            sQuery += ' And ID = \'' + String.escapeSingleQuotes(usuarioPaso.id) + '\'';
            sQuery += ' Order by id, Name';
            sQuery += ' Limit 1';           
            System.debug('EN TAM_ActFechaDDChkOutBchOK.execute sQuery: ' + sQuery);
            
            //Crea el objeto de  OppUpdEnvEmailBch_cls      
            TAM_ActDatosUsrSegLeadBch_cls objActDatosUsrSegLeadBch = new TAM_ActDatosUsrSegLeadBch_cls(sQuery);
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objActDatosUsrSegLeadBch, 1);
            
            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_ActDatosUsrSegLeadSch_cls objActDatosUsrSegLeadSch = new TAM_ActDatosUsrSegLeadSch_cls();
            System.schedule('Ejecuta_TAM_ActDatosUsrSegLeadSch_cls', sch, objActDatosUsrSegLeadSch);
 
        Test.stopTest();
        
    }
    
}