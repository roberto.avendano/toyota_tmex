public class SolicitudSiteUsuarioVController {
    public String uvId {get;set;}    
    public String retURL {get;set;}
    public SolicitudInternaVehiculos__c solicitud{get;set;}
    public ApexPages.StandardController uvController;
    public boolean showBotonCan{get;set;}
    public boolean showBotonGua{get;set;}
    public boolean showBotonEdi{get;set;}
    public boolean showLectura{get;set;}
    public boolean showEditar{get;set;}
    public Boolean alertaOKApp {get;set;}
    public String sivId{get;set;}
    public String usuariovID{get;set;}
    public String url{get;set;}
    public UsuarioVehiculo__c uv {get;set;}
    

    public boolean showAllFields{get;set;}
    public boolean showNuevoUsuario{get;set;}
    public boolean showExistenteUsuario{get;set;}
    public boolean showRegresarGuardado{get;set;}
    public boolean showCancelar{get;set;}
    public String picklistValue{get;set;}
    
    public SolicitudSiteUsuarioVController(){
        this.sivId =  ApexPages.currentPage().getParameters().get('id');
        this.url = ApexPages.currentPage().getURL();
        System.debug('URL UVEditar comonente controller: ' +url);
        System.debug('sivId ' +sivId);
        System.debug('usuariovID ' +usuariovID);
         solicitud = [SELECT Id,  Name, Solicitante__c FROM SolicitudInternaVehiculos__c  WHERE Id =:sivId];
        uv = new UsuarioVehiculo__c(Nombre__c = solicitud.Solicitante__c, TipoUsuarioSite__c = 'Nuevo Usuario');
         showNuevoUsuario = true;
    }
    
    public SolicitudSiteUsuarioVController(ApexPages.StandardController stdcontroller){
        retURL = ApexPages.currentPage().getParameters().get('prev');
        System.debug('retURL'+retURL);
        this.uvController = stdcontroller;
        uvId =stdcontroller.getId();
        showBotonEdi = true;
        showBotonGua = false;
        showBotonCan = false;
        showLectura = true;
        showEditar = false;
        showNuevoUsuario = true;
    }
    
    public PageReference editar(){
        showBotonCan = true;
        showBotonGua = true;
        showBotonEdi = false;
        showLectura = false;
        showEditar = true;
        return null;
    }
    
    public PageReference cancelarEdi(){
        showBotonCan = false;
        showBotonEdi = true;
        showBotonGua = false;
        showLectura = true;
        showEditar = false;
        return null;
    }
    
   /* public PageReference guardar(){
        try{
            update (UsuarioVehiculo__c)uvController.getRecord();
            showBotonCan = false;
            showBotonEdi = true;
            showBotonGua = false;
            showLectura = true;
            showEditar = false;
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
        return null;
    }*/
        public PageReference guardar(){
        PageReference page;   
             
        uv.SolicitudInternaVehiculos__c = sivId;
        try{
            insert uv;  
            showAllFields = false;
            showRegresarGuardado = true;
            showCancelar = false;
             solicitud = [SELECT Id,  Name, Solicitante__c FROM SolicitudInternaVehiculos__c  WHERE Id =:sivId];
       		 uv = new UsuarioVehiculo__c(Nombre__c = solicitud.Solicitante__c, TipoUsuarioSite__c = 'Nuevo Usuario');
         	showNuevoUsuario = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Usuario creado/registrado')); 
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        } 
        return null; 
    }
    
    public PageReference cerrar(){
        PageReference page;
        page = new PageReference(retURL);
        page.setRedirect(true);               
        return page;
    } 

    public List<UsuarioVehiculo__c> getUsuarioSiv(){
        return [SELECT Id, Name, NombreCompleto__c,RelacionEmpleado__c, RecogeVehiculo__c FROM UsuarioVehiculo__c WHERE SolicitudInternaVehiculos__c =:sivId];
    } 
    
    public PageReference nuevoUsuarioSiv(){
        PageReference page;
        //page = new PageReference('/SolicitudSiteUsuarioVEditar?id='+sivId+'&prev='+url);
        page = new PageReference('/SolicitudSiteNewUserV?idsolicitud='+sivId+'&prev='+url);
        page.setRedirect(false);
        return page;
    }
    
    public PageReference abrirUV(){
        PageReference page;
        page = new PageReference('/SolicitudSiteUsuarioVEditar?id='+usuariovID+'&prev='+url);
        //page = new PageReference('/SolicitudSiteUsuarioVEditar?id='+usuariovID);
        page.setRedirect(false);
        return page;
    }
    
    public PageReference mostrarCampos(){
        // Contacto Existente Nuevo Usuario
        showAllFields = true;
        showNuevoUsuario = picklistValue=='Nuevo Usuario'? true: false;
        showExistenteUsuario = pickListValue=='Contacto Existente'? true:false;
        return null;
    }
}