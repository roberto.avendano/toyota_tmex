/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para el objeto de estados para el servicio de Quiter

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    04-Marzo-2021        Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_QuiterWrpEstados {

    //Ahora Crea el llamado para la serializacion JSON.deserialize
    public List<QuiterEstados> states;    

    public class QuiterEstados{
        @AuraEnabled 
        public String id {get;set;}    
        @AuraEnabled 
        public String country {get;set;}    
        @AuraEnabled 
        public String description {get;set;}    
        @AuraEnabled 
        public String smallDescription {get;set;}    
                    
        //Un contructor por default
        public QuiterEstados(){
            this.description = '';
            this.id = ''; 
            this.country = '';
            this.smallDescription = '';
        }
        
        //Un contructor por default
        public QuiterEstados(String id, String country, String description, String smallDescription){
            this.id = id;
            this.country = country;   
            this.description = description;
            this.smallDescription = smallDescription;
        }
    }
    
}