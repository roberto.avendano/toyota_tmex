@isTest
private class AltaAutoDemoDistribuidorControllerTest {
    
    @TestSetup static void loadData(){
        
        
        //Create portal account owner
        List<String> validAdmins= new List<String>{'Administrador del sistemaAD', 'Administrador del sistema', 'System Administrator'};
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile admin = [Select Id From Profile Where Name IN:validAdmins LIMIT 1];
        
        
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = admin.Id,
            Username = 'ToyotaAdminPartner@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
    }
    
    
    @TestSetup static void loadData2(){
        List<InventarioWholesale__c> wholesale = new List<InventarioWholesale__c>();
        
        for(Integer i=0; i<50; i++){			
            String sufix = i<10? '0'+String.valueOf(i):String.valueOf(i);  
            
            wholesale.add(new InventarioWholesale__c(
                Name = 'JTDKBRFUXH30310'+sufix,
                Model_Year__c = '2017',            		
                Dealer_Code__c = '57055',
                Exterior_Color_Description__c ='',
                Exterior_Color_Code__c = String.valueOf(i),
                Interior_Color_Description__c = '',
                Interior_Color_Code__c = String.valueOf(i),
                Toms_Series_Name__c = '',
                Model_Number__c = String.valueOf(AltaAutoDemoDistribuidorControllerTest.randomWithMax(10))
            )); 			 				 				 		      
        }
        
        insert wholesale;
        
        //Inventario 2
        InventarioWholesale__c invTest = new InventarioWholesale__c();
        invTest.name = 'JTDKBRFUXH30311';
        invTest.Model_Year__c = '2017';         		
        invTest.Dealer_Code__c = '57055';
        invTest.Exterior_Color_Description__c ='';
        invTest.Exterior_Color_Code__c = 'test';
        invTest.Interior_Color_Description__c = '';
        invTest.Interior_Color_Code__c = 'test';
        invTest.Toms_Series_Name__c = '';
        invTest.Model_Number__c = '21';
        insert invTest;
        
        //Serie
        Serie__c serieTest = new Serie__c();
        serieTest.name = 'TEST';
        serieTest.Marca__c = 'Toyota';
        insert serieTest;
        
        //Producto
        Product2 prodTest = new Product2();
        prodTest.Anio__c = '2017';
        prodTest.ProductCode = '5';
        prodTest.IdExternoProducto__c = '52017'; 
        prodTest.Name = '52017'; 
        prodTest.Serie__c = serieTest.Id;
        insert prodTest;
    }
    
    
    public static Integer randomWithMax(Integer max){
        Integer rand = 5;
        return Math.mod(rand, max);
    }
    
    
    @isTest static void test_one() {
        Test.startTest();			
        User testAcc = [SELECT Id, Name, Alias FROM User WHERE Alias='batman' LIMIT 1];
        
        Account portalAccount1 = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '57055',
            OwnerId = testAcc.Id,
            UnidadesAutosDemoAutorizadas__c = 5
        );
        insert portalAccount1;
        
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        
        Profile p = [Select Id, Name From Profile Where Name='Gerente de Ventas Distribuidor' LIMIT 1];
        User user1 = new User(
            Username = 'toyotaPartnerU@test.com',
            ContactId = contact1.Id,
            ProfileId = p.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        insert user1;
        
        System.runAs(user1){
            
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(new SolicitudAutoDemo__c());
            AltaAutoDemoDistribuidorController extCtrl = new AltaAutoDemoDistribuidorController(stdcontroller);
            extCtrl.camposBusqueda.vin = 'J';
            extCtrl.buscarFiltros();
            extCtrl.anterior();
            extCtrl.siguiente();				
            extCtrl.help.VIN_Name__c = 'JTDKBRFUXH3031005';				
            
            List<InventarioWholesale__c> misVines = extCtrl.getVines();
            //System.debug(JSON.serialize(misVines));
            extCtrl.camposBusqueda.selectedWholeSale = misVines[0].Id;
            extCtrl.guardar();
        }			
        Test.stopTest();
    }

    
}