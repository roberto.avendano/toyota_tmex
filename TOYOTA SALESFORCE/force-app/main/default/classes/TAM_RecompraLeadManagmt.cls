/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladora para el componente TAM_RecompraLeadManagmt

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_RecompraLeadManagmt {

    public static String VaRtLeadRtPrecalificado = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cotización TFS').getRecordTypeId();
    public static String VaRtLeadRtCotizaTmex = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Cotización.').getRecordTypeId();
    public static String VaRtLeadRtBdc = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('BDC').getRecordTypeId();

    public class wrpDatosRecompraLead{
        @AuraEnabled public Lead objRecompraLead {get;set;}    
        @AuraEnabled public Boolean blnFacturadoEntregado {get;set;}    
        
        //Un contructor por default
        public wrpDatosRecompraLead(){
            this.objRecompraLead = new Lead();
            this.blnFacturadoEntregado = false;
        }
        
        //Un contructor por default
        public wrpDatosRecompraLead(Lead objRecompraLead, Boolean blnFacturadoEntregado){
            this.objRecompraLead = objRecompraLead;
            this.blnFacturadoEntregado = blnFacturadoEntregado;
        }
    }
    

    //Obtener getDatosLead.
    @AuraEnabled
    public static wrpDatosRecompraLead getDatosLead(String recordId) {
        System.debug('EN getDatosLead recordId: ' + recordId);

        wrpDatosRecompraLead objWrpDatosRecompraLead = new wrpDatosRecompraLead();
        SObject sObjLeadClone;
        Lead objLeadPaso = new Lead();
        objWrpDatosRecompraLead.objRecompraLead = objLeadPaso;
        objWrpDatosRecompraLead.blnFacturadoEntregado = false;
        
        //Consulta todos los reistros que estan en estapa de Pedido en Proceso
        for (Lead candidatoCons : [Select Id, Name, FWY_ID_de_cliente__c, FirstName, LastName, FWY_ApellidoMaterno__c, TAM_RazonSocial__c, 
            FWY_Tipo_de_persona__c, TAM_RFC__c, Phone, MobilePhone, Email, TAM_FormaContactoPreferida__c, LeadSource, Status,
            FWY_Estado_del_candidatoWEB__c, TAM_ConfirmaDatos__c, FWY_Vehiculo__c, TAM_Cantidad__c, TAM_AplicaBDC__c, TAM_ComentariosBDC__c, 
            TAM_BdcContactado__c, TAM_BdcMetodoContacto__c, TAM_FechaActividadContac__c, FWY_Comentarios__c, RecordTypeId, TAM_AplicaBdcChk__c,
            TAM_BdcGenerada__c, TAM_ComentariosActBdcCitaGen__c, TAM_BdcConcretada__c, TAM_EmailAdicional__c, RecordType.Name, 
            TAM_TelefonoAdicional__c, TAM_PruebaManejo__c, TAM_FechaActividadPrueMan__c, TAM_ComentariosActPruMan__c,
            TAM_SeEntregoCotizacion__c, Forma_de_adquisici_n__c, TAM_Banco__c, TAM_FechaActividadCot__c, TAM_ComentariosActCot__c,
            TAM_ProspectoCaliente__c, TAM_FechaActividadCteCal__c, TAM_ComentariosCteCal__c, 
            TAM_DejoApartadoEnganche__c, TAM_CuentaInventarioDisponible__c, TAM_AutoCuenta__c, TAM_Marca__c,
            TAM_EsCertificableComoNuevo__c, TAM_FechaActividadPedProc__c, TAM_EntregoDocumentos__c, 
            TAM_FormaPagoDefinitiva__c, TAM_BancoForPagoFinal__c, TAM_ArrendadoraFormPagoFinal__c,
            TAM_ComentariosActPedProc__c, TAM_PedidoFacturado__c, TAM_FechaFacturacionDealer__c,
            TAM_PedidoReportado__c, TAM_FechaEntrega__c, TAM_FechaEntregado__c, TAM_FechaFacturado__c
            From Lead Where id =: recordId And (Status = 'Facturado' OR Status = 'Entregado')]){
            //Clona el objeto de la consulta candidatoCons
            sObjLeadClone = candidatoCons.clone(false, false, false, false);
            System.debug('EN getDatosLead sObjLeadClone: ' + sObjLeadClone);
        }       
        
        //Inicializa el objLeadPaso con candidatoCons
        objLeadPaso = (Lead) sObjLeadClone;
        //Actualiza los datos del objeto objWrpDatosRecompraLead
        objWrpDatosRecompraLead.objRecompraLead = objLeadPaso;
        objWrpDatosRecompraLead.blnFacturadoEntregado = objLeadPaso != null ? (objLeadPaso.Name != null ? true : false) : false;
        
        System.debug('EN getDatosLead objWrpDatosRecompraLead: ' + objWrpDatosRecompraLead);
        return objWrpDatosRecompraLead;
    
    }    

    //Obtener getDatosLead.
    @AuraEnabled
    public static TAM_WrpResultadoActualizacion creaNuevoProspecto(String recordId, 
        wrpDatosRecompraLead DatosLeadRecompraPrm, String strEstadoNvoCandidatoPrm) {
        System.debug('EN getDatosLead recordId: ' + recordId + ' DatosLeadRecompraPrm: ' + DatosLeadRecompraPrm + ' strEstadoNvoCandidatoPrm: ' + strEstadoNvoCandidatoPrm);

        TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
        objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se actualizarón con exito.');        

        //Limpia los campos que no se requieren en este momento.
        Lead objLeadFinalRecompra = DatosLeadRecompraPrm.objRecompraLead;
        System.debug('EN getDatosLead NOMBRE: ' + objLeadFinalRecompra.FirstName + ' AP: ' + objLeadFinalRecompra.LastName + ' AM: ' + objLeadFinalRecompra.FWY_ApellidoMaterno__c + ' objLeadFinalRecompra: ' + objLeadFinalRecompra.TAM_FormaContactoPreferida__c + ' FWY_Vehiculo__c: ' + objLeadFinalRecompra.FWY_Vehiculo__c + ' RecordTypeId: ' + objLeadFinalRecompra.RecordTypeId);
        
        //El lead final 
        Lead objLeadFinalRecompraFinal = new Lead();
        
        objLeadFinalRecompraFinal.RecordTypeId = objLeadFinalRecompra.RecordTypeId;
        objLeadFinalRecompraFinal.TAM_TipoCandidato__c = 'Retail Nuevos';
        objLeadFinalRecompraFinal.FirstName = objLeadFinalRecompra.FirstName;
        objLeadFinalRecompraFinal.LastName = objLeadFinalRecompra.LastName;
        objLeadFinalRecompraFinal.FWY_ApellidoMaterno__c = objLeadFinalRecompra.FWY_ApellidoMaterno__c;
        objLeadFinalRecompraFinal.TAM_RazonSocial__c = objLeadFinalRecompra.TAM_RazonSocial__c;
        objLeadFinalRecompraFinal.FWY_Tipo_de_persona__c = objLeadFinalRecompra.FWY_Tipo_de_persona__c;
        objLeadFinalRecompraFinal.TAM_RFC__c = objLeadFinalRecompra.TAM_RFC__c;
        objLeadFinalRecompraFinal.Phone = objLeadFinalRecompra.Phone;
        objLeadFinalRecompraFinal.MobilePhone = objLeadFinalRecompra.MobilePhone;
        objLeadFinalRecompraFinal.Email = objLeadFinalRecompra.Email;
        objLeadFinalRecompraFinal.TAM_FormaContactoPreferida__c = objLeadFinalRecompra.TAM_FormaContactoPreferida__c;
        objLeadFinalRecompraFinal.LeadSource = objLeadFinalRecompra.LeadSource;
        objLeadFinalRecompraFinal.Status = objLeadFinalRecompra.Status;
        objLeadFinalRecompraFinal.FWY_Estado_del_candidatoWEB__c = objLeadFinalRecompra.FWY_Estado_del_candidatoWEB__c;
        objLeadFinalRecompraFinal.TAM_ConfirmaDatos__c = objLeadFinalRecompra.TAM_ConfirmaDatos__c;
        objLeadFinalRecompraFinal.FWY_Vehiculo__c = objLeadFinalRecompra.FWY_Vehiculo__c;
        objLeadFinalRecompraFinal.TAM_Cantidad__c = objLeadFinalRecompra.TAM_Cantidad__c;

        objLeadFinalRecompraFinal.TAM_AplicaBDC__c = objLeadFinalRecompra.TAM_AplicaBDC__c;
        objLeadFinalRecompraFinal.TAM_AplicaBdcChk__c = objLeadFinalRecompra.TAM_AplicaBdcChk__c;
        objLeadFinalRecompraFinal.TAM_ComentariosBDC__c = objLeadFinalRecompra.TAM_ComentariosBDC__c;
        objLeadFinalRecompraFinal.TAM_BdcContactado__c = objLeadFinalRecompra.TAM_BdcContactado__c;
        objLeadFinalRecompraFinal.TAM_BdcMetodoContacto__c = objLeadFinalRecompra.TAM_BdcMetodoContacto__c;
        objLeadFinalRecompraFinal.TAM_FechaActividadContac__c = objLeadFinalRecompra.TAM_FechaActividadContac__c;
        objLeadFinalRecompraFinal.FWY_Comentarios__c = objLeadFinalRecompra.FWY_Comentarios__c;
        objLeadFinalRecompraFinal.TAM_BdcGenerada__c = objLeadFinalRecompra.TAM_BdcGenerada__c;
        objLeadFinalRecompraFinal.TAM_ComentariosActBdcCitaGen__c = objLeadFinalRecompra.TAM_ComentariosActBdcCitaGen__c;
        objLeadFinalRecompraFinal.TAM_BdcConcretada__c = objLeadFinalRecompra.TAM_BdcConcretada__c;
        objLeadFinalRecompraFinal.TAM_EmailAdicional__c = objLeadFinalRecompra.TAM_EmailAdicional__c;
        objLeadFinalRecompraFinal.TAM_TelefonoAdicional__c = objLeadFinalRecompra.TAM_TelefonoAdicional__c;
        objLeadFinalRecompraFinal.TAM_PruebaManejo__c = objLeadFinalRecompra.TAM_PruebaManejo__c;
        objLeadFinalRecompraFinal.TAM_FechaActividadPrueMan__c = objLeadFinalRecompra.TAM_FechaActividadPrueMan__c;
        objLeadFinalRecompraFinal.TAM_ComentariosActPruMan__c = objLeadFinalRecompra.TAM_ComentariosActPruMan__c;

        objLeadFinalRecompraFinal.TAM_SeEntregoCotizacion__c = objLeadFinalRecompra.TAM_SeEntregoCotizacion__c;
        objLeadFinalRecompraFinal.TAM_TareaCotizacion__c = objLeadFinalRecompra.TAM_TareaCotizacion__c;
        objLeadFinalRecompraFinal.Forma_de_adquisici_n__c = objLeadFinalRecompra.Forma_de_adquisici_n__c;
        objLeadFinalRecompraFinal.TAM_Banco__c = objLeadFinalRecompra.TAM_Banco__c;
        objLeadFinalRecompraFinal.TAM_FechaActividadCot__c = objLeadFinalRecompra.TAM_FechaActividadCot__c;
        objLeadFinalRecompraFinal.TAM_ComentariosActCot__c = objLeadFinalRecompra.TAM_ComentariosActCot__c;
                
        //Si no tiene etapa de prospecto caliente
        if (objLeadFinalRecompra.RecordTypeId == VaRtLeadRtCotizaTmex){        
            objLeadFinalRecompraFinal.TAM_ProspectoCaliente__c = true;
            objLeadFinalRecompraFinal.TAM_FechaActividadCteCal__c = Date.today();
            objLeadFinalRecompraFinal.TAM_ComentariosCteCal__c = 'Prospecto caliente Recompra';
            objLeadFinalRecompraFinal.TAM_TareaProspectoCaliente__c = true;
        }//Fin si !objLeadFinalRecompra.TAM_ProspectoCaliente__c
        
        //Limpia los campos que no sirven
        objLeadFinalRecompraFinal.TAM_TareaPedidoProceso__c = false;
        objLeadFinalRecompraFinal.TAM_DejoApartadoEnganche__c = false;
        objLeadFinalRecompraFinal.TAM_CuentaInventarioDisponible__c = null;
        objLeadFinalRecompraFinal.TAM_AutoCuenta__c = false;
        objLeadFinalRecompraFinal.TAM_Marca__c = null;
        objLeadFinalRecompraFinal.TAM_EsCertificableComoNuevo__c = null;
        objLeadFinalRecompraFinal.TAM_FechaActividadPedProc__c = null;
        objLeadFinalRecompraFinal.TAM_EntregoDocumentos__c = false;
        objLeadFinalRecompraFinal.TAM_FormaPagoDefinitiva__c = null;
        objLeadFinalRecompraFinal.TAM_BancoForPagoFinal__c = null;
        objLeadFinalRecompraFinal.TAM_ArrendadoraFormPagoFinal__c = null;
        objLeadFinalRecompraFinal.TAM_ComentariosActPedProc__c = null;
        objLeadFinalRecompraFinal.TAM_PedidoFacturado__c = false;
        objLeadFinalRecompraFinal.TAM_FechaFacturacionDealer__c = null;
        objLeadFinalRecompraFinal.TAM_PedidoReportado__c = false;
        objLeadFinalRecompraFinal.TAM_FechaEntrega__c = null;
        objLeadFinalRecompraFinal.TAM_FechaEntregado__c = null;
        objLeadFinalRecompraFinal.TAM_FechaFacturado__c = null;
        
        //Es TMEX
        if (objLeadFinalRecompra.RecordTypeId == VaRtLeadRtCotizaTmex){        
            System.debug('EN getDatosLead objLeadFinalRecompraFinal ENTRO A TMEX...');
            objLeadFinalRecompraFinal.Status = strEstadoNvoCandidatoPrm;
            objLeadFinalRecompraFinal.FWY_Estado_del_candidatoWEB__c = strEstadoNvoCandidatoPrm;
        }//Fin si objLeadFinalRecompra.RecordTypeId == VaRtLeadRtCotizaTmex
        
        //Es TFS
        if (objLeadFinalRecompra.RecordTypeId == VaRtLeadRtPrecalificado){        
            System.debug('EN getDatosLead objLeadFinalRecompraFinal ENTRO A TFS...');
            objLeadFinalRecompraFinal.Status = 'Cotización';
            objLeadFinalRecompraFinal.FWY_Estado_del_candidatoWEB__c = 'Cotización';
            
            objLeadFinalRecompraFinal.TAM_Precalificado__c = false;
            objLeadFinalRecompraFinal.TAM_TareaPrecalificado__c = false;
            objLeadFinalRecompraFinal.Resultado_PreCalificacion__c = null;
            objLeadFinalRecompraFinal.TAM_FechaActividadCtePrecal__c = null;
            objLeadFinalRecompraFinal.TAM_ComentariosActPrecal__c = null;
            
            objLeadFinalRecompraFinal.TAM_SolicitudTFS__c = false;
            objLeadFinalRecompraFinal.TAM_TareaSolicitud__c = false;
            objLeadFinalRecompraFinal.TAM_EstatusSolicitudTfs__c = null;
            objLeadFinalRecompraFinal.TAM_FechaActividadSol__c = null;
            objLeadFinalRecompraFinal.TAM_ComentariosActSol__c = null;
            
            objLeadFinalRecompraFinal.TAM_ProspectoCaliente__c = false;
            objLeadFinalRecompraFinal.TAM_TareaProspectoCaliente__c = false;
            objLeadFinalRecompraFinal.TAM_FechaActividadCteCal__c = null;
            objLeadFinalRecompraFinal.TAM_ComentariosCteCal__c = null;
        }//Fin si objLeadFinalRecompra.RecordTypeId == VaRtLeadRtCotizaTmex
        
        objLeadFinalRecompraFinal.TAM_Recompra__c = true;
        System.debug('EN getDatosLead objLeadFinalRecompraFinal: ' + objLeadFinalRecompraFinal);
        
        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        
        
        Database.SaveResult dtSvInsLead = Database.insert(objLeadFinalRecompraFinal, false);
        if (dtSvInsLead.isSuccess()){
            objRespoPaso.strParamOpcional2 = dtSvInsLead.getId();
            System.debug('EN getDatosLead objRespoPaso: ' + objRespoPaso);
        }
        if (!dtSvInsLead.isSuccess()){            
            //objRespoPaso.strDetalle = dtSvInsLead.getErrors()[0].getMessage();
            objRespoPaso = new TAM_WrpResultadoActualizacion(true, dtSvInsLead.getErrors()[0].getMessage());            
            System.debug('EN getDatosLead objRespoPaso: ' + objRespoPaso);
        }
        
        //Roleback a todo
        //Database.rollback(sp);

        //Regresa el objeto del tipo objRespoPaso
        return objRespoPaso;
    }
            
}