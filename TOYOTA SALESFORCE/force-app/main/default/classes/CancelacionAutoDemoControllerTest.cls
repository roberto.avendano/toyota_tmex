@isTest
private class CancelacionAutoDemoControllerTest {
	
	@TestSetup static void loadData() {
		Schema.DescribeSObjectResult vinSchema = Schema.SObjectType.Vehiculo__c;
		Schema.RecordTypeInfo rtVin = vinSchema.getRecordTypeInfosByName().get('Vehículo');

		List<Vehiculo__c> newVehicles = new List<Vehiculo__c>();
		List<Account> newAccounts = new List<Account>();
		List<SolicitudAutoDemo__c> newSolicitudes = new List<SolicitudAutoDemo__c>();

		for(Integer i=0; i<10;i++){
			newAccounts.add(new Account(
				Name='Account'+i,
				UnidadesAutosDemoAutorizadas__c = 15
			));			
		}		
		insert newAccounts;

		for(Integer j=0; j<20; j++){			
		 	Integer rand = CancelacionAutoDemoControllerTest.randomWithMax(newAccounts.size());
		 	newVehicles.add(new Vehiculo__c(
	            Name='VIN'+j,          
	            Distribuidor__c=newAccounts.get(rand).Id,	            
	            RecordTypeId= rtVin.recordTypeId
	        ));	        
		}
		insert newVehicles;
		System.debug('-------->>> newVehicles'+newVehicles.size());
		for(Integer k=0; k<5; k++){
			Integer randVIN = CancelacionAutoDemoControllerTest.randomWithMax(newVehicles.size());
			newSolicitudes.add(new SolicitudAutoDemo__c(
				Estatus__c = Constantes.AUTO_DEMO_SOLICITUD_ALTA,
				VIN__c = newVehicles.get(k).Id
			));
		}
		insert newSolicitudes;
	}

	public static Integer randomWithMax(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}
	
	@isTest static void test_one() {
		ApexPages.StandardController stdController = new ApexPages.StandardController(new SolicitudAutoDemo__c());
		CancelacionAutoDemoController extController = new CancelacionAutoDemoController(stdController);
		List<SolicitudAutoDemo__c> solicitudes = extController.getSolicitudes();
		extController.anterior();
		extController.siguiente();
		extController.solicitudID = solicitudes.get(CancelacionAutoDemoControllerTest.randomWithMax(solicitudes.size())).Id;
		extController.solicitudEdit();
	}
	
}