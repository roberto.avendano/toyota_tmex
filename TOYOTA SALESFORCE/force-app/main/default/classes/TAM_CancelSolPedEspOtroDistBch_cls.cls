/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto 
                        TAM_SolicitudDealerDestino__c y cancelar las soicitudes 
                        que el usuario final no ha enviado a autorizar.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    07-Agosto-2021       Héctor Figueroa             Creación
******************************************************************************* */

global without sharing class TAM_CancelSolPedEspOtroDistBch_cls implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    global String sQuery {get;set;}

    //Un constructor por default
    global TAM_CancelSolPedEspOtroDistBch_cls(string query){
        this.sQuery = query;
    }
    
    //Start
    global Database.querylocator start(Database.BatchableContext BC){
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls.start sQuery: ' + this.sQuery);
        return Database.getQueryLocator(this.sQuery);
    }
    
    //Execute
    global void execute(Database.BatchableContext BC, List<TAM_SolicitudDealerDestino__c> scope){ 
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls.');

        Map<String, List<TAM_DistribuidoresFlotillaPrograma__c>> mapDistrEntrega = new Map<String, List<TAM_DistribuidoresFlotillaPrograma__c>>();
        Map<String, TAM_SolicitudDealerDestino__c> mapDistFlotProg = new Map<String, TAM_SolicitudDealerDestino__c>();
        Map<String, TAM_SolicitudDealerDestino__c> mapSolDealerDestinoCancel = new Map<String, TAM_SolicitudDealerDestino__c>();
        Map<String, TAM_DistribuidoresFlotillaPrograma__c> mapDistFlotProgCancel = new Map<String, TAM_DistribuidoresFlotillaPrograma__c>();

        Map<String, Set<String>> mapSolDealer = new Map<String, Set<String>>();
        Set<String> setDealers = new Set<String>();        
        Map<String, Set<String>> mapDistFlotProgPaso = new Map<String, Set<String>>();
        Map<String, DateTime> mapSolDealerFechaCancel = new Map<String, Datetime>();

        Map<String, TAM_CheckOutDetalleSolicitudCompra__c> mapCheckOutCancel = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
        Map<String, TAM_DODSolicitudesPedidoEspecial__c> mapDodCancel = new Map<String, TAM_DODSolicitudesPedidoEspecial__c>();

        //Recorre los registros del objeto  TAM_SolicitudDealerDestino__c y ve cuales no estan autorizados o cancelados
        for (TAM_SolicitudDealerDestino__c objDistFlotProgSol : scope){
            //Ve si el estatus es Pendiente
            if (objDistFlotProgSol.TAM_Estatus__c == 'Pendiente' || objDistFlotProgSol.TAM_Estatus__c == 'En proceso de cancelación'){
                mapDistFlotProg.put(objDistFlotProgSol.TAM_IdExterno__c, objDistFlotProgSol);
                String[] arrIdExter = objDistFlotProgSol.TAM_IdExterno__c.split('-');
                String sIdSolDealer = arrIdExter[0] + '-' + arrIdExter[1];
                String sIdExterDealer = arrIdExter[2] + '-' + arrIdExter[3];
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls arrIdExter: ' + arrIdExter + ' sIdSolDealer: ' + sIdSolDealer + ' sIdExterDealer: ' + sIdExterDealer);
                //Metelo al mapa de sIdExterDealer
                if (mapDistFlotProgPaso.containsKey(sIdSolDealer.trim()))
                    mapDistFlotProgPaso.get(sIdSolDealer.trim()).add(sIdExterDealer.trim());
                if (!mapDistFlotProgPaso.containsKey(sIdSolDealer.trim()))
                    mapDistFlotProgPaso.put(sIdSolDealer.trim(), new set<String>{sIdExterDealer.trim()});
                //Mete la sol y su fecha de cancel al mapa de mapSolDealerFechaCancel
                mapSolDealerFechaCancel.put(sIdSolDealer.trim(), objDistFlotProgSol.TAM_FechaCancelacion__c);
            }//Fin si objDistFlotProgSol.TAM_Estatus__c == 'Pendiente' || objDistFlotProgSol.TAM_Estatus__c == 'En proceso de cancelación'
        }//Fin del for para scope
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistFlotProgPaso: ' + mapDistFlotProgPaso.keyset());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistFlotProgPaso: ' + mapDistFlotProgPaso.values());

        //Seleciona los distribuidires asociados a la solicutud :
        for (TAM_DistribuidoresFlotillaPrograma__c objDistrRecibe : [Select id,
            t.TAM_DetalleSolicitudCompra_FLOTILLA__c, t.TAM_SolicitudFlotillaPrograma__c,
            t.TAM_SolicitudFlotillaPrograma__r.Name, t.TAM_IdExterno__c, t.TAM_Cantidad__c, 
            t.TAM_Cuenta__c, t.TAM_Cuenta__r.Name, t.TAM_Cuenta__r.Codigo_Distribuidor__c,
            t.TAM_NombreDistribuidor__c, t.TAM_IdDistribuidor__c
            From TAM_DistribuidoresFlotillaPrograma__c t 
            Where TAM_SolicitudFlotillaPrograma__r.Name IN : mapDistFlotProgPaso.keySet()
            ]){
            String sFolSol = objDistrRecibe.TAM_SolicitudFlotillaPrograma__r.Name;
            String sIdExterDealer = objDistrRecibe.TAM_IdDistribuidor__c + '-' + objDistrRecibe.TAM_NombreDistribuidor__c;
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls sFolSol: '+ sFolSol);
            //Toma los datos del folio de la sol y buscalo en el mapa de mapDistFlotProg
            if (mapDistFlotProgPaso.containsKey(sFolSol)){
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls VAL 1 sFolSol: '+ sFolSol + ' sIdExterDealer: ' + sIdExterDealer);
                if (mapDistFlotProgPaso.get(sFolSol).contains(sIdExterDealer)){
                    System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls VAL 2 sIdExterDealer: ' + sIdExterDealer);
		            //Metelo al mapa de Map<String, List<TAM_DistribuidoresFlotillaPrograma__c>> mapDistrEntrega
		            if (mapDistrEntrega.containsKey(objDistrRecibe.TAM_SolicitudFlotillaPrograma__c))
		                mapDistrEntrega.get(objDistrRecibe.TAM_SolicitudFlotillaPrograma__c).add(objDistrRecibe);
		            if (!mapDistrEntrega.containsKey(objDistrRecibe.TAM_SolicitudFlotillaPrograma__c))
		                mapDistrEntrega.put(objDistrRecibe.TAM_SolicitudFlotillaPrograma__c, new List<TAM_DistribuidoresFlotillaPrograma__c>{objDistrRecibe});    
                    //Agregalo al mapa de mapDistFlotProgCancel para cancelar 
                    mapDistFlotProgCancel.put(objDistrRecibe.id, new TAM_DistribuidoresFlotillaPrograma__c(
                            id = objDistrRecibe.id, TAM_Estatus__c = 'Cancelado'
                        )
                    );
                }//Fin si mapDistFlotProg.get(sFolSol).contains(sIdExterDealer)
            }//Fin si mapDistFlotProg.containsKey(sFolSol)
        }//Fin del for para TAM_DistribuidoresFlotillaPrograma__c
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistrEntrega '+ mapDistrEntrega.keySet());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistrEntrega '+ mapDistrEntrega.Values());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistFlotProgCancel '+ mapDistFlotProgCancel.keySet());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistFlotProgCancel '+ mapDistFlotProgCancel.Values());
        
        //Recorre los reg del mapa objDistFlotProg
        for (TAM_SolicitudDealerDestino__c objDistFlotProgCancel : mapDistFlotProg.values()){
            mapSolDealerDestinoCancel.put(objDistFlotProgCancel.id, new TAM_SolicitudDealerDestino__c(
                    id = objDistFlotProgCancel.id, TAM_Estatus__c = 'Procesada'
                    //, TAM_CanceldaProceso__c = true 
                )
            );
            String sIdExtrDist = objDistFlotProgCancel.TAM_DealerDestino__c;
            String sIdExterSol = objDistFlotProgCancel.Name;
            //Agrega el id de la sol y el nombre del dist objSolDealer
            if (mapSolDealer.containsKey(sIdExterSol))
                mapSolDealer.get(sIdExterSol).add(sIdExtrDist);
            if (!mapSolDealer.containsKey(sIdExterSol))
                mapSolDealer.put(sIdExterSol, new Set<string>{sIdExtrDist});
            //Agrega el deaeler al set de setDealers
            setDealers.add(sIdExtrDist);                
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls sIdExtrDist: ' + sIdExtrDist + ' ' + sIdExterSol);        
        }//Fin del for para objDistFlotProg.values()
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapSolDealerDestinoCancel: ' + mapSolDealerDestinoCancel.keyset());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapSolDealerDestinoCancel: ' + mapSolDealerDestinoCancel.values());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapSolDealer: ' + mapSolDealer.keyset());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapSolDealer: ' + mapSolDealer.values());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls setDealers: ' + setDealers);
        
        //Consulta en el Checkout los datos de las solocitudes y los modelos
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutCancel : [Select id, TAM_DistribuidorEntrega__c, 
            TAM_SolicitudFlotillaPrograma__r.Name, TAM_SolicitudFlotillaPrograma__c 
            From TAM_CheckOutDetalleSolicitudCompra__c 
            Where TAM_SolicitudFlotillaPrograma__r.Name IN : mapSolDealer.keyset()
            And TAM_DistribuidorEntrega__c IN :setDealers ]){
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls objCheckOutCancel: ' + objCheckOutCancel);
            //Toma los modeños asociados a la solicitud     
            Set<String> setDealersDest = mapSolDealer.containsKey(objCheckOutCancel.TAM_SolicitudFlotillaPrograma__r.Name) ? mapSolDealer.get(objCheckOutCancel.TAM_SolicitudFlotillaPrograma__r.Name) : new Set<String>();
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls TAM_DistribuidorEntrega__c: ' + objCheckOutCancel.TAM_DistribuidorEntrega__c);
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls setDealersDest: ' + setDealersDest);
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls objCheckOutCancel: ' + objCheckOutCancel.TAM_SolicitudFlotillaPrograma__r.Name);
            //Recorre la lista de Dealers
            for (String setDealersDestPaso : setDealersDest){
                if (setDealersDestPaso == objCheckOutCancel.TAM_DistribuidorEntrega__c){
		            //Agregalo al mapa de mapCheckOutCancel
		            mapCheckOutCancel.put(objCheckOutCancel.id, new TAM_CheckOutDetalleSolicitudCompra__c(
		                    id = objCheckOutCancel.id, TAM_EstatusDealerSolicitud__c = 'Cancelada', TAM_EstatusDOD__c = 'Cancelado', 
		                    TAM_FechaCancelacion__c = mapSolDealerFechaCancel.get(objCheckOutCancel.TAM_SolicitudFlotillaPrograma__r.Name).date()
		                )
		            );
                }//Fin si setDealersDestPaso == objCheckOutCancel.TAM_DistribuidorEntrega__c
            }//Fin del for para setDealersDest
        }//Fin del for para objDistFlotProg.values()
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapCheckOutCancel: ' + mapCheckOutCancel.keyset());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapCheckOutCancel: ' + mapCheckOutCancel.values());
        
        //Consulta en el Dod los datos de las solocitudes y los modelos
        for (TAM_DODSolicitudesPedidoEspecial__c objDodCancel : [Select id, TAM_Entrega__c, TAM_SolicitudFlotillaPrograma__c,
            TAM_SolicitudFlotillaPrograma__r.Name From TAM_DODSolicitudesPedidoEspecial__c 
            Where TAM_SolicitudFlotillaPrograma__r.Name IN : mapSolDealer.KeySet()
            And TAM_Entrega__c IN :setDealers]){
            //Toma los modeños asociados a la solicitud     
            Set<String> setDealersDest = mapSolDealer.containsKey(objDodCancel.TAM_SolicitudFlotillaPrograma__r.Name) ? mapSolDealer.get(objDodCancel.TAM_SolicitudFlotillaPrograma__r.Name) : new Set<String>() ;
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls TAM_DistribuidorEntrega__c: ' + objDodCancel.TAM_Entrega__c);
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls setDealersDest: ' + setDealersDest);
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls objDodCancel: ' + objDodCancel.TAM_SolicitudFlotillaPrograma__r.Name);
            //Recorre la lista de Dealers
            for (String setDealersDestPaso : setDealersDest){
                if (setDealersDestPaso == objDodCancel.TAM_Entrega__c){                    
                    //Agregalo al mapa de mapCheckOutCancel
                    mapDodCancel.put(objDodCancel.id, new TAM_DODSolicitudesPedidoEspecial__c(
                            id = objDodCancel.id, TAM_Estatus__c = 'Cancelado', TAM_Observaciones__c = 'cancelado por sistema el día 15 del mes, por no ser aprobado por el Dealer de destino', 
                            TAM_FechaCancelacion__c = mapSolDealerFechaCancel.get(objDodCancel.TAM_SolicitudFlotillaPrograma__r.Name).Date()
                        )
                    );
                }//Fin si setDealersDestPaso == objDodCancel.TAM_DistribuidorEntrega__c
            }//Fin del for para setDealersDest
        }//Fin del for para objDistFlotProg.values()
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDodCancel: ' + mapDodCancel.keyset());
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDodCancel: ' + mapDodCancel.values());
        
        Boolean bErrror = false;
        //Ya tienes todos los registros que tienes que cancelar 
        Savepoint sp = Database.setSavepoint();        

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsRes = Database.update(mapSolDealerDestinoCancel.values(), false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess()){
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls ERROR en TAM_SolicitudDealerDestino__c : ' + objDtbUpsRes.getErrors()[0].getMessage());
                bErrror = true;
            }
            if (objDtbUpsRes.isSuccess())
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapSolDealerDestinoCancel: ' + objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes                    

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsRes2 = Database.update(mapDistFlotProgCancel.values(), false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsRes2){
            if (!objDtbUpsRes.isSuccess()){
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls ERROR en TAM_DistribuidoresFlotillaPrograma__c : ' + objDtbUpsRes.getErrors()[0].getMessage());
                bErrror = true;
            }
            if (objDtbUpsRes.isSuccess())
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistFlotProgCancel: ' + objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsRes3 = Database.update(mapCheckOutCancel.values(), false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsRes3){
            if (!objDtbUpsRes.isSuccess()){
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls ERROR en TAM_CheckOutDetalleSolicitudCompra__c : ' + objDtbUpsRes.getErrors()[0].getMessage());
                bErrror = true;
            }
            if (objDtbUpsRes.isSuccess())
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapCheckOutCancel: ' + objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes                    

        //Recorre la lista de Opp y busca el VIN para asociar el id de la cuenta en AccountId
        List<Database.SaveResult> lDtbUpsRes4 = Database.update(mapDodCancel.values(), false);
        //Ve si hubo error
        for (Database.SaveResult objDtbUpsRes : lDtbUpsRes4){
            if (!objDtbUpsRes.isSuccess()){
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls ERROR en TAM_SolicitudDealerDestino__c : ' + objDtbUpsRes.getErrors()[0].getMessage());
                bErrror = true;
            }
            if (objDtbUpsRes.isSuccess())
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDodCancel: ' + objDtbUpsRes.getId());
        }//Fin del for para lDtbUpsRes                    

        //Es para pruebas
        if (bErrror) 
            Database.rollback(sp);
        
        //No hay error asi que hay que mandar el coreo a los usuarios
        if (!bErrror){
            String sCorreoAdici1 = System.Label.TAM_CorreoAcuseCheckOut;
            String sCorreoAdici2;
            System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls sCorreoAdici1: ' + sCorreoAdici1);
            //Recorre las solicitudes que se van a cancelar
            for (String sIdSolPaso : mapDistrEntrega.keySet()){
                //Un mapa de paso para los reg del tipo 
                Map<String, TAM_DistribuidoresFlotillaPrograma__c> mapDistrEntregaPaso = new Map<String, TAM_DistribuidoresFlotillaPrograma__c>();
                Map<String, TAM_DetalleOrdenCompra__c> mapDelaerMismoDist = new Map<String, TAM_DetalleOrdenCompra__c>();
                for (TAM_DistribuidoresFlotillaPrograma__c objPaso : mapDistrEntrega.get(sIdSolPaso)){
                    mapDistrEntregaPaso.put(objPaso.TAM_IdExterno__c, objPaso);    
                }//Fin del for para mapDistrEntrega
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDistrEntregaPaso: ' + mapDistrEntregaPaso);
                System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls mapDelaerMismoDist: ' + mapDelaerMismoDist);
                //Llama al metodo de TAM_CheckOutProgramaCtrl.enviaCorreoSolDist
                TAM_CheckOutProgramaCtrl.enviaCorreoSolDist(sIdSolPaso, sCorreoAdici1, sCorreoAdici2, mapDistrEntregaPaso, mapDelaerMismoDist, true);
            }//Fin del for para mapSolDealer.keySet()
        } //Fin si !bErrror
   
    }
        
    //Finish
    global void finish(Database.BatchableContext BC){
        System.debug('EN TAM_CancelSolPedEspOtroDistBch_cls.finish Hora: ' + DateTime.now());      
    } 
        
}