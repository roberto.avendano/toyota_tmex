public without sharing class SolicitudSiteHomeController {
    public ApexPages.StandardSetController setController{get;set;}
    private Map<String,Map<String,RecordType>> recordTypesMap;
    public String solicitudID{get;set;}
    public ContactoSite__c csId{get;set;}
    public Boolean ligtningMode {get; set;}
    
    public SolicitudSiteHomeController(ApexPages.StandardController stdcontroller) {
        this.recordTypesMap = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
        System.debug(solicitudID);
        csId = getContatSite(stdcontroller.getId());
        setController = new ApexPages.StandardSetController(Database.getQueryLocator([
            SELECT Id, Name, Solicitante__c, VehiculoSolicitado__c, TipoSolicitud__c, Estatus__c, LastModifiedDate,NombreVehiculoSite__c, RecordTypeId
            FROM SolicitudInternaVehiculos__c WHERE Solicitante__c =: csId.Contacto__c ORDER BY LastModifiedDate DESC
        ]));
    }
    
    public ContactoSite__c getContatSite(String cssId){
        return [SELECT Contacto__c FROM ContactoSite__c WHERE Id=: cssId];
    }
    
    public List<SolicitudInternaVehiculos__c> getSolicitudes(){
        return (List<SolicitudInternaVehiculos__c>) setController.getRecords();
    } 
    
    public PageReference inicio(){
        PageReference ret = new PageReference('/SolicitudSiteHome?id='+csId);
        ret.setRedirect(true);
        return ret;
    }
    
    public PageReference abrirSolicitud(){
        system.debug('entro al metodo');
        PageReference ret;
        //recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolAsignacion').Id
        System.debug(solicitudID);
        List<SolicitudInternaVehiculos__c> solicitudes = [SELECT Id, Name, Solicitante__c, VehiculoSolicitado__c, TipoSolicitud__c, Estatus__c, LastModifiedDate 
                                                          FROM SolicitudInternaVehiculos__c WHERE Solicitante__c =: csId.Contacto__c limit 1];
        
        
        SolicitudInternaVehiculos__c solicitud = [SELECT Id, Name, RecordTypeId FROM SolicitudInternaVehiculos__c WHERE Id =:solicitudID];
        system.debug('record de solicitud'+solicitud.RecordTypeId);
        if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolAsignacion').Id /*|| 
solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraPlanZero').Id*/){
    System.debug(solicitud.RecordTypeId);
    System.debug('Solicitud PoolAsignacion o CompraPlanZero' );
    ret = new PageReference('/SolicitudSiteEditar?id='+solicitudID);
}else{
    
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolSolicitante').Id){
        System.debug(solicitud.RecordTypeId);
        System.debug('Solicitud PoolAsignacion' );
        ret = new PageReference('/SolicitudSiteEPoolSolicitante?id='+solicitudID);
    }
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolPlacas').Id){
        System.debug(solicitud.RecordTypeId);
        System.debug('Solicitud PoolPlacas' );
        ret = new PageReference('/SolicitudSiteEPoolPlacas?id='+solicitudID);
    }
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolLogistica').Id){
        System.debug(solicitud.RecordTypeId);
        System.debug('Solicitud PoolLogistica' );
        ret = new PageReference('/SolicitudSiteEpoolLogistica?id='+solicitudID);
    }
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolFacturacion').Id){
        System.debug(solicitud.RecordTypeId);
        System.debug('Solicitud PoolFacturacion' );
        ret = new PageReference('/SolicitudSiteEPoolFacturacion?id='+solicitudID);
    }
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolEntrega').Id){
        System.debug(solicitud.RecordTypeId);
        System.debug('Solicitud PoolEntrega' );
        ret = new PageReference('/SolicitudSiteEPoolPlacas?id='+solicitudID);
    } /*
if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraSolicitante').Id){
System.debug(solicitud.RecordTypeId);
System.debug('Solicitud CompraSolicitante' );
ret = new PageReference('/SolicitudSiteECompraSolicitante?id='+solicitudID);
}
if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraLogistica').Id){
System.debug(solicitud.RecordTypeId);
System.debug('Solicitud CompraLogistica' );
ret = new PageReference('/SolicitudSiteECompraLogistica?id='+solicitudID);
}
if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraFacturacin').Id){
System.debug(solicitud.RecordTypeId);
System.debug('Solicitud CompraFacturacin' );
ret = new PageReference('/SolicitudSiteECompraFacturacion?id='+solicitudID);
}
if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('CompraEntrega').Id){
System.debug(solicitud.RecordTypeId);
System.debug('Solicitud CompraEntrega' );
ret = new PageReference('/SolicitudSiteEComprEntrega?id='+solicitudID);
}*/
    
    if(solicitud.RecordTypeId == recordTypesMap.get('SolicitudInternaVehiculos__c').get('PoolEntregado').Id){
        System.debug(solicitud.RecordTypeId);
        ret = new PageReference('/SolicitudSiteEPoolPlacas?id='+solicitudID);
    } 
    
}
        
        ret.setRedirect(true);
        System.debug(ret);
        return ret;
    }
}