public with sharing class EvaluacionIntegralWizardController{

    public Evaluaciones_Dealer__c evalDealer{get; set;}
    public String errores{get;set;}

    public Map<String,Seccion__c> mapSecciones{get;set;}
    public Map<String, List<Respuestas_Preguntas_TSM__c>> mapRespuestas{get;set;}
    
    public Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPI{get; set;}
    public Actividad_Plan_Integral__c currentRespuestaActividadPI{get; set;}

    public Respuestas_Preguntas_TSM__c respuestaEval{get;set;}
    
    public Id idRTAPI{get; set;}

    public List<Seccion__c> listSeccionesOrdenados{get; set;}

    public EvaluacionIntegralWizardController(ApexPages.StandardController stdController){
        errores = '';
        respuestaEval = new Respuestas_Preguntas_TSM__c();
        evalDealer = (Evaluaciones_Dealer__c)stdController.getRecord();
        
        Set<Id> setIdsPreguntas = new Set<Id>();

        mapSecciones = new Map<String,Seccion__c>();
        listSeccionesOrdenados = new List<Seccion__c>();
        mapRespuestas = new Map<String, List<Respuestas_Preguntas_TSM__c>>();

        if(evalDealer!=null && evalDealer.Id!=null){
            evalDealer = this.getEvaluacion();
        
			Map<String,Map<String,RecordType>> tipoRegEvalDealer = Constantes.TIPOS_REGISTRO_DEVNAME_ID;
	        if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral').Id){
	        	idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_EI').Id;
	        }else if(evalDealer.RecordTypeId == tipoRegEvalDealer.get('Evaluaciones_Dealer__c').get('Evaluacion_Integral_Como_Nuevos').Id){
	        	idRTAPI = tipoRegEvalDealer.get('Actividad_Plan_Integral__c').get('API_EI_Como_Nuevos').Id;
	        }
            
            currentRespuestaActividadPI = new Actividad_Plan_Integral__c(Cuenta__c=evalDealer.Nombre_Dealer__c);
            
            if(evalDealer.Respuestas_Preguntas_TSM__r!=null && evalDealer.Respuestas_Preguntas_TSM__r.size()>0){
                for(Respuestas_Preguntas_TSM__c resp : evalDealer.Respuestas_Preguntas_TSM__r){
                    if(!mapSecciones.containsKey(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c)){
                        mapSecciones.put(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r);
                        listSeccionesOrdenados.add(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r);
                        mapRespuestas.put(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, new List<Respuestas_Preguntas_TSM__c>());
                    }
                    mapRespuestas.get(resp.Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c).add(resp);
                    setIdsPreguntas.add(resp.Id);
                }
            }
            
            mapActividadesPI = this.loadMapActividadesPI(setIdsPreguntas);
            
        }
    }
    
    private Map<Id, List<Actividad_Plan_Integral__c>> loadMapActividadesPI(Set<Id> setIdsPreguntas){
    	Map<Id, List<Actividad_Plan_Integral__c>> mapActividadesPITmp = new Map<Id, List<Actividad_Plan_Integral__c>>();
        Map<Id,Respuestas_Preguntas_TSM__c> mapRespuestasConAPI = new Map<Id,Respuestas_Preguntas_TSM__c>([
        	SELECT Id, (
        		select Id, Name, Condicion_Observada__c, Prioridad__c, Responsable_Dealer__c, Actividad_Mejora__c, Respuestas_Preguntas_TSM__c, Celula_Area__c, Fecha_Compromiso__c 
        	    from Actividades_Planes_Integrales__r),
        	    (select Id, Name from Attachments) 
        	FROM Respuestas_Preguntas_TSM__c WHERE Id IN :setIdsPreguntas
        ]);
        
        for(Id item : setIdsPreguntas){
        	if(!mapRespuestasConAPI.containsKey(item) || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r==null || mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r.size()<1){
        		Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
        			Respuestas_Preguntas_TSM__c =item,
        			Cuenta__c = evalDealer.Nombre_Dealer__c
        		);
        		if(idRTAPI!=null){
        			api.RecordTypeId = idRTAPI;
        		}
        		mapActividadesPITmp.put(item,new List<Actividad_Plan_Integral__c>{api});
        	}else{
        		mapActividadesPITmp.put(item,mapRespuestasConAPI.get(item).Actividades_Planes_Integrales__r);
        	}
        }
    	return mapActividadesPITmp;
    }
    
	public List<SelectOption> getAreas(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Celula_Area__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		//options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
    
    public List<SelectOption> getReferencia(){
    	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Actividad_Plan_Integral__c.Referencia__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		//options.add(new SelectOption('', ''));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
    
    private Evaluaciones_Dealer__c getEvaluacion(){
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ';
        }
        String queryRespuestas = '(SELECT Name, Realizado__c, Pregunta_Relacionada__r.Name, Pregunta_Relacionada__r.No_de_Reactivo__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Id, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Name,  Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Objetivo__c, Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Orden__c, Pregunta_Relacionada__r.Reactivo__c, Pregunta_Relacionada__r.Area__c, Pregunta_Relacionada__r.Seccion__c, Pregunta_Relacionada__r.Obligatoria__c, Pregunta_Relacionada__r.Programa__c, Pregunta_Relacionada__r.Criterio_Negativo__c, Pregunta_Relacionada__r.Criterio_Positivo__c, Pregunta_Relacionada__r.Metodo_de_Evaluacion__c, Pregunta_Relacionada__r.Ponderacion__c, Pregunta_Relacionada__r.Fotografia_Obligatoria_del__c, Pregunta_Relacionada__r.Foto__c, Observaciones__c FROM Respuestas_Preguntas_TSM__r ORDER BY Pregunta_Relacionada__r.Seccion_Toyota_Mexico__r.Orden__c, Pregunta_Relacionada__r.No_de_Reactivo__c), ';
        query += queryRespuestas + ' (select Id, Name from Attachments), Id FROM Evaluaciones_Dealer__c WHERE Id=\'' + evalDealer.Id + '\'';
        
        return Database.query(query);
    }

    public String getEvalDealerTxt(){
        return JSON.serializePretty(mapSecciones); 
    }

    public List<Schema.FieldSetMember> getFields(){
             return SObjectType.Evaluaciones_Dealer__c.FieldSets.Evaluacion_Integral_01.getFields();
    }
	
	public List<SelectOption> getResponsablesDealer(){
		List<SelectOption> options = new List<SelectOption>();
		
		for( Contact c : [Select Id, Name, Title From Contact Where AccountId=:evalDealer.Nombre_Dealer__c]){
			String titulo = (c.Title!=null && c.Title!='')?' - ' + c.Title : '';
			options.add(new SelectOption(c.Id, c.Name + titulo));
		}       
		return options;
	}

    @RemoteAction
    public static String guardarRespuesta(String idRP, Boolean estado, String observaciones, Double puntos){
        String resp = 'Se guardo correctamente';
        try{
            if(observaciones!=null){
                update new Respuestas_Preguntas_TSM__c(
                    Id = Id.valueOf(idRP),
                    Observaciones__c = observaciones
                );
            }else{
                update new Respuestas_Preguntas_TSM__c(
                    Id = Id.valueOf(idRP),
                    Realizado__c = estado,
                    PuntosObtenidos__c = puntos
                );
            }
		}catch(DMLException  dmle){
			System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
        return resp;
    }
    
    @RemoteAction
    public static Actividad_Plan_Integral__c guardaAPI(String idRespuesta, String idAPI, String condicion, String actividad, String celula, String responsable, String evalDealerId, String fechaComp, String rtAPI){
    	try{
    		System.debug(Id.valueOf(idAPI));
    	}catch(Exception e){
    		System.debug(e);
    		idAPI=null;
    	}
        Date fechaCompDate = null;
        if(fechaComp!=null && fechaComp!=''){
            try{
                fechaCompDate = Date.parse(fechaComp);
            }catch(Exception e){
                throw new ToyotaException('La fecha no esta en el formato requerido');
            }
        }
        try{
        	Actividad_Plan_Integral__c api = new Actividad_Plan_Integral__c(
        		Respuestas_Preguntas_TSM__c = idRespuesta,
        		Id = idAPI,
        		Condicion_Observada__c = condicion,
        		Actividad_Mejora__c = actividad,
        		Celula_Area__c = celula,
        		Responsable_Dealer__c = responsable,
        		Cuenta__c=evalDealerId,
                Fecha_Compromiso__c=fechaCompDate,
                RecordTypeId = rtAPI
        	); 
        	
            System.debug(api);
        	upsert api;
        	return api;//[SELECT Id, Name, Respuestas_Preguntas_TSM__c, Condicion_Observada__c, Actividad_Mejora__c, Celula_Area__c, Responsable_Dealer__c, Fecha_Compromiso__c FROM Actividad_Plan_Integral__c WHERE Id=:api.Id];
		}catch(DMLException  dmle){
			System.debug(dmle.getStackTraceString());
            throw new ToyotaException('Error al guardar. \n'+dmle.getdmlMessage(0));
        }catch(Exception e){
            System.debug(e.getMessage());
            throw new ToyotaException('Error al guardar. \n'+e.getMessage());
        }
    }
}