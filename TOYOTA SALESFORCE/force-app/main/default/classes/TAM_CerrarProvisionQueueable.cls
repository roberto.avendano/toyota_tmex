public class TAM_CerrarProvisionQueueable implements Queueable{
    private List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision;
    private ID recordId;
    private String Tipo;
    
    public TAM_CerrarProvisionQueueable(List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision, String recordId, String Tipo) {
        this.lstDetalleProvision = lstDetalleProvision;
        this.recordId = recordId;
        this.Tipo = Tipo;
    }
    public void execute(QueueableContext context) {
        Map<String, Integer> mapMes = new Map<String, Integer>();
        mapMes.put('Enero', 1);
        mapMes.put('Febrero', 2);
        mapMes.put('Marzo', 3);
        mapMes.put('Abril', 4);
        mapMes.put('Mayo', 5);
        mapMes.put('Junio', 6);
        mapMes.put('Julio', 7);
        mapMes.put('Agosto', 8);
        mapMes.put('Septiembre', 9);
        mapMes.put('Octubre', 10); 
        mapMes.put('Noviembre', 11);
        mapMes.put('Diciembre', 12);
        
        TAM_ProvisionIncentivos__c objProvision = [SELECT 	id, TAM_MesDeProvision__c, TAM_AnioDeProvision__c 
                                                   FROM 	TAM_ProvisionIncentivos__c
                                                   WHERE	id=: recordId];
        
        Integer mesProvision = mapMes.get(objProvision.TAM_MesDeProvision__c);
        Integer anioProvision = Integer.valueOf(objProvision.TAM_AnioDeProvision__c);
        
        Date dateToday = date.newInstance(anioProvision, mesProvision, 01);
        Date dateFinVigenciaPolitica;
        if(Tipo == 'VentaCorporativa'){
            dateFinVigenciaPolitica = lstDetalleProvision[0].TAM_FinVigPoliticaRetail_AUX__c;
        } else{
            dateFinVigenciaPolitica = lstDetalleProvision[0].TAM_PoliticaIncentivos__r.TAM_FinVigencia__c;
        }
        
        
       
        System.debug('CERRAR: *********** ' + dateFinVigenciaPolitica + '----' + Tipo);
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleUpsert = new List<TAM_DetalleProvisionIncentivo__c>();
        for(TAM_DetalleProvisionIncentivo__c objDetalle : lstDetalleProvision){
            
            objDetalle.TAM_FechaCierre__c = dateToday;
            objDetalle.TAM_ProvisionCerrada__c = true;
            
            lstDetalleUpsert.add(objDetalle);
        }
        
        try {
            /**ACTUALIZAR DETALLE PROVISION */
            Database.upsert(lstDetalleUpsert, TAM_DetalleProvisionIncentivo__c.TAM_IdExterno__c);
            /**ENVIAR A ESTADO DE CUENTA */
            TAM_EstadoCuentaIncentivos.generaEstadoCuenta(recordId, dateToday, Tipo, dateFinVigenciaPolitica);
        } catch (DmlException e) {
            System.debug(e.getMessage());
        }
    }
}