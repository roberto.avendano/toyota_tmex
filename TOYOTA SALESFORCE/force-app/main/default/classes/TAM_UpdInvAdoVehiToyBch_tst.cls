/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_UpdInvAdoVehiToySch y
                        eliminar los registros del objeto TAM_LeadInventarios__c

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    01-Febrero-2021      Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_UpdInvAdoVehiToyBch_tst {

    static String VaRtLeadFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Flotilla').getRecordTypeId();
    static String VaRtLeadAutFlotilla = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Flotilla').getRecordTypeId();
    static String VaRtLeadAutPrograma = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Autorización Programa').getRecordTypeId();
    static String VaRtCteMoralNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral No Vigente').getRecordTypeId();
    static String VaRtCteFisicaNoVigente = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica No Vigente').getRecordTypeId();
    static String VaRtCteMoralPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral Preautorizado').getRecordTypeId();
    static String VaRtCteFisicaPreAuto = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica Preautorizado').getRecordTypeId();

    static  Account clientePruebaMoral = new Account();
    static  Account clientePruebaFisica = new Account();
    static  Contact contactoPrueba = new Contact();
    static  Lead candidatoPrueba = new Lead();
    static  TAM_LeadInventarios__c leadInventa = new TAM_LeadInventarios__c();
    static  TAM_InventarioVehiculosToyota__c objInvVehToy = new TAM_InventarioVehiculosToyota__c();
    static  TAM_CatalogoCodigosModelo__c objCatCodMod = new TAM_CatalogoCodigosModelo__c();

    @TestSetup static void loadData(){

        Account clienteMoral = new Account(
            Name = 'TestAccount',
            Codigo_Distribuidor__c = '570550',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización'
        );
        insert clienteMoral;

        Account clienteFisico = new Account(
            FirstName = 'TestAccount',
            LastName = 'TestAccountLastName',
            Codigo_Distribuidor__c = '570551',          
            UnidadesAutosDemoAutorizadas__c = 5,
            TAM_EstatusCliente__c = 'Preautorización'
        );
        insert clienteFisico;
        
        Contact cont = new Contact(
            LastName ='testCon',            
            AccountId = clienteMoral.Id
        );
        insert cont;  
        
        Lead cand = new Lead(
            RecordTypeId = VaRtLeadFlotilla,
            FirstName = 'Test12',
            FWY_Intencion_de_compra__c = 'Este mes',    
            Email = 'aw@a.com',
            phone = '224',
            Status='Nuevo Lead',
            LastName = 'Test3',
            FWY_Tipo_de_persona__c = 'Person física',
            TAM_TipoCandidato__c = 'Programa',
            TAM_PedidoFacturado__c = false,
            TAM_PedidoReportado__c = false      
        );
        insert cand;

        CatalogoCentralizadoModelos__c catCenMod = new CatalogoCentralizadoModelos__c(
            Name = 'AVANZA-22060-2020-B79-10-LE AT',
            Marca__c = 'Toyota',
            Serie__c = 'AVANZA',
            Modelo__c = '22060',
            AnioModelo__c = '2020',
            CodigoColorExterior__c = 'B79', 
            CodigoColorInterior__c = '10',
            DescripcionColorExterior__c = 'Azul', 
            DescripcionColorInterior__c = 'Gris',
            Version__c = 'LE AT',
            Disponible__c = 'SI'
        );
        insert catCenMod;  
                
        TAM_InventarioVehiculosToyota__c TAMInventarioVehiculosToyota = new TAM_InventarioVehiculosToyota__c(
            Name='MR2B29F36K1168300', 
            Dealer_Code__c='57002', 
            Distributor_Invoice_Date_MM_DD_YYYY__c='03/17/19 12:00 AM', 
            Dummy_VIN__c='36K1168300', 
            Exterior_Color_Code__c='01G3', 
            Exterior_Color_Description__c='GRAY ME.', 
            Interior_Color_Code__c='FC20', 
            Interior_Color_Description__c='S/D BLACK', 
            Model_Number__c='22060', 
            Model_Year__c='2020', 
            Toms_Series_Name__c='YARIS SD'
        );
        insert TAMInventarioVehiculosToyota;

        TAM_LeadInventarios__c leadInv = new TAM_LeadInventarios__c(
            Name = 'MR2B29F36K1168300',
            TAM_Prospecto__c = cand.id,
            TAM_InventarioVehiculosFyG__c = TAMInventarioVehiculosToyota.id,
            TAM_Idexterno__c = cand.id + '-' + TAMInventarioVehiculosToyota.id
        );
        insert leadInv;
                                            
    }

    static testMethod void TAM_UpdInvAdoVehiToyBchOK() {

        //Inicia las pruebas        
        Test.startTest();

	        String sQuery = ' SELECT Id, Name, TAM_Idexterno__c ';
	        sQuery += ' FROM TAM_LeadInventarios__c ';
	        sQuery += ' Limit 1';
            System.debug('EN TAM_UpdInvAdoVehiToyBchOK.execute sQuery: ' + sQuery);
            
            //Crea el objeto de  OppUpdEnvEmailBch_cls                  
            TAM_UpdInvAdoVehiToyBch objUpdInvAdoVehiToyBch = new TAM_UpdInvAdoVehiToyBch(sQuery);
            
            //No es una prueba entonces procesa de 1 en 1
            Id batchInstanceId = Database.executeBatch(objUpdInvAdoVehiToyBch, 1);

            string strSeconds = '0';
            string strMinutes = '0';
            string strHours = '1';
            string strDay_of_month = 'L';
            string strMonth = '6,12';
            string strDay_of_week = '?';
            string strYear = '2050-2051';
            String sch = strSeconds + ' ' + strMinutes + ' ' + strHours + ' ' + strDay_of_month + ' ' + strMonth + ' ' + strDay_of_week + ' ' + strYear;
            
            TAM_UpdInvAdoVehiToySch objUpdInvAdoVehiToySch = new TAM_UpdInvAdoVehiToySch();              
            System.schedule('Ejecuta_TAM_UpdInvAdoVehiToySch:', sch, objUpdInvAdoVehiToySch);
            
        Test.stopTest();
        
    }
    
}