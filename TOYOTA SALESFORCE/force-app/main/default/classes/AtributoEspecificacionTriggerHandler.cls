public class AtributoEspecificacionTriggerHandler Extends TriggerHandler {
    private List<EspecificacionAtributo__c> atributosList;
    
    public AtributoEspecificacionTriggerHandler(){
        this.atributosList = (List<EspecificacionAtributo__c>) Trigger.new;    
        
    }   
    
    public override void afterInsert(){
        actualizaAtributos(atributosList);	
    }
    
    public static void actualizaAtributos (List<EspecificacionAtributo__c> nuevosAtributos){
        List<EspecificacionFichaTecnica__c> listaNuevosAtributos = new List<EspecificacionFichaTecnica__c>();
        List<CodigoModeloFichaTecnica__c>  fichasTecnicas = [Select id, CodigoModeloFichaTecnica__c.FichaTecnica__c from CodigoModeloFichaTecnica__c WHERE FichaTecnica__r.estatus__c != 'Final' AND CodigoModeloFichaTecnica__c.FichaTecnica__c != null];
        if(!fichasTecnicas.isEmpty()){
            for(EspecificacionAtributo__c listaAtributos :nuevosAtributos){
                if(listaAtributos.Activo__c == true){
                    for(CodigoModeloFichaTecnica__c listaFichaTecnica : fichasTecnicas){
                        EspecificacionFichaTecnica__c atributosNuevos = new EspecificacionFichaTecnica__c();
                        atributosNuevos.EspecificacionAtributo__c = listaAtributos.id;
                        atributosNuevos.EstatusEspecificacion__c = 'NA';
                        atributosNuevos.NoAplica__c = true;
                        atributosNuevos.FichaTecnicaProducto__c = listaFichaTecnica.FichaTecnica__c;
                        atributosNuevos.CodigoModeloFichaTecnica__c = listaFichaTecnica.id;
                        listaNuevosAtributos.add(atributosNuevos);
                    }
                } 
                
            }   
            
        }
        
        if(!listaNuevosAtributos.isEmpty()){
            insert listaNuevosAtributos;
        }
        
    }    
}