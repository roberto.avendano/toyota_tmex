public class TAM_CheckoutWrapperClass {
    
    
    @AuraEnabled 
    public String strName {get;set;}   
    @AuraEnabled
    public String strCodigoColorExterior {get;set;}
    @AuraEnabled
    public String strCodigoColorInterior {get;set;}
    @AuraEnabled
    public String strMarca {get;set;}
    @AuraEnabled
    public String strModelo {get;set;}
    @AuraEnabled
    public String strAnio {get;set;}
    @AuraEnabled
    public String strPrecioSinIVA {get;set;}
    @AuraEnabled 
    public String strCantidad {get;set;}
    @AuraEnabled 
    public String strTipo {get;set;} //Este campo sirve para pedido especial
    @AuraEnabled 
    public String strSerie {get;set;}
     @AuraEnabled 
    public String strVersion {get;set;}
    @AuraEnabled 
    public String strRecordTypeID {get;set;} 
    @AuraEnabled 
    public String strDistribuidor {get;set;}
    @AuraEnabled 
    public String strIdExterno {get;set;}
    @AuraEnabled 
    public String strCash {get;set;}
    @AuraEnabled 
    public String strFiltroIncentivo {get;set;}
    @AuraEnabled
    public String strDescripcionColorExterior {get;set;}
    @AuraEnabled
    public String strDescripcionColorInterior {get;set;}
    @AuraEnabled
    public Boolean boolClonado {get;set;} 
    @AuraEnabled 
    public String strSolicitados {get;set;}
    @AuraEnabled 
    public String strVin {get;set;}
    @AuraEnabled 
    public String strTipoPago {get;set;}
    @AuraEnabled 
    public Integer intTotalVines {get;set;}
    @AuraEnabled 
    public String strIdCatCentMod {get;set;}
    @AuraEnabled 
    public String strsTipoRegFlotProg {get;set;}
    @AuraEnabled 
    public String strPorDescDTM {get;set;}
    @AuraEnabled 
    public String strPorDescTMEX {get;set;}
    @AuraEnabled 
    public String strPorDescDLER {get;set;}
    @AuraEnabled 
    public String strMontoDescDTM {get;set;}
    @AuraEnabled 
    public String strMontoDescTMEX {get;set;}
    @AuraEnabled 
    public String strMontoDescDLER {get;set;}
    @AuraEnabled 
    public String strIdPoliticaIncent {get;set;}
    @AuraEnabled 
    public Boolean blnValidaTfs {get;set;}
    @AuraEnabled 
    public Boolean blnPorMonto {get;set;}
    @AuraEnabled 
    public String strDealerIntercambio {get;set;}
    @AuraEnabled 
    public Boolean blnIntercambio {get;set;}

	//Un constructor por default
    public TAM_CheckoutWrapperClass(){} 
    
    //Un constrctor con parametros
    public TAM_CheckoutWrapperClass(String strName, String strCodigoColorExterior, String strCodigoColorInterior, String strMarca, 
    	String strModelo, String strAnio, String strPrecioSinIVA, String strCantidad, String strTipo, string strSerie,
    	string strVersion, String strRecordTypeID) {
        this.strName = strName;
        this.strCodigoColorExterior = strCodigoColorExterior;
        this.strCodigoColorInterior = strCodigoColorInterior;
        this.strMarca = strMarca;
        this.strModelo = strModelo;
        this.strAnio = strAnio;
        this.strPrecioSinIVA = strPrecioSinIVA;
        this.strCantidad = strCantidad;
        this.strTipo = strTipo;
        this.strSerie = strSerie;
        this.strVersion = strVersion;
        this.strRecordTypeID = strRecordTypeID;
    }
    
}