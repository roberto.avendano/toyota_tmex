/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del objeto TAM_CheckOutDetalleSolicitudCompra__c
                        y actualizar los registros TAM_MovimientosSolicitudes__c.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    06-Julio-2021        Héctor Figueroa             Creación
******************************************************************************* */

public without sharing class TAM_CheckOutDetalleSolicitudComHandler extends TriggerHandler{

    private List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutSolCom;
    private Map<Id,TAM_CheckOutDetalleSolicitudCompra__c> mapCheckOutSolCom;

    public TAM_CheckOutDetalleSolicitudComHandler(){
        System.debug(LoggingLevel.INFO,'Inicia TAM_CheckOutDetalleSolicitudComHandler');  
        this.lCheckOutSolCom = (List<TAM_CheckOutDetalleSolicitudCompra__c>) trigger.new;
        this.mapCheckOutSolCom = (Map<id,TAM_CheckOutDetalleSolicitudCompra__c>) trigger.oldMap;
    }

    public override void afterUpdate(){
        actualizaMovSol(lCheckOutSolCom, mapCheckOutSolCom);
    }

    //El metodo que valida los datos del rango de la flotilla 
    private void actualizaMovSol(List<TAM_CheckOutDetalleSolicitudCompra__c> lCheckOutSolCom, 
        Map<Id,TAM_CheckOutDetalleSolicitudCompra__c> mapCheckOutSolCom){
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol...');

        String strRecordTypeId = Schema.SObjectType.Movimiento__c.getRecordTypeInfosByName().get('Movimiento').getRecordTypeId();
        String sFleet = 'N';

        String sRectorTypeCheckOutInv = Schema.SObjectType.TAM_CheckOutDD__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
        Map<String, Date> mapVinFechCancela = new Map<String, Date>();
        Map<String, String> mapVinIdSolAuto = new Map<String, String>();
        Map<String, Map<Date, Date>> mapMesFechIniFechaFin = new Map<String, Map<Date, Date>>();
        Date dtFechaActual = Date.today();
        String strAnioActual = String.valueOf(dtFechaActual.year());
        Set<String> setIdSol = new Set<String>();
        Map<String, String> mapVinIdSol = new Map<String, String>(); 
        Map<String, TAM_SolicitudesFlotillaPrograma__c> mapIdSolName = new Map<String, TAM_SolicitudesFlotillaPrograma__c>(); 
        Map<String, TAM_MovimientosSolicitudes__c> MapMovSolUps = new Map<String, TAM_MovimientosSolicitudes__c>();
        Map<String, Integer> mapVinTotMovAut = new Map<String, Integer>();

        //Consulta el calendario de toyota y metelo al mapa de mapCalenToyota
        //Busca en el objeto de TAM_CalendarioToyota__c el rango al que le corresponda
        for (TAM_CalendarioToyota__c objCalToy : [Select id, TAM_FechaInicio__c, TAM_FechaFin__c 
            From TAM_CalendarioToyota__c where TAM_Anio__c =:strAnioActual 
            Order by TAM_FechaInicio__c ASC]){
            //Toma el mes y el año y metelos en la llave del mapa
            String sAnioMes = objCalToy.TAM_FechaInicio__c.year() + '' + objCalToy.TAM_FechaInicio__c.month();    
            //Metelo al mapa de mapMesFechIniFechaFin
            mapMesFechIniFechaFin.put(sAnioMes, new Map<Date, Date>{objCalToy.TAM_FechaInicio__c => objCalToy.TAM_FechaFin__c});
        }
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapMesFechIniFechaFin: ' + mapMesFechIniFechaFin.keyset());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapMesFechIniFechaFin: ' + mapMesFechIniFechaFin.values());
        
        Boolean blnTAM_ActualizaMovimientosSolicitud = (System.Label.TAM_ActualizaMovimientosSolicitud != null && System.Label.TAM_ActualizaMovimientosSolicitud != '') 
            ? Boolean.valueOf(System.Label.TAM_ActualizaMovimientosSolicitud) : false ;        
        System.debug('ENTRO TAM_InventarioVehiculosToyotaTgrHandler blnTAM_ActualizaMovimientosSolicitud: ' + blnTAM_ActualizaMovimientosSolicitud);
        
        //Ve si esta activa la banera global para poder actuelizar los mov en la cancelacion o autorización de la solcitud
        if (blnTAM_ActualizaMovimientosSolicitud || Test.isRunningTest()){

        //Recorre la lista de lCheckOutSolCom y ve si es de tipo Inventario
        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutPaso : lCheckOutSolCom){
            Map<String, Integer> mapVinTotMov = new Map<String, Integer>();
            Map<String, Integer> mapVinTotMovCanAut = new Map<String, Integer>();
            //Map<String, Map<Date, Date>> MapSolCancelDeAuto = new Map<String, Map<Date, Date>>();
            Map<String, TAM_CheckOutDetalleSolicitudCompra__c> MapSolCancelDeAuto = new Map<String, TAM_CheckOutDetalleSolicitudCompra__c>();
            
            //Ve si tiene ID de solicitud
            if (objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c != null)
                mapVinIdSol.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c);            
            //Ve si es de tipo Inventario y estas cancelada
            if (objCheckOutPaso.TAM_TipoVenta__c == 'Inventario'){
                if ( (objCheckOutPaso.TAM_EstatusDOD__c == 'Cancelado' || objCheckOutPaso.TAM_EstatusDealerSolicitud__c == 'Cancelada' || objCheckOutPaso.TAM_EstatusDealerSolicitud__c == 'Rechazada')                    
                    &&  mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_EstatusDOD__c != 'Cancelado' 
                    &&  mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_EstatusDealerSolicitud__c != 'Cancelada'
                    &&  mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_EstatusDealerSolicitud__c != 'Rechazada'
                    ||  (objCheckOutPaso.TAM_ActualizaCancelada__c && !mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_ActualizaCancelada__c)
                    ){
                    //Metelo al mapa de mapVinFechCancela
                    mapVinFechCancela.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso.TAM_FechaCancelacion__c);
                    
		            //Consulta las solicitudes asociadas a mapIdSolName
		            for (TAM_SolicitudesFlotillaPrograma__c objSolVeta : [Select t.Id, t.Name, t.TAM_FechaCierreSolicitudForm__c 
		                From TAM_SolicitudesFlotillaPrograma__c t Where Id IN: mapVinIdSol.values()]){
		                //Metelo al mapa de mapIdSolName
		                mapIdSolName.put(objSolVeta.id, objSolVeta);
		            }
		            String sNameSolCons = '%' + mapIdSolName.get(objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c).Name + '%';
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapIdSolName: ' + mapIdSolName + ' sNameSolCons: ' + sNameSolCons);

		            //Ya tienes los vines que se estan cancelando consulta los mov en el objeto de TAM_MovimientosSolicitudes__c
		            for (TAM_MovimientosSolicitudes__c objMovSol : [Select t.id, t.TAM_VIN__c, 
		                    t.TAM_UltimoMovimientoDD__c, 
		                    t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
		                    From TAM_MovimientosSolicitudes__c t Where TAM_FechaMovimiento__c <= :objCheckOutPaso.TAM_FechaCancelacion__c
		                    AND TAM_VIN__c =:objCheckOutPaso.TAM_VIN__c and Name LIKE :sNameSolCons Order by TAM_VIN__c]){
		                //Busca el vin en el mapa de mapVinFechCancela
		                //if (mapVinFechCancela.containsKey(objMovSol.TAM_VIN__c)){
		                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol objMovSol: ' + objMovSol);
		                    Date dtFechaCancela = objCheckOutPaso.TAM_FechaCancelacion__c;
		                    //Busca la fecha de cancelacion en el mapa de mapMesFechIniFechaFin
		                    String sAnioMes = dtFechaCancela.year() + '' + dtFechaCancela.month();    
		                    Map<Date, Date> mapFechaCalToy = mapMesFechIniFechaFin.containsKey(sAnioMes) ? mapMesFechIniFechaFin.get(sAnioMes) : new Map<Date, Date>();
		                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol TAM_FechaMovimiento__c: ' + objMovSol.TAM_FechaMovimiento__c + ' dtFechaCancela: ' + dtFechaCancela + ' sAnioMes: ' + sAnioMes + ' mapFechaCalToy: ' + mapFechaCalToy);
		                    //Ve si la fecha del movimiento esta dentro del mapa de mapFechaCalToy
		                    for (Date dtFechaIniPaso : mapFechaCalToy.KeySet()){
		                        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol dtFechaIniPaso: ' + dtFechaIniPaso + ' TAM_FechaMovimiento__c: ' + objMovSol.TAM_FechaMovimiento__c + ' mapFechaCalToy.get(dtFechaIniPaso): ' + mapFechaCalToy.get(dtFechaIniPaso));
		                        if (objMovSol.TAM_FechaMovimiento__c >= dtFechaIniPaso && objMovSol.TAM_FechaMovimiento__c <= mapFechaCalToy.get(dtFechaIniPaso) ){
		                            Integer intTotMov = Integer.valueOf(objMovSol.TAM_Total__c);
		                            //Metelo al mapa de mapVinTotMov
		                            if (mapVinTotMov.containsKey(objMovSol.TAM_VIN__c)){
		                                intTotMov = mapVinTotMov.get(objMovSol.TAM_VIN__c);
		                                intTotMov += Integer.valueOf(objMovSol.TAM_Total__c);
		                                mapVinTotMov.put(objMovSol.TAM_VIN__c, intTotMov);
		                            }//Fin si mapVinTotMov.containsKey(objMovSol.TAM_VIN__c)
		                            if (!mapVinTotMov.containsKey(objMovSol.TAM_VIN__c))
		                                mapVinTotMov.put(objMovSol.TAM_VIN__c, intTotMov);
		                        }//Fin si objMovSol.TAM_FechaMovimiento__c > dtFechaIniPaso && objMovSol.TAM_FechaMovimiento__c < mapFechaCalToy.get(dtFechaIniPaso)
		                    }//Fin del for mapFechaCalToy.KeySet()
		                
		                //}//Fin si mapVinFechCancela.containsKey(objMovSol.TAM_VIN__c)        
		            }//Fin del for para TAM_MovimientosSolicitudes__c
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMov: ' + mapVinTotMov.keyset());
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMov: ' + mapVinTotMov.values());

	                String strDtFechaCheckOut = String.valueOf(objCheckOutPaso.TAM_FechaCancelacion__c);  
	                //Buscalos en el mapa de mapVinFechCancela
	                //if (mapVinFechCancela.containsKey(objCheckOutPaso.TAM_VIN__c)){
	                    String sIdSol = mapVinIdSol.get(objCheckOutPaso.TAM_VIN__c);
	                    String sNameSol = mapIdSolName.get(sIdSol).Name;
	                    String sIdExterno = objCheckOutPaso.TAM_VIN__c + '-' + sNameSol  + '-' + strDtFechaCheckOut + '-' + sRectorTypeCheckOutInv;                                    
	                    Integer intTotVta = mapVinTotMov.containsKey(objCheckOutPaso.TAM_VIN__c) ? mapVinTotMov.get(objCheckOutPaso.TAM_VIN__c) : 0;
	                    //Crea el objeto de objeto del tipoTAM_MovimientosSolicitudes__c
	                    //Metelo al mapa  
	                    TAM_MovimientosSolicitudes__c objMovSol = new TAM_MovimientosSolicitudes__c(
	                        Name = sNameSol + ' - ' + strDtFechaCheckOut,
	                        TAM_IdExternoSFDC__c = sIdExterno,
	                        TAM_Total__c = intTotVta == 1 ? -1 : 0,
	                        TAM_FechaMovimiento__c = objCheckOutPaso.TAM_FechaCancelacion__c,                                    
	                        TAM_VIN__c = objCheckOutPaso.TAM_VIN__c,
	                        TAM_CheckOutSolicitud__c = objCheckOutPaso.id
	                    );
	                    //Metelo al mapa de MapMovSolUps
	                    MapMovSolUps.put(sIdExterno, objMovSol);
	                //} //Fin si mapVinFechCancela.containsKey(sVinPaso)

                }//Fin si esta cancelada
                //Quedo cerrada la solicitud
                if ( (objCheckOutPaso.TAM_EstatusDealerSolicitud__c == 'Cerrada' && mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_EstatusDealerSolicitud__c != 'Cerrada')
                    || (objCheckOutPaso.TAM_ActualizaAutorizada__c && !mapCheckOutSolCom.get(objCheckOutPaso.id).TAM_ActualizaAutorizada__c) ){
                    if (objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c != null)
                        mapVinIdSolAuto.put(objCheckOutPaso.TAM_VIN__c, objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c);
                    Boolean blnTieneMovMesAnt = false;
                    
                    //Consulta las solicitudes asociadas a mapIdSolName
                    for (TAM_SolicitudesFlotillaPrograma__c objSolVeta : [Select t.Id, t.Name, t.TAM_FechaCierreSolicitudForm__c 
                        From TAM_SolicitudesFlotillaPrograma__c t Where Id =: objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c]){
                        //Metelo al mapa de mapIdSolName
                        mapIdSolName.put(objSolVeta.id, objSolVeta);
                    }                    
                    String sNameSolCons = '%' + mapIdSolName.get(objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c).Name + '%';
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapIdSolName2: ' + mapIdSolName + ' sNameSolCons: ' + sNameSolCons);
                    
                    Date dtFechaCierreSol = mapIdSolName.get(objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c).TAM_FechaCierreSolicitudForm__c;
                    //Consulta la solcitud en el mapa de mapMesFechIniFechaFin
                    String sAnioMesPaso = dtFechaCierreSol.year() + '' + dtFechaCierreSol.month();    
                    Map<Date, Date> mapFechaCalToyPaso = mapMesFechIniFechaFin.containsKey(sAnioMesPaso) ? mapMesFechIniFechaFin.get(sAnioMesPaso) : new Map<Date, Date>();
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapFechaCalToyPaso: ' + mapFechaCalToyPaso);
                    Date dtFechaIniCalToy = Date.today();
                    for (Date dtFechaIniCalToyPaso : mapFechaCalToyPaso.KeySet()){
                        dtFechaIniCalToy = dtFechaIniCalToyPaso;
                    }//Fin del for para mapFechaCalToy.KeySet()
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol dtFechaIniCalToy: ' + dtFechaIniCalToy + ' dtFechaCierreSol: ' + dtFechaCierreSol);
                    
		            //Ya tienes el objeto el checkOut ve si tiene mov asociados en esa fecha.
		            String queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
		                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
		                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
		                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
		                ' And TAM_FechaEnvForm__c >= ' + String.valueOf(dtFechaIniCalToy) +
                        ' And TAM_FechaEnvForm__c <= ' + String.valueOf(dtFechaCierreSol) +
		                ' And VIN__r.Name = \'' + String.escapeSingleQuotes(objCheckOutPaso.TAM_VIN__c) + '\'' +
		                ' And Fleet__c = \'' + String.escapeSingleQuotes(sFleet) + '\'' +
		                ' Order by Submitted_Date__c DESC';
		            //Ees una prueba
		            if (Test.isRunningTest())    
		                queryMovimiento = 'Select ID, Name, Sale_Date__c, Submitted_Date__c, Submitted_Time__c, VIN__c, ' +  
		                ' Trans_Type__c, Id_Modelo__c, Distribuidor__c, Objetivo_Venta__c, Month__c, Year__c,' +  
		                ' Qty__c, TAM_FechaEnvForm__c, TAM_ContadorVenta__c, Fleet__c, VIN__r.Name, TAM_CodigoDistribuidorFrm__c' +
		                ' From Movimiento__c Where RecordTypeId = \'' + String.escapeSingleQuotes(strRecordTypeId) + '\'' + 
		                ' Order by Submitted_Date__c DESC LIMIT 2';
		            System.debug('EN TAM_ActTotVtaInvBch_cls queryMovimiento: ' + queryMovimiento);

                    //Busca los movimientos    
                    List<sObject> lstObjLstMov = Database.query(queryMovimiento);
                    System.debug('EN TAM_ActTotVtaInvBch_cls lstObjLstMov: ' + lstObjLstMov);
                    
                    //Si tiene mov regrea el ultimo mov de la clase TAM_ActTotVtaInvBch_cls
                    Movimiento__c objMovPaso = new Movimiento__c();
                    if (!lstObjLstMov.isEmpty()){
                        if (lstObjLstMov.size() == 1)
                            objMovPaso = (Movimiento__c) lstObjLstMov.get(0);
                        if (lstObjLstMov.size() > 1)
                            objMovPaso = TAM_ActTotVtaInvBch_cls.getUltMov(lstObjLstMov);
                    }//Fin si !lstObjLstMov.isEmpty()
                    System.debug('EN TAM_ActTotVtaInvBch_cls objMovPaso: ' + objMovPaso);
                    
                    Integer intTotMovPaso = 0;
                    //Recorre la lista de reg del tipo Movimiento__c
		            for (sObject objsObjectPaso : lstObjLstMov){
		                Movimiento__c objMovPasoCons = (Movimiento__c) objsObjectPaso;
                         System.debug('EN TAM_ActFechaDDChkOutBch_cls Fleet__c: ' + objMovPasoCons.Fleet__c + ' intTotMov: ' + objMovPasoCons.Qty__c + ' Trans_Type__c: ' + objMovPasoCons.Trans_Type__c + ' Submitted_Date__c: ' + objMovPasoCons.Submitted_Date__c + ' Submitted_Time__c: ' + objMovPasoCons.Submitted_Time__c);
                         if (objMovPasoCons.Trans_Type__c == 'RDR')
                             intTotMovPaso += 1;
                         if (objMovPasoCons.Trans_Type__c == 'RVRSL')
                             intTotMovPaso += -1;
                    }//Fin del for para objVenta.Movimientos__r
                    System.debug('EN TAM_ActFechaDDChkOutBch_cls intTotMovPaso: ' + intTotMovPaso);
                    
			        //Busca la primer sol camcelada aspciada a mapVinIdSolAuto
			        for (TAM_CheckOutDetalleSolicitudCompra__c objCheckOutCancela : [Select Id, TAM_VIN__c, TAM_FechaCancelacion__c, 
			            TAM_EstatusDOD__c, TAM_SolicitudFlotillaPrograma__c, TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c, 
			            TAM_EstatusDealerSolicitud__c, TAM_TipoVenta__c, TAM_FechaCreacionFormula__c, TAM_SolicitudFlotillaPrograma__r.Name
			            From TAM_CheckOutDetalleSolicitudCompra__c Where TAM_VIN__c =:objCheckOutPaso.TAM_VIN__c
			            And (TAM_EstatusDOD__c = 'Cancelado' OR TAM_EstatusDealerSolicitud__c = 'Cancelada' OR TAM_EstatusDealerSolicitud__c = 'Rechazada')
			            And TAM_TipoVenta__c = 'Inventario' Order by TAM_FechaCreacionFormula__c DESC LIMIT 1]){
			            //Metelo al mapa de canceladas MapSolCancelDeAuto
    	                MapSolCancelDeAuto.put(objCheckOutCancela.TAM_VIN__c, objCheckOutCancela);
                        //MapSolCancelDeAuto.put(objCheckOutCancela.TAM_VIN__c, new Map<Date, Date>{objCheckOutCancela.TAM_SolicitudFlotillaPrograma__r.TAM_FechaCierreSolicitudForm__c => objCheckOutCancela.TAM_FechaCancelacion__c});
			        }
			        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapSolCancelDeAuto2: ' + MapSolCancelDeAuto.keyset());
			        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapSolCancelDeAuto2: ' + MapSolCancelDeAuto.values());

			        //Ve si tienen algo los mapas de !MapSolCancelDeAuto.isEmpty() && !MapSolCancelDeAuto.isEmpty()
			        if (!MapSolCancelDeAuto.isEmpty()){	
			             String sNomSolPaso = '%' + MapSolCancelDeAuto.get(objCheckOutPaso.TAM_VIN__c).TAM_SolicitudFlotillaPrograma__r.Name + '%';		
			             //Ya tienes los vines que se estan cancelando consulta los mov en el objeto de TAM_MovimientosSolicitudes__c
			             for (TAM_MovimientosSolicitudes__c objMovSol : [Select t.id, t.TAM_VIN__c, 
			                    t.TAM_UltimoMovimientoDD__c, 
			                    t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
			                    From TAM_MovimientosSolicitudes__c t Where TAM_VIN__c IN :MapSolCancelDeAuto.keyset() 
			                    And Name Like :sNomSolPaso Order by TAM_VIN__c]){
			                Date dtFechaCancela = MapSolCancelDeAuto.get(objCheckOutPaso.TAM_VIN__c).TAM_FechaCancelacion__c;        
			                //Busca el vin en el mapa de MapSolCancelDeAuto
			                if (MapSolCancelDeAuto.containsKey(objMovSol.TAM_VIN__c) && dtFechaCancela != null){
			                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol objMovSol2: ' + objMovSol);
			                    //Ve si la fecha del mov objMovSol.TAM_FechaMovimiento__c
			                    //if (objMovSol.TAM_FechaMovimiento__c >= dtFechaIniSol && objMovSol.TAM_FechaMovimiento__c <= dtFechaCancela ){
			                        //Busca la fecha de cancelacion en el mapa de mapMesFechIniFechaFin
			                        String sAnioMes = dtFechaCancela.year() + '' + dtFechaCancela.month();    
			                        Map<Date, Date> mapFechaCalToy = mapMesFechIniFechaFin.containsKey(sAnioMes) ? mapMesFechIniFechaFin.get(sAnioMes) : new Map<Date, Date>();
			                        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol TAM_FechaMovimiento__c2: ' + objMovSol.TAM_FechaMovimiento__c + ' dtFechaCancela: ' + dtFechaCancela + ' sAnioMes: ' + sAnioMes + ' mapFechaCalToy: ' + mapFechaCalToy);
			                        //Ve si la fecha del movimiento esta dentro del mapa de mapFechaCalToy
			                        for (Date dtFechaIniPaso : mapFechaCalToy.KeySet()){
			                            if (objMovSol.TAM_FechaMovimiento__c >= dtFechaIniPaso && objMovSol.TAM_FechaMovimiento__c <= mapFechaCalToy.get(dtFechaIniPaso) ){
			                                Integer intTotMov = Integer.valueOf(objMovSol.TAM_Total__c);
			                                if (intTotMov == 1 || intTotMov == -1)
			                                     blnTieneMovMesAnt = true;
			                                //Metelo al mapa de mapVinTotMovCanAut
			                                if (mapVinTotMovCanAut.containsKey(objMovSol.TAM_VIN__c)){
			                                    intTotMov = mapVinTotMovCanAut.get(objMovSol.TAM_VIN__c);
			                                    intTotMov += Integer.valueOf(objMovSol.TAM_Total__c);
			                                    mapVinTotMovCanAut.put(objMovSol.TAM_VIN__c, intTotMov);
			                                }//Fin si mapVinTotMovCanAut.containsKey(objMovSol.TAM_VIN__c)
			                                if (!mapVinTotMovCanAut.containsKey(objMovSol.TAM_VIN__c))
			                                    mapVinTotMovCanAut.put(objMovSol.TAM_VIN__c, intTotMov);
			                            }//Fin si objMovSol.TAM_FechaMovimiento__c > dtFechaIniPaso && objMovSol.TAM_FechaMovimiento__c < mapFechaCalToy.get(dtFechaIniPaso)
			                        }//Fin del for mapFechaCalToy.KeySet()
			                    //}//Fin si objMovSol.TAM_FechaMovimiento__c >= dtFechaIniSol && objMovSol.TAM_FechaMovimiento__c <= dtFechaCancela
			                }//Fin si MapSolCancelDeAuto.containsKey(objMovSol.TAM_VIN__c)        
			             }//Fin del for para TAM_MovimientosSolicitudes__c
			        }//Fin si !MapSolCancelDeAuto.isEmpty() && !MapSolCancelDeAuto.isEmpty()        
			        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMovCanAut2: ' + mapVinTotMovCanAut.keyset());
			        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMovCanAut2: ' + mapVinTotMovCanAut.values());

                    //No tiene cancelaciones pevias y si tiene mov previos
                    if (!lstObjLstMov.isEmpty()){
                        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol Tiene antes de la fecha de creación objMovPaso: ' + objMovPaso);
                        //Toma el registro de la fecha de autorizacion de la solicitud en SFDC
                        String sNameSolPaso = mapIdSolName.get(objCheckOutPaso.TAM_SolicitudFlotillaPrograma__c).Name;
                        String sIdExterno = objCheckOutPaso.TAM_VIN__c + '-' + sNameSolPaso  + '-' +  String.valueOf(dtFechaCierreSol) + '-' + sRectorTypeCheckOutInv;                                    
                        //Ve si el moviento final es una venta
                        if (objMovPaso.Trans_Type__c == 'RDR'){
                            //Metelo al mapa  
                            TAM_MovimientosSolicitudes__c objMovSolAuto = new TAM_MovimientosSolicitudes__c(
                                Name = sNameSolPaso + ' - ' + String.valueOf(dtFechaCierreSol),
                                TAM_EstatusSolicitud__c = 'Autorizada',
                                TAM_FechaMovimiento__c = dtFechaCierreSol,
                                TAM_VIN__c = objCheckOutPaso.TAM_VIN__c,
                                TAM_CheckOutSolicitud__c = objCheckOutPaso.id,
                                
                                TAM_IdExternoSFDC__c = sIdExterno,
                                TAM_Total__c = 1,
                                TAM_UltimoMovimientoDD__c = objMovPaso.id                                
                            );
                            System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol objMovSolAuto1: ' + objMovSolAuto);
                            //Metelo al mapa de MapMovSolUps
                            MapMovSolUps.put(sIdExterno, objMovSolAuto);                        
                        }//Fin si objMovPaso.Trans_Type__c == 'RDR'
                    }//Fin si MapSolCancelDeAuto.isEmpty() && !lstObjLstMov.isEmpty
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps1: ' + MapMovSolUps.keyset());
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps1: ' + MapMovSolUps.values());                

                    //Ya tienes los vines que se estan cancelando consulta los mov en el objeto de TAM_MovimientosSolicitudes__c
                    for (TAM_MovimientosSolicitudes__c objMovSol : [Select t.id, t.TAM_VIN__c, 
                        //t.TAM_UltimoMovimientoDD__c, 
                        t.TAM_Total__c, t.TAM_IdExternoSFDC__c, t.TAM_FechaMovimiento__c, t.TAM_CheckOutSolicitud__c, t.TAM_EstatusSolicitud__c
                        From TAM_MovimientosSolicitudes__c t Where TAM_VIN__c IN :mapVinIdSolAuto.KeySet() and Name Like :sNameSolCons
                        And TAM_Total__c != 0 Order by TAM_VIN__c]){
                        //Metelo al mapa de mapVinTotMovAut
                        mapVinTotMovAut.put(objMovSol.TAM_VIN__c, 1);
                    }
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMovAut2: ' + mapVinTotMovAut.keyset());
                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinTotMovAut2: ' + mapVinTotMovAut.values());                

		            //Ve si el vin esta en una solicitud autorizada
		            //if (mapVinIdSolAuto.containsKey(objCheckOutPaso.TAM_VIN__c)){
		                String sIdSolAuto = mapVinIdSolAuto.get(objCheckOutPaso.TAM_VIN__c);
		                if (mapIdSolName.get(sIdSolAuto).TAM_FechaCierreSolicitudForm__c != null){
		                    String strDtSolAutoCierre = String.valueOf(mapIdSolName.get(sIdSolAuto).TAM_FechaCierreSolicitudForm__c);  
		                    //Ve si no existe la solicitud en el mapa de mapVinTotMovAut
		                    if (!mapVinTotMovAut.containsKey(objCheckOutPaso.TAM_VIN__c)){
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol NO EXISTE EN EL MAPA mapVinTotMovAut: ' + mapVinTotMovAut);
		                        String sIdSol = mapVinIdSol.get(objCheckOutPaso.TAM_VIN__c);
		                        String sNameSol = mapIdSolName.get(sIdSol).Name;
		                        String sIdExterno = objCheckOutPaso.TAM_VIN__c + '-' + sNameSol  + '-' + strDtSolAutoCierre + '-' + sRectorTypeCheckOutInv;                                    
		                        Integer intTotVta = 0;
		                        //Ve si el vin existe en mapVinTotMovCanAut y termino en 0 por la suma logica
		                        if (mapVinTotMovCanAut.containsKey(objCheckOutPaso.TAM_VIN__c))
		                            if (mapVinTotMovCanAut.get(objCheckOutPaso.TAM_VIN__c) == 0 && blnTieneMovMesAnt)
		                               intTotVta = 1;
		                        //Crea el objeto de objeto del tipoTAM_MovimientosSolicitudes__c
		                        TAM_MovimientosSolicitudes__c objMovSolAuto = new TAM_MovimientosSolicitudes__c(
		                            Name = sNameSol + ' - ' + strDtSolAutoCierre,
                                    TAM_EstatusSolicitud__c = 'Autorizada',
		                            TAM_IdExternoSFDC__c = sIdExterno,
		                            TAM_Total__c = intTotVta,
		                            TAM_FechaMovimiento__c = mapIdSolName.get(sIdSolAuto).TAM_FechaCierreSolicitudForm__c,                                    
		                            TAM_VIN__c = objCheckOutPaso.TAM_VIN__c,
		                            TAM_CheckOutSolicitud__c = objCheckOutPaso.id
		                        );
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol objMovSolAuto2: ' + objMovSolAuto);
		                        //Ve si no existe previmente en el mapa de metelo al mapa de MapMovSolUps
		                        if (!MapMovSolUps.containsKey(sIdExterno))
		                          MapMovSolUps.put(sIdExterno, objMovSolAuto);
                                if (MapMovSolUps.containsKey(sIdExterno))
                                    System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol YA EXISTE EN EL MAPA sIdExterno: ' + sIdExterno);
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps2: ' + MapMovSolUps.keyset());
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps2: ' + MapMovSolUps.values());                
		                    }//Fin si !mapVinTotMovAut.containsKey(objCheckOutPaso.TAM_VIN__c)
		                
                            //Ve si no existe la solicitud en el mapa de mapVinTotMovAut
                            if (mapVinTotMovAut.containsKey(objCheckOutPaso.TAM_VIN__c)){
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol YA EXISTE EN EL MAPA mapVinTotMovAut: ' + mapVinTotMovAut);
                                String sIdSol = mapVinIdSol.get(objCheckOutPaso.TAM_VIN__c);
                                String sNameSol = mapIdSolName.get(sIdSol).Name;
                                String sIdExterno = objCheckOutPaso.TAM_VIN__c + '-' + sNameSol  + '-' + strDtSolAutoCierre + '-' + sRectorTypeCheckOutInv;                                    
                                Integer intTotVta = 0;                               
                                //Ve si el vin existe en mapVinTotMovCanAut y termino en 0 por la suma logica
                                if (mapVinTotMovCanAut.containsKey(objCheckOutPaso.TAM_VIN__c))
                                    if (mapVinTotMovCanAut.get(objCheckOutPaso.TAM_VIN__c) == 0 && !blnTieneMovMesAnt)
                                       intTotVta = 1;
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol YA EXISTE EN EL MAPA sIdExterno: ' + sIdExterno + ' intTotVta: ' + intTotVta);
                                //Crea el objeto de objeto del tipoTAM_MovimientosSolicitudes__c
                                TAM_MovimientosSolicitudes__c objMovSolAuto = new TAM_MovimientosSolicitudes__c(
                                    Name = sNameSol + ' - ' + strDtSolAutoCierre,
                                    TAM_EstatusSolicitud__c = 'Autorizada',                                    
                                    TAM_IdExternoSFDC__c = sIdExterno,
                                    TAM_Total__c = intTotVta,
                                    TAM_FechaMovimiento__c = mapIdSolName.get(sIdSolAuto).TAM_FechaCierreSolicitudForm__c,                                    
                                    TAM_VIN__c = objCheckOutPaso.TAM_VIN__c,
                                    TAM_CheckOutSolicitud__c = objCheckOutPaso.id
                                );
                                System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol objMovSolAuto4: ' + objMovSolAuto);
                                //Agregalo al mapa de MapMovSolUps
                                MapMovSolUps.put(sIdExterno, objMovSolAuto);                               
                            }//Fin si mapVinTotMovAut.containsKey(objCheckOutPaso.TAM_VIN__c)
                                           
		                } //Fin si mapIdSolName.get(sIdSolAuto).TAM_FechaCierreSolicitudForm__c != null
		            //}//Fin si mapVinIdSolAuto.containsKey(objCheckOutPaso.TAM_VIN__c)

                }//Fin si no esta cancelada
            }//Fin si objCheckOutPaso.TAM_TipoVenta__c == 'Inventario'
        }//Fin del for para lCheckOutSolCom
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinIdSol: ' + mapVinIdSol.keyset());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinIdSol: ' + mapVinIdSol.values());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinFechCancela: ' + mapVinFechCancela.keyset());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinFechCancela: ' + mapVinFechCancela.values());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinIdSolAuto: ' + mapVinIdSolAuto.keyset());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol mapVinIdSolAuto: ' + mapVinIdSolAuto.values());
        
        }//Fin si blnTAM_ActualizaMovimientosSolicitud || Test.isRunningTest()
                
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps: ' + MapMovSolUps.keyset());
        System.debug('EN TAM_CheckOutDetalleSolicitudComHandler.actualizaMovSol MapMovSolUps: ' + MapMovSolUps.values());

        //Un objeto del tipo Savepoint
        Savepoint sp = Database.setSavepoint();        

        //Ya tienes el mapa de MapMovSolUps
        List<Database.Upsertresult> lDtbUpsRes = Database.upsert(MapMovSolUps.values(), TAM_MovimientosSolicitudes__c.TAM_IdExternoSFDC__c, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess())
                System.debug('EN actualizaMovSol Hubo un error a la hora de crear/Actualizar los registros en Detalle Orden ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
        }//Fin del for para lDtbUpsRes

        //Roleback a todo
        //Database.rollback(sp);
        
    }

    
}