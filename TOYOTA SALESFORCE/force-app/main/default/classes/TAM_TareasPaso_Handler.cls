/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para procesar los registros del 
                        objeto TAM_TareasPaso_Handler

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             Autor                       Descripcion
    1.0    26-MAYO-2021      Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_TareasPaso_Handler extends TriggerHandler{

    private List<TAM_TareasPaso__c> CteServList;
    private Map<Id,TAM_TareasPaso__c> mapCtesServ;
    
    public TAM_TareasPaso_Handler() {
        this.CteServList = Trigger.new;
        this.mapCtesServ = (Map<Id,TAM_TareasPaso__c>)Trigger.newMap;
    }

    public override void afterInsert(){
        buscaRegistrosTareas(CteServList);
    }

    public override void afterUpdate(){
        buscaRegistrosTareas(CteServList);
    }

    public static void buscaRegistrosTareas(List<TAM_TareasPaso__c> CteServList){
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas...');
        Map<String, String> mapIdLeadIdProp = new Map<String, String>();
        Set<String> setIdLead = new Set<String>();
        List<Task> lTareasUpd = new List<Task>();
        
        //Toma la lista de los reg que vienen en CteServList
        for (TAM_TareasPaso__c objTareasPaso : CteServList){
            //Toma los el canpo TAM_IdRegRelacionado__c
            if (objTareasPaso.TAM_IdRegRelacionado__c.startsWith('00Q'))
               setIdLead.add(objTareasPaso.TAM_IdRegRelacionado__c);            
        }
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas setIdLead: ' + setIdLead);

        //Cossulta los datos de los Leads asociados a setIdLead y metelo al mapa de mapIdLeadIdProp
        for (Lead objCand : [Select id, OwnerId From Lead Where ID IN:setIdLead]){
            mapIdLeadIdProp.put(objCand.id, objCand.OwnerId);
        }
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas mapIdLeadIdProp: ' + mapIdLeadIdProp.KeySet());
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas mapIdLeadIdProp: ' + mapIdLeadIdProp.values());

        //Recorre la lista de nuevo y crear los reg de las tareas que vas a actueliar
        for (TAM_TareasPaso__c objTareasPaso : CteServList){
            //Toma los el canpo TAM_IdRegRelacionado__c
            if (objTareasPaso.TAM_IdRegRelacionado__c.startsWith('00Q')){
                //Ve si el lead TAM_IdRegRelacionado__c tiene el mismo prop que TAM_IdPropietario__c
                if (mapIdLeadIdProp.containsKey(objTareasPaso.TAM_IdRegRelacionado__c))
                    if (mapIdLeadIdProp.get(objTareasPaso.TAM_IdRegRelacionado__c) != objTareasPaso.TAM_IdPropietario__c)
                        lTareasUpd.add(new Task(id = objTareasPaso.TAM_IdTarea__c, OwnerId = mapIdLeadIdProp.get(objTareasPaso.TAM_IdRegRelacionado__c) ));                        
            }//Fin si objTareasPaso.TAM_IdRegRelacionado__c.startsWith('00Q')
        }
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas lTareasUpd: ' + lTareasUpd);

        //Ve si tiene algo la lista de lTareasUpd y mandala a actualizar
        if (!lTareasUpd.isEmpty())   
            actalizaRegistrosTareas(lTareasUpd);                  
    }

    public static void actalizaRegistrosTareas(List<Task> lTareasUpd){
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas...');
        List<TAM_LogsErrores__c> lError = new List<TAM_LogsErrores__c>();        
        Integer inCntCont = 0;
        
        //Recorre la lista de Opp y busca el VIN para asociar el id de la Cand en AccountId
        List<Database.Upsertresult> lDtbUpsRes = Database.upsert(lTareasUpd, Task.id, false);
        //Ve si hubo error
        for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
            if (!objDtbUpsRes.isSuccess())
                System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas error: ' + objDtbUpsRes.getErrors()[0].getMessage());
            if (objDtbUpsRes.isSuccess())
                lError.add(new TAM_LogsErrores__c(TAM_Proceso__c = 'Actualiza Propietario Tarea', TAM_Fecha__c = Date.today(), TAM_DetalleError__c = 'objTarea id: ' + objDtbUpsRes.getId() + ' - ' + lTareasUpd.get(inCntCont) ) );
            inCntCont++;
        }//Fin del for para lDtbUpsRes
        System.debug('EN TAM_TareasPaso_Handler.actalizaRegistrosTareas lError: ' + lError);
        //Crea los reg en el log
        if (!lError.isEmpty())
            insert lError;
        
    }
        
}