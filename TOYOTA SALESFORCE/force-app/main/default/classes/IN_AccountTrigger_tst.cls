/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Samuel Galindo Rodríguez
Proyecto: Toyota Incentivos
Descripción: Clase de prueba para el trigger IN_ListaNegra_tgr
-------------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------
0.1    09-05-2018 Samuel Galindo Rodríguez   Creación
*******************************************************************************/
@isTest
private with sharing class IN_AccountTrigger_tst {
	// Caso 1: Elimina el accountshare cuenta fisica
	@isTest static void testCuentaFisica() {

		Test.startTest();

			RecordType objPersonaFisica = [
				SELECT Id, DeveloperName
				FROM RecordType
				WHERE SobjectType = 'Account' AND DeveloperName = 'ClientePersonaFisica'
				LIMIT 1
			];

			Account objAccount = new Account();
			objAccount.FirstName = 'Valerie';
			objAccount.LastName	= 'Duncan';
			objAccount.RecordTypeId	= objPersonaFisica.Id;
			objAccount.IN_Estatus__pc = 'Boletinado';
			insert objAccount;

			objAccount.IN_Estatus__pc = 'No_Boletinado';
			upsert objAccount;
		Test.stopTest();
	}
}