@isTest
public class test_CountAttachments {
    
  @isTest static void testCarga() {
         
        RecordType rectype = [select id,DeveloperName from RecordType where DeveloperName = :'Landing_Page' and SobjectType = 'Lead'];   	
        Lead l = new Lead();
        l.RecordTypeId = rectype.id;
        l.FirstName = 'Test';
        l.FWY_Intencion_de_compra__c = 'Este mes';	
        l.Email = 'as@a.com';
        l.phone = '225';
        l.Status='Nuevo Lead';
        l.LastName = 'Test1';
        l.FWY_codigo_distribuidor__c = '57050';
      insert l;

      ContentVersion content=new ContentVersion(); 
            content.Title='Header_Picture1_Test_class'; 
            content.PathOnClient='/' + content.Title + '.jpg'; 
            Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
            content.VersionData=bodyBlob; 
            //content.LinkedEntityId=sub.id;
            content.origin = 'H';
        insert content;
      
        ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId=l.Id;
            contentlink.contentdocumentid=[select contentdocumentid from contentversion where id =: content.id].contentdocumentid;
            contentlink.ShareType = 'V';
            test.starttest();
      insert contentlink;
      
        ContentDocument idImg = [SELECT Id FROM ContentDocument WHERE Title='Header_Picture1_Test_class'];
        delete idImg;
      //  delete contentlink;
      
      test.stoptest();
}
}