/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para el servicio de SFDC_ConsultaClientesSunroad_ws_rst

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    18-Oct-2021          Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SFDC_ConsultaClientesSunroad_ws_rst_tst {

    static testMethod void SFDC_ConsultaClientesSunroad_ws_rst_OK() {
        Date dtFechaConsulta = Date.today();
        String strFechaConsulta = String.valueOf(dtFechaConsulta);
        System.debug('EN SFDC_ConsultaCtesParaFacturarCtrlRstOK strFechaConsulta: ' + strFechaConsulta);
        String strBody = '{"IdUnicoUserSFDC": "566hj1056gd-89be2-221220-af82-54981754bcc0","NumeroDealer": "57039","blnTodosDistRel": true,"strFechaIniCons": "'+ strFechaConsulta +'","strFechaFinCons": "'+ strFechaConsulta +'"}';
        
        RestRequest req         =   new RestRequest(); 
        RestResponse resp       =   new RestResponse();
        req.requestURI          =   'https://cs41.salesforce.com/services/apexrest/SFDC_ConsultaClientesSunroad_ws_rst';
        req.httpMethod          =   'POST';
        req.requestBody         =   Blob.valueOf(strBody);
        RestContext.request     =   req;
        RestContext.response    =   resp;
        SFDC_ConsultaClientesSunroad_ws_rst.ConsultaClientesParaSunroadRst();
        
        system.assertEquals(strBody, '{"IdUnicoUserSFDC": "566hj1056gd-89be2-221220-af82-54981754bcc0","NumeroDealer": "57039","blnTodosDistRel": true,"strFechaIniCons": "'+ strFechaConsulta +'","strFechaFinCons": "'+ strFechaConsulta +'"}');    
        
    }
}