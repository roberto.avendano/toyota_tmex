/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase controladora para el componente TAM_ReasignaMasivaLeadsCtlr
                        que sirve para reasignar Lead de forma masiva.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    12-Mayo-2021         Héctor Figueroa             Creación
******************************************************************************* */

public with sharing class TAM_ReasignaMasivaLeadsCtlr {

    public class wrpCandidatos{
        @AuraEnabled public String sIdCand {get;set;}
        @AuraEnabled public String sName {get;set;}
        @AuraEnabled public String sVehiculo {get;set;}
        @AuraEnabled public String sCodDist {get;set;}
        @AuraEnabled public String sCodDistOrig {get;set;}        
        @AuraEnabled public String sNomPropKban {get;set;}
        @AuraEnabled public DateTime sFechaCreacion {get;set;}
        @AuraEnabled public String sOrigenCand {get;set;}
        @AuraEnabled public String sEstadoCand {get;set;}
        @AuraEnabled public String sTipoReg {get;set;}
        @AuraEnabled public String sTelefono {get;set;}
        
        public wrpCandidatos(){
            this.sIdCand = '';
            this.sName = '';
            this.sVehiculo = '';
            this.sCodDist = '';
            this.sCodDistOrig = '';            
            this.sNomPropKban = '';
            this.sFechaCreacion = DateTime.now();
            this.sOrigenCand = '';
            this.sEstadoCand = '';
            this.sTipoReg = '';
            this.sTelefono = '';
        }
        
        public wrpCandidatos(String sIdCand, String sName, String sVehiculo, String sNomPropKban, 
            DateTime sFechaCreacion, String sOrigenCand, String sEstadoCand, String sTipoReg, 
            String sCodDist, String sTelefono){
            this.sIdCand = sIdCand;
            this.sName = sName;
            this.sVehiculo = sVehiculo;
            this.sCodDist = sCodDist;
            this.sNomPropKban = sNomPropKban;
            this.sFechaCreacion = sFechaCreacion;
            this.sOrigenCand = sOrigenCand;
            this.sEstadoCand = sEstadoCand;
            this.sTipoReg = sTipoReg;
            this.sTelefono = sTelefono;
        }
    }

    public class wrpListaDealersUsuarios{
        @AuraEnabled public List<wrpListaUsuarios> lWrpListaUsuarios {get;set;} 
        @AuraEnabled public List<wrpListaDealers> lWrpListaDealers {get;set;} 
                
        //Un contrusctor con parametros
        public wrpListaDealersUsuarios(List<wrpListaUsuarios> lWrpListaUsuarios, 
            List<wrpListaDealers> lWrpListaDealers){
            this.lWrpListaUsuarios = lWrpListaUsuarios;
            this.lWrpListaDealers = lWrpListaDealers;
        }//Fin si wrpListaDealers       
    }

    public class wrpListaUsuarios{
        @AuraEnabled public String codigo {get;set;} 
        @AuraEnabled public String nombreUsuario {get;set;} 
                
        //Un contrusctor con parametros
        public wrpListaUsuarios(String codigo, String nombreUsuario){
            this.codigo = codigo;
            this.nombreUsuario = nombreUsuario;
        }//Fin si wrpListaDealers
        
    }

    public class wrpListaDealers{
        @AuraEnabled public String codigo {get;set;} 
        @AuraEnabled public String nombre {get;set;} 
                
        //Un contrusctor con parametros
        public wrpListaDealers(String codigo, String nombre){
            this.codigo = codigo;
            this.nombre = nombre;
        }//Fin si wrpListaDealers
        
    }

    //Obtener getDatosCandidato.
    @AuraEnabled
    public static Boolean getPerfilUserActual() {
        System.debug('En getPerfilUserActual...');

        Boolean bEsUserMerge = false;
        Boolean bEsUserAdmin = false;
        String sIdUser = UserInfo.getUserId();
        
        //Busca el perfil del usuario
        for (Profile usuario : [Select p.Name From Profile p
                where Name LIKE '%Administrador del sistema%'
                and id =:UserInfo.getProfileId()
                order by p.Name ]){
            bEsUserAdmin = true;
        }
        
        //Busca el Role en el portal
        for (User usuarioActual : [Select id, UserRoleId, UserRole.PortalRole, CodigoDistribuidor__c 
            From User Where Id =:UserInfo.getUserId() 
            And (UserRole.PortalRole = 'Executive' or UserRole.PortalRole = 'Manager')
            And TAM_ReasignacionMasivaLeads__c = true]){
            bEsUserAdmin = true;
        }
        
        System.debug('En getPerfilUserActual bEsUserAdmin: ' + bEsUserAdmin);
        return bEsUserAdmin;
    }

    //Obtener getDatosCandidato.
    @AuraEnabled
    public static wrpListaDealersUsuarios getUsuariosActivos (String IdClienteActual) {
        System.debug('En getUsuariosActivos  IdClienteActual: ' + IdClienteActual);
        String strCadenaDeBusqueda = '';
        String sCodDist;
        Set<String> setCodDist = new Set<String>();
        Map<String, set<String>> mapCondNombreUsr = new  Map<String, set<String>>();
        Map<String, String> mapCodDealerNombre = new  Map<String, String>();
        List<String> lNomCodDealer = new List<String>();
        
        List<wrpListaUsuarios> lWrpListaUsuarios = new List<wrpListaUsuarios>();
        List<wrpListaDealers> lWrpListaDealers = new List<wrpListaDealers>();
        wrpListaDealersUsuarios objWrpListaDealersUsuarios;
               
        //Busca el Role en el portal
        for (User usuarioActual : [Select id, CodigoDistribuidor__c 
            From User Where Id =:UserInfo.getUserId() ]){
            sCodDist = usuarioActual.CodigoDistribuidor__c;
        }
        
        //Ve si esta vacio el sCodDist inicializalo con 57000
        if (sCodDist == null)
            sCodDist = '57000';
        
        //Busca los diferentes Cod de Delaer a los que puede consultar en Lead
        //Consulta todos los reg asociados a setIdcteId en Opp
        for (AggregateResult aggrCodDist : [ SELECT TAM_DealerOrigen__c, COUNT(ID)
            FROM Lead Group By TAM_DealerOrigen__c]){
            //Mete a un mapa el total
            String sIdCodDist = (String) aggrCodDist.get('TAM_DealerOrigen__c');
            if (sIdCodDist != null && sIdCodDist != '')
                setCodDist.add(sIdCodDist);
        }
        System.debug('EN getUsuariosActivos setCodDist: ' + setCodDist);        

        //Consulta los datos de los usuarios en la tabla de 
        for (TAM_UsuariosSeguimientoLead__c usrSeg : [Select id, TAM_NoDistribuidor__c, 
            TAM_NombreCompleto__c, TAM_NombreDistribuidor__c From TAM_UsuariosSeguimientoLead__c 
            Where TAM_NoDistribuidor__c IN:setCodDist and TAM_ActivoSFDC__c = true 
            Order by TAM_NombreCompleto__c]){
            if (usrSeg.TAM_NombreCompleto__c != null){
                String sNomCodDea = usrSeg.TAM_NombreCompleto__c + '-' + usrSeg.TAM_NoDistribuidor__c;
                lNomCodDealer.add(sNomCodDea);
	            //Metelo al mapa de mapCondNombreUsr
	            if (mapCondNombreUsr.containsKey(usrSeg.TAM_NoDistribuidor__c))
	                mapCondNombreUsr.get(usrSeg.TAM_NoDistribuidor__c).add(usrSeg.TAM_NombreCompleto__c);
	            if (!mapCondNombreUsr.containsKey(usrSeg.TAM_NoDistribuidor__c))
	                mapCondNombreUsr.put(usrSeg.TAM_NoDistribuidor__c, new set<String>{usrSeg.TAM_NombreCompleto__c});        
	            //Metelo al mapa de mapCodDealerNombre
	            mapCodDealerNombre.put(usrSeg.TAM_NoDistribuidor__c, usrSeg.TAM_NombreDistribuidor__c);
            }//Fin si usrSeg.TAM_NombreCompleto__c != null
        }//Fin del for para TAM_UsuariosSeguimientoLead__c
        System.debug('EN getUsuariosActivos mapCondNombreUsr: ' + mapCondNombreUsr);        
        System.debug('EN getUsuariosActivos mapCodDealerNombre: ' + mapCodDealerNombre);        
        
        List<String> lDealers = new List<String>();
        //Llena la lista de lWrpListaUsuarios
        for (String sCod : mapCodDealerNombre.KeySet()){
            String sNomCodDist = sCod + ' ' + mapCodDealerNombre.get(sCod);            
            lDealers.add(sNomCodDist);
        }
        lDealers.sort();
        
        //Recorre la lista de lDealers
        for (String sDealerPaso : lDealers){
            //Toma el numero de dealer
            String sNoDealer = sDealerPaso.leftPad(5);
            System.debug('EN getUsuariosActivos sNoDealer: ' + sNoDealer);
            lWrpListaDealers.add(new wrpListaDealers(sNoDealer, sDealerPaso));                 
        }
                        
        //Recorre la lista de lNomCodDealer
        for (String sNomCodDealerPaso : lNomCodDealer){
            //Toma el numero de dealer
            String[] arrNomCodDealerPaso = sNomCodDealerPaso.split('-');
            System.debug('EN getUsuariosActivos arrNomCodDealerPaso: ' + arrNomCodDealerPaso);
            lWrpListaUsuarios.add(new wrpListaUsuarios(arrNomCodDealerPaso[0], arrNomCodDealerPaso[0] + ' ' + arrNomCodDealerPaso[1]));                 
        }
        
        //Agrelos a la lista de lWrpListaDealersUsuarios
        objWrpListaDealersUsuarios = new wrpListaDealersUsuarios(lWrpListaUsuarios, lWrpListaDealers);        
        System.debug('EN getUsuariosActivos objWrpListaDealersUsuarios: ' + objWrpListaDealersUsuarios);        
                
        //Regresa la lista de Dealers
        return objWrpListaDealersUsuarios;
        
    }
        
    //Obtener getDatosCandidato.
    @AuraEnabled
    public static List<wrpCandidatos> buscaCandidatos(String sPropietario, String sDistribuidor, 
        String strCadenaDeBusqueda, String defaultPrm, String totCandConsPrm) {
        System.debug('EN buscaCandidatos sPropietario: ' + sPropietario + ' sDistribuidor: ' + sDistribuidor + ' strCadenaDeBusqueda: ' + strCadenaDeBusqueda + ' defaultPrm: ' + defaultPrm + ' totCandConsPrm: ' + totCandConsPrm);
        Boolean blnDefaultPrm = defaultPrm != null ? Boolean.valueOf(defaultPrm) : false;
        Integer intTotCandConsPrm = totCandConsPrm != null ? Integer.valueOf(totCandConsPrm) : 100;
        List<wrpCandidatos> lCandidatos = new List<wrpCandidatos>(); 
            
        TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
        objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los datos se estan actualizado y deben quedar listos en 10 minutos.');     
        String CadenaBusqueda = '';

        //Una busqueda por default
        if (blnDefaultPrm){
            System.debug('EN buscaCandidatos blnDefaultPrm: ' + blnDefaultPrm + ' intTotCandConsPrm: ' + intTotCandConsPrm);
            for (Lead objLead : [Select id, Name, FirstName, LastName, FWY_Vehiculo__c, TAM_NomPropKban__c,
                CreatedDate, LeadSource, Status, RecordType.Name, FWY_codigo_distribuidor__c, Phone, TAM_DealerOrigen__c  
                From Lead Order by CreatedDate Limit :intTotCandConsPrm]){
                //Crea un objeto del tipo wrpCandidatos
                wrpCandidatos objPasowrpCandidatos = new wrpCandidatos(objLead.id, objLead.Name, objLead.FWY_Vehiculo__c,
                    objLead.TAM_NomPropKban__c, objLead.CreatedDate, objLead.LeadSource, objLead.Status,
                    objLead.RecordType.Name, objLead.FWY_codigo_distribuidor__c, objLead.Phone);
                 //Inicializa el cammpo de sCodDistOrig
                 objPasowrpCandidatos.sCodDistOrig = objLead.TAM_DealerOrigen__c != null ? objLead.TAM_DealerOrigen__c : '';
                //Metelos a la lista de lCandidatos
                lCandidatos.add(objPasowrpCandidatos);
            }
        }

        //Si es por el propietario
        if (sPropietario != null && sPropietario != '' && sPropietario != '57999'){
            CadenaBusqueda = sPropietario; 
            System.debug('EN buscaCandidatos CadenaBusqueda1: ' + CadenaBusqueda);
            for (Lead objLead : [Select id, Name, FirstName, LastName, FWY_Vehiculo__c, TAM_NomPropKban__c,
                CreatedDate, LeadSource, Status, RecordType.Name, FWY_codigo_distribuidor__c, Phone, TAM_DealerOrigen__c 
                From Lead Where TAM_NomPropKban__c =:CadenaBusqueda Order by CreatedDate]){
                //Crea un objeto del tipo wrpCandidatos
                wrpCandidatos objPasowrpCandidatos = new wrpCandidatos(objLead.id, objLead.Name, objLead.FWY_Vehiculo__c,
                    objLead.TAM_NomPropKban__c, objLead.CreatedDate, objLead.LeadSource, objLead.Status,
                    objLead.RecordType.Name, objLead.FWY_codigo_distribuidor__c, objLead.Phone);
                 //Inicializa el cammpo de sCodDistOrig
                 objPasowrpCandidatos.sCodDistOrig = objLead.TAM_DealerOrigen__c != null ? objLead.TAM_DealerOrigen__c : '';
                //Metelos a la lista de lCandidatos
                lCandidatos.add(objPasowrpCandidatos);
            }
        }

        //Si es por el codigo del dist
        if (sDistribuidor != null && sDistribuidor != '' && sDistribuidor != '57999'){
            CadenaBusqueda = sDistribuidor.left(5);
            System.debug('EN buscaCandidatos CadenaBusqueda2: ' + CadenaBusqueda);
            for (Lead objLead : [Select id, Name, FirstName, LastName, FWY_Vehiculo__c, TAM_NomPropKban__c,
                CreatedDate, LeadSource, Status, RecordType.Name, FWY_codigo_distribuidor__c, Phone, TAM_DealerOrigen__c 
                From Lead Where FWY_codigo_distribuidor__c =:CadenaBusqueda Order by CreatedDate]){
                //Crea un objeto del tipo wrpCandidatos
                wrpCandidatos objPasowrpCandidatos = new wrpCandidatos(objLead.id, objLead.Name, objLead.FWY_Vehiculo__c,
                    objLead.TAM_NomPropKban__c, objLead.CreatedDate, objLead.LeadSource, objLead.Status,
                    objLead.RecordType.Name, objLead.FWY_codigo_distribuidor__c, objLead.Phone);
                 //Inicializa el cammpo de sCodDistOrig
                 objPasowrpCandidatos.sCodDistOrig = objLead.TAM_DealerOrigen__c != null ? objLead.TAM_DealerOrigen__c : '';
                //Metelos a la lista de lCandidatos
                lCandidatos.add(objPasowrpCandidatos);
            }
        }//Fin si sDistribuidor != null && sDistribuidor != ''

        //Si es por una frase en particula
        if (strCadenaDeBusqueda != null && strCadenaDeBusqueda != ''){
            CadenaBusqueda = strCadenaDeBusqueda;

            //Busca el nombre del cliente en la base de datos
            List<List<SObject>> searchListLeadRel = [FIND :strCadenaDeBusqueda IN ALL FIELDS RETURNING Lead(id, Name, FirstName, LastName, FWY_Vehiculo__c, TAM_NomPropKban__c, CreatedDate, LeadSource, Status, RecordType.Name, FWY_codigo_distribuidor__c, Phone Order by CreatedDate)];
            System.debug('EN validacionBoletinado CadenaBusqueda: ' + CadenaBusqueda + ' searchListLeadRel: ' + searchListLeadRel + ' searchListLeadRel.isEmpty(): ' + searchListLeadRel.isEmpty() + ' searchListLeadRel.size(): ' + searchListLeadRel.size());
            
            List<Lead> searchLeadsRel = (List<Lead>) searchListLeadRel[0];
            //Ve si encontraste Cands
            if(!searchLeadsRel.isEmpty()){
                //Recorre la lista de reg de las Cands que fueron encontradas
                for (Lead objLead : searchLeadsRel){
                    System.debug('EN buscaCandidatos ENCONTRO COINCIDENCIAS PARA objLead: ' + objLead);
	                //Crea un objeto del tipo wrpCandidatos
	                wrpCandidatos objPasowrpCandidatos = new wrpCandidatos(objLead.id, objLead.Name, objLead.FWY_Vehiculo__c,
	                    objLead.TAM_NomPropKban__c, objLead.CreatedDate, objLead.LeadSource, objLead.Status,
	                    objLead.RecordType.Name, objLead.FWY_codigo_distribuidor__c, objLead.Phone);
	                //Metelos a la lista de lCandidatos
	                lCandidatos.add(objPasowrpCandidatos);
                }//Fin del for para searchLeadsRel           
            }//Fin si   !searchLeadsRel.isEmpty()
            System.debug(LoggingLevel.INFO,'EN buscaCandidatos creaClienteCorporativo CandRelac: ');
        }//Fin si strCadenaDeBusqueda != null && strCadenaDeBusqueda != ''

        System.debug('EN buscaCandidatos lCandidatos: ' + lCandidatos);
            
        //Regresa el objeto con la respuesta
        return lCandidatos;
    }

    /*public static void actualizaDatosClienteFinal(String IdRegistroPrincipal, String strClienteSeleccionadoMerge){
        System.debug('EN TAM_ReasignaMasivaLeadsCtlr actualizaDatosClienteFinal strClienteSeleccionadoMerge: ' + strClienteSeleccionadoMerge + ' IdRegistroPrincipal: ' + IdRegistroPrincipal);
        
        //Crea el objeto para TAM_SolicitudesParaAprobar__c
        TAM_SolicitudesParaAprobar__c objSolUpd = new TAM_SolicitudesParaAprobar__c(id = IdRegistroPrincipal,
            TAM_ClienteCorporativoRef__c = strClienteSeleccionadoMerge);

        //Un obheto del tipo Savepoint para hacer rollback
        Savepoint sp = Database.setSavepoint();        
        
        //Actualiza la solicitud
        if (!Test.isRunningTest())
            update objSolUpd;

        //Ve si la etiqueta de TAM_HabilitaMergeAccounts
        String sHabilitaMergeAccounts = System.Label.TAM_HabilitaMergeAccounts;
        Boolean bHabilitaMergeAccounts = Boolean.valueOf(sHabilitaMergeAccounts);
        System.debug('EN actualizaDatosClienteFinal bHabilitaMergeAccounts: ' + bHabilitaMergeAccounts);
        if (!bHabilitaMergeAccounts){
            System.debug('EN actualizaDatosClienteFinal bHabilitaMergeAccounts ROLLBACK: ' + bHabilitaMergeAccounts);   
            Database.rollback(sp);
        }
    }*/

    //Obtener getDatosCandidato.
    @AuraEnabled            
    public static TAM_WrpResultadoActualizacion tamConfirmaReasignacion(String sPropietarioFinalPrm, 
        List<TAM_ReasignaMasivaLeadsCtlr.wrpCandidatos> lCandidSelecPrm){
        System.debug('EN TAM_ReasignaMasivaLeadsCtlr tamConfirmaReasignacion sPropietarioFinalPrm: ' + sPropietarioFinalPrm);
        System.debug('EN TAM_ReasignaMasivaLeadsCtlr tamConfirmaReasignacion lCandidSelecPrm: ' + lCandidSelecPrm);
        
        //Map para actualizar a los lead con el nuevo propietario
        Map<String, Lead> mapIdLeadObLeadUpd = new Map<String, Lead>();
        TAM_WrpResultadoActualizacion objRespoPaso = new TAM_WrpResultadoActualizacion();
        objRespoPaso = new TAM_WrpResultadoActualizacion(false, 'Los registros se reasignaron con exito, espera 3 minutos para revisar los cambios.');

        //Un obheto del tipo Savepoint para hacer rollback
        Savepoint sp = Database.setSavepoint();        
        try{
        
	        //Toma la lista lCandidSelecPrm y crea los reg de los 
	        for (TAM_ReasignaMasivaLeadsCtlr.wrpCandidatos objWrpCand : lCandidSelecPrm){
	            //Agregalo a la lista de mapIdLeadObLeadUpd
	            mapIdLeadObLeadUpd.put(objWrpCand.sIdCand, new Lead(
	                    id = objWrpCand.sIdCand,
	                    TAM_NuevoPropietario__c = sPropietarioFinalPrm
	                )
	            );
	        }//Fin del for para lCandidSelecPrm
	        System.debug('EN TAM_ReasignaMasivaLeadsCtlr tamConfirmaReasignacion mapIdLeadObLeadUpd: ' + mapIdLeadObLeadUpd.KeySet());        
	        System.debug('EN TAM_ReasignaMasivaLeadsCtlr tamConfirmaReasignacion mapIdLeadObLeadUpd: ' + mapIdLeadObLeadUpd.Values());        
                    
            //Ve si tiene algo la lista de mapInvDummyPasoUpd
            if (!mapIdLeadObLeadUpd.isEmpty()){
                //Recorre la lista de Opp y busca el VIN para asociar el id de la Cand en AccountId
                List<Database.Upsertresult> lDtbUpsRes = Database.upsert(mapIdLeadObLeadUpd.values(), Lead.id, false);
                //Ve si hubo error
                for (Database.Upsertresult objDtbUpsRes : lDtbUpsRes){
                    if (!objDtbUpsRes.isSuccess())
                        objRespoPaso = new TAM_WrpResultadoActualizacion(true, 'EN tamConfirmaReasignacion Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());                    
                        //System.debug('EN tamConfirmaReasignacion Hubo un error a la hora de crear/Actualizar los registros en Lead ERROR: ' + objDtbUpsRes.getErrors()[0].getMessage());
                    //if (objDtbUpsRes.isSuccess())
                    //    System.debug('EN TAM_ReasignaMasivaLeadsCtlr Los datos de la SolFlotProg se crear/Actualizar con exito objDtbUpsRes.getId(): ' + objDtbUpsRes.getId());
                }//Fin del for para lDtbUpsRes
            }//Fin si !mapInvDummyPasoUpd.isEmpty()

            //*** NO SE TE OLVIDE QUITAR ESTE COMENTARIO ES PARA PRUREBAS ****///
            //Database.rollback(sp);

        }catch(Exception e){
            System.debug('En tamConfirmaReasignacion error: ' + e.getMessage() + ' ' + e.getLineNumber());
            Database.rollback(sp);
        }
        
        System.debug('En tamConfirmaReasignacion objRespoPaso: ' + objRespoPaso);
        //Regresa el objeto objRespoPaso;
        return objRespoPaso;
    }
    
}