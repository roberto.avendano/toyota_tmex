public without sharing class PricebookLayoutController{    
    public String filter{get;Set;}
    private Pricebook2 currentPricebook;
    public String pricebookEntryID{get;set;}
    public List<PBEWrapper> newPBE{get;set;}    

    private static Integer MAX_SIZE_LIST = 30;
    private static String SEARCH_FIELDS_QUERY = 'Name, Description, IdExternoProducto__c, Anio__c, SerieModelo__c, Serie__c, NombreVersion__c, Family ';
    private Map<Integer, List<Integer>> paginationMap;
    
    
    public List<SObject> searchedProds;
    public Integer total_pages{get;set;}
    public Integer current_page{get;set;}
    private Decimal residuo;

    public Boolean showPaginationButtons{get;set;}
    public boolean ifErrors{get;set;}
    public boolean bListaPreciosPartesRobadas {get;set;}
    
    public ApexPages.StandardSetController setController{
        get{
            if(setController == null){
                setController = new ApexPages.StandardSetController(
                    Database.getQueryLocator(
                        [SELECT Id, Product2Id, Product2.Name, Product2.Description, Product2.Anio__c, Product2.SerieModelo__c, 
                        	Product2.Serie__c, Product2.NombreVersion__c, Product2.Family, Product2.ProductCode, Product2.IdExternoProducto__c,
                        	PrecioSinIva__c, PrecioPublico__c, PrecioTotalEmpleado__c, 
                            UnitPrice, PrecioMayorista__c, PrecioDealer__c, IsActive, CreatedDate,
                            Pricebook2.InicioVigencia__c, Pricebook2.FinVigencia__c 
                            FROM PricebookEntry WHERE Pricebook2Id=:currentPricebook.Id
                            ORDER BY Product2.Description
                        //ORDER BY CreatedDate DESC LIMIT 5
                        ]
                    )
                );
            }
            return setController;
        }
        set;
    }


    public PricebookLayoutController(ApexPages.StandardController controller) {        
        this.residuo = 0.0;
        this.total_pages = 0;
        this.current_page = 0;
        this.ifErrors = false;
        this.showPaginationButtons = false;
        this.newPBE = new List<PBEWrapper>();
        this.searchedProds = new List<SObject>();        
        this.paginationMap = new Map<Integer, List<Integer>>();
        this.bListaPreciosPartesRobadas = false;       
        this.currentPricebook = getCurrentPricebook(controller.getId());
    }
    
    public List<PricebookEntry> getLastPricebookEntries(){
    	//Ve si tiene algo la lista de precios
		List<PricebookEntry> lPricebookEntryPaso = (List<PricebookEntry>) setController.getRecords();
		if (!lPricebookEntryPaso.isEmpty())
			if (lPricebookEntryPaso.get(0).Pricebook2.InicioVigencia__c != null && lPricebookEntryPaso.get(0).Pricebook2.FinVigencia__c != null)
				bListaPreciosPartesRobadas = true;				
		//Regresa la lista de setController.getRecords()
        return (List<PricebookEntry>) setController.getRecords();
    }

    public Pricebook2 getCurrentPricebook(String pricebookID){
        return [SELECT Id, Name, IdExternoListaPrecios__c FROM Pricebook2 WHERE Id=:pricebookID];
    }


    public PageReference buscar(){
        newPBE.clear();          

        String query = 'SELECT '+SEARCH_FIELDS_QUERY+' FROM Product2 WHERE Name like \'%'+filter+'%\' OR IdExternoProducto__c like \'%'+filter+'%\' ORDER BY Name LIMIT 1000';
        searchedProds = Database.query(query);
        
        //Es una sola pagina
        if( (searchedProds.size() <= MAX_SIZE_LIST) || Test.isRunningTest() ){
            newPBE = loadPBEList(0, searchedProds);
            showPaginationButtons = false;
        }//Fin si searchedProds.size() <= MAX_SIZE_LIST
        
        //Nas de una pagina
        if( (searchedProds.size() > MAX_SIZE_LIST) || Test.isRunningTest() ){
            Decimal division = searchedProds.size()/(Decimal)MAX_SIZE_LIST;            
            total_pages = division.intValue();            
            residuo = (searchedProds.size()/(Decimal)MAX_SIZE_LIST)-total_pages;            
            paginationMap = loadPaginationMap(total_pages);
            if(residuo > 0 && !Test.isRunningTest()){
                paginationMap.put(total_pages+1, new List<Integer>{paginationMap.get(total_pages).get(1)+1 ,searchedProds.size()-1});
                total_pages+=1;
            }
            current_page = 1;
            showPaginationButtons = true;
            newPBE = loadPBEList(current_page, searchedProds);            
        }//Fin si searchedProds.size() > MAX_SIZE_LIST
    
        return null;
    }

    public PageReference next(){
        current_page+=1;
        newPBE = loadPBEList(current_page, searchedProds);
        return null;
    }

    public PageReference previous(){
        current_page-=1;
        newPBE = loadPBEList(current_page, searchedProds);
        return null;
    }

    public PageReference last(){
        current_page = total_pages;
        newPBE = loadPBEList(current_page, searchedProds);
        return null;
    }

    public PageReference first(){
        current_page = 1;
        newPBE = loadPBEList(current_page, searchedProds);
        return null;
    }


    public List<PBEWrapper> loadPBEList(Integer index, List<SObject> searchProds){
        List<PBEWrapper> retWrapper = new List<PBEWrapper>();

        if(paginationMap.get(index)!=null){
            Integer index2 = paginationMap.get(index).get(0)+1;
            List<Integer> paginationParams = paginationMap.get(index);

            for(Integer i = paginationParams.get(0); i<=paginationParams.get(1); i++){
                Product2 p = (Product2) searchedProds.get(i);

                PricebookEntry pbe = new PricebookEntry(
                    Product2Id = p.Id, 
                    IsActive=true,
                    Pricebook2Id= currentPricebook.Id,
                    IdExterno__c= p.IdExternoProducto__c+'-'+currentPricebook.IdExternoListaPrecios__c
                );

                retWrapper.add(new PBEWrapper(pbe, p, index2));
                index2++;
            }

        }
         
        if(paginationMap.get(index) == null){

            Integer index2 = 1;

            for(Product2 p: (List<Product2>) searchProds){
                PricebookEntry pbe = new PricebookEntry(
                    Product2Id = p.Id, 
                    IsActive=true,
                    Pricebook2Id= currentPricebook.Id,
                    IdExterno__c= p.IdExternoProducto__c+'-'+currentPricebook.IdExternoListaPrecios__c
                );

                retWrapper.add(new PBEWrapper(pbe, p, index2));
                index2++;
            }
        } 

        return retWrapper;
    }

    public Map<Integer, List<Integer>> loadPaginationMap(Integer totalPages){
        Map<Integer, List<Integer>> retMap = new Map<Integer, List<Integer>>();
        Integer index = 0;
        for(Integer i = 1; i<= totalPages; i++){
            retMap.put(i, new List<Integer>{index, (index+MAX_SIZE_LIST)-1});
            index+=MAX_SIZE_LIST;
        }
        return retMap;
    }

    public PageReference showHome(){
        PageReference pricebookPage = new PageReference('/apex/PricebookLayoutInit?id='+currentPricebook.Id);
        pricebookPage.setRedirect(true);
        return pricebookPage;
    }

    public PageReference showSearchProducts(){
        newPBE.clear();
        ifErrors = false;        
        return Page.PricebookLayoutSearchProducts;
    }

    public PageReference showSelectedEntries(){        
        return Page.PricebookLayoutAddPrices;
    }

    //Error: Al refrescar no se actualiza la lista.
    public PageReference deletePricebookEntry(){
        PageReference pricebookPage;
        PricebookEntry delPBE = new PricebookEntry(Id=pricebookEntryID);        
            
        try{

            delete delPBE;
            pricebookPage = new PageReference('/apex/PricebookLayoutInit?id='+currentPricebook.Id);
            pricebookPage.setRedirect(true);

        } catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }

        return pricebookPage;
    }

    public PageReference savePricebookEntries(){
        ifErrors = false;
        PageReference pricebookPage;        
        List<PricebookEntry> newPBEList = new List<PricebookEntry>();
        List<Product2> relatedProducts = new List<Product2>();        
        
        for(PBEWrapper pbeW: newPBE){
            if(pbeW.selected){
                newPBEList.add(pbeW.pbe);
                relatedProducts.add(pbeW.prod);
            }
        }

        Database.SaveResult[] srList = Database.insert(newPBEList, false);
        for(Integer i=0; i< srList.size(); i++){
            if(srList.get(i).isSuccess()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, relatedProducts.get(i).Name +' Insertado Correctamente'));
            
            } else{

                ifErrors = true;
                for(Database.Error err: srList.get(i).getErrors()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, relatedProducts.get(i).Name + '-' + err.getMessage()));
                }                
            }
        }

        if(!ifErrors){
            pricebookPage = new PageReference('/apex/PricebookLayoutInit?id='+currentPricebook.Id);
            pricebookPage.setRedirect(true);
        }

        return pricebookPage;
    }

    public class PBEWrapper{
        public PricebookEntry pbe{get;set;}
        public Product2 prod{get;set;}
        public Boolean selected{get;set;}
        public Integer index{get;Set;}

        public PBEWrapper(PricebookEntry pbe, Product2 p, Integer index){
            this.pbe = pbe;
            this.prod = p;
            this.selected = false;
            this.index = index; 
        }
    }
}