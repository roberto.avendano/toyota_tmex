/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba para crear la cobertura de la clase TAM_MergeAccountCRMCtrl.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    25-Agosto-2020       Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_MergeAccountCRMCtrlTest {

	static String sRectorTypePasoPersonaMoral = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Moral').getRecordTypeId();
	static String sRectorTypePasoPersonaFisica = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Cliente - Persona Fisica').getRecordTypeId();
	static String sRectorTypePasoDistribuidor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distribuidor').getRecordTypeId();

	static String sRectorTypePasoProductoUnidad = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Unidad').getRecordTypeId();
	
	static String sRectorTypePasoInventario = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Inventario').getRecordTypeId();
	static String sRectorTypePasoPedidoEspecial = Schema.SObjectType.TAM_DetalleOrdenCompra__c.getRecordTypeInfosByName().get('Pedido Especial').getRecordTypeId();
		
	static String sRectorTypePasoFlotilla = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Flotillas').getRecordTypeId();
	static String sRectorTypePasoPrograma = Schema.SObjectType.Rangos__c.getRecordTypeInfosByName().get('Programas').getRecordTypeId();

	static String VaRtOppRegVCrm = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ventas CRM').getRecordTypeId();
	static String sRectorTypePasoSolVentaCorporativa = Schema.SObjectType.TAM_SolicitudesFlotillaPrograma__c.getRecordTypeInfosByName().get('Venta Corporativa').getRecordTypeId();
		
	static	Account clienteDealer = new Account();
	static	List<Account> lclientePruebaMoral = new List<Account>();
	static	Account clientePruebaFisica = new Account();
	static	Account clientePruebaMoral = new Account();

	static	Contact contactoPrueba = new Contact();
	static	Rangos__c rango = new Rangos__c();
	static  Opportunity objOpp = new Opportunity();
	static  TAM_SolicitudesFlotillaPrograma__c solFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c();
	
	@TestSetup static void loadData(){

		Rangos__c rangoFlotilla = new Rangos__c(
			Name = 'AAA',
			NumUnidadesMinimas__c = 10,			
			NumUnidadesMaximas__c = 50,
			PerGraciaRango__c = 15,
			Activo__c = true,
			DiasVigentes__c = 180,
			ID_Externo__c = 'AAA | 10 | 50',
			RecordTypeId = sRectorTypePasoFlotilla
			 
		);
		insert rangoFlotilla;
		
		Account clienteDealer = new Account(
			Name = 'TOYOTA PRUEBA',
			Codigo_Distribuidor__c = '570550',			
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoDistribuidor
		);
		insert clienteDealer;

		Account clienteMoral = new Account(
			Name = 'CARSON',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id,
			TAM_IdExternoNombre__c = 'CARSON'
		);
		insert clienteMoral;

		Account clienteMora2 = new Account(
			Name = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaMoral,
			TAM_ProgramaRango__c = rangoFlotilla.id,
			TAM_IdExternoNombre__c = 'CARSON DEL SURESTE S.A. DE C.V.'
		);
		insert clienteMora2;

		Account clienteFisico = new Account(
			FirstName = 'CARSON DEL SURESTE S.A. DE C.V.',
			LastName = 'CARSON DEL SURESTE S.A. DE C.V.',
			UnidadesAutosDemoAutorizadas__c = 5,
			TAM_EstatusCliente__c = 'Preautorización',
			RecordTypeId = sRectorTypePasoPersonaFisica
		);
		insert clienteFisico;
		
		Contact cont = new Contact(
	    	LastName ='testCon',            
	        AccountId = clienteMoral.Id
	    );
	    insert cont;  

		Opportunity OpportunidadPaso = new Opportunity( 
			Name = 'XXXXXXXX1' + '-' + clienteMoral.Name,
			TAM_Vin__c = 'XXXXXXXX1',						
			CloseDate = Date.today(),
			StageName = 'Closed Won',
			Pricebook2Id = Test.getStandardPricebookId(),
			Amount = 0.00,
			TAM_IdExterno__c = 'XXXXXXXX1' + '-' + clienteMoral.Name,
			recordTypeId = VaRtOppRegVCrm,
			AccountId = clienteMora2.id
		);						 
		upsert OpportunidadPaso Opportunity.TAM_IdExterno__c;

		TAM_SolicitudesFlotillaPrograma__c solVentaFlotillaPrograma = new TAM_SolicitudesFlotillaPrograma__c(
			TAM_Cliente__c = clienteMora2.id,			
			TAM_Estatus__c = 'El Proceso',
			RecordTypeId = sRectorTypePasoSolVentaCorporativa,
			TAM_ProgramaRango__c = rangoFlotilla.id
		);
		insert solVentaFlotillaPrograma;
		             				
	}

	public static Integer aleatorio(Integer max){
    	Integer rand = Math.round(Math.random()*1000);
    	return Math.mod(rand, max);
	}

    static testMethod void TAM_MergeAccountCRMCtrlOK() {
		
		String IdRegistroPrincipal; 
		String strCadenaDeBusqueda = 'CARSON';
		
		//Consulta los datos del cliente
		lclientePruebaMoral = [Select Id, Name, TAM_ProgramaRango__c, TAM_IdExternoNombre__c From Account 
		Where recordTypeId =:sRectorTypePasoPersonaMoral Order by Name LIMIT 2];        
   		System.debug('EN TAM_MergeAccountCRMCtrlTest lclientePruebaMoral: ' + lclientePruebaMoral);
		for (Account cliente : lclientePruebaMoral){
			if (cliente.Name == 'CARSON'){
				IdRegistroPrincipal = cliente.id;
				clientePruebaMoral = cliente; 
			}
		}
		//Consulta los datos del cliente
		objOpp = [Select Id, TAM_VIN__c, AccountId, TAM_IdExterno__c From Opportunity LIMIT 1];        
   		System.debug('EN TAM_MergeAccountCRMCtrlTest objOpp: ' + objOpp);

		//Consulta los datos del cliente
		solFlotillaPrograma = [Select Id, Name From TAM_SolicitudesFlotillaPrograma__c LIMIT 1];        
   		System.debug('EN TAM_MergeAccountCRMCtrlTest solFlotillaPrograma: ' + solFlotillaPrograma);

		//Llama al metodo de getClientesCorporativos
		TAM_MergeAccountCRMCtrl.getClientesCorporativos(IdRegistroPrincipal, strCadenaDeBusqueda);
		
		//Crea una lista del tipo List<TAM_MergeAccountCRMCtrl.wrpListaClientes> 
		List<TAM_MergeAccountCRMCtrl.wrpListaClientes> lWrpListaClientes = 
			new List<TAM_MergeAccountCRMCtrl.wrpListaClientes>();
		//Crea un objeto del tipo TAM_MergeAccountCRMCtrl.wrpListaClientes	
		TAM_MergeAccountCRMCtrl.wrpListaClientes objWrpListaClientes = 
			new TAM_MergeAccountCRMCtrl.wrpListaClientes(clientePruebaMoral, true, false, IdRegistroPrincipal,
			true, '5', 'Persona Moral', 'Boletinado');
		//Un segundo objeto del tipo wrpListaClientes
		TAM_MergeAccountCRMCtrl.wrpListaClientes objWrpListaClientes2 = new TAM_MergeAccountCRMCtrl.wrpListaClientes();
			
		//Agrega a la lista de lWrpListaClientes el objetoobjWrpListaClientes
		lWrpListaClientes.add(objWrpListaClientes);

		//Account cliente, Boolean blnSeleccionado, Boolean blnBloqueado, String strIdCliente,
		//Boolean blnSeleccionaCte, String intTotalVehiculos, String strTipoCliente

		//Llama al metodo de getClientesCorporativos
		TAM_MergeAccountCRMCtrl.mergeClientes(IdRegistroPrincipal, IdRegistroPrincipal, 
			lWrpListaClientes, 'true');
        
    }
}