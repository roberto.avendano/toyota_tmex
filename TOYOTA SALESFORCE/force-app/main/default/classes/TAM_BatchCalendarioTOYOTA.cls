/******************************************************************************** 
Desarrollado por:   Globant de México
Autor:              Roberto Carlos Avendaño Quintana
Proyecto:           TOYOTA TAM
Descripción:        Clase que contiene la logica para el funcionamiento de TAM_BatchCalendarioTOYOTA
*************************************************************************/
global class TAM_BatchCalendarioTOYOTA implements Database.Batchable<sObject>, schedulable{
    List<TAM_CalendarioToyota__c> CalendarioTOYOTA ; 
    
    global TAM_BatchCalendarioTOYOTA() 
    {
        CalendarioTOYOTA = [Select name,TAM_Anio__c,TAM_FechaFin__c,TAM_FechaInicio__c,TAM_Periodo__c
                            FROM TAM_CalendarioToyota__c];
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select Id,TAM_AnioCalToyota__c,TAM_Fleet__c,TAM_MesCalToyota__c,TAM_TipoMovimiento__c,TAM_TipoRegistro__c FROM ConfigCheckOutDD__c';
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<ConfigCheckOutDD__c> paramList) {
        Date fechaHoy = date.today();
        Date periodoToyota;
        
        for(TAM_CalendarioToyota__c cal : CalendarioTOYOTA){
            if(fechaHoy.addDays(-1) >= cal.TAM_FechaInicio__c && fechaHoy <= cal.TAM_FechaFin__c){
                periodoToyota = cal.TAM_FechaInicio__c;
            }    
            
        }
        
        //Se actualiza el custom setting
        List<ConfigCheckOutDD__c> customSetting = new List<ConfigCheckOutDD__c>();
        
        for(ConfigCheckOutDD__c configParam : paramList){
            ConfigCheckOutDD__c updRecord = new ConfigCheckOutDD__c();
            updRecord.id = configParam.Id;
            updRecord.TAM_AnioCalToyota__c = String.valueOf(periodoToyota.year());
            updRecord.TAM_MesCalToyota__c =  String.valueOf(periodoToyota.month());
            customSetting.add(updRecord);
            
        }
        
        if(!customSetting.isEmpty()){
            system.debug('upd'+customSetting);
            update customSetting;
        }
        
    } 
    
    global void execute(SchedulableContext SC) {
        database.executebatch(new TAM_BatchCalendarioTOYOTA());
    }
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }
}