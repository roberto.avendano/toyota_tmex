/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que sirve para lanzar el proceso de TAM_DelShareAccMergSch y
    					eliminar los registros del objeto AccountShare para las cuentas que ya 
    					estan con la badera de merge
    					y para que nadielas vea

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    05-Octubre-2020	    Héctor Figueroa            	Creación
******************************************************************************* */

global with sharing class TAM_DelShareAccMergSch  implements Schedulable{

	global String sQuery {get;set;}
		
    global void execute(SchedulableContext ctx){
		System.debug('EN TAM_DelShareAccMergSch.execute...');
		
		//Crea la consulta para los Leads
		this.sQuery = ' Select Id, TAM_Merge__c From Account ';
		if (!Test.isRunningTest())
			this.sQuery += ' Where TAM_Merge__c = true ';
		//Si es una prueba
		if (Test.isRunningTest())
			this.sQuery += ' Limit 1';
		System.debug('EN TAM_DelShareAccMergSch.execute sQuery: ' + sQuery);
		
		//Crea el objeto de  OppUpdEnvEmailBch_cls   	
        TAM_DelShareAccMergBch objDelShareAccMergBch = new TAM_DelShareAccMergBch(sQuery);
        //No es una prueba entonces procesa de 1 en 1
        if (!Test.isRunningTest())
       		Id batchInstanceId = Database.executeBatch(objDelShareAccMergBch, 25);
    }
    
}