public with sharing class PresupuestoDetailController {

	private final Presupuesto__c presupuesto;

    public PresupuestoDetailController(ApexPages.StandardController stdController) {
        seccionesDetalleDescripcion = new Map<String,String>();
        if(!Test.isRunningTest()){
            stdController.addFields(camposDeFieldSet());
        }

        this.presupuesto = (Presupuesto__c)stdController.getRecord();
    }

    public List<String> camposDeFieldSet(){
        Map<String, Schema.FieldSet> mapaFS = Schema.SObjectType.Presupuesto__c.fieldSets.getMap();
        List<String> listCamposDeFieldSet = new List<String>();
        for(String mes : mesesOrdenados){
            if(mapaFS.containsKey(mes)){
                for(FieldSetMember fsm : mapaFS.get(mes).getFields()){
                    listCamposDeFieldSet.add(fsm.getFieldPath());
                }
            }
        }
        for(String secc : seccionesDetalle){
            if(mapaFS.containsKey(secc)){
                for(FieldSetMember fsm : mapaFS.get(secc).getFields()){
                    listCamposDeFieldSet.add(fsm.getFieldPath());
                    seccionesDetalleDescripcion.put(secc,mapaFS.get(secc).getLabel());
                }
            }
        }
        return listCamposDeFieldSet;
    }

    public List<String> seccionesDetalle{
        get{
            if(seccionesDetalle==null){
                seccionesDetalle = new List<String>{
                    'DatosPeriodoFiscalPresupuesto'
                };
            }
            return seccionesDetalle;
        }
        set;
    }

    public Map<String,String> seccionesDetalleDescripcion{
        get;
        set;
    }

    public List<String> mesesOrdenados{
        get{
            return Constantes.MESES_ORDENADOS;
        }
        set;
    }

    public List<String> cabecerasDetail{
        get{
            if(cabecerasDetail==null){
                cabecerasDetail = new List<String>{
                    'Mes',
                    'Autorizado $', 
                    'Asignado $', 
                    'Disponible por Asignar $',
                    'Estimado $',
                    'Disponible Estimado $',
                    'Real $',
                    'Disponible Real $',
                    'Autos Demo Activos',
                    'Real unidades'
                };
            }
            return cabecerasDetail;
        }
        set;
    }

    public List<String> cabecerasEdit{
        get{
            if(cabecerasEdit==null){
                cabecerasEdit = new List<String>{
                    'Mes',
                    'Monto autorizado $'
                };
            }
            return cabecerasEdit;
        }
        set;
    }
}