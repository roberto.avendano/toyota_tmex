@isTest
private class MovimientoTriggerTest {

    static testMethod void myUnitTest() {
    	
    	List<RecordType> rtDealer = [select DeveloperName, Id, IsActive, Name, SobjectType from RecordType where SobjectType='Account' and DeveloperName='Dealer' limit 1];
    	
    	Account a01 = new Account(
    		RecordTypeId = rtDealer.get(0).Id,
    		Name = 'Dealer',
    		Codigo_Distribuidor__c = '12345'
    	);
    	insert a01;
    	
        Vehiculo__c v01 = new Vehiculo__c(
        	Name='12345678901234567',
        	Id_Externo__c='12345678901234567'
        );
        insert v01;
        
        Serie__c ser01 = new Serie__c(
        	Id_Externo_Serie__c = 'X',
        	Name = 'X'
        );
        insert ser01;
        
        Modelo__c modelo01 = new Modelo__c(
        	Serie__c = ser01.Id,
        	ID_Modelo__c = 'Y'
        );
        insert modelo01;
        
        Objetivo_de_Venta__c ov01 = new Objetivo_de_Venta__c(
        	Distribuidor__c = a01.Id,
        	Serie__c = ser01.Id,
        	Anio__c = '2015',
        	Mes__c = '1',
        	MK__c = 1,
        	Objetivo__c = 1
        );
        insert ov01;
        
        Movimiento__c m01 = new Movimiento__c(
        	Distribuidor__c = a01.Id,
        	Submitted_Date__c = Datetime.now(),
        	VIN__c = v01.Id,
        	Id_Modelo__c = modelo01.Id
        );
        insert m01;
        
    }
}