/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase de prueba que contiene la logica de TAM_ActLimDiasApdoDeskCtrl
                        para caoturar el limite de dias permitido para tener un VIN
                        que esta en estatus de G

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha                Autor                       Descripcion
    1.0    27-Enero-2021        Héctor Figueroa             Creación
******************************************************************************* */

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TAM_ActLimDiasApdoDeskCtrl_Tst {

    static  Account Distrib = new Account();
    
    @TestSetup static void loadData(){
        Test.startTest();
        
            Id p = [select id from profile where name='Partner Community User'].id;
            
            Account ac = new Account(
                name ='TOYOTA POLANCO',
                Codigo_Distribuidor__c = '57000',
                TAM_GestorLeads__c = UserInfo.getUserId(),
                TAM_NoDiasApartado__c = '10'
            );              
            insert ac; 
                        
            Contact con = new Contact(
                LastName ='testCon',            
                AccountId = ac.Id
            );
            insert con;  
            
            User user = new User(
                alias = 'test123p', 
                email='test123tfsproduc@noemail.com',
                Owner_Candidatos__c= true,
                emailencodingkey='UTF-8', 
                lastname='Testingproductfs', 
                languagelocalekey='en_US',
                localesidkey='en_US', 
                profileid = p, 
                country='United States',
                IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', 
                username='testOwnertfsproduc@noemail.com.test'
            );
            insert user;
        
        TAM_MaximoNumeroLeads__c testCustum = new TAM_MaximoNumeroLeads__c();
        testCustum.TAM_CodigoDealer__c = '57000';
        testCustum.Id_Externo__c = '57000';
        testCustum.TAM_MaximoLeads__c = '20';
        insert testCustum;
            
        Test.stopTest();

    }

    static testMethod void TAM_ActLimDiasApdoDeskCtrlOK() {
        System.debug('En TAM_ActLimDiasApdoDeskCtrlOK...');
        
        Distrib = [Select id, Codigo_Distribuidor__c, TAM_GestorLeads__c From Account Limit 1];
        System.debug('En TAM_ActLimDiasApdoDeskCtrlOK Distrib: ' + Distrib);
        
        /*//Llama al metodo getNoDistlUserActual
        TAM_ActLimDiasApdoDeskCtrl.getNoDistlUserActual();
        //Llama al metodo getUsuarosDealer      
        TAM_ActLimDiasApdoDeskCtrl.getUsuarosDealer(Distrib.Codigo_Distribuidor__c);
        
        //Crea un objeto del tipo TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist
        TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist objWrpListaUsuariosDist = 
            new TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist('Usuario Pruba ', 'pruebauser@hotmail.com', 'Gestor Leads', 
            'Gestor Leads', true, UserInfo.getUserId(), 100, 100, 100);
        TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist objWrpListaUsuariosDist2 = new TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist();
        //Una lista del tipo TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist
        List<TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist> lObjWrpListaUsuariosDist = 
            new List<TAM_ActLimDiasApdoDeskCtrl.wrpListaUsuariosDist>{objWrpListaUsuariosDist};
        System.debug('En TAM_ActLimDiasApdoDeskCtrlOK antes de AcctUsrSegLead lObjWrpListaUsuariosDist: ' + lObjWrpListaUsuariosDist);
        
        //Lalama al metod de  AcctUsrSegLead
        TAM_ActLimDiasApdoDeskCtrl.AcctUsrSegLead(Distrib.Codigo_Distribuidor__c, lObjWrpListaUsuariosDist);
        */
    }
    
     static testMethod void TAM_ActLimDiasApdoDeskCtrlOK2() {
        User usr = [Select id from User Where email='test123tfsproduc@noemail.com'];
        TAM_ActLimDiasApdoDeskCtrl.consultaMaxNoDiasAdo(usr.id);
        TAM_ActLimDiasApdoDeskCtrl.seteaValorMaximoLeads(usr.id, '100');
        
    }

}