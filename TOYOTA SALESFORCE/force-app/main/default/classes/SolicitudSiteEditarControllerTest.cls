@isTest
public class SolicitudSiteEditarControllerTest {
    
    @isTest
    static void test_one(){
        Date inicio = date.parse('01/04/2018');
        Date fin = date.parse('30/04/2018');
        
        RecordType accRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Account' 
                            AND Name='Distribuidor'];
        
        Account acc = new Account(Name='TMEX Account Test',
                                  Compania__c='TMEX',
                                  Pais__c='México',
                                  RecordTypeId = accRT.Id);
        insert acc;
        
        VehiculoSIV__c vsiv = new VehiculoSIV__c(Name='ML1062 Yaris R XLE 6AT L4 FWD',
                                                 NombreVehiculo__c = 'ML1062 Yaris R XLE 6AT L4 FWD',
                                                 AnoModelo__c = '2018',
                                                 PrecioPublico__c = 238800,
                                                 PrecioTotalEmpleado__c = 216800,
                                                 InicioVigencia__c = inicio,
                                                 FinVigencia__c = fin,
                                                 VehiculoDisponiblePuestos__c='A');
        insert vsiv;
        
        PoliticaAutosPoolAsignados__c paa = new PoliticaAutosPoolAsignados__c(Name='Director de área',
                                                                              VehiculoSIV__c = vsiv.Id);
        insert paa;
        
        RecordType conRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='Contact' 
                            AND DeveloperName='TMEX'];
        
        Contact c = new Contact(AccountId= acc.Id, 
                                LastName= 'Contact TMEX Test', 
                                Estatus__c = 'Activo',
                                Departamento__c = 'Human Resources',
                                Codigo__c = '456RTY',
                                Puesto__c	= 'Chief Coordinating Officer',
                                PuestoEmpleado__c = paa.Id,
                                Email ='aramos@grupoassa.com', 
                                //Permiso_de_Privacidad__c = false,
                                RecordTypeId = conRT.Id);
        insert c;
        
        ColorExterno__c ce = new ColorExterno__c(Name = 'MAGNETIC GRAY MET',
                                                 CodigoColor__c='1G3',descripcion__c = '1G3');
        insert ce;
        
        ColorInterno__c ci = new ColorInterno__c(Name ='MID BLUE BLACK',
                                                 CodigoColor__c	='70',descripcion__c = '70');
        insert ci;
        //CREACION DE SERIES
        Serie__c s = new Serie__c(
            Name = 'C-HR',
            Marca__c='Toyota',
            CdigoModeloFinanzas__c='230418',
            Id_Externo_Serie__c='2304'
        );
        insert s;
        
        //CREACION DE PRODUCTOS          
        Id stdPBId = Test.getStandardPricebookId();
        Product2 p1 = new Product2(
            Serie__c = s.Id,
            NombreVersion__c = '2304 C-HR A',
            Name = '2304 C-HR A',
            IsActive = true,
            IdExternoProducto__c= '2304C-HRA'
        );
        insert p1;
        
        ColorInternoModelo__c cim = new ColorInternoModelo__c(
            Name = ci.Name, 
            Modelo__c = p1.Id, 
            IDExterno__c = 'TESTCOLORINTERNOMODELO01',
            CodigoColorInterno__c = ci.CodigoColor__c, 
            AgnioModelo__c = '2018'
        );
        insert cim;
        
        ColorExternoModelo__c cem = new ColorExternoModelo__c(
            Name = ce.Name, 
            ColorInternoModelo__c = cim.Id,
            Modelo__c = cim.Modelo__c, 
            ID_Externo__c = 'TESTCOLOREXTERNOMODELO01' ,
            CodigoColorExterno__c = ce.CodigoColor__c ,
            AnioModelo__c = '2018',
            Tipo__c = 'Estandar'
        );
        insert cem;
        
        RecordType sivRT = [SELECT Id, Name, SobjectType, DeveloperName 
                            FROM RecordType WHERE SobjectType='SolicitudInternaVehiculos__c' 
                            AND DeveloperName='PoolAsignacion'];
        
        List<SolicitudInternaVehiculos__c> sivs = new List<SolicitudInternaVehiculos__c>();
        
        SolicitudInternaVehiculos__c siv1 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             Distribuidor_recepcion__c = acc.id,
                                                                             ColorInternoOpcion1__c = cim.id,
                                                                             ColorExternoOpcion1__c = cem.id,
                                                                             RecordTypeId = sivRT.Id,
                                                                             TipoSolicitud__c = 'Asignación',
                                                                             Estatus__c='Nuevo',
                                                                             AnioModelo__c = '2018',
                                                                             Modelo__c = p1.Id);
        
        SolicitudInternaVehiculos__c siv2 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             Distribuidor_recepcion__c = acc.id,
                                                                             
                                                                             TipoSolicitud__c = 'Asignación',
                                                                             MontoMaximoSolicitud__c = 247100,
                                                                             RecordTypeId = sivRT.Id,
                                                                             Estatus__c='En procesos de Autorización',
                                                                             
                                                                             VehiculoSolicitado__c = vsiv.Id,
                                                                             PrecioEmpleado__c = 215905,
                                                                             AnioModelo__c = '2018',
                                                                             Modelo__c = p1.Id,
                                                                             ColorInternoOpcion1__c = cim.Id,
                                                                             ColorInternoOpcion2__c = cim.Id,
                                                                             ColorExternoOpcion1__c = cem.Id,
                                                                             ColorExternoOpcion2__c = cem.Id,
                                                                             DistribuidoraDondeDeseaRecibir__c = '57001 DALTON TOYOTA LOPEZ MATEOS',
                                                                             QuienRecogevehculo__c = 'Solicitante'
                                                                            );
        
        SolicitudInternaVehiculos__c siv3 = new SolicitudInternaVehiculos__c(Solicitante__c = c.Id,
                                                                             RecordTypeId = sivRT.Id,
                                                                             TipoSolicitud__c = 'Pool',
                                                                             Estatus__c='Nuevo',
                                                                             AnioModelo__c = '2018',
                                                                             Modelo__c = p1.Id);
        
        
        system.debug('colors'+siv2);
        sivs.add(siv1);
        sivs.add(siv2); 
        sivs.add(siv3);
        insert sivs;
        
        List<String> sivsId = new List<String>();
        for(SolicitudInternaVehiculos__c Sin : sivs){
            sivsId.add(Sin.id);
        }
        
        
        test.startTest();
        
        PageReference page = System.Page.SolicitudSiteEPoolSolicitante;
        Test.setCurrentPage(page);
        SolicitudSiteEditarController ssec = new SolicitudSiteEditarController(new ApexPages.standardController(sivs[1]));
        page = ssec.vehicleSelectedUpdate();   
        page = ssec.externalColor1Update();
        page = ssec.externalColor2Update();
        page = ssec.updateSolicitudFields();
        page = ssec.enviarAprobacion();
        page = ssec.updateAvisoPrivacidad();
        ssec.editar();  
        ssec.cancelarEdi();  
        ssec.guardar();
        ssec.actualizar();
        
        SolicitudSiteEditarController ssec2 = new SolicitudSiteEditarController(new ApexPages.standardController(sivs[1]));
        List<SelectOption> options = ssec2.DistribuidorList;
        List<SelectOption> selectVehicle = ssec2.getVehiclesSIVOptions();
        List<SelectOption> getExternalColor1 = ssec2.getExternalColor1();
        List<SelectOption> getExternalColor2 = ssec2.getExternalColor2();
        ssec2.editar();  
        ssec2.cancelarEdi();  
        ssec2.guardar();
        ssec2.vehicleSelectedUpdate();
        ssec2.externalColor1Update();
        ssec2.externalColor2Update();
        ssec2.actualizar();
        
        SolicitudSiteEditarController ssec3 = new SolicitudSiteEditarController(new ApexPages.standardController(sivs[2]));
        List<SelectOption> vehicleOptions2 = ssec3.getVehiclesSIVOptions();
        ssec3.editar();  
        ssec3.cancelarEdi();  
        ssec3.guardar();
        ssec.vehicleSelectedUpdate();
        ssec.externalColor1Update();
        ssec.externalColor2Update();
        ssec3.actualizar(); 
        test.stopTest();
        //System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
    }

}