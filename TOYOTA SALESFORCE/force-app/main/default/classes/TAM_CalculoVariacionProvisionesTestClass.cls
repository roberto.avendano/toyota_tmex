/********************************************************************************
Desarrollado por:   Globant de México
Proyecto:           TOYOTA TAM
Descripción:        Clase prueba para TAM_CalculoVariacionProvisionesClass
__________________________________________________________________________________
Autor:							Fecha:               	Descripción:
__________________________________________________________________________________
Cecilia Cruz Morán        		09/Junio/2021      		Versión Inicial
__________________________________________________________________________________
*********************************************************************************/
@isTest(SeeAllData=false)
public class TAM_CalculoVariacionProvisionesTestClass {
    
    //Constante
    static final String STRING_AUTORIZADO_TMEX = 'Pago Autorizado TMEX'; 

    @testSetup static void setup() {
        Integer intCantidadRegistros = 20;
        
        //Vehiculos
        List<Vehiculo__c> lstVehiculos = new List<Vehiculo__c>();
        for(Integer i=0; i<=intCantidadRegistros; i++){
            Vehiculo__c objVehiculo = new Vehiculo__c();
            objVehiculo.Name = 'JTDKDTB33M00000' + i;
     		lstVehiculos.add(objVehiculo);
        }
        Database.insert(lstVehiculos); 
        
        //Provisión Incentivos
        TAM_ProvisionIncentivos__c objProvision = new TAM_ProvisionIncentivos__c();
        objProvision.Name = 'Provisión Prueba';
        objProvision.TAM_AnioDeProvision__c = '2021';
        objProvision.TAM_MesDeProvision__c = 'Enero';
        Database.insert(objProvision);
        
        //Detalle Provisión Incentivos
        List<TAM_DetalleProvisionIncentivo__c> lstDetalleProvision =  new List<TAM_DetalleProvisionIncentivo__c>();
        Double dblIncentivoInicial = 20000;
        for(Vehiculo__c objVehiculo : lstVehiculos){
            dblIncentivoInicial = dblIncentivoInicial - 500;
            TAM_DetalleProvisionIncentivo__c objDetalleProvision = new TAM_DetalleProvisionIncentivo__c();
            objDetalleProvision.TAM_ProvisionIncentivos__c = objProvision.Id;
            objDetalleProvision.TAM_VIN__c = objVehiculo.Id;
            objDetalleProvision.TAM_IncentivoTMEX_Efectivo__c = dblIncentivoInicial;
            lstDetalleProvision.add(objDetalleProvision);
        }
        Database.insert(lstDetalleProvision); 
        
        //Detalle Estado de Cuenta
        List<TAM_DetalleEstadoCuenta__c> lstDetalleCuenta =  new List<TAM_DetalleEstadoCuenta__c>();
        Double dblIncentivoPagado = 500;
        for(Vehiculo__c objVehiculo : lstVehiculos){
            dblIncentivoPagado = dblIncentivoPagado +500;
            TAM_DetalleEstadoCuenta__c objDetalleCuenta = new TAM_DetalleEstadoCuenta__c();
            objDetalleCuenta.TAM_VIN__c = objVehiculo.Name;
            objDetalleCuenta.TAM_Monto_sin_IVA__c = dblIncentivoPagado;
            objDetalleCuenta.TAM_Historico_Estado_Cuenta__c = false;
            objDetalleCuenta.TAM_Estatus_Finanzas__c = STRING_AUTORIZADO_TMEX;
            lstDetalleCuenta.add(objDetalleCuenta);
        }
        Database.insert(lstDetalleCuenta); 
        
        //Variacion
        TAM_VariacionProvisiones__c objVariacion = new TAM_VariacionProvisiones__c();
        objVariacion.TAM_DetalleEstadoCuenta__c = lstDetalleCuenta[0].Id;
        objVariacion.TAM_DetalleProvision__c = lstDetalleProvision[0].Id;
        insert objVariacion;
    }
    
    //Test Class TAM_CalculoVariacionBatch
    @isTest
    public static void TAM_CalculoVariacionBatch_Test(){
        Test.startTest();
        TAM_CalculoVariacionBatch objCalculo = new TAM_CalculoVariacionBatch();
        Id batchId = Database.executeBatch(objCalculo);
        Test.stopTest();
        System.assertNotEquals(0, [SELECT count() FROM TAM_VariacionProvisiones__c]);
    }
    
    //Test Class TAM_VariacionProvisionesController
    @isTest
    public static void TAM_VariacionProvisionesController_Test(){
        //Obtener Información
        TAM_ProvisionIncentivos__c objProvision = [SELECT Id FROM TAM_ProvisionIncentivos__c LIMIT 1];
        Test.startTest();
        List<TAM_VariacionProvisiones__c> lstrecordReturn = TAM_VariacionProvisionesController.getVariaciones(objProvision.Id, NULL, '', NULL);
        System.debug(lstrecordReturn.size());
        TAM_VariacionProvisionesController.updateRecord(NULL);
        Test.stopTest();
        System.assertNotEquals(0, lstrecordReturn.size());
    }
    
    //Test Schedule
    @isTest
    public static void Shedule_Test(){
        Test.startTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('ScheduledApexTest',CRON_EXP,new TAM_VariacionProvisionSchedulable());
        List<TAM_VariacionProvisiones__c> lt = [SELECT Id FROM TAM_VariacionProvisiones__c];
        System.assertNotEquals(0, lt.size(), 'No se inserto ningun registro de variacion');
        Test.stopTest();
    }
}