/* 
   Productivo
   Usuario de Leslie 005i00000018E7C
   Usuario de Admin  005i0000000HOf6
   Imagen https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000000lxwy&oid=00Di0000000afp8&lastMod=1377282768000
   
   Sandbox
   Usuario de Leslie 005i00000018E7C
   Usuario de Admin  005i0000000HOf6
   Imagen https://c.cs14.content.force.com/servlet/servlet.ImageServer?id=015c0000000At2E&oid=00Dc0000003sGV2&lastMod=1406658284000

   Para ejecutar la clase y dejar programada la clase
      String day          = string.valueOf(system.now().day());
      String month        = string.valueOf(system.now().month());
      String hour         = string.valueOf(system.now().hour());
      String minute       = string.valueOf(system.now().minute() + 1);
      String second       = string.valueOf(system.now().second());
      String year         = string.valueOf(system.now().year());
      String strJobName   = 'Envio de Emails';
      String strSchedule  = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
      System.schedule(strJobName, strSchedule, new SendEmail ());
*/

global class SendEmail implements Schedulable {
	private List<String>emails;
	private List<User>usuario;
	private String html = '<html><body><center><img src="https://c.na15.content.force.com/servlet/servlet.ImageServer?id=015i0000000lxwy&oid=00Di0000000afp8&lastMod=1377282768000"><hr><font face="verdana" color="green">Se aproxima la carga del archivo de ventas del mes actual</font><hr></center></body></html>';		
	
	global void execute(SchedulableContext sc) {
		usuario = [Select Id, Email From User];
		emails = new List<String>();
			
		for(User forUsuarios : usuario) {
			if(forUsuarios.Id == '005i00000018E7C' /*Leslie*/ || forUsuarios.Id == '005i0000000HOf6' /*Administrador*/)			
			emails.add(forUsuarios.Email);
		}
			
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			
		if(emails[0] != null && emails[1] != null) {
			email.setSubject('Proxima carga de archivo de reversas');
			email.setToAddresses(emails);
			email.setHtmlBody(html);
			
			Messaging.SendEmailResult [] send_mail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
		}
	}		
}