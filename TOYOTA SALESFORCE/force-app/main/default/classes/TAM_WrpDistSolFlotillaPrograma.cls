/******************************************************************************** 
    Desarrollado por:   Avanxo México
    Autor:              Héctor Figueroa
    Proyecto:           TOYOTA TAM
    Descripción:        Clase que contiene la logica para asociar los distribuidores cuando se trata de una Venta Especial
    					Para Flotilla o Programa.

    Infomación de cambios (versiones)
    ===============================================================================
    No.    Fecha             	Autor                       Descripcion
    1.0    10-Diciembre-2019    Héctor Figueroa             Creación
******************************************************************************* */

public class TAM_WrpDistSolFlotillaPrograma {

    @AuraEnabled 
    public Integer intIndex {get;set;}    
    @AuraEnabled 
    public Boolean bolEliminar {get;set;}
    @AuraEnabled 
    public String strClaveNomDistrib {get;set;}    
    @AuraEnabled 
    public String strAnioModelo {get;set;}    
    @AuraEnabled 
    public String strSerie {get;set;}    
    @AuraEnabled 
    public String strModelo {get;set;}    
    @AuraEnabled 
    public String strVersion {get;set;}   
    @AuraEnabled 
    public String strIdColExt {get;set;}    
    @AuraEnabled 
    public String strIdColInt {get;set;}         
    @AuraEnabled 
    public String strIdExterno {get;set;}
    @AuraEnabled 
    public Integer intCantidad {get;set;}
    @AuraEnabled 
    public String sVIN {get;set;}    
    @AuraEnabled 
    public String strIdDistrib {get;set;}    
    @AuraEnabled 
    public List<TAM_WrpDistribuidores> listWrpDistrib {get;set;}
    @AuraEnabled 
    public Boolean bolDisable {get;set;}
    @AuraEnabled 
    public Boolean bolSeleccionado {get;set;}
    @AuraEnabled 
    public String strEstatusVIN {get;set;}    
            
    //Un constructor por default
    public TAM_WrpDistSolFlotillaPrograma(){
    	this.intIndex = 0;
    	this.bolEliminar = false;
    	this.strClaveNomDistrib = '';
    	this.strAnioModelo = '';
    	this.strSerie = '';
    	this.strModelo = '';
    	this.strVersion = '';
    	this.strIdExterno = '';
    	this.intCantidad = 0;
    	this.sVIN = '';
    	this.strIdDistrib = '';
    	this.listWrpDistrib = new List<TAM_WrpDistribuidores>();
    	this.bolDisable = false;
    	this.bolSeleccionado = false;
    }
    
    //Un constructor con parametros 
    public TAM_WrpDistSolFlotillaPrograma(Integer intIndex, Boolean bolEliminar, String strClaveNomDistrib,
    	String strAnioModelo, String strSerie, String strModelo, String strVersion, String strIdExterno,
    	Integer intCantidad, String strIdDistrib, List<TAM_WrpDistribuidores> listWrpDistrib){
		this.intIndex = intIndex;
    	this.bolEliminar = bolEliminar;
    	this.strClaveNomDistrib = strClaveNomDistrib;
   		this.strAnioModelo = strAnioModelo;
    	this.strSerie = strSerie;
    	this.strModelo = strModelo;
    	this.strVersion = strVersion;
    	this.strIdExterno = strIdExterno;
    	this.intCantidad = intCantidad;
    	this.strIdDistrib = strIdDistrib;
    	this.listWrpDistrib = listWrpDistrib;
    }
    
}